author Michel Basili

///////////
WARNING: //
///////////

The focus of the work is on the implementation of pure ray tracing, which
requires considerable processing time. If the time taken to process the ray
tracing is excessive, both applications crash due to timeout limits imposed
by operating systems. However, it is possible to avoid this problem by
modifying an operating system's timeout registers.

// Windows:
To configure the registers, please consult the following link:
https://learn.microsoft.com/en-us/windows-hardware/drivers/display/tdr-registry-keys

Configuration used:

TdrLevel => value = 0
TdrDelay => value = 120 (hexadecimal) == 288 (decimal)
TdrDdiDelay => value = 120 (hexadecimal) == 288 (decimal)
TdrLimitCount => value = 120 (hexadecimal) == 288 (decimal)

Once the registers have been updated, restart the computer to allow the changes
to be applied.

Anti-virus:
If you run the programs be sure to disable the anti virus if you have one
 as it may conflict with the application.

///////////////////////////
Development Environment: //
///////////////////////////

Visual studio 2019.

To build the application in debug mode, the Vulkan SDK version 1.3.204.1
must be installed.
It can be found at the following link:
https://vulkan.lunarg.com/sdk/home#windows


////////////////
Keys mapping: //
////////////////

1 => First scene
2 => Second scene
3 => Third scene
4 => Fourth scene
5 => Fifth scene
6 => Sixth scene
7 => Seventh scene
7 => Eighth scene

o => Enable/Disable animation
g => Enable/Disable GUIs


Overv_vk specific:

p + mouse cursor position => Select an object to change its properties

w=> move camera forward
s=> move camera right
a=> move camera left
d=> move camera backward
q=> move camera down
e=> move camera up

mouse movement + mouse right button => rotate the camera


//////////////////////
Scene modification: //
//////////////////////

If you want to change the properties of scene geometries, you can only do so
via the Vulkan implementation. So customise the scene in the Vulkan
implementation and later display it in Overv_gl. If you use visual studio
to run programmes, the resources folder is shared. If, on the other hand,
".exe" files are used, the ".ovo" file of the modified scene must be moved
 to the "resources" folder used by Overv_gl.
