
//////////////
// #INCLUDE //
//////////////

// Main engine header:
#include "overv_vk.h"

// C/C++:
#include <iostream>
#include <chrono>
#include <fstream>

// GLM
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/euler_angles.hpp>

// Assimp
//#include <assimp/postprocess.h>

//////////   
// VARS //
//////////

// General status:
double oldMouseX, oldMouseY;
float rotX, rotY;
float spinSpeed = 30.0f;
bool mouseBR, mouseBL, isPicking, startRotation, resetNrFramesAO, isAnimationOn;
glm::vec3 cameraPosition;
glm::mat4 rot_matrix;
glm::vec3 lookAt;
glm::vec3 sideVec;
std::ofstream reportTestFile;
uint32_t currentScene = 1, lastScene = 1;
std::string nameOfTheSceneToSave = "first_scene_comparison.ovo";
bool isGUIEnabled = true;

// Pipelines
OvVK::PickingPipeline pickingPipeline;
OvVK::WhittedRTwithPBRPipeline whittedRTwithPBRPipeline;
OvVK::GUIPipeline guiPipeline;

///////////////
// CALLBACKS //
///////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Mouse cursor callback.
 * @param mouseX updated mouse X coordinate
 * @param mouseY updated mouse Y coordinate
 */
void mouseCursorCallback(double mouseX, double mouseY)
{
	if (mouseBR) {
		// ENG_LOG_DEBUG("x: %.1f, y: %.1f", mouseX, mouseY);
		float deltaX = (startRotation ? 0.0f : (float)(mouseX - oldMouseX));
		oldMouseX = mouseX;
		rotX -= deltaX;
		// Clamp rotX in the 0-360 range. Not strictly necessary but it makes values cleaner.
		rotX -= ((int)rotX / 360) * 360;
		rotX = rotX < 0 ? 360 + rotX : rotX;

		float deltaY = (startRotation ? 0.0f : (float)(mouseY - oldMouseY));
		oldMouseY = mouseY;
		rotY -= deltaY;
		rotY = glm::clamp(rotY, -90.0f, 90.0f);

		startRotation = false;
		// Because moving the camera
		resetNrFramesAO = true;
	}
	else 
	{
		oldMouseX = mouseX;
		oldMouseY = mouseY;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Mouse button callback.
 * @param button mouse button id
 * @param action action
 * @param mods modifiers
 */
void mouseButtonCallback(int button, int action, int mods)
{
	// ENG_LOG_DEBUG("button: %d, action: %d, mods: %d", button, action, mods);
	switch (button)
	{
	case 0: mouseBL = (bool)action; break;
	case 1:
		mouseBR = (bool)action;
		if (mouseBR) {
			startRotation = true;
		}
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Mouse scroll callback.
 * @param scrollX updated mouse scroll X coordinate
 * @param scrollY updated mouse scroll Y coordinate
 */
void mouseScrollCallback(double scrollX, double scrollY) {}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method keyboard callback.
 * @param key The keyboard key that was pressed or released.
 * @param scancode The system-specific scancode of the key.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
void keyboardCallback(int key, int scancode, int action, int mods)
{
	// A few useful defines (https://www.glfw.org/docs/3.3/group__keys.html):
	#define GLFW_RELEASE   0
	#define GLFW_PRESS     1	

	#define GLFW_KEY_1	   49
	#define GLFW_KEY_2	   50
	#define GLFW_KEY_3	   51
	#define GLFW_KEY_4	   52
	#define GLFW_KEY_5	   53
	#define GLFW_KEY_6	   54
	#define GLFW_KEY_7	   55
	#define GLFW_KEY_8	   56
	#define GLFW_KEY_G	   71
	#define GLFW_KEY_O     79
	#define GLFW_KEY_P     80

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_G:
			isGUIEnabled = !isGUIEnabled;
			break;
		case GLFW_KEY_1:
			currentScene = 1;
			break;
		case GLFW_KEY_2:
			currentScene = 2;
			break;
		case GLFW_KEY_3:
			currentScene = 3;
			break;
		case GLFW_KEY_4:
			currentScene = 4;
			break;
		case GLFW_KEY_5:
			currentScene = 5;
			break;
		case GLFW_KEY_6:
			currentScene = 6;
			break;
		case GLFW_KEY_7:
			currentScene = 7;
			break;
		case GLFW_KEY_8:
			currentScene = 8;
			break;
		case GLFW_KEY_O:
			isAnimationOn = !isAnimationOn;
			break;
		case GLFW_KEY_P:
			isPicking = true;
			break;
		}
	}
	else if (action == GLFW_RELEASE)
	{
		switch (key)
		{
		case GLFW_KEY_P:
			isPicking = false;
			break;
		}
	}
}


//////////
// MAIN //
//////////

/**
 * Application entry point.
 * @param argc number of command-line arguments passed
 * @param argv array containing up to argc passed arguments
 * @return error code (0 on success, error code otherwise)
 */
int main() {
	//////////
	// Config:
	Ov::Config config;
	config.setWindowTitle("OvVK Test Ray tracing");
	config.setWindowSizeX(800);
	config.setWindowSizeY(800);
	config.setNrOfAASamples(1);

	///////////////
	// Init engine:
	OvVK::Engine& engine = OvVK::Engine::getInstance();
	engine.init(config);

	// Register callbacks:
	engine.getPresentSurface().setMouseScrollCallback(mouseScrollCallback);
	engine.getPresentSurface().setMouseButtonCallback(mouseButtonCallback);
	engine.getPresentSurface().setMouseCursorCallback(mouseCursorCallback);
	engine.getPresentSurface().setKeyboardCallback(keyboardCallback);

	{
		//////////////////
		// Loading scenes:   
		OvVK::Container container;
		OvVK::Ovo scenes;

		glm::mat4 rootMatrix = glm::mat4(1.0f);
		// In Vulkan the origin is in the top left of the screen, with Y pointing downwards.
		// Quick and easy fix would be just inverting all y coordinates
		rootMatrix[1][1] = -1.0f;
		// load scens
		std::reference_wrapper<Ov::Node> root_scene1 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "first_scene_comparison.ovo", container);
		root_scene1.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene2 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "second_scene_comparison.ovo", container);
		root_scene2.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene3 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "third_scene_comparison.ovo", container);
		root_scene3.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene4 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "fourth_scene_comparison.ovo", container);
		root_scene4.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene5 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "geometries_sharing_scene.ovo", container);
		root_scene5.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene6 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "multiple_reflection_scene.ovo", container);
		root_scene6.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene7 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "anti_aliasing.ovo", container);
		root_scene7.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene8 = scenes.load(OvVK::Engine::modelsSourceFolder +
			"\\" + "presentation_final_scene.ovo", container);
		root_scene8.get().setMatrix(rootMatrix);

		// Animated geometries of scene 5 and 8
		std::list<Ov::Node>& nodes = container.getNodeList();

		std::reference_wrapper<Ov::Node> rotate1 = Ov::Node::empty;
		std::reference_wrapper<Ov::Node> rotate2 = Ov::Node::empty;
		std::reference_wrapper<Ov::Node> rotate3 = Ov::Node::empty;
		std::reference_wrapper<Ov::Node> rotate4 = Ov::Node::empty;

		for (auto& node : nodes) {
			if (node.getName().find("rotate1_Node") != std::string::npos)
				rotate1 = node;
			if (node.getName().find("rotate2_Node") != std::string::npos)
				rotate2 = node;
			if (node.getName().find("rotate3_Node") != std::string::npos)
				rotate3 = node;
			if (node.getName().find("rotate4_Node") != std::string::npos)
				rotate4 = node;
		}

		std::reference_wrapper<Ov::Node> root = root_scene1;

		OvVK::Camera camera;
		cameraPosition = glm::vec3(0, -50, 170);

		// Rendering elements:
		OvVK::RTScene scene;
		scene.init();

		// Picking pipeline
		pickingPipeline.create();

		// Ray tracing with PBR pipeline
		whittedRTwithPBRPipeline.create();
		whittedRTwithPBRPipeline.setNrOfAASamples(1);
		whittedRTwithPBRPipeline.setRayRecursionDepth(0);
		// Enable only in case one needs to measure the time required for the commands
		// used to perform ray tracing and calculate the lighting model.
		// whittedRTwithPBRPipeline.setAnalysingTimeRequired(true);

		// GUI pipeline
		guiPipeline.create();
		guiPipeline.addDrawGUIElement(scene.getId(), scene);
		guiPipeline.addDrawGUIElement(whittedRTwithPBRPipeline.getId(), whittedRTwithPBRPipeline);

		std::chrono::high_resolution_clock timer;
		auto lastFrameTime = timer.now();
		float deltaTime = 0.0f;
		float processingTime = 0.0f;
		float renderingTime = 0.0f;
		float frameTime = 0.0f;

		reportTestFile.open("scene_measurements_vulkan.csv");
		reportTestFile << "Processing; Rendering; Frame\n";

		///////////////
		// Main loop:
		std::cout << "Entering main loop..." << std::endl;
		while (engine.processEvents())
		{
			// If windows minimized skip rendering
			// so no render for nothing
			if (engine.getPresentSurface().isMinimize())
				continue;

			//Update camera rotationand direction matrices
			glm::mat4 ground_rot = glm::rotate(glm::mat4(1.0f), glm::radians(rotX), { 0.0f, 1.0f, 0.0f });
			glm::vec3 ground_sideVec = ground_rot * glm::vec4({ 1.0f, 0.0f, 0.0f, 0.0f });
			rot_matrix = glm::rotate(glm::mat4(1.0f), glm::radians(-rotY), ground_sideVec) * ground_rot;
			lookAt = rot_matrix * glm::vec4({ 0.0f, 0.0f, -1.0f, 0.0f });
			sideVec = rot_matrix * glm::vec4({ -1.0f, 0.0f, 0.0f, 0.0f });

			// Move camera
			float speed = 100.0f * deltaTime / 1000.0f; // Speed in units/sec
			if (engine.getPresentSurface().queryKey('W')) {
				cameraPosition += lookAt * speed;
				// Because moving the camera
				resetNrFramesAO = true;
			}
			if (engine.getPresentSurface().queryKey('S')) {
				cameraPosition += -lookAt * speed;
				// Because moving the camera
				resetNrFramesAO = true;
			}
			if (engine.getPresentSurface().queryKey('A')) {
				cameraPosition += sideVec * speed;
				// Because moving the camera
				resetNrFramesAO = true;
			}
			if (engine.getPresentSurface().queryKey('D')) {
				cameraPosition += -sideVec * speed;
				// Because moving the camera
				resetNrFramesAO = true;
			}
			if (engine.getPresentSurface().queryKey('E')) {
				cameraPosition += glm::vec3({ 0.0f, 1.0f, 0.0f }) * speed;
				// Because moving the camera
				resetNrFramesAO = true;
			}
			if (engine.getPresentSurface().queryKey('Q')) {
				cameraPosition += glm::vec3({ 0.0f, -1.0f, 0.0f }) * speed;
				// Because moving the camera
				resetNrFramesAO = true;
			}
			if (engine.getPresentSurface().queryKey('C')) {
				glm::vec3 scale;
				glm::quat rotation;
				glm::vec3 translation;
				glm::vec3 skew;
				glm::vec4 perspective;
				glm::decompose(camera.getMatrix(), scale, rotation, translation, skew, perspective);
				std::cout << "rot: " << glm::to_string(glm::eulerAngles(rotation) * 3.14159f / 180.f) << std::endl;
				std::cout << "rot r: " << glm::to_string(glm::eulerAngles(rotation)) << std::endl;
				std::cout << "pos: " << glm::to_string(camera.getMatrix()[3]) << std::endl;
			}
			// Update camera position matrix to match
			camera.setMatrix(glm::translate(glm::mat4(1.0f), cameraPosition) * rot_matrix);
			glm::highp_dmat4x4 tmpProj = glm::perspective(glm::radians(45.0f),
				engine.getPresentSurface().getWidth() / (float)engine.getPresentSurface().getHeight(), 1.0f, 1000.0f);
			camera.setProjMatrix(tmpProj);

			// Aquire target image to present on the screen
			engine.acquireFrameImage();

			// Select scene
			switch (currentScene)
			{
			case 1:
				if (lastScene != 1) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 1;
				}
				root = root_scene1;
				nameOfTheSceneToSave = "first_scene_comparison.ovo";
				break;
			case 2:
				if (lastScene != 2) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 2;
				}
				root = root_scene2;
				nameOfTheSceneToSave = "second_scene_comparison.ovo";
				break;
			case 3:
				if (lastScene != 3) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 3;
				}
				root = root_scene3;
				nameOfTheSceneToSave = "third_scene_comparison.ovo";
				break;
			case 4:
				if (lastScene != 4) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 4;
				}

				root = root_scene4;
				nameOfTheSceneToSave = "fourth_scene_comparison.ovo";
				break;
			case 5:
				if (lastScene != 5) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 5;
				}

				root = root_scene5;
				nameOfTheSceneToSave = "geometries_sharing_scene.ovo";

				if (isAnimationOn) {
					rotate1.get().setMatrix(glm::rotate(rotate1.get().getMatrix(),
						glm::radians(-spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
					rotate2.get().setMatrix(glm::rotate(rotate2.get().getMatrix(),
						glm::radians(spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
					resetNrFramesAO = true;
				}
				break;
			case 6:
				if (lastScene != 6) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 6;
				}

				root = root_scene6;
				nameOfTheSceneToSave = "multiple_reflection_scene.ovo";
				break;
			case 7:
				if (lastScene != 7) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 7;
				}

				root = root_scene7;
				nameOfTheSceneToSave = "anti_aliasing.ovo";
				break;
			case 8:

				if (lastScene != 8) {
					resetNrFramesAO = true;
					scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
					lastScene = 8;
				}

				root = root_scene8;
				nameOfTheSceneToSave = "presentation_final_scene.ovo";

				if (isAnimationOn) {
					rotate3.get().setMatrix(glm::rotate(rotate3.get().getMatrix(),
						glm::radians(-spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
					rotate4.get().setMatrix(glm::rotate(rotate4.get().getMatrix(),
						glm::radians(spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
					resetNrFramesAO = true;
				}
				break;
			}

			// Start time to measure scene preparation time
			auto startTime = timer.now();
			
			// List processing
			if (!scene.pass(root, container)) {
				std::cout << "Fail to pass the scene graph" << std::endl;
				break;
			}
			if (!scene.render(camera.getMatrix(), camera.getProjMatrix()))
			{
				std::cout << "Fail to render the rtscene" << std::endl;
				break;
			}

			// Calculate the time taken
			auto endTime = timer.now();
			deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count() / 1000.0f;
			processingTime = (deltaTime / 1000.0f);

			// GUI enables
			if (isGUIEnabled) {
				// First render gui pipeline
				guiPipeline.render(0);
			}

			// Picking triggered
			if (isPicking) {
				pickingPipeline.render(scene);
				isPicking = false;
			}

			// Ambient occlusion set up
			if (resetNrFramesAO) {
				whittedRTwithPBRPipeline.resetAmbientOcclusionNrOfFrames();
				resetNrFramesAO = false;
			}

			// Start time to measure the time required to perform ray tracing and calculate the lighting model.
			// Warning set the analysingTimeRequired variable of the pipeline whittedRTwithPBRPipeline to TRUE (this file line 235).
			// Otherwise, it is measured how long it takes to submit commands to the device.
			startTime = timer.now();

			// Ray tracing
			whittedRTwithPBRPipeline.render(scene);

			// Calculate the time taken
			endTime = timer.now();
			deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count() / 1000.0f;
			renderingTime = (deltaTime / 1000.0f);

			// GUI enables
			if (isGUIEnabled) {
				// Second render gui pipeline
				guiPipeline.render(1);
			}

			// Draw Frame
			engine.drawFrame();


			// Compute frame time
			auto curTime = timer.now();
			// nano seconds
			deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(curTime - lastFrameTime).count() / 1000.0f;
			// seconds
			frameTime = (deltaTime / 1000.0f);
			lastFrameTime = curTime;

			// seconds
			reportTestFile << processingTime << ";" << renderingTime << ";" << frameTime << "\n";
		}
		std::cout << "Leaving main loop..." << std::endl;

		scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" + nameOfTheSceneToSave);
		reportTestFile.close();
	}

	engine.free();

	// Done:
	std::cout << "[application terminated]" << std::endl;
	return 0;
}


// Extra code
// 
// 
// To load from fbx. Warining assimp can't find roughness textures in file FBX.
// So AO textures are redead as roughness textures.
//OvVK::Loader loader;
//loader.load("first_scene_comparison.fbx", aiProcess_JoinIdenticalVertices
//	| aiProcess_OptimizeMeshes
//	| aiProcess_Triangulate
//	| aiProcess_GenSmoothNormals
//	| aiProcess_CalcTangentSpace
//	| aiProcess_FixInfacingNormals
//	| aiProcess_FindInvalidData
//	| aiProcess_ValidateDataStructure);

//std::reference_wrapper<Ov::Node> root = loader.loadScene(container);


// To save scene
//scenes.save(root, OvVK::Engine::modelsSourceFolder + "\\" +  "geometries_sharing_scene.ovo");