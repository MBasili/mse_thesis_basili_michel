/**
 * @file	overv_base_pipeline.h
 * @brief	Basic generic rendering pipeline
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


/**
 * @brief Class for setting up a rendering pipeline
 */
class OV_API Pipeline : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

    // Special values:
	static Pipeline empty;

	// Const/dest:
	Pipeline();
	Pipeline(Pipeline&& other) noexcept;
	Pipeline(Pipeline const&) = delete;
	virtual ~Pipeline();

	// Management:
	bool setProgram(Ov::Program& program);
	Ov::Program& getProgram() const;

	// Rendering methods:
	virtual bool render(const Ov::List& list);

	// Cache:
	static Pipeline& getCached();

/////////////
protected: //
/////////////

	// Cache:
	static std::reference_wrapper<Ov::Pipeline> cache;

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Pipeline(const std::string& name);
};