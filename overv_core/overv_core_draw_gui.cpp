/**
 * @file	overv_core_draw_gui.h
 * @brief	Class to draw gui using ImGui methods
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 //////////////
 // #INCLUDE //
 //////////////

 // Main inlcude:
#include "overv_core.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief DrawGUI class reserved structure.
 */
struct Ov::DrawGUI::Reserved
{
    /**
     * Constructor
     */
    Reserved()
    {}
};


///////////////////////////
// BODY OF CLASS DrawGUI //
///////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::DrawGUI::DrawGUI() :
    reserved(std::make_unique<Ov::DrawGUI::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OV_API Ov::DrawGUI::DrawGUI(const std::string& name) :
    reserved(std::make_unique<Ov::DrawGUI::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::DrawGUI::DrawGUI(DrawGUI&& other) noexcept :
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::DrawGUI::~DrawGUI()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method to draw a gui on a windows with the ImGUI library.
 */
void OV_API Ov::DrawGUI::drawGUI()
{
    OV_LOG_ERROR("The base drawGUI method of the class DrawGUI was called.");
}

#pragma endregion
