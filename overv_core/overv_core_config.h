/**
 * @file	overv_base_config.h
 * @brief	Basic generic configuration properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


/**
 * @brief Class for generic config properties.
 */
class OV_API Config
{
//////////
public: //
//////////

    // Special values:
	static Config empty;

	// Const/dest:
	Config();
	Config(Config&& other);
	Config(Config const&);
	virtual ~Config();

	// Operators:
	Ov::Config& operator=(const Ov::Config& other);

	// Get/set:   
	void setWindowTitle(const std::string& title);
	const std::string& getWindowTitle() const;
	void setWindowSizeX(uint32_t sizeX);
	uint32_t getWindowSizeX() const;
	void setWindowSizeY(uint32_t sizeY);
	uint32_t getWindowSizeY() const;
	void setVSyncFlag(bool flag);
	bool isVSync() const;
	void setNrOfAASamples(uint32_t nrOfSamples);
	uint32_t getNrOfAASamples() const;

	// Loaders:
	bool load(const std::string& filename);


/////////////
protected: //
/////////////

   // Reserved:
	struct Reserved;
	Reserved* reserved;
};