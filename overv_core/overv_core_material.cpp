/**
 * @file	overv_base_material.cpp
 * @brief	Basic generic material properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"


////////////
// STATIC //
////////////

// Special values:
Ov::Material Ov::Material::empty("[empty]");   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Material reserved structure.
 */
struct Ov::Material::Reserved
{  
   std::vector<std::reference_wrapper<const Ov::Texture>> texture;   ///< Texture levels


   /**
    * Constructor. 
    */
   Reserved() : texture{static_cast<uint32_t>(Ov::Texture::Type::last), Ov::Texture::empty}
   {}
};


////////////////////////////
// BODY OF CLASS Material //
////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Material::Material() : reserved(std::make_unique<Ov::Material::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the material
 */
OV_API Ov::Material::Material(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<Ov::Material::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Material::Material(Material&& other) : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Material::~Material()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the given texture for the specified texturing level.
 * @param texture texture (can be Texture::empty for not used)
 * @param type texture level
 * @return TF
 */
bool OV_API Ov::Material::setTexture(const Ov::Texture& texture, Ov::Texture::Type type)
{
    reserved->texture[static_cast<uint32_t>(type)] = texture;

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the texture used at the specified texturing level.
 * @param type texture level
 * @return texture texture (can be Texture::empty for not used)
 */
const Ov::Texture OV_API& Ov::Material::getTexture(Ov::Texture::Type type) const
{
    return reserved->texture[static_cast<uint32_t>(type)];
}

#pragma endregion

#pragma region Textures

bool Ov::Material::loadTexture(const std::string& textureName, Ov::Texture::Type type, Ov::Container& container)
{
    OV_LOG_ERROR("This function must be implemented in the derived class!");
    return false;
}

#pragma endregion