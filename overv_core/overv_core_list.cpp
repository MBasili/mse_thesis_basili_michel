/**
 * @file	overv_base_list.cpp
 * @brief	Basic list of objects after the scenegraph traversal
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"


////////////
// STATIC //
////////////

// Special values:
Ov::List Ov::List::empty("[empty]");   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////


/**
 * @brief List reserved structure.
 */
struct Ov::List::Reserved
{    
   std::vector<Ov::List::RenderableElem> renderableElem;   ///< List of rendering elements
   uint32_t nrOfLights;                                     ///< Number of lights in the list (lights come first)
   bool isSkyboxPresent;

   /**
    * Constructor. 
    */
   Reserved() : nrOfLights{ 0 }, isSkyboxPresent{ false }
   {}
};

////////////////////////
// BODY OF CLASS List //
////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::List::List() : reserved(std::make_unique<Ov::List::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::List::List(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::List::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::List::List(List&& other) : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::List::~List()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of currently loaded renderable elements.
 * @return number of loaded renderable elements
 */
uint32_t OV_API Ov::List::getNrOfRenderableElems() const
{
    return (uint32_t)reserved->renderableElem.size();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the number of lights currently loaded in the list.
 * @return number of loaded lights
 */
uint32_t OV_API Ov::List::getNrOfLights() const
{
    return reserved->nrOfLights;
}

bool OV_API Ov::List::isSkyboxPresent() const
{
    return reserved->isSkyboxPresent;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get internal list of renderable elements.
 * @return list of renderable elements
 */
const std::vector<Ov::List::RenderableElem> OV_API& Ov::List::getRenderableElems() const
{
    return reserved->renderableElem;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets a reference to the specified element in the list.
 * @return element at the given position
 */
const Ov::List::RenderableElem OV_API& Ov::List::getRenderableElem(uint32_t elemNr) const
{
    return reserved->renderableElem.at(elemNr);
}

#pragma endregion

#pragma region SceneGraph

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Reset internal list.
 */
void OV_API Ov::List::reset()
{
    reserved->renderableElem.clear();
    reserved->nrOfLights = 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Recursively parse the scenegraph starting at the given node and append the parsed elements to this list.
 * @param node starting node
 * @param prevMatrix previous node matrix
 * @return TF
 */
bool OV_API Ov::List::pass(const Ov::Node& node, const glm::mat4& prevMatrix)
{
    // Safety net:
    if (node == Ov::Node::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    RenderableElem re;
    re.matrix = prevMatrix * node.getMatrix();
    re.reference = node;

    // Store renderable:
    for (auto& r : node.getListOfRenderables())
    {
        // Visible?
        if (!r.visibleFlag)
            continue;

        re.renderableData = r;

        // Lights first:
        if (dynamic_cast<Ov::Light*>(&re.renderableData.first.get())) {
            reserved->renderableElem.insert(reserved->renderableElem.begin(), 1, re);
            reserved->nrOfLights++;
        }
        else {
            reserved->renderableElem.push_back(re);

            if (typeid(re.renderableData.first.get()) == typeid(Ov::Geometry) && reserved->isSkyboxPresent == false)
            {
                Ov::Geometry& geom = dynamic_cast<Ov::Geometry&>(re.renderableData.first.get());
                // If skybox skip
                if (geom.getName().find("skybox") != std::string::npos)
                    reserved->isSkyboxPresent = true;
            }
        }
            
    }

    // Parse hiearchy recursively:
    for (auto& n : node.getListOfChildren())
        if (pass(n, re.matrix) == false)
            return false;

    // Done:
    return true;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Parse the list and call the render method of each renderable.
 * @param cameraMatrix camera (also view) matrix (must be already inverted)
 * @param projectionMatrix projection matrix
 * @param passType type of pass
 * @return number of loaded renderable elements
 */
bool OV_API Ov::List::render(const glm::mat4& cameraMatrix, const glm::mat4& projectionMatrix, Ov::List::Type passType) const
{
    for (auto& re : reserved->renderableElem)
    {
        glm::mat4 finalMatrix = cameraMatrix * re.matrix;

#if OV_DEBUG      
        // Various sanity checks:
        if (re.renderableData.first.get() == Ov::Renderable::empty)
        {
            OV_LOG_ERROR("Invalid first renderable (empty)");
            return false;
        }
        if (dynamic_cast<Ov::Geometry*>(&re.renderableData.first.get()))
        {
            // Second must be a material or empty:
            if (re.renderableData.second.get() != Ov::Renderable::empty && dynamic_cast<Ov::Material*>(&re.renderableData.second.get()) == nullptr)
            {
                OV_LOG_ERROR("Invalid second renderable (material expected)");
                return false;
            }
        }
#endif

        // Do render (first secondary, then primary):
        if (re.renderableData.second.get() != Ov::Renderable::empty)
            re.renderableData.second.get().render();

        //re.renderable.first.get().render(&finalMatrix);
        re.renderableData.first.get().render(0, &re.matrix);
    }

    // Done:
    return true;
}

#pragma endregion