/**
 * @file	overv_base_scenestats.h
 * @brief	Provides basic statistics about the given scene
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */
#pragma once


/**
 * @brief Class for processing a scenegraph and extracting some basic statistics.
 */
class OV_API SceneStats : public Ov::Object
{
//////////
public: //
//////////

    // Special values:
	static SceneStats empty;

	// Const/dest:
	SceneStats();
	SceneStats(SceneStats&& other) noexcept;
	SceneStats(SceneStats const&) = delete;
	virtual ~SceneStats();

	// Get/set:   
	const glm::vec3& getMinBBox() const;
	const glm::vec3& getMaxBBox() const;

	// Scene graph traversal:
	void reset();
	bool pass(const Ov::Node& node, const glm::mat4& prevMatrix = glm::mat4(1.0f));


/////////////
protected: //
/////////////   

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	SceneStats(const std::string& name);
};