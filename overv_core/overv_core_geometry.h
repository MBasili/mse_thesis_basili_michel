/**
 * @file	overv_core_geometry.h
 * @brief	Basic geometry properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


 /**
  * @brief Class for storing a geometric model.
  */
class OV_API Geometry : public Ov::Renderable, public Ov::Managed, public Ov::Ovo
{
//////////
public: //
//////////

    // Special values:
	static Geometry empty;


	/**
	 * @brief Per-vertex data
	 */
	struct VertexData
	{
		glm::vec3 vertex;		///< Vertex data, native
		uint32_t normal;		///< Normal, packed as 10_10_10_2
		uint32_t uv;         ///< Tex coords, packed as 2xfp16
		uint32_t tangent;	   ///< Tangent, packed as 10_10_10_2


		/**
		 * Constructor.
		 */
		inline VertexData() noexcept : vertex{ 0.0f }, normal{ 0 }, uv{ 0 }, tangent{ 0 }
		{}
	};


	/**
	* @brief Per-face data
	*/
	//__declspec(align(4)) struct ovMeshFace
	struct FaceData
	{
		uint32_t a, b, c;


		/**
		 * Constructor.
		 */
		inline FaceData() noexcept : a{ 0 }, b{ 0 }, c{ 0 }
		{}
	};


	// Const/dest/init:
	Geometry();
	Geometry(Geometry&& other);
	Geometry(Geometry const&) = delete;
	virtual ~Geometry();
	void reset();

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const;

	// Vertex operations:
	void addVertex(const VertexData& v);
	uint32_t getNrOfVertices() const;
	VertexData getVertex(uint32_t i) const;
	VertexData* getVertexDataPtr() const;

	// Face operations:
	void addFace(const FaceData& f);
	uint32_t getNrOfFaces() const;
	FaceData getFace(uint32_t i) const;
	FaceData* getFaceDataPtr() const;

	// Object-specific chunk save function:
	Ov::Node& load(const std::string& filename, Ov::Container& container) = delete;
	bool save(const Ov::Node& root, const std::string& filename) = delete;

	bool saveChunk(Ov::Serializer& serial) const;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr);

	// Sync data to device:
	virtual bool upload();

/////////////
protected: //
/////////////   

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Geometry(const std::string& name);
};