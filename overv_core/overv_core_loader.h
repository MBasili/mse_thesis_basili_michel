/**
 * @file	overv_base_loader.h
 * @brief	Base 3D model loading functionality
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


 /**
  * @brief Base 3D model loader. This class is a singleton.
  */
class OV_API Loader
{
//////////
public: //
//////////	   

    // Const/dest (static class, can't be used):
	Loader(Loader const&) = delete;
	virtual ~Loader();

	// Operators:
	void operator=(Loader const&) = delete;

	// Singleton:
	static Loader& getInstance();

	// Loading methods:
	bool load(const std::string& filename, unsigned int pFlags);
	bool loadLight(void* light, Ov::Light& ld);
	bool loadGeometry(void* mesh, Ov::Geometry& md);
	Ov::Node& loadScene(std::list<Ov::Node>& node,
		std::list<Ov::Light>& lights,
		std::list<Ov::Geometry>& geometries,
		void* current = nullptr);

/////////////
protected: //
/////////////	

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Loader();
};