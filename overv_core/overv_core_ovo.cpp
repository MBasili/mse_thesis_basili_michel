/**
 * @file	overv_base_ovo.cpp
 * @brief	Base OVO import/export functions
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


/////////////
// INCLUDE //
/////////////

// Main include:
#include "overv_core.h"
#include <functional>
#include <map>

// GLM:
#include <glm/gtc/packing.hpp>  

///////////////////////
// BODY OF CLASS Ovo //
///////////////////////

#pragma region Loadings

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads an OVO file.
 * @param filename 3D file
 * @param container use this container to store loaded data
 * @return root node or Node::empty if error
 */
Ov::Node OV_API& Ov::Ovo::load(const std::string& filename, Ov::Container& container)
{
    // Safety net:
    if (filename.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Node::empty;
    }


    /////////////////////////////////////////
    // STEP 1: load file into a memory buffer
    bool error = false;
    FILE* dat = fopen(filename.c_str(), "rb");
    if (dat == nullptr)
    {
        OV_LOG_ERROR("Unable to open file '%s'", filename.c_str());
        return Ov::Node::empty;
    }

    // Get file length (max 2 GB):
    uint64_t length;
    fseek(dat, 0L, SEEK_END);
    length = ftell(dat);
    fseek(dat, 0L, SEEK_SET);

    // Init mem and copy:
    Ov::Serializer serial(nullptr, length);
    if (fread(serial.getData(), sizeof(uint8_t), length, dat) != length)
    {
        OV_LOG_ERROR("File '%s' is corrupted", filename.c_str());
        fclose(dat);
        return Ov::Node::empty;
    }
    fclose(dat);

    // First chunk must be the format version:   
    if (loadChunk(serial) == 0)
    {
        OV_LOG_ERROR("Invalid format version or wrong file format for file '%s'", filename.c_str());
        return Ov::Node::empty;
    }


    ///////////////////////////////
    // STEP 2: Materials and geoms:
    std::map<uint32_t, std::reference_wrapper<Ov::Renderable>> renderables; // Keep track of loaded renderables

    std::function<Ov::Node& (void)> parse;
    parse = [&renderables, &serial, &container, this, &parse, &error](void)->Ov::Node&
    {
        switch (*(static_cast<uint8_t*>(serial.getDataAtCurPos())))
        {
            /////////////////////////////////////////////////////////
        case static_cast<uint8_t>(Ov::Ovo::ChunkId::material): //
        {
            OV_LOG_DEBUG("Processing material...");

            Ov::MaterialPbr* mat = static_cast<Ov::MaterialPbr*>(createByType(Ov::Ovo::ChunkId::material));
            uint32_t id = mat->loadChunk(serial, &container);
            container.add(*mat);

            renderables.insert(std::make_pair(id, std::reference_wrapper(static_cast<Ov::Renderable&>(container.getLastObject()))));
            return Ov::Node::empty;
        }
        break;

        /////////////////////////////////////////////////////////
        case static_cast<uint8_t>(Ov::Ovo::ChunkId::geometry): //
        {
            OV_LOG_DEBUG("Processing geometry...");

            Ov::Geometry* geom = static_cast<Ov::Geometry*>(createByType(Ov::Ovo::ChunkId::geometry));
            uint32_t id = geom->loadChunk(serial);
            container.add(*geom);

            renderables.insert(std::make_pair(id, std::reference_wrapper(static_cast<Ov::Renderable&>(container.getLastObject()))));
            return Ov::Node::empty;
        }
        break;

        //////////////////////////////////////////////////////
        case static_cast<uint8_t>(Ov::Ovo::ChunkId::light): //
        {
            OV_LOG_DEBUG("Processing light...");

            Ov::Light* light = static_cast<Ov::Light*>(createByType(Ov::Ovo::ChunkId::light));
            uint32_t id = light->loadChunk(serial);
            container.add(*light);

            renderables.insert(std::make_pair(id, std::reference_wrapper(static_cast<Ov::Renderable&>(container.getLastObject()))));
            return Ov::Node::empty;
        }
        break;

        /////////////////////////////////////////////////////
        case static_cast<uint8_t>(Ov::Ovo::ChunkId::node): //
        {
            OV_LOG_DEBUG("Processing node...");

            Ov::Node* node = static_cast<Ov::Node*>(createByType(Ov::Ovo::ChunkId::node));
            uint32_t nrOfChildren = node->loadChunk(serial, &renderables);
            container.add(*node);

            Ov::Node& _node = static_cast<Ov::Node&>(container.getLastObject());
            while (_node.getNrOfChildren() < nrOfChildren)
                _node.addChild(parse());
            
            return _node;
        }
        break;

        ///////////
        default: //
            OV_LOG_ERROR("Unknown chunk ID (%u) found", *(static_cast<uint8_t*>(serial.getDataAtCurPos())));
            error = true;
            return Ov::Node::empty;
        }
    };

    // Iterate:
    std::reference_wrapper<Ov::Node> root(Ov::Node::empty);
    while (serial.getDataAtCurPos() && !error)
        root = parse();

    // Done:   
    return root;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return version or 0 if error
 */
uint32_t OV_API Ov::Ovo::loadChunk(Ov::Serializer& serial, void* data)
{
    uint8_t chunkId;
    serial.deserialize(chunkId);

    if (chunkId != static_cast<uint8_t>(Ovo::ChunkId::version))
    {
        OV_LOG_ERROR("Invalid chunk ID found");
        return 0;
    }

    uint32_t chunkSize;
    serial.deserialize(chunkSize);

    uint32_t version;
    serial.deserialize(version);
    if (version != Ovo::version)
    {
        OV_LOG_ERROR("Invalid format version");
        return 0;
    }

    // Done:   
    return version;
}

#pragma endregion

#pragma region Savings

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves an OVO file.
 * @param filename 3D file
 * @return TF
 */
bool OV_API Ov::Ovo::save(const Ov::Node& root, const std::string& filename)
{
    if (filename.empty() || root == Ov::Node::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    OV_LOG_DEBUG("Writing OVO file '%s'...", filename.c_str());


    ///////////////////////////
    // STEP 1: open output file
    FILE* dat = fopen(filename.c_str(), "wb");
    if (dat == nullptr)
    {
        OV_LOG_ERROR("Unable to open file '%s' for writing", filename.c_str());
        return false;
    }

    Ov::Serializer serial;
    saveChunk(serial);


    /////////////////////////////////////////////////
    // STEP 2: iterate scene and store arrays of data 
    std::list<std::reference_wrapper<const Ov::Material>> materials;
    std::list<std::reference_wrapper<const Ov::Geometry>> geoms;
    std::list<std::reference_wrapper<const Ov::Light>> lights;
    std::list<std::reference_wrapper<const Ov::Node>> nodes;

    std::function<void(std::reference_wrapper<const Ov::Node>)> parse;
    parse = [&materials, &geoms, &lights, &nodes, &parse](std::reference_wrapper<const Ov::Node> node)->void
    {
        OV_LOG_PLAIN("Processing %s... (children: %u)", node.get().getName().c_str(), node.get().getNrOfChildren());

        // Renderables:
        for (uint32_t c = 0; c < node.get().getNrOfRenderables(); c++)
        {
            Ov::Node::RenderableData p = node.get().getRenderable(c);

            // Materials:
            Ov::Renderable* ptr = &p.second.get();
            if (dynamic_cast<Ov::Material*>(ptr))
            {
                const Ov::Material& mat = dynamic_cast<const Ov::Material&>(p.second.get());
                bool found = false;
                for (auto& m : materials)
                    if (m.get() == mat)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                    materials.push_back(mat);
            }

            // Geometries:
            ptr = &p.first.get();
            if (dynamic_cast<Ov::Geometry*>(ptr))
            {
                const Ov::Geometry& geom = dynamic_cast<const Ov::Geometry&>(p.first.get());
                bool found = false;
                for (auto& g : geoms)
                    if (g.get() == geom)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                    geoms.push_back(geom);
            }

            // Lights:         
            if (dynamic_cast<Ov::Light*>(ptr))
            {
                const Ov::Light& light = dynamic_cast<const Ov::Light&>(p.first.get());
                bool found = false;
                for (auto& l : lights)
                    if (l.get() == light)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                    lights.push_back(light);
            }
        }

        // Nodes:
        nodes.push_back(node);

        // Iterate:
        for (uint32_t c = 0; c < node.get().getNrOfChildren(); c++)
            parse(node.get().getChild(c));
    };
    OV_LOG_PLAIN("Scene graph:\n%s", root.getTreeAsString().c_str());
    parse(root);

    // Serialize chunks:
    for (auto& m : materials)
        m.get().saveChunk(serial);
    for (auto& g : geoms)
        g.get().saveChunk(serial);
    for (auto& l : lights)
        l.get().saveChunk(serial);
    for (auto& n : nodes)
        n.get().saveChunk(serial);


    ////////////////////
    // STEP 3: dump data
    if (fwrite(serial.getData(), sizeof(uint8_t), serial.getNrOfBytes(), dat) != serial.getNrOfBytes())
    {
        OV_LOG_ERROR("Unable to write data to file '%s'", filename.c_str());
        fclose(dat);
        return false;
    }

    // Done:
    fclose(dat);
    OV_LOG_DEBUG("File '%s' written", filename.c_str());

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of a given object. In its base class, this function saves the file version chunk.
 * @param serial serializer
 * @return TF
 */
bool OV_API Ov::Ovo::saveChunk(Ov::Serializer& serial) const
{
    // Write header with version ID:
    uint8_t chunkId = static_cast<uint8_t>(ChunkId::version);
    uint32_t chunkSize = sizeof(Ovo::version);

    // Store data:
    serial.serialize(chunkId);
    serial.serialize(chunkSize);
    serial.serialize(Ovo::version);

    // Done:   
    return true;
}

#pragma endregion

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Factory to create derived objects in later implementations.
 * @param type type of object required by the chunk
 * @return generic pointer to the allocated object
 */
void OV_API *Ov::Ovo::createByType(ChunkId type) const
{
   OV_LOG_ERROR("This function must be implemented in the derived class!");
   return nullptr;   
}