/**
 * @file	overv_base_bitmap.h
 * @brief	Basic generic bitmap properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 
 * @author	Mchel Basili (michel.basili@supsi.student.ch), (C) 2022
 */
#pragma once

/**
 * @brief Class for modeling a generic bitmap.
 */
class OV_API Bitmap : public Ov::Object
{
//////////
public: //
//////////

    // Special values:
	static Bitmap empty;

	/**
	 * @brief Types of bitmap format.
	 */
	enum class Format : uint32_t
	{
		none,

		// Uncompressed formats:
		r8,
		r8g8,
		r8g8b8,
		r8g8b8a8,

		// Compressed:
		r8_compressed,
		r8g8_compressed,
		r8g8b8_compressed,
		r8g8b8a8_compressed,

		// Terminator:
		last
	};

	/**
     * @brief Bitmap layer.
     */
	struct Layer
	{
		std::vector<uint8_t> data;       ///< Image raw data
		glm::u32vec2 size;               ///< Layer size


		/**
		 * Constructor.
		 */
		Layer() : size{ 0, 0 }
		{}
	};

	// Const/dest:
	Bitmap();
	Bitmap(Bitmap&& other);
	Bitmap(Bitmap const&) = delete;
	Bitmap(Format format, uint32_t sizeX, uint32_t sizeY, uint8_t* data);
	virtual ~Bitmap();

	// Get/set:
	Format getFormat() const;
	uint32_t getNrOfSides() const;
	uint32_t getNrOfLevels() const;
	uint32_t getColorDepth() const;
	uint32_t getSizeX(uint32_t level = 0, uint32_t side = 0) const;
	uint32_t getSizeY(uint32_t level = 0, uint32_t side = 0) const;
	uint8_t* getData(uint32_t level = 0, uint32_t side = 0) const;
	uint32_t getNrOfBytes(uint32_t level = 0, uint32_t side = 0) const;

	// Loaders:
	bool loadRawData(Format format, uint32_t sizeX, uint32_t sizeY, uint8_t* data);
	bool loadDDS(const std::string& filename);
	virtual bool loadKTX(const std::string& filename, Format loadFormat);

	// Savers
	bool saveAsDDS() const;
	virtual bool saveAsKTX(bool supercompress = true, bool preserveHighQuality = true) const;

/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// General:
	bool clear();

	// Get/Set:
	bool setLayers(std::vector<Layer>& layers);

	// Const/dest:
	Bitmap(const std::string& name);
};