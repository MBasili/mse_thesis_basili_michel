/**
 * @file	overv_base_program.cpp
 * @brief	Basic generic program properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Program Ov::Program::empty("[empty]");   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Program reserved structure.
 */
struct Ov::Program::Reserved
{     
   Type type;           ///< Program type   
   std::vector<std::reference_wrapper<Ov::Shader>> shader;  ///< Shaders used by the program


   /**
    * Constructor. 
    */
   Reserved() : type{Ov::Program::Type::none}
   {}
};


///////////////////////////
// BODY OF CLASS Program //
///////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Program::Program() : reserved(std::make_unique<Ov::Program::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Program::Program(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<Ov::Program::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Program::Program(Program&& other) noexcept : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Program::~Program()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get type.
 * @return program type
 */
const Ov::Program::Type OV_API Ov::Program::getType() const
{
    return reserved->type;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of shaders.
 * @return number of shaders
 */
const uint32_t OV_API Ov::Program::getNrOfShaders() const
{
    return static_cast<uint32_t>(reserved->shader.size());
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the shader at the specified position.
 * @param id shader position in the list
 * @return shader reference
 */
const Ov::Shader OV_API& Ov::Program::getShader(uint32_t id) const
{
    // Safety net:
    if (id >= reserved->shader.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Shader::empty;
    }
    return reserved->shader[id];
}

#pragma endregion

#pragma region Building

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Fill the internal series of shaders for building.
 * @param args variadic list of arguments
 * @return TF
 */
bool OV_API Ov::Program::build(std::initializer_list<std::reference_wrapper<Ov::Shader>> args)
{
    reserved->shader.clear();
    for (auto& arg : args)
    {
        if (arg.get() == Ov::Shader::empty)
        {
            OV_LOG_ERROR("Invalid params (empty shader)");
            return false;
        }
        /*if (std::find(reserved->shader.begin(), reserved->shader.end(), arg.get()) != reserved->shader.end())
        {
           OV_LOG_ERROR("Invalid params (same shader passed twice)");
           return false;
        }*/
        reserved->shader.push_back(arg);
    }

    // Done:
    return true;
}

#pragma endregion