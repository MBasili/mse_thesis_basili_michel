/**
 * @file	overv_base_timer.cpp
 * @brief   Timing utils
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// Windows:
#include <Windows.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Timer reserved structure.
 */
struct Ov::Timer::Reserved
{
   uint64_t cpuFreq;   ///< Frequency multiplier used by performance counter


   /**
    * Constructor.
    */
   Reserved() : cpuFreq{0}
   {
      if (!QueryPerformanceFrequency((LARGE_INTEGER *) &cpuFreq))    
         OV_LOG_ERROR("Performance counter not supported");         
   }
};


/////////////////////////
// BODY OF CLASS Timer //
/////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Timer::Timer() : reserved(std::make_unique<Ov::Timer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Timer::~Timer()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Sigleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 */
Ov::Timer OV_API& Ov::Timer::getInstance()
{
    static Timer instance;
    return instance;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get current time elapsed in ticks.
 * @return Time elapsed in ticks
 */
uint64_t OV_API Ov::Timer::getCounter() const
{
    // Not supported?
    if (reserved->cpuFreq == 0)
        return 0;

    uint64_t li = 0;
    QueryPerformanceCounter((LARGE_INTEGER*)&li);
    return li;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get time interval in milliseconds between two tick snapshots.
 * @return Time elapsed in milliseconds
 */
double OV_API Ov::Timer::getCounterDiff(uint64_t t1, uint64_t t2) const
{
    // Not supported?
    if (reserved->cpuFreq == 0)
        return 0.0;

    uint64_t r = ((t2 - t1) * 1000000) / reserved->cpuFreq;

    return static_cast<double>(r) / 1000.0;
}

#pragma endregion