/**
 * @file	overv_base_tracked.cpp
 * @brief	Basic generic tracked object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"


////////////
// STATIC //
////////////

// Special values:   
Ov::Tracked *Ov::Tracked::first = nullptr;
Ov::Tracked *Ov::Tracked::last = nullptr;


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Tracked reserved structure.
 */
struct Ov::Tracked::Reserved
{   
   Ov::Tracked *prev, *next;


   /**
    * Constructor.
    */
   Reserved() : prev{ nullptr }, next{ nullptr }
   {}
};


///////////////////////////
// BODY OF CLASS Tracked //
///////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Tracked::Tracked() : reserved(std::make_unique<Ov::Tracked::Reserved>())
{
    OV_LOG_DETAIL("[+]");

    // Append to current chain:     
    if (first == nullptr)
        first = this;
    else
    {
        last->reserved->next = this;
        reserved->prev = last;
    }
    last = this;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Tracked::Tracked(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Tracked::Reserved>())
{
    OV_LOG_DETAIL("[+]");

    // Append to current list:     
    if (first == nullptr)
        first = this;
    else
    {
        last->reserved->next = this;
        reserved->prev = last;
    }
    last = this;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Tracked::Tracked(Tracked&& other) : reserved(std::make_unique<Ov::Tracked::Reserved>())
{
    OV_LOG_DETAIL("[M]");

    Ov::Tracked* origPrev = reserved->prev;
    Ov::Tracked* origNext = reserved->next;

    // Substitute original:
    reserved->prev = other.reserved->prev;
    reserved->next = other.reserved->next;

    // Fix deps:
    if (reserved->prev)
        reserved->prev->reserved->next = this;
    else
        first = this;
    if (reserved->next)
        reserved->next->reserved->prev = this;
    else
        last = this;

    delete (other.reserved.release());
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Tracked::~Tracked()
{
    OV_LOG_DETAIL("[-]");

    // Remove from linked list:   
    if (reserved)
        if (reserved->prev == nullptr)
        {
            first = reserved->next;
            if (reserved->next)
                reserved->next->reserved->prev = nullptr;
            else
                last = nullptr;
        }
        else
        {
            reserved->prev->reserved->next = reserved->next;
            if (reserved->next)
                reserved->next->reserved->prev = reserved->prev;
            else
                last = reserved->prev;
        }
}

#pragma endregion

#pragma region Operators

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Operator == override.
 */
bool OV_API Ov::Tracked::operator==(const Tracked& rhs) const
{
    if (this->reserved == rhs.reserved)
        return true;
    else
        return false;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Operator != override.
 */
bool OV_API Ov::Tracked::operator!=(const Tracked& rhs) const
{
    if (this->reserved != rhs.reserved)
        return true;
    else
        return false;
}

#pragma endregion

#pragma region LinkedElements

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets pointer to first element.
 * @return pointer to first element
 */
Ov::Tracked OV_API* Ov::Tracked::getFirst()
{
    return first;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets pointer to previous element.
 * @return pointer to previous element
 */
Ov::Tracked OV_API* Ov::Tracked::getPrev() const
{
    return reserved->prev;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets pointer to next element.
 * @return pointer to previous element
 */
Ov::Tracked OV_API* Ov::Tracked::getNext() const
{
    return reserved->next;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets pointer to first element.
 * @return pointer to first element
 */
uint32_t OV_API Ov::Tracked::getNrOf()
{
    uint32_t c = 0;
    Tracked* next = first;
    while (next)
    {
        c++;
        next = next->getNext();
    }

    // Done:
    return c;
}

#pragma endregion