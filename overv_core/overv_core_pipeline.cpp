/**
 * @file	overv_base_pipeline.cpp
 * @brief	Basic generic rendering pipeline
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Pipeline Ov::Pipeline::empty("[empty]");   

// Cache:
std::reference_wrapper<Ov::Pipeline> Ov::Pipeline::cache = Ov::Pipeline::empty;


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Pipeline reserved structure.
 */
struct Ov::Pipeline::Reserved
{     
   std::reference_wrapper<Ov::Program> program;  ///< Program of the pipeline


   /**
    * Constructor. 
    */
   Reserved() : program{Ov::Program::empty}
   {}
};


////////////////////////////
// BODY OF CLASS Pipeline //
////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Pipeline::Pipeline() : reserved(std::make_unique<Ov::Pipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Pipeline::Pipeline(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Pipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Pipeline::Pipeline(Pipeline&& other) noexcept : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Pipeline::~Pipeline()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set pipeline program.
 * @param program pipeline program
 * @return TF
 */
bool OV_API Ov::Pipeline::setProgram(Ov::Program& program)
{
    reserved->program = program;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get pipeline program.
 * @return pipeline program
 */
Ov::Program OV_API& Ov::Pipeline::getProgram() const
{
    return reserved->program;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the last rendered pipeline.
 * @return last rendered pipeline
 */
Ov::Pipeline OV_API& Ov::Pipeline::getCached()
{
    return cache;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Main rendering method for the pipeline.
 * @param list list of renderables
 * @return TF
 */
bool OV_API Ov::Pipeline::render(const Ov::List& list)
{
    // Just update cache:
    Ov::Pipeline::cache = const_cast<Ov::Pipeline&>(*this);
    return true;
}

#pragma endregion