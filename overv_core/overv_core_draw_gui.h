/**
 * @file	overv_core_draw_gui.h
 * @brief	Class to draw gui using ImGui methods
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for managed the ImGUI draw.
  */
class OV_API DrawGUI
{
	//////////
public: //
//////////

	// Const/dest:
	DrawGUI();
	DrawGUI(DrawGUI&& other) noexcept;
	DrawGUI(DrawGUI const&) = delete;
	virtual ~DrawGUI();

	// Operators
	DrawGUI& operator=(const DrawGUI&) = delete;
	DrawGUI& operator=(DrawGUI&&) = delete;

	// General:
	virtual void drawGUI();

	///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	DrawGUI(const std::string& name);
};
