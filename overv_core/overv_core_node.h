/**
 * @file	overv_base_node.h
 * @brief	Basic hiearchy node properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 *          Randolf Schaerfig (randolf.schaerfig@supsi.ch)
 */
#pragma once


 /**
  * @brief Class for modelling a generic node.
  */
class OV_API Node : public Ov::Object, public Ov::Ovo
{
//////////
public: //
//////////

    // Special values:
	static Node empty;


	/**
	 * @brief Per-renderable data
	 */
	struct RenderableData
	{
		// Special values:
		static RenderableData empty;

		// Data:
		std::reference_wrapper<Ov::Renderable> first;
		std::reference_wrapper<Ov::Renderable> second;
		bool visibleFlag;


		/**
		 * Constructor.
		 */
		inline RenderableData(Ov::Renderable& _first = Ov::Renderable::empty, Ov::Renderable& _second = Ov::Renderable::empty, bool _visibleFlag = true) noexcept : first{ _first }, second{ _second }, visibleFlag{ _visibleFlag }
		{}
	};

	Node& createCopy();

	// Const/dest:
	Node();
	Node(Node&& other);
	Node(Node const&) = delete;
	virtual ~Node();

	// Operators:
	void operator=(Node const&) = delete;

	// Positioning:
	void setMatrix(const glm::mat4& matrix);
	const glm::mat4& getMatrix() const;
	glm::mat4 getWorldMatrix(Ov::Node& root = Ov::Node::empty) const;

	// Hierarchy:
	uint32_t getNrOfChildren() const;
	Node& getParent() const;
	bool addChild(Node& child);
	Node& getChild(uint32_t id) const;
	Node& removeChild(uint32_t id);

	bool removeChild(Node& node);

	const std::list<std::reference_wrapper<Ov::Node>>& getListOfChildren() const;

	// Renderables:
	uint32_t getNrOfRenderables() const;
	bool addRenderable(std::reference_wrapper<Ov::Renderable> first, std::reference_wrapper<Ov::Renderable> second = Ov::Renderable::empty, bool visibleFlag = true);
	bool addRenderableData(const RenderableData& renderableData);
	RenderableData& getRenderable(uint32_t id) const;
	RenderableData& removeRenderable(uint32_t id);
	const std::list<RenderableData>& getListOfRenderables() const;
	bool setFirstRenderable(uint32_t id, std::reference_wrapper<Ov::Renderable> renderable);
	bool setSecondRenderable(uint32_t id, std::reference_wrapper<Ov::Renderable> renderable);
	bool setVisibleFlag(bool visibleFlag, uint32_t id = std::numeric_limits<uint32_t>::max());
	bool isVisible(uint32_t id = std::numeric_limits<uint32_t>::max()) const;

	// Object-specific chunk save function:
	Ov::Node& load(const std::string& filename, Ov::Container& container) = delete;
	bool save(const Ov::Node& root, const std::string& filename) = delete;

	bool saveChunk(Ov::Serializer& serial) const;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr);

	// Debugging:
	std::string getTreeAsString() const;


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Node(const std::string& name);

	// Hierarchy:
	void setParent(Node& parent);
};