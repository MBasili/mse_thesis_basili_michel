/**
 * @file	overv_base_framebuffer.cpp
 * @brief	Basic generic framebuffer properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Framebuffer reserved structure.
 */
struct Ov::Framebuffer::Reserved
{        
   std::vector<Attachment> attachment;    ///< Array of attachments


   /**
    * Constructor. 
    */
   Reserved() 
   {}
};

/**
 * @brief Framebuffer static reserved structure.
 */
struct Ov::Framebuffer::StaticReserved
{
   glm::u32vec2 size;            ///< Current screen/window/framebuffer size


   /**
    * Constructor.
    */
   StaticReserved() : size{ 0, 0 }
   {}
};


////////////
// STATIC //
////////////

// Special values:
Ov::Framebuffer Ov::Framebuffer::empty("[empty]");
Ov::Framebuffer::Attachment Ov::Framebuffer::Attachment::empty;
Ov::Framebuffer::StaticReserved Ov::Framebuffer::staticReserved;


///////////////////////////////
// BODY OF CLASS Framebuffer //
///////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Framebuffer::Framebuffer() : reserved(std::make_unique<Ov::Framebuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Framebuffer::Framebuffer(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<Ov::Framebuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Framebuffer::Framebuffer(Framebuffer&& other) : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Framebuffer::~Framebuffer()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Empty rendering method. Bad sign if you can this.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OV_API Ov::Framebuffer::render(uint32_t value, void* data) const
{
    OV_LOG_ERROR("Empty rendering method called");
    return false;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get framebuffer width.
 * @return width
 */
uint32_t OV_API Ov::Framebuffer::getSizeX() const
{
    if (reserved->attachment.size() == 0)
        return 0;
    return reserved->attachment[0].size.x;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get framebuffer height.
 * @return height
 */
uint32_t OV_API Ov::Framebuffer::getSizeY() const
{
    if (reserved->attachment.size() == 0)
        return 0;
    return reserved->attachment[0].size.y;
}

#pragma endregion

#pragma region GlobalState

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get current screen/window/framebuffer width.
 * @return width
 */
uint32_t OV_API Ov::Framebuffer::getCurSizeX()
{
    return staticReserved.size.x;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get current screen/window/framebuffer height.
 * @return height
 */
uint32_t OV_API Ov::Framebuffer::getCurSizeY()
{
    return staticReserved.size.y;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set current screen/window/framebuffer height.
 * @param sizeX height
 */
void OV_API Ov::Framebuffer::setCurSizeX(uint32_t sizeX)
{
    staticReserved.size.x = sizeX;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set current screen/window/framebuffer width.
 * @param sizeY width
 */
void OV_API Ov::Framebuffer::setCurSizeY(uint32_t sizeY)
{
    staticReserved.size.y = sizeY;
}

#pragma endregion

#pragma region Manage

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a texture in the next slot of the framebuffer.
 * @param texture texture
 * @return TF
 */
bool OV_API Ov::Framebuffer::attachTexture(const Ov::Texture& texture, uint32_t level, uint32_t side)
{
    // Safety net:
    if (texture == Ov::Texture::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }
    if (level != 0 || side != 0)
    {
        OV_LOG_ERROR("Invalid params (to be implemented)");
        return false;
    }

    // Already attached?
    for (auto& a : reserved->attachment)
        if (a.texture.get() == texture)
        {
            OV_LOG_ERROR("Already attached");
            return false;
        }

    Attachment att;
    switch (texture.getFormat())
    {
        ////////////////////////////////////
    case Ov::Texture::Format::r8g8b8: //
    case Ov::Texture::Format::r8g8b8a8:
        att.type = Ov::Framebuffer::Attachment::Type::color_texture;
        break;

        ///////////////////////////////////
    case Ov::Texture::Format::depth: //
        att.type = Ov::Framebuffer::Attachment::Type::depth_texture;
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Invalid texture type");
        return false;
    }

    att.size = glm::u32vec2(texture.getSizeX(), texture.getSizeY());
    att.texture = texture;
    reserved->attachment.push_back(att);

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a color render buffer.
 * @param sizeX width
 * @param sizeY height
 * @return TF
 */
bool OV_API Ov::Framebuffer::attachColorBuffer(uint32_t sizeX, uint32_t sizeY)
{
    Attachment att;
    att.type = Ov::Framebuffer::Attachment::Type::color_buffer;
    att.size = glm::u32vec2(sizeX, sizeY);
    att.data = 0;
    reserved->attachment.push_back(att);

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a depth render buffer.
 * @param sizeX width
 * @param sizeY height
 * @return TF
 */
bool OV_API Ov::Framebuffer::attachDepthBuffer(uint32_t sizeX, uint32_t sizeY)
{
    Attachment att;
    att.type = Ov::Framebuffer::Attachment::Type::depth_buffer;
    att.size = glm::u32vec2(sizeX, sizeY);
    att.data = 0;
    reserved->attachment.push_back(att);

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of attachments.
 * @return number of attachments
 */
uint32_t OV_API Ov::Framebuffer::getNrOfAttachments() const
{
    return static_cast<uint32_t>(reserved->attachment.size());
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Remove the specified attachment.
 * @param id attachment id
 * @return TF
 */
bool OV_API Ov::Framebuffer::remove(uint32_t id)
{
    // Safety net:
    if (id >= reserved->attachment.size())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    reserved->attachment.erase(reserved->attachment.begin() + id);
    return true;
}

#pragma endregion

#pragma region Internal

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of attachments.
 * @return number of attachments
 */
Ov::Framebuffer::Attachment OV_API& Ov::Framebuffer::getAttachment(uint32_t id)
{
    // Safety net:
    if (id >= reserved->attachment.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Framebuffer::Attachment::empty;
    }

    return reserved->attachment[id];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of attachments (immutable).
 * @return number of attachments
 */
const Ov::Framebuffer::Attachment OV_API& Ov::Framebuffer::getAttachment(uint32_t id) const
{
    // Safety net:
    if (id >= reserved->attachment.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Framebuffer::Attachment::empty;
    }

    return reserved->attachment[id];
}

#pragma endregion