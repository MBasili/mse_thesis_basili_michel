/**
 * @file	overv_core.h
 * @brief	OverVision 3D base generic architecture
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


/////////////
// INCLUDE //
/////////////

// Base include
#include "overv_core_compatibility.h"

 /////////////
 // VERSION //
 /////////////

	// Export API:
#ifdef OV_WINDOWS
   // Specifies i/o linkage (VC++ spec):
#ifdef OVERV_EXPORTS
#define OV_API __declspec(dllexport)
#else
#define OV_API __declspec(dllimport)
#endif      
#endif
#ifdef OV_LINUX
#define OV_API       
#endif


///////////////
// NAMESPACE //
///////////////

namespace Ov {


	//////////////
	// #INCLUDE //
	//////////////   

	// Common:
	#include "overv_core_log.h"
	#include "overv_core_timer.h"
	#include "overv_core_config.h"
	#include "overv_core_serializer.h"      
	#include "overv_core_ovo.h"
	#include "overv_core_draw_gui.h"
	
	// Foundation types:
	#include "overv_core_object.h"   
	#include "overv_core_managed.h"   
	#include "overv_core_tracked.h"  
	#include "overv_core_bitmap.h"   
	#include "overv_core_renderable.h"   
	#include "overv_core_preprocessor.h"   
	
	// Derived objects:
	#include "overv_core_buffer.h"
	#include "overv_core_shader.h"
	#include "overv_core_program.h"
	#include "overv_core_texture.h"
	#include "overv_core_framebuffer.h"
	#include "overv_core_material.h"
	#include "overv_core_material_phong.h"
	#include "overv_core_material_pbr.h"
	#include "overv_core_geometry.h"   
	
	// Scene graph:
	#include "overv_core_node.h"
	#include "overv_core_list.h"
	
	// Objects:
	#include "overv_core_camera.h"
	#include "overv_core_light.h"
	#include "overv_core_container.h"
	
	// File loading:
	#include "overv_core_image.h"         
	#include "overv_core_loader.h"         
	
	// Pipelines:
	#include "overv_core_pipeline.h"
	
	// Tools:
	#include "overv_core_scenestats.h"



	/**
	 * @brief Core engine main class. This class is a singleton.
	 */
	class OV_API Core
	{
	//////////
	public: //
	//////////	   
	
		// Const/dest:
		Core(Core const&) = delete;
		virtual ~Core();

		// Operators:
		void operator=(Core const&) = delete;

		// Singleton:
		static Core& getInstance();

		// Get/set:
		Ov::Config& getConfig();

		// Init/free:
		bool init(const Ov::Config& config = Ov::Config::empty);
		bool free();


	/////////////
	protected: //
	/////////////	
	
		// Reserved:
		struct Reserved;
		std::unique_ptr<Reserved> reserved;
	
		// Const/dest:
		Core();
	};

}; // end of namespace Ov::