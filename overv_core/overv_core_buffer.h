/**
 * @file	overv_base_buffer.h
 * @brief	Basic generic buffer properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 * @author	Mchel Basili (michel.basili@supsi.student.ch), (C) 2022
 */
#pragma once


/**
 * @brief Class for modeling a generic buffer.
 */
class OV_API Buffer : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Special values:
	static Buffer empty;


	/**
	 * @brief Types of buffer format.
	 */
	enum class Format : uint32_t
	{
		none,

		// Geometry containers:
		vertex,
		face,

		// General containers:
		storage,
		uniform,
		staging,
		scratch,

		// Terminator:
		last
	};


	// Const/dest:
	Buffer();
	Buffer(Buffer&& other);
	Buffer(Buffer const&) = delete;
	virtual ~Buffer();

	// Get/set:
	Format getFormat() const;
	uint64_t getSize() const;


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Buffer(const std::string& name);

	// Get/set:
	void setSize(uint64_t size);
};



