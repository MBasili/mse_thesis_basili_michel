/**
 * @file	overv_base_scenestats.cpp
 * @brief	Basic statistics on the processed scene
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++:
#include <limits>


////////////
// STATIC //
////////////

// Special values:
Ov::SceneStats Ov::SceneStats::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief SceneStats reserved structure.
 */
struct Ov::SceneStats::Reserved
{
   glm::vec3 bboxMin;
   glm::vec3 bboxMax;
   

   /**
    * Constructor.
    */
   Reserved() : bboxMin{ std::numeric_limits<float>::max() }, bboxMax{ std::numeric_limits<float>::lowest() }
   {}
};


//////////////////////////////
// BODY OF CLASS SceneStats //
//////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::SceneStats::SceneStats() : reserved(std::make_unique<Ov::SceneStats::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::SceneStats::SceneStats(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::SceneStats::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::SceneStats::SceneStats(SceneStats&& other) noexcept : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::SceneStats::~SceneStats()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region SceneGraph

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Reset internal stats.
 */
void OV_API Ov::SceneStats::reset()
{
    reserved->bboxMin = glm::vec3(std::numeric_limits<float>::max());
    reserved->bboxMax = glm::vec3(std::numeric_limits<float>::lowest());
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Recursively parse the scenegraph starting at the given node and append the parsed elements to this list.
 * @param node starting node
 * @param prevMatrix previous node matrix
 * @return TF
 */
bool OV_API Ov::SceneStats::pass(const Ov::Node& node, const glm::mat4& prevMatrix)
{
    // Safety net:
    if (node == Ov::Node::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // World coords:
    glm::mat4 matrix = prevMatrix * node.getMatrix();

    // Store renderable:
    for (auto& r : node.getListOfRenderables())
    {
        Ov::Renderable* rPtr = &r.first.get();

        // Geometries:      
        if (dynamic_cast<Ov::Geometry*>(rPtr))
        {
            Ov::Geometry& geom = static_cast<Ov::Geometry&>(*rPtr);

            Ov::Geometry::VertexData* vd = geom.getVertexDataPtr();
            for (uint32_t c = 0; c < geom.getNrOfVertices(); c++)
            {
                glm::vec4 v = matrix * glm::vec4(vd[c].vertex, 1.0f);
                reserved->bboxMax.x = std::max(reserved->bboxMax.x, v.x);
                reserved->bboxMax.y = std::max(reserved->bboxMax.y, v.y);
                reserved->bboxMax.z = std::max(reserved->bboxMax.z, v.z);

                reserved->bboxMin.x = std::min(reserved->bboxMin.x, v.x);
                reserved->bboxMin.y = std::min(reserved->bboxMin.y, v.y);
                reserved->bboxMin.z = std::min(reserved->bboxMin.z, v.z);
            }
        }
    }

    // Parse hiearchy recursively:
    for (auto& n : node.getListOfChildren())
        if (pass(n, matrix) == false)
            return false;

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Min bounding box values.
 * @return min bounding box values
 */
const glm::vec3 OV_API& Ov::SceneStats::getMinBBox() const
{
    return reserved->bboxMin;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Max bounding box values.
 * @return max bounding box values
 */
const glm::vec3 OV_API& Ov::SceneStats::getMaxBBox() const
{
    return reserved->bboxMax;
}

#pragma endregion