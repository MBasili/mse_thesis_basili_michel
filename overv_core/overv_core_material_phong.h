/**
 * @file	overv_base_material_phong.h
 * @brief	Basic generic material with Phong properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */
#pragma once


 /**
  * @brief Class for modeling a generic Phong material.
  */
class OV_API MaterialPhong : public Ov::Material
{
//////////
public: //
//////////

   // Special values:
	static MaterialPhong empty;

	// Consts:
	static constexpr uint8_t ovoSubtype = 2;       ///< Material subtype identifier 

	// Const/dest:
	MaterialPhong();
	MaterialPhong(MaterialPhong&& other);
	MaterialPhong(MaterialPhong const&) = delete;
	virtual ~MaterialPhong();

	// Get/set:
	void setEmission(const glm::vec3& emission);
	void setAmbient(const glm::vec3& ambient);
	void setDiffuse(const glm::vec3& diffuse);
	void setSpecular(const glm::vec3& specular);
	void setOpacity(float opacity);
	void setShininess(float shininess);
	const glm::vec3& getEmission() const;
	const glm::vec3& getAmbient() const;
	const glm::vec3& getDiffuse() const;
	const glm::vec3& getSpecular() const;
	float getOpacity() const;
	float getShininess() const;

	// Object-specific chunk save function:
	bool saveChunk(Ov::Serializer& serial) const;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr);


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	MaterialPhong(const std::string& name);
};