/**
 * @file	overv_base_framebuffer.h
 * @brief	Basic generic framebuffer properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


 /**
  * @brief Class for modeling a generic framebuffer.
  */
class OV_API Framebuffer : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

	// Special values:
	static Framebuffer empty;

	/**
	 * Attachment.
	 */
	struct OV_API Attachment
	{
		// Special values:
		static Attachment empty;

		/**
		 * @brief Types of framebuffer attachments.
		 */
		enum class Type : uint32_t
		{
			none,

			// Attachments:      
			color_texture,
			depth_texture,
			color_buffer,
			depth_buffer,

			// Terminator:
			last
		};

		Type type;                    ///< Type of attachment
		glm::u32vec2 size;            ///< Attachment size
		union
		{
			std::reference_wrapper<const Ov::Texture> texture;
			uint64_t data;
		};


		/**
		 * Constructor.
		 */
		Attachment() : type{ Type::none }, size{ 0, 0 }, data{ 0 }
		{}
	};

	// Const/dest:
	Framebuffer();
	Framebuffer(Framebuffer&& other);
	Framebuffer(Framebuffer const&) = delete;
	virtual ~Framebuffer();

	// Get/set:
	uint32_t getSizeX() const;
	uint32_t getSizeY() const;

	// Manage attachments:
	virtual bool attachTexture(const Ov::Texture& texture, uint32_t level = 0, uint32_t side = 0);
	virtual bool attachColorBuffer(uint32_t sizeX, uint32_t sizeY);
	virtual bool attachDepthBuffer(uint32_t sizeX, uint32_t sizeY);
	uint32_t getNrOfAttachments() const;
	virtual bool remove(uint32_t id);

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const;

	// Global state:
	static uint32_t getCurSizeX();
	static uint32_t getCurSizeY();


/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;
	struct StaticReserved;
	static StaticReserved staticReserved;

	// Const/dest:
	Framebuffer(const std::string& name);

	// Internal:
	Attachment& getAttachment(uint32_t id);
	const Attachment& getAttachment(uint32_t id) const;

	// Global state:
	static void setCurSizeX(uint32_t sizeX);
	static void setCurSizeY(uint32_t sizeY);
};