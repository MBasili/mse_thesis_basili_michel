/**
 * @file	overv_base_container.h
 * @brief	Basic generic, centralized data container
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 *			Mchel Basili (michel.basili@supsi.student.ch), (C) 2022
 */
#pragma once


 /**
  * @brief Class for storing all the data used during the life-cycle of the engine. 
  */
class OV_API Container : public Ov::Tracked
{
//////////
public: //
//////////

   // Special values:
   static Container empty;

   // Const/dest:
   Container();
   Container(Container &&other);
   Container(Container const &) = delete;
   virtual ~Container();

   // Manager:
   virtual bool add(Ov::Object &obj);
   virtual bool remove(uint32_t id);

   // Get/set:
   Ov::Object &getLastObject() const;   
   std::vector<std::reference_wrapper<Ov::Object>> &getObjectList();   
   void *getFinalClass();  // Virtual from Ov::Tracked  
   
   // Finders:
   Ov::Object &find(const std::string &name) const;   ///< By name
   Ov::Object &find(uint32_t id) const;               ///< By ID

   // Debugging:
   void dumpList() const;


/////////////
protected: //
/////////////

   // Reserved:
   struct Reserved;
   std::unique_ptr<Reserved> reserved;

   // Const/dest:
   Container(const std::string &name);
};




