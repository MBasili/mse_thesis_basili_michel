/**
 * @file	overv_base_loader.cpp
 * @brief	Base 3D model loading functionality
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 *          Randolf Schaerfig (randolf.schaerfig@SUPSI.ch), 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// Assimp:    
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/matrix4x4.h>
#include <assimp/cimport.h>

// GLM:
#include <glm/gtc/packing.hpp>

/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Loader class reserved structure.
 */
struct Ov::Loader::Reserved
{   
   Assimp::Importer importer;
	const aiScene *modelScene;	
};


//////////////////////////
// BODY OF CLASS Loader //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Loader::Loader() : reserved(std::make_unique<Ov::Loader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Loader::~Loader()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Singleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 */
Ov::Loader OV_API& Ov::Loader::getInstance()
{
    static Loader instance;
    return instance;
}


#pragma endregion

#pragma region Loading

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load 3D scene.
 * @param filename 3D file
 * @return TF
 */
bool Ov::Loader::load(const std::string& filename, unsigned int pFlags)
{
    reserved->modelScene = reserved->importer.ReadFile(filename, pFlags);

    if (!reserved->modelScene)
    {
        OV_LOG_ERROR("Error loading file '%s': %s", filename.c_str(), reserved->importer.GetErrorString());
        return false;
    }

    // Scene stats:
    OV_LOG_PLAIN("   Nr. of meshes :  %u", reserved->modelScene->mNumMeshes);
    OV_LOG_PLAIN("   Nr. of lights :  %u", reserved->modelScene->mNumLights);

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp mesh as Ov::Geometry.
 * @param mesh pointer to aiMesh
 * @param geometry in/out geometry
 * @return TF
 */
bool OV_API Ov::Loader::loadGeometry(void* mesh, Ov::Geometry& md)
{
    // Safety net:
    if (mesh == nullptr)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }
    aiMesh* _mesh = reinterpret_cast<aiMesh*>(mesh);

    // Process vertices:
    md.reset();
    for (unsigned int v = 0; v < _mesh->mNumVertices; v++)
    {
        Ov::Geometry::VertexData vd;
        vd.vertex.x = _mesh->mVertices[v].x;
        vd.vertex.y = _mesh->mVertices[v].y;
        vd.vertex.z = _mesh->mVertices[v].z;

        if (_mesh->HasNormals())
            vd.normal = glm::packSnorm3x10_1x2(glm::vec4(_mesh->mNormals[v].x,
                _mesh->mNormals[v].y,
                _mesh->mNormals[v].z, 0.0f));

        if (_mesh->HasTextureCoords(0))
            vd.uv = glm::packHalf2x16(glm::vec2(_mesh->mTextureCoords[0][v].x, _mesh->mTextureCoords[0][v].y));

        if (_mesh->HasTangentsAndBitangents())
            vd.tangent = glm::packSnorm3x10_1x2(glm::vec4(_mesh->mTangents[v].x,
                _mesh->mTangents[v].y,
                _mesh->mTangents[v].z, 0.0f));

        // Store it:
        md.addVertex(vd);
    }

    // Process faces:
    for (unsigned int f = 0; f < _mesh->mNumFaces; f++)
    {
        Ov::Geometry::FaceData fd;

        aiFace* face = &_mesh->mFaces[f];
        fd.a = face->mIndices[0];
        fd.b = face->mIndices[1];
        fd.c = face->mIndices[2];

        // Store it:
        md.addFace(fd);
    }

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp light as Ov::Light.
 * @param light pointer to aiLight
 * @param ld in/out light
 * @return TF
 */
bool OV_API Ov::Loader::loadLight(void* light, Ov::Light& l)
{
    // Safety net:
    if (light == nullptr)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }
    aiLight* _light = reinterpret_cast<aiLight*>(light);

    // get properties
    l.setName(_light->mName.C_Str());
    l.setColor(glm::vec3(_light->mColorDiffuse.r, _light->mColorDiffuse.g, _light->mColorDiffuse.b));

    OV_LOG_PLAIN("Light  . . . :  %s", l.getName().c_str());
    OV_LOG_PLAIN("Color  . . . :  %s", glm::to_string(l.getColor()).c_str());

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load whole content starting from root node. A list is used instead of a vector because elements are not randomly reallocated.
 * @param node in/out node container
 * @param lights in/out lights container
 * @param geometries in/out geometries container
 * @param current current assimp node (starts with nullptr for root)
 * @return TF
 */
Ov::Node OV_API& Ov::Loader::loadScene(std::list<Ov::Node>& node, std::list<Ov::Light>& lights, std::list<Ov::Geometry>& geometries, void* current)
{
    // Root node?
    aiNode* _node;
    if (current == nullptr)
        _node = reserved->modelScene->mRootNode;
    else
        _node = reinterpret_cast<aiNode*>(current);

    // Setup node:
    Ov::Node n;
    n.setName(_node->mName.C_Str());

    // Set matrix:
    glm::mat4 m;
    memcpy(&m, &_node->mTransformation, sizeof(glm::mat4));
    m = glm::transpose(m); // Assimp matrices are transposed wrt OpenGL
    n.setMatrix(m);

    // Check if this node contains a light:
    bool isLight = false;
    for (uint32_t c = 0; c < reserved->modelScene->mNumLights; c++)
    {
        aiLight* _light = reserved->modelScene->mLights[c];
        if (_light->mName == _node->mName)
        {
            isLight = true;

            Ov::Light l;
            this->loadLight(_light, l);
            lights.push_back(std::move(l));
            n.addRenderable(lights.back(), Ov::Renderable::empty);
        }
    }

    // 0 mesh => Logical node, nothing to do
    if (isLight == false && _node->mNumMeshes > 0)
    {
        for (uint32_t c = 0; c < _node->mNumMeshes; c++)
        {
            // Get mesh
            aiMesh* aimesh = reserved->modelScene->mMeshes[_node->mMeshes[c]];
			Ov::Geometry g;
			if (this->loadGeometry(aimesh, g) == false)
				continue;

            geometries.push_back(std::move(g));
            n.addRenderable(geometries.back(), Ov::Renderable::empty);
        }
    }

    // Iterate through the scenegraph:
    for (unsigned int c = 0; c < _node->mNumChildren; c++)
        n.addChild(loadScene(node, lights, geometries, _node->mChildren[c]));

    // Store and done:
    node.push_back(std::move(n));
    return node.back();
}

#pragma endregion