/**
 * @file	overv_base_material_pbr.h
 * @brief	Basic generic material with Phyisically-Based Rendering (PBR) properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */
#pragma once


 /**
  * @brief Class for modeling a generic PBR material.
  */
class OV_API MaterialPbr : public Ov::Material
{
//////////
public: //
//////////

    // Special values:
	static MaterialPbr empty;

	// Consts:
	static constexpr uint8_t ovoSubtype = 1;       ///< Material subtype identifier 

	// Const/dest:
	MaterialPbr();
	MaterialPbr(MaterialPbr&& other);
	MaterialPbr(MaterialPhong const&);
	MaterialPbr(MaterialPbr const&) = delete;
	virtual ~MaterialPbr();

	// Operators:
	void operator=(MaterialPhong const&);

	// Get/set:
	void setEmission(const glm::vec3& emission);
	void setAlbedo(const glm::vec3& albedo);
	void setOpacity(float opacity);
	void setRoughness(float roughness);
	void setMetalness(float metalness);
	void setRefractionIndex(float refractionIndex);
	void setRimIntensity(float rimIntensity);
	void setAbsorptionThreshold(float absorptionThreshold);
	const glm::vec3& getEmission() const;
	const glm::vec3& getAlbedo() const;
	float getOpacity() const;
	float getRoughness() const;
	float getMetalness() const;
	float getRefractionIndex() const;
	float getRimIntensity() const;
	float getAbsorptionThreshold() const;

	// Object-specific chunk save function:
	bool saveChunk(Ov::Serializer& serial) const override;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr) override;


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	MaterialPbr(const std::string& name);
};