/**
 * @file	overv_base_image.h
 * @brief	Bitmap support extended to generic 2D image formats
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */
#pragma once


 /**
  * @brief Class for modeling a generic bitmap loaded from a 2D file.
  */
class OV_API Image : public Ov::Bitmap
{
//////////
public: //
//////////

    // Special values:
	static Image empty;

	// Const/dest:
	Image();
	Image(Image&& other) noexcept;
	Image(Image const&) = delete;
	virtual ~Image();

	// Loaders:
	bool loadWithFreeImage(const std::string& filename, bool flipHorizontal = false,
		bool flipVertical = false, bool castTo4Comp = false);

	// Savers:
	bool saveWithFreeImage(const std::string& filename, bool flipHorizontal = false, bool flipVertical = false);


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Image(const std::string& name);
};