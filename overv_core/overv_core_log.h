/**
 * @file	overv_base_log.h
 * @brief	Logging facilities
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /////////////
 // #DEFINE //
 /////////////

// Macros for logging (including method and lines):      
#define OV_LOG_ENUM(x) std::string(magic_enum::enum_name(x)).c_str() 
#define OV_LOG(kind, message, ...) Ov::Log::log(kind, __FILENAME__, __FUNCTION__, __LINE__, message, ##__VA_ARGS__) ///< More or less verbose logging command          
#define OV_LOG_ERROR(message, ...)  OV_LOG(Ov::Log::level::error, message, ##__VA_ARGS__)
#define OV_LOG_WARN(message, ...)   OV_LOG(Ov::Log::level::warning, message, ##__VA_ARGS__)
#define OV_LOG_PLAIN(message, ...)  OV_LOG(Ov::Log::level::plain, message, ##__VA_ARGS__)            
#define OV_LOG_INFO(message, ...)   OV_LOG(Ov::Log::level::info, message, ##__VA_ARGS__)
#define OV_LOG_DEBUG(message, ...)  OV_LOG(Ov::Log::level::debug, message, ##__VA_ARGS__)
#define OV_LOG_DETAIL(message, ...) OV_LOG(Ov::Log::level::detail, message, ##__VA_ARGS__)      


/**
 * @brief Logging facilities. Static components are lazy-loaded at first usage. Beware that it is not thread-safe yet.
 */
class OV_API Log
{
//////////
public: //
//////////

   // Constants:
	static constexpr uint32_t maxLength = 65536;                   ///< Maximum size of a log message
	static constexpr const char filename[] = "overv.log";          ///< Output logging filename


	/**
	 * @brief Logging levels.
	 */
	enum class level : uint32_t
	{
		none,          ///< No logging, used for levels
		error,         ///< Error message
		warning,       ///< Warning message
		plain,         ///< Clean simple output message
		info,          ///< Hint or additional information (slightly verbose)
		debug,         ///< Debugging information (very verbose)
		detail,        ///< Debugging information (extremely verbose)
		last,          ///< Terminator
	};

#ifdef OV_DEBUG
	static constexpr const level debugLvl = level::debug;    ///< Logging message level
#else
	static constexpr const level debugLvl = level::info;     ///< Logging message level
#endif

    // Log:
	static bool log(level lvl, const char* filename, const char* functionName, int32_t codeLine, const char* text, ...);

	// Parser proto:
	typedef bool(*CustomCallbackProto)(char* msg, level lvl, void* data);

	// Get/set:
	static void setCustomCallback(CustomCallbackProto cb);


///////////
private: //
///////////

    // Reserved:
	struct StaticReserved;
	static StaticReserved* staticReserved;

	// Init/free:
	static bool init();
	static bool free();
};
