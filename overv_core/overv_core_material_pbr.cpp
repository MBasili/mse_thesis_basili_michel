/**
 * @file		overv_base_material_pbr.cpp
 * @brief	Basic generic material with Pbr properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */


/////////////
// INCLUDE //
/////////////

// Main include:
#include "overv_core.h"


////////////
// STATIC //
////////////

// Special values:
Ov::MaterialPbr Ov::MaterialPbr::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief MaterialPbr reserved structure.
 */
struct Ov::MaterialPbr::Reserved
{
   // Keep these vars first and in this order...:
   glm::vec3 emission;                                   ///< Emissive term
   float opacity;                                        ///< Transparency (1 = solid, 0 = invisible)
   glm::vec3 albedo;                                     ///< Albedo color
   float roughness;                                      ///< Roughness    
   float metalness;                                      ///< Metalness
   float refractionIndex;                                ///< Index of refraction
   float rimIntensity;                                   ///< Rim lighting intensity
   float absorptionThreshold;                            ///< Padding  
   // 48 bytes

   /**
    * Constructor.
    */
   Reserved() : emission{ 0.0f },
                albedo{ 0.6f },                
                opacity{ 1.0f }, 
                roughness{ 0.5f }, metalness{ 0.01f }, refractionIndex{ 1.0f }, rimIntensity{ 0.7f },
                absorptionThreshold{ 0.0f }
   {}
};


///////////////////////////////
// BODY OF CLASS MaterialPbr //
///////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::MaterialPbr::MaterialPbr() : reserved(std::make_unique<Ov::MaterialPbr::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Copy and transform constructor from a Phong material.
 * @param phong phong material
 */
OV_API Ov::MaterialPbr::MaterialPbr(const MaterialPhong& phong) : Ov::Material()
{
    OV_LOG_DETAIL("[+] (phong2pbr)");
    setName(phong.getName() + "_as_pbr");
    reserved->emission = phong.getEmission();
    reserved->opacity = phong.getOpacity();
    reserved->albedo = phong.getDiffuse();
    reserved->metalness = 0.0f;
    reserved->refractionIndex = 1.0f;
    reserved->rimIntensity = 0.7f;
    reserved->roughness = std::max(pow(1.0f - phong.getShininess() / 128.0f, 2.0f), 0.01f);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::MaterialPbr::MaterialPbr(const std::string& name) : Ov::Material(name), reserved(std::make_unique<Ov::MaterialPbr::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::MaterialPbr::MaterialPbr(MaterialPbr&& other) : Ov::Material(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::MaterialPbr::~MaterialPbr()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Operators

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Assign and transform a Phong material.
 * @param phong phong material
 */
void OV_API Ov::MaterialPbr::operator=(const MaterialPhong& phong)
{
    OV_LOG_DETAIL("[copy] (phong2pbr)");
    setName(phong.getName() + "_as_pbr");
    reserved->emission = phong.getEmission();
    reserved->opacity = phong.getOpacity();
    reserved->albedo = phong.getDiffuse();
    reserved->metalness = 0.0f;
    reserved->roughness = std::max(pow(1.0f - phong.getShininess() / 128.0f, 2.0f), 0.01f);
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the emissive component.
 * @param emission emissive term
 */
void OV_API Ov::MaterialPbr::setEmission(const glm::vec3& emission)
{
    reserved->emission = emission;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the albedo component.
 * @param albedo term
 */
void OV_API Ov::MaterialPbr::setAlbedo(const glm::vec3& albedo)
{
    reserved->albedo = albedo;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the roughness level.
 * @param roughness roughness level
 */
void OV_API Ov::MaterialPbr::setRoughness(float roughness)
{
    reserved->roughness = roughness;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the metalness level.
 * @param metalness metalness level
 */
void OV_API Ov::MaterialPbr::setMetalness(float metalness)
{
    reserved->metalness = metalness;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the index of refraction (IOR).
 * @param refractionIndex index of refraction
 */
void OV_API Ov::MaterialPbr::setRefractionIndex(float refractionIndex)
{
    reserved->refractionIndex = refractionIndex;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the rim lighting intensity.
 * @param rimIntensity rim lighting intensity
 */
void OV_API Ov::MaterialPbr::setRimIntensity(float rimIntensity)
{
    reserved->rimIntensity = rimIntensity;
    setDirty(true);
}

void OV_API Ov::MaterialPbr::setAbsorptionThreshold(float absorptionThreshold)
{
    reserved->absorptionThreshold = absorptionThreshold;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the emissive component.
 * @return emissive term
 */
const glm::vec3 OV_API& Ov::MaterialPbr::getEmission() const
{
    return reserved->emission;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the albedo component.
 * @return albedo term
 */
const glm::vec3 OV_API& Ov::MaterialPbr::getAlbedo() const
{
    return reserved->albedo;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the roughness level.
 * @return roughness level
 */
float OV_API Ov::MaterialPbr::getRoughness() const
{
    return reserved->roughness;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the metalness level.
 * @return metalness level
 */
float OV_API Ov::MaterialPbr::getMetalness() const
{
    return reserved->metalness;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the index of refraction.
 * @return index of refraction
 */
float OV_API Ov::MaterialPbr::getRefractionIndex() const
{
    return reserved->refractionIndex;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the rim lighting intensity.
 * @return rim lighting intensity
 */
float OV_API Ov::MaterialPbr::getRimIntensity() const
{
    return reserved->rimIntensity;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the opacity of the material (0 = invisibile, 1 = solid).
 * @param opacity opacity level
 */
void OV_API Ov::MaterialPbr::setOpacity(float opacity)
{
    reserved->opacity = opacity;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the opacity level of the material.
 * @return opacity level
 */
float OV_API Ov::MaterialPbr::getOpacity() const
{
    return reserved->opacity;
}

float OV_API Ov::MaterialPbr::getAbsorptionThreshold() const {

    return reserved->absorptionThreshold;
}

#pragma endregion

#pragma region Ovo

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of this material into the serializer.
 * @param serial serializer
 * @return TF
 */
bool OV_API Ov::MaterialPbr::saveChunk(Ov::Serializer& serial) const
{
    OV_LOG_DEBUG("Serializing material...");

    // Chunk header:
    uint8_t chunkId = static_cast<uint8_t>(ChunkId::material);
    uint32_t chunkSize = sizeof(uint32_t);
    chunkSize += static_cast<uint32_t>(sizeof(ovoSubtype));
    chunkSize += static_cast<uint32_t>(strlen(this->getName().c_str())) + 1;
    chunkSize += sizeof(glm::vec3) * 2 + sizeof(float) * 6;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::albedo).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::normal).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::height).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::roughness).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::metalness).getName().c_str())) + 1;

    serial.serialize(&chunkId, sizeof(uint8_t));
    serial.serialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    serial.serialize(ovoSubtype);
    serial.serialize(this->getId());
    serial.serialize(this->getName());

    serial.serialize(reserved->emission);
    serial.serialize(reserved->albedo);

    serial.serialize(reserved->opacity);
    serial.serialize(reserved->roughness);
    serial.serialize(reserved->metalness);
    serial.serialize(reserved->refractionIndex);
    serial.serialize(reserved->rimIntensity);
    serial.serialize(reserved->absorptionThreshold);

    // Textures:   
    serial.serialize(this->getTexture(Ov::Texture::Type::albedo).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::normal).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::height).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::roughness).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::metalness).getName());


    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return OVO-internal ID of the renderable, or 0 if error
 */
uint32_t OV_API Ov::MaterialPbr::loadChunk(Ov::Serializer& serial, void* data)
{
    // Chunk header:
    uint8_t chunkId;
    serial.deserialize(&chunkId, sizeof(uint8_t));
    if (chunkId != static_cast<uint8_t>(ChunkId::material))
    {
        OV_LOG_ERROR("Invalid chunk ID found");
        return 0;
    }

    uint32_t chunkSize;
    serial.deserialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    uint8_t subtype;
    serial.deserialize(subtype);
    if (subtype != ovoSubtype)
    {
        OV_LOG_ERROR("Invalid subtype for this material");
        return 0;
    }

    uint32_t id;
    serial.deserialize(id);
    std::string name;
    serial.deserialize(name);
    this->setName(name);
    serial.deserialize(reserved->emission);
    serial.deserialize(reserved->albedo);
    serial.deserialize(reserved->opacity);
    serial.deserialize(reserved->roughness);
    serial.deserialize(reserved->metalness);
    serial.deserialize(reserved->refractionIndex);
    serial.deserialize(reserved->rimIntensity);
    serial.deserialize(reserved->absorptionThreshold);

    // Textures:
    Ov::Container* container = static_cast<Ov::Container*>(data);

    serial.deserialize(name);
    if (name.size() > 0 && name != "[empty]")
        if (loadTexture(name, Ov::Texture::Type::albedo, *container) == false)
            OV_LOG_ERROR("Fail to load albedo texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (name.size() > 0 && name != "[empty]")
        if (loadTexture(name, Ov::Texture::Type::normal, *container) == false)
            OV_LOG_ERROR("Fail to load normal texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (name.size() > 0 && name != "[empty]")
        if (loadTexture(name, Ov::Texture::Type::height, *container) == false)
         OV_LOG_ERROR("Fail to load height texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (name.size() > 0 && name != "[empty]")
        if (loadTexture(name, Ov::Texture::Type::roughness, *container) == false)
            OV_LOG_ERROR("Fail to load roughness texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (name.size() > 0 && name != "[empty]")
        if (loadTexture(name, Ov::Texture::Type::metalness, *container) == false)
            OV_LOG_ERROR("Fail to load metalness texture for the material with id= %d", getId());

    // Done:   
    return id;
}

#pragma endregion