/**
 * @file	overv_base_texture.cpp
 * @brief	Basic generic texture properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Texture Ov::Texture::empty("[empty]");   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Texture reserved structure.
 */
struct Ov::Texture::Reserved
{ 
   std::reference_wrapper<const Ov::Bitmap> bitmap;
   Ov::Texture::Format format;
   glm::u32vec3 size;


   /**
    * Constructor. 
    */
   Reserved() : bitmap{ Ov::Bitmap::empty }, format{ Ov::Texture::Format::none }, size{ 0, 0, 1 }
   {}
};


///////////////////////////
// BODY OF CLASS Texture //
///////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Texture::Texture() : reserved(std::make_unique<Ov::Texture::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Texture::Texture(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<Ov::Texture::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Texture::Texture(Texture&& other) noexcept : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)),
reserved(std::move(other.reserved)) 
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Texture::~Texture()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get texture bitmap.
 * @return texture bitmap
 */
const Ov::Bitmap OV_API& Ov::Texture::getBitmap() const
{
    return reserved->bitmap.get();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set texture bitmap.
 * @param bitmap bitmap
 */
void OV_API Ov::Texture::setBitmap(const Ov::Bitmap& bitmap)
{
    reserved->bitmap = bitmap;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get texture format.
 * @return texture format
 */
Ov::Texture::Format OV_API Ov::Texture::getFormat() const
{
    return reserved->format;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set texture format.
 * @param format texture format
 */
void OV_API Ov::Texture::setFormat(Ov::Texture::Format format)
{
    reserved->format = format;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get texture size X.
 * @return texture size X
 */
uint32_t OV_API Ov::Texture::getSizeX() const
{
    return reserved->size.x;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set texture size X.
 * @param sizeX texture width
 */
void OV_API Ov::Texture::setSizeX(uint32_t sizeX)
{
    reserved->size.x = sizeX;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get texture size Y.
 * @return texture size Y
 */
uint32_t OV_API Ov::Texture::getSizeY() const
{
    return reserved->size.y;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set texture size Y.
 * @param sizeY texture height
 */
void OV_API Ov::Texture::setSizeY(uint32_t sizeY)
{
    reserved->size.y = sizeY;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get texture size Z.
 * @return texture size Z
 */
uint32_t OV_API Ov::Texture::getSizeZ() const
{
    return reserved->size.z;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set texture size Z.
 * @param sizeX texture depth
 */
void OV_API Ov::Texture::setSizeZ(uint32_t sizeZ)
{
    reserved->size.z = sizeZ;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Empty rendering method. Bad sign if you can this.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OV_API Ov::Texture::render(uint32_t value, void* data) const
{
    OV_LOG_ERROR("Empty rendering method called");
    return false;
}

#pragma endregion