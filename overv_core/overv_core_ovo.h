/**
 * @file	overv_base_ovo.h
 * @brief	Base OVO import/export functions
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 ///////////////
 // PREDEFINE //
 ///////////////

// Several predefines are required, as many later classes will derive from this one:
class Node;
class MaterialPhong;
class MaterialPbr;
class Geometry;
class Container;

/**
 * @brief Base 3D OVO manager.
 */
class OV_API Ovo
{
//////////
public: //
//////////	   

   /**
	* @brief Chunk IDs.
	*/
	enum class ChunkId : uint8_t
	{
		none = 0,
		version = 1,
		material = 2,
		node = 3,
		geometry = 4,
		light = 5,
		camera = 6,

		// Terminator:
		last
	};

	// Consts:
	static constexpr uint32_t version = 9;       ///< OVO format revision (divide by 10)   

	// Loading methods:
	Ov::Node& load(const std::string& filename, Ov::Container& container);
	virtual uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr);

	// Saving methods:
	bool save(const Ov::Node& root, const std::string& filename);
	virtual bool saveChunk(Ov::Serializer& serial) const;

/////////////
protected: //
/////////////	

    // Derived-type specific factories:
	virtual void* createByType(ChunkId type) const;
};