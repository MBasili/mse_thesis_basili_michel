/**
 * @file	overv_base_list.h
 * @brief	Basic list of objects after the scenegraph traversal
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


 /**
  * @brief Class for storing list of objects after the scenegraph traversal.
  */
class OV_API List : public Ov::Object
{
//////////
public: //
//////////

    // Special values:
	static List empty;


	/**
	* @brief Types of passes.
	*/
	enum class Type : uint32_t
	{
		none = 0,
		lights = 1,
		geoms = 2,
		clouds = 3,
		all,
	};


	/**
	 * @brief Renderable element
	 */
	struct RenderableElem
	{
		Ov::Node::RenderableData renderableData;              ///< Renderable tuple      
		glm::mat4 matrix;                                     ///< Final position matrix
		std::reference_wrapper<const Ov::Node> reference;     ///< Reference to the original node


		/**
		 * Constructor.
		 */
		RenderableElem() : reference{ Ov::Node::empty }
		{}
	};


	// Const/dest:
	List();
	List(List&& other);
	List(List const&) = delete;
	virtual ~List();

	// Get/set:
	uint32_t getNrOfRenderableElems() const;
	uint32_t getNrOfLights() const;
	bool isSkyboxPresent() const;
	const std::vector<Ov::List::RenderableElem>& getRenderableElems() const;
	const Ov::List::RenderableElem& getRenderableElem(uint32_t elemNr) const;

	// Scene graph traversal:
	void reset();
	bool pass(const Ov::Node& node, const glm::mat4& prevMatrix = glm::mat4(1.0f));

	// Rendering:
	virtual bool render(const glm::mat4& cameraMatrix, const glm::mat4& projectionMatrix, Type passType = Type::all) const;


/////////////
protected: //
/////////////   

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	List(const std::string& name);
};