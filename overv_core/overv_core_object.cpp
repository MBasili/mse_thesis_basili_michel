/**
 * @file	overv_base_object.cpp
 * @brief	Basic generic object properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Object Ov::Object::empty("[empty]");   

// Parity check and counters:
static std::atomic<int32_t> counter = 0;    
static std::atomic<uint32_t> idCounter = 0;

// Const/dest callbacks:
static Ov::Object::ConstCallback constCallback = nullptr;
static Ov::Object::DestCallback destCallback = nullptr;


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Object reserved structure.
 */
struct Ov::Object::Reserved
{  
   // General:
   std::string name;                         ///< Name
   uint32_t id;                              ///< UID
   bool dirty;                               ///< Object needs update  

   // Custom data:
   void *customData;                         ///< A generic pointer to whatever, just in case


   /**
    * Constructor. 
    */
   Reserved() : name{ "[none]" }, id{ idCounter++ }, dirty { true },
                customData{ nullptr }
   {
      counter++;
   }

   /**
    * Destructor. 
    */
   ~Reserved()
   {
      counter--;
   }
};


//////////////////////////
// BODY OF CLASS Object //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Object::Object() : reserved(std::make_unique<Ov::Object::Reserved>())
{
    OV_LOG_DETAIL("[+]");

    // Custom callback:
    if (constCallback)
        constCallback(this);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Object::Object(const std::string& name) : reserved(std::make_unique<Ov::Object::Reserved>()) // : Object() // To avoid the custom callback to be invoked twice
{
    OV_LOG_DETAIL("[+]");

    reserved->name = name;

    // Custom callback:
    if (constCallback)
        constCallback(this);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Object::Object(Object&& other) noexcept : reserved(std::move(other.reserved)) {
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Object::~Object()
{
    OV_LOG_DETAIL("[-]");

    // Custom callback:
    if (destCallback)
        destCallback(this);
}

#pragma endregion

#pragma region Operators

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Operator == override.
 */
bool OV_API Ov::Object::operator==(const Object& rhs) const
{
    if (this->reserved == rhs.reserved)
        return true;
    else
        return false;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Operator != override.
 */
bool OV_API Ov::Object::operator!=(const Object& rhs) const
{
    if (this->reserved != rhs.reserved)
        return true;
    else
        return false;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set node name.
 * @param name node name
 */
void OV_API Ov::Object::setName(const std::string& name)
{
    // Safety net:
    if (name.empty() || name == "[none]" || name == "[empty]") // Some names are reserved
    {
        OV_LOG_ERROR("Invalid params");
        return;
    }

    // Done:
    reserved->name = name;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get node name.
 * @return node name
 */
const std::string OV_API& Ov::Object::getName() const
{
    return reserved->name;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get object UID.
 * @return UID
 */
uint32_t OV_API Ov::Object::getId() const
{
    return reserved->id;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the dirty flag.
 * @param dirty dirty flag
 */
void OV_API Ov::Object::setDirty(bool dirty) const
{
    // First time?   
    reserved->dirty = dirty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return true if dirty.
 * @return true if dirty
 */
bool OV_API Ov::Object::isDirty() const
{
    return reserved->dirty;
}

#pragma endregion

#pragma region CustomData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set custom data pointer.
 * @param data pointer to custom data
 */
void OV_API Ov::Object::setCustomData(void* data)
{
    // Done:
    reserved->customData = data;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get custom data pointer.
 * @return pointer to custom data
 */
void OV_API* Ov::Object::getCustomData() const
{
    // Done:
    return reserved->customData;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set custom callback that will be invoked at the end of the Object constructor.
 * @param cb callback function
 */
bool OV_API Ov::Object::setConstCallback(Ov::Object::ConstCallback cb)
{
    constCallback = cb;

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set custom callback that will be invoked at the beginning of the Object destructor.
 * @param cb callback function
 */
bool OV_API Ov::Object::setDestCallback(Ov::Object::DestCallback cb)
{
    destCallback = cb;

    // Done:
    return true;
}

#pragma endregion

#pragma region Statistics

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of currently allocated objects. This counter shall be zero in the end.
 * @return number of allocated objects
 */
int32_t OV_API Ov::Object::getNrOfObjects()
{
    return counter;
}

#pragma endregion