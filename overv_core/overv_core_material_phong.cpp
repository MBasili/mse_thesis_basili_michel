/**
 * @file	overv_base_material_phong.cpp
 * @brief	Basic generic material with Phong properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"


////////////
// STATIC //
////////////

// Special values:
Ov::MaterialPhong Ov::MaterialPhong::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief MaterialPhong reserved structure.
 */
struct Ov::MaterialPhong::Reserved
{
   // Keep these vars first and in this order...:
   glm::vec3 emission;                                   ///< Emissive term
   float opacity;                                        ///< Transparency (1 = solid, 0 = invisible)
   glm::vec3 ambient;                                    ///< Ambient term
   float shininess;                                      ///< Shininess
   glm::vec3 diffuse;                                    ///< Diffuse term
   float _pad0;                                          ///< Padding
   glm::vec3 specular;                                   ///< Specular term
   float _pad1;                                          ///< Padding
   // ...64 bytes


   /**
    * Constructor.
    */
   Reserved() : emission { 0.0f },
                ambient{ 0.2f },
                diffuse{ 0.6f },
                specular{ 0.2f },
                opacity{ 1.0f }, shininess{ 70.0f }
   {}
};


/////////////////////////////////
// BODY OF CLASS MaterialPhong //
/////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::MaterialPhong::MaterialPhong() : reserved(std::make_unique<Ov::MaterialPhong::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::MaterialPhong::MaterialPhong(const std::string& name) : Ov::Material(name), reserved(std::make_unique<Ov::MaterialPhong::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::MaterialPhong::MaterialPhong(MaterialPhong&& other) : Ov::Material(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::MaterialPhong::~MaterialPhong()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the emissive component.
 * @param emission emissive term
 */
void OV_API Ov::MaterialPhong::setEmission(const glm::vec3& emission)
{
    reserved->emission = emission;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the ambient component.
 * @param ambient ambient term
 */
void OV_API Ov::MaterialPhong::setAmbient(const glm::vec3& ambient)
{
    reserved->ambient = ambient;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the diffuse component.
 * @param diffuse diffuse term
 */
void OV_API Ov::MaterialPhong::setDiffuse(const glm::vec3& diffuse)
{
    reserved->diffuse = diffuse;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the specular component.
 * @param specular specular term
 */
void OV_API Ov::MaterialPhong::setSpecular(const glm::vec3& specular)
{
    reserved->specular = specular;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the emissive component.
 * @return emissive term
 */
const glm::vec3 OV_API& Ov::MaterialPhong::getEmission() const
{
    return reserved->emission;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the ambient component.
 * @return ambient term
 */
const glm::vec3 OV_API& Ov::MaterialPhong::getAmbient() const
{
    return reserved->ambient;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the diffuse component.
 * @return diffuse term
 */
const glm::vec3 OV_API& Ov::MaterialPhong::getDiffuse() const
{
    return reserved->diffuse;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the specular component.
 * @return specular term
 */
const glm::vec3 OV_API& Ov::MaterialPhong::getSpecular() const
{
    return reserved->specular;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the opacity of the material (0 = invisibile, 1 = solid).
 * @param opacity opacity level
 */
void OV_API Ov::MaterialPhong::setOpacity(float opacity)
{
    reserved->opacity = opacity;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the opacity level of the material.
 * @return opacity level
 */
float OV_API Ov::MaterialPhong::getOpacity() const
{
    return reserved->opacity;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the shininess of the material.
 * @param shininess shininess level
 */
void OV_API Ov::MaterialPhong::setShininess(float shininess)
{
    reserved->shininess = shininess;
    setDirty(true);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the shininess level of the material.
 * @return shininess level
 */
float OV_API Ov::MaterialPhong::getShininess() const
{
    return reserved->shininess;
}

#pragma endregion

#pragma region Ovo

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of this material into the serializer.
 * @param serial serializer
 * @return TF
 */
bool OV_API Ov::MaterialPhong::saveChunk(Ov::Serializer& serial) const
{
    OV_LOG_DEBUG("Serializing material...");

    // Chunk header:
    uint8_t chunkId = static_cast<uint8_t>(ChunkId::material);
    uint32_t chunkSize = sizeof(uint32_t);
    chunkSize += static_cast<uint32_t>(sizeof(ovoSubtype));
    chunkSize += static_cast<uint32_t>(strlen(this->getName().c_str())) + 1;
    chunkSize += sizeof(glm::vec3) * 4 + sizeof(float) * 2;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::diffuse).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::normal).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::roughness).getName().c_str())) + 1;
    chunkSize += static_cast<uint32_t>(strlen(this->getTexture(Ov::Texture::Type::metalness).getName().c_str())) + 1;

    serial.serialize(&chunkId, sizeof(uint8_t));
    serial.serialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    serial.serialize(ovoSubtype);
    serial.serialize(this->getId());
    serial.serialize(this->getName());
    serial.serialize(reserved->emission);
    serial.serialize(reserved->ambient);
    serial.serialize(reserved->diffuse);
    serial.serialize(reserved->specular);
    serial.serialize(reserved->opacity);
    serial.serialize(reserved->shininess);

    // Textures:   
    serial.serialize(this->getTexture(Ov::Texture::Type::diffuse).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::normal).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::roughness).getName());
    serial.serialize(this->getTexture(Ov::Texture::Type::metalness).getName());

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return OVO-internal ID of the renderable, or 0 if error
 */
uint32_t OV_API Ov::MaterialPhong::loadChunk(Ov::Serializer& serial, void* data)
{
    // Chunk header:
    uint8_t chunkId;
    serial.deserialize(&chunkId, sizeof(uint8_t));
    if (chunkId != static_cast<uint8_t>(Ovo::ChunkId::material))
    {
        OV_LOG_ERROR("Invalid chunk ID found");
        return 0;
    }

    uint32_t chunkSize;
    serial.deserialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    uint8_t subtype;
    serial.deserialize(subtype);
    if (subtype != ovoSubtype)
    {
        OV_LOG_ERROR("Invalid subtype for this material");
        return 0;
    }

    uint32_t id;
    serial.deserialize(id);
    std::string name;
    serial.deserialize(name);
    this->setName(name);
    serial.deserialize(reserved->emission);
    serial.deserialize(reserved->ambient);
    serial.deserialize(reserved->diffuse);
    serial.deserialize(reserved->specular);
    serial.deserialize(reserved->opacity);
    serial.deserialize(reserved->shininess);

    // Textures:
    Ov::Container* container = static_cast<Ov::Container*>(data);

    serial.deserialize(name);
    if (loadTexture(name, Ov::Texture::Type::diffuse, *container) == false)
        OV_LOG_ERROR("Fail to load diffuse texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (loadTexture(name, Ov::Texture::Type::normal, *container) == false)
        OV_LOG_ERROR("Fail to load normal texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (loadTexture(name, Ov::Texture::Type::roughness, *container) == false)
        OV_LOG_ERROR("Fail to load roughness texture for the material with id= %d", getId());

    serial.deserialize(name);
    if (loadTexture(name, Ov::Texture::Type::metalness, *container) == false)
        OV_LOG_ERROR("Fail to load metalness texture for the material with id= %d", getId());

    // Done:   
    return id;
}

#pragma endregion