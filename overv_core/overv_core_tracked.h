/**
 * @file	overv_base_tracked.h
 * @brief	Basic generic tracked object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief Class for keeping track of all the created class instances.
  */
class OV_API Tracked : public Ov::Object
{
//////////
public: //
//////////   

   // Const/dest:
	Tracked();
	Tracked(Tracked&& other);
	Tracked(Tracked const&) = delete;
	virtual ~Tracked();

	// Operators:
	void operator=(Tracked const&) = delete;
	bool operator==(const Tracked& rhs) const;
	bool operator!=(const Tracked& rhs) const;

	// Linked elements:
	static Tracked* getFirst();
	Tracked* getPrev() const;
	Tracked* getNext() const;

	// Get/set:
	virtual void* getFinalClass() = 0;
	static uint32_t getNrOf();


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;
	static Tracked* first;
	static Tracked* last;

	// Const/dest:
	Tracked(const std::string& name);
};