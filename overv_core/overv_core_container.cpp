/**
 * @file	overv_base_container.cpp
 * @brief	Basic generic, centralized data container
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 *      	Mchel Basili (michel.basili@supsi.student.ch), 2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++:
#include <algorithm>   


////////////
// STATIC //
////////////

// Special values:
Ov::Container Ov::Container::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Container reserved structure.
 */
struct Ov::Container::Reserved
{
   std::vector<std::reference_wrapper<Ov::Object>> allObjs;  
  

   /**
    * Constructor.
    */
   Reserved() 
   {}
};


/////////////////////////////
// BODY OF CLASS Container //
/////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Container::Container() : reserved(std::make_unique<Ov::Container::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Container::Container(const std::string& name) : Ov::Tracked(name), reserved(std::make_unique<Ov::Container::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Container::Container(Container&& other) : Ov::Tracked(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Container::~Container()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Managers

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds the given object to the proper container.
 * @return TF
 */
bool OV_API Ov::Container::add(Ov::Object& obj)
{
    // Safety net:
    if (obj == Ov::Object::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Store a reference:
    reserved->allObjs.push_back(obj);

    // Done:   
    setDirty(true);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Removes the given object to the proper container.
 * @return TF
 */
bool OV_API Ov::Container::remove(uint32_t id)
{
    // Safety net:
    if (id == Ov::Object::empty.getId())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Remove element from vector. This is generally an inefficient operation
    std::vector<std::reference_wrapper<Ov::Object>>::iterator it;
    for (it = reserved->allObjs.begin(); it != reserved->allObjs.end(); it++)
    {
        if (it->get().getId() == id)
        {
            reserved->allObjs.erase(it);
            setDirty(true);
            return true;
        }
    }

    // Done:   
    return false;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added object.
 * @return last added object
 */
Ov::Object OV_API& Ov::Container::getLastObject() const
{
    // Safety net:
    if (reserved->allObjs.empty())
        return Ov::Object::empty;
    return reserved->allObjs.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of objects.
 * @return list of objects
 */
std::vector<std::reference_wrapper<Ov::Object>> OV_API& Ov::Container::getObjectList()
{
    return reserved->allObjs;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a raw pointer to the final class (to be casted accordingly).
 * @return pointer to this
 */
void OV_API* Ov::Container::getFinalClass()
{
    // Done:   
    return this;
}

#pragma endregion

#pragma region Finders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns, if existing, the first object with the given name among its various lists.
 * @param name object name
 * @return found object or empty
 */
Ov::Object OV_API& Ov::Container::find(const std::string& name) const
{
    // Safety net:
    if (name.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Object::empty;
    }

    // Seach:
    for (auto& c : reserved->allObjs)
        if (c.get().getName() == name)
            return c;

    // Not found:
    return Ov::Object::empty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns, if existing, the first object with the given ID among its various lists.
 * @param id object id
 * @return found object or empty
 */
Ov::Object OV_API& Ov::Container::find(const uint32_t id) const
{
    // Fast lane:
    if (id == 0)
        return Ov::Object::empty;

    // Seach:
    for (auto& c : reserved->allObjs)
        if (c.get().getId() == id)
            return c;

    // Not found:
    return Ov::Object::empty;
}

#pragma endregion

#pragma region Debugging

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Dumps current content.
 */
void OV_API Ov::Container::dumpList() const
{
    OV_LOG_PLAIN("%llu loaded object(s)", reserved->allObjs.size());

    // Seach:
    for (auto& c : reserved->allObjs)
    {
        OV_LOG_PLAIN("  ID: %u, name: %s", c.get().getId(), c.get().getName().c_str());
    }
}

#pragma endregion