/**
 * @file	overv_base_image.cpp
 * @brief	Bitmap support extended to generic 2D image formats
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 *      	Mchel Basili (michel.basili@supsi.student.ch), 2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// FreeImage:
#include <FreeImage.h>
#ifndef FREEIMAGE_LIB
#define FREEIMAGE_LIB
#endif
#include <Utilities.h>


////////////
// STATIC //
////////////

// Special values:
Ov::Image Ov::Image::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Image reserved structure.
 */
struct Ov::Image::Reserved
{
   // Just a placeholder for now...

   /**
    * Constructor.
    */
   Reserved() 
   {}
};


/////////////////////////
// BODY OF CLASS Image //
/////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Image::Image() : reserved(std::make_unique<Ov::Image::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Image::Image(const std::string& name) : Ov::Bitmap(name), reserved(std::make_unique<Ov::Image::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Image::Image(Image&& other) noexcept: Ov::Bitmap(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Image::~Image()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Loders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads image from an arbitrary file with FreeImage library.
 * @param filename 2D file name
 * @param flipHorizontal flips the image horizontally when true
 * @param flipVertical flips the image vertically when true
 * @return TF
 */
bool OV_API Ov::Image::loadWithFreeImage(const std::string& filename, bool flipHorizontal, bool flipVertical, bool castTo4Comp)
{
    // Safety net:
    if (filename.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Load texture:
    FIBITMAP* fBitmap = FreeImage_Load(FreeImage_GetFileType(filename.c_str(), 0), filename.c_str());
    if (fBitmap == nullptr)
    {
        OV_LOG_ERROR("Unable to load file '%s'", filename.c_str());
        return false;
    }

    // Flipping:
    if (flipHorizontal)
        FreeImage_FlipHorizontal(fBitmap);
    if (flipVertical)
        FreeImage_FlipVertical(fBitmap);

    // Pixel type:   
    Format format = Format::none;
    uint32_t numberOfBit = FreeImage_GetBPP(fBitmap);
    switch (numberOfBit)
    {
        ///////////
    case 8: // RGB
        format = Format::r8;
        break;

        ///////////
    case 16: // RGB
        format = Format::r8g8;
        break;

        ///////////
    case 24: // RGB
        format = Format::r8g8b8;
        break;

        ///////////
    case 32: // RGBA
        format = Format::r8g8b8a8;
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Unsupported pixel format in file '%s'", filename.c_str());
        FreeImage_Unload(fBitmap);
        return false;
    }

    if (numberOfBit == 24 || numberOfBit == 32) {
        // BGR->RGB:
        FIBITMAP* redChannel = FreeImage_GetChannel(fBitmap, FICC_RED);
        FIBITMAP* blueChannel = FreeImage_GetChannel(fBitmap, FICC_BLUE);

        if (!FreeImage_SetChannel(fBitmap, blueChannel, FICC_RED) || !FreeImage_SetChannel(fBitmap, redChannel, FICC_BLUE))
        {
            OV_LOG_ERROR("Unable to swap red/blue channels for file '%s'", filename.c_str());
            FreeImage_Unload(fBitmap);
            return false;
        }
    }

    if (castTo4Comp && format != Format::r8g8b8a8 && format == Format::r8g8b8) {
        format = Format::r8g8b8a8;
        FIBITMAP* fBitmapProv = FreeImage_ConvertTo32Bits(fBitmap);
        FreeImage_Unload(fBitmap);
        fBitmap = fBitmapProv;
    }

    // Load via super-method:
    if (this->Ov::Bitmap::loadRawData(format, FreeImage_GetWidth(fBitmap), FreeImage_GetHeight(fBitmap), FreeImage_GetBits(fBitmap)) == false)
    {
        OV_LOG_ERROR("Unable to create bitmap from file '%s'", filename.c_str());
        FreeImage_Unload(fBitmap);
        return false;
    }

    // Release unused resources:
    FreeImage_Unload(fBitmap);

    // Done:
    this->setName(filename);
    return true;
}

#pragma endregion

#pragma region Savers

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves image to an arbitrary file with FreeImage.
 * @param filename 2D file name
 * @param flipHorizontal flips the image horizontally when true
 * @param flipVertical flips the image vertically when true
 * @return TF
 */
bool OV_API Ov::Image::saveWithFreeImage(const std::string& filename, bool flipHorizontal, bool flipVertical)
{
    // Safety net:
    if (filename.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }


    // Load texture:
    FIBITMAP* fBitmap = nullptr;
    switch (this->getFormat())
    {
        ///////////////////////
    case Format::r8g8b8: //
        fBitmap = FreeImage_ConvertFromRawBits(this->getData(), this->getSizeX(), this->getSizeY(), 3 * this->getSizeX(), 24, 0xFF0000, 0x00FF00, 0x0000FF, flipHorizontal);
        break;

        /////////////////////////
    case Format::r8g8b8a8: //
        fBitmap = FreeImage_ConvertFromRawBits(this->getData(), this->getSizeX(), this->getSizeY(), 4 * this->getSizeX(), 32, 0xFF0000, 0x00FF00, 0x0000FF, flipHorizontal);
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Unsupported pixel format");
        return false;
    }

    // BGR->RGB:
    FIBITMAP* redChannel = FreeImage_GetChannel(fBitmap, FICC_RED);
    FIBITMAP* blueChannel = FreeImage_GetChannel(fBitmap, FICC_BLUE);

    if (!FreeImage_SetChannel(fBitmap, blueChannel, FICC_RED) || !FreeImage_SetChannel(fBitmap, redChannel, FICC_BLUE))
    {
       OV_LOG_ERROR("Unable to swap red/blue channels for file '%s'", filename.c_str());
       FreeImage_Unload(fBitmap);
       return false;
    }

    // Save:
    if (FreeImage_Save(FIF_JPEG, fBitmap, filename.c_str(), 0) == false)
    {
        OV_LOG_ERROR("Unable to save to file '%s'", filename.c_str());
        FreeImage_Unload(fBitmap);
        return false;
    }

    // Release unused resources:
    FreeImage_Unload(fBitmap);

    // Done:
    this->setName(filename);
    return true;
}

#pragma endregion