/**
 * @file	overv_base_bitmap.cpp
 * @brief	Basic generic bitmap properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 *			Mchel Basili (michel.basili@supsi.student.ch), 2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// DDS support:
#include "dds.h"

// KTX support:
#include "ktx.h"

// Magic enum:
#include "magic_enum.hpp"

// C/C++:
#include <algorithm>


////////////
// STATIC //
////////////

// Special values:
Ov::Bitmap Ov::Bitmap::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Bitmap reserved structure.
 */
struct Ov::Bitmap::Reserved
{
	Ov::Bitmap::Format format;       ///< Image format
	std::vector<Layer> layer;        ///< Bitmap layers;
	uint32_t nrOfLevels;             ///< Number of levels (mipmaps)
	uint32_t nrOfSides;              ///< Number of sides (faces)


	/**
	 * Constructor.
	 */
	Reserved() : format{ Ov::Bitmap::Format::none }, nrOfLevels{ 0 }, nrOfSides{ 0 }
	{}
};


//////////////////////////
// BODY OF CLASS Bitmap //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Bitmap::Bitmap() : reserved(std::make_unique<Ov::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Bitmap::Bitmap(Format format, uint32_t sizeX, uint32_t sizeY, uint8_t* data) : reserved(std::make_unique<Ov::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
	loadRawData(format, sizeX, sizeY, data);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Bitmap::Bitmap(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Bitmap::Bitmap(Bitmap&& other) : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Bitmap::~Bitmap()
{
	OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get bitmap format.
 * @return bitmap format (as Format enum)
 */
Ov::Bitmap::Format OV_API Ov::Bitmap::getFormat() const
{
	return reserved->format;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of sides.
 * @return number of sides
 */
uint32_t OV_API Ov::Bitmap::getNrOfSides() const
{
	return reserved->nrOfSides;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of levels.
 * @return number of levels
 */
uint32_t OV_API Ov::Bitmap::getNrOfLevels() const
{
	return reserved->nrOfLevels;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get color depth in bytes.
 * @return color depth or 0 on error
 */
uint32_t OV_API Ov::Bitmap::getColorDepth() const
{
	switch (reserved->format)
	{
	case Format::r8_compressed:
		return 1;

	case Format::r8g8_compressed:
		return 2;

	case Format::r8g8b8:
	case Format::r8g8b8_compressed:
		return 3;

	case Format::r8g8b8a8:
	case Format::r8g8b8a8_compressed:
		return 4;

	default:
		OV_LOG_ERROR("Invalid value");
		return 0;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get width in pixels of the specific level/side.
 * @param level
 * @param side
 * @return width in pixels or 0 on error
 */
uint32_t OV_API Ov::Bitmap::getSizeX(uint32_t level, uint32_t side) const
{
	// Safet net:
	if (reserved->layer.empty() || level >= reserved->nrOfLevels || side >= reserved->nrOfSides)
	{
		OV_LOG_ERROR("Invalid params");
		return 0;
	}

	return reserved->layer[side * reserved->nrOfLevels + level].size.x;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get height in pixels of the specific level/side.
 * @param level
 * @param side
 * @return height in pixels or 0 on error
 */
uint32_t OV_API Ov::Bitmap::getSizeY(uint32_t level, uint32_t side) const
{
	// Safet net:
	if (reserved->layer.empty() || level >= reserved->nrOfLevels || side >= reserved->nrOfSides)
	{
		OV_LOG_ERROR("Invalid params");
		return 0;
	}

	return reserved->layer[side * reserved->nrOfLevels + level].size.y;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get a pointer to the data of a specific level/side.
 * @param level
 * @param side
 * @return pointer to data
 */
uint8_t OV_API* Ov::Bitmap::getData(uint32_t level, uint32_t side) const
{
	// Safet net:
	if (reserved->layer.empty() || level >= reserved->nrOfLevels || side >= reserved->nrOfSides)
	{
		OV_LOG_ERROR("Invalid params");
		return nullptr;
	}

	return reserved->layer[side * reserved->nrOfLevels + level].data.data();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
/**
 * Returns the size in bytes of the given level/side.
 * @param level
 * @param side
 * @return size in bytes
 */
uint32_t OV_API Ov::Bitmap::getNrOfBytes(uint32_t level, uint32_t side) const
{
	// Safet net: 
	if (reserved->layer.empty() || level >= reserved->nrOfLevels || side >= reserved->nrOfSides)
	{
		OV_LOG_ERROR("Invalid params");
		return 0;
	}

	return static_cast<uint32_t>(reserved->layer[side * reserved->nrOfLevels + level].data.size());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
/**
 * Set the layer that compose the bitmap
 * @param layers vector containing all the data of all the layers.
 * @return TF
 */
bool OV_API Ov::Bitmap::setLayers(std::vector<Layer>& layers) 
{
	reserved->layer = std::move(layers);

	// Done:
	return true;
}

#pragma endregion

#pragma region Loaders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load image from memory.
 * @param format image format
 * @param sizeX width in pixels
 * @param sizeY height in pixels
 * @param data pointer to the image data
 * @return TF
 */
bool OV_API Ov::Bitmap::loadRawData(Format format, uint32_t sizeX, uint32_t sizeY, uint8_t* data)
{
	// Safety net:
	if (data == nullptr)
	{
		OV_LOG_ERROR("Invalid params");
		return false;
	}

	// Image size:
	uint32_t colorDepth = 0;
	switch (format)
	{
	case Format::r8: colorDepth = 1; break;
	case Format::r8g8: colorDepth = 2; break;
	case Format::r8g8b8: colorDepth = 3; break;
	case Format::r8g8b8a8: colorDepth = 4; break;
	default:
		OV_LOG_ERROR("Invalid format");
		return false;
	}
	uint64_t size = (uint64_t)sizeX * (uint64_t)sizeY * (uint64_t)colorDepth;

	clear();

	// Force single image:
	reserved->format = format;
	reserved->nrOfSides = 1;
	reserved->nrOfLevels = 1;

	// Allocate and populate layer:   
	Layer l;
	l.size.x = sizeX;
	l.size.y = sizeY;
	l.data.resize(size);
	memcpy(l.data.data(), data, size);

	// Store layer:
	reserved->layer.push_back(l);

	// Done:   
	this->setName(std::string("fromMemory"));
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load image from a .dds file.
 * @param filename DDS file name
 * @return TF
 */
bool OV_API Ov::Bitmap::loadDDS(const std::string& filename)
{
	// Safety net:
	if (filename.empty())
	{
		OV_LOG_ERROR("Invalid params");
		return false;
	}

	clear();

	// Get file size:
	FILE* dat = fopen(filename.c_str(), "rb");
	if (dat == nullptr)
	{
		OV_LOG_ERROR("File '%s' not found", filename.c_str());
		return false;
	}
	fseek(dat, 0, SEEK_END);
	uint64_t filesize = ftell(dat);
	fseek(dat, 0, SEEK_SET);

	// Copy file to memory:
	std::vector<uint8_t> data(filesize);
	uint8_t* position = data.data();
	if (fread(position, sizeof(uint8_t), filesize, dat) != filesize)
	{
		OV_LOG_ERROR("File '%s' damaged", filename.c_str());
		fclose(dat);
		return false;
	}
	fclose(dat);

	// Check header:   
	uint32_t magicNumber;
	memcpy(&magicNumber, position, sizeof(uint32_t)); position += sizeof(uint32_t);
	if (magicNumber != DDS_MAGICNUMBER)
	{
		OV_LOG_ERROR("File '%s' is not a valid DDS", filename.c_str());
		return false;
	}

	// Get header:
	DDS_HEADER* header = reinterpret_cast<DDS_HEADER*> (position); position += sizeof(DDS_HEADER);
	reserved->nrOfLevels = header->dwMipMapCount;

	// Cubemap (old format)?
	reserved->nrOfSides = 1;
	if (header->dwCaps2 & DDSCAPS2_CUBEMAP)
	{
		OV_LOG_DEBUG("Image is a cubemap");

		// Check completeness (only 6-sided cubemaps are supported):
		bool complete = true;
		if (!(header->dwCaps2 & DDSCAPS2_CUBEMAP_POSITIVEX)) complete = false;
		if (!(header->dwCaps2 & DDSCAPS2_CUBEMAP_POSITIVEY)) complete = false;
		if (!(header->dwCaps2 & DDSCAPS2_CUBEMAP_POSITIVEZ)) complete = false;
		if (!(header->dwCaps2 & DDSCAPS2_CUBEMAP_NEGATIVEX)) complete = false;
		if (!(header->dwCaps2 & DDSCAPS2_CUBEMAP_NEGATIVEY)) complete = false;
		if (!(header->dwCaps2 & DDSCAPS2_CUBEMAP_NEGATIVEZ)) complete = false;
		if (!complete)
		{
			OV_LOG_ERROR("File '%s' is an incomplete cubemap", filename.c_str());
			return false;
		}
		reserved->nrOfSides = 6;
	}

	// Check format:
	char fourCC[5];
	memcpy(fourCC, &header->ddspf.dwFourCC, 4);
	fourCC[4] = '\0';

	OV_LOG_DEBUG("File fourCC: '%s'", fourCC);
	if (strcmp(fourCC, "DXT1") == 0)
		reserved->format = Ov::Bitmap::Format::r8g8b8_compressed;
	else
		if (strcmp(fourCC, "DXT5") == 0)
			reserved->format = Ov::Bitmap::Format::r8g8b8a8_compressed;
		else
			if (strcmp(fourCC, "ATI1") == 0)
				reserved->format = Ov::Bitmap::Format::r8_compressed;
			else
				if (strcmp(fourCC, "ATI2") == 0)
					reserved->format = Ov::Bitmap::Format::r8g8_compressed;
				else
					if (strcmp(fourCC, "DX10") == 0)
					{
						// Get header10:
						DDS_HEADER10* header10 = reinterpret_cast<DDS_HEADER10*> (position); position += sizeof(DDS_HEADER10);

						// Cube map (new format)?
						OV_LOG_DEBUG("Array: %u", header10->arraySize);
						if (header10->arraySize == 6)
						{
							OV_LOG_DEBUG("Image is a cubemap");
							reserved->nrOfSides = header10->arraySize;
						}

						// Check format:
						switch (header10->dxgiFormat)
						{
						case DXGI_FORMAT_BC1_UNORM:
							reserved->format = Ov::Bitmap::Format::r8g8b8_compressed;
							break;

						case DXGI_FORMAT_BC3_UNORM:
							reserved->format = Ov::Bitmap::Format::r8g8b8a8_compressed;
							break;

						default:
							OV_LOG_ERROR("File '%s' uses an unsupported DX10 compression format", filename.c_str());
							return false;
						}
					}
					else
					{
						OV_LOG_ERROR("File '%s' uses an unsupported compression format", filename.c_str());
						//return false;
					}

	float compFactor = 0.0f;
	switch (reserved->format)
	{
	case Ov::Bitmap::Format::r8_compressed:         compFactor = 0.5f; break;
	case Ov::Bitmap::Format::r8g8_compressed:       compFactor = 1.0f; break;
	case Ov::Bitmap::Format::r8g8b8_compressed:     compFactor = 0.5f; break;
	case Ov::Bitmap::Format::r8g8b8a8_compressed:   compFactor = 1.0f; break;
	}

	// Allocate and populate layers:   
	Layer l;
	for (uint32_t s = 0; s < reserved->nrOfSides; s++)
	{
		uint32_t sizeX = header->dwWidth;
		uint32_t sizeY = header->dwHeight;
		for (uint32_t c = 0; c < reserved->nrOfLevels; c++)
		{
			// Store layer:
			reserved->layer.push_back(l);
			Layer& curLayer = reserved->layer.back();

			curLayer.size.x = sizeX;
			curLayer.size.y = sizeY;
			uint32_t levelSize = (uint32_t)(compFactor * sizeX * sizeY);
			if (sizeX < 4)
				levelSize = (uint32_t)(compFactor * sizeX * 2 * sizeY);
			if (compFactor == 0.5f && levelSize < 8)
				levelSize = 8;
			if (compFactor == 1.0f && levelSize < 16)
				levelSize = 16;
			curLayer.data.resize(levelSize);
			memcpy(curLayer.data.data(), position, levelSize); position += levelSize;

			OV_LOG_DEBUG("Mipmap: %u, %ux%u, %u bytes", c, sizeX, sizeY, levelSize);

			// Update values:
			if (sizeX > 1)
				sizeX /= 2;
			if (sizeY > 1)
				sizeY /= 2;
		}
	}

	// Done:
	this->setName(filename);
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load image from a .ktx file.
 * @param filename ktx file name
 * @param format to load
 * @return TF
 */
bool OV_API Ov::Bitmap::loadKTX(const std::string& filename, Format loadFormat)
{
	OV_LOG_ERROR("This is the Overv Core loadKTX method. Need to implement specific version.");
	return false;
}

#pragma endregion

#pragma region Savers

// Todo fix the method. Not working well
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Write bitmap to a .dds file.
 * @return TF
 */
bool OV_API Ov::Bitmap::saveAsDDS() const
{
	// Gather information
	Format format = Ov::Bitmap::getFormat();
	uint32_t width = Ov::Bitmap::getSizeX();
	uint32_t height = Ov::Bitmap::getSizeY();
	uint32_t nrOfLevels = Ov::Bitmap::getNrOfLevels();
	uint32_t nrOfSides = Ov::Bitmap::getNrOfSides();
	uint32_t bytesPerPixel = Ov::Bitmap::getColorDepth();

	// Safety net:
	if (nrOfLevels == 0 || nrOfSides == 0)
	{
		OV_LOG_ERROR("The mipmapLevel or the nr of sides is 0.");
		return false;
	}
	if (width == 0 && height == 0)
	{
		OV_LOG_ERROR("The width or the height is 0.");
		return false;
	}
	//// Not working for uncompressed image
	//if (format == Format::r8g8b8 || format == Format::r8g8b8a8) 
	//{
	//	OV_LOG_ERROR("For know the implementation to save uncompressed image in dds is not working.");
	//	return false;
	//}


	// Fill headers
	uint32_t magicNumber = DDS_MAGICNUMBER;
	DDS_HEADER header = {};
	DDS_PIXELFORMAT pixelFormat = {};
	DDS_HEADER10 header10 = {};

	////////////
	// HEADER //
	////////////

	// Size of structure. This member must be set to 124.
	header.dwSize = 124;
	// Flags to indicate which members contain valid data.
	// Required in every .dds file.
	header.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
	// Surface height(in pixels).
	header.dwHeight = height;
	// Surface width (in pixels).
	header.dwWidth = width;
	// The pitch or number of bytes per scan line in an uncompressed texture;
	// the total number of bytes in the top level texture for a compressed texture. 
	if (format == Format::r8g8b8 || format == Format::r8g8b8a8)// || format == Format::b8g8r8a8)
	{
		// For uncompressed formats, this is the number of bytes per scan line (DWORD> aligned)
		// for the main image. dwFlags should include DDSD_PITCH in this case.
		header.dwFlags |= DDSD_PITCH;
		header.dwPitchOrLinearSize = (width * bytesPerPixel + 7) / 8;
	}
	else if (format == Format::r8g8b8_compressed || format == Format::r8g8b8a8_compressed ||
		format == Format::r8g8_compressed || format == Format::r8_compressed)
	{
		// For compressed formats, this is the total number of bytes for the main
		// image.dwFlags should be include DDSD_LINEARSIZE in this case.
		header.dwFlags |= DDSD_LINEARSIZE;
		header.dwPitchOrLinearSize = Ov::Bitmap::getNrOfBytes(0, 0);//std::max(1, (((int)width + 3) / 4)) *
		((format == Format::r8g8b8_compressed || format == Format::r8_compressed) ? 8 : 16);
	}
	else
	{
		OV_LOG_ERROR("Invalid Bitmap Format.");
		return false;
	}
	// Depth of a volume texture (in pixels), otherwise unused.
	header.dwDepth = 0;
	// Number of mipmap levels, otherwise unused.
	header.dwMipMapCount = 0; nrOfLevels;
	// Specifies the complexity of the surfaces stored.
	// Required
	header.dwCaps = DDSCAPS_TEXTURE;
	// Additional detail about the surfaces stored.


	if (nrOfLevels > 1)
	{
		// Required in a mipmapped texture.
		header.dwFlags |= DDSD_MIPMAPCOUNT;

		// should be used for a mipmap.
		header.dwCaps |= DDSCAPS_MIPMAP;
		header.dwCaps |= DDSCAPS_COMPLEX;
	}

	if (nrOfSides > 1)
	{
		// must be used on any file that contains more than one surface 
		// (a mipmap, a cubic environment map, or mipmapped volume texture).
		header.dwCaps |= DDSCAPS_COMPLEX;
	}

	// Additional detail about the surfaces stored.
	header.dwCaps2 = 0;
	if (nrOfLevels >= 6)
	{
		header.dwCaps2 = DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEX;
		if (nrOfSides > 1) header.dwCaps2 |= DDSCAPS2_CUBEMAP_POSITIVEY;
		if (nrOfSides > 2) header.dwCaps2 |= DDSCAPS2_CUBEMAP_POSITIVEZ;
		if (nrOfSides > 3) header.dwCaps2 |= DDSCAPS2_CUBEMAP_NEGATIVEX;
		if (nrOfSides > 4) header.dwCaps2 |= DDSCAPS2_CUBEMAP_NEGATIVEY;
		if (nrOfSides > 5) header.dwCaps2 |= DDSCAPS2_CUBEMAP_NEGATIVEZ;
	}


	/////////////////
	// PIXELFORMAT //
	/////////////////

	// Structure size; set to 32 (bytes).
	pixelFormat.dwSize = 32;
	// Red (or luminance or Y) mask for reading color data.
	pixelFormat.dwRBitMask = 0xFF000000;
	// Green(or U) mask for reading color data.
	pixelFormat.dwGBitMask = 0x00FF0000;
	// Blue (or V) mask for reading color data.
	pixelFormat.dwBBitMask = 0x0000FF00;
	// Alpha mask for reading alpha data.
	pixelFormat.dwABitMask = 0x000000FF;

	// store format
	std::string fourCC;
	switch (format)
	{
		///////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8: //    
		// Values which indicate what type of data is in the surface.
		pixelFormat.dwFlags = DDPF_RGB;

		// Four-character codes for specifying compressed or custom formats.
		pixelFormat.dwFourCC = 0x0;

		// Number of bits in an RGB (possibly including alpha) format.
		pixelFormat.dwRGBBitCount = bytesPerPixel * 8;

		break;
		/////////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8a8: //		      
		// Values which indicate what type of data is in the surface.
		pixelFormat.dwFlags = DDPF_RGB | DDPF_ALPHAPIXELS;

		// Four-character codes for specifying compressed or custom formats.
		pixelFormat.dwFourCC = 0x0;

		// Number of bits in an RGB (possibly including alpha) format.
		pixelFormat.dwRGBBitCount = bytesPerPixel * 8;

		//if (format == Ov::Bitmap::Format::b8g8r8a8)
		//{
		//	// Red (or luminance or Y) mask for reading color data.
		//	pixelFormat.dwRBitMask = 0x0000FF00;
		//	// Blue (or V) mask for reading color data.
		//	pixelFormat.dwBBitMask = 0xFF000000;
		//}
		//break;

		//////////////////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8_compressed: //
		// Values which indicate what type of data is in the surface.
		pixelFormat.dwFlags = DDPF_FOURCC;

		// Four-character codes for specifying compressed or custom formats.
		// store format
		fourCC = "DXT1";
		memcpy(&pixelFormat.dwFourCC, fourCC.c_str(), 4);

		// Number of bits in an RGB (possibly including alpha) format.
		pixelFormat.dwRGBBitCount = bytesPerPixel * 8;
		break;

		////////////////////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8a8_compressed: //
		// Values which indicate what type of data is in the surface.
		pixelFormat.dwFlags = DDPF_FOURCC;

		// Four-character codes for specifying compressed or custom formats.
		// store format
		fourCC = "DXT5";
		memcpy(&pixelFormat.dwFourCC, fourCC.c_str(), 4);

		// Number of bits in an RGB (possibly including alpha) format.
		pixelFormat.dwRGBBitCount = bytesPerPixel * 8;
		break;

		////////////////////////////////////////////
	case Ov::Bitmap::Format::r8g8_compressed: //
		// Values which indicate what type of data is in the surface.
		pixelFormat.dwFlags = DDPF_FOURCC;

		// Four-character codes for specifying compressed or custom formats.
		// store format
		fourCC = "ATI2";
		memcpy(&pixelFormat.dwFourCC, fourCC.c_str(), 4);

		// Number of bits in an RGB (possibly including alpha) format.
		pixelFormat.dwRGBBitCount = bytesPerPixel * 8;
		break;

		//////////////////////////////////////////
	case Ov::Bitmap::Format::r8_compressed: //
		// Values which indicate what type of data is in the surface.
		pixelFormat.dwFlags = DDPF_FOURCC;

		// Four-character codes for specifying compressed or custom formats.
		// store format
		fourCC = "ATI1";
		memcpy(&pixelFormat.dwFourCC, fourCC.c_str(), 4);

		// Number of bits in an RGB (possibly including alpha) format.
		pixelFormat.dwRGBBitCount = bytesPerPixel * 8;
		break;

		///////////
	default: //
		OV_LOG_ERROR("Invalid Bitmap Format.");
		return false;
		break;
	}

	//////////////////
	// HEADER_DXT10 //
	//////////////////

	bool useDXT10 = false;
	if (nrOfSides > 1)
	{
		// Four-character codes for specifying compressed or custom formats.
		// store format
		char fourCC[5] = "DX10";
		memcpy(&pixelFormat.dwFourCC, fourCC, 4);

		switch (format)
		{
			///////////////////////////////////
		case Ov::Bitmap::Format::r8g8b8: //    
			// The surface pixel format 
			//header10.dxgiFormat = DXGI_FORMAT_R8G8B8_UNORM;
			OV_LOG_ERROR("Cna't save dds format with alpha channel. Need to ad 1 byte for each rgb value.");
			return false;
			break;
			/////////////////////////////////////
		case Ov::Bitmap::Format::r8g8b8a8: //	
			// The surface pixel format 
			header10.dxgiFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
			break;

			//////////////////////////////////////////////
		case Ov::Bitmap::Format::r8g8b8_compressed: //
			// The surface pixel format 
			header10.dxgiFormat = DXGI_FORMAT_BC1_UNORM;
			break;

			////////////////////////////////////////////////
		case Ov::Bitmap::Format::r8g8b8a8_compressed: //
			// The surface pixel format 
			header10.dxgiFormat = DXGI_FORMAT_BC3_UNORM;
			break;

			////////////////////////////////////////////
		case Ov::Bitmap::Format::r8g8_compressed: //
			// The surface pixel format 
			header10.dxgiFormat = DXGI_FORMAT_BC5_UNORM;
			break;

			//////////////////////////////////////////
		case Ov::Bitmap::Format::r8_compressed: //
			// The surface pixel format 
			header10.dxgiFormat = DXGI_FORMAT_BC4_UNORM;
			break;

			///////////
		default: //
			OV_LOG_ERROR("Invalid Bitmap Format.");
			return false;
		}

		// Identifies the type of resource.
		header10.resourceDimension = D3D10_RESOURCE_DIMENSION_TEXTURE2D;

		if (nrOfSides % 6 == 0)
		{
			// Indicates a 2D texture is a cube-map texture.
			header10.miscFlag = D3D10_RESOURCE_MISC_TEXTURECUBE;
			// For a 2D texture that is also a cube-map texture, this number represents the number of cubes
			header10.arraySize = nrOfSides / 6;
		}
		else
		{
			// Indicates a 2D texture is a cube-map texture.
			header10.miscFlag = 0;
			// For a 2D texture that is also a cube-map texture, this number represents the number of cubes
			header10.arraySize = 0;
		}

		// The legacy D3DX 10 and D3DX 11 utility libraries will fail to
		// load any .DDS file with miscFlags2 not equal to zero.
		header10.miscFlags2 = 0;

		useDXT10 = true;
	}

	// The pixel format
	header.ddspf = pixelFormat;

	///////////////////
	// File assembly //
	///////////////////

	std::vector<uint8_t> ddsFile;
	uint64_t numberOfBytes = 0;

	// Magic Number
	{
		int size = sizeof(uint32_t);
		ddsFile.resize(numberOfBytes + size);
		const void* rawData = &magicNumber;
		memcpy(ddsFile.data() + numberOfBytes, static_cast<const uint8_t*>(rawData), size);
		numberOfBytes += size;
	}

	// Header
	{
		int size = sizeof(DDS_HEADER);
		ddsFile.resize(numberOfBytes + size);
		const void* rawData = &header;
		memcpy(ddsFile.data() + numberOfBytes, static_cast<const uint8_t*>(rawData), size);
		numberOfBytes += size;
	}

	// Header DXT10
	if (useDXT10)
	{
		int size = sizeof(DDS_HEADER10);
		ddsFile.resize(numberOfBytes + size);
		const void* rawData = &header10;
		memcpy(ddsFile.data() + numberOfBytes, static_cast<const uint8_t*>(rawData), size);
		numberOfBytes += size;
	}

	// Data
	for (uint32_t s = 0; s < nrOfSides; s++)
	{
		for (uint32_t c = 0; c < nrOfLevels; c++)
		{
			// Copy data in bitmap buffer.
			uint32_t currentLevelSize = Ov::Bitmap::getNrOfBytes(c, s);
			ddsFile.resize(numberOfBytes + currentLevelSize);
			memcpy(ddsFile.data() + numberOfBytes, Ov::Bitmap::getData(c, s), (size_t)currentLevelSize);
			numberOfBytes += currentLevelSize;
		}
	}

	// Reduce its capacity to fit its size.
	ddsFile.shrink_to_fit();


	// Write file
	std::string rawname = Ov::Object::getName().substr(0, Ov::Object::getName().find_last_of(".")) + ".dds";
	FILE* dat = nullptr;
	fopen_s(&dat, rawname.c_str(), "wb");
	if (dat == nullptr)
	{
		OV_LOG_ERROR("Unable to open file '%s' for writing", rawname.c_str());
		return false;
	}

	if (fwrite(ddsFile.data(), sizeof(uint8_t), numberOfBytes, dat) != numberOfBytes)
	{
		OV_LOG_ERROR("Unable to write data to file '%s'", rawname.c_str());
		fclose(dat);
		return false;
	}

	// Done:
	fclose(dat);
	OV_LOG_DEBUG("File '%s' written", rawname.c_str());

	//Done
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Write bitmap to a .ktx file.
 * @param supercompress determine if apply supercompression.
 * @param preserveHighQuality determine which type of compression to use.
 * @return TF
 */
bool OV_API Ov::Bitmap::saveAsKTX(bool supercompress, bool preserveHighQuality) const
{
	OV_LOG_ERROR("This is the Overv Core SAVEKTX method. Need to implement specific version.");
	return false;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear the bitmap.
 * @return TF
 */
bool OV_API Ov::Bitmap::clear() 
{
	reserved->layer.clear();

	reserved->format = Ov::Bitmap::Format::none;
	reserved->nrOfLevels = 0;
	reserved->nrOfSides = 0;

	// Done:
	return true;
}

#pragma endregion