/**
 * @file	overv_base_serializer.cpp
 * @brief	Basic generic serializer
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++:
#include <iterator>


////////////
// STATIC //
////////////

// Special values:
Ov::Serializer Ov::Serializer::empty;


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Serializer reserved structure.
 */
struct Ov::Serializer::Reserved
{
   uint64_t position;
   uint64_t nrOfBytes;
   std::vector<uint8_t> data;


   /**
    * Constructor.
    */
   Reserved() : position{ 0 }, nrOfBytes{ 0 }
   {}
};


//////////////////////////////
// BODY OF CLASS Serializer //
//////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Serializer::Serializer() : reserved(std::make_unique<Ov::Serializer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Copy constructor.
 */
OV_API Ov::Serializer::Serializer(const Serializer& other) : reserved(std::make_unique<Ov::Serializer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    *reserved = *other.reserved;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor from serialized data.
 * @param rawData pointer to serialized data (nullptr allocates empty space)
 * @param nrOfBytes size of the serialized data
 */
OV_API Ov::Serializer::Serializer(const void* rawData, uint64_t nrOfBytes) : reserved(std::make_unique<Ov::Serializer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    reserved->nrOfBytes = nrOfBytes;
    if (rawData == nullptr)
        reserved->data.resize(nrOfBytes);
    else
    {
        const uint8_t* ptr = static_cast<const uint8_t*>(rawData);
        std::copy(ptr, ptr + nrOfBytes, std::back_inserter(reserved->data));
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Serializer::~Serializer()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Operators

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Copy assignment.
 */
void OV_API Ov::Serializer::operator=(const Serializer& other)
{
    *reserved = *other.reserved;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Pointer to serialized data.
 * @return pointer to serialized data
 */
void OV_API* Ov::Serializer::getData() const
{
    reserved->data.shrink_to_fit();
    return static_cast<void*>(reserved->data.data());
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Pointer to serialized data at the current deserializing position.
 * @return pointer to serialized data at the current position, or nullptr if no more data left
 */
void OV_API* Ov::Serializer::getDataAtCurPos() const
{
    if (reserved->position >= reserved->nrOfBytes)
        return nullptr;

    reserved->data.shrink_to_fit();
    return static_cast<void*>(reserved->data.data() + reserved->position);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the number of bytes stored in the serializer.
 * @return number of bytes stored
 */
uint64_t OV_API Ov::Serializer::getNrOfBytes() const
{
    return reserved->nrOfBytes;
}

#pragma endregion

#pragma region Serialization

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Resets the internal data.
 */
void OV_API Ov::Serializer::reset()
{
    reserved->position = 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clears the internal data.
 */
void OV_API Ov::Serializer::clear()
{
    reserved->data.clear();
    reserved->position = 0;
    reserved->nrOfBytes = 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a string.
 * @param text string to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const std::string& text)
{
    // Safety net:
    if (text.length() > 255)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    uint8_t size = static_cast<uint8_t>(text.length());
    serialize(&size, sizeof(uint8_t));
    serialize(text.c_str(), text.length());

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a byte.
 * @param byte byte to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const uint8_t& byte)
{
    serialize(&byte, sizeof(uint8_t));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a boolean.
 * @param _bool boolean to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const bool& _bool)
{
    serialize(&_bool, sizeof(bool));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a uint.
 * @param uint unsigned int to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const uint32_t& uint)
{
    serialize(&uint, sizeof(uint32_t));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a float.
 * @param _float float to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const float& _float)
{
    serialize(&_float, sizeof(float));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a vec3.
 * @param vec vec3 to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const glm::vec3& vec)
{
    serialize(&vec, sizeof(glm::vec3));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a vec4.
 * @param vec vec4 to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const glm::vec4& vec)
{
    serialize(&vec, sizeof(glm::vec4));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a mat4.
 * @param mat mat4 to serialize
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const glm::mat4& mat)
{
    serialize(&mat, sizeof(glm::mat4));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Serializes a series of raw bytes.
 * @param rawData pointer to data
 * @param nrOfBytes number of bytes
 * @return TF
 */
bool OV_API Ov::Serializer::serialize(const void* rawData, uint64_t nrOfBytes)
{
    // Safet net:
    if (rawData == nullptr || nrOfBytes == 0)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Increase and store:
    reserved->data.resize(reserved->nrOfBytes + nrOfBytes);
    memcpy(reserved->data.data() + reserved->nrOfBytes, static_cast<const uint8_t*>(rawData), nrOfBytes);
    reserved->nrOfBytes += nrOfBytes;

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a string.
 * @param text string to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(std::string& text)
{
    uint8_t size;
    deserialize(&size, sizeof(uint8_t));
    if (reserved->position + size > reserved->nrOfBytes)
    {
        OV_LOG_ERROR("Corrupted serialization");
        return false;
    }
    text.resize(size);
    deserialize(text.data(), size);

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a byte.
 * @param byte byte to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(uint8_t& byte)
{
    deserialize(&byte, sizeof(uint8_t));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a boolean.
 * @param _bool boolean to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(bool& _bool)
{
    deserialize(&_bool, sizeof(bool));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a uint.
 * @param uint unsigned int to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(uint32_t& uint)
{
    deserialize(&uint, sizeof(uint32_t));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a float.
 * @param _float float to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(float& _float)
{
    deserialize(&_float, sizeof(float));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a vec3.
 * @param vec vec3 to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(glm::vec3& vec)
{
    deserialize(&vec, sizeof(glm::vec3));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a vec4.
 * @param vec vec4 to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(glm::vec4& vec)
{
    deserialize(&vec, sizeof(glm::vec4));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a mat4.
 * @param mat mat4 to deserialize
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(glm::mat4& mat)
{
    deserialize(&mat, sizeof(glm::mat4));

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deserializes a series of raw bytes.
 * @param rawData pointer to data
 * @param nrOfBytes number of bytes
 * @return TF
 */
bool OV_API Ov::Serializer::deserialize(void* rawData, uint64_t nrOfBytes)
{
    // Safet net:
    if (rawData == nullptr)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }
    if (reserved->position + nrOfBytes > reserved->nrOfBytes)
    {
        OV_LOG_ERROR("Buffer overflow");
        return false;
    }

    // Increase and store:   
    memcpy(rawData, &reserved->data[reserved->position], nrOfBytes);
    reserved->position += nrOfBytes;

    // Done:
    return true;
}

#pragma endregion