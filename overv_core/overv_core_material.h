/**
 * @file	overv_base_material.h
 * @brief	Basic generic material properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


 /**
  * @brief Class for modelling a generic material.
  */
class OV_API Material : public Ov::Renderable, public Ov::Managed, public Ov::Ovo
{
//////////
public: //
//////////

    // Special values:
	static Material empty;

	// Const/dest:
	Material();
	Material(Material&& other);
	Material(Material const&) = delete;
	virtual ~Material();

	// Textures:
	bool setTexture(const Ov::Texture& texture, Ov::Texture::Type type = Ov::Texture::Type::diffuse);
	const Ov::Texture& getTexture(Ov::Texture::Type type = Ov::Texture::Type::diffuse) const;

	// Object-specific chunk save function:
	Ov::Node& load(const std::string& filename, Ov::Container& container) = delete;
	bool save(const Ov::Node& root, const std::string& filename) = delete;

/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Textures
	virtual bool loadTexture(const std::string& textureName, Ov::Texture::Type type, Ov::Container& container);

	// Const/dest:
	Material(const std::string& name);
};