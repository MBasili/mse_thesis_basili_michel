/**
 * @file	overv_base_log.cpp
 * @brief	Logging facilities
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++ libs:
#include <stdarg.h>
#include <stdio.h>
#include <iostream>
#include <fstream> 
#include <mutex>   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Log static reserved structure.
 */
struct Ov::Log::StaticReserved
{   
   std::ofstream outputFile;              ///< Textual output file
   std::recursive_mutex mutex;            ///< Mutex to enable concurrent writing   
   CustomCallbackProto customCallback;    ///< Optional callback invoked after each message

   
   /**
    * Constructor.
    */
   StaticReserved() : customCallback{ nullptr }
   {}
};


////////////
// STATIC //
////////////   

// Reserved data:
Ov::Log::StaticReserved *Ov::Log::staticReserved = nullptr; // No unique_ptr, as the pointer might go out of scope *before* the atexit invocation!


///////////////////////
// BODY OF CLASS Log //
///////////////////////

#pragma region Init/free

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Init static components of the class.
 * @return TF
 */
bool OV_API Ov::Log::init()
{
    // Safety net:
    if (staticReserved != nullptr)
    {
        OV_LOG_ERROR("Static class already initialized");
        return false;
    }

    // Allocate and reset:
    staticReserved = new Ov::Log::StaticReserved();

    // Add shutdown hook:
    atexit([]()
        {
            OV_LOG_DEBUG("Parity check: %d", Ov::Object::getNrOfObjects());
            if (Ov::Object::getNrOfObjects() != 0)
                OV_LOG_ERROR("Memory leak detected (parity check)");
            Ov::Log::free();
        });

    staticReserved->outputFile.open(filename);
    if (!staticReserved->outputFile.is_open())
    {
        std::cout << "[!] Unable to open output log file '" << filename << "'" << std::endl;
        return false;
    }

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Release static components of the class.
 * @return TF
 */
bool OV_API Ov::Log::free()
{
    // Safety net:
    if (staticReserved == nullptr)
        return false;

    OV_LOG_DEBUG("[-] Logging completed");

    // Release resources:
    staticReserved->outputFile.close();
    delete staticReserved;
    staticReserved = nullptr;

    // Done:
    return true;
}

#pragma endregion

#pragma region Log

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Log a message. Static components are lazy-loaded at first usage.
 * @param lvl level of log (use level enum types)
 * @param fileName name of the file invoking the log
 * @param functionName name of the function invoking the log
 * @param text message, with custom series of params
 * @warning race conditions when run concurrently
 */
bool OV_API Ov::Log::log(level lvl, const char* fileName, const char* functionName, int32_t codeLine, const char* text, ...)
{
    // Init at first usage:  
    if (staticReserved == nullptr)
        if (Log::init())
            OV_LOG_DEBUG("[+] Logging to file '%s' enabled", filename);
        else
            std::cout << "[!] No logging to file for this session" << std::endl;

    //OV_STATIC_SYNCHRONIZED(staticReserved->mutex);

    // Retrieve string:
    char buffer[Log::maxLength];
    va_list list;

    // Get params:
    va_start(list, text);
    vsprintf_s(buffer, text, list);
    va_end(list);

    // Set values according to kind:
    const uint32_t maxPrefixSize = Log::maxLength;
    char prefix[maxPrefixSize];
    bool returnMessage = true;
    switch (lvl)
    {
        /////////////////////
    case level::plain: //
        sprintf_s(prefix, "%s", "");
        returnMessage = true;
        break;

        ////////////////////
    case level::info: //
        sprintf_s(prefix, "%s ", "[*]");
        returnMessage = true;
        break;

        ///////////////////////
    case level::warning: //
        sprintf_s(prefix, "%s [%s] ", "[?]", functionName);
        returnMessage = true;
        break;

        /////////////////////
    case level::error: //
        sprintf_s(prefix, "%s [%s, %s:%d] ", "[!]", fileName, functionName, codeLine);
        returnMessage = false;
        break;

        //////////////////////
    case level::debug:  //
    case level::detail: //
        sprintf_s(prefix, "%s [%s:%d] ", "[D]", functionName, codeLine);
        returnMessage = true;
        break;
    }

    // Unnecessary?
    if (lvl > Ov::Log::debugLvl)
        return returnMessage;

    // To file:
    staticReserved->outputFile << prefix << buffer << std::endl;

    // To console:
    std::cout << prefix << buffer << std::endl;

    // Custom callback?
    if (staticReserved->customCallback)
        staticReserved->customCallback(buffer, lvl, nullptr);

    // Done:
    return returnMessage;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets an optional callback that is triggered when a message occurs.
 * @param cb custom callback (nullptr to disable)
 */
void OV_API Ov::Log::setCustomCallback(CustomCallbackProto cb)
{
    // Init at first usage:  
    if (staticReserved == nullptr)
        if (Log::init())
            OV_LOG_DEBUG("[+] Logging to file '%s' enabled", filename);
        else
            std::cout << "[!] No logging to file for this session" << std::endl;

    // Release resources:
    staticReserved->customCallback = cb;
}

#pragma endregion