/**
 * @file	overv_base_object.h
 * @brief	Basic generic object properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


 /**
  * @brief Class for modeling a generic, base object. This class is inherited by most of the engine classes.
  */
class OV_API Object
{
//////////
public: //
//////////

   // Special values:
	static Object empty;

	// Callback signatures:
	typedef void (*ConstCallback) (Object*);
	typedef void (*DestCallback)  (Object*);


	/**
	* @brief Various flags.
	*/
	enum class Flag : uint32_t
	{
		none = 0,
		upload = 1,    ///< Enable to only upload data

		// Terminator:
		last
	};


	// Const/dest:
	Object();
	Object(Object&& other) noexcept;
	Object(Object const&) = delete;
	virtual ~Object();

	// Operators:
	void operator=(Object const&) = delete;
	bool operator==(const Object& rhs) const;
	bool operator!=(const Object& rhs) const;

	// General:
	void setName(const std::string& name);
	const std::string& getName() const;
	uint32_t getId() const;
	bool isDirty() const;
	void setDirty(bool dirty) const;

	// Custom data:
	void setCustomData(void* data);
	void* getCustomData() const;
	static bool setConstCallback(ConstCallback cb);
	static bool setDestCallback(DestCallback cb);

	// Statistics:
	static int32_t getNrOfObjects();


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Object(const std::string& name);
};