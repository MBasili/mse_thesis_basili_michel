/**
 * @file	overv_base_config.cpp
 * @brief	Basic generic config properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// Harsh way to use a .c as a .lib:
#pragma warning (disable : 4996)
#include <tinyxml2.cpp>
#pragma warning (default : 4996)


////////////
// STATIC //
////////////

// Special values:
Ov::Config Ov::Config::empty;


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Config reserved structure.
 */
struct Ov::Config::Reserved
{   
   std::string windowTitle;      ///< Window title
   glm::u32vec2 windowSize;      ///< Window size
   bool vsyncFlag;               ///< Vertical sync flag
   uint32_t nrOfAASamples;       ///< Number of anti-aliasing samples


   /**
    * Constructor.
    */
   Reserved() : windowTitle{ "Powered by OverV" }, windowSize{ 1024, 768 },
                vsyncFlag{ true }, nrOfAASamples{ 4 }
   {}
};


//////////////////////////
// BODY OF CLASS Config //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Config::Config() : reserved{ new Ov::Config::Reserved() }
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Config::Config(Config&& other) : reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Copy constructor.
 */
OV_API Ov::Config::Config(const Config& other) : Config()
{
    OV_LOG_DETAIL("[C]");
    *reserved = *other.reserved;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Config::~Config()
{
    OV_LOG_DETAIL("[-]");
    delete reserved;
}

#pragma endregion

#pragma region Operators

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Copy operator.
 */
Ov::Config& Ov::Config::operator=(const Ov::Config& other)
{
    if (this != &other)
        *reserved = *other.reserved;
    return *this;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets window title.
 * @param title window title
 */
void OV_API Ov::Config::setWindowTitle(const std::string& title)
{
    reserved->windowTitle = title;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets window title.
 * @return window title
 */
const std::string OV_API& Ov::Config::getWindowTitle() const
{
    return reserved->windowTitle;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets window width.
 * @param sizeX window width
 */
void OV_API Ov::Config::setWindowSizeX(uint32_t sizeX)
{
    reserved->windowSize.x = sizeX;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets window width
 * @return window width
 */
uint32_t OV_API Ov::Config::getWindowSizeX() const
{
    return reserved->windowSize.x;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets window height.
 * @param sizeY window height
 */
void OV_API Ov::Config::setWindowSizeY(uint32_t sizeY)
{
    reserved->windowSize.y = sizeY;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets window height
 * @return window height
 */
uint32_t OV_API Ov::Config::getWindowSizeY() const
{
    return reserved->windowSize.y;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the vertical sync flag.
 * @param flag vsync flag status
 */
void OV_API Ov::Config::setVSyncFlag(bool flag)
{
    reserved->vsyncFlag = flag;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the status of the vertical sync.
 * @return status of the flag
 */
bool OV_API Ov::Config::isVSync() const
{
    return reserved->vsyncFlag;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets nr. of anti-aliasing samples.
 * @param nrOfSamples nr. of anti-aliasing samples
 */
void OV_API Ov::Config::setNrOfAASamples(uint32_t nrOfSamples)
{
    reserved->nrOfAASamples = nrOfSamples;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets nr. of anti-aliasing samples.
 * @return nr. of anti-aliasing samples
 */
uint32_t OV_API Ov::Config::getNrOfAASamples() const
{
    return reserved->nrOfAASamples;
}

#pragma endregion

#pragma region loaders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads config settings from a .xml file.
 * @param filename config file name
 * @return TF
 */
bool OV_API Ov::Config::load(const std::string& filename)
{
    // Safety net:
    if (filename.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // File exists?
    FILE* dat = fopen(filename.c_str(), "rt");
    if (dat == nullptr)
    {
        OV_LOG_ERROR("Unable to open config file '%s'", filename.c_str());
        return false;
    }
    fclose(dat);

    // XML document:
    tinyxml2::XMLDocument document;

    // Load file:
    if (document.LoadFile(filename.c_str()) != 0)
    {
        OV_LOG_ERROR("Error loading config file '%s'", filename.c_str());
        return false;
    }

    // Look for tags:
    tinyxml2::XMLElement* section = document.FirstChildElement("overv");
    if (section == nullptr)
    {
        OV_LOG_ERROR("Main 'overv' section not found");
        return false;
    }

    // Window title:
    tinyxml2::XMLElement* windowTitle = section->FirstChildElement("windowtitle");
    if (windowTitle != nullptr && windowTitle->GetText() != nullptr)
        reserved->windowTitle = windowTitle->GetText();

    // Window size X:
    tinyxml2::XMLElement* windowSizeX = section->FirstChildElement("windowsizex");
    if (windowSizeX != nullptr && windowSizeX->GetText() != nullptr)
        reserved->windowSize.x = atoi(windowSizeX->GetText());

    // Window size Y:
    tinyxml2::XMLElement* windowSizeY = section->FirstChildElement("windowsizey");
    if (windowSizeY != nullptr && windowSizeY->GetText() != nullptr)
        reserved->windowSize.y = atoi(windowSizeY->GetText());

    // V-Synch flag:
    tinyxml2::XMLElement* vsyncFlag = section->FirstChildElement("vsync");
    if (vsyncFlag != nullptr && vsyncFlag->GetText() != nullptr)
        reserved->vsyncFlag = (atoi(vsyncFlag->GetText()) == 0) ? false : true;

    // MSAA nr. of samples:
    tinyxml2::XMLElement* nrOfAASamples = section->FirstChildElement("nrofaasamples");
    if (nrOfAASamples != nullptr && nrOfAASamples->GetText() != nullptr)
        reserved->nrOfAASamples = atoi(nrOfAASamples->GetText());

    // Show config:
    OV_LOG_PLAIN("   Config loaded:");
    OV_LOG_PLAIN("      window title  . . :  %s", reserved->windowTitle.c_str());
    OV_LOG_PLAIN("      window size x . . :  %u", reserved->windowSize.x);
    OV_LOG_PLAIN("      window size y . . :  %u", reserved->windowSize.y);
    OV_LOG_PLAIN("      vsync flag  . . . :  %d", reserved->vsyncFlag);
    OV_LOG_PLAIN("      MSAA samples  . . :  %u", reserved->nrOfAASamples);

    // Done:
    return true;
}

#pragma endregion