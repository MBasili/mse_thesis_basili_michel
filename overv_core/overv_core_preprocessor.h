/**
 * @file	overv_base_preprocessor.h
 * @brief	Basic preprocessor
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


/**
 * @brief Class for preprocessing various shaders.
 */
class OV_API Preprocessor : public Ov::Object
{
//////////
public: //
//////////

    // Special values:
	static Preprocessor empty;

	// Const/dest:
	Preprocessor();
	Preprocessor(Preprocessor&& other) noexcept;
	Preprocessor(Preprocessor const&) = delete;
	virtual ~Preprocessor();

	// Operators:
	void operator=(Preprocessor const&) = delete;

	// Variable manager:
	bool set(const std::string& name, const std::string& value);
	bool remove(const std::string& name);
	const std::string& get(const std::string& name) const;

	// Preprocessing:
	std::string preprocess(const std::string& source) const;


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Preprocessor(const std::string& name);
};