/**
 * @file	overv_base_texture.h
 * @brief	Basic generic texture properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


/**
 * @brief Class for modeling a generic texture.
 */
class OV_API Texture : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

    // Special values:
	static Texture empty;


	/**
	 * @brief Types of texture.
	 */
	enum class Type : uint32_t
	{
		none,

		// Texture levels:
		albedo,
		emission,
		ambient,
		diffuse,
		specular,
		normal,
		roughness,
		metalness,
		height,
		occlusion,

		// Terminator:
		last
	};


	/**
	 * @brief Types of texture formats.
	 */
	enum class Format : uint32_t
	{
		none,

		r32ui,

		// Uncompressed formats:
		r8,
		r8g8,
		r8g8b8,
		r8g8b8a8,

		// Compressed formats:
		r8_compressed,
		r8g8_compressed,
		r8g8b8_compressed,
		r8g8b8a8_compressed,

		// Depth maps:
		depth,

		// Terminator:
		last
	};


	// Const/dest:
	Texture();
	Texture(Texture&& other) noexcept;
	Texture(Texture const&) = delete;
	virtual ~Texture();

	// Get/set:
	const Ov::Bitmap& getBitmap() const;
	Format getFormat() const;
	uint32_t getSizeX() const;
	uint32_t getSizeY() const;
	uint32_t getSizeZ() const;

	// Rendering methods:
	virtual bool render(uint32_t value = 0, void* data = nullptr) const;


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Texture(const std::string& name);

	// Get/set:
	void setBitmap(const Ov::Bitmap& bitmap);
	void setFormat(Format format);
	void setSizeX(uint32_t sizeX);
	void setSizeY(uint32_t sizeY);
	void setSizeZ(uint32_t sizeZ);
};