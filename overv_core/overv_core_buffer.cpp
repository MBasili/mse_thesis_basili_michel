/**
 * @file	overv_base_buffer.cpp
 * @brief	Basic generic buffer properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++:
#include <algorithm>


////////////
// STATIC //
////////////

// Special values:
Ov::Buffer Ov::Buffer::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Buffer reserved structure.
 */
struct Ov::Buffer::Reserved
{
	Ov::Buffer::Format format;       ///< Image format
	uint64_t size;                   ///< Buffer size in bytes


	/**
	 * Constructor.
	 */
	Reserved() : format{ Ov::Buffer::Format::none }, size{ 0 }
	{}
};


//////////////////////////
// BODY OF CLASS Buffer //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Buffer::Buffer() : reserved(std::make_unique<Ov::Buffer::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Buffer::Buffer(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Buffer::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Buffer::Buffer(Buffer&& other) : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Buffer::~Buffer()
{
	OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get buffer format.
 * @return buffer format (as Format enum)
 */
Ov::Buffer::Format OV_API Ov::Buffer::getFormat() const
{
	return reserved->format;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get buffer size in bytes.
 * @return buffer size in bytes
 */
uint64_t OV_API Ov::Buffer::getSize() const
{
	return reserved->size;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set buffer size in bytes.
 * @param size buffer size in bytes
 */
void OV_API Ov::Buffer::setSize(uint64_t size)
{
	reserved->size = size;
}

#pragma endregion