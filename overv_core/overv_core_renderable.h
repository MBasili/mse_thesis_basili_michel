/**
 * @file	overv_base_renderable.h
 * @brief	Basic generic renderable object properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


/**
 * @brief Class for modelling a generic renderable object. This class is inherited by all renderable classes.
 */
class OV_API Renderable : public Ov::Object
{
//////////
public: //
//////////

    // Special values:
	static Renderable empty;

	// Const/dest:
	Renderable();
	Renderable(Renderable&& other) noexcept;
	Renderable(Renderable const&) = delete;
	virtual ~Renderable();

	// Rendering methods:
	virtual bool render(uint32_t value = 0, void* data = nullptr) const;


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Renderable(const std::string& name);
};