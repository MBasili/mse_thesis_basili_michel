/**
 * @file	overv_base_camera.cpp
 * @brief	Basic generic camera properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"


////////////
// STATIC //
////////////

// Special values:
Ov::Camera Ov::Camera::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Camera reserved structure.
 */
struct Ov::Camera::Reserved
{
	glm::mat4 projMatrix;   ///< Projection matrix


	/**
	 * Constructor.
	 */
	Reserved() : projMatrix(1.0f)
	{}
};


//////////////////////////
// BODY OF CLASS Camera //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Camera::Camera() : reserved(std::make_unique<Ov::Camera::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OV_API Ov::Camera::Camera(const std::string& name) : Ov::Node(name), reserved(std::make_unique<Ov::Camera::Reserved>())
{
	OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Camera::Camera(Camera&& other) : Ov::Node(std::move(other)), reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Camera::~Camera()
{
	OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set camera projection matrix.
 * @param matrix glm mat4x4
 */
void OV_API Ov::Camera::setProjMatrix(const glm::mat4& matrix)
{
	reserved->projMatrix = matrix;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get camera projection matrix.
 * @return glm 4x4 projection matrix
 */
const glm::mat4 OV_API& Ov::Camera::getProjMatrix() const
{
	return reserved->projMatrix;
}

#pragma endregion