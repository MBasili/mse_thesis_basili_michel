/**
 * @file	overv_base_compatibility.h
 * @brief	Header with common define and includes for xplatform porting
 *
 * @author	Achille Peternier (achille.peternier@supsi.com), (C) 2020
 */
#pragma once


//////////////
// #INCLUDE //
//////////////

// C/C++:   
#include <mutex>
#include <string>
#include <vector>
#include <list>
#include <cstdint>
#include <memory> 

// GLM:
#ifndef _DEBUG
   #define GLM_FORCE_INLINE
   #define GLM_FORCE_SSE4
#endif
   #include <glm/glm.hpp>   
   #include <glm/detail/type_half.hpp>
   #include <glm/gtc/matrix_transform.hpp>
   #include <glm/gtc/matrix_inverse.hpp>
   #include <glm/gtc/type_ptr.hpp>
   #include <glm/gtx/string_cast.hpp>   
   #include <glm/gtx/quaternion.hpp>   


/////////////
// VERSION //
/////////////

#ifdef _MSC_VER
   #define OV_WINDOWS   1     ///< We are compiling under Windows
   #pragma warning(disable : 4251) 
#else
   #define OV_LINUX     1     ///< ...then we must be under Linux
#endif
#ifdef __GNUC__
   #define OV_LINUX     1     ///< We are compiling under Linux
#endif
#if !defined(OV_WINDOWS) && !defined(OV_LINUX)
   #error "Impossible to identify (or unsupported) building environment"
#endif
#ifdef _DEBUG
   #define OV_DEBUG     1     ///< Debugging enabled macro
#endif


////////////
// MACROS //
////////////

// Sync methods:
#define OV_SYNCHRONIZED(x)             std::lock_guard<std::recursive_mutex> lock(x)                        ///< Synchronizing access via mutex
#define OV_STATIC_SYNCHRONIZED(x)      std::lock_guard<std::recursive_mutex> staticLock(x)                  ///< Syncrhonizing access via static mutex
#define __FILENAME__                   (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)   ///< Commodity macro for getting the filename only   
#define __CLASS__ std::remove_reference<decltype(classMacroImpl(this))>::type                               ///< Superduper trick to get class name at compile-time:
template<class T> T& classMacroImpl(const T* t);


///////////////////
// OPTIMIZED GLM //
///////////////////

#define OV_SSE_ALIGNED void * operator new(size_t size) \
										{ return _mm_malloc(size, 16);	} \
										void * operator new[] (size_t size) \
										{ return _mm_malloc(size, 16); } \
										void operator delete(void * p) \
										{ _mm_free(p); } \
										void operator delete[](void * p) \
										{ _mm_free(p); } ///< Put that into each class using SSE-accelerated variables
