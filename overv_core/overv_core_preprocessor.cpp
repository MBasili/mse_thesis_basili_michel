/**
 * @file	overv_base_preprocessor.cpp
 * @brief	Basic generic preprocessor
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++:
#include <unordered_map>

   
////////////
// STATIC //
////////////

// Special values:
Ov::Preprocessor Ov::Preprocessor::empty("[empty]");   

// Silly empty string:
static std::string emptyString = "";


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Preprocessor reserved structure.
 */
struct Ov::Preprocessor::Reserved
{  
   std::unordered_map <std::string, std::string> map; ///< Hash map containing the search and replace terms
   

   /**
    * Constructor. 
    */
   Reserved() 
   {}
};


////////////////////////////////
// BODY OF CLASS Preprocessor //
////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Preprocessor::Preprocessor() : reserved(std::make_unique<Ov::Preprocessor::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Preprocessor::Preprocessor(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Preprocessor::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Preprocessor::Preprocessor(Preprocessor&& other) noexcept : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Preprocessor::~Preprocessor()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region VariablesManager

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a new or modify an existing entry value.
 * @param name new (or existing) variable name
 * @param value new variable value
 * @return TF
 */
bool OV_API Ov::Preprocessor::set(const std::string& name, const std::string& value)
{
    // Update existing field?
    auto it = reserved->map.find(name);
    if (it != reserved->map.end())
        it->second = value;
    else // Add new one
        reserved->map.insert(std::pair<std::string, std::string>(name, value));

    // Done:
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Remove an existing entry.
 * @param variable existing variable name
 * @return TF
 */
bool OV_API Ov::Preprocessor::remove(const std::string& name)
{
    // Does it exist?
    auto it = reserved->map.find(name);
    if (it != reserved->map.end())
    {
        reserved->map.erase(std::string(name));
        return true;
    }

    // Whoops:
    OV_LOG_ERROR("Variable not found");
    return false;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get a variable value given its name.
 * @param name variable name
 * @return variable value or empty string if not found
 */
const std::string OV_API& Ov::Preprocessor::get(const std::string& name) const
{
    auto it = reserved->map.find(std::string(name));
    if (it == reserved->map.end())
    {
        OV_LOG_ERROR("Variable not found");
        return emptyString;
    }

    // Done:
    return it->second;
}

#pragma endregion

#pragma region Preprocessing

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Preprocess a textual shader and returns the modified content.
 * @param source shader source code
 * @return a string with the modified code or an empty string in case of error
 */
std::string OV_API Ov::Preprocessor::preprocess(const std::string& source) const
{
    // Create a copy and process it:
    std::string _source = source;

    // Step 2, find all the $$ pairs:
    size_t startPos = _source.find("$", 0);
    while (startPos != std::string::npos)
    {
        size_t endPos = _source.find("$", startPos + 1);
        if (endPos == std::string::npos)
        {
            OV_LOG_ERROR("Invalid syntax: missing end '$'");
            return std::string();
        }

        // Isolate variable name:
        std::string command = _source.substr(startPos + 1, endPos - startPos - 1);

        // Look for an entry in the available variables:
        auto it = reserved->map.find(std::string(command));
        if (it == reserved->map.end())
        {
            OV_LOG_ERROR("Variable '%s' not found", command.c_str());
            return std::string();
        }

        // Replace var:
        _source.replace(startPos, (endPos - startPos + 1), it->second);

        // Next include:
        startPos++;
        startPos = _source.find("$", startPos);
    }

    // Done:
    return _source;
}

#pragma endregion