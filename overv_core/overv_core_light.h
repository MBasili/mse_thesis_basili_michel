/**
 * @file	overv_base_light.h
 * @brief	Basic light properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief Class for modeling a generic light.
  */
class OV_API Light : public Ov::Renderable, public Ov::Ovo
{
//////////
public: //
//////////

    // Special values:
	static Light empty;

	// Const/dest:
	Light();
	Light(Light&& other) noexcept;
	Light(Light const&) = delete;
	virtual ~Light();

	// Get/set:
	const glm::vec3& getColor() const;
	void setColor(const glm::vec3& color);

	// Object-specific chunk save function:
	Ov::Node& load(const std::string& filename, Ov::Container& container) = delete;
	bool save(const Ov::Node& root, const std::string& filename) = delete;

	bool saveChunk(Ov::Serializer& serial) const override;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr) override;


/////////////
protected: //
/////////////   

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Light(const std::string& name);
};