/**
 * @file	overv_base_node.cpp
 * @brief	Basic generic node properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// C/C++:
#include <map>

   
////////////
// STATIC //
////////////

// Special values:
Ov::Node Ov::Node::empty("[empty]");      
Ov::Node::RenderableData Ov::Node::RenderableData::empty;


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Node reserved structure.
 */
struct Ov::Node::Reserved
{  
   glm::mat4 matrix;                                                    ///< Node matrix
   std::reference_wrapper<Ov::Node> parent;                             ///< Parent node
   std::list<std::reference_wrapper<Ov::Node>> children;                ///< List of children nodes   
   std::list<Ov::Node::RenderableData> renderable;


   /**
    * Constructor. 
    */
   Reserved() : matrix{ 1.0f },
                parent{ Ov::Node::empty }
   {}
};


////////////////////////
// BODY OF CLASS Node //
////////////////////////

// Where to permanently store the node? Cant be a local variable.
Ov::Node OV_API& Ov::Node::createCopy() 
{
   Ov::Node* copy = new Node();
   copy->setMatrix(getMatrix());
   copy->setName(getName() + "_Copy");
   copy->reserved->parent = Ov::Node::empty;

   std::list<Ov::Node::RenderableData>::iterator it = reserved->renderable.begin();
   while (it != reserved->renderable.end())
   {
      copy->reserved->renderable.push_back((*it));
      it++;
   }

   return *copy;
}

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Node::Node() : reserved(std::make_unique<Ov::Node::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Node::Node(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Node::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Node::Node(Node&& other) : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Node::~Node()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Positioning

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set node matrix.
 * @param matrix glm mat4x4
 */
void OV_API Ov::Node::setMatrix(const glm::mat4& matrix)
{
    reserved->matrix = matrix;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get node matrix.
 * @return glm 4x4 matrix
 */
const glm::mat4 OV_API& Ov::Node::getMatrix() const
{
    return reserved->matrix;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the world coordinate matrix of this node starting from the specified node (if empty, root node is used)
 * @param node starting node (root if empty)
 * @return world coordinate glm 4x4 matrix
 */
glm::mat4 OV_API Ov::Node::getWorldMatrix(Ov::Node& root) const
{
    auto current = std::reference_wrapper<Ov::Node>(const_cast<Ov::Node&>(*this));
    glm::mat4 result = glm::mat4(1.0f);

    // Back iteration:
    do
    {
        result = result * glm::transpose(current.get().getMatrix());
        current = current.get().getParent();
    } while (current.get() != Ov::Node::empty && current.get() != root);

    // Done:
    return glm::transpose(result);
}

#pragma endregion

#pragma region Hierarchy

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Return the parent node.
 * @return parent node
 */
Ov::Node OV_API& Ov::Node::getParent() const
{
    return reserved->parent;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Set the parent node.
 * @param parent parent node
 */
void OV_API Ov::Node::setParent(Ov::Node& parent)
{
    reserved->parent = parent;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Return a specific child, given its number.
 * @param child returns the child'th children of this node (range 0 to lastChild)
 * @return child reference or empty reference if none or error
 */
Ov::Node OV_API& Ov::Node::getChild(uint32_t id) const
{
    // Safey net:
    if (id >= reserved->children.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Node::empty;
    }

    // Iterate up to the good one:	
    auto i = reserved->children.begin();
    for (uint32_t c = 0; c < id; c++)
        i++;
    return i->get();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Return (as read-only) the internal list of children. This is used for perfomance reasons, to avoid iterating too much over the list.
 * @return reference to the internal list of children
 */
const std::list<std::reference_wrapper<Ov::Node>> OV_API& Ov::Node::getListOfChildren() const
{
    return reserved->children;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Remove and return a specific child, given its number.
 * @param child returns the child'th child of this node (range 0 to nrOfChildren - 1)
 * @return child reference or empty reference if none or error
 */
Ov::Node OV_API& Ov::Node::removeChild(uint32_t id)
{
    // Safey net:
    if (id >= reserved->children.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Node::empty;
    }

    // Iterate up to the good one:	
    auto i = reserved->children.begin();
    for (unsigned int c = 0; c < id; c++)
        i++;

    // Remove and update:
    i->get().setParent(Ov::Node::empty);
    auto& x = i->get();
    reserved->children.erase(i);
    return x;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Remove a specific node from the list of children (if it is a child).
 * @return true if node was an actuall child of this node, false otherwise
 */
bool OV_API Ov::Node::removeChild(Node& node)
{
    // Iterate up to the good one:
    std::list< std::reference_wrapper<Ov::Node> >::iterator it = reserved->children.begin();
    while (it != reserved->children.end())
    {
        if (it->get().getId() == node.getId())
        {
            it->get().setParent(Ov::Node::empty);
            reserved->children.erase(it);
            return true;
        }
        else
            it++;
    }
    return false;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Add a new child to this node.
 * @param child child node to add
 * @return TF
 */
bool OV_API Ov::Node::addChild(Ov::Node& child)
{
    // Safety net:
    if (child == Node::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Already got a parent?
    if (child.getParent() != Node::empty)
    {
        OV_LOG_ERROR("Child node already has a parent");
        return false;
    }

    // Add and update:
    reserved->children.push_back(child);
    child.setParent(*this);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Get number of children nodes.
 * @return number of children nodes
 */
uint32_t OV_API Ov::Node::getNrOfChildren() const
{
    return (uint32_t)reserved->children.size();
}

#pragma endregion

#pragma region Renderables

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Adds a new renderable to this node.
 * @param first first renderable to add
 * @param second second renderable to add (optional)
 * @param TF
 */
bool OV_API Ov::Node::addRenderable(std::reference_wrapper<Ov::Renderable> first, std::reference_wrapper<Ov::Renderable> second, bool visibleFlag)
{
    RenderableData rd(first, second, visibleFlag);
    return addRenderableData(rd);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Adds a new renderable to this node.
 * @param renderableData renderable data to add to the internal list
 * @param TF
 */
bool OV_API Ov::Node::addRenderableData(const Ov::Node::RenderableData& renderableData)
{
    // Already in the list?	
    for (auto& r : reserved->renderable)
        if (r.first.get() == renderableData.first && r.second.get() == renderableData.second)
        {
            OV_LOG_ERROR("Already in the list");
            return false;
        }

    // Add and update:   
    reserved->renderable.push_back(renderableData);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Overwrites first element of the given renderable pair.
 * @param id renderable position
 * @param renderable renderable to overwrite
 * @param TF
 */
bool OV_API Ov::Node::setFirstRenderable(uint32_t id, std::reference_wrapper<Ov::Renderable> renderable)
{
    // Safey net:
    if (id >= reserved->renderable.size())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Iterate up to the good one:	
    auto i = reserved->renderable.begin();
    for (uint32_t c = 0; c < id; c++)
        i++;
    i->first = renderable;

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Overwrites second element of the given renderable pair.
 * @param id renderable position
 * @param renderable renderable to overwrite
 * @return TF
 */
bool OV_API Ov::Node::setSecondRenderable(uint32_t id, std::reference_wrapper<Ov::Renderable> renderable)
{
    // Safey net:
    if (id >= reserved->renderable.size())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Iterate up to the good one:	
    auto i = reserved->renderable.begin();
    for (uint32_t c = 0; c < id; c++)
        i++;
    i->second = renderable;

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Sets the visibility flag on the specified renderable. If invoked without parameters, the same flag is applied to all the renderables.
 * @param visibleFlag visible flag
 * @param id renderable position (if not specified, the flag is applied to all the renderables)
 * @return TF
 */
bool OV_API Ov::Node::setVisibleFlag(bool visibleFlag, uint32_t id)
{
    // Magic value:
    if (id == std::numeric_limits<uint32_t>::max())
    {
        for (auto& r : reserved->renderable)
            r.visibleFlag = visibleFlag;

        // Done:
        return true;
    }

    // Safey net:
    if (id >= reserved->renderable.size())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Iterate up to the good one:	
    auto i = reserved->renderable.begin();
    for (uint32_t c = 0; c < id; c++)
        i++;
    i->visibleFlag = visibleFlag;

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Returns true when the specified renderable is visible (or at least one renderable is visibile when no parameter is specified).
 * @param id renderable position (if not specified, it returns true if at least one renderable is visible)
 * @return TF
 */
bool OV_API Ov::Node::isVisible(uint32_t id) const
{
    // Magic value:
    if (id == std::numeric_limits<uint32_t>::max())
    {
        bool oneAtLeast = false;
        for (auto& r : reserved->renderable)
            if (r.visibleFlag)
                oneAtLeast = true;

        // Done:
        return oneAtLeast;
    }

    // Safey net:
    if (id >= reserved->renderable.size())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Iterate up to the good one:	
    auto i = reserved->renderable.begin();
    for (uint32_t c = 0; c < id; c++)
        i++;
    return i->visibleFlag;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Get number of renderables.
 * @return number of renderables
 */
uint32_t OV_API Ov::Node::getNrOfRenderables() const
{
    return static_cast<uint32_t>(reserved->renderable.size());
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Return a specific renderable, given its number.
 * @param renderable returns the renderable'th renderable of this node (range 0 to nrOfRenderables - 1)
 * @return renderable reference or empty reference if none or error
 */
Ov::Node::RenderableData OV_API& Ov::Node::getRenderable(uint32_t id) const
{
    // Safey net:
    if (id >= reserved->renderable.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Node::RenderableData::empty;
    }

    // Iterate up to the good one:	
    auto i = reserved->renderable.begin();
    for (uint32_t c = 0; c < id; c++)
        i++;
    return *i;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Remove and return a specific renderable, given its number.
 * @param renderable returns the renderable'th renderable of this node (range 0 to nrOfRenderables - 1)
 * @return renderable reference or empty reference if none or error
 */
Ov::Node::RenderableData OV_API& Ov::Node::removeRenderable(uint32_t id)
{
    // Safey net:
    if (id >= reserved->renderable.size())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Node::RenderableData::empty;
    }

    // Iterate up to the good one:	
    auto i = reserved->renderable.begin();
    for (unsigned int c = 0; c < id; c++)
        i++;

    // Remove and update:   
    auto& x = *i;
    reserved->renderable.erase(i);
    return x;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Return (as read-only) the internal list of renderables. This is used for perfomance reasons, to avoid iterating too much over the list.
 * @return reference to the internal list of renderables
 */
const std::list<Ov::Node::RenderableData> OV_API& Ov::Node::getListOfRenderables() const
{
    return reserved->renderable;
}

#pragma endregion

#pragma region Ovo

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of this node into the serializer.
 * @param serial serializer
 * @return TF
 */
bool OV_API Ov::Node::saveChunk(Ov::Serializer& serial) const
{
    OV_LOG_DEBUG("Serializing node...");

    // Chunk header:
    uint8_t chunkId = static_cast<uint8_t>(ChunkId::node);
    uint32_t chunkSize = sizeof(uint32_t);
    chunkSize += static_cast<uint32_t>(strlen(this->getName().c_str())) + 1;
    chunkSize += sizeof(glm::mat4);
    chunkSize += sizeof(uint32_t) * 2;

    serial.serialize(&chunkId, sizeof(uint8_t));
    serial.serialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    serial.serialize(this->getId());
    serial.serialize(this->getName());
    serial.serialize(reserved->matrix);
    serial.serialize(this->getNrOfChildren());
    serial.serialize(this->getNrOfRenderables());

    // Store renderable pairs:
    for (uint32_t c = 0; c < this->getNrOfRenderables(); c++)
    {
        // First:
        uint32_t id = 0;
        auto& r = this->getRenderable(c);
        if (r.first.get() != Ov::Renderable::empty)
            id = r.first.get().getId();
        else
            id = 0;
        serial.serialize(id);

        // Second:            
        if (r.second.get() != Ov::Renderable::empty)
            id = r.second.get().getId();
        else
            id = 0;
        serial.serialize(id);

        // Visibility flag:
        serial.serialize(r.visibleFlag);
    }

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return number of children
 */
uint32_t OV_API Ov::Node::loadChunk(Ov::Serializer& serial, void* data)
{
    auto* renderables = reinterpret_cast<std::map<uint32_t, std::reference_wrapper<Ov::Renderable>> *>(data);

    // Chunk header:
    uint8_t chunkId;
    serial.deserialize(&chunkId, sizeof(uint8_t));
    if (chunkId != static_cast<uint8_t>(Ovo::ChunkId::node))
    {
        OV_LOG_ERROR("Invalid chunk ID found");
        return 0;
    }
    uint32_t chunkSize;
    serial.deserialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    uint32_t id;
    serial.deserialize(id);
    std::string name;
    serial.deserialize(name);
    this->setName(name);
    serial.deserialize(reserved->matrix);
    uint32_t nrOfChildren;
    serial.deserialize(nrOfChildren);
    uint32_t nrOfRenderables;
    serial.deserialize(nrOfRenderables);

    // Retrieve renderable pairs:
    for (uint32_t c = 0; c < nrOfRenderables; c++)
    {
        Ov::Node::RenderableData rd;

        uint32_t id;
        serial.deserialize(id);
        if (id)
        {
            auto f = renderables->find(id);
            if (f == renderables->end())
                rd.first = Ov::Renderable::empty;
            else
                rd.first = renderables->find(id)->second;
        }

        serial.deserialize(id);
        if (id)
        {
            auto f = renderables->find(id);
            if (f == renderables->end())
                rd.second = Ov::Renderable::empty;
            else
                rd.second = renderables->find(id)->second;
        }

        // Visibility flag:
        serial.deserialize(rd.visibleFlag);

        this->addRenderableData(rd);
    }

    // Done:   
    return nrOfChildren;
}

#pragma endregion

#pragma region Debugging

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Get a string representation of the hierarchy tree. For debugging purposes.
 * @return multi-line string
 */
std::string OV_API Ov::Node::getTreeAsString() const
{
    static std::string output = "";
    static unsigned int calls = 0;

    // Reset on first:
    if (calls == 0)
        output = "";

    // Update text:
    output += std::string(calls, ' ') + "+ " + this->getName();
    output += " (R: " + std::to_string(this->getNrOfRenderables()) + ")";
    output += '\n';
    /*for (uint32_t c = 0; c < this->getNrOfRenderables(); c++)
    {
       output += std::string(calls + 6, ' ') + this->getRenderable(c).first.get().getName() + " (" + std::to_string(this->getRenderable(c).first.get().getId()) + ")";
       if (this->getRenderable(c).second.get() != Ov::Renderable::empty)
          output += " and " + this->getRenderable(c).second.get().getName() + " (" + std::to_string(this->getRenderable(c).second.get().getId()) + ")";
       output += '\n';
    }*/

    // Recursion:
    calls++;
    for (auto& i : reserved->children)
        i.get().getTreeAsString();
    calls--;

    // Done:
    if (calls == 0)
        return output;
    else
        return "";
}

#pragma endregion