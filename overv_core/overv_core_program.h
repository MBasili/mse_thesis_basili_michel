/**
 * @file	overv_base_program.h
 * @brief	Basic generic program properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


/**
 * @brief Class for modeling a generic program.
 */
class OV_API Program : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

    // Special values:
	static Program empty;


	/**
	 * @brief Types of program.
	 */
	enum class Type : uint32_t
	{
		none,

		// Programs:
		program,

		// Terminator:
		last
	};

	// Const/dest:
	Program();
	Program(Program&& other) noexcept;
	Program(Program const&) = delete;
	virtual ~Program();

	// Get/set:
	const Type getType() const;
	const uint32_t getNrOfShaders() const;
	const Ov::Shader& getShader(uint32_t id) const;

	// Building:
	virtual bool build(std::initializer_list<std::reference_wrapper<Ov::Shader>> args);

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Program(const std::string& name);
};