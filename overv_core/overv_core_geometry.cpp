/**
 * @file	overv_base_geometry.cpp
 * @brief	Basic generic geometry properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Geometry Ov::Geometry::empty("[empty]");   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Geometry reserved structure.
 */
struct Ov::Geometry::Reserved
{  
   // This is data stored within the class:
   std::vector<Ov::Geometry::VertexData> vertex;
   std::vector<Ov::Geometry::FaceData> face;	  

   /**
    * Constructor. 
    */
   Reserved()
   {}
};


////////////////////////////
// BODY OF CLASS Geometry //
////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Geometry::Geometry() : reserved(std::make_unique<Ov::Geometry::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OV_API Ov::Geometry::Geometry(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<Ov::Geometry::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Geometry::Geometry(Geometry&& other) : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Geometry::~Geometry()
{
	OV_LOG_DETAIL("[-]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear the content of this geometry.
 */
void OV_API Ov::Geometry::reset()
{
	reserved->vertex.clear();
	reserved->face.clear();
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OV_API Ov::Geometry::render(uint32_t value, void* data) const
{
	OV_LOG_DEBUG("Geometry rendering method called");
	return false;
}

#pragma endregion

#pragma region VertexOperation

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a vertex to the geometry.
 * @param v new vertex
 */
void OV_API Ov::Geometry::addVertex(const VertexData& v)
{
	reserved->vertex.push_back(v);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the data of the specified vertex.
 * @param i vertex index
 * @return vertex data
 */
Ov::Geometry::VertexData OV_API Ov::Geometry::getVertex(uint32_t i) const
{
	if (i <= reserved->vertex.size())
		return reserved->vertex[i];

	OV_LOG_ERROR("Invalid index");
	Ov::Geometry::VertexData empty;
	return empty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return a pointer to the vertex data array.
 * @return vertex data pointer
 */
Ov::Geometry::VertexData OV_API* Ov::Geometry::getVertexDataPtr() const
{
	return reserved->vertex.data();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the total number of vertices of the mesh.
 * @return total number of vertices
 */
uint32_t OV_API Ov::Geometry::getNrOfVertices() const
{
	return (uint32_t)reserved->vertex.size();
}

#pragma endregion

#pragma region FaceOperation

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a face to the geometry.
 * @param f new face
 */
void OV_API Ov::Geometry::addFace(const FaceData& f)
{
	reserved->face.push_back(f);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the data of the specified face.
 * @param i face index
 * @return face data
 */
Ov::Geometry::FaceData OV_API Ov::Geometry::getFace(uint32_t i) const
{
	if (i <= reserved->face.size())
		return reserved->face[i];

	OV_LOG_ERROR("Invalid index");
	Ov::Geometry::FaceData empty;
	return empty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return a pointer to the face data array.
 * @return face data pointer
 */
Ov::Geometry::FaceData OV_API* Ov::Geometry::getFaceDataPtr() const
{
	return reserved->face.data();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the total number of faces of the geometry.
 * @return total number of faces
 */
uint32_t OV_API Ov::Geometry::getNrOfFaces() const
{
	return (uint32_t)reserved->face.size();
}

#pragma endregion

#pragma region Ovo

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of this geometry into the serializer.
 * @param serial serializer
 * @return TF
 */
bool OV_API Ov::Geometry::saveChunk(Ov::Serializer& serial) const
{
	OV_LOG_DEBUG("Serializing geometry...");

	// Chunk header:
	uint8_t chunkId = static_cast<uint8_t>(ChunkId::geometry);
	uint32_t chunkSize = sizeof(uint32_t);
	chunkSize += static_cast<uint32_t>(strlen(this->getName().c_str())) + 1;
	chunkSize += sizeof(uint32_t) * 2;
	uint32_t nrOfVertices = static_cast<uint32_t>(reserved->vertex.size());
	chunkSize += nrOfVertices * sizeof(VertexData);
	uint32_t nrOfFaces = static_cast<uint32_t>(reserved->face.size());
	chunkSize += nrOfFaces * sizeof(FaceData);

	serial.serialize(&chunkId, sizeof(uint8_t));
	serial.serialize(&chunkSize, sizeof(uint32_t));

	// Properties:   
	serial.serialize(this->getId());
	serial.serialize(this->getName());
	serial.serialize(nrOfVertices);
	serial.serialize(nrOfFaces);

	// Data:
	serial.serialize(reserved->vertex.data(), nrOfVertices * sizeof(VertexData));
	serial.serialize(reserved->face.data(), nrOfFaces * sizeof(FaceData));

	// Done:   
	return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return TF
 */
uint32_t OV_API Ov::Geometry::loadChunk(Ov::Serializer& serial, void* data)
{
	// Chunk header
	uint8_t chunkId;
	serial.deserialize(&chunkId, sizeof(uint8_t));
	if (chunkId != static_cast<uint8_t>(Ovo::ChunkId::geometry))
	{
		OV_LOG_ERROR("Invalid chunk ID found");
		return 0;
	}
	uint32_t chunkSize;
	serial.deserialize(&chunkSize, sizeof(uint32_t));

	// Properties:      
	uint32_t id;
	serial.deserialize(id);
	std::string name;
	serial.deserialize(name);
	this->setName(name);
	uint32_t nrOfVertices;
	serial.deserialize(nrOfVertices);
	uint32_t nrOfFaces;
	serial.deserialize(nrOfFaces);

	// Data:
	reserved->vertex.resize(nrOfVertices);
	serial.deserialize(reserved->vertex.data(), nrOfVertices * sizeof(VertexData));
	reserved->face.resize(nrOfFaces);
	serial.deserialize(reserved->face.data(), nrOfFaces * sizeof(FaceData));

	// Synch:
	if (!upload())
		return 0;

	// Done:      
	return id;
}

#pragma endregion

#pragma region SyncData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sync data to the device.
 * @return TF
 */
bool OV_API Ov::Geometry::upload()
{
	OV_LOG_ERROR("This method must be implemented in a derived class");
	return false;
}

#pragma endregion