/**
 * @file	overv_base_shader.h
 * @brief	Basic generic shader properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


/**
 * @brief Class for modeling a generic shader.
 */
class OV_API Shader : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

    // Special values:
	static Shader empty;


	/**
	 * @brief Types of shader.
	 */
	enum class Type : uint32_t
	{
		none,

		// Rasterization:
		vertex,
		tessellation_ctrl,
		tessellation_eval,
		geometry,
		fragment,
		compute,

		// RayTracing:
		ray_generation,
		miss,
		callable,
		closes_hit,
		any_hit,
		intersection,

		// Terminator:
		last
	};


	// Const/dest:
	Shader();
	Shader(Shader&& other) noexcept;
	Shader(Shader const&) = delete;
	virtual ~Shader();

	// Get/set:
	const Type getType() const;
	const std::string& getCode() const;

	// Accessing data:
	virtual bool load(Type kind, const std::string& code);


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Shader(const std::string& name);
};