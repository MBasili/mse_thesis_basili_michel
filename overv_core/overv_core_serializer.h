/**
 * @file	overv_base_serializer.h
 * @brief	Basic generic serializer
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief Class for serializing data to memory.
  */
class OV_API Serializer
{
//////////
public: //
//////////

   // Special values:
	static Serializer empty;

	// Const/dest:
	Serializer();
	Serializer(const Serializer& other);
	Serializer(const void* rawData, uint64_t nrOfBytes);
	virtual ~Serializer();

	// Operators:
	void operator=(const Serializer& other);

	// Get/set:
	void* getData() const;
	void* getDataAtCurPos() const;
	uint64_t getNrOfBytes() const;

	// Serialization:
	void clear();
	void reset();
	bool serialize(const std::string& text);
	bool serialize(const uint8_t& byte);
	bool serialize(const bool& _bool);
	bool serialize(const uint32_t& uint);
	bool serialize(const float& _float);
	bool serialize(const glm::vec3& vec);
	bool serialize(const glm::vec4& vec);
	bool serialize(const glm::mat4& mat);
	bool serialize(const void* rawData, uint64_t nrOfBytes);
	bool deserialize(std::string& text);
	bool deserialize(uint8_t& byte);
	bool deserialize(bool& _bool);
	bool deserialize(uint32_t& uint);
	bool deserialize(float& _float);
	bool deserialize(glm::vec3& vec);
	bool deserialize(glm::vec4& vec);
	bool deserialize(glm::mat4& mat);
	bool deserialize(void* rawData, uint64_t nrOfBytes);


/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;
};