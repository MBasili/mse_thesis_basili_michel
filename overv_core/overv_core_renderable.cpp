/**
 * @file	overv_base_renderable.cpp
 * @brief	Basic generic renderable object properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Renderable Ov::Renderable::empty("[empty]");      


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Renderable reserved structure.
 */
struct Ov::Renderable::Reserved
{     
   /**
    * Constructor. 
    */
   Reserved() 
   {}
};


//////////////////////////////
// BODY OF CLASS Renderable //
//////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Renderable::Renderable() : reserved(std::make_unique<Ov::Renderable::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Renderable::Renderable(const std::string& name) : Ov::Object(name), reserved(std::make_unique<Ov::Renderable::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Renderable::Renderable(Renderable&& other) noexcept : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Renderable::~Renderable()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Empty rendering method. Bad sign if you can this.
 * @param value generic value (optional)
 * @param data generic pointer to any kind of data (optional)
 * @return TF
 */
bool OV_API Ov::Renderable::render(uint32_t value, void* data) const
{
    OV_LOG_ERROR("Emtpy rendering method called");
    return false;
}

#pragma endregion