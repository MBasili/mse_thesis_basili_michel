/**
 * @file	overv_base_shader.cpp
 * @brief	Basic generic shader properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

   
////////////
// STATIC //
////////////

// Special values:
Ov::Shader Ov::Shader::empty("[empty]");   
   

/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Shader reserved structure.
 */
struct Ov::Shader::Reserved
{     
   Type type;           ///< Shader type
   std::string code;    ///< Shader source code


   /**
    * Constructor. 
    */
   Reserved() : type{Ov::Shader::Type::none}
   {}
};


//////////////////////////
// BODY OF CLASS Shader //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Shader::Shader() : reserved(std::make_unique<Ov::Shader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OV_API Ov::Shader::Shader(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<Ov::Shader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Shader::Shader(Shader&& other) noexcept : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Shader::~Shader()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get type.
 * @return shader type
 */
const Ov::Shader::Type OV_API Ov::Shader::getType() const
{
    return reserved->type;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get source code.
 * @return source code
 */
const std::string OV_API& Ov::Shader::getCode() const
{
    return reserved->code;
}

#pragma endregion

#pragma region AccessingData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Use the specified string as source code for the shader.
 */
bool OV_API Ov::Shader::load(Type type, const std::string& code)
{
    // Safety net:
    if (code.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Pass params:
    reserved->type = type;
    reserved->code = code;

    // Done:
    return true;
}

#pragma endregion