/**
 * @file	overv_base_camera.h
 * @brief	Basic camera properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


/**
 * @brief Class for modelling a generic camera.
 */
class OV_API Camera : public Ov::Node
{
//////////
public: //
//////////

    // Special values:
	static Camera empty;

	// Const/dest:
	Camera();
	Camera(Camera&& other);
	Camera(Camera const&) = delete;
	virtual ~Camera();

	// Projection matrix:
	void setProjMatrix(const glm::mat4& matrix);
	const glm::mat4& getProjMatrix() const;


/////////////
protected: //
/////////////   

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Camera(const std::string& name);
};


