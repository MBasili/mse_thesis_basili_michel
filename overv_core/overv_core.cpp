/**
 * @file	overv_core.cpp
 * @brief	OverVision 3D base generic architecture
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main include:
#include "overv_core.h"

// FreeImage:
#include <FreeImage.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Core class reserved structure.
 */
struct Ov::Core::Reserved
{
	Ov::Config config;      ///< Current configuration
};


////////////////////////
// BODY OF CLASS Base //
////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Core::Core() : reserved(std::make_unique<Ov::Core::Reserved>())
{
	OV_LOG_DETAIL("[+]");
	FreeImage_Initialise();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Core::~Core()
{
	OV_LOG_DETAIL("[-]");
	FreeImage_DeInitialise();
}

#pragma endregion

#pragma region Singleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 */
Ov::Core OV_API& Ov::Core::getInstance()
{
	static Core instance;
	return instance;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get current configuration.
 * @return current configuration
 */
Ov::Config OV_API& Ov::Core::getConfig()
{
	return reserved->config;
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Init internal components.
 * @param config optional configuration
 * @return TF
 */
bool OV_API Ov::Core::init(const Ov::Config& config)
{
	reserved->config = config;

	// Done:
	return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free internal components.
 * @return TF
 */
bool OV_API Ov::Core::free()
{
	// Since the context is about to be released, unload all objects that are still allocated:
	Ov::Managed::forceRelease();

	// Done:
	return true;
}

#pragma endregion