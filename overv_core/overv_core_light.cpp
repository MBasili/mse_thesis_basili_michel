/**
 * @file	overv_core_light.cpp
 * @brief	Basic generic light properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_core.h"

// Magic enum
#include "magic_enum.hpp"
   
////////////
// STATIC //
////////////

// Special values:
Ov::Light Ov::Light::empty("[empty]");   


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Light reserved structure.
 */
struct Ov::Light::Reserved
{  
	glm::vec3 color;        ///< Light color


	/**
	 * Constructor.
	 */
	Reserved() : color{ 1.0f, 1.0f, 1.0f }
	{}
};


/////////////////////////
// BODY OF CLASS Light //
/////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OV_API Ov::Light::Light() : reserved(std::make_unique<Ov::Light::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OV_API Ov::Light::Light(const std::string& name) : Ov::Renderable(name),
reserved(std::make_unique<Ov::Light::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OV_API Ov::Light::Light(Light&& other) noexcept: Ov::Renderable(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OV_API Ov::Light::~Light()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the color of the light.
 * @param color light color
 */
void OV_API Ov::Light::setColor(const glm::vec3& color)
{
    reserved->color = color;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the light color.
 * @return light color
 */
const glm::vec3 OV_API& Ov::Light::getColor() const
{
    return reserved->color;
}

#pragma endregion

#pragma region Ovo

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of this light into the serializer.
 * @param serial serializer
 * @return TF
 */
bool OV_API Ov::Light::saveChunk(Ov::Serializer& serial) const
{
    OV_LOG_DEBUG("Serializing light...");

    // Chunk header:
    uint8_t chunkId = static_cast<uint8_t>(ChunkId::light);
    uint32_t chunkSize = sizeof(uint32_t);
    chunkSize += static_cast<uint32_t>(strlen(this->getName().c_str())) + 1;
    chunkSize += sizeof(glm::vec3);

    serial.serialize(&chunkId, sizeof(uint8_t));
    serial.serialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    serial.serialize(this->getId());
    serial.serialize(this->getName());
    serial.serialize(this->getColor());
    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return id of the light loaded
 */
uint32_t OV_API Ov::Light::loadChunk(Ov::Serializer& serial, void* data)
{
    // Chunk header
    uint8_t chunkId;
    serial.deserialize(&chunkId, sizeof(uint8_t));
    if (chunkId != static_cast<uint8_t>(Ovo::ChunkId::light))
    {
        OV_LOG_ERROR("Invalid chunk ID found");
        return 0;
    }
    uint32_t chunkSize;
    serial.deserialize(&chunkSize, sizeof(uint32_t));

    // Properties:      
    uint32_t id;
    serial.deserialize(id);

    std::string name;
    serial.deserialize(name);
    this->setName(name);

    glm::vec3 color;
    serial.deserialize(color);
    this->setColor(color);

    // Done:      
    return id;
}

#pragma endregion