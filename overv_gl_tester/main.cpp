/**
 * @file		main.cpp
 * @brief	Engine usage example
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main engine header:
#include "overv_gl.h"

// C/C++:
#include <iostream>
#include <chrono>
#include <fstream>

// GLM:
#include <glm/gtx/euler_angles.hpp>

//////////   
// VARS //
//////////

// General status:
glm::vec3 cameraPosition;
std::ofstream reportTestFile;
uint32_t currentScene = 1;
float spinSpeed = 30.0f;
bool isGUIEnabled = true;
bool isAnimationOn = false;

// Pipelines:
OvGL::PipelineRayTracing rtPipe;
OvGL::PipelineLighting lighting;
OvGL::PipelineFullscreen2D full2dPipe;


///////////////
// CALLBACKS //
///////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Mouse cursor callback.
 * @param mouseX updated mouse X coordinate
 * @param mouseY updated mouse Y coordinate
 */
void mouseCursorCallback(double mouseX, double mouseY)
{}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Mouse button callback.
 * @param button mouse button id
 * @param action action
 * @param mods modifiers
 */
void mouseButtonCallback(int button, int action, int mods)
{}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Mouse scroll callback.
 * @param scrollX updated mouse scroll X coordinate
 * @param scrollY updated mouse scroll Y coordinate
 */
void mouseScrollCallback(double scrollX, double scrollY)
{}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method keyboard callback.
 * @param key The keyboard key that was pressed or released.
 * @param scancode The system-specific scancode of the key.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
void keyboardCallback(int key, int scancode, int action, int mods)
{
	// A few useful defines (https://www.glfw.org/docs/3.3/group__keys.html):
#define GLFW_RELEASE   0
#define GLFW_PRESS     1	

#define GLFW_KEY_1	   49
#define GLFW_KEY_2	   50
#define GLFW_KEY_3	   51
#define GLFW_KEY_4	   52
#define GLFW_KEY_5	   53
#define GLFW_KEY_6	   54
#define GLFW_KEY_7	   55
#define GLFW_KEY_8	   56
#define GLFW_KEY_G	   71
#define GLFW_KEY_O     79
#define GLFW_KEY_P     80

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_G:
			isGUIEnabled = !isGUIEnabled;
			break;
		case GLFW_KEY_1:
			currentScene = 1;
			break;
		case GLFW_KEY_2:
			currentScene = 2;
			break;
		case GLFW_KEY_3:
			currentScene = 3;
			break;
		case GLFW_KEY_4:
			currentScene = 4;
			break;
		case GLFW_KEY_5:
			currentScene = 5;
			break;
		case GLFW_KEY_6:
			currentScene = 6;
			break;
		case GLFW_KEY_7:
			currentScene = 7;
			break;
		case GLFW_KEY_8:
			currentScene = 8;
			break;
		case GLFW_KEY_O:
			isAnimationOn = !isAnimationOn;
			break;
		}
	}
	else if (action == GLFW_RELEASE)
	{
		switch (key)
		{

		}
	}
}


//////////
// MAIN //
//////////

/**
 * Application entry point.
 * @param argc number of command-line arguments passed
 * @param argv array containing up to argc passed arguments
 * @return error code (0 on success, error code otherwise)
 */
int main(int argc, char* argv[])
{
	//////////
	// Config:
	Ov::Config config;
	config.setWindowTitle("OvGL Test Ray tracing");
	// window dimensions need to be divisible for workgroup size of compute shaders(8)
	// and number of section (4). so divisibe for (24)
	config.setWindowSizeX(800);
	config.setWindowSizeY(800);
	config.setNrOfAASamples(1);

	///////////////
	// Init engine:
	std::reference_wrapper<OvGL::Engine> eng = OvGL::Engine::getInstance();
	eng.get().init(config);

	// Register callbacks:
	eng.get().setMouseCursorCallback(mouseCursorCallback);
	eng.get().setMouseButtonCallback(mouseButtonCallback);
	eng.get().setMouseScrollCallback(mouseScrollCallback);
	eng.get().setKeyboardCallback(keyboardCallback);

	{
		//////////////////
		// Loading scenes:   
		OvGL::Container container;
		OvGL::Ovo scenes;

		glm::mat4 rootMatrix = glm::mat4(1.0f);

		// load scens
		std::reference_wrapper<Ov::Node> root_scene1 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "first_scene_comparison.ovo", container);
		root_scene1.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene2 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "second_scene_comparison.ovo", container);
		root_scene2.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene3 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "third_scene_comparison.ovo", container);
		root_scene3.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene4 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "fourth_scene_comparison.ovo", container);
		root_scene4.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene5 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "geometries_sharing_scene.ovo", container);
		root_scene5.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene6 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "multiple_reflection_scene.ovo", container);
		root_scene6.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene7 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "anti_aliasing.ovo", container);
		root_scene7.get().setMatrix(rootMatrix);
		std::reference_wrapper<Ov::Node> root_scene8 = scenes.load(OvGL::Engine::modelsSourceFolder +
			"\\" + "presentation_final_scene.ovo", container);
		root_scene8.get().setMatrix(rootMatrix);

		// Animated geometries of scene 5 and 8
		std::list<Ov::Node>& nodes = container.getNodeList();

		std::reference_wrapper<Ov::Node> rotate1 = Ov::Node::empty;
		std::reference_wrapper<Ov::Node> rotate2 = Ov::Node::empty;
		std::reference_wrapper<Ov::Node> rotate3 = Ov::Node::empty;
		std::reference_wrapper<Ov::Node> rotate4 = Ov::Node::empty;

		for (auto& node : nodes) {
			if (node.getName().find("rotate1_Node") != std::string::npos)
				rotate1 = node;
			if (node.getName().find("rotate2_Node") != std::string::npos)
				rotate2 = node;
			if (node.getName().find("rotate3_Node") != std::string::npos)
				rotate3 = node;
			if (node.getName().find("rotate4_Node") != std::string::npos)
				rotate4 = node;
		}

		std::reference_wrapper<Ov::Node> root = root_scene1;
		
		// Create camera
		OvGL::Camera camera;
		cameraPosition = glm::vec3(0, 50, 170);
		camera.setProjMatrix(glm::perspective(glm::radians(45.0f),
			eng.get().getWidth() / (float)eng.get().getHeight(), 1.0f, 1000.0f));
		camera.setMatrix(glm::translate(glm::mat4(1.0f), cameraPosition));

		// Rendering elements setUp:
		OvGL::List list;

		// Gui pipeline
		OvGL::GUIPipeline guiPipeline;
		guiPipeline.create();
		guiPipeline.addDrawGUIElement(list.getId(), list);
		guiPipeline.addDrawGUIElement(rtPipe.getId(), rtPipe);
		guiPipeline.addDrawGUIElement(lighting.getId(), lighting);

		// RT pipeline
		rtPipe.setNrOfRecursion(0);

		// Test measurement
		std::chrono::high_resolution_clock timer;
		auto lastFrameTime = timer.now();
		float deltaTime = 0.0f;
		float processingTime = 0.0f;
		float renderingTime = 0.0f;
		float frameTime = 0.0f;

		reportTestFile.open("scene_measurements_opengl.csv");
		reportTestFile << "Processing; Rendering; Frame\n";

		/////////////
		// Main loop:
		std::cout << "Entering main loop..." << std::endl;
		while (eng.get().processEvents())
		{
			// GUI enables
			if(isGUIEnabled)
				guiPipeline.render(0);

			// Select scene
			switch (currentScene)
			{
			case 1:
				root = root_scene1;
				break;
			case 2:
				root = root_scene2;
				break;
			case 3:
				root = root_scene3;
				break;
			case 4:
				root = root_scene4;
				break;
			case 5:
				root = root_scene5;
				if (isAnimationOn) {
					rotate1.get().setMatrix(glm::rotate(rotate1.get().getMatrix(),
						glm::radians(-spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
					rotate2.get().setMatrix(glm::rotate(rotate2.get().getMatrix(),
						glm::radians(spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
				}
				break;
			case 6:
				root = root_scene6;
				break;
			case 7:
				root = root_scene7;
				break;
			case 8:
				root = root_scene8;
				if (isAnimationOn) {
					rotate3.get().setMatrix(glm::rotate(rotate3.get().getMatrix(),
						glm::radians(-spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
					rotate4.get().setMatrix(glm::rotate(rotate4.get().getMatrix(),
						glm::radians(spinSpeed * deltaTime / 1000.0f), { 1.0f,0.0f,0.0f }));
				}
				break;
			}

			// Start time to measure scene preparation time
			auto startTime = timer.now();

			// Update list:
			list.reset();
			list.pass(root);
			list.render(camera.getMatrix(), camera.getProjMatrix());

			// Main rendering:
			eng.get().clear();
			rtPipe.migrate(list);

			// Calculate the time taken
			auto endTime = timer.now();
			deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count() / 1000.0f;
			processingTime = (deltaTime / 1000.0f);


			// Start time to measure the time required to perform ray tracing and calculate the lighting model.
			startTime = timer.now();

			// Only ray tracing
			rtPipe.render(camera, list);
			lighting.render(rtPipe, list);
			full2dPipe.render(lighting.getOutputBuffer(), list);

			// Calculate the time taken
			endTime = timer.now();
			deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count() / 1000.0f;
			renderingTime = (deltaTime / 1000.0f);
			

			// GUI enables
			if (isGUIEnabled)
				guiPipeline.render(1);

			// Swap framebuffer
			eng.get().swap();


			// Compute frame time
			auto curTime = timer.now();
			// nano seconds
			deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(curTime - lastFrameTime).count() / 1000.0f;
			// seconds
			frameTime = (deltaTime / 1000.0f);
			lastFrameTime = curTime;

			// seconds
			reportTestFile << processingTime << ";" << renderingTime << ";" << frameTime << "\n";
		}
		std::cout << "Leaving main loop..." << std::endl;

		reportTestFile.close();
	}
	// Release engine:
	eng.get().free();

	// Done:
	std::cout << "[application terminated]" << std::endl;
	return 0;
}