/**
 * @file	overv_gl_program.h
 * @brief	OpenGL generic program properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


/**
 * @brief Class for modeling an OpenGL program.
 */
class OVGL_API Program final : public Ov::Program
{
//////////
public: //
//////////

	// Special values:
	static Program empty;

	// Const/dest:
	Program();
	Program(Program&& other) noexcept;
	Program(Program const&) = delete;
	virtual ~Program();

	// Managed:
	bool init() override;
	bool free() override;

	// Building:
	bool build(std::initializer_list<std::reference_wrapper<Ov::Shader>> args);

	// Get/set:
	int32_t getShaderID();
	uint32_t getNrOfShaders() const;
	const Ov::Shader& getShader(uint32_t id) const;
	bool setFloat(const std::string& name, float value);
	bool setInt(const std::string& name, int32_t value);
	bool setUInt(const std::string& name, uint32_t value);
	bool setUInt64(const std::string& name, uint64_t value);
	bool setVec3(const std::string& name, const glm::vec3& value);
	bool setVec4(const std::string& name, const glm::vec4& value);
	bool setMat3(const std::string& name, const glm::mat3& value);
	bool setMat4(const std::string& name, const glm::mat4& value);
	bool setUInt64Array(const std::string& name, const uint64_t* data, uint32_t count);

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;
	static void reset();

	// Compute-only:
	bool compute(uint32_t sizeX, uint32_t sizeY = 1, uint32_t sizeZ = 1) const;
	bool wait() const;

	// Cache:
	static OvGL::Program& getCached();

///////////
private: //
///////////

	// Cache:
	static std::reference_wrapper<OvGL::Program> cache;

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Get/set:
	int32_t getParamLocation(const std::string& name);
	int32_t getParamLocationARB(const std::string& name);

	// Const/dest:
	Program(const std::string& name);
};