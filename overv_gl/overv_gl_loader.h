/**
 * @file	overv_gl_loader.h
 * @brief	Base 3D model loading functionality
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


/**
 * @brief Base 3D model loader.
 */
class OVGL_API Loader final : public Ov::Object
{
//////////
public: //
//////////	   

	// Const/Dest:
	Loader();
	Loader(Loader&& other) noexcept;
	Loader(Loader const&) = delete;
	~Loader();

	// Operators:
	Loader& operator=(const Loader&) = delete;
	Loader& operator=(Loader&&) = delete;

	// Get/set:
	bool tooManyMaterials();

	// Loading methods:
	bool load(const std::string& filename, unsigned int pFlags);
	bool loadLight(void* light, OvGL::Light& l, Ov::Node& node);
	bool loadGeometry(void* mesh, OvGL::Geometry& g);
	bool loadMaterial(void* mesh, OvGL::Material& m);
	bool loadTexture2D(std::string imageName, Ov::Texture::Type type, OvGL::Texture2D& t);
	Ov::Node& loadScene(OvGL::Container& container, void* current = nullptr);

	// Just tmp debug:
	void showParamsUsed(unsigned int pFlags);

///////////
private: //
///////////	

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Loader(const std::string& name);
};