/**
 * @file	overv_gl_ovo.h
 * @brief	OVO import/export functions
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */
#pragma once


/**
 * @brief 3D OVO manager.
 */
class OVGL_API Ovo : public Ov::Ovo
{
/////////////
protected: //
/////////////	

	// Derived-type specific factories:
	void* createByType(ChunkId type) const;
};