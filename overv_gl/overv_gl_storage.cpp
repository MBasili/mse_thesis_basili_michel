/**
 * @file	overv_gl_storage.cpp
 * @brief	OpenGL GPU storage
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 *          Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>

// Magic enum
#include <magic_enum.hpp>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Storage class reserved structure.
 */
struct OvGL::Storage::Reserved
{
    // Buffers:
    OvGL::UniformBufferObj global[nrOfRedundantBuffers];                                            ///< Global uniform buffer on GPU
    OvGL::UniformBufferObj material[nrOfRedundantBuffers];                                          ///< Materials buffer on GPU
    OvGL::UniformBufferObj light[nrOfRedundantBuffers];                                             ///< RTObjDesc buffer on GPU
   
    // Dirty states:
    std::vector<std::pair<std::reference_wrapper<const Ov::Renderable>, uint32_t>> toBeUpdated;     ///< List of objects to be updated

    // Counters:
    uint32_t curBuffer;                                                                             ///< Current processing buffers.
    uint32_t nrOfInstances[nrOfTypes];                                                              ///< Number of instances contained by the buffers on the GPU.
    uint32_t lastBufferUpdate;                                                                      ///< Last processed buffers.

    /**
     * Constructor
     */
    Reserved() : curBuffer{ 0 },
        lastBufferUpdate{ std::numeric_limits<uint32_t>::max() }
    {
        for (uint32_t c = 0; c < nrOfTypes; c++)
            nrOfInstances[c] = 0;
    }
};


///////////////////////////
// BODY OF CLASS Storage //
///////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Storage::Storage() : reserved(std::make_unique<OvGL::Storage::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Storage::~Storage()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Singleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 */
OvGL::Storage OVGL_API& OvGL::Storage::getInstance()
{
    static Storage instance;
    return instance;
}

#pragma endregion

#pragma region Init/free

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization method.
 */
bool OVGL_API OvGL::Storage::init()
{
    // Init buffers:
    for (uint32_t c = 0; c < nrOfRedundantBuffers; c++)
        reserved->global[c].create(sizeof(OvGL::Storage::GlobalUniformData));

    // Init buffer:
    for (uint32_t c = 0; c < nrOfRedundantBuffers; c++)
        reserved->material[c].create(OvGL::Material::maxNrOfMaterials * sizeof(OvGL::Storage::MaterialData));

    // Init buffer:
    for (uint32_t c = 0; c < nrOfRedundantBuffers; c++)
        reserved->light[c].create(OvGL::Light::maxNrOfLights * sizeof(OvGL::Storage::LightData));

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization method.
 */
bool OVGL_API OvGL::Storage::free()
{
    // Done:
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get global buffer object.
 * @return global buffer object
 */
OvGL::UniformBufferObj OVGL_API& OvGL::Storage::getGlobalUniformBuffer()
{
    return reserved->global[reserved->curBuffer];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get pointer to global mapped memory.
 * @return pointer to global mapped memory
 */
OvGL::Storage::GlobalUniformData OVGL_API* OvGL::Storage::getGlobalUniform()
{
    return reinterpret_cast<OvGL::Storage::GlobalUniformData*>(reserved->global[reserved->curBuffer].getDataPtr());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get material buffer object.
 * @return material buffer object
 */
OvGL::UniformBufferObj OVGL_API& OvGL::Storage::getMaterialDataBuffer()
{
    return reserved->material[reserved->curBuffer];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get pointer to material mapped memory.
 * @param id material position
 * @return pointer to material mapped memory or nullptr if error
 */
OvGL::Storage::MaterialData OVGL_API* OvGL::Storage::getMaterialData(uint32_t id)
{
    // Safety net:
    if (id >= OvGL::Material::maxNrOfMaterials)
    {
        OV_LOG_ERROR("Invalid params");
        return nullptr;
    }

    return reinterpret_cast<OvGL::Storage::MaterialData*>(((uint8_t*)reserved->material[reserved->curBuffer].getDataPtr()) + id * sizeof(OvGL::Storage::MaterialData));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get light buffer object.
 * @return light buffer object
 */
OvGL::UniformBufferObj OVGL_API& OvGL::Storage::getLightBuffer()
{
    return reserved->light[reserved->curBuffer];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get pointer to light mapped memory.
 * @param id light position
 * @return pointer to light mapped memory or nullptr if error
 */
OvGL::Storage::LightData OVGL_API* OvGL::Storage::getLightData(uint32_t id)
{
    // Safety net:
    if (id >= OvGL::Light::maxNrOfLights)
    {
        OV_LOG_ERROR("Invalid params");
        return nullptr;
    }

    return reinterpret_cast<OvGL::Storage::LightData*>(((uint8_t*)reserved->light[reserved->curBuffer].getDataPtr()) + id * sizeof(OvGL::Storage::LightData));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set number of used instances.
 * @param nrOfIstances nr. of currently used instances
 * @param type type of instance to update
 */
void OVGL_API OvGL::Storage::setNrOfInstances(uint32_t nrOfInstances, Ov::List::Type type)
{
    reserved->nrOfInstances[static_cast<uint32_t>(type)] = nrOfInstances;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of used instances.
 * @param type type of instances
 * @return nr. of currently used instances
 */
uint32_t OVGL_API OvGL::Storage::getNrOfInstances(Ov::List::Type type) const
{
    return reserved->nrOfInstances[static_cast<uint32_t>(type)];
}

#pragma endregion

#pragma region Update

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a renderable to the list of objects to update.
 * @param renderable object to update
 * @return TF
 */
bool OVGL_API OvGL::Storage::addToUpdateList(const Ov::Renderable& renderable)
{
    // Safety net:
    if (renderable == Ov::Renderable::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    const OvGL::Material& mat = dynamic_cast<const OvGL::Material&>(renderable);

    // Alread in the list?
    for (auto& c : reserved->toBeUpdated)
        if (c.first.get() == renderable)
            return false;

    // Store it:
    reserved->toBeUpdated.push_back(std::make_pair(std::reference_wrapper<const Ov::Renderable>(renderable), OvGL::Storage::nrOfRedundantBuffers));

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Uses the next redundat buffer from now on.
 * @param TF
 */
bool OVGL_API OvGL::Storage::useNext()
{
    // Use next redundant buffers:
    uint32_t prevBuffer = reserved->curBuffer;
    reserved->curBuffer++;
    if (reserved->curBuffer >= nrOfRedundantBuffers)
        reserved->curBuffer = 0;

    // Release lock on old buffers:
    reserved->global[prevBuffer].unlock();
    reserved->material[prevBuffer].unlock();
    reserved->light[prevBuffer].unlock();

    // Acquire new lock and render buffers:
    reserved->global[reserved->curBuffer].lock();
    reserved->material[reserved->curBuffer].lock();
    reserved->light[reserved->curBuffer].lock();

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Refreshing and committing storage.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Storage::render(uint32_t value, void* data, Ov::List::Type type) const
{
    // Commit pending updates:   
    if (nrOfRedundantBuffers == 1 || reserved->curBuffer != reserved->lastBufferUpdate)
    {
        reserved->lastBufferUpdate = reserved->curBuffer;

        // Updated and decrease counter:      
        auto c = std::begin(reserved->toBeUpdated);
        while (c != std::end(reserved->toBeUpdated))
        {
            // Render and decrease count:        
            c->first.get().render(static_cast<uint32_t>(Ov::Object::Flag::upload));
            c->second--;

            if (c->second == 0)
                c = reserved->toBeUpdated.erase(c);
            else
                c++;
        }
    }

    // Bind all storage buffers as current:
    uint32_t curBuffer = reserved->curBuffer % nrOfRedundantBuffers;

    // Render buffers:   
    reserved->global[curBuffer].render(static_cast<uint32_t>(OvGL::Buffer::Binding::global));
    reserved->material[curBuffer].render(static_cast<uint32_t>(OvGL::Buffer::Binding::material));
    reserved->light[curBuffer].render(static_cast<uint32_t>(OvGL::Buffer::Binding::light));

    // Done:
    return true;
}

#pragma endregion

#pragma region Shader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds to the preprocessor of the Shader class the storage include.
 * @return TF
 */
bool OVGL_API OvGL::Storage::addShaderPreproc()
{
    //////////
    // Global:   
    std::string globalUniformPreproc = R"(
#ifndef OVGL_STORAGE_GLOBAL_UNIFORM
#define OVGL_STORAGE_GLOBAL_UNIFORM 
)";

    globalUniformPreproc += GlobalUniformData::getShaderStruct() + "\n";

    globalUniformPreproc += "layout(std140, binding = " +
        std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::global)) + ") uniform _GlobalUniform { GlobalUniformData globalUniform; };";
    globalUniformPreproc += R"(   
#endif
)";

    ////////////
    // Material:
    std::string materialPreproc = R"(   
#ifndef OVGL_STORAGE_MATERIAL
#define OVGL_STORAGE_MATERIAL
)";

    materialPreproc += MaterialData::getShaderStruct() + "\n";

    materialPreproc += "layout(std140, binding = " +
        std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::material)) +
        ") uniform _Material { MaterialData materials[" + std::to_string(OvGL::Material::maxNrOfMaterials) + "]; };";

    materialPreproc += R"(
#endif
)";

    /////////
    // Light:
    std::string lightPreproc = R"(   
#ifndef OVGL_STORAGE_LIGHT
#define OVGL_STORAGE_LIGHT 
)";

    lightPreproc += LightData::getShaderStruct() + "\n";

    lightPreproc += "layout(std140, binding = " +
        std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::light)) +
        ") uniform _Light { LightData lights[" + std::to_string(OvGL::Light::maxNrOfLights) + "]; };";
    lightPreproc += R"(
#endif
)";

    // Add storage binding to the preprocess operations
    return OvGL::Shader::preprocessor.set("include Storage", globalUniformPreproc + materialPreproc + lightPreproc);
}

#pragma endregion