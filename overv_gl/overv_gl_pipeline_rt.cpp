/**
 * @file	engine_pipeline_raytracing.cpp
 * @brief	A pipeline for doing simple ray tracing on GPU
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// GLM:
#include <glm/gtc/packing.hpp>  

// OGL:      
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// Miniball:
#include <Miniball.hpp>

// ImGui
#include "imgui.h"


/////////////
// SHADERS //
/////////////

#pragma region Shaders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default pipeline compute shader.
 */
static const std::string pipeline_cs = R"(

// This is the (hard-coded) workgroup size:
layout (local_size_x = 8, local_size_y = 8) in;

// Output framebuffers:
layout(binding = 0, r32ui) uniform uimage2D outputBuffer;

layout(binding = 1, offset = 0) uniform atomic_uint atomicCounterHitInfoData;
layout(binding = 2, offset = 0) uniform atomic_uint atomicCounterRaysGenerationData;

$include Storage$
$include RTPipeline$

/**
 * Ray casting function for tracing a (recursive) ray within the scene.
 * param ray primary ray
 */
uint rayCasting(in RayData rayToCast, ivec2 imageSize) 
{
    uint hitInfoIndex = UINT_MAX;

    HitInfoData hit;
    if (intersect(rayToCast, hit))    
    {
        hitInfoIndex = atomicCounterIncrement(atomicCounterHitInfoData);

        hitsInfoData[hitInfoIndex].triangleId = hit.triangleId;
        hitsInfoData[hitInfoIndex].distance = hit.distance;
        hitsInfoData[hitInfoIndex].uvCoords = hit.uvCoords;
        hitsInfoData[hitInfoIndex].barycentricCoords = hit.barycentricCoords;
        hitsInfoData[hitInfoIndex].shadowsHitsInfoId = UINT_MAX;
        hitsInfoData[hitInfoIndex].collisionPoint = hit.collisionPoint;
        hitsInfoData[hitInfoIndex].normal = hit.normal;
        hitsInfoData[hitInfoIndex].tangent = hit.tangent;
        hitsInfoData[hitInfoIndex].faceNormal = hit.faceNormal;
        hitsInfoData[hitInfoIndex].matId = hit.matId;
        hitsInfoData[hitInfoIndex].geomId = hit.geomId;
        hitsInfoData[hitInfoIndex].reflectionHitInfoId = UINT_MAX;
        hitsInfoData[hitInfoIndex].refractionHitInfoId = UINT_MAX;
        hitsInfoData[hitInfoIndex].inDir = vec4(rayToCast.dir, 0.0f);


        ////////////////////////////////////
        // Refraction and reflection rays //
        ////////////////////////////////////

        if(globalUniformRT.nrOfRecursion > 0) {
            
            uint rayOriginId = atomicCounterIncrement(atomicCounterRaysGenerationData);
            uint maxNrOfRayToCast = uint(pow(2, globalUniformRT.nrOfRecursion)); 

            uint sourceRaysStartIndex = rayOriginId * maxNrOfRayToCast;
            uint newRaysToCastStartIndex = sourceRaysStartIndex + imageSize.x * imageSize.y * maxNrOfRayToCast;

            raysGenerationDatas[sourceRaysStartIndex] = hitInfoIndex;
            for(uint i = 0; i < globalUniformRT.nrOfRecursion; i++) {
            
                uint nrOfSourceRays = uint(pow(2, i));
                for(uint j = 0; j < nrOfSourceRays; j++) {

                    // Reset pos
                    raysGenerationDatas[newRaysToCastStartIndex + (2 * j)] = UINT_MAX;
                    raysGenerationDatas[newRaysToCastStartIndex + (2 * j) + 1] = UINT_MAX;

                    if(raysGenerationDatas[sourceRaysStartIndex + j] != UINT_MAX) {
                    
                        vec3 faceNormal = hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].faceNormal.xyz;
                        float refractionIndex = materials[hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].matId].refractionIndex;
                        float ratioRefractionIndex = globalUniformRT.defaultRefractionIndex/refractionIndex;

                        ////////////////////
                        // Normal mapping //
                        ////////////////////
                        
                        vec3 vertexNormal =  hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].normal.xyz;
                        vec3 vertexTanget =  hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].tangent.xyz;
                        
                        // Correction => if tangent and normal are perpendicular dot product equal to 0.
                        vertexTanget = normalize(vertexTanget - dot(vertexTanget, vertexNormal) * vertexNormal);
                        
                        // inverse = transpose iff the matrix is orthogonal 
                        mat3 TBN;          

                        bool isInside = false;
                        if (dot(vertexNormal,
                             hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].faceNormal.xyz) < 0.0f) // Coll. from inside
                        {
                            isInside = true;
                            faceNormal = -faceNormal;
                            ratioRefractionIndex = refractionIndex/globalUniformRT.defaultRefractionIndex;

                            // BiTangent
                            vec3 B = cross(-vertexNormal, -vertexTanget);
                            TBN = mat3(-vertexTanget, B, -vertexNormal);
                        }
                        else
                        {
                            // BiTangent
                            vec3 B = cross(vertexNormal, vertexTanget);
                            TBN = mat3(vertexTanget, B, vertexNormal);
                        }  



                        // Normal
                        vec3 N = vertexNormal;
                        if(materials[hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].matId].normalTextureHandle > 0) {
                            vec3 normal = vec3(texture(sampler2D(materials[hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].matId].normalTextureHandle),
                                               hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].uvCoords).rg, 0.0f);
                            normal = vec3(normal.r, normal.g, sqrt(pow(1.0,2.0) - pow(normal.r,2.0) - pow(normal.g,2.0)));
                            normal = normal * 2.0 - 1.0;
                            N = normalize(TBN * normal);
                            if (isInside)
                            {
                                N = -N;
                            }  
                        }

                        ////////////////
                        // Reflection //
                        ////////////////

                        RayData reflectRay;
                        reflectRay.origin = hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].collisionPoint.xyz + faceNormal * 0.001f;
                        reflectRay.dir = normalize(reflect(hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].inDir.xyz, 
                                                   ((globalUniformRT.useNormalMapForReflection == true) ? N : vertexNormal)));
    
                        if (intersect(reflectRay, hit)) {

                            uint reflectionHitInfoIndex = atomicCounterIncrement(atomicCounterHitInfoData);
            
                            hitsInfoData[reflectionHitInfoIndex].triangleId = hit.triangleId;
                            hitsInfoData[reflectionHitInfoIndex].distance = hit.distance;
                            hitsInfoData[reflectionHitInfoIndex].uvCoords = hit.uvCoords;
                            hitsInfoData[reflectionHitInfoIndex].barycentricCoords = hit.barycentricCoords;
                            hitsInfoData[reflectionHitInfoIndex].shadowsHitsInfoId = UINT_MAX;
                            hitsInfoData[reflectionHitInfoIndex].collisionPoint = hit.collisionPoint;
                            hitsInfoData[reflectionHitInfoIndex].normal = hit.normal;
                            hitsInfoData[reflectionHitInfoIndex].tangent = hit.tangent;
                            hitsInfoData[reflectionHitInfoIndex].faceNormal = hit.faceNormal;
                            hitsInfoData[reflectionHitInfoIndex].matId = hit.matId;
                            hitsInfoData[reflectionHitInfoIndex].geomId = hit.geomId;
                            hitsInfoData[reflectionHitInfoIndex].reflectionHitInfoId = UINT_MAX;
                            hitsInfoData[reflectionHitInfoIndex].refractionHitInfoId = UINT_MAX;
                            hitsInfoData[reflectionHitInfoIndex].inDir = vec4(reflectRay.dir, 0.0f);

                            raysGenerationDatas[newRaysToCastStartIndex + (2 * j)] = reflectionHitInfoIndex;

                            hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].reflectionHitInfoId = reflectionHitInfoIndex;
                        }

                        ////////////////
                        // Refraction //
                        ////////////////

                        RayData refractionRay;
                        refractionRay.origin = hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].collisionPoint.xyz - faceNormal * 0.001f;
                        refractionRay.dir = normalize(refract(hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].inDir.xyz, 
                                                      ((globalUniformRT.useNormalMapForRefraction == true) ? N : vertexNormal), ratioRefractionIndex));

                        if (refractionRay.dir != vec3(0.0f) &&
                            (materials[hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].matId].opacity < 1.0f || globalUniformRT.alwaysCalculateRefractedRays == true) &&
                            intersect(refractionRay, hit)) {

                            uint refractionHitInfoIndex = atomicCounterIncrement(atomicCounterHitInfoData);

                            hitsInfoData[refractionHitInfoIndex].triangleId = hit.triangleId;
                            hitsInfoData[refractionHitInfoIndex].distance = hit.distance;
                            hitsInfoData[refractionHitInfoIndex].uvCoords = hit.uvCoords;
                            hitsInfoData[refractionHitInfoIndex].barycentricCoords = hit.barycentricCoords;
                            hitsInfoData[refractionHitInfoIndex].shadowsHitsInfoId = UINT_MAX;
                            hitsInfoData[refractionHitInfoIndex].collisionPoint = hit.collisionPoint;
                            hitsInfoData[refractionHitInfoIndex].normal = hit.normal;
                            hitsInfoData[refractionHitInfoIndex].tangent = hit.tangent;
                            hitsInfoData[refractionHitInfoIndex].faceNormal = hit.faceNormal;
                            hitsInfoData[refractionHitInfoIndex].matId = hit.matId;
                            hitsInfoData[refractionHitInfoIndex].geomId = hit.geomId;
                            hitsInfoData[refractionHitInfoIndex].reflectionHitInfoId = UINT_MAX;
                            hitsInfoData[refractionHitInfoIndex].refractionHitInfoId = UINT_MAX;
                            hitsInfoData[refractionHitInfoIndex].inDir = vec4(refractionRay.dir, 0.0f);

                            raysGenerationDatas[newRaysToCastStartIndex + (2 * j) + 1] = refractionHitInfoIndex;

                            hitsInfoData[raysGenerationDatas[sourceRaysStartIndex + j]].refractionHitInfoId = refractionHitInfoIndex;
                        }
                    }
                }
                // Switch buffer of ray to cast
                uint tmp = sourceRaysStartIndex;
                sourceRaysStartIndex = newRaysToCastStartIndex;
                newRaysToCastStartIndex = tmp;   
            }
        }
    }
    return hitInfoIndex;
}

//////////
// MAIN //
//////////

void main()
{   
    // Pixel coordinates:
    ivec2 pix = ivec2(gl_GlobalInvocationID.xy);
    pix += ivec2(globalUniformRT.pixelPad);
    ivec2 size = imageSize(outputBuffer);
    ivec2 sizePart = ivec2(globalUniformRT.processingPartDimension);

    // Avoid out of range values:
    if (pix.x >= size.x || pix.y >= size.y) {
        imageStore(outputBuffer, pix, uvec4(UINT_MAX));
        return;
    }

    // Perspective:
    vec2 pos = (vec2(pix) / vec2(size.x, size.y));

    RayData rayToCast;
    rayToCast.origin = globalUniformRT.eyePosition.xyz;
    rayToCast.dir = normalize(mix(mix(globalUniformRT.ray00.xyz, globalUniformRT.ray01.xyz, pos.y),
        mix(globalUniformRT.ray10.xyz, globalUniformRT.ray11.xyz, pos.y), pos.x));   

    // Ray casting:
    imageStore(outputBuffer, pix, uvec4(rayCasting(rayToCast, sizePart)));
})";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default pipeline compute shader for shadows.
 */
static const std::string pipeline_cs_shadow = R"(

// This is the (hard-coded) workgroup size:
layout (local_size_x = 8) in;

// Uniforms:
layout(binding = 0, offset = 0) uniform atomic_uint atomicCounterShadowHitInfoData;
uniform uint nrOfHitsInfoData;

$include Storage$
$include RTPipeline$

/**
 * Compute shadow rays
 */
void shadowRay(uint hitInfoDataId) {

    // Check if collision point is in the shadow or not
    uint shadowHitInfoIndex = atomicCounterIncrement(atomicCounterShadowHitInfoData);
    hitsInfoData[hitInfoDataId].shadowsHitsInfoId = shadowHitInfoIndex * globalUniform.nrOfLights;

    vec3 faceNormal = hitsInfoData[hitInfoDataId].faceNormal.xyz;
    if (dot(hitsInfoData[hitInfoDataId].normal.xyz,
         faceNormal) < 0.0f) // Coll. from inside
    {
        faceNormal = -faceNormal;
    }  

    ShadowRayData shadowRayData;
    shadowRayData.origin = hitsInfoData[hitInfoDataId].collisionPoint.xyz + faceNormal * (2f * K_EPSILON);

    for(int l = 0; l < globalUniform.nrOfLights; l++) {
        
        uint shadowHitInfoPerLightIndex = hitsInfoData[hitInfoDataId].shadowsHitsInfoId + l;

        if(lights[l].lightType == 1)
        {
            shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir = vec4(normalize(-lights[l].direction.xyz), 0.0f);
            shadowsHitsInfoData[shadowHitInfoPerLightIndex].distance = 1000000;
        }
        else if(lights[l].lightType == 2 || lights[l].lightType == 3)
        {
            vec3 lDir = lights[l].position.xyz - hitsInfoData[hitInfoDataId].collisionPoint.xyz;
            shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir = vec4(normalize(lDir), 0.0f);
            shadowsHitsInfoData[shadowHitInfoPerLightIndex].distance = length(lDir);
        }

        shadowsHitsInfoData[shadowHitInfoPerLightIndex].absorption = vec4(1.0f);

        shadowRayData.dir = shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir.xyz;
        shadowRayData.maxDistance = shadowsHitsInfoData[shadowHitInfoPerLightIndex].distance;
        shadowRayData.intensityAbsorption = vec3(1.0f);
        intersectShadow(shadowRayData);
        shadowsHitsInfoData[shadowHitInfoPerLightIndex].absorption = vec4(shadowRayData.intensityAbsorption.xyz, 1.0f);
    }
}


//////////
// MAIN //
//////////

void main()
{   
    // Pixel coordinates:
    uint hitInfoDataId = gl_GlobalInvocationID.x;

    // Avoid out of range values:
    if (hitInfoDataId >= nrOfHitsInfoData)   
       return;  

    shadowRay(hitInfoDataId);
})";

#pragma endregion


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief PipelineRayTracing reserved structure.
 */
struct OvGL::PipelineRayTracing::Reserved
{
    OvGL::Shader csRT;
    OvGL::Shader csShadow;
    OvGL::Program programRT;
    OvGL::Program programShadow;
    OvGL::UniformBufferObj globalRT;
    OvGL::StorageBufferObj triangles;
    OvGL::StorageBufferObj bspheres;
    OvGL::Texture2D outputBuffer;

    OvGL::StorageBufferObj hitsInfoDatas[PipelineRayTracing::numberOfProcessingPart];
    OvGL::StorageBufferObj shadowsHitsInfoDatas[PipelineRayTracing::numberOfProcessingPart];
    OvGL::StorageBufferObj raysGenerationData;

    OvGL::AtomicCounter atomicCountersHitInfoData[PipelineRayTracing::numberOfProcessingPart];
    OvGL::AtomicCounter atomicCountersShadowHitInfoData[PipelineRayTracing::numberOfProcessingPart];
    OvGL::AtomicCounter atomicCountersRaysGenerationData;

    // Scene-specific:
    OvGL::PipelineRayTracing::GlobalUniformRTData globalUniformRTData;

    /**
     * Constructor.
     */
    Reserved()
    {}
};


//////////////////////////////////////
// BODY OF CLASS PipelineRayTracing //
//////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::PipelineRayTracing::PipelineRayTracing() : reserved(std::make_unique<OvGL::PipelineRayTracing::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    this->setProgram(reserved->programRT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVGL_API OvGL::PipelineRayTracing::PipelineRayTracing(const std::string& name) : Ov::Pipeline(name),
reserved(std::make_unique<OvGL::PipelineRayTracing::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    this->setProgram(reserved->programRT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::PipelineRayTracing::PipelineRayTracing(PipelineRayTracing&& other) : Ov::Pipeline(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::PipelineRayTracing::~PipelineRayTracing()
{
    OV_LOG_DETAIL("[-]");
    if (isInitialized() && reserved)
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::PipelineRayTracing::init()
{
    // Already initialized?
    if (this->Ov::Pipeline::init() == false)
        return false;
    if (!this->isDirty())
        return false;

    // Build:
    reserved->csRT.load(OvGL::Shader::Type::compute, pipeline_cs);
    if (reserved->programRT.build({ reserved->csRT }) == false)
    {
        OV_LOG_ERROR("Unable to build RayTracing program");
        return false;
    }
    this->setProgram(reserved->programRT);

    reserved->csShadow.load(OvGL::Shader::Type::compute, pipeline_cs_shadow);
    if (reserved->programShadow.build({ reserved->csShadow }) == false)
    {
        OV_LOG_ERROR("Unable to build Shadow program");
        return false;
    }

    // Create uniform buffer:
    reserved->globalRT.create(sizeof(PipelineRayTracing::GlobalUniformRTData));

    // Create output image:   
    OvGL::Engine& eng = OvGL::Engine::getInstance();
    reserved->outputBuffer.create(eng.getWidth(), eng.getHeight(), Ov::Texture::Format::r32ui);

    // Create atomic counter:
    for (int i = 0; i < PipelineRayTracing::numberOfProcessingPart; i++) {
        reserved->atomicCountersHitInfoData[i].create(sizeof(uint32_t));
        reserved->atomicCountersHitInfoData[i].reset();
        reserved->atomicCountersShadowHitInfoData[i].create(sizeof(uint32_t));
        reserved->atomicCountersShadowHitInfoData[i].reset();
    }

    reserved->atomicCountersRaysGenerationData.create(sizeof(uint32_t));
    reserved->atomicCountersRaysGenerationData.reset();

    // Done: 
    this->setDirty(false);
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::PipelineRayTracing::free()
{
    if (this->Ov::Pipeline::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the color buffer used as ray tracing output.
 * @return color buffer texture reference
 */
const OvGL::Texture2D OVGL_API& OvGL::PipelineRayTracing::getOutputBuffer() const
{
    return reserved->outputBuffer;
}

const OvGL::StorageBufferObj OVGL_API& OvGL::PipelineRayTracing::getHitsInfoDataBuffer(uint32_t id) const
{
    if (id >= PipelineRayTracing::numberOfProcessingPart)
    {
        OV_LOG_ERROR("The passed index is out of range.");
        return OvGL::StorageBufferObj::empty;
    }

    return reserved->hitsInfoDatas[id];
}

const OvGL::StorageBufferObj OVGL_API& OvGL::PipelineRayTracing::getShadowsHitsInfoDataBuffer(uint32_t id) const
{
    if (id >= PipelineRayTracing::numberOfProcessingPart)
    {
        OV_LOG_ERROR("The passed index is out of range.");
        return OvGL::StorageBufferObj::empty;
    }

    return reserved->shadowsHitsInfoDatas[id];
}

uint32_t OVGL_API OvGL::PipelineRayTracing::getNrOfRecursion() const
{
    return reserved->globalUniformRTData.nrOfRecursion;
}

bool OVGL_API OvGL::PipelineRayTracing::getUseNormalMapForReflection() const 
{
    return reserved->globalUniformRTData.useNormalMapForReflection == 1 ? true : false;
}

bool OVGL_API OvGL::PipelineRayTracing::getUseNormalMapForRefraction() const
{
    return reserved->globalUniformRTData.useNormalMapForRefraction == 1 ? true : false;
}

bool OVGL_API OvGL::PipelineRayTracing::getUseRoghnessTexture() const
{
    return reserved->globalUniformRTData.useRoghnessTexture == 1 ? true : false;
}

bool OVGL_API OvGL::PipelineRayTracing::getUseMetalnessTexture() const
{
    return reserved->globalUniformRTData.useMetalnessTexture == 1 ? true : false;
}

bool OVGL_API OvGL::PipelineRayTracing::getUseAlbedoTexture() const
{
    return reserved->globalUniformRTData.useAlbedoTexture == 1 ? true : false;
}

bool OVGL_API OvGL::PipelineRayTracing::setNrOfRecursion(uint32_t nrOfRecursion)
{
    if (nrOfRecursion > PipelineRayTracing::maxNrOfRecursion) 
    {
        OV_LOG_ERROR("The passed number of recursion exceed max bound.");
        return false;
    }

    reserved->globalUniformRTData.nrOfRecursion = nrOfRecursion;
    return true;
}

#pragma endregion

#pragma region DataPreparation

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Migrates the data from a standard list into RT-specific structures.
 * @param list list of renderables
 * @return TF
 */
bool OVGL_API OvGL::PipelineRayTracing::migrate(const OvGL::List& list)
{
    // Safety net:
    if (list == Ov::List::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }


    ////////////////////////////////////////
    // 1st pass: count elems and fill lights
    uint32_t nrOfVertices = 0;
    uint32_t nrOfFaces = 0;
    uint32_t nrOfLights = list.getNrOfLights();
    uint32_t nrOfRenderables = list.getNrOfRenderableElems();
    uint32_t nrOfBSpheres = nrOfRenderables - nrOfLights;

    for (uint32_t c = nrOfLights; c < nrOfRenderables; c++)
    {
        const Ov::List::RenderableElem& re = list.getRenderableElem(c);

        const OvGL::Geometry& geom = dynamic_cast<const OvGL::Geometry&>(re.renderableData.first.get());

        const OvGL::ArrayBuffer& vbo = geom.getVertexBuffer();
        const OvGL::ElemArrayBuffer& ebo = geom.getFaceBuffer();

        nrOfVertices += vbo.getNrOfVertices();
        nrOfFaces += ebo.getNrOfFaces();
    }

    OV_LOG_DEBUG("Tot. nr. of faces . . :  %u", nrOfFaces);
    OV_LOG_DEBUG("Tot. nr. of vertices  :  %u", nrOfVertices);


    /////////////////////////
    // 2nd pass: fill buffers
    std::vector<OvGL::PipelineRayTracing::TriangleData> allTriangles(nrOfFaces);
    std::vector<OvGL::PipelineRayTracing::BSphereData> allBSpheres(nrOfBSpheres);
    nrOfFaces = 0; // Reset counter

    for (uint32_t c = nrOfLights; c < nrOfRenderables; c++)
    {
        const Ov::List::RenderableElem& re = list.getRenderableElem(c);

        // Get renderable world matrices:
        glm::mat4 modelMat = re.matrix;
        glm::mat3 normalMat = glm::inverseTranspose(re.matrix);

        const OvGL::Geometry& geom = dynamic_cast<const OvGL::Geometry&>(re.renderableData.first.get());
        std::reference_wrapper<const OvGL::Material> material = OvGL::Material::getDefault();

        if (re.renderableData.second.get() != Ov::Renderable::empty) {
            material = dynamic_cast<const OvGL::Material&>(re.renderableData.second.get());
        }

		// Read VBO back:
		const OvGL::ArrayBuffer& vbo = geom.getVertexBuffer();
		std::vector<OvGL::Geometry::VertexData> vData(geom.getNrOfVertices());

		glBindBuffer(GL_ARRAY_BUFFER, vbo.getOglHandle());
		glGetBufferSubData(GL_ARRAY_BUFFER, 0, geom.getNrOfVertices() * sizeof(OvGL::Geometry::VertexData), vData.data());

		// Read EBO back:
		const OvGL::ElemArrayBuffer& ebo = geom.getFaceBuffer();
		std::vector<OvGL::Geometry::FaceData> fData(geom.getNrOfFaces());

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo.getOglHandle());
		glGetBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, geom.getNrOfFaces() * sizeof(OvGL::Geometry::FaceData), fData.data());

		OV_LOG_DEBUG("Object: %s, data: %s, face: %u, %u, %u", geom.getName().c_str(),
			glm::to_string(vData[0].vertex).c_str(), fData[0].a, fData[0].b, fData[0].c);

        // List of vector containing the point to generate the radius.
        std::list<std::vector<float>> transformedVertices;
        for (uint32_t c = 0; c < vData.size(); c++) {
            std::vector<float> p(3);
            glm::vec4 vertexTrans = modelMat * glm::vec4(vData[c].vertex, 1.0f);
            p[0] = vertexTrans.x;
            p[1] = vertexTrans.y;
            p[2] = vertexTrans.z;
            transformedVertices.push_back(std::move(p));
        }

        // define the types of iterators through the points and their coordinates
        // ----------------------------------------------------------------------
        typedef std::list<std::vector<float>>::const_iterator PointIterator;
        typedef std::vector<float>::const_iterator CoordIterator;

        // create an instance of Miniball
        // ------------------------------
        typedef Miniball::Miniball <Miniball::CoordAccessor<PointIterator, CoordIterator>> MB;
        MB mb(3, transformedVertices.begin(), transformedVertices.end());

        // Bounding sphere:
        OvGL::PipelineRayTracing::BSphereData s;
        s.firstTriangle = nrOfFaces;
        s.nrOfTriangles = geom.getNrOfFaces();
        s.radius = sqrt(mb.squared_radius());
        s.geomId = geom.getId();

        const float* center = mb.center();
        s.position.x = *(center);
        s.position.y = *(++center);
        s.position.z = *(++center);
        allBSpheres[c - nrOfLights] = s;

		// Copy faces and vertices into the std::vector:
		for (uint32_t f = 0; f < ebo.getNrOfFaces(); f++)
		{
			OvGL::PipelineRayTracing::TriangleData t;
            t.matId = material.get().getLutPos();
            t.geomId = geom.getId();

			// First vertex:
            t.v[0] = modelMat * glm::vec4(vData[fData[f].a].vertex, 1.0f);
			t.n[0] = glm::vec4(normalMat * glm::vec3(glm::unpackSnorm4x8(vData[fData[f].a].normal)), 1.0f);
			t.t[0] = glm::vec4(normalMat * glm::vec3(glm::unpackSnorm4x8(vData[fData[f].a].tangent)), 1.0f);
			t.uv[0] = glm::vec2(glm::unpackHalf2x16(vData[fData[f].a].uv));

			// Second vertex:
			t.v[1] = modelMat * glm::vec4(vData[fData[f].b].vertex, 1.0f);
			t.n[1] = glm::vec4(normalMat * glm::vec3(glm::unpackSnorm4x8(vData[fData[f].b].normal)), 1.0f);
			t.t[1] = glm::vec4(normalMat * glm::vec3(glm::unpackSnorm4x8(vData[fData[f].b].tangent)), 1.0f);
			t.uv[1] = glm::vec2(glm::unpackHalf2x16(vData[fData[f].b].uv));

			// Third vertex:
			t.v[2] = modelMat * glm::vec4(vData[fData[f].c].vertex, 1.0f);
			t.n[2] = glm::vec4(normalMat * glm::vec3(glm::unpackSnorm4x8(vData[fData[f].c].normal)), 1.0f);
			t.t[2] = glm::vec4(normalMat * glm::vec3(glm::unpackSnorm4x8(vData[fData[f].c].tangent)), 1.0f);
			t.uv[2] = glm::vec2(glm::unpackHalf2x16(vData[fData[f].c].uv));

			allTriangles[nrOfFaces] = t;
			nrOfFaces++;
		}
    }

    ////////////////////////////
    // 3rd: copy data into SSBOs

    uint64_t size = nrOfFaces * sizeof(OvGL::PipelineRayTracing::TriangleData);
    if (reserved->triangles.getSize() != size) {
        reserved->triangles.free();
        reserved->triangles.create(size, allTriangles.data());
    }
    else
        memcpy(reserved->triangles.getDataPtr(), allTriangles.data(), size);
        
    size = nrOfBSpheres * sizeof(OvGL::PipelineRayTracing::BSphereData);
    if (reserved->bspheres.getSize() != size) {
        reserved->bspheres.free();
        reserved->bspheres.create(size, allBSpheres.data());
    }
    else
        memcpy(reserved->bspheres.getDataPtr(), allBSpheres.data(), size);

    // Done:
    reserved->globalUniformRTData.nrOfTriangles = nrOfFaces;
    reserved->globalUniformRTData.nrOfBSpheres = nrOfBSpheres;
    return true;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Main rendering method for the pipeline.
 * @param camera view camera
 * @param list list of renderables
 * @return TF
 */
bool OVGL_API OvGL::PipelineRayTracing::render(const Ov::Camera& camera, const Ov::List& list)
{
    // Safety net:
    if (camera == Ov::Camera::empty || list == Ov::List::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Just to update the cache
    this->Ov::Pipeline::render(list);

    // Lazy-loading:
    if (this->isDirty())
        if (!this->init())
        {
            OV_LOG_ERROR("Unable to render (initialization failed)");
            return false;
        }


    // Update compoenents used
    OvGL::Engine& eng = OvGL::Engine::getInstance();

    // Buffers
    uint64_t pixelPerPart = static_cast<uint64_t>(eng.getWidth()) *
        (static_cast<uint64_t>(eng.getHeight()) / PipelineRayTracing::numberOfProcessingPart);


    // HitInfoData buffers size
    uint64_t sizeHitsInfoDataBuffers = (pow(2, reserved->globalUniformRTData.nrOfRecursion + 1) - 1) * pixelPerPart;
    sizeHitsInfoDataBuffers *= sizeof(OvGL::PipelineRayTracing::HitInfoData);

    // ShadowHitInfo buffers size
    uint64_t sizeShadowHitsInfoDataBuffers = (pow(2, reserved->globalUniformRTData.nrOfRecursion + 1) - 1) * pixelPerPart;
    sizeShadowHitsInfoDataBuffers *= sizeof(OvGL::PipelineRayTracing::ShadowHitInfoData) * list.getNrOfLights();

    // RayGeneration buffers size
    uint64_t sizeRaysGenerationDatasBuffers = (
        (reserved->globalUniformRTData.nrOfRecursion > 0) ?
        pow(2, reserved->globalUniformRTData.nrOfRecursion + 1) : 0) * pixelPerPart;
    sizeRaysGenerationDatasBuffers *= sizeof(uint32_t);

    for (int i = 0; i < PipelineRayTracing::numberOfProcessingPart; i++) {

        if (reserved->hitsInfoDatas[i].getSize() != sizeHitsInfoDataBuffers) {
            reserved->hitsInfoDatas[i].free();
            if (reserved->hitsInfoDatas[i].create(sizeHitsInfoDataBuffers) == false) {
                return false;
            }
        }

        if (reserved->shadowsHitsInfoDatas[i].getSize() != sizeShadowHitsInfoDataBuffers) {
            reserved->shadowsHitsInfoDatas[i].free();
            if(reserved->shadowsHitsInfoDatas[i].create(sizeShadowHitsInfoDataBuffers) == false) {
            return false;
            }
        }
    }

    if (reserved->raysGenerationData.getSize() != sizeRaysGenerationDatasBuffers) {
        reserved->raysGenerationData.free();
        if(reserved->raysGenerationData.create(sizeRaysGenerationDatasBuffers) == false) {
        return false;
            }
    }


    // Get camera rays in the corners (will then be interpolated in the compute shader):
    glm::mat4 cameraMat = camera.getWorldMatrix();
    glm::mat4 viewMat = glm::inverse(cameraMat);
    glm::mat4 invViewProjMat = glm::inverse(camera.getProjMatrix() * viewMat);
    glm::vec4 eyePosition = cameraMat[3];

    glm::vec4 ray00 = invViewProjMat * glm::vec4(-1.0f, -1.0f, 0.0f, 1.0f);
    ray00 /= ray00.w;
    ray00 -= eyePosition;
    ray00 = glm::normalize(ray00);

    glm::vec4 ray01 = invViewProjMat * glm::vec4(-1.0f, 1.0f, 0.0f, 1.0f);
    ray01 /= ray01.w;
    ray01 -= eyePosition;
    ray01 = glm::normalize(ray01);

    glm::vec4 ray10 = invViewProjMat * glm::vec4(1.0f, -1.0f, 0.0f, 1.0f);
    ray10 /= ray10.w;
    ray10 -= eyePosition;
    ray10 = glm::normalize(ray10);

    glm::vec4 ray11 = invViewProjMat * glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
    ray11 /= ray11.w;
    ray11 -= eyePosition;
    ray11 = glm::normalize(ray11);


    OvGL::PipelineRayTracing::GlobalUniformRTData* globalUniformRT =
        reinterpret_cast<OvGL::PipelineRayTracing::GlobalUniformRTData*>(reserved->globalRT.getDataPtr());
    memcpy(globalUniformRT, &reserved->globalUniformRTData, sizeof(OvGL::PipelineRayTracing::GlobalUniformRTData));

    globalUniformRT->eyePosition = eyePosition;
    globalUniformRT->ray00 = ray00;
    globalUniformRT->ray01 = ray01;
    globalUniformRT->ray10 = ray10;
    globalUniformRT->ray11 = ray11;
    globalUniformRT->processingPartDimension = glm::vec2(reserved->outputBuffer.getSizeX(), reserved->outputBuffer.getSizeY() / PipelineRayTracing::numberOfProcessingPart);

    glm::vec2 pixelPad = glm::vec2(0, reserved->outputBuffer.getSizeY() / PipelineRayTracing::numberOfProcessingPart);

    for (int i = 0; i < PipelineRayTracing::numberOfProcessingPart; i++) {

        globalUniformRT->pixelPad = pixelPad * static_cast<float>(i);

        reserved->atomicCountersHitInfoData[i].reset();
        reserved->atomicCountersShadowHitInfoData[i].reset();
        reserved->atomicCountersRaysGenerationData.reset();

        // Apply program:
        if (reserved->programRT == Ov::Program::empty || reserved->programShadow == Ov::Program::empty)
        {
            OV_LOG_ERROR("Invalid program");
            return false;
        }

        // Ray tracing Reflection and refraction

        reserved->programRT.render();

        // Bindings:
        reserved->outputBuffer.bindImage(0);

        reserved->atomicCountersHitInfoData[i].render(1);
        reserved->atomicCountersRaysGenerationData.render(2);

        reserved->globalRT.render(static_cast<uint32_t>(OvGL::Buffer::Binding::globalRT));
        reserved->triangles.render(static_cast<uint32_t>(OvGL::Buffer::Binding::triangleRT));
        reserved->bspheres.render(static_cast<uint32_t>(OvGL::Buffer::Binding::bSphereRT));
        reserved->hitsInfoDatas[i].render(static_cast<uint32_t>(OvGL::Buffer::Binding::hitInfoRT));
        reserved->raysGenerationData.render(static_cast<uint32_t>(OvGL::Buffer::Binding::rayOriginHitRT));

        OvGL::Storage::getInstance().render();

        // Execute:
        reserved->programRT.compute(reserved->outputBuffer.getSizeX() / 8, (reserved->outputBuffer.getSizeY() / PipelineRayTracing::numberOfProcessingPart) / 8); // 8 is the hard-coded size of the workgroup
        reserved->programRT.wait();
        reserved->programRT.reset();

        // Ray tracing Shadows
        reserved->programShadow.render();

        uint32_t hitsInfoData;
        reserved->atomicCountersHitInfoData[i].read(&hitsInfoData);

        // Bindings:
        reserved->atomicCountersShadowHitInfoData[i].render(0);

        reserved->globalRT.render(static_cast<uint32_t>(OvGL::Buffer::Binding::globalRT));
        reserved->triangles.render(static_cast<uint32_t>(OvGL::Buffer::Binding::triangleRT));
        reserved->bspheres.render(static_cast<uint32_t>(OvGL::Buffer::Binding::bSphereRT));
        reserved->hitsInfoDatas[i].render(static_cast<uint32_t>(OvGL::Buffer::Binding::hitInfoRT));
        reserved->shadowsHitsInfoDatas[i].render(static_cast<uint32_t>(OvGL::Buffer::Binding::shadowHitInfoRT));

        reserved->programShadow.setUInt("nrOfHitsInfoData", hitsInfoData);
        
        OvGL::Storage::getInstance().render();

        // Execute:
        hitsInfoData += (8 - (hitsInfoData % 8));
        reserved->programShadow.compute(hitsInfoData / 8); // 8 is the hard-coded size of the workgroup
        reserved->programShadow.wait();
        reserved->programShadow.reset();
    }

    // Done:   
    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method to draw a gui on a windows with the ImGUI library.
 */
void OVGL_API OvGL::PipelineRayTracing::drawGUI()
{
    if (!ImGui::CollapsingHeader("RayTracing pipeline"))
        return;

    int rayRecursionDepth = reserved->globalUniformRTData.nrOfRecursion;
    ImGui::SliderInt("Ray recursion depth", &rayRecursionDepth, 0, OvGL::PipelineRayTracing::maxNrOfRecursion, "%d");
    reserved->globalUniformRTData.nrOfRecursion = rayRecursionDepth;

    float scalerFresnel = reserved->globalUniformRTData.offsetRayRefrRefl;
    ImGui::SliderFloat("Offset start ray Refr./Refl.", &scalerFresnel, 0.00001f, 0.01f, "%.5f");
    reserved->globalUniformRTData.offsetRayRefrRefl = scalerFresnel;

    bool boolStatus = false;

    if (ImGui::TreeNode("Reflections"))
    {
        bool boolStatus = reserved->globalUniformRTData.useNormalMapForReflection == 1;
        ImGui::Checkbox("Use normal map for reflection", &boolStatus);
        reserved->globalUniformRTData.useNormalMapForReflection = (boolStatus) ? 1 : 0;

        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Refractions"))
    {

        boolStatus = reserved->globalUniformRTData.useNormalMapForRefraction == 1;
        ImGui::Checkbox("Use normal map for refraction", &boolStatus);
        reserved->globalUniformRTData.useNormalMapForRefraction = (boolStatus) ? 1 : 0;

        boolStatus = reserved->globalUniformRTData.alwaysCalculateRefractedRays == 1;
        ImGui::Checkbox("Always calculate refracted rays", &boolStatus);
        reserved->globalUniformRTData.alwaysCalculateRefractedRays = (boolStatus) ? 1 : 0;

        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Materials"))
    {
        boolStatus = reserved->globalUniformRTData.useRoghnessTexture == 1;
        ImGui::Checkbox("Use roughness texture", &boolStatus);
        reserved->globalUniformRTData.useRoghnessTexture = (boolStatus) ? 1 : 0;

        boolStatus = reserved->globalUniformRTData.useMetalnessTexture == 1;
        ImGui::Checkbox("Use metalness texture", &boolStatus);
        reserved->globalUniformRTData.useMetalnessTexture = (boolStatus) ? 1 : 0;

        boolStatus = reserved->globalUniformRTData.useAlbedoTexture == 1;
        ImGui::Checkbox("Use albedo texture", &boolStatus);
        reserved->globalUniformRTData.useAlbedoTexture = (boolStatus) ? 1 : 0;

        ImGui::TreePop();
    }
}

#pragma endregion

#pragma region Shader

bool OVGL_API OvGL::PipelineRayTracing::addShaderPreproc()
{

    ////////////////////
    // GLOBAL UNIFORM //
    ////////////////////

    std::string globalUniformRTPreproc = R"(
#ifndef OVGL_RT_PIPELINE_GLOBAL_UNIFORM_INCLUDED
#define OVGL_RT_PIPELINE_GLOBAL_UNIFORM_INCLUDED
)";

    globalUniformRTPreproc += GlobalUniformRTData::getShaderStruct() + "\n";

    globalUniformRTPreproc += "layout(std140, binding = " +
        std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::globalRT)) + ") uniform GlobalUniformRT { GlobalUniformRTData globalUniformRT; };";
    globalUniformRTPreproc += R"(   
#endif
)";

    ///////////////
    // TRIANGLES //
    ///////////////

    std::string trianglesPreproc = R"(
#ifndef OVGL_RT_PIPELINE_TRIANGLE_INCLUDED
#define OVGL_RT_PIPELINE_TRIANGLE_INCLUDED
)";

    trianglesPreproc += TriangleData::getShaderStruct();

    trianglesPreproc += "layout(std430, binding="+ std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::triangleRT)) 
        +") buffer Triangles { TriangleData triangles[]; }; ";

    trianglesPreproc += R"(
#endif
)";

    //////////////
    // BOUNDING //
    //////////////

    std::string bSpheresPreproc = R"(
#ifndef OVGL_RT_PIPELINE_BOUNDING_SPHERE_INCLUDED
#define OVGL_RT_PIPELINE_BOUNDING_SPHERE_INCLUDED
)";

    bSpheresPreproc += BSphereData::getShaderStruct();

    bSpheresPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::bSphereRT))
        + ") buffer BSpheres { BSphereData bSpheres[]; }; ";

    bSpheresPreproc += R"(
#endif
)";


    /////////////
    // HITINFO //
    /////////////

    std::string hitInfoPreproc = R"(
#ifndef OVGL_RT_PIPELINE_HIT_INFO_INCLUDED
#define OVGL_RT_PIPELINE_HIT_INFO_INCLUDED
)";

    hitInfoPreproc += HitInfoData::getShaderStruct();

    hitInfoPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::hitInfoRT))
        + ") buffer HitsInfoData { HitInfoData hitsInfoData[]; }; ";

    hitInfoPreproc += R"(
#endif
)";


    /////////
    // Ray //
    /////////

    std::string rayPreproc = R"(
#ifndef OVGL_RT_PIPELINE_RAY_INCLUDED
#define OVGL_RT_PIPELINE_RAY_INCLUDED
)";

    rayPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::rayOriginHitRT))
        + ") buffer RaysGenerationDatas { uint raysGenerationDatas[]; }; ";

    rayPreproc += R"(
#endif
)";


    ////////////
    // Shadow //
    ////////////

    std::string shadowHitPreproc = R"(
#ifndef OVGL_RT_PIPELINE_SHADOW_HIT_INCLUDED
#define OVGL_RT_PIPELINE_SHADOW_HIT_INCLUDED
)";

    shadowHitPreproc += ShadowHitInfoData::getShaderStruct();

    shadowHitPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::shadowHitInfoRT))
        + ") buffer ShadowsHitsInfoData { ShadowHitInfoData shadowsHitsInfoData[]; }; ";

    shadowHitPreproc += R"(
#endif
)";


    ///////////////
    // RTGENERAL //
    ///////////////

    std::string rtGeneralPreproc = R"(
#ifndef OVGL_RT_PIPELINE_GENERAL_INCLUDED
#define OVGL_RT_PIPELINE_GENERAL_INCLUDED
)";

    rtGeneralPreproc += R"(

/////////////
// #DEFINE //
/////////////

#define K_EPSILON       1e-4f               // Tolerance around zero   
#define FLT_MAX         3.402823466e+38f    // Max float value
#define UINT_MAX        4294967295          // Max uint (32 bit) value
// #define CULLING                          // Back face culling enabled when defined
// #define SHOW_BOUNCES_AS_COLORS           // When defined, each pixel has a different color according to the bounce nr.


/**
 * Structure for modeling a ray. 
 */
struct RayData
{ 
    vec3 origin;    // Ray origin point
    vec3 dir;       // Normalized ray direction
};

///////////////
// FUNCTIONS //
///////////////

/**
 * Ray-sphere intersection.
 * param ray input ray
 * param center sphere center coords
 * param radius sphere radius size
 * param t output collision distance
 * return true on collision, false otherwise
 */
bool intersectSphere(const RayData ray, 
                     const vec3 center, const float radius,
                     out float t)
{ 
   float t0, t1; // solutions for t if the ray intersects 

   // Geometric solution:
   vec3 L = center - ray.origin; 
   float tca = dot(L, ray.dir); 
   //if (tca < 0.0f) return false; // the sphere is behind the ray origin
   float d2 = dot(L, L) - tca * tca; 
   if (d2 > (radius * radius)) 
      return false; 
   float thc = sqrt((radius * radius) - d2); 
   t0 = tca - thc; 
   t1 = tca + thc; 

   if (t0 > t1) 
   {
      float _t = t0;
      t0 = t1;
      t1 = _t;
   }
 
   if (t0 < 0.0f) 
   { 
      t0 = t1; // if t0 is negative, let's use t1 instead 
      if (t0 < 0.0f) 
         return false; // both t0 and t1 are negative 
   } 
 
   t = t0;  
   return true; 
} 


/**
 * Ray-triangle intersection.
 * param ray current ray
 * param v0 first triangle vertex
 * param v1 second triangle vertex
 * param v2 third triangle vertex
 * param t output collision distance
 * param u output barycentric coordinate u
 * param v output barycentric coordinate v
 */
bool intersectTriangle(const RayData ray, 
                       const vec3 v0, const vec3 v1, const vec3 v2, 
                       out float t, out float u, out float v) 
{ 
   vec3 v0v1 = v1 - v0; 
   vec3 v0v2 = v2 - v0; 
   vec3 pvec = cross(ray.dir, v0v2); 
   float det = dot(v0v1, pvec);    

#ifdef CULLING 
    // if the determinant is negative the triangle is backfacing
    // if the determinant is close to 0, the ray misses the triangle    
    if (det < K_EPSILON) 
      return false; 
#else 
    // ray and triangle are parallel if det is close to 0    
    if (abs(det) < K_EPSILON)
      return false;     
#endif 
    float invDet = 1.0f / det; 
 
    vec3 tvec = ray.origin - v0; 
    u = dot(tvec, pvec) * invDet; 
    if (u < 0.0f || u > 1.0f)     
      return false; 
 
    vec3 qvec = cross(tvec, v0v1); 
    v = dot(ray.dir, qvec) * invDet; 
    if (v < 0.0f || ((u + v) > 1.0f))     
      return false; 
 
    t = dot(v0v2, qvec) * invDet;  
    return (t > 0.0f) ? true : false;     
} 

/**
 * Main intersection method
 * param ray current ray  
 * param info collision information (output)
 * return true when the ray intersects a triangle, false otherwise
 */
bool intersect(RayData ray, out HitInfoData hitInfoData)
{  
    float dist;
    hitInfoData.triangleId = UINT_MAX; // Special value for "no triangle"
    hitInfoData.distance = FLT_MAX;         

    for (uint b = 0; b < globalUniformRT.nrOfBSpheres; b++) {
        if (intersectSphere(ray, bSpheres[b].position.xyz, bSpheres[b].radius, dist)) {
            float t, u, v;
            for (uint i = bSpheres[b].firstTriangle; i < bSpheres[b].firstTriangle + bSpheres[b].nrOfTriangles; i++) {
                if (intersectTriangle(ray, triangles[i].v[0].xyz, triangles[i].v[1].xyz, triangles[i].v[2].xyz, t, u, v)) {
                    if (t < hitInfoData.distance && i != hitInfoData.triangleId) {
                        hitInfoData.triangleId = i;
                        hitInfoData.distance = t;   
                        hitInfoData.barycentricCoords = vec2(u, v);
                        hitInfoData.matId = triangles[i].matId;                                                                               
                        hitInfoData.geomId = triangles[i].geomId;                                                                                                                                    
                    }
                }
            }
        }
    }


    // Compute final values:
    if (hitInfoData.triangleId != 999999)
    {  
        hitInfoData.collisionPoint = vec4(ray.origin + hitInfoData.distance * ray.dir, 0.0f);

        // Compute normal and tangent
        hitInfoData.normal = vec4(normalize(hitInfoData.barycentricCoords.x * triangles[hitInfoData.triangleId].n[1].xyz + hitInfoData.barycentricCoords.y * triangles[hitInfoData.triangleId].n[2].xyz + (1.0f - hitInfoData.barycentricCoords.x - hitInfoData.barycentricCoords.y) * triangles[hitInfoData.triangleId].n[0].xyz), 0.0f);      
        hitInfoData.tangent = vec4(normalize(hitInfoData.barycentricCoords.x * triangles[hitInfoData.triangleId].t[1].xyz + hitInfoData.barycentricCoords.y * triangles[hitInfoData.triangleId].t[2].xyz + (1.0f - hitInfoData.barycentricCoords.x - hitInfoData.barycentricCoords.y) * triangles[hitInfoData.triangleId].t[0].xyz), 0.0f);    
        
        if (dot(hitInfoData.normal.xyz, -ray.dir) < 0.0f) // Coll. from inside
        {
            hitInfoData.normal = -hitInfoData.normal;
            hitInfoData.tangent = -hitInfoData.tangent;
        }     

        // Compute texture coordinates
        hitInfoData.uvCoords = hitInfoData.barycentricCoords.x * triangles[hitInfoData.triangleId].uv[1].xy + hitInfoData.barycentricCoords.y * triangles[hitInfoData.triangleId].uv[2].xy + (1.0f - hitInfoData.barycentricCoords.x - hitInfoData.barycentricCoords.y) * triangles[hitInfoData.triangleId].uv[0].xy;      
    
        // Compute face normal:
        vec3 v0v1 = triangles[hitInfoData.triangleId].v[1].xyz - triangles[hitInfoData.triangleId].v[0].xyz;
        vec3 v0v2 = triangles[hitInfoData.triangleId].v[2].xyz - triangles[hitInfoData.triangleId].v[0].xyz;
        hitInfoData.faceNormal = vec4(normalize(cross(v0v1, v0v2)), 0.0f);
    }
            
    return hitInfoData.triangleId != 999999;
})";

    rtGeneralPreproc += R"(
#endif
)";


    ////////////
    // SHADOW //
    ////////////

    std::string shadowPreproc = R"(
#ifndef OVGL_RT_PIPELINE_SHADOW_INCLUDED
#define OVGL_RT_PIPELINE_SHADOW_INCLUDED
)";

    shadowPreproc += R"(

struct ShadowRayData
{ 
    vec3 origin;    // Ray origin point
    vec3 dir;       // Normalized ray direction  
    vec3 firstColorHit;
    vec3 intensityAbsorption;
    float maxDistance;
};

/**
 * Shadow intersection method
 * param ray current ray  
 * return true when the ray intersects a triangle, false otherwise
 */
void intersectShadow(inout ShadowRayData shadowRay)
{  
    float dist;
    uint triangleId = UINT_MAX; // Special value for "no triangle"
    uint geomId = UINT_MAX;    
    float NdotRFirst = 0.0f;

    RayData ray;
    ray.origin = shadowRay.origin;
    ray.dir = shadowRay.dir;

    for (uint b = 0; b < globalUniformRT.nrOfBSpheres; b++) {
        if (intersectSphere(ray, bSpheres[b].position.xyz, bSpheres[b].radius, dist)) {
            float t, u, v;
            for (uint i = bSpheres[b].firstTriangle; i < bSpheres[b].firstTriangle + bSpheres[b].nrOfTriangles; i++) {
                if (intersectTriangle(ray, triangles[i].v[0].xyz, triangles[i].v[1].xyz, triangles[i].v[2].xyz, t, u, v)) {
                    if (t < shadowRay.maxDistance && i != triangleId) {
  
                        vec2 barycentricCoords = vec2(u, v);
                        triangleId = i;

                        // Albedo
                        vec3 albedo = materials[triangles[i].matId].albedo.xyz;
                        if(materials[triangles[i].matId].albedoTextureHandle != 0 && globalUniformRT.useAlbedoTexture == true) {

                            // Compute texture coordinates
                            vec2 uvCoords = barycentricCoords.x * triangles[i].uv[1].xy + 
                                barycentricCoords.y * triangles[i].uv[2].xy +
                                (1.0f - barycentricCoords.x - barycentricCoords.y) * triangles[i].uv[0].xy;
      
                            albedo = texture(sampler2D(materials[triangles[i].matId].albedoTextureHandle), uvCoords).rgb;
                        }

                        // Compute normal and tangent
                        vec4 normal = vec4(normalize(barycentricCoords.x * triangles[triangleId].n[1].xyz +
                                barycentricCoords.y * triangles[triangleId].n[2].xyz + (1.0f - barycentricCoords.x -
                                 barycentricCoords.y) * triangles[triangleId].n[0].xyz), 0.0f);      
        
                        float NDotR = max(dot(normal.xyz, shadowRay.dir), 0.0f);

                        ////////////////////
                        // Compute Shadow //
                        ////////////////////

                        if(geomId != triangles[i].geomId)
                        {
                            shadowRay.firstColorHit = albedo;
                            geomId = triangles[i].geomId;
                            NdotRFirst = NDotR;   
                        }
                        else if((NdotRFirst <= 0.0f && NDotR >= 0.0f) || (NdotRFirst >= 0.0f && NDotR <= 0.0f))
                        {
                            vec3 finalColor = (shadowRay.firstColorHit + albedo) / 2 * (1 - materials[triangles[i].matId].opacity);
                            finalColor = mix(finalColor, vec3(1.0f), (1 - materials[triangles[i].matId].opacity));                            
                            shadowRay.intensityAbsorption *= finalColor;
                            shadowRay.firstColorHit = vec3(0.0f);
                        }                                                                                                                                                                      
                    }
                }
            }
        }
    }
})";

    shadowPreproc += R"(
#endif
)";


    std::string rtPipelineDescSetsPreproc = globalUniformRTPreproc + trianglesPreproc + bSpheresPreproc + hitInfoPreproc
        + shadowHitPreproc + rayPreproc + rtGeneralPreproc + shadowPreproc;
    return OvGL::Shader::preprocessor.set("include RTPipeline", rtPipelineDescSetsPreproc);
}

#pragma endregion