/**
 * @file	overv_gl_geometry.cpp
 * @brief	OGL geometry
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// GLM:
#include <glm/gtc/packing.hpp>  

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>


////////////
// STATIC //
////////////

// Special values:
OvGL::Geometry OvGL::Geometry::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Geometry class reserved structure.
 */
struct OvGL::Geometry::Reserved
{
    // Buffers:
    OvGL::VertexArrayObj vao;
    OvGL::ArrayBuffer vertexVbo;
    OvGL::ElemArrayBuffer faceVbo;

    /**
     * Constructor
     */
    Reserved()
    {}
};


////////////////////////////
// BODY OF CLASS Geometry //
////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Geometry::Geometry() : reserved(std::make_unique<OvGL::Geometry::Reserved>())
{
    // setDirty(false);
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the geometry
 */
OVGL_API OvGL::Geometry::Geometry(const std::string& name) : Ov::Geometry(name),
reserved(std::make_unique<OvGL::Geometry::Reserved>())
{
    // setDirty(false);
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Geometry::Geometry(Geometry&& other) noexcept : Ov::Geometry(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Geometry::~Geometry()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes an OpenGL geometry. Not really used, just a placeholder.
 * @return TF
 */
bool OVGL_API OvGL::Geometry::init()
{
    if (this->Ov::Managed::init() == false)
        return false;

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases an OpenGL geometry. Not really used, just a placeholder.
 * @return TF
 */
bool OVGL_API OvGL::Geometry::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the vertex buffer VBO.
 * @return vertex buffer VBO
 */
const OvGL::ArrayBuffer OVGL_API& OvGL::Geometry::getVertexBuffer() const
{
    return reserved->vertexVbo;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the face element VBO.
 * @return face element VBO
 */
const OvGL::ElemArrayBuffer OVGL_API& OvGL::Geometry::getFaceBuffer() const
{
    return reserved->faceVbo;
}

#pragma endregion

#pragma region Sync

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Upload data to GPU.
 * @return TF
 */
bool OVGL_API OvGL::Geometry::upload()
{
    // Create VAO and its VBOs:
    reserved->vao.create();
    reserved->vao.render();
    reserved->vertexVbo.create(this->getNrOfVertices(), this->getVertexDataPtr());
    reserved->faceVbo.create(this->getNrOfFaces(), this->getFaceDataPtr());
    setDirty(true);

    // Done:
    return true;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Geometry::render(uint32_t value, void* data) const
{
    const uint32_t nrOfFaces = this->getNrOfFaces();

    OvGL::Program& program = dynamic_cast<OvGL::Program&>(OvGL::Program::getCached());
    program.setMat4("model", *((glm::mat4*)data));

    reserved->vao.render();
    glDrawElements(GL_TRIANGLES, nrOfFaces * 3, GL_UNSIGNED_INT, nullptr);

    // Done:
    return true;
}

#pragma endregion