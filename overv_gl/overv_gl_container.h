/**
 * @file	overv_gl_container.h
 * @brief	OpenGL centralized data container
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief Class for storing all the data used during the life-cycle of the engine.
  */
class OVGL_API Container final : public Ov::Container
{
//////////
public: //
//////////

	// Special values:
	static Container empty;

	// Const/dest:
	Container();
	Container(Container&& other) noexcept;
	Container(Container const&) = delete;
	virtual ~Container();

	// Manager:
	bool add(Ov::Object& obj);
	bool reset();

	// Get/set:
	Ov::Node& getLastNode() const;
	OvGL::Geometry& getLastGeometry() const;
	OvGL::Material& getLastMaterial() const;
	OvGL::Texture& getLastTexture() const;
	OvGL::Texture2D& getLastTexture2D() const;
	OvGL::Light& getLastLight() const;
	void* getFinalClass();

	std::list<Ov::Node>& getNodeList();
	std::list<OvGL::Geometry>& getGeometryList();
	std::list<OvGL::Material>& getMaterialList();
	std::list<OvGL::Texture>& getTextureList();
	std::list<OvGL::Texture2D>& getTexture2DList();
	std::list<OvGL::Light>& getLightList();

	// Finders:
	Ov::Object& find(const std::string& name) const;   ///< By name
	Ov::Object& find(uint32_t id) const;               ///< By ID

///////////
private: //
///////////

   // Reserved:
   struct Reserved;
   std::unique_ptr<Reserved> reserved;

   // Const/dest:
   Container(const std::string& name);
};