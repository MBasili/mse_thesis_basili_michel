/**
 * @file	overv_gl_shader.h
 * @brief	OpenGL generic shader properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


 /**
  * @brief Class for modeling an OpenGL shader.
  */
class OVGL_API Shader : public Ov::Shader
{
//////////
public: //
//////////

	// Special values:
	static Shader empty;

    // Consts:
	static constexpr char glslVersion[] = "#version 460 core\n#extension GL_ARB_bindless_texture : require\n#extension GL_ARB_gpu_shader_int64 : require\n";   ///< This is the first line of all the shaders we used
	static constexpr uint32_t maxLogSize = 4096;              ///< Max shader compiler log size in bytes

	// Unique preprocessor:
	static Ov::Preprocessor preprocessor;                      ///< This preprocessor is always used on every shader

	// Const/dest:
	Shader();
	Shader(Shader&& other) noexcept;
	Shader(Shader const&) = delete;
	virtual ~Shader();

	// Managed:
	bool init() override;
	bool free() override;

	// Get/set:   
	uint32_t getOglHandle() const;

	// Accessing data:
	bool load(Type kind, const std::string& code);

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////   

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Shader(const std::string& name);
};