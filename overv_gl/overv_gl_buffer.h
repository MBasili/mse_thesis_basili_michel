/**
 * @file	overv_gl_buffer.h
 * @brief	OpenGL buffers
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 *          Michel Basili (michel.basili@supsi.ch), 2021 - 2022
 */
#pragma once


/**
 * @brief OpenGL buffers
 */
class OVGL_API Buffer : public Ov::Buffer
{
//////////
public: //
//////////

    /**
     * @brief Definition of binding points.
     */
    enum class Binding : uint32_t
    {
        none = 0,
        global = 10,   // Triple buffering will use 10, 11, 12
        material = 20, // 20, 21, 22
        light = 30,
        globalRT = 40,
        rayOriginHitRT = 41,
        triangleRT = 42,
        bSphereRT = 43,
        hitInfoRT = 44,
        shadowHitInfoRT = 45,
        hitPBRInfoRTLight = 46,
        globalL = 47,

        // Terminator:
        last
    };

    // Const/dest:
    Buffer();
    Buffer(Buffer&& other) noexcept;
    Buffer(Buffer const&) = delete;
    virtual ~Buffer();

    // Managed:
    bool init() override;
    bool free() override;

    // Sync:
    bool lock();
    bool unlock();

    // Get/set:   
    uint32_t getOglHandle() const;

/////////////
protected: //
/////////////

    // Reserved:
    struct Reserved;
    std::unique_ptr<Reserved> reserved;

    // Const/dest:
    Buffer(const std::string& name);
};