/**
 * @file	overv_gl_light.cpp
 * @brief	OpenGL light properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// Magic enum:
#include "magic_enum.hpp"

// ImGui
#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"
#include "imgui_internal.h"
#include "imconfig.h"


////////////
// STATIC //
////////////

// Special values:
OvGL::Light OvGL::Light::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Light reserved structure.
 */
struct OvGL::Light::Reserved
{
    glm::vec3 direction;
    Type type;
    float power;
    float spotCutOff;
    float spotExponent;

    /**
     * Constructor.
     */
    Reserved() : power{ 0 },
        direction{ 0.0 },
        spotCutOff{ 0 },
        spotExponent{ 0 },
        type{ Type::none }
    {}
};


/////////////////////////
// BODY OF CLASS Light //
/////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Light::Light() : reserved(std::make_unique<OvGL::Light::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the light.
 */
OVGL_API OvGL::Light::Light(const std::string& name) : Ov::Light(name),
reserved(std::make_unique<OvGL::Light::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Light::Light(Light&& other) noexcept: Ov::Light(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Light::~Light()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the type of the light.
 * @return light type
 */
OvGL::Light::Type OVGL_API OvGL::Light::getLightType() const
{
    return reserved->type;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the power of the light.
 * @return float
 */
float OVGL_API OvGL::Light::getPower() const
{
    return reserved->power;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the direction in which the light is pointing.
 * @return glm::vec3
 */
glm::vec3 OVGL_API OvGL::Light::getDirection() const
{
    return reserved->direction;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the cutOff of the light.
 * @return float
 */
float OVGL_API OvGL::Light::getSpotCutoff() const
{
    return reserved->spotCutOff;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the exponent of the light.
 * @return float
 */
float OVGL_API OvGL::Light::getSpotExponent() const
{
    return reserved->spotExponent;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the type of the light. The type need to be a supported one.
 * @param type the type of the light
 */
void OVGL_API OvGL::Light::setLightType(Type type)
{
    reserved->type = type;
    setDirty(true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the radius of the light.
 * @param radius the radius of the light
 */
void OVGL_API OvGL::Light::setPower(float power)
{
    reserved->power = power;
    setDirty(true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the direction of the light. For omnidirectional lights this parameter is ignored.
 * @param direction the direction where the light is pointing
 */
void OVGL_API OvGL::Light::setDirection(glm::vec3 direction)
{
    reserved->direction = direction;
    setDirty(true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the cutOff of the light. This parameter is used only for spot lights.
 * @param cutOff the cutOff of the light
 */
void OVGL_API OvGL::Light::setSpotCutoff(float cutOff)
{
    reserved->spotCutOff = cutOff;
    setDirty(true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the spotExponent of the light. This parameter is used only for spot lights.
 * @param spotExponent the spotExponent of the light
 */
void OVGL_API OvGL::Light::setSpotExponent(float spotExponent)
{
    reserved->spotExponent = spotExponent;
    setDirty(true);
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Light::render(uint32_t value, void* data) const
{
    // Lazy-loading:
    if (isDirty())
        setDirty(false);

    // Done:
    return true;
}

#pragma endregion

#pragma region GUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Draw GUI for a light with ImGUI
 */
void OVGL_API OvGL::Light::drawGUI() {

    // Light color
    glm::vec3 color = getColor();
    ImGui::SliderFloat3("Light color", glm::value_ptr(color), 0.0f, 1.0f);
    setColor(color);

    float cutOffInDegrees;
    float cutOffOutDegrees;
    switch (reserved->type)
    {
    case OvGL::Light::Type::directional:
        break;

    case OvGL::Light::Type::omni:
        // Radius
        ImGui::SliderFloat("Light radius", &reserved->power, 0.0f, 1000.0f);
        break;

    case OvGL::Light::Type::spot:

        // Radius
        ImGui::SliderFloat("Light radius", &reserved->power, 0.0f, 1000.0f);

        // Inner cutOff and Outer cutOff
        // Source for the range https://registry.khronos.org/OpenGL-Refpages/gl2.1/xhtml/glLight.xml
        cutOffInDegrees = 360 * (reserved->spotCutOff / (2 * IM_PI));
        cutOffOutDegrees = 360 * ((acos(cos(reserved->spotCutOff) - reserved->spotExponent)) / (2 * IM_PI));

        ImGui::SliderFloat("Light cutOffInnerCone", &cutOffInDegrees, 0.0f, 90.0f);
        ImGui::SliderFloat("Light cutOffOuterCone", &cutOffOutDegrees, 0.0f, 90.0f);

        if (cutOffInDegrees < 90 && cutOffOutDegrees < 90 && cutOffOutDegrees >= cutOffInDegrees)
        {
            reserved->spotCutOff = (2 * IM_PI) * (cutOffInDegrees / 360);
            reserved->spotExponent = cos(reserved->spotCutOff) - cos(((2 * IM_PI) * (cutOffOutDegrees / 360)));
        }
        break;

    default:
        break;
    }
}

#pragma endregion

#pragma region Ovo

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Saves the specific information of this light into the serializer.
 * @param serial serializer
 * @return TF
 */
bool OVGL_API OvGL::Light::saveChunk(Ov::Serializer& serial) const
{
    OV_LOG_DEBUG("Serializing light...");

    // Chunk header:
    uint8_t chunkId = static_cast<uint8_t>(ChunkId::light);
    uint32_t chunkSize = sizeof(uint32_t);
    chunkSize += static_cast<uint32_t>(strlen(this->getName().c_str())) + 1;
    chunkSize += sizeof(glm::vec3) * 2;
    chunkSize += sizeof(float) * 3;
    chunkSize += sizeof(uint32_t);

    serial.serialize(&chunkId, sizeof(uint8_t));
    serial.serialize(&chunkSize, sizeof(uint32_t));

    // Properties:   
    serial.serialize(this->getId());
    serial.serialize(this->getName());
    serial.serialize(this->getColor());
    serial.serialize(reserved->power);
    serial.serialize(reserved->direction);
    serial.serialize(reserved->spotCutOff);
    serial.serialize(reserved->spotExponent);
    serial.serialize(static_cast<uint32_t>(reserved->type));

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads the specific information of a given object. In its base class, this function loads the file version chunk.
 * @param serializer serial data
 * @param data optional pointer
 * @return id of the light loaded
 */
uint32_t OVGL_API OvGL::Light::loadChunk(Ov::Serializer& serial, void* data)
{
    // Chunk header
    uint8_t chunkId;
    serial.deserialize(&chunkId, sizeof(uint8_t));
    if (chunkId != static_cast<uint8_t>(Ovo::ChunkId::light))
    {
        OV_LOG_ERROR("Invalid chunk ID found");
        return 0;
    }
    uint32_t chunkSize;
    serial.deserialize(&chunkSize, sizeof(uint32_t));

    // Properties:      
    uint32_t id;
    serial.deserialize(id);

    std::string name;
    serial.deserialize(name);
    this->setName(name);

    glm::vec3 color;
    serial.deserialize(color);
    this->setColor(color);

    serial.deserialize(reserved->power);
    serial.deserialize(reserved->direction);
    serial.deserialize(reserved->spotCutOff);
    serial.deserialize(reserved->spotExponent);
    uint32_t typeLight;
    serial.deserialize(typeLight);
    reserved->type = static_cast<Type>(typeLight);

    // Done:      
    return id;
}

#pragma endregion