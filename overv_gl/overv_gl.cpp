/**
 * @file	overv_gl.cpp
 * @brief	OverVision 3D vintage OpenGL implementation
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 *          Randolf Schaerfig (randolf.schaerfig@SUPSI.ch), 2021
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:     
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// GUI:
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

// C/C++:
#include <sstream>

////////////////////////
// HANDLES STRUCTURES //
////////////////////////


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Engine class reserved structure.
 */
struct OvGL::Engine::Reserved
{
    GLFWwindow* glfwWindow;                 ///< Window handler
    int32_t windowSizeX, windowSizeY;   ///< Window width and height

    // Some counters:
    int64_t frameCounter;               ///< Total number of rendered frames   

    // Callbacks:
    OvGL::Engine::KeyboardCallback keyboardCallback;
    OvGL::Engine::MouseCursorCallback mouseCursorCallback;
    OvGL::Engine::MouseButtonCallback mouseButtonCallback;
    OvGL::Engine::MouseScrollCallback mouseScrollCallback;
    OvGL::Engine::WindowSizeCallback windowSizeCallback;


    /**
     * Constructor
     */
    Reserved() : frameCounter{ 0 },
        keyboardCallback{ nullptr },
        mouseCursorCallback{ nullptr },
        mouseButtonCallback{ nullptr },
        mouseScrollCallback{ nullptr },
        windowSizeCallback{ nullptr }
    {}
};


////////////
// STATIC //
////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Debug message parser for OpenGL.
 * @return formatted string for logging
 */
static std::string FormatDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, const char* msg)
{
    std::stringstream stringStream;
    std::string sourceString;
    std::string typeString;
    std::string severityString;

    switch (source)
    {
    case GL_DEBUG_SOURCE_API:              sourceString = "API"; break;
    case GL_DEBUG_SOURCE_APPLICATION:      sourceString = "Application"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:    sourceString = "Window System"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:  sourceString = "Shader Compiler"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:      sourceString = "Third Party"; break;
    case GL_DEBUG_SOURCE_OTHER:            sourceString = "Other"; break;
    default:                               sourceString = "Unknown";
    }

    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:                 typeString = "Error"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:   typeString = "Deprecated Behavior"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:    typeString = "Undefined Behavior"; break;
    case GL_DEBUG_TYPE_PERFORMANCE:           typeString = "Performance"; break;
    case GL_DEBUG_TYPE_OTHER:                 typeString = "Other"; break;
    default:                                  typeString = "Unknown";
    }

    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:     severityString = "High"; break;
    case GL_DEBUG_SEVERITY_MEDIUM:   severityString = "Medium"; break;
    case GL_DEBUG_SEVERITY_LOW:      severityString = "Low"; break;
    default:                         severityString = "Unknown";
    }

    stringStream << "[OGL] " << msg;
    stringStream << " [Source = " << sourceString;
    stringStream << ", Type = " << typeString;
    stringStream << ", Severity = " << severityString;
    stringStream << ", ID = " << id << "]";

    // Done:
    return stringStream.str();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Debug message callback for OpenGL.
 */
static void __stdcall DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar* message, GLvoid* userParam)
{
    if (id == 131218) // Shader recompilation
        return;

    if (id == 131185) // Info that buffer will use video memory as source for buffer operations
        return;


    std::string error = FormatDebugOutput(source, type, id, severity, message);
    if (type == GL_DEBUG_TYPE_ERROR)
    {
        OV_LOG_ERROR("%s", error.c_str());
    }
    else if (type != GL_DEBUG_TYPE_OTHER)
    {
        OV_LOG_INFO("%s", error.c_str());
    }
}


//////////////////////////
// BODY OF CLASS Engine //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Engine::Engine() : reserved(std::make_unique<OvGL::Engine::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Engine::~Engine()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Singleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 */
OvGL::Engine OVGL_API& OvGL::Engine::getInstance()
{
    static Engine instance;
    return instance;
}

#pragma endregion

#pragma region Init/Free

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization method.
 * @param config configuration
 * @return TF
 */
bool OVGL_API OvGL::Engine::init(const Ov::Config& config)
{
    if (this->Ov::Core::init(config) == false)
        return false;


    /////////////
    // Init glfw:
    typedef void(*GLWF_ERROR_CALLBACK_PTR)(int32_t error, const char* description);
    glfwSetErrorCallback(static_cast<GLWF_ERROR_CALLBACK_PTR>
        (
            // Callback:
            [](int32_t error, const char* description)
            {
                OV_LOG_ERROR("[GLFW] code: %d, %s", error, description);
            }
    ));

    // Init framework:
    if (!glfwInit())
    {
        OV_LOG_ERROR("Unable to init GLFW");
        return false;
    }

    int32_t glfwMajor, glfwMinor, glfwRev;
    glfwGetVersion(&glfwMajor, &glfwMinor, &glfwRev);
    OV_LOG_PLAIN("   Using GLFW v%d.%d.%d", glfwMajor, glfwMinor, glfwRev);

    // Open an OpenGL window:      
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
#ifdef OV_DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#else
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_FALSE);
    glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_TRUE);
#endif
    glfwWindowHint(GLFW_RED_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS, 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);

    reserved->glfwWindow = glfwCreateWindow(static_cast<int32_t>(this->getConfig().getWindowSizeX()),
        static_cast<int32_t>(this->getConfig().getWindowSizeY()),
        this->getConfig().getWindowTitle().c_str(),
        nullptr,
        nullptr);
    if (reserved->glfwWindow == nullptr)
    {
        OV_LOG_ERROR("Unable to create window");
        return false;
    }

    // Different-than-specified size?
    int32_t width, height;
    glfwGetWindowSize(reserved->glfwWindow, &width, &height);
    if (width != static_cast<int32_t>(this->getConfig().getWindowSizeX()) ||
        height != static_cast<int32_t>(this->getConfig().getWindowSizeY()))
    {
        OV_LOG_WARN("Window resized to %dx%d pixels", width, height);

        // Overwrite config:
        this->getConfig().setWindowSizeX(static_cast<uint32_t>(width));
        this->getConfig().setWindowSizeY(static_cast<uint32_t>(height));
    }

    // Set custom ptr and context:
    glfwSetWindowUserPointer(reserved->glfwWindow, this);
    glfwMakeContextCurrent(reserved->glfwWindow);

    // Glew:
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        OV_LOG_ERROR("Unable to init GLEW");
        return false;;
    }
    OV_LOG_PLAIN("   Using GLEW v%s.%s.%s", glewGetString(GLEW_VERSION_MAJOR), glewGetString(GLEW_VERSION_MINOR), glewGetString(GLEW_VERSION_MICRO));
    if (!glewIsSupported("GL_VERSION_4_6"))
    {
        OV_LOG_ERROR("OpenGL 4.6 not supported");
        return false;
    }

    // Log and validate supported settings:
    OV_LOG_PLAIN("OpenGL properties:");
    OV_LOG_PLAIN("   Vendor . . . :  %s", glGetString(GL_VENDOR));
    OV_LOG_PLAIN("   Driver . . . :  %s", glGetString(GL_RENDERER));

    int32_t oglVersion[2];
    glGetIntegerv(GL_MAJOR_VERSION, &oglVersion[0]);
    glGetIntegerv(GL_MINOR_VERSION, &oglVersion[1]);
    OV_LOG_PLAIN("   Version  . . :  %s [%d.%d]", glGetString(GL_VERSION), oglVersion[0], oglVersion[1]);
    if (glfwGetWindowAttrib(reserved->glfwWindow, GLFW_CONTEXT_NO_ERROR))
        OV_LOG_PLAIN("   No error . . :  enabled");
    else
        OV_LOG_PLAIN("   No error . . :  disabled");
    OV_LOG_PLAIN("   GLSL . . . . :  %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

    int32_t nrOfExtensions;
    glGetIntegerv(GL_NUM_EXTENSIONS, &nrOfExtensions);
    OV_LOG_PLAIN("   Extensions . :  %d", nrOfExtensions);

    // Check for required extensions:
    if (!glewIsSupported("GL_ARB_bindless_texture"))
    {
        OV_LOG_ERROR("GL_ARB_bindless_texture not supported");
        return false;
    }
    if (!glewIsSupported("GL_EXT_texture_compression_s3tc"))
    {
        OV_LOG_ERROR("GL_EXT_texture_compression_s3tc not supported");
        return false;
    }

    int workGroupSizes[3] = { 0 };
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &workGroupSizes[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &workGroupSizes[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &workGroupSizes[2]);
    int workGroupCounts[3] = { 0 };
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &workGroupCounts[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &workGroupCounts[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &workGroupCounts[2]);

    OV_LOG_PLAIN("   Max group sz :  %d, %d, %d", workGroupSizes[0], workGroupSizes[1], workGroupSizes[2]);
    OV_LOG_PLAIN("   Max group cnt:  %d, %d, %d", workGroupCounts[0], workGroupCounts[1], workGroupCounts[2]);

    float maxAnisotropy = 0.0f;
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY, &maxAnisotropy);
    OV_LOG_PLAIN("   Max anistropy:  %.1f", maxAnisotropy);
    if (maxAnisotropy < 16.0f)
    {
        OV_LOG_ERROR("Anistropic filter level 16 or higher not supported");
        return false;
    }

#if OV_DEBUG
    // Query the OpenGL function to register your callback function:
    PFNGLDEBUGMESSAGECALLBACKPROC _glDebugMessageCallback = (PFNGLDEBUGMESSAGECALLBACKPROC)glfwGetProcAddress("glDebugMessageCallback");

    // Register callback function:
    if (_glDebugMessageCallback != nullptr)
    {
        _glDebugMessageCallback((GLDEBUGPROC)DebugCallback, nullptr);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    }
    else
        OV_LOG_ERROR("Unable to register debug callback");
#endif   
    glfwGetFramebufferSize(reserved->glfwWindow, &reserved->windowSizeX, &reserved->windowSizeY);
    if (this->getConfig().isVSync())
        glfwSwapInterval(1);
    else
        glfwSwapInterval(0);
    glViewport(0, 0, reserved->windowSizeX, reserved->windowSizeY);

    // Common OpenGL settings:
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);         // Not sure whether it is really global state
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);       // Not sure whether it is really global state
    glEnable(GL_MULTISAMPLE);
    glDisable(GL_DITHER);
    // Init storage:
    OvGL::Storage::getInstance().init();

    // Init default material adn texture:
    OvGL::Material::getDefault();
    OvGL::Texture2D::getDefault();

    // Add shader preprocess
    OvGL::PipelineRayTracing::addShaderPreproc();
    OvGL::PipelineLighting::addShaderPreproc();
    OvGL::Storage::addShaderPreproc();

    int workGroupSizess[3] = { 0 };
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &workGroupSizess[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &workGroupSizess[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &workGroupSizess[2]);
    int workGroupCountss[3] = { 0 };
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &workGroupCountss[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &workGroupCountss[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &workGroupCountss[2]);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Deinitialization method.
 */
bool OVGL_API OvGL::Engine::free()
{
    OV_LOG_DEBUG("Releasing context...");

    this->Ov::Core::free();

    // Release glfw:
    if (reserved->glfwWindow)
    {
        // Release OGL resources:
        OvGL::Storage::getInstance().free();
        // ...

        glfwDestroyWindow(reserved->glfwWindow);
        reserved->glfwWindow = nullptr;
    }
    glfwTerminate();

    OV_LOG_PLAIN("   Context deinitialized");

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the current frame number.
 * @return current frame number
 */
uint64_t OVGL_API OvGL::Engine::getFrameNr() const
{
    return reserved->frameCounter;
}

int OVGL_API OvGL::Engine::getWidth() { return reserved->windowSizeX; }

int OVGL_API OvGL::Engine::getHeight() { return reserved->windowSizeY; }

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Process events.
 * @return TF
 */
bool OVGL_API OvGL::Engine::processEvents()
{
    glfwPollEvents();

    // Window shall be closed?
    if (glfwWindowShouldClose(reserved->glfwWindow))
        return false;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear buffers.
 * @return TF
 */
bool OVGL_API OvGL::Engine::clear()
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Done:
    return true;
}

bool OVGL_API OvGL::Engine::clearDepth()
{
    glClear(GL_DEPTH_BUFFER_BIT);
    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Swap buffers.
 * @return TF
 */
bool OVGL_API OvGL::Engine::swap()
{
    // OV_LOG_DEBUG("Finished with frame %llu", reserved->frameCounter);
    glfwSwapBuffers(reserved->glfwWindow);

    // New frame:
    reserved->frameCounter++;

    // Next storage:
    OvGL::Storage& storage = OvGL::Storage::getInstance();
    storage.useNext();

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set keyboard callback.
 * @param cb keyboard callback function pointer
 * @return TF
 */
bool OVGL_API OvGL::Engine::setKeyboardCallback(KeyboardCallback cb)
{
    reserved->keyboardCallback = cb;

    // Register callbacks:
    glfwSetKeyCallback(reserved->glfwWindow, static_cast<GLFWkeyfun>
        (
            // Callback:
            [](GLFWwindow* window, int key, int scancode, int action, int mods)
            {
                OvGL::Engine* _this = static_cast<OvGL::Engine*>(glfwGetWindowUserPointer(window));
                _this->reserved->keyboardCallback(key, scancode, action, mods);
            }
    ));

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set mouse cursor callback.
 * @param cb mouse callback function pointer
 * @return TF
 */
bool OVGL_API OvGL::Engine::setMouseCursorCallback(MouseCursorCallback cb)
{
    reserved->mouseCursorCallback = cb;

    // Register callbacks:
    glfwSetCursorPosCallback(reserved->glfwWindow, static_cast<GLFWcursorposfun>
        (
            // Callback:
            [](GLFWwindow* window, double mouseX, double mouseY)
            {
                OvGL::Engine* _this = static_cast<OvGL::Engine*>(glfwGetWindowUserPointer(window));
                _this->reserved->mouseCursorCallback(mouseX, mouseY);
            }
    ));

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set mouse button callback.
 * @param cb mouse button callback function pointer
 * @return TF
 */
bool OVGL_API OvGL::Engine::setMouseButtonCallback(MouseButtonCallback cb)
{
    reserved->mouseButtonCallback = cb;

    // Register callbacks:
    glfwSetMouseButtonCallback(reserved->glfwWindow, static_cast<GLFWmousebuttonfun>
        (
            // Callback:
            [](GLFWwindow* window, int button, int action, int mods)
            {
                OvGL::Engine* _this = static_cast<OvGL::Engine*>(glfwGetWindowUserPointer(window));
                _this->reserved->mouseButtonCallback(button, action, mods);
            }
    ));

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set mouse scroll callback.
 * @param cb mouse scroll callback function pointer
 * @return TF
 */
bool OVGL_API OvGL::Engine::setMouseScrollCallback(MouseScrollCallback cb)
{
    reserved->mouseScrollCallback = cb;

    // Register callbacks:
    glfwSetScrollCallback(reserved->glfwWindow, static_cast<GLFWscrollfun>
        (
            // Callback:
            [](GLFWwindow* window, double scrollX, double scrollY)
            {
                OvGL::Engine* _this = static_cast<OvGL::Engine*>(glfwGetWindowUserPointer(window));
                _this->reserved->mouseScrollCallback(scrollX, scrollY);
            }
    ));

    // Done:
    return true;
}

void OVGL_API OvGL::Engine::setWindowSizeCallback() {
    glfwSetWindowSizeCallback(reserved->glfwWindow, static_cast<GLFWwindowsizefun>(
        // Callback:
        [](GLFWwindow* window, int _width, int _height) {
            OvGL::Engine* _this = static_cast<OvGL::Engine*>(glfwGetWindowUserPointer(window));
            _this->windowSizeCallback(_width, _height);
        }
    ));
}

void OVGL_API OvGL::Engine::windowSizeCallback(int _width, int _height) {
    reserved->windowSizeX = _width;
    reserved->windowSizeY = _height;

    // glfwGetFramebufferSize(reserved->window, &reserved->windowSizeX, &reserved->windowSizeY);
    if (this->getConfig().isVSync())
        glfwSwapInterval(1);
    else
        glfwSwapInterval(0);
    glViewport(0, 0, reserved->windowSizeX, reserved->windowSizeY);
}

#pragma endregion

#pragma region GLFW

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the key corresponding to the passed key code is pressed on the surface.
 * @return TF
 */
bool OVGL_API OvGL::Engine::queryKey(int keyCode) const
{
    if (glfwGetKey(reserved->glfwWindow, keyCode) == GLFW_PRESS)
        return true;

    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the window use to display the render.
 * @return GLFW Window
 */
void OVGL_API* OvGL::Engine::getGLFWWindow() const {
    return reserved->glfwWindow;
}

#pragma endregion