/**
 * @file	overv_gl_camera.h
 * @brief	Basic camera properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"


/**
 * @brief Class for modelling an OpenGL camera.
 */
class OVGL_API Camera : public Ov::Camera
{
//////////
public: //
//////////

	// Const/dest:
	Camera();
	Camera(Camera&& other) noexcept;
	Camera(Camera const&) = delete;
	virtual ~Camera();

///////////
private: //
///////////   

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Camera(const std::string& name);
};
