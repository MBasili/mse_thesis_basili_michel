/**
 * @file	overv_gl_quad.cpp
 * @brief	OpenGL fullscreen quad renderer
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>


/////////////
// SHADERS //
/////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default quad vertex shader.
 */
static const std::string quad_vs = R"(
/////////////
// INCLUDE //
/////////////

   $include global$
   


//////////
// VARS //
//////////

   // Out:
   out vec4 color;


//////////
// MAIN //
//////////
void main()
{  
   float x = -1.0f + float((gl_VertexID & 1) << 2);
   float y = -1.0f + float((gl_VertexID & 2) << 1);
   
   // texCoord.x = (x + 1.0f) * 0.5f;
   // texCoord.y = (y + 1.0f) * 0.5f;

   color = vec4(((1.0f - y) + 1.2f) * 0.1f, ((1.0f - y) + 1.2f) * 0.4f, ((1.0f - y) + 1.2f) * 0.5f, 1.0f);
   gl_Position = vec4(x, y, 1.0f, 1.0f);
})";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default quad fragment shader.
 */
static const std::string quad_fs = R"(
/////////////
// INCLUDE //
/////////////

   

//////////
// VARS //
//////////

   // In:   
   in vec4 color;
   
   // Out:
   out vec4 colorOut;
 


//////////
// MAIN //
//////////
void main()
{   
   colorOut = vec4(color);
})";



/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Quad reserved structure.
 */
struct OvGL::Quad::Reserved
{
    // Shader:
    OvGL::Program program;
    OvGL::Shader vs;
    OvGL::Shader fs;

    // Buffers:
    OvGL::VertexArrayObj vao; ///< Dummy VAO, required for core context


    /**
     * Constructor.
     */
    Reserved()
    {
        vs.load(Ov::Shader::Type::vertex, quad_vs);
        fs.load(Ov::Shader::Type::fragment, quad_fs);
        program.build({ vs, fs });

        vao.create();
    }
};


////////////////////////
// BODY OF CLASS Quad //
////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Quad::Quad() : reserved(std::make_unique<OvGL::Quad::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the quad.
 */
OVGL_API OvGL::Quad::Quad(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<OvGL::Quad::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Quad::Quad(Quad&& other) noexcept : Ov::Renderable(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Quad::~Quad()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Quad::render(uint32_t value, void* data) const
{
    // Set shader:
    reserved->program.render();

    // Smart trick:   
    reserved->vao.render();
    glDrawArrays(GL_TRIANGLES, 0, 3);

    // Done:
    return true;
}

#pragma endregion