/**
 * @file	overv_gl_texture2dmulti.cpp
 * @brief	OpenGL 2D-specific texture properties with multisampling
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// Magic enum:
#include "magic_enum.hpp"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Texture2DMulti reserved structure.
 */
struct OvGL::Texture2DMulti::Reserved
{
    /**
     * Constructor.
     */
    Reserved()
    {}
};


//////////////////////////////////
// BODY OF CLASS Texture2DMulti //
//////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Texture2DMulti::Texture2DMulti() : reserved(std::make_unique<OvGL::Texture2DMulti::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVGL_API OvGL::Texture2DMulti::Texture2DMulti(const std::string& name) : OvGL::Texture(name), reserved(std::make_unique<OvGL::Texture2DMulti::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Texture2DMulti::Texture2DMulti(Texture2DMulti&& other) : OvGL::Texture(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Texture2DMulti::~Texture2DMulti()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Bitmap

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Allocate memory and initialize an empty texture.
 * @param sizeX texture width
 * @param sizeY texture height
 * @param format pixel layout
 * @return TF
 */
bool OVGL_API OvGL::Texture2DMulti::create(uint32_t sizeX, uint32_t sizeY, Format format)
{
    // Safety net:
    if (sizeX == 0 || sizeY == 0 || format == Format::none)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Init texture:
    this->OvGL::Texture::init();

    // Bind texture and copy content:   
    GLuint intFormat;
    GLuint extFormat;
    GLuint extType;
    GLuint nrOfComponents;
    switch (format)
    {
        /////////////////////////////
    case Format::r8g8b8_multi: //      
        intFormat = GL_RGB8;
        extFormat = GL_RGB;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 3;
        break;

        ///////////////////////////////      
    case Format::r8g8b8a8_multi: //
        intFormat = GL_RGBA8;
        extFormat = GL_RGBA;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 4;
        break;

        ////////////////////////////
    case Format::depth_multi: //
        intFormat = GL_DEPTH_COMPONENT32;
        extFormat = GL_DEPTH_COMPONENT;
        extType = GL_FLOAT;
        nrOfComponents = 1;
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Unexpected format type");
        return false;
    }

    // Create it:		    
    const GLuint oglId = this->getOglHandle();
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, oglId);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, OvGL::Engine::getInstance().getConfig().getNrOfAASamples(), intFormat, sizeX, sizeY, GL_TRUE);
    glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MAX_LEVEL, 0);

    // Resident:
    this->OvGL::Texture::makeResident();

    // Done:
    this->setFormat(format);
    this->setSizeX(sizeX);
    this->setSizeY(sizeY);
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Texture2DMulti::render(uint32_t value, void* data) const
{
    // OV_LOG_PLAIN("   render tex2D %s", this->getName().c_str());

    // Apply texture props to OpenGL:
    const GLuint oglId = this->getOglHandle();

    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, oglId);

    // Done:
    return true;
}

#pragma endregion