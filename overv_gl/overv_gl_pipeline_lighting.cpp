/**
 * @file		engine_pipeline_fullscreen2d.cpp
 * @brief	A pipeline for rendering a texture to the fullscreen in 2D
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) SUPSI, Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// ImGui
#include "imgui.h"

/////////////
// SHADERS //
/////////////

#pragma region Shaders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default pipeline vertex shader.
 */
static const std::string pipeline_vs = R"(

// Per-vertex data from VBOs:
layout(location = 0) in vec3 a_vertex;

// Out:
out vec2 texCoord;

void main()
{   
   float x = a_vertex.x;
   float y = a_vertex.y;
   
   texCoord.x = (x + 1.0f) * 0.5f;
   texCoord.y = (y + 1.0f) * 0.5f;
   
   gl_Position = vec4(x, y, 1.0f, 1.0f);
})";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default pipeline fragment shader.
 */
static const std::string pipeline_fs = R"(
$include Storage$
$include Lighting$

// In:   
in vec2 texCoord;

// Uniform:
layout(binding = 0, rgba8) uniform image2D outputBuffer;
layout(binding = 1, r32ui) uniform uimage2D sourceHitinfoStruct;
layout(binding = 2, offset = 0) uniform atomic_uint atomicCounterHitPBRInfoData;

void main()
{
    ivec2 size = imageSize(sourceHitinfoStruct);
    ivec2 pix = ivec2(int(texCoord.x * size.x), int(texCoord.y * size.y));

    uint hitInfoId = imageLoad(sourceHitinfoStruct, pix).r;
    if(hitInfoId != UINT_MAX) {

        if(globalUniformL.showProblemNDFFunction == true) {
            
            uint hitInfoIdReflection = hitsInfoData[hitInfoId].reflectionHitInfoId;
            if(hitInfoIdReflection != UINT_MAX) {
                // Incoming direction
                vec3 V = -hitsInfoData[hitInfoId].inDir.xyz;
                vec3 directionRefl = hitsInfoData[hitInfoIdReflection].inDir.xyz;

                vec3 H = normalize(V + directionRefl);

                // Roughness
                float roughness = materials[hitsInfoData[hitInfoId].matId].roughness;
                if(materials[hitsInfoData[hitInfoId].matId].roughnessTextureHandle != 0 && globalUniformL.useRoghnessTexture == true)
                    roughness = texture(sampler2D(materials[hitsInfoData[hitInfoId].matId].roughnessTextureHandle), hitsInfoData[hitInfoId].uvCoords).r;

                // NDF Function
                float NDF = DistributionGGX(H, H, roughness);
                vec3 valueToStore = (NDF > 1) ? vec3(1.0f,0.0f,0.0f) : vec3(NDF);
                imageStore(outputBuffer, pix, vec4(valueToStore, 0.0f));
            }
            else{ imageStore(outputBuffer, pix, vec4(0.0f)); }
            return;
        }

        uint numberOfHitedPointsPerPixel = uint(pow(2, globalUniformL.nrOfRecursion + 1)) - 1;
        uint hitPBRInfoDataIndex = atomicCounterIncrement(atomicCounterHitPBRInfoData);
        uint hitPBRInfoDataStartIndex = hitPBRInfoDataIndex * numberOfHitedPointsPerPixel;
        
        // Set first hit info
        hitsPBRInfoData[hitPBRInfoDataStartIndex].hitInfoId = hitInfoId;

        for(uint i = 0; i < numberOfHitedPointsPerPixel; i++) {
            
            uint reflectPos = hitPBRInfoDataStartIndex + (2 * i) + 1;
            uint refractPos = hitPBRInfoDataStartIndex + (2 * i) + 2;

            hitsPBRInfoData[hitPBRInfoDataStartIndex + i].intermediateLo = vec4(0.0f, 0.0f, 0.0f, 1.0f);
            hitsPBRInfoData[hitPBRInfoDataStartIndex + i].finalLo = vec4(0.0f, 0.0f, 0.0f, 1.0f);
            hitsPBRInfoData[hitPBRInfoDataStartIndex + i].reflectHitPBRInfoDataPos = UINT_MAX;
            hitsPBRInfoData[hitPBRInfoDataStartIndex + i].reflractHitPBRInfoDataPos = UINT_MAX;    
            
            // Reflect compute
            if(((2 * i) + 1) < numberOfHitedPointsPerPixel) {    
                hitsPBRInfoData[reflectPos].hitInfoId = UINT_MAX;
            }

            // Refract compute
            if(((2 * i) + 2) < numberOfHitedPointsPerPixel) {
                hitsPBRInfoData[refractPos].hitInfoId = UINT_MAX;
            }

            if(hitsPBRInfoData[hitPBRInfoDataStartIndex + i].hitInfoId != UINT_MAX) {
                
                // PBR compute
                hitsPBRInfoData[hitPBRInfoDataStartIndex + i].intermediateLo = vec4(computePBR(hitsPBRInfoData[hitPBRInfoDataStartIndex + i].hitInfoId), 1.0f);

                // Reflect compute
                if(((2 * i) + 1) < numberOfHitedPointsPerPixel) {
                    hitsPBRInfoData[reflectPos].hitInfoId = hitsInfoData[hitsPBRInfoData[hitPBRInfoDataStartIndex + i].hitInfoId].reflectionHitInfoId;
                    if(hitsPBRInfoData[reflectPos].hitInfoId != UINT_MAX) {
                        hitsPBRInfoData[hitPBRInfoDataStartIndex + i].reflectHitPBRInfoDataPos = reflectPos;
                    }
                }

                // Refract compute
                if(((2 * i) + 2) < numberOfHitedPointsPerPixel) {
                    hitsPBRInfoData[refractPos].hitInfoId = hitsInfoData[hitsPBRInfoData[hitPBRInfoDataStartIndex + i].hitInfoId].refractionHitInfoId;
                    if(hitsPBRInfoData[refractPos].hitInfoId != UINT_MAX) {
                        hitsPBRInfoData[hitPBRInfoDataStartIndex + i].reflractHitPBRInfoDataPos = refractPos;
                    }
                }
            }
        }

        for(int j = int(numberOfHitedPointsPerPixel); j >= 0; --j) {
            if( hitsPBRInfoData[hitPBRInfoDataStartIndex + j].hitInfoId != UINT_MAX) {

                uint hitInfoDataId = hitsPBRInfoData[hitPBRInfoDataStartIndex + j].hitInfoId;
                vec3 Lo = hitsPBRInfoData[hitPBRInfoDataStartIndex + j].intermediateLo.xyz;
                vec3 kD = vec3(1.0f);

                // Albedo
                vec3 albedo = materials[hitsInfoData[hitInfoDataId].matId].albedo.xyz;
                if(materials[hitsInfoData[hitInfoDataId].matId].albedoTextureHandle != 0 && globalUniformL.useAlbedoTexture == true)
                    albedo = texture(sampler2D(materials[hitsInfoData[hitInfoDataId].matId].albedoTextureHandle), hitsInfoData[hitInfoDataId].uvCoords).rgb;

                // Roughness
                float roughness = materials[hitsInfoData[hitInfoDataId].matId].roughness;
                if(materials[hitsInfoData[hitInfoDataId].matId].roughnessTextureHandle != 0 && globalUniformL.useRoghnessTexture == true)
                    roughness = texture(sampler2D(materials[hitsInfoData[hitInfoDataId].matId].roughnessTextureHandle), hitsInfoData[hitInfoDataId].uvCoords).r;

                // Metalness
                float metalness = materials[hitsInfoData[hitInfoDataId].matId].metalness;
                if(materials[hitsInfoData[hitInfoDataId].matId].metalnessTextureHandle != 0 && globalUniformL.useMetalnessTexture == true)
                    metalness = texture(sampler2D(materials[hitsInfoData[hitInfoDataId].matId].metalnessTextureHandle), hitsInfoData[hitInfoDataId].uvCoords).r;


                ////////////////////
                // Normal mapping //
                ////////////////////
                
                vec3 vertexNormal =  hitsInfoData[hitInfoDataId].normal.xyz;
                vec3 vertexTanget =  hitsInfoData[hitInfoDataId].tangent.xyz;
                
                // Correction => if tangent and normal are perpendicular dot product equal to 0.
                vertexTanget = normalize(vertexTanget - dot(vertexTanget, vertexNormal) * vertexNormal);
                
                // inverse = transpose iff the matrix is orthogonal 
                mat3 TBN;          

                bool isInside = false;
                if (dot(vertexNormal,
                     hitsInfoData[hitInfoDataId].faceNormal.xyz) < 0.0f) // Coll. from inside
                {
                    isInside = true;

                    // BiTangent
                    vec3 B = cross(-vertexNormal, -vertexTanget);
                    TBN = mat3(-vertexTanget, B, -vertexNormal);
                }
                else
                {
                    // BiTangent
                    vec3 B = cross(vertexNormal, vertexTanget);
                    TBN = mat3(vertexTanget, B, vertexNormal);
                }  

                // Normal
                vec3 N = vertexNormal;
                if(materials[hitsInfoData[hitInfoDataId].matId].normalTextureHandle > 0 && globalUniformL.useNormalMapForReflection == true) {
                    vec3 normal = vec3(texture(sampler2D(materials[hitsInfoData[hitInfoDataId].matId].normalTextureHandle),
                                       hitsInfoData[hitInfoDataId].uvCoords).rg, 0.0f);
                    normal = vec3(normal.r, normal.g, sqrt(pow(1.0,2.0) - pow(normal.r,2.0) - pow(normal.g,2.0)));
                    normal = normal * 2.0 - 1.0;
                    N = normalize(TBN * normal);
                    if (isInside)
                    {
                        N = -N;
                    }  
                }

                //Reflection
                if(hitsPBRInfoData[hitPBRInfoDataStartIndex + j].reflectHitPBRInfoDataPos != UINT_MAX) {
                    
                    // Incoming direction
                    vec3 V = -hitsInfoData[hitInfoDataId].inDir.xyz;
                    vec3 direction = hitsInfoData[hitsInfoData[hitInfoDataId].reflectionHitInfoId].inDir.xyz;
                    vec3 H = normalize(V + direction);

                    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
                    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
                    vec3 F0 = vec3(0.04); 
                    F0 = mix(F0, albedo, metalness);
                    
                    // cook-torrance brdf
                    float NDF = DistributionGGX(N, H, roughness);

                    if(globalUniformL.useNDFStandard == false) {
                        NDF = clamp(NDF, 0.0f, 1.0f);
                    }
 
                    float G   = GeometrySmith(N, V, direction, roughness);   
                    vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);   

                    vec3 kS = F;
                    vec3 kD = vec3(1.0) - kS;
                    kD *= 1.0 - metalness;
    
                    vec3 numerator    = NDF * G * F;
                    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, direction), 0.0) + 0.0001;
                    vec3 specular     = numerator / denominator;

                    Lo += specular * ((globalUniformL.scaleReflectionInvRoughness == true) ? 1 - roughness : 1.0f)
                            * hitsPBRInfoData[hitsPBRInfoData[hitPBRInfoDataStartIndex + j].reflectHitPBRInfoDataPos].finalLo.xyz;
                }

                // Refraction
                if(hitsPBRInfoData[hitPBRInfoDataStartIndex + j].reflractHitPBRInfoDataPos != UINT_MAX) {
                    // * filter wavelenghts
                    Lo += kD * hitsPBRInfoData[hitsPBRInfoData[hitPBRInfoDataStartIndex + j].reflractHitPBRInfoDataPos].finalLo.xyz *
                        (1 - materials[hitsInfoData[hitInfoDataId].matId].opacity);
                }    
                hitsPBRInfoData[hitPBRInfoDataStartIndex + j].finalLo = vec4(Lo.xyz, 1.0f);
            }
        }
        vec3 color = hitsPBRInfoData[hitPBRInfoDataStartIndex].finalLo.xyz;
        // Reinhard operator, preserving the high dynamic range of a possibly highly varying irradiance
        if(globalUniformL.isHDRCorrectionEnabled == true) {
            color = hitsPBRInfoData[hitPBRInfoDataStartIndex].finalLo.xyz / (hitsPBRInfoData[hitPBRInfoDataStartIndex].finalLo.xyz + vec3(1.0));
            color = pow(color.xyz, vec3(1.0/2.2)); 
        }
        imageStore(outputBuffer, pix, vec4(color.xyz, 1.0f));
    }
    else { imageStore(outputBuffer, pix, vec4(0.0f)); }
})";

#pragma endregion


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief PipelineFullscreen2D reserved structure.
 */
struct OvGL::PipelineLighting::Reserved
{
    OvGL::Shader vs;
    OvGL::Shader fs;
    OvGL::Program program;
    OvGL::VertexArrayObj vao[PipelineRayTracing::numberOfProcessingPart];
    OvGL::ArrayBuffer vbo[PipelineRayTracing::numberOfProcessingPart];
    uint32_t indices[4];
    OvGL::Texture2D outputBuffer;
    OvGL::StorageBufferObj hitsPBRInfoData;
    OvGL::AtomicCounter atomicCounterHitPBRInfoData;
    OvGL::UniformBufferObj globalL;

    OvGL::PipelineLighting::GlobalUniformLData globalUniformLData;

    /**
     * Constructor.
     */
    Reserved()
    {}
};


////////////////////////////////////////
// BODY OF CLASS PipelineFullscreen2D //
////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::PipelineLighting::PipelineLighting() : reserved(std::make_unique<OvGL::PipelineLighting::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    this->setProgram(reserved->program);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVGL_API OvGL::PipelineLighting::PipelineLighting(const std::string& name) : Ov::Pipeline(name), reserved(std::make_unique<OvGL::PipelineLighting::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    this->setProgram(reserved->program);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::PipelineLighting::PipelineLighting(PipelineLighting&& other) : Ov::Pipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::PipelineLighting::~PipelineLighting()
{
    OV_LOG_DETAIL("[-]");
    if (this->isInitialized() && reserved)
        free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::PipelineLighting::init()
{
    // Already initialized?
    if (this->Ov::Managed::init() == false)
        return false;
    if (!this->isDirty())
        return false;

    // Build:
    reserved->vs.load(Ov::Shader::Type::vertex, pipeline_vs);
    reserved->fs.load(Ov::Shader::Type::fragment, pipeline_fs);
    if (reserved->program.build({ reserved->vs, reserved->fs }) == false)
    {
        OV_LOG_ERROR("Unable to build fullscreenLighting program");
        return false;
    }
    this->setProgram(reserved->program);

    // Init vaos for quad rendering
    // normalized device coordinates go to (0,1) to (0, -1) so total vertical gap is 2.
    float verticalStep = 2.0f / PipelineRayTracing::numberOfProcessingPart;

    for (int i = 0; i < PipelineRayTracing::numberOfProcessingPart; i++) {
        
        if (reserved->vao[i].init() == false)
        {
            OV_LOG_ERROR("Unable to init VAO for quand screen redering");
            return false;
        }

        reserved->vao[i].render();
        
        std::vector<glm::vec3> vertices(4);
        vertices[0] = glm::vec3(-1.0f, -1.0f + (verticalStep * i) + verticalStep, 0.0f);
        vertices[1] = glm::vec3(1.0f, -1.0f + (verticalStep * i) + verticalStep, 0.0f);
        vertices[2] = glm::vec3(-1.0f, -1.0f + (verticalStep * i), 0.0f);
        vertices[3] = glm::vec3(1.0f, -1.0f + (verticalStep * i),0.0f);

        reserved->vbo[i].create(4, vertices.data(), true, false, false, false);
    }

    reserved->indices[0] = 1;
    reserved->indices[1] = 0;
    reserved->indices[2] = 2;
    reserved->indices[3] = 3;

    // Create output image:   
    OvGL::Engine& eng = OvGL::Engine::getInstance();
    reserved->outputBuffer.create(eng.getWidth(), eng.getHeight(), Ov::Texture::Format::r8g8b8a8);

    // Create atomic counter:
    reserved->atomicCounterHitPBRInfoData.create(sizeof(uint32_t));
    reserved->atomicCounterHitPBRInfoData.reset();

    // Create uniform buffer:
    reserved->globalL.create(sizeof(PipelineLighting::GlobalUniformLData));

    // Done: 
    this->setDirty(false);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::PipelineLighting::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the color buffer used as ray tracing output.
 * @return color buffer texture reference
 */
const OvGL::Texture2D OVGL_API& OvGL::PipelineLighting::getOutputBuffer() const
{
    return reserved->outputBuffer;
}

#pragma endregion

#pragma region Render

bool OVGL_API OvGL::PipelineLighting::render(const OvGL::PipelineRayTracing& raytracing, const Ov::List& list) {

    // Safety net:
    if (list == Ov::List::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Just to update the cache
    this->Ov::Pipeline::render(list);

    // Lazy-loading:
    if (this->isDirty())
        if (!this->init())
        {
            OV_LOG_ERROR("Unable to render (initialization failedl)");
            return false;
        }


    // Size of the storage buffer for PBR calculation
    OvGL::Engine& eng = OvGL::Engine::getInstance();

    // Compute size of buffer to compute the pixels value
    uint64_t pixelPerPart = static_cast<uint64_t>(eng.getWidth()) * (static_cast<uint64_t>(eng.getHeight()) / PipelineRayTracing::numberOfProcessingPart);

    uint64_t size = pixelPerPart;
    size += (pow(2, raytracing.getNrOfRecursion() + 1) - 1) * pixelPerPart;
    size *= sizeof(OvGL::PipelineLighting::HitPBRInfoData);

    if (reserved->hitsPBRInfoData.getSize() != size) {
        reserved->hitsPBRInfoData.free();
        reserved->hitsPBRInfoData.create(size);
    }

    // Apply program:
    if (reserved->program == OvGL::Program::empty)
    {
        OV_LOG_ERROR("Invalid program");
        return false;
    }


    OvGL::PipelineLighting::GlobalUniformLData* globalUniformL =
        reinterpret_cast<OvGL::PipelineLighting::GlobalUniformLData*>(reserved->globalL.getDataPtr());
    memcpy(globalUniformL, &reserved->globalUniformLData, sizeof(OvGL::PipelineLighting::GlobalUniformLData));
    
    globalUniformL->useNormalMapForReflection = raytracing.getUseNormalMapForReflection();
    globalUniformL->useNormalMapForRefraction = raytracing.getUseNormalMapForRefraction();
    globalUniformL->useRoghnessTexture = raytracing.getUseRoghnessTexture();
    globalUniformL->useMetalnessTexture = raytracing.getUseMetalnessTexture();
    globalUniformL->useAlbedoTexture = raytracing.getUseAlbedoTexture();
    globalUniformL->nrOfRecursion = raytracing.getNrOfRecursion();

    for (int i = 0; i < PipelineRayTracing::numberOfProcessingPart; i++) {

        // Lighting program
        reserved->program.render();

        // Bindings
        OvGL::Storage::getInstance().render();

        reserved->outputBuffer.bindImage(0);
        raytracing.getOutputBuffer().bindImage(1);
        reserved->atomicCounterHitPBRInfoData.render(2);

        reserved->globalL.render(static_cast<uint32_t>(OvGL::Buffer::Binding::globalL));
        reserved->hitsPBRInfoData.render(static_cast<uint32_t>(OvGL::Buffer::Binding::hitPBRInfoRTLight));
        raytracing.getHitsInfoDataBuffer(i).render(static_cast<uint32_t>(OvGL::Buffer::Binding::hitInfoRT));
        raytracing.getShadowsHitsInfoDataBuffer(i).render(static_cast<uint32_t>(OvGL::Buffer::Binding::shadowHitInfoRT));

        reserved->vao[i].render();
        glDrawElements(GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, reserved->indices);

        reserved->program.reset();
        reserved->atomicCounterHitPBRInfoData.reset();
    }

    // Done:   
    return true;

}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method to draw a gui on a windows with the ImGUI library.
 */
void OVGL_API OvGL::PipelineLighting::drawGUI()
{
    if (!ImGui::CollapsingHeader("Lighting pipeline"))
        return;

    bool boolStatus = (reserved->globalUniformLData.isHDRCorrectionEnabled == 1) ? true : false;
    ImGui::Checkbox("Enable/Disable HDR correction", &boolStatus);
    reserved->globalUniformLData.isHDRCorrectionEnabled = (boolStatus) ? 1 : 0;

    if (ImGui::TreeNode("Reflections"))
    {
        boolStatus = (reserved->globalUniformLData.useNDFStandard == 1) ? true : false;
        ImGui::Checkbox("Use NDF Standard", &boolStatus);
        reserved->globalUniformLData.useNDFStandard = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->globalUniformLData.showProblemNDFFunction == 1) ? true : false;
        ImGui::Checkbox("Show NDF function problem", &boolStatus);
        reserved->globalUniformLData.showProblemNDFFunction = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->globalUniformLData.scaleReflectionInvRoughness == 1) ? true : false;
        ImGui::Checkbox("Scale reflection with inverse roughness", &boolStatus);
        reserved->globalUniformLData.scaleReflectionInvRoughness = (boolStatus) ? 1 : 0;

        ImGui::TreePop();
    }
}

#pragma endregion

#pragma region Shader

bool OVGL_API OvGL::PipelineLighting::addShaderPreproc()
{
    ////////////////////
    // GLOBAL UNIFORM //
    ////////////////////

    std::string globalUniformLPreproc = R"(
#ifndef OVGL_LIGHTING_PIPELINE_GLOBAL_UNIFORM_INCLUDED
#define OVGL_LIGHTING_PIPELINE_GLOBAL_UNIFORM_INCLUDED
)";

    globalUniformLPreproc += GlobalUniformLData::getShaderStruct() + "\n";

    globalUniformLPreproc += "layout(std140, binding = " +
        std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::globalL)) + ") uniform GlobalUniformL { GlobalUniformLData globalUniformL; };";
    globalUniformLPreproc += R"(   
#endif
)";

    /////////////
    // HITINFO //
    /////////////

    std::string hitInfoPreproc = R"(
#ifndef OVGL_LIGHTING_PIPELINE_HIT_INFO_INCLUDED
#define OVGL_LIGHTING_PIPELINE_HIT_INFO_INCLUDED
)";

    hitInfoPreproc += OvGL::PipelineRayTracing::HitInfoData::getShaderStruct();

    hitInfoPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::hitInfoRT))
        + ") buffer HitsInfoData { HitInfoData hitsInfoData[]; }; ";

    hitInfoPreproc += R"(
#endif
)";

    ////////////
    // Shadow //
    ////////////

    std::string shadowHitPreproc = R"(
#ifndef OVGL_LIGHTING_PIPELINE_SHADOW_INCLUDED
#define OVGL_LIGHTING_PIPELINE_SHADOW_INCLUDED
)";

    shadowHitPreproc += OvGL::PipelineRayTracing::ShadowHitInfoData::getShaderStruct();

    shadowHitPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::shadowHitInfoRT))
        + ") buffer ShadowsHitsInfoData { ShadowHitInfoData shadowsHitsInfoData[]; }; ";

    shadowHitPreproc += R"(
#endif
)";


    ////////////
    // Hitpbr //
    ////////////

    std::string hitPBRInfoPreproc = R"(
#ifndef OVGL_LIGHTING_PIPELINE_HITPBR_INCLUDED
#define OVGL_LIGHTING_PIPELINE_HITPBR_INCLUDED
)";

    hitPBRInfoPreproc += OvGL::PipelineLighting::HitPBRInfoData::getShaderStruct();

    hitPBRInfoPreproc += "layout(std430, binding=" + std::to_string(static_cast<uint32_t>(OvGL::Buffer::Binding::hitPBRInfoRTLight))
        + ") buffer HitsPBRInfoData { HitPBRInfoData hitsPBRInfoData[]; }; ";

    hitPBRInfoPreproc += R"(
#endif
)";


    /////////
    // PBR //
    /////////

    std::string pbrPreproc = R"(
#ifndef OVGL_LIGHTING_PIPELINE_PBR_INCLUDED
#define OVGL_LIGHTING_PIPELINE_PBR_INCLUDED

#define UINT_MAX        4294967295          // Max uint (32 bit) value
const float PI = 3.14159265359;

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}

vec3 computePBR(uint hitInfoId) {
    // Albedo
    vec3 albedo = materials[hitsInfoData[hitInfoId].matId].albedo.xyz;
    if(materials[hitsInfoData[hitInfoId].matId].albedoTextureHandle != 0 && globalUniformL.useAlbedoTexture == true)
        albedo = texture(sampler2D(materials[hitsInfoData[hitInfoId].matId].albedoTextureHandle), hitsInfoData[hitInfoId].uvCoords).rgb;
    
    // Roughness
    float roughness = materials[hitsInfoData[hitInfoId].matId].roughness;
    if(materials[hitsInfoData[hitInfoId].matId].roughnessTextureHandle != 0 && globalUniformL.useRoghnessTexture == true)
        roughness = texture(sampler2D(materials[hitsInfoData[hitInfoId].matId].roughnessTextureHandle), hitsInfoData[hitInfoId].uvCoords).r;
    
    // Metalness
    float metalness = materials[hitsInfoData[hitInfoId].matId].metalness;
    if(materials[hitsInfoData[hitInfoId].matId].metalnessTextureHandle != 0 && globalUniformL.useMetalnessTexture == true)
        metalness = texture(sampler2D(materials[hitsInfoData[hitInfoId].matId].metalnessTextureHandle), hitsInfoData[hitInfoId].uvCoords).r;
    
     ////////////////////
     // Normal mapping //
     ////////////////////
     
     vec3 vertexNormal =  hitsInfoData[hitInfoId].normal.xyz;
     vec3 vertexTanget =  hitsInfoData[hitInfoId].tangent.xyz;
     
     // Correction => if tangent and normal are perpendicular dot product equal to 0.
     vertexTanget = normalize(vertexTanget - dot(vertexTanget, vertexNormal) * vertexNormal);
     
     // inverse = transpose iff the matrix is orthogonal 
     mat3 TBN;          
     
     bool isInside = false;
     if (dot(vertexNormal,
          hitsInfoData[hitInfoId].faceNormal.xyz) < 0.0f) // Coll. from inside
     {
         isInside = true;
     
         // BiTangent
         vec3 B = cross(-vertexNormal, -vertexTanget);
         TBN = mat3(-vertexTanget, B, -vertexNormal);
     }
     else
     {
         // BiTangent
         vec3 B = cross(vertexNormal, vertexTanget);
         TBN = mat3(vertexTanget, B, vertexNormal);
     }  
     
     // Normal
     vec3 N = vertexNormal;
     if(materials[hitsInfoData[hitInfoId].matId].normalTextureHandle > 0) {
         vec3 normal = vec3(texture(sampler2D(materials[hitsInfoData[hitInfoId].matId].normalTextureHandle),
                            hitsInfoData[hitInfoId].uvCoords).rg, 0.0f);
         normal = vec3(normal.r, normal.g, sqrt(pow(1.0,2.0) - pow(normal.r,2.0) - pow(normal.g,2.0)));
         normal = normal * 2.0 - 1.0;
         N = normalize(TBN * normal);
         if (isInside)
         {
             N = -N;
         }  
     }


    
    /////////
    // PBR //
    /////////
    
    // Incoming direction
    vec3 V = -hitsInfoData[hitInfoId].inDir.xyz;
    
    // reflectance equation
    vec3 Lo = vec3(0.0);
    
    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metalness);
    
    for(int l = 0; l < globalUniform.nrOfLights; l++)
    {
        uint shadowHitInfoPerLightIndex = hitsInfoData[hitInfoId].shadowsHitsInfoId + l;
    
        float intensity = 0.0f;
        float distance = shadowsHitsInfoData[shadowHitInfoPerLightIndex].distance;
        // 1 => directional / 2 => spot / 3 =>omni
        if(lights[l].lightType == 1)
        {
            intensity = 1.0f/((distance > 0) ? distance : 1.0f);
        }
        else if(lights[l].lightType == 3)
        {
            intensity = 1.0f / ((distance > 0) ? distance*distance : 1.0f);
        }
        else if(lights[l].lightType == 2)
        {
            intensity = 1.0f / ((distance > 0) ? distance*distance : 1.0f);

            // https://learnopengl.com/Lighting/Light-casters
            float theta          = dot(shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir.xyz , normalize(-lights[l].direction.xyz));
            float outerCutoffCos = cos(lights[l].cutOff) - lights[l].spotExponent;
            float spotIntensity  = clamp((theta - outerCutoffCos) / lights[l].spotExponent, 0.0, 1.0);
            intensity *= spotIntensity;
        }

        vec3 radiance = lights[l].color.xyz * intensity * lights[l].power;
        vec3 H = normalize(V + shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir.xyz);
    
        // cook-torrance brdf
        float NDF = DistributionGGX(N, H, roughness);        
        float G   = GeometrySmith(N, V, shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir.xyz, roughness);      
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);    
    
        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metalness;
    
        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir.xyz), 0.0) + 0.0001;
        vec3 specular     = numerator / denominator;  
            
        // Check if the light hit the surface from the front.
        float NdotL = max(dot(vertexNormal, shadowsHitsInfoData[shadowHitInfoPerLightIndex].lightDir.xyz), 0.0);
    
        vec3 absorption = vec3(1.0f);
        // Shadow
        if(NdotL > 0) {
            absorption = shadowsHitsInfoData[shadowHitInfoPerLightIndex].absorption.xyz;
        }

        Lo += (kD * albedo / PI * materials[hitsInfoData[hitInfoId].matId].opacity + specular) *
                 ((isInside) ? 1.0f : NdotL) * radiance * absorption; 
    }

    return Lo;
}

#endif
)";

    std::string rtPipelineDescSetsPreproc = globalUniformLPreproc + hitInfoPreproc + shadowHitPreproc
        + hitPBRInfoPreproc + pbrPreproc;
    return OvGL::Shader::preprocessor.set("include Lighting", rtPipelineDescSetsPreproc);
}

#pragma endregion