/**
 * @file	overv_gl_pipeline_gui.h
 * @brief	GUI pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main inlcude:
#include "overv_gl.h"

// OGL:     
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// ImGui
#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"
#include "imgui_internal.h"
#include "imconfig.h"

// C/C++
#include <map>


////////////////
// STRUCTURES //
////////////////

/**
 * @brief GUI pipeline class reserved structure.
 */
struct OvGL::GUIPipeline::Reserved
{
    std::map<uint32_t, std::reference_wrapper<Ov::DrawGUI>> drawGUIObjects; ///< All the elemente that need to be draw.

    bool contextCreated = false;                                            ///< Context is created.
    bool rederingStarted = false;                                           ///< Rendernis started. To know witch action to do.

    /**
     * Constructor
     */
    Reserved() : contextCreated{ false },
        rederingStarted{ false }
    {}
};


///////////////////////////////
// BODY OF CLASS GUIPipeline //
///////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::GUIPipeline::GUIPipeline() :
    reserved(std::make_unique<OvGL::GUIPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVGL_API OvGL::GUIPipeline::GUIPipeline(const std::string& name) : Ov::Pipeline(name),
reserved(std::make_unique<OvGL::GUIPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::GUIPipeline::GUIPipeline(GUIPipeline&& other) :
    Ov::Pipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::GUIPipeline::~GUIPipeline()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes buffer.
 * @return TF
 */
bool OVGL_API OvGL::GUIPipeline::init()
{
    // init method base class call
    if (this->Ov::Pipeline::init() == false)
        return false;


    // Free ImGUI
    if (reserved->rederingStarted)
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
    }

    if (reserved->contextCreated)
        ImGui::DestroyContext();


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the OvVulkan buffer.
 * @return TF
 */
bool OVGL_API OvGL::GUIPipeline::free()
{
    // free method base class call
    if (this->Ov::Pipeline::free() == false)
        return false;


    // Free ImGUI
    if (reserved->rederingStarted)
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
    }

    if (reserved->contextCreated)
        ImGui::DestroyContext();


    // Done:
    return true;
}

#pragma endregion

#pragma region SurfaceDependencies

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the keyboard callback is forwarded.
 * @param key The keyboard key that was pressed or released.
 * @param scancode The system-specific scancode of the key.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
bool OVGL_API OvGL::GUIPipeline::keyboardCallback(int key, int scancode, int action, int mods)
{
    ImGui_ImplGlfw_KeyCallback((GLFWwindow*)Engine::getInstance().getGLFWWindow(),
        key, scancode, action, mods);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse cursor callback is forwarded.
 * @param mouseX The new cursor x-coordinate, relative to the left edge of the content area.
 * @param mouseY The new cursor y-coordinate, relative to the top edge of the content area.
 */
bool OVGL_API OvGL::GUIPipeline::mouseCursorCallback(double mouseX, double mouseY)
{
    ImGui_ImplGlfw_CursorPosCallback((GLFWwindow*)Engine::getInstance().getGLFWWindow(),
        mouseX, mouseY);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse button callback is forwarded.
 * @param button The mouse button that was pressed or released.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
bool OVGL_API OvGL::GUIPipeline::mouseButtonCallback(int button, int action, int mods)
{
    ImGui_ImplGlfw_MouseButtonCallback((GLFWwindow*)Engine::getInstance().getGLFWWindow(),
        button, action, mods);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse scroll callback is forwarded.
 * @param scrollX The scroll offset along the x-axis.
 * @param scrollY The scroll offset along the y-axis.
 */
bool OVGL_API OvGL::GUIPipeline::mouseScrollCallback(double scrollX, double scrollY)
{
    ImGui_ImplGlfw_ScrollCallback((GLFWwindow*)Engine::getInstance().getGLFWWindow(),
        scrollX, scrollY);

    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create gui pipeline.
 * @return TF
 */
bool OvGL::GUIPipeline::create()
{

    // Init
    if (this->init() == false)
        return false;

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL((GLFWwindow*)Engine::getInstance().getGLFWWindow(), true);
    ImGui_ImplOpenGL3_Init("#version 460");
    // Setup Dear ImGui style
    ImGui::StyleColorsDark();


    reserved->contextCreated = true;

    //Done:
    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds a Ov::Draw obj to the gui pipeline.
 * @param id The Ov::Draw obj id to add.
 * @return TF
 */
bool OVGL_API OvGL::GUIPipeline::addDrawGUIElement(uint32_t id, Ov::DrawGUI& object)
{
    std::reference_wrapper<Ov::DrawGUI> surfaceResizableObject = object;

    // Check if already present
    if (reserved->drawGUIObjects.count(id) == 0)
        reserved->drawGUIObjects.insert(std::make_pair(id, surfaceResizableObject));
    else
        OV_LOG_WARN("The ov draw obj is already present in gui pipeline with id= %d.", getId());

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Removes a Ov::Draw obj from the gui pipeline.
 * @param id The Ov::Draw obj id to remove.
 * @return TF
 */
bool OVGL_API OvGL::GUIPipeline::removeDrawGUIElement(uint32_t id)
{
    if (!reserved->drawGUIObjects.empty())
        for (auto it = reserved->drawGUIObjects.begin(); it != reserved->drawGUIObjects.end(); ++it)
            if (it->first == id)
            {
                reserved->drawGUIObjects.erase(it);
                break;
            }

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::GUIPipeline::render(uint32_t value, void* data)
{

    if (isDirty())
        this->setDirty(false);


    if (value == 0)
    {
        // feed inputs to dear imgui, start new frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        reserved->rederingStarted = true;
    }
    else if (value == 1)
    {
        // render your GUI
        ImGui::Begin("OvGL");

        for (auto [key, value] : reserved->drawGUIObjects)
            value.get().drawGUI();

        ImGui::End();

        // Render dear imgui into screen
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

    // Done:
    return true;
}

#pragma endregion