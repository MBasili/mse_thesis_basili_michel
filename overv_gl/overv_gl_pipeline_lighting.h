/**
 * @file	engine_pipeline_fullscreen2d.h
 * @brief	A pipeline for rendering a texture fullscreen in 2D
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) SUPSI, Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


 /**
  * @brief Fullscreen rendering 2D.
  */
class OVGL_API PipelineLighting : public Ov::Pipeline, public Ov::DrawGUI
{
//////////
public: //
//////////

    /**
     * @brief Global struct ray tracing
     */
    struct GlobalUniformLData
    {
        uint32_t nrOfRecursion;
        uint32_t isHDRCorrectionEnabled;
        uint32_t useNDFStandard;
        uint32_t scaleReflectionInvRoughness;
        uint32_t showProblemNDFFunction;
        uint32_t useRoghnessTexture;
        uint32_t useMetalnessTexture;
        uint32_t useAlbedoTexture;
        uint32_t useNormalMapForReflection;
        uint32_t useNormalMapForRefraction;
        glm::vec2 _pad0;


        GlobalUniformLData() : isHDRCorrectionEnabled{ 0 },
            useNDFStandard{ 0 },
            scaleReflectionInvRoughness{ 0 },
            showProblemNDFFunction{ 0 }
        {}

        static std::string getShaderStruct()
        {
            return R"(
struct GlobalUniformLData
{
    uint nrOfRecursion;
    bool isHDRCorrectionEnabled;
    bool useNDFStandard;
    bool scaleReflectionInvRoughness;
    bool showProblemNDFFunction;
    bool useRoghnessTexture;
    bool useMetalnessTexture;
    bool useAlbedoTexture;
    bool useNormalMapForReflection;
    bool useNormalMapForRefraction;
    vec2 _pad0;
};
)";
        }
    };

    /**
     * @brief Struct containing the value of the PBR of a hit point.
     */
    struct HitPBRInfoData
    {
        glm::vec4 intermediateLo;
        glm::vec4 finalLo;
        uint32_t hitInfoId;
        uint32_t reflectHitPBRInfoDataPos;
        uint32_t reflractHitPBRInfoDataPos;
        uint32_t isFinalValueComputed;

        HitPBRInfoData() : intermediateLo{0.0f}, finalLo{0.0f}, hitInfoId{0},
			reflectHitPBRInfoDataPos{0}, reflractHitPBRInfoDataPos{0}, isFinalValueComputed{0}
        {}

        static std::string getShaderStruct()
        {
            return R"(
struct HitPBRInfoData
{
	vec4 intermediateLo;
	vec4 finalLo;
	uint hitInfoId;
	uint reflectHitPBRInfoDataPos;
	uint reflractHitPBRInfoDataPos;
};
)";
        }
    };

	// Const/dest:
	PipelineLighting();
	PipelineLighting(PipelineLighting&& other);
	PipelineLighting(PipelineLighting const&) = delete;
	virtual ~PipelineLighting();

	// Rendering methods:
	// bool render(uint32_t value = 0, void *data = nullptr) const = delete;
	bool render(const OvGL::PipelineRayTracing& raytracing, const Ov::List& list);

    // DrawGUI
    virtual void drawGUI() override;

	// Managed:
	bool init() override;
	bool free() override;

	// Get/Set:
	const OvGL::Texture2D& getOutputBuffer() const;

	// Shader:
	static bool addShaderPreproc();

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	PipelineLighting(const std::string& name);
};
