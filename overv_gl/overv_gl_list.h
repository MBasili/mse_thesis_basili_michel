/**
 * @file	overv_gl_list.h
 * @brief	OpenGL list properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief Class for handling lists.
  */
class OVGL_API List final : public Ov::List, public Ov::DrawGUI
{
//////////
public: //
//////////

	// Special values:
	static List empty;

	// Const/dest:
	List();
	List(List&& other) noexcept;
	List(List const&) = delete;
	virtual ~List();

	// Rendering:
	bool render(const glm::mat4& cameraMatrix, const glm::mat4& projectionMatrix, Ov::List::Type passType = Ov::List::Type::all) const override;

	// DrawGUI
	virtual void drawGUI() override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	List(const std::string& name);
};