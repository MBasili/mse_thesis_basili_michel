/**
 * @file	overv_gl_material.h
 * @brief	OpenGL material properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once



/**
 * @brief Class for modeling a generic material.
 */
class OVGL_API Material final : public Ov::MaterialPbr, public Ov::DrawGUI
{
//////////
public: //
//////////   

	// Special values:
	static Material empty;
	constexpr static uint32_t maxNrOfMaterials = 256;           ///< Max number of concurrent materials
	constexpr static uint32_t maxNrOfTextures = 5;     ///< Max number of textures per material

	// Const/dest:
	Material();
	Material(Material&& other) noexcept;
	Material(Material const&) = delete;
	virtual ~Material();

	// Default materials:
	static const Material& getDefault();

	// Managed:
	bool init() override;
	bool free() override;

	// Get/set:   
	uint32_t getLutPos() const;
	const OvGL::Texture2D& getTexture(Ov::Texture::Type type = Ov::Texture::Type::diffuse) const;

	bool setTexture(const OvGL::Texture2D& tex, Ov::Texture::Type type = Ov::Texture::Type::diffuse);

	// Texture
	virtual bool loadTexture(const std::string& textureName, Ov::Texture::Type type, Ov::Container& container) override;

	// Synch data:
	bool storageUpload() const;

	// Rendering methods:   
	bool render(uint32_t value = 0, void* data = nullptr) const override;

	// Gui draw
	virtual void drawGUI() override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Material(const std::string& name);

	// LUT:
	static bool lut[maxNrOfMaterials];
};
