/**
 * @file	overv_gl_arraybuffer.h
 * @brief	OpenGL array buffer
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


/**
 * @brief OpenGL array buffer
 */
class OVGL_API ArrayBuffer final : public OvGL::Buffer
{
//////////
public: //
//////////

	/**
	 * @brief Definition of vertex attrib layers.
	 */
	enum class Attrib : uint32_t
	{
		vertex,
		normal,
		texcoord,
		tangent,
		color,
		boneindex,
		boneweight,
		
		// Terminator:
		last
	};

	// Const/dest:
	ArrayBuffer();
	ArrayBuffer(ArrayBuffer&& other) noexcept;
	ArrayBuffer(ArrayBuffer const&) = delete;
	~ArrayBuffer();

	// Managed:
	bool init() override;
	bool free() override;

	// Get/Set
	uint32_t getNrOfVertices() const;

	// Management:
	bool create(uint64_t nrOfVertices, const void* data = nullptr,
		bool vertexFlag = true, bool normalFlag = true, bool uvFlag = true, bool tangentFlag = true);

/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	ArrayBuffer(const std::string& name);
};