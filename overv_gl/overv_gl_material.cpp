/**
 * @file	overv_gl_material.cpp
 * @brief	OpenGL material properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>

// ImGui
#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"
#include "imgui_internal.h"
#include "imconfig.h"


////////////
// STATIC //
////////////

// Special values:   
OvGL::Material OvGL::Material::empty("[empty]");

// Look-up table:
bool OvGL::Material::lut[OvGL::Material::maxNrOfMaterials];


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Material reserved structure.
 */
struct OvGL::Material::Reserved
{
    uint64_t texHandles[OvGL::Material::maxNrOfTextures];    ///< Texture bindless handles  

    int lutPos;        ///< Position in the lut  
    std::reference_wrapper<const OvGL::Texture2D> textures[OvGL::Material::maxNrOfTextures];


    /**
     * Constructor.
     */
    Reserved() : texHandles { 0, 0, 0, 0, 0},
        lutPos{ maxNrOfMaterials },
        textures{ OvGL::Texture2D::empty, OvGL::Texture2D::empty, OvGL::Texture2D::empty, OvGL::Texture2D::empty, OvGL::Texture2D::empty }
    {
        // Find a free slot in the LUT:
        for (uint32_t c = 0; c < maxNrOfMaterials; c++)
            if (lut[c] == false)
            {
                lut[c] = true;
                lutPos = c;
                break;
            }
        if (lutPos == maxNrOfMaterials)
            OV_LOG_ERROR("Too many materials created");
    }


    /**
     * Destructor.
     */
    ~Reserved()
    {
        if (lutPos != maxNrOfMaterials)
            lut[lutPos] = false;
    }
};


////////////////////////////
// BODY OF CLASS Material //
////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Material::Material() : reserved(std::make_unique<OvGL::Material::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the material.
 */
OVGL_API OvGL::Material::Material(const std::string& name) : Ov::MaterialPbr(name), reserved(std::make_unique<OvGL::Material::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Material::Material(Material&& other) noexcept : Ov::MaterialPbr(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Material::~Material()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Default

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets default material, which is instatiated on first usage.
 * @return default material
 */
const OvGL::Material OVGL_API& OvGL::Material::getDefault()
{
    static Material dfltMaterial("[default]");
    dfltMaterial.setAlbedo(glm::vec3(1.0f, 0.0f, 0.0f));
    return dfltMaterial;
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes an OpenGL material. Not really used, just a placeholder.
 * @return TF
 */
bool OVGL_API OvGL::Material::init()
{
    if (this->Ov::MaterialPbr::init() == false)
        return false;

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases an OpenGL material. Not really used, just a placeholder.
 * @return TF
 */
bool OVGL_API OvGL::Material::free()
{
    if (this->Ov::MaterialPbr::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get position of this material in the LUT.
 * @return LUT position
 */
uint32_t OVGL_API OvGL::Material::getLutPos() const
{
    return reserved->lutPos;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets texture.
 * @param type texture level
 * @return texture at level
 */
const OvGL::Texture2D OVGL_API& OvGL::Material::getTexture(Ov::Texture::Type type) const
{
    // Get texture:
    switch (type)
    {
    case Ov::Texture::Type::albedo:
        return reserved->textures[0];

    case Ov::Texture::Type::normal:
        return reserved->textures[1];

    case Ov::Texture::Type::height:
        return reserved->textures[2];

    case Ov::Texture::Type::roughness:
        return reserved->textures[3];

    case Ov::Texture::Type::metalness:
        return reserved->textures[4];

    default:
        OV_LOG_ERROR("Unsupported texture level");
        return OvGL::Texture2D::empty;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets texture.
 * @param tex texture
 * @param type texture level
 * @return TF
 */
bool OVGL_API OvGL::Material::setTexture(const OvGL::Texture2D& tex, Ov::Texture::Type type)
{
    // Safety net:
    if (tex == OvGL::Texture2D::empty) {
        OV_LOG_ERROR("The passed texture is the empty texture in material with id= %d", getId());
        return false;
    }

    switch (type)
    {
    case Ov::Texture::Type::albedo:
        reserved->textures[0] = tex;
        reserved->texHandles[0] = tex.getOglBindlessHandle();
        setDirty(true);
        break;
    case Ov::Texture::Type::normal:
        reserved->textures[1] = tex;
        reserved->texHandles[1] = tex.getOglBindlessHandle();
        setDirty(true);
        break;
    case Ov::Texture::Type::height:
        reserved->textures[2] = tex;
        reserved->texHandles[2] = tex.getOglBindlessHandle();
        setDirty(true);
        break;
    case Ov::Texture::Type::roughness:
        reserved->textures[3] = tex;
        reserved->texHandles[3] = tex.getOglBindlessHandle();
        setDirty(true);
        break;
    case Ov::Texture::Type::metalness:
        reserved->textures[4] = tex;
        reserved->texHandles[4] = tex.getOglBindlessHandle();
        setDirty(true);
        break;

    default:
        OV_LOG_ERROR("Unsupported texture level");
        return false;
    }

    Ov::MaterialPbr::setTexture(tex, type);

    // Done:
    return true;
}

#pragma endregion

#pragma region Texture

bool OvGL::Material::loadTexture(const std::string& textureName, Ov::Texture::Type type, Ov::Container& container) {

    // Cast container to current engine container definition
    std::reference_wrapper<OvGL::Container> loadingContainer = static_cast<OvGL::Container&>(container);

    // Texture.
    std::reference_wrapper<const OvGL::Texture2D> textureToSet = OvGL::Texture2D::empty;

    // Search for the texture
    Ov::Object& textureFound = loadingContainer.get().find(textureName);
    if (textureFound == Ov::Object::empty)
    {
        OvGL::Texture2D texture;

        // Set default texture if maximum number is reached.
        if (texture.getLutPos() == OvGL::Texture::maxNrOfTextures)
        {
            OV_LOG_ERROR("Too many texture, the default texture2D is returned.");
            textureToSet = OvGL::Texture2D::getDefault();
        }
        else
        {
            // Load bitmap
            OvGL::Bitmap bitmap;

            // Check extension 
            bool loadingResult = false;
            if (textureName.substr(textureName.find_last_of(".") + 1) == "dds") {
                loadingResult = bitmap.loadDDS(OvGL::Engine::texturesSourceFolder + "\\" + textureName);
            }
            if (textureName.substr(textureName.find_last_of(".") + 1) == "ktx") {

                Ov::Bitmap::Format formatToLoad = Ov::Bitmap::Format::r8g8b8a8_compressed;

                switch (type) {
                case Ov::Texture::Type::metalness:
                case Ov::Texture::Type::roughness:
                case Ov::Texture::Type::height:
                    formatToLoad = Ov::Bitmap::Format::r8_compressed;
                    break;
                case Ov::Texture::Type::normal:
                    formatToLoad = Ov::Bitmap::Format::r8g8_compressed;
                    break;
                default:
                    break;
                }

                loadingResult = bitmap.loadKTX(OvGL::Engine::texturesSourceFolder + "\\" + textureName, formatToLoad);
            }
            else {
                loadingResult = bitmap.loadWithFreeImage(OvGL::Engine::texturesSourceFolder + "\\" + textureName);
            }

            if (loadingResult) 
            {
                bool isLinearEncoded = true;
                if (type == Ov::Texture::Type::albedo) {
                    isLinearEncoded = false;
                }

                if (texture.load(bitmap, isLinearEncoded))
                {
                    // Add texture :
                    texture.setName(textureName);
                    if (loadingContainer.get().add(texture))
                        textureToSet = loadingContainer.get().getLastTexture2D();
                    else
                        OV_LOG_ERROR("Fail to add the texture with id= %d into a container in the material with id= %d.", texture.getId(), getId());
                }
                else
                {
                    OV_LOG_ERROR("Fail to load image with name %s into a texture in the material with id= %d", textureName, getId());
                }
            }
            else
            {
                OV_LOG_ERROR("Fail to load image with name %s into a bitmap in the material with id= %d", textureName, getId());
            }
        }
    }
    else
    {
        textureToSet = dynamic_cast<OvGL::Texture2D&>(textureFound);
    }

    return setTexture(textureToSet, type);
}

#pragma endregion

#pragma region Sync

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Synch material data to GPU.
 * @return TF
 */
bool OVGL_API OvGL::Material::storageUpload() const
{
    // Apply mat props to OpenGL:
    OvGL::Storage::MaterialData* mat = OvGL::Storage::getInstance().getMaterialData(reserved->lutPos);

    mat->emission = glm::vec4(getEmission(), 0.0f);
    mat->albedo = glm::vec4(getAlbedo(), 0.0f);
    mat->opacity = getOpacity();
    mat->roughness = getRoughness();
    mat->metalness = getMetalness();
    mat->refractionIndex = getRefractionIndex();
    mat->rimIntensity = getRimIntensity();
    mat->absorptionThreshold = getAbsorptionThreshold();

    uint8_t* matTexture = reinterpret_cast<uint8_t*>(mat);
    matTexture += 64;
    memcpy(matTexture, reserved->texHandles, OvGL::Material::maxNrOfTextures * sizeof(uint64_t)); // Copy additional 32 bytes with texture info

    // Done:
    return true;

}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Material::render(uint32_t value, void* data) const
{
    // Sanity check:
    if (reserved->lutPos == maxNrOfMaterials)
    {
        OV_LOG_ERROR("Invalid LUT entry for material '%s', id %u", this->getName().c_str(), this->getId());
        return false;
    }

    // If value is non-zero, perform a data synch instead:
    if (value)
        return storageUpload();

    // Force refresh if dirty:
    if (isDirty())
    {
        if (OvGL::Storage::getInstance().addToUpdateList(*this) == false)
        {
            OV_LOG_ERROR("Fail to upload material into the engine storage.");
            return false;
        }

        setDirty(false);
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Draw the gui for a material.
 */
void OVGL_API OvGL::Material::drawGUI()
{
    // Emissions
    //glm::vec3 emission = getEmission();
    //ImGui::SliderFloat3("Emission color", glm::value_ptr(emission), 0.0f, 1.0f);
    //setEmission(emission);

	// Opacity
	float opacity = getOpacity();
	ImGui::SliderFloat("Opacity", &opacity, 0.0f, 1.0f);
	setOpacity(opacity);

	// Albedo
	//if (getTexture(Ov::Texture::Type::albedo) == OvGL::Texture2D::empty)
	//{
	glm::vec3 albedo = getAlbedo();
	ImGui::SliderFloat3("Albedo color", glm::value_ptr(albedo), 0.0f, 1.0f);
	setAlbedo(albedo);
	//}

	// RefractionIndex
	float refractionIndex = getRefractionIndex();
	ImGui::SliderFloat("Refraction index", &refractionIndex, 0.0f, 3.0f);
	setRefractionIndex(refractionIndex);

	// RimIntensity
	//float rimIntensity = getRimIntensity();
	//ImGui::SliderFloat("Rim intensity", &rimIntensity, 0.01f, 1.0f);
	//setRimIntensity(rimIntensity);

	// Roughness
	//if (getTexture(Ov::Texture::Type::roughness) == OvGL::Texture2D::empty)
	//{
	float roughness = getRoughness();
	ImGui::SliderFloat("Roughness", &roughness, 0.0f, 1.0f);
	setRoughness(roughness);
	//}

	// Metalness
	//if (getTexture(Ov::Texture::Type::metalness) == OvGL::Texture2D::empty)
	//{
	float metalness = getMetalness();
	ImGui::SliderFloat("Metalness", &metalness, 0.0f, 1.0f);
	setMetalness(metalness);
	//}

	// absorption threshold:
	//float absorptionThreshold = getAbsorptionThreshold();
	//ImGui::SliderFloat("Absorption Threashold", &absorptionThreshold, 0.0f, 1.0f);
	//setAbsorptionThreshold(absorptionThreshold);
}

#pragma endregion