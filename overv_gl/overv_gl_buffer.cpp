/**
 * @file	overv_gl_buffer.cpp
 * @brief	OpenGL buffers
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 *          Michel Basili (michel.basili@supsi.ch), 2021 - 2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Buffer reserved structure.
 */
struct OvGL::Buffer::Reserved
{
    GLuint oglId;   ///< OpenGL handle
    GLsync syncObj; ///< OGL sync object


    /**
     * Constructor.
     */
    Reserved() : oglId{ 0 }, syncObj{ nullptr }
    {}
};


//////////////////////////
// BODY OF CLASS Buffer //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Buffer::Buffer() : reserved(std::make_unique<OvGL::Buffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the buffer.
 */
OVGL_API OvGL::Buffer::Buffer(const std::string& name) : Ov::Buffer(name),
reserved(std::make_unique<OvGL::Buffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Buffer::Buffer(Buffer&& other) noexcept: Ov::Buffer(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Buffer::~Buffer()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Buffer::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Free buffer if already stored:
    if (reserved->oglId)
    {
        glDeleteBuffers(1, &reserved->oglId);
        reserved->oglId = 0;
        this->Ov::Buffer::setSize(0);
    }

    // Create it:		    
    glGenBuffers(1, &reserved->oglId);


    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Buffer::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Free texture if stored:
    if (reserved->oglId)
    {
        glDeleteBuffers(1, &reserved->oglId);
        reserved->oglId = 0;
        this->Ov::Buffer::setSize(0);
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region Sync

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Lock buffer by putting a fence.
 * @return TF
 */
bool OVGL_API OvGL::Buffer::lock()
{
    // Only keep the latest lock:
    if (reserved->syncObj)
        glDeleteSync(reserved->syncObj);

    reserved->syncObj = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Unlock buffer using a fence.
 * @return TF
 */
bool OVGL_API OvGL::Buffer::unlock()
{
    // As multiple lock() calls are allowed, only the last one will be considered:
    if (reserved->syncObj == nullptr)
        return true;

    // First round, low cost:
    GLenum result = glClientWaitSync(reserved->syncObj, 0, 0ULL);
    if (result == GL_ALREADY_SIGNALED || result == GL_CONDITION_SATISFIED)
    {
        glDeleteSync(reserved->syncObj);
        reserved->syncObj = nullptr;
        return true;
    }

    // Second round, expensive:
    result = glClientWaitSync(reserved->syncObj, GL_SYNC_FLUSH_COMMANDS_BIT, 100000000ULL);
    glDeleteSync(reserved->syncObj);
    reserved->syncObj = nullptr;

    // Check results:
    switch (result)
    {
        ////////////////////////////
    case GL_ALREADY_SIGNALED: //
    case GL_CONDITION_SATISFIED:
        return true;

        ///////////////////////////
    case GL_TIMEOUT_EXPIRED: //
        OV_LOG_ERROR("Timeout triggered by fence");
        return false;

        ///////////
    default: //
        OV_LOG_ERROR("Unhandled case");
        return false;
    }
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the GLuint buffer ID.
 * @return Buffer ID or 0 if not valid.
 */
uint32_t OVGL_API OvGL::Buffer::getOglHandle() const
{
    return reserved->oglId;
}

#pragma endregion