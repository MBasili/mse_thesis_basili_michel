/**
 * @file	overv_gl_storagebufferobj.h
 * @brief	OpenGL storage buffer object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief OpenGL storage buffer object
  */
class OVGL_API StorageBufferObj final : public OvGL::Buffer, Ov::Renderable
{
//////////
public: //
//////////

	// Static
	static StorageBufferObj empty;

	// Const/dest:
	StorageBufferObj();
	StorageBufferObj(StorageBufferObj&& other) noexcept;
	StorageBufferObj(StorageBufferObj const&) = delete;
	~StorageBufferObj();

	// Get/set:
	static uint64_t getMaxSize();
	void* getDataPtr() const;

	// Managed:
	bool init() override;
	bool free() override;

	// Management:
	static void reset();
	bool create(uint64_t nrOfBytes, void* data = nullptr);

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	StorageBufferObj(const std::string& name);
};
