/**
 * @file	overv_gl_light.h
 * @brief	OpenGL light properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once

/**
 * @brief Class for modeling an OpenGL light.
 */
class OVGL_API Light : public Ov::Light, public Ov::DrawGUI
{
//////////
public: //
//////////

	// Consts:
	constexpr static uint32_t maxNrOfLights = 16;    ///< Max number of lights that can be rendered per frame

	// Types of lights
	enum class Type : uint32_t {
		none = 0,
		directional = 1,
		spot = 2,
		omni = 3,
		all
	};

	// Special values:
	static Light empty;

	// Const/dest:
	Light();
	Light(Light&& other) noexcept;
	Light(Light const&) = delete;
	virtual ~Light();

	// Get/set:
	Type getLightType() const;
	float getPower() const;
	float getSpotCutoff() const;
	float getSpotExponent() const;
	glm::vec3 getDirection() const;

	void setLightType(Type lightType);
	void setPower(float power);
	void setSpotCutoff(float cutOff);
	void setSpotExponent(float exponent);
	void setDirection(glm::vec3 direction);

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

	// Draw GUI
	virtual void drawGUI() override;

	// Object-specific chunk save function:
	bool saveChunk(Ov::Serializer& serial) const override;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr) override;

///////////
private: //
///////////   

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Light(const std::string& name);
};
