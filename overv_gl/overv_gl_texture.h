/**
 * @file	overv_gl_texture.h
 * @brief	OpenGL texture properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


 /**
  * @brief Class for modeling an OpenGL texture.
  */
class OVGL_API Texture : public Ov::Texture
{
//////////
public: //
//////////

    // Special values:
	static Texture empty;
	constexpr static uint32_t maxNrOfTextures = 500;	///< Max number of textures per material

	// Const/dest:
	Texture();
	Texture(Texture&& other) noexcept;
	Texture(Texture const&) = delete;
	virtual ~Texture();

	// Managed:
	bool init() override;
	bool free() override;

	// Get/set:   
	uint32_t getLutPos() const;
	uint32_t getOglHandle() const;
	uint64_t getOglBindlessHandle() const;
	uint64_t getInternalFormat() const;

	void setInternalFormat(uint64_t format);

	// Rendering methods:
	virtual bool render(uint32_t value = 0, void* data = nullptr) const override;
	virtual bool bindImage(uint32_t location = 0) const;

/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Texture(const std::string& name);

	// Internal memory manager:   
	bool makeResident();

	// LUT:
	static bool lut[OvGL::Texture::maxNrOfTextures];
};