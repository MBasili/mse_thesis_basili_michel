/**
 * @file	overv_gl_geometry.h
 * @brief	OGL geometric data
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"


/**
 * @brief Geometric mesh class.
 */
class OVGL_API Geometry : public Ov::Geometry
{
//////////
public: //
//////////	   

    // Special values:
    static Geometry empty;

    // Consts:
    constexpr static uint32_t maxNrOfGeometries = 2048;    ///< Max number of geometries that can be rendered per frame

    // Const/Dest:
    Geometry();
    Geometry(Geometry&& other) noexcept;
    Geometry(Geometry const&) = delete;
    virtual ~Geometry();

    // Operators:
    void operator=(Geometry const&) = delete;

    // Managed:
    bool init() override;
    bool free() override;

    // Get/set:
    const OvGL::ArrayBuffer& getVertexBuffer() const;
    const OvGL::ElemArrayBuffer& getFaceBuffer() const;

    // Synch data:
    bool upload();

    // Rendering methods:
    bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////

    // Reserved:
    struct Reserved;
    std::unique_ptr<Reserved> reserved;

    // Const/dest:
    Geometry(const std::string& name);
};