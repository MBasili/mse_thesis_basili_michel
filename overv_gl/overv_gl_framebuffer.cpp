/**
 * @file	overv_gl_framebuffer.cpp
 * @brief	OpenGL framebuffer properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Framebuffer reserved structure.
 */
struct OvGL::Framebuffer::Reserved
{
    GLuint oglId;              ///< OpenGL framebuffer ID   
    std::vector<GLenum> mrt;   ///< List of multiple rendering target


    /**
     * Constructor.
     */
    Reserved() : oglId{ 0 }
    {}
};


///////////////////////////////
// BODY OF CLASS Framebuffer //
///////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Framebuffer::Framebuffer() : reserved(std::make_unique<OvGL::Framebuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the framebuffer.
 */
OVGL_API OvGL::Framebuffer::Framebuffer(const std::string& name) : Ov::Framebuffer(name),
reserved(std::make_unique<OvGL::Framebuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Framebuffer::Framebuffer(Framebuffer&& other) noexcept : Ov::Framebuffer(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Framebuffer::~Framebuffer()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::init() {
    if (this->Ov::Managed::init() == false)
        return false;

    // Free texture if already stored:
    if (reserved->oglId)
    {
        glDeleteFramebuffers(1, &reserved->oglId);
        reserved->oglId = 0;
    }

    // Create it:		    
    glGenFramebuffers(1, &reserved->oglId);

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::free() {
    if (this->Ov::Managed::free() == false)
        return false;

    // Delete and remove render buffers:
    for (uint32_t c = 0; c < this->getNrOfAttachments(); c++)
    {
        Ov::Framebuffer::Attachment& att = this->getAttachment(c);
        switch (att.type)
        {
            ////////////////////////////////////////////////////////
        case Ov::Framebuffer::Attachment::Type::color_buffer: //
        case Ov::Framebuffer::Attachment::Type::depth_buffer:
            GLuint oglId = static_cast<GLuint>(att.data);
            glDeleteRenderbuffers(1, &oglId);
            break;
        }
    }
    for (uint32_t c = 0; c < this->getNrOfAttachments(); c++)
        this->remove(c);

    // Free framebuffer if used:
    if (reserved->oglId)
    {
        glDeleteFramebuffers(1, &reserved->oglId);
        reserved->oglId = 0;
    }

    // Done:   
    return true;
}

#pragma endregion

#pragma region Attachments

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a texture in the next slot of the framebuffer.
 * @param texture texture
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::attachTexture(const Ov::Texture& texture, uint32_t level, uint32_t side)
{
    if (this->Ov::Framebuffer::attachTexture(texture, level, side) == false)
        return false;

    uint32_t attId = this->getNrOfAttachments() - 1;
    Ov::Framebuffer::Attachment& att = this->getAttachment(attId);
    const OvGL::Texture& ovglTexture = dynamic_cast<const OvGL::Texture&>(texture);

    if (attId == 0)
        this->init();

    glBindFramebuffer(GL_FRAMEBUFFER, reserved->oglId);
    switch (texture.getFormat())
    {
        ////////////////////////////////////
    case Ov::Texture::Format::r8g8b8: //
    case Ov::Texture::Format::r8g8b8a8:
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attId, GL_TEXTURE_2D, ovglTexture.getOglHandle(), 0);
        break;

        ///////////////////////////////////
    case Ov::Texture::Format::depth: //
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, ovglTexture.getOglHandle(), 0);
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Unsupported texture format");
        return false;
    }

    // Done:   
    return updateMrtCache();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a color render buffer.
 * @param sizeX width
 * @param sizeY height
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::attachColorBuffer(uint32_t sizeX, uint32_t sizeY)
{
    if (this->Ov::Framebuffer::attachColorBuffer(sizeX, sizeY) == false)
        return false;

    uint32_t attId = this->getNrOfAttachments() - 1;
    Ov::Framebuffer::Attachment& att = this->getAttachment(attId);

    GLuint oglId = 0;
    glGenRenderbuffers(1, &oglId);
    glBindRenderbuffer(GL_RENDERBUFFER, oglId);

    // @TODO, color attachments are pretty non-sense

    // Done:   
    return false;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a depth render buffer.
 * @param sizeX width
 * @param sizeY height
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::attachDepthBuffer(uint32_t sizeX, uint32_t sizeY)
{
    if (this->Ov::Framebuffer::attachDepthBuffer(sizeX, sizeY) == false)
        return false;

    uint32_t attId = this->getNrOfAttachments() - 1;
    Ov::Framebuffer::Attachment& att = this->getAttachment(attId);

    // Generate render buffer:
    GLuint oglId = 0;
    glGenRenderbuffers(1, &oglId);
    glBindRenderbuffer(GL_RENDERBUFFER, oglId);

    // Attach renderbuffer:
    glBindFramebuffer(GL_FRAMEBUFFER, reserved->oglId);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, sizeX, sizeY);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, oglId);

    // Done:   
    att.data = oglId;
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Update the MRT cache.
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::updateMrtCache()
{
    reserved->mrt.clear();
    for (uint32_t c = 0; c < this->getNrOfAttachments(); c++)
        switch (this->getAttachment(c).type)
        {
            /////////////////////////////////////////////////////////
        case Ov::Framebuffer::Attachment::Type::color_texture: //
            reserved->mrt.push_back(GL_COLOR_ATTACHMENT0 + c);
            break;
        }

    // Done: 
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Validate the state of the framebuffer.
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::validate() const
{
    // Additional check (for attachments of different size, which I think are ok according to the specs):
    bool throwWarning = false;
    if (this->getNrOfAttachments() > 1)
        for (uint32_t c = 1; c < this->getNrOfAttachments(); c++)
            if (this->getAttachment(c).size != this->getAttachment(0).size)
                throwWarning = true;
    if (throwWarning)
        OV_LOG_WARN("Attachments have different size");

    glBindFramebuffer(GL_FRAMEBUFFER, reserved->oglId);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        OV_LOG_ERROR("Framebuffer not complete (error: %u)", status);
        return false;
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Detach framebuffer: rendering is done on the main context buffers.
 * @param viewportSizeX width of the viewport
 * @param viewportSizeY height of the viewport
 */
void OVGL_API OvGL::Framebuffer::reset(uint32_t viewportSizeX, uint32_t viewportSizeY)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, viewportSizeX, viewportSizeY);

    // Update global cache:
    Ov::Framebuffer::setCurSizeX(viewportSizeX);
    Ov::Framebuffer::setCurSizeY(viewportSizeY);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Blits directly from FBO. FBO and main buffer size must match.
 * @param viewportSizeX width of the viewport
 * @param viewportSizeY height of the viewport
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::blit(uint32_t viewportSizeX, uint32_t viewportSizeY) const
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, reserved->oglId);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, getSizeX(), getSizeY(),
        0, 0, viewportSizeX, viewportSizeY,
        GL_COLOR_BUFFER_BIT, GL_NEAREST);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Framebuffer::render(uint32_t value, void* data) const
{
    // Safety net:
    if (this->getNrOfAttachments() == 0)
    {
        OV_LOG_ERROR("No attachments available");
        return false;
    }

    // Bind buffers:
    glBindFramebuffer(GL_FRAMEBUFFER, reserved->oglId);
    const GLsizei nrOfMrts = static_cast<GLsizei>(reserved->mrt.size());
    if (nrOfMrts)
    {
        glDrawBuffers(nrOfMrts, reserved->mrt.data());
    }
    glViewport(0, 0, getSizeX(), getSizeY());

    // Update global cache:
    Ov::Framebuffer::setCurSizeX(getSizeX());
    Ov::Framebuffer::setCurSizeY(getSizeY());

    // Done:
    return true;
}

#pragma endregion