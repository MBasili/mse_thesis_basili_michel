/**
 * @file	overv_gl_bitmap.cpp
 * @brief	Basic generic bitmap properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

 // Main include:
#include "overv_gl.h"

// Ktx include
#include "ktx.h"

// Magic enum
#include "magic_enum.hpp"

// C/C++
#include <functional>


////////////
// STATIC //
////////////

// Special values:
OvGL::Bitmap OvGL::Bitmap::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Bitmap reserved structure.
 */
struct OvGL::Bitmap::Reserved
{
	/**
	 * Constructor.
	 */
	Reserved()
	{}
};


//////////////////////////
// BODY OF CLASS Bitmap //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Bitmap::Bitmap() : reserved(std::make_unique<OvGL::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the bitmap
 */
OVGL_API OvGL::Bitmap::Bitmap(const std::string& name) : Ov::Image(name),
reserved(std::make_unique<OvGL::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Bitmap::Bitmap(Bitmap&& other) noexcept : Ov::Image(std::move(other)),
reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Bitmap::~Bitmap()
{
	OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Loader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load image from a .ktx file.
 * @param filename ktx file name
 * @param format to load
 * @return TF
 */
bool OVGL_API OvGL::Bitmap::loadKTX(const std::string& filename, Format loadFormat)
{
	// Safety net:
	if (filename.empty())
	{
		OV_LOG_ERROR("Invalid params");
		return false;
	}

	// Clear
	if (!Ov::Bitmap::clear())
	{
		OV_LOG_ERROR("Fail to clear bitmap with id= %d.", getId());
		return false;
	}

	// Done:
	return true;
}

#pragma endregion

#pragma region Save

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Save image to a .ktx file.
 * @param preserveHighQuality determine which type of compression to use.
 * @return TF
 */
bool OVGL_API OvGL::Bitmap::saveAsKTX(bool supercompress, bool preserveHighQuality) const
{
	// Define struct to create ktxtecture
	ktxTextureCreateInfo createInfo;

	// Done:
	return true;
}

#pragma endregion