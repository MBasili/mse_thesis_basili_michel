/**
 * @file	overv_gl_uniformbufferobj.cpp
 * @brief	OpenGL uniform buffer object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


 //////////////
 // #INCLUDE //
 //////////////

 // Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief UniformBufferObj reserved structure.
 */
struct OvGL::UniformBufferObj::Reserved
{
    void* dataPtr;       ///< Mapped data pointer

    /**
     * Constructor.
     */
    Reserved() : dataPtr{ nullptr }
    {}
};


////////////////////////////////////
// BODY OF CLASS UniformBufferObj //
////////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::UniformBufferObj::UniformBufferObj() : reserved(std::make_unique<OvGL::UniformBufferObj::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the uniform buffer
 */
OVGL_API OvGL::UniformBufferObj::UniformBufferObj(const std::string& name) : Ov::Renderable(name), OvGL::Buffer(name),
reserved(std::make_unique<OvGL::UniformBufferObj::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::UniformBufferObj::UniformBufferObj(UniformBufferObj&& other) noexcept : Ov::Renderable(std::move(other)),
OvGL::Buffer(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::UniformBufferObj::~UniformBufferObj()
{
    OV_LOG_DETAIL("[-]");

    if (reserved->dataPtr)
    {
        const GLuint oglId = this->getOglHandle();
        glBindBuffer(GL_UNIFORM_BUFFER, oglId);
        glUnmapBuffer(GL_UNIFORM_BUFFER);
    }
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Unbind any uniform buffer object.
 */
void OVGL_API OvGL::UniformBufferObj::reset()
{
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create buffer by allocating the required storage.
 * @param nfOfBytes number of bytes to store
 * @param data (optional) initialization data
 * @return TF
 */
bool OVGL_API OvGL::UniformBufferObj::create(uint64_t nrOfBytes, void* data)
{
    // Init buffer:
    if (!this->isInitialized())
        this->init();

    uint64_t size = nrOfBytes;
    this->OvGL::Buffer::setSize(size);

    // Create it:		              
    const GLuint oglId = this->getOglHandle();
    glBindBuffer(GL_UNIFORM_BUFFER, oglId);
    GLbitfield flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
    glBufferStorage(GL_UNIFORM_BUFFER, nrOfBytes, data, flags);

    // Map buffer forever:
    reserved->dataPtr = glMapBufferRange(GL_UNIFORM_BUFFER, 0, nrOfBytes, flags);

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get UBO max size in bytes.
 * @return max UBO size in bytes
 */
uint64_t OVGL_API OvGL::UniformBufferObj::getMaxSize()
{
    GLint size;
    glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &size);
    return static_cast<uint64_t>(size);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get access to the mapped data.
 * @return pointer to mapped data
 */
void OVGL_API* OvGL::UniformBufferObj::getDataPtr() const
{
    return reserved->dataPtr;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data GLuint pointer to the binding index
 * @return TF
 */
bool OVGL_API OvGL::UniformBufferObj::render(uint32_t value, void* data) const
{
    const GLuint oglId = this->getOglHandle();
    glBindBufferBase(GL_UNIFORM_BUFFER, value, oglId);

    // Done:
    return true;
}

#pragma endregion