/**
 * @file	overv_gl_pipeline_rt.h
 * @brief	A pipeline for doing simple ray tracing on GPU
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


 /**
  * @brief Basic ray tracing.
  */
class OVGL_API PipelineRayTracing : public Ov::Pipeline, public Ov::DrawGUI
{
//////////
public: //
//////////

    // Special values
    static constexpr uint32_t maxNrOfRecursion = 6;
    static constexpr uint32_t numberOfProcessingPart = 4;      /// Need to be power of two

    /**
     * @brief Global struct ray tracing
     */
    struct GlobalUniformRTData
    {
        uint32_t nrOfTriangles;
        uint32_t nrOfBSpheres;
        uint32_t nrOfRecursion;
        float defaultRefractionIndex;
        glm::vec4 eyePosition;
        glm::vec4 ray00;
        glm::vec4 ray01;
        glm::vec4 ray10;
        glm::vec4 ray11;
        glm::vec2 processingPartDimension;
        glm::vec2 pixelPad;
        uint32_t useNormalMapForReflection;
        uint32_t useNormalMapForRefraction;
        float offsetRayRefrRefl;
        uint32_t useRoghnessTexture;
        uint32_t useMetalnessTexture;
        uint32_t useAlbedoTexture;
        uint32_t alwaysCalculateRefractedRays;
        uint32_t _pad0;

        GlobalUniformRTData() : nrOfTriangles{ 0 },
            nrOfBSpheres{ 0 },
            nrOfRecursion{ 0 },
            defaultRefractionIndex{ 1.0f },
            eyePosition{ 0.0f },
            ray00{ 0.0f },
            ray01{ 0.0f },
            ray10{ 0.0f },
            ray11{ 0.0f },
            useNormalMapForReflection{ 1 },
            useNormalMapForRefraction{ 1 },
            useRoghnessTexture{ 1 },
            useMetalnessTexture{ 1 },
            useAlbedoTexture{ 1 },
            offsetRayRefrRefl{ 0.0001f },
            alwaysCalculateRefractedRays{ 0 }
        {}

        static std::string getShaderStruct()
        {
            return R"(
struct GlobalUniformRTData
{
    uint nrOfTriangles;
    uint nrOfBSpheres;
    uint nrOfRecursion;
    float defaultRefractionIndex;
    vec4 eyePosition;
    vec4 ray00;
    vec4 ray01;
    vec4 ray10;
    vec4 ray11;
    vec2 processingPartDimension;
    vec2 pixelPad;
    bool useNormalMapForReflection;
    bool useNormalMapForRefraction;
    float offsetRayRefrRefl;
    bool useRoghnessTexture;
    bool useMetalnessTexture;
    bool useAlbedoTexture;
    bool alwaysCalculateRefractedRays;
    bool _pad0;
};
)";
        }
    };

    /**
     * Per-triangle data. This struct must be aligned for OpenGL std430.
     */
    struct TriangleData
    {
        glm::vec4 v[3];  
        glm::vec4 n[3];
        glm::vec4 t[3];
        glm::vec2 uv[3];
        uint32_t matId;
        uint32_t geomId;

        TriangleData()
        {}

        static std::string getShaderStruct()
        {
            return R"(
struct TriangleData
{
    vec4 v[3];
    vec4 n[3];
    vec4 t[3];
    vec2 uv[3];
    uint matId;
    uint geomId;
};
)";
        }
    };

    /**
     * Bounding sphere data. This struct must be aligned for OpenGL std430.
     */
    struct BSphereData
    {
        uint32_t firstTriangle;
        uint32_t nrOfTriangles;
        float radius;
        uint32_t geomId;
        glm::vec4 position;

        BSphereData() : position{ 0.0f }, radius { 0 }, firstTriangle{ 0 }, nrOfTriangles{ 0 }
        {}

        static std::string getShaderStruct()
        {
            return R"(
struct BSphereData
{    
   uint firstTriangle;
   uint nrOfTriangles;
   float radius;
   uint geomId;
   vec4 position;
};
)";
        }
    };

    /**
     * HitInfo data. This struct must be aligned for OpenGL std430.
     */
    struct HitInfoData 
    {
        uint32_t triangleId;            // Triangle index (within the triangle[] array)
        float distance;
        glm::vec2 uvCoords;
        glm::vec2 barycentricCoords;    // Triangle barycentric coords
        uint32_t shadowsHitsInfoId;
        uint32_t geomId;
        glm::vec4 collisionPoint;       // Triangle's coords at collision point
        glm::vec4 normal;
        glm::vec4 tangent;
        glm::vec4 faceNormal;
        uint32_t matId;
        uint32_t reflectionHitInfoId;;
        uint32_t refractionHitInfoId;
        uint32_t _pad0;
        glm::vec4 inDir;

        HitInfoData() : barycentricCoords{ 0.0f }, distance{ 0 }, triangleId{ 0 }, collisionPoint{ 0.0f },
            normal{ 0.0f }, tangent{ 0.0f }, faceNormal{ 0.0f }, uvCoords{ 0.0f },
            matId{ 0 }, reflectionHitInfoId{ 0 }, refractionHitInfoId{ 0 }
        {}

        static std::string getShaderStruct()
        {
            return R"(
struct HitInfoData
{    
    uint triangleId;            // Triangle index (within the triangles[] array)
    float distance;
    vec2 uvCoords;
    vec2 barycentricCoords;    // Triangle barycentric coords
    uint shadowsHitsInfoId;
    uint geomId;
    vec4 collisionPoint;       // Triangle's coords at collision point
    vec4 normal;
    vec4 tangent;
    vec4 faceNormal;
    uint matId;
    uint reflectionHitInfoId;
    uint refractionHitInfoId;
    uint _pad0;
    vec4 inDir;
};
)";
        }
    };

    /**
     * Structure for modeling a ray.
     */
    struct ShadowHitInfoData
    {
        glm::vec4 lightDir;
        glm::vec4 absorption;
        float distance;
        uint32_t _pad0[3];

        ShadowHitInfoData() : lightDir{ 0.0f }, distance{ 0.0f }
        {}

        static std::string getShaderStruct()
        {
            return R"(
/**
 * Structure for modeling a shadow hit
 */
struct ShadowHitInfoData
{ 
    vec4 lightDir;
    vec4 absorption;
    float distance;
    uint _pad0[3];
};
)";
        }
    };

    // Const/dest:
    PipelineRayTracing();
    PipelineRayTracing(PipelineRayTracing&& other);
    PipelineRayTracing(PipelineRayTracing const&) = delete;
    virtual ~PipelineRayTracing();

    // Get/set:
    const OvGL::Texture2D& getOutputBuffer() const;
    const OvGL::StorageBufferObj& getHitsInfoDataBuffer(uint32_t id) const;
    const OvGL::StorageBufferObj& getShadowsHitsInfoDataBuffer(uint32_t id) const;
    bool getUseNormalMapForReflection() const; 
    bool getUseNormalMapForRefraction() const;
    bool getUseRoghnessTexture() const;
    bool getUseMetalnessTexture() const;
    bool getUseAlbedoTexture() const;
    uint32_t getNrOfRecursion() const;

    bool setNrOfRecursion(uint32_t nrOfRecursion);

    // Data preparation:
    bool migrate(const OvGL::List& list);

    // Rendering methods:
    bool render(const Ov::Camera& camera, const Ov::List& list);

    // Managed:
    bool init() override;
    bool free() override;

    // DrawGUI
    virtual void drawGUI() override;

    // Shader
    static bool addShaderPreproc();

/////////////
protected: //
/////////////

    // Reserved:
    struct Reserved;
    std::unique_ptr<Reserved> reserved;

    // Const/dest:
    PipelineRayTracing(const std::string& name);
};