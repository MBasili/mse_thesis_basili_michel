/**
 * @file	overv_gl_list.cpp
 * @brief	OpenGL material properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// Magic enum:
#include "magic_enum.hpp"

// ImGui
#include "imgui.h"


////////////
// STATIC //
////////////

// Special values:
OvGL::List OvGL::List::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief List reserved structure.
 */
struct OvGL::List::Reserved
{
    // GUI light target
    uint32_t targetLight;

    /**
     * Constructor.
     */
    Reserved() : targetLight{ 0 }
    {}
};


////////////////////////
// BODY OF CLASS List //
////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::List::List() : reserved(std::make_unique<OvGL::List::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the list
 */
OVGL_API OvGL::List::List(const std::string& name) : Ov::List(name), reserved(std::make_unique<OvGL::List::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::List::List(List&& other) noexcept : Ov::List(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::List::~List()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method. Object data in the list is stored in world coordinates: if other spaces are needed, compute them
 * directly in the shader. All the data to do that is provided.
 * @param cameraMatrix camera matrix to use
 * @param projectionMatrix projection matrix to use
 * @param passType type of pass
 * @return TF
 */
bool OVGL_API OvGL::List::render(const glm::mat4& cameraMatrix, const glm::mat4& projectionMatrix, Ov::List::Type passType) const
{
    const std::vector<Ov::List::RenderableElem>& renderableElem = this->getRenderableElems();
    OvGL::Storage& storage = OvGL::Storage::getInstance();

    // Set global data
    OvGL::Storage::GlobalUniformData* globalUniformData = storage.getGlobalUniform();
    globalUniformData->viewProj = glm::inverse(cameraMatrix) * projectionMatrix;
    globalUniformData->viewInverse = cameraMatrix;
    globalUniformData->projInverse = glm::inverse(projectionMatrix);


    // Iterate:
    uint32_t curInstance[OvGL::Storage::nrOfTypes];
    for (uint32_t c = 0; c < OvGL::Storage::nrOfTypes; c++)
        curInstance[c] = 0;

    bool renderAll = false;
    if (static_cast<uint32_t>(passType) == static_cast<uint32_t>(Ov::List::Type::all))
        renderAll = true;

    for (auto& re : renderableElem)
    {
        ////////////////////
        // Fill the storage:

        // Lights:      
        if ((renderAll || static_cast<uint32_t>(passType) == static_cast<uint32_t>(Ov::List::Type::lights)) &&
            typeid(re.renderableData.first.get()) == typeid(OvGL::Light))
        {
            const uint32_t type = static_cast<uint32_t>(Ov::List::Type::lights);

            OvGL::Light& light = dynamic_cast<OvGL::Light&>(re.renderableData.first.get());
            OvGL::Storage::LightData* lightInstance = storage.getLightData(curInstance[type]);

            // Fill light data:
            lightInstance->color = glm::vec4(light.getColor(), 1.0f);
            lightInstance->power = light.getPower();
            lightInstance->direction = glm::vec4(light.getDirection(), 1.0f);
            lightInstance->cutOff = light.getSpotCutoff();
            lightInstance->position = re.matrix[3]; // In world coordinates
            lightInstance->lightType = magic_enum::enum_integer(light.getLightType());
            lightInstance->spotExponent = light.getSpotExponent();

            // Next:
            curInstance[type]++;
            continue;
        }

        // Materials:
        if ((renderAll || static_cast<uint32_t>(passType) == static_cast<uint32_t>(Ov::List::Type::geoms)) &&
            typeid(re.renderableData.first.get()) == typeid(OvGL::Geometry))
        {
            OvGL::Geometry& geom = dynamic_cast<OvGL::Geometry&>(re.renderableData.first.get());
            if (re.renderableData.second.get() == Ov::Renderable::empty)
            {
                OV_LOG_ERROR("Missing material as second renderable");
                continue;
            }

            OvGL::Material& mat = dynamic_cast<OvGL::Material&>(re.renderableData.second.get());

            curInstance[static_cast<uint32_t>(Ov::List::Type::geoms)]++;

            // Update material:
            mat.render();
            continue;
        }
    }

    // Done:
    storage.setNrOfInstances(curInstance[static_cast<uint32_t>(Ov::List::Type::lights)], Ov::List::Type::lights);
    globalUniformData->nrOfLights = curInstance[static_cast<uint32_t>(Ov::List::Type::lights)];

    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method to draw a gui on a windows with the ImGUI library.
 */
void OVGL_API OvGL::List::drawGUI()
{
    if (!ImGui::CollapsingHeader("List"))
        return;

    // Lights
    if (ImGui::TreeNode("Lights"))
    {
        std::vector<Ov::List::RenderableElem>& renderableElem =
            const_cast<std::vector<Ov::List::RenderableElem>&>(this->getRenderableElems());
        // Get TargetLight Light
        // Light are all hat the beginning
        std::reference_wrapper<Ov::List::RenderableElem> re =
            renderableElem[reserved->targetLight];
        std::reference_wrapper<OvGL::Light> light = dynamic_cast<OvGL::Light&>(re.get().renderableData.first.get());

        // Switch Light
        ImGui::Text("Switch light");

        if (ImGui::ArrowButton("arrowbtn01RT", 0))
        {
            if (reserved->targetLight > 0)
                reserved->targetLight -= 1;
            else
                reserved->targetLight = this->getNrOfLights() - 1;
        }

        ImGui::SameLine();

        ImGui::Text("%d", reserved->targetLight);

        ImGui::SameLine();

        if (ImGui::ArrowButton("arrowbtn02RT", 1))
        {
            reserved->targetLight += 1;
            reserved->targetLight %= this->getNrOfLights();
        }

        ImGui::NewLine();

        // Target light
        ImGui::Text("Target light: Name= %s ,Id= %d", light.get().getName(), light.get().getId());

        // Set visible or not
        ImGui::Checkbox("Enable/Disable", &re.get().reference.get().getRenderable(0).visibleFlag);
        re.get().renderableData.visibleFlag = re.get().reference.get().getRenderable(0).visibleFlag;

        // Possibility to add position and rotation

        // light properties
        light.get().drawGUI();

        ImGui::TreePop();
    }
}

#pragma endregion