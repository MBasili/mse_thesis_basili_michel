/**
 * @file	overv_gl_ovo.cpp
 * @brief	OVO file format support
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2021
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main include:
#include "overv_gl.h"


///////////////////////
// BODY OF CLASS Ovo //
///////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
void OVGL_API* OvGL::Ovo::createByType(ChunkId type) const
{
    switch (type)
    {
    case ChunkId::node:     return new Ov::Node();
    case ChunkId::material: return new OvGL::Material();
    case ChunkId::geometry: return new OvGL::Geometry();
    case ChunkId::light:    return new OvGL::Light();

    default:
        OV_LOG_ERROR("Unsupported object for the given chunk type");
        return nullptr;
    }
}