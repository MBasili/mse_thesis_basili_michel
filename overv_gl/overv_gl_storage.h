/**
 * @file	overv_gl_storage.h
 * @brief	OpenGL GPU storage
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 *          Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


/**
 * @brief Class for storing most of the GPU-sided memory. This class is a singleton.
 */
class OVGL_API Storage final : public Ov::Renderable {
//////////
public: //
//////////	   

	constexpr static uint32_t nrOfRedundantBuffers = 3;                                   ///< Circular list of buffers for persistent, coherent GPU access
	constexpr static uint32_t nrOfTypes = static_cast<uint32_t>(Ov::List::Type::all);     ///< MDI and instance lists are created per type

	/**
	 * @brief Global struct.
	 */
	struct GlobalUniformData {
		glm::mat4 viewProj;     // Camera view * projection
		// ...64 bytes
		glm::mat4 viewInverse;  // Camera inverse view matrix
		// ...64 bytes
		glm::mat4 projInverse;  // Camera inverse projection matrix
		// ...64 bytes

		uint32_t nrOfLights;
		uint32_t _pad0[3];
		// ...16bytes

		GlobalUniformData() : viewProj{ 1.0f },
			viewInverse{ 1.0f },
			projInverse{ 1.0f },
			nrOfLights{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct GlobalUniformData
{
	mat4 viewProj;
	mat4 viewInverse;    
	mat4 projInverse;
	uint nrOfLights;
	uint _pad0[3];
};
)";
		}
	};

	/**
	 * @brief PBR material struct (Modify for ray tracing support).
	 */
	struct MaterialData {
		glm::vec4 emission;			///< Emissive term
		glm::vec4 albedo;           ///< Albedo color
		float opacity;              ///< Transparency (1 = solid, 0 = invisible)
		float roughness;			///< Roughness   
		float metalness;            ///< Metalness
		float refractionIndex;      ///< Refraction Index
		float rimIntensity;         ///< Rim lighting intensity
		float absorptionThreshold;  ///< AbsorptionThreshold  
		glm::vec2 _pad0;
		//64bytes
		uint64_t albedoTextureHandle;
		uint64_t normalTextureHandle;
		// 64bytes
		uint64_t heightTextureHandle;
		uint64_t roughnessTextureHandle;
		uint64_t metalnessTextureHandle;
		uint64_t _pad1;
		// 112 bytes

		/**
		 * Constructor.
		 */
		MaterialData() noexcept : emission{ 0.0f },
			opacity{ 1.0f },
			albedo{ 0.6f },
			roughness{ 0.5f },
			metalness{ 0.01f },
			refractionIndex{ 1.0f },
			rimIntensity{ 0.7f },
			absorptionThreshold{ 0.0f },
			albedoTextureHandle{ 0 },
			normalTextureHandle{ 0 },
			heightTextureHandle{ 0 },
			roughnessTextureHandle{ 0 },
			metalnessTextureHandle{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct MaterialData
{
	vec4 emission;						///< Emissive term
	vec4 albedo;						///< Albedo color
	float opacity;						///< Transparency (1 = solid, 0 = invisible)
	float roughness;					///< Roughness   
	float metalness;					///< Metalness
	float refractionIndex;				///< Refraction Index
	float rimIntensity;					///< Rim lighting intensity
	float absorptionThreshold;			///< AbsorptionThreshold  
	vec2 _pad0;
	uint64_t albedoTextureHandle;
	uint64_t normalTextureHandle;
	uint64_t heightTextureHandle;
	uint64_t roughnessTextureHandle;
	uint64_t metalnessTextureHandle;
	uint64_t _pad1;
};
)";
		}
	};


	/**
	 * Light GPU data structure.
	 */
	struct LightData {
		glm::vec4 color;
		glm::vec4 direction;
		glm::vec4 position;
		uint32_t lightType;
		float power;
		float cutOff;
		float spotExponent;
		// ... 64 bytes

		/**
		 * Constructor.
		*/
		LightData() : position{ 0.0f },
			lightType{ 0 },
			color{ 1.0f },
			power{ 0.0f },
			direction{ 0.0f },
			cutOff{ 0.0f },
			spotExponent{ 0.0f }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct LightData
{
	vec4 color;
	vec4 direction;
	vec4 position;
	uint lightType;
	float power;
	float cutOff;
	float spotExponent;
};
)";
		}
	};

	// Const/Dest (static class, can't be used):
	Storage(Storage const&) = delete;
	virtual ~Storage();

	// Operators:
	void operator=(Storage const&) = delete;

	// Singleton:
	static Storage& getInstance();

	// Init/free:
	bool init();
	bool free();

	// Get/set:
	OvGL::UniformBufferObj& getGlobalUniformBuffer();
	GlobalUniformData* getGlobalUniform();
	OvGL::UniformBufferObj& getMaterialDataBuffer();
	MaterialData* getMaterialData(uint32_t id);
	OvGL::UniformBufferObj& getLightBuffer();
	LightData* getLightData(uint32_t id);
	void setNrOfInstances(uint32_t nrOfInstances, Ov::List::Type type);
	uint32_t getNrOfInstances(Ov::List::Type type) const;

	// Update list:
	bool addToUpdateList(const Ov::Renderable& renderable);
	bool useNext();

	// Rendering:
	bool render(uint32_t value = 0, void* data = nullptr, Ov::List::Type type = Ov::List::Type::geoms) const;

	// Shader
	static bool addShaderPreproc();

///////////
private: //
///////////	

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Storage();
};