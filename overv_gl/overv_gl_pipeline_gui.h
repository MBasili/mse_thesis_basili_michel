/**
 * @file	overv_gui_pipeline_gui.h
 * @brief	GUI pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for manage the ImGUI libraries throught a pipeline
  */
class OVGL_API GUIPipeline final : public Ov::Pipeline
{
//////////
public: //
//////////

	// Const/dest:
	GUIPipeline();
	GUIPipeline(GUIPipeline&& other);
	GUIPipeline(GUIPipeline const&) = delete;
	virtual ~GUIPipeline();

	// Operators
	GUIPipeline& operator=(const GUIPipeline&) = delete;
	GUIPipeline& operator=(GUIPipeline&&) = delete;

	// Managed
	virtual bool init() override;
	virtual bool free() override;

	// SurfaceResize
	bool keyboardCallback(int key, int scancode, int action, int mods);
	bool mouseCursorCallback(double mouseX, double mouseY);
	bool mouseButtonCallback(int button, int action, int mods);
	bool mouseScrollCallback(double scrollX, double scrollY);

	// General
	bool create();

	// DrawGUI
	bool addDrawGUIElement(uint32_t id, Ov::DrawGUI& object);
	bool removeDrawGUIElement(uint32_t id);

	// Rendering
	bool render(uint32_t value = 0, void* data = nullptr);

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	GUIPipeline(const std::string& name);
};