/**
 * @file	overv_gl_texture.cpp
 * @brief	OpenGL texture properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// Magic enum:
#include "magic_enum.hpp"

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>


////////////
// STATIC //
////////////

// Special values:
OvGL::Texture OvGL::Texture::empty("[empty]");

// Look-up table:
bool OvGL::Texture::lut[OvGL::Texture::maxNrOfTextures];

/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Texture reserved structure.
 */
struct OvGL::Texture::Reserved
{
    GLuint oglId;               ///< OpenGL texture ID   
    GLuint64 oglBindlessHandle; ///< GL_ARB_bindless_texture special handle
    GLuint oglInternalFormat;   ///< OpenGL internal format enum

    int lutPos;                 ///< Position in the lut                 

    /**
     * Constructor.
     */
    Reserved() : oglId{ 0 },
        oglBindlessHandle{ 0 },
        oglInternalFormat{ 0 },
        lutPos{ maxNrOfTextures }
    {
        // Find a free slot in the LUT:
        for (uint32_t c = 0; c < maxNrOfTextures; c++)
            if (lut[c] == false)
            {
                lut[c] = true;
                lutPos = c;
                break;
            }
        if (lutPos == maxNrOfTextures)
            OV_LOG_ERROR("Too many materials created");
    }

    /**
     * Destructor.
     */
    ~Reserved()
    {
        if (lutPos != maxNrOfTextures)
            lut[lutPos] = false;
    }
};


///////////////////////////
// BODY OF CLASS Texture //
///////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Texture::Texture() : reserved(std::make_unique<OvGL::Texture::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVGL_API OvGL::Texture::Texture(const std::string& name) : Ov::Texture(name), reserved(std::make_unique<OvGL::Texture::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Texture::Texture(Texture&& other) noexcept : Ov::Texture(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Texture::~Texture()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized()) // Because of the move constructor   
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Texture::init()
{
    if (this->Ov::Managed::init() == false)
        return false;

    // Free texture if already stored:
    if (reserved->oglBindlessHandle)
    {
        glMakeTextureHandleNonResidentARB(reserved->oglBindlessHandle);
        reserved->oglBindlessHandle = 0;
    }
    if (reserved->oglId)
    {
        glDeleteTextures(1, &reserved->oglId);
        reserved->oglId = 0;
    }

    // Create it:		    
    glGenTextures(1, &reserved->oglId);

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Texture::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Free texture if stored:
    if (reserved->oglBindlessHandle)
    {
        glMakeTextureHandleNonResidentARB(reserved->oglBindlessHandle);
        reserved->oglBindlessHandle = 0;
    }
    if (reserved->oglId)
    {
        glDeleteTextures(1, &reserved->oglId);
        reserved->oglId = 0;
    }

    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get position of this texture in the LUT.
 * @return LUT position
 */
uint32_t OVGL_API OvGL::Texture::getLutPos() const
{
    return reserved->lutPos;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the GLuint texture ID.
 * @return texture ID or 0 if not valid
 */
uint32_t OVGL_API OvGL::Texture::getOglHandle() const
{
    return reserved->oglId;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the GLuint64 bindless texture ID.
 * @return binddless texture handle or 0 if not valid
 */
uint64_t OVGL_API OvGL::Texture::getOglBindlessHandle() const
{
    return reserved->oglBindlessHandle;
}

uint64_t OVGL_API OvGL::Texture::getInternalFormat() const {
    return reserved->oglInternalFormat;
}

void OVGL_API OvGL::Texture::setInternalFormat(uint64_t format) {
    reserved->oglInternalFormat = format;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Texture::render(uint32_t value, void* data) const
{
    OvGL::Program& program = OvGL::Program::getCached();
    std::string texLevel = "texture" + std::to_string(value);
    program.setUInt64(texLevel, this->getOglBindlessHandle());
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Binds image for imagestore operations.
 * @param location location to bind the texture on
 * @return TF
 */
bool OVGL_API OvGL::Texture::bindImage(uint32_t location) const
{
    glBindImageTexture(location, reserved->oglId, 0, GL_FALSE, 0, GL_WRITE_ONLY, reserved->oglInternalFormat);
    return true;
}

#pragma endregion

#pragma region InternalMemoryManaged

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Makes the texture resident (can't be modified anymore).
 * @return TF
 */
bool OVGL_API OvGL::Texture::makeResident()
{
    // Sanity check:
    if (reserved->oglBindlessHandle)
    {
        OV_LOG_ERROR("Texture already resident");
        return false;
    }

    // Bindless:   
    reserved->oglBindlessHandle = glGetTextureHandleARB(reserved->oglId);
    glMakeTextureHandleResidentARB(reserved->oglBindlessHandle);

    // Done:   
    return true;
}

#pragma endregion
