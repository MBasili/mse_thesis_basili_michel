/**
 * @file	overv_gl_vertexarrayobj.h
 * @brief	OpenGL vertex array object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


 /**
  * @brief OpenGL vertex array object
  */
class OVGL_API VertexArrayObj final : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

	// Const/dest:
	VertexArrayObj();
	VertexArrayObj(VertexArrayObj&& other) noexcept;
    VertexArrayObj(VertexArrayObj const&) = delete;
	virtual ~VertexArrayObj();

	// Managed:
	bool init() override;
	bool free() override;

	// Management:
	bool create();

	// Get/set:   
	uint32_t getOglHandle() const;

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;
	static void reset();

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	VertexArrayObj(const std::string& name);
};