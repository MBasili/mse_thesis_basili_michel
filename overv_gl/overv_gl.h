/**
 * @file	overv_gl.h
 * @brief	OverVision 3D vintage OpenGL implementation
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */
#pragma once


//////////////
// #INCLUDE //
//////////////

// Abstract implementation:
#include "overv_core.h"

/////////////
// VERSION //
/////////////

// Export API:
#ifdef OV_WINDOWS
// Specifies i/o linkage (VC++ spec):
#ifdef OVERV_OPENGL_EXPORTS
#define OVGL_API __declspec(dllexport)
#else
#define OVGL_API __declspec(dllimport)
#endif
#endif
#ifdef OV_LINUX
#define OVGL_API
#endif

///////////////
// NAMESPACE //
///////////////

namespace OvGL {

    //////////////
    // #INCLUDE //
    //////////////   

    // Entities:
    #include "overv_gl_buffer.h"
    #include "overv_gl_arraybuffer.h"
    #include "overv_gl_elemarraybuffer.h"
    #include "overv_gl_vertexarrayobj.h"
    #include "overv_gl_uniformbufferobj.h" 
    #include "overv_gl_storagebufferobj.h"
    #include "overv_gl_camera.h"
    #include "overv_gl_bitmap.h"
    #include "overv_gl_atomic_counter.h"

    // Renderables:
    #include "overv_gl_texture.h"
    #include "overv_gl_texture2d.h"
    #include "overv_gl_framebuffer.h"
    #include "overv_gl_geometry.h"
    #include "overv_gl_light.h"
    #include "overv_gl_material.h"  
    #include "overv_gl_shader.h"
    #include "overv_gl_program.h"
    #include "overv_gl_quad.h"  

    // Scene graph:
    #include "overv_gl_list.h"

    // Storage:
    #include "overv_gl_container.h"
    #include "overv_gl_storage.h"

    // Pipelines
    #include "overv_gl_pipeline_rt.h"
    #include "overv_gl_pipeline_lighting.h"
    #include "overv_gl_pipeline_fullscreen2d.h"
    #include "overv_gl_pipeline_gui.h"

    // Loaders
    #include "overv_gl_ovo.h"
    #include "overv_gl_loader.h"


    /**
     * @brief Base engine main class. This class is a singleton.
     */
    class OVGL_API Engine : public Ov::Core
    {
    //////////
    public: //
    //////////	   

        inline static const std::string texturesSourceFolder = "..\\resources\\textures";	///< Folder containing all the textures
        inline static const std::string modelsSourceFolder = "..\\resources\\models";     ///< Folder containing all the models

        // Callback signatures:
        typedef void (*KeyboardCallback)   (int key, int scancode, int action, int mods);
        typedef void (*MouseCursorCallback)(double mouseX, double mouseY);
        typedef void (*MouseButtonCallback)(int button, int action, int mods);
        typedef void (*MouseScrollCallback)(double scrollX, double scrollY);
        typedef void (*WindowSizeCallback) (int width, int height);

        // Const/Dest (static class, can't be used):
        Engine(Engine const&) = delete;
        virtual ~Engine();

        // Init/free:
        bool init(const Ov::Config& config = Ov::Config::empty);
        bool free();

        // Operators:
        void operator=(Engine const&) = delete;

        // Singleton:
        static Engine& getInstance();

        // Get/set:
        uint64_t getFrameNr() const;
        int getWidth();
        int getHeight();

        // Management:
        bool processEvents();
        bool clear();
        bool clearDepth();
        bool swap();
        
        // Callback
        bool setKeyboardCallback(KeyboardCallback cb);
        bool setMouseCursorCallback(MouseCursorCallback cb);
        bool setMouseButtonCallback(MouseButtonCallback cb);
        bool setMouseScrollCallback(MouseScrollCallback cb);
        void setWindowSizeCallback();

        // GLFW
        bool queryKey(int keyCode) const;
        void* getGLFWWindow() const;

    ///////////
    private: //
    ///////////	

        // Reserved:
        struct Reserved;
        std::unique_ptr<Reserved> reserved;

        void windowSizeCallback(int _width, int _height);

        // Const/dest:
        Engine();
    };

}; // end of namespace OvGL::