/**
 * @file	overv_gl_loader.cpp
 * @brief	Base 3D model loading functionality
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// Magic enum:
#include "magic_enum.hpp"

// Assimp:    
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/matrix4x4.h>
#include <assimp/cimport.h>
#include <assimp/material.h>

// GLM:
#include <glm/gtc/packing.hpp>  

// C/C++
#include <iostream>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Loader class reserved structure.
 */
struct OvGL::Loader::Reserved
{
    Assimp::Importer importer;
    const aiScene* modelScene;

    // Status flags:
    bool tooManyMaterialsFlag;


    /**
     * Constructor.
     */
    Reserved() : modelScene{ nullptr },
        tooManyMaterialsFlag{ false }
    {
        // Specifies the maximum angle that may be between two face normals at the same vertex position that their are smoothed together.
        importer.SetPropertyFloat("PP_GSN_MAX_SMOOTHING_ANGLE", 45);
    }
};


//////////////////////////
// BODY OF CLASS Loader //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Loader::Loader() : reserved(std::make_unique<OvGL::Loader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 * @param name loader name
 */
OVGL_API OvGL::Loader::Loader(const std::string& name) : Ov::Object(name), reserved(std::make_unique<OvGL::Loader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Loader::Loader(Loader&& other) noexcept : Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Loader::~Loader()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * True when the tooManyMaterials error has been triggered.
 * @return TF
 */
bool OVGL_API OvGL::Loader::tooManyMaterials()
{
    return reserved->tooManyMaterialsFlag;
}

#pragma endregion

#pragma region Loading

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load 3D scene.
 * @param filename 3D file.
 * @param pFlags Assimp flags for post processing.
 * @return TF
 */
bool OVGL_API OvGL::Loader::load(const std::string& filename, unsigned int pFlags)
{

    showParamsUsed(pFlags);

    ////////////////////
    // Prepare import //
    ////////////////////

    reserved->modelScene = reserved->importer.ReadFile( OvGL::Engine::modelsSourceFolder + "\\" + filename, pFlags);

    if (reserved->modelScene == nullptr)
    {
        OV_LOG_ERROR("Error loading file '%s': %s", filename.c_str(), reserved->importer.GetErrorString());
        return false;
    }

    // Scene stats:
    OV_LOG_PLAIN("   Nr. of meshes :  %u", reserved->modelScene->mNumMeshes);
    OV_LOG_PLAIN("   Nr. of lights :  %u", reserved->modelScene->mNumLights);

    if (reserved->modelScene->mMetaData)
    {
        int upAxis = 0;
        reserved->modelScene->mMetaData->Get<int>("UpAxis", upAxis);
        int upAxisSign = 1;
        reserved->modelScene->mMetaData->Get<int>("UpAxisSign", upAxisSign);
        int frontAxis = 0;
        reserved->modelScene->mMetaData->Get<int>("FrontAxis", frontAxis);
        int frontAxisSign = 1;
        reserved->modelScene->mMetaData->Get<int>("FrontAxisSign", frontAxisSign);
        int coordAxis = 0;
        reserved->modelScene->mMetaData->Get<int>("CoordAxis", coordAxis);
        int coordAxisSign = 1;
        reserved->modelScene->mMetaData->Get<int>("CoordAxisSign", coordAxisSign);

        aiVector3D upVec = upAxis == 0 ? aiVector3D(-(ai_real)upAxisSign, 0, 0) : upAxis == 1 ? aiVector3D(0, -(ai_real)upAxisSign, 0) : aiVector3D(0, 0, -(ai_real)upAxisSign);
        aiVector3D forwardVec = frontAxis == 0 ? aiVector3D((ai_real)frontAxisSign, 0, 0) : frontAxis == 1 ? aiVector3D(0, (ai_real)frontAxisSign, 0) : aiVector3D(0, 0, (ai_real)frontAxisSign);
        aiVector3D rightVec = coordAxis == 0 ? aiVector3D((ai_real)coordAxisSign, 0, 0) : coordAxis == 1 ? aiVector3D(0, (ai_real)coordAxisSign, 0) : aiVector3D(0, 0, (ai_real)coordAxisSign);
        aiMatrix4x4 mat(rightVec.x, rightVec.y, rightVec.z, 0.0f,
            upVec.x, upVec.y, upVec.z, 0.0f,
            forwardVec.x, forwardVec.y, forwardVec.z, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f);

        reserved->modelScene->mRootNode->mTransformation *= mat;

        float factor;
        if (reserved->modelScene->mMetaData->Get("UnitScaleFactor", factor))
            OV_LOG_PLAIN("   Scaling factor:  %f", factor);
        else
            OV_LOG_PLAIN("   Scaling factor:  N/A");
    }

    /////////
    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp light as OvGL::Light.
 * @param light pointer to aiLight
 * @param light in/out light
 * @param node in/out node (Assimp stores the light position in the light props and not in its node)
 * @return TF
 */
bool OVGL_API OvGL::Loader::loadLight(void* light, OvGL::Light& l, Ov::Node& node)
{
    // Safety net:
    if (light == nullptr || l == OvGL::Light::empty)
    {
        OV_LOG_ERROR("Invalid params passed to the loadLight method of the loader with id= %d", getId());
        return false;
    }

    aiLight* _light = reinterpret_cast<aiLight*>(light);

    // Set node position
    node.setMatrix(node.getMatrix() * glm::translate(glm::mat4(1.0f), glm::vec3(_light->mPosition.x, _light->mPosition.y, _light->mPosition.z)));

    // Set props:
    // Name
    l.setName(_light->mName.C_Str());

    // Type
    switch (_light->mType)
    {
        //////////////////////////////
    case aiLightSource_DIRECTIONAL: //
        l.setLightType(OvGL::Light::Type::directional);
        break;
        ////////////////////////
    case aiLightSource_POINT: //
        l.setLightType(OvGL::Light::Type::omni);
        break;
        ///////////////////////
    case aiLightSource_SPOT: //
        l.setLightType(OvGL::Light::Type::spot);
        break;
    default:
        OV_LOG_ERROR("Light type not supported in the loader with id= %d", getId());
        return false;
    }

    // Set color of the light
    l.setColor(glm::vec3(_light->mColorDiffuse.r, _light->mColorDiffuse.g, _light->mColorDiffuse.b));
    l.setPower(0.0f);

    // Relative to the transformation of the node corresponding to the light.
    // The direction is undefined for point lights. The vector may be normalized, but it needn't.
    glm::vec3 dirVec = glm::vec3(_light->mDirection.x, _light->mDirection.y, _light->mDirection.z);
    l.setDirection(glm::normalize(dirVec));

    // CutOff value in radians
    l.setSpotCutoff(_light->mAngleInnerCone);

    // SpotExponent value in radians
    // sources formula: https://learnopengl.com/Lighting/Light-casters
    l.setSpotExponent(cos(_light->mAngleInnerCone) - cos(_light->mAngleOuterCone));

    OV_LOG_PLAIN("Light  . . . :  %s", l.getName().c_str());
    OV_LOG_PLAIN("Color  . . . :  %s", glm::to_string(l.getColor()).c_str()); 

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp mesh as Ov::Geometry.
 * @param mesh pointer to aiMesh
 * @param geometry in/out geometry
 * @return TF
 */
bool OVGL_API OvGL::Loader::loadGeometry(void* mesh, OvGL::Geometry& g)
{
    // Safety net:
    if (mesh == nullptr || g == OvGL::Geometry::empty)
    {
        OV_LOG_ERROR("Invalid mesh passed to the loadGeometry method of the loader with id= %d", getId());
        return false;
    }

    aiMesh* _mesh = reinterpret_cast<aiMesh*>(mesh);
    g.setName(_mesh->mName.C_Str());
    g.reset();

    // Process vertices:
    for (unsigned int v = 0; v < _mesh->mNumVertices; v++)
    {
        Ov::Geometry::VertexData vd;
        vd.vertex.x = _mesh->mVertices[v].x;
        vd.vertex.y = _mesh->mVertices[v].y;
        vd.vertex.z = _mesh->mVertices[v].z;

        if (_mesh->HasNormals())
            // glm::packSnorm3x10_1x2 better but not GLSL function to unpack
            vd.normal = glm::packSnorm4x8(glm::vec4(_mesh->mNormals[v].x,
                _mesh->mNormals[v].y,
                _mesh->mNormals[v].z, 0.0f));

        if (_mesh->HasTextureCoords(0))
            vd.uv = glm::packHalf2x16(glm::vec2(_mesh->mTextureCoords[0][v].x, _mesh->mTextureCoords[0][v].y));

        if (_mesh->HasTangentsAndBitangents())
            // glm::packSnorm3x10_1x2 better but not GLSL function to unpack
            vd.tangent = glm::packSnorm4x8(glm::vec4(_mesh->mTangents[v].x,
                _mesh->mTangents[v].y,
                _mesh->mTangents[v].z, 0.0f));

        // Store it:
        g.addVertex(vd);
    }

    // Process faces:
    for (unsigned int f = 0; f < _mesh->mNumFaces; f++)
    {
        Ov::Geometry::FaceData fd;

        aiFace* face = &_mesh->mFaces[f];
        fd.a = face->mIndices[0];
        fd.b = face->mIndices[1];
        fd.c = face->mIndices[2];

        // Store it:
        g.addFace(fd);
    }

#ifdef OV_DEBUG
    OV_LOG_PLAIN("Geometry . . :  %s", g.getName().c_str());
#endif

    // Upload to device:
    return g.upload();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp material as OvVulkan::Material.
 * @param mesh pointer to the aiMesh holding the material
 * @param m in/out material
 * @return TF
 */
bool OVGL_API OvGL::Loader::loadMaterial(void* material, OvGL::Material& m)
{
    // Safety net:
    if (material == nullptr || m == OvGL::Material::empty)
    {
        OV_LOG_ERROR("Invalid params passed to the loadMaterial method of the loader with id= %d", getId());
        return false;
    }

    aiMaterial* _material = reinterpret_cast<aiMaterial*>(material);

    // Pass parameters:
    m.setName(_material->GetName().C_Str());

    int shadingModel;
    aiGetMaterialInteger(_material, AI_MATKEY_SHADING_MODEL, &shadingModel);

    aiColor3D color;
    float value;


    // Emission:
    if (_material->Get(AI_MATKEY_COLOR_EMISSIVE, color) == AI_SUCCESS)
    {
        m.setEmission(glm::vec3(color.r, color.g, color.b));
    }
    else
    {
        m.setEmission(glm::vec3(0.0f));
        OV_LOG_WARN("Material '%s' has no emission: using default color", m.getName().c_str());
    }

    // Albedo:
    if (_material->Get(AI_MATKEY_COLOR_AMBIENT, color) == AI_SUCCESS)
    {
        m.setAlbedo(glm::vec3(color.r, color.g, color.b));
    }
    else
    {
        OV_LOG_WARN("Material '%s' has no ambient: using default color", m.getName().c_str());
        m.setAlbedo(glm::vec3(0.0f));
    }

    // Opacity
    if (_material->Get(AI_MATKEY_OPACITY, value) == AI_SUCCESS)
    {
        m.setOpacity(value);
    }
    else
    {
        m.setOpacity(1.0f);
        OV_LOG_WARN("Material '%s' has no opacity: using default value", m.getName().c_str());
    }

    // Roughness
    if (_material->Get(AI_MATKEY_ROUGHNESS_FACTOR, value) == AI_SUCCESS)
    {
        m.setRoughness(value < 0 ? 0 : value);
    }
    else
    {
        m.setRoughness(0.0f);
        OV_LOG_WARN("Material '%s' has no roughness: using default value", m.getName().c_str());
    }

    // Metalness
    if (_material->Get(AI_MATKEY_METALLIC_FACTOR, value) == AI_SUCCESS)
    {
        m.setMetalness(value);
    }
    else
    {
        m.setMetalness(0.0f);
        OV_LOG_WARN("Material '%s' has no roughness: using default value", m.getName().c_str());
    }

    // Refraction index
    if (_material->Get(AI_MATKEY_REFRACTI, value) == AI_SUCCESS)
    {
        m.setRefractionIndex((value == 0 ? 1.0f : value));
    }
    else
    {
        m.setRefractionIndex(1.0f);
        OV_LOG_WARN("Material '%s' has no refraction index: using default value", m.getName().c_str());
    }

#ifdef OV_DEBUG
    OV_LOG_PLAIN("Material . . :  %s", m.getName().c_str());
    OV_LOG_PLAIN("Emission . . :  %s", glm::to_string(m.getEmission()).c_str());
    OV_LOG_PLAIN("Albedo . . . :  %s", glm::to_string(m.getAlbedo()).c_str());
    OV_LOG_PLAIN("Opacity  . . :  %f", m.getOpacity());
    OV_LOG_PLAIN("Roughness  . :  %f", m.getRoughness());
    OV_LOG_PLAIN("Metalness  . :  %f", m.getMetalness());
    OV_LOG_PLAIN("Refraction index  . :  %f", m.getRefractionIndex());
#endif

    //Done: 
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load texure2D as OvVulkan::Texture2D.
 * @param imageName The name of the texture to load.
 * @param type The OvVulkan texture type of the texture2D.
 * @param t The OvVulkan texture2D in which load the image.
 * @return TF
 */
bool OVGL_API OvGL::Loader::loadTexture2D(std::string imageName, Ov::Texture::Type type, OvGL::Texture2D& t)
{
    // Texture create
    t.setName(imageName);

    // Load bitmap
    OvGL::Bitmap bitmap;
    // Check extension 
    bool loadingResult = false;
    if (imageName.substr(imageName.find_last_of(".") + 1) == "dds") {
        loadingResult = bitmap.loadDDS(OvGL::Engine::texturesSourceFolder + "\\" + imageName);
    }
    if (imageName.substr(imageName.find_last_of(".") + 1) == "ktx") {

        Ov::Bitmap::Format formatToLoad = Ov::Bitmap::Format::r8g8b8a8_compressed;

        switch (type) {
        case Ov::Texture::Type::metalness:
        case Ov::Texture::Type::roughness:
        case Ov::Texture::Type::height:
            formatToLoad = Ov::Bitmap::Format::r8_compressed;
            break;
        case Ov::Texture::Type::normal:
            formatToLoad = Ov::Bitmap::Format::r8g8_compressed;
            break;
        default:
            break;
        }

        loadingResult = bitmap.loadKTX(OvGL::Engine::texturesSourceFolder + "\\" + imageName, formatToLoad);
    }
    else {
        loadingResult = bitmap.loadWithFreeImage(OvGL::Engine::texturesSourceFolder + "\\" + imageName);
    }

    // Load bitmap into texture
    if (loadingResult == false)
    {
        OV_LOG_ERROR("Fail to create the bitmap for '%s' image in loader with id= %d.", imageName.c_str(), getId());
        return false;
    }

    bool isLinearEncoded = true;
    if (type == Ov::Texture::Type::albedo) {
        isLinearEncoded = false;
    }

    if (t.load(bitmap, isLinearEncoded) == false) {
        OV_LOG_ERROR("Fail to load bitmap with name %s in the texture2D with id= %d in loader with id= %d.",
            imageName, t.getId(), getId());
        return false;
    }

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load whole content starting from root node. A list is used instead of a vector because elements are not randomly reallocated.
 * @param container in/out data container
 * @param current current assimp node (starts with nullptr for root)
 * @return TF
 */
Ov::Node OVGL_API& OvGL::Loader::loadScene(OvGL::Container& container, void* current)
{
    // Root node?
    aiNode* _node;
    static int meshCount = 0;
    if (current == nullptr)
    {
        _node = reserved->modelScene->mRootNode;
        // The following does not change anything. So even if names are used to identify nodes, this is not it...
        _node->mName.Append(std::to_string(meshCount++).c_str());
    }
    else
        _node = reinterpret_cast<aiNode*>(current);

    // Setup node:
    Ov::Node n;
    std::string nodeName = std::string(_node->mName.C_Str()) + "Node";
    OV_LOG_DEBUG(nodeName.c_str());
    n.setName(nodeName.c_str());

    // Set matrix:
    glm::mat4 m;
    memcpy(&m, &_node->mTransformation, sizeof(glm::mat4));
    m = glm::transpose(m); // Assimp matrices are transposed wrt OpenGL
    n.setMatrix(m);

    // Check if this node contains a light:
    bool isLight = false;
    for (uint32_t c = 0; c < reserved->modelScene->mNumLights; c++)
    {
        aiLight* _light = reserved->modelScene->mLights[c];
        if (_light->mName == _node->mName)
        {
            isLight = true;

            // Create light source
            OvGL::Light l;
            this->loadLight(_light, l, n);
            container.add(l);
            n.addRenderable(container.getLastLight(), Ov::Renderable::empty);
        }
    }

    // Add mesh data?
    if (!isLight)
		switch (_node->mNumMeshes)
		{
			//////////
		case 0: //
		   // Logical node, nothing to do
			break;

			///////////
		default: // Can also be multi-mat          
		{
			for (uint32_t c = 0; c < _node->mNumMeshes; c++)
			{
                // Get mesh
                aiMesh* aimesh = reserved->modelScene->mMeshes[_node->mMeshes[c]];


                //////////////
                // Geometry //
                //////////////
                
                Ov::Object& geomFound = container.find(aimesh->mName.C_Str());
                std::reference_wrapper<OvGL::Geometry> geom = OvGL::Geometry::empty;
                if (geomFound != Ov::Object::empty)
                {
                    OV_LOG_WARN("Geometry already loaded");
                    geom = dynamic_cast<OvGL::Geometry&>(geomFound);
                }
                else
                {
                    OvGL::Geometry g;
                    if (this->loadGeometry(aimesh, g) == false) {
                        OV_LOG_ERROR("Fail to load geometry with name %s", aimesh->mName.C_Str());
                        continue;
                    }
                    container.add(g);
                    geom = container.getLastGeometry();
                }


                //////////////
                // Material //
                //////////////

                aiMaterial* aimaterial = reserved->modelScene->mMaterials[aimesh->mMaterialIndex];
                Ov::Object& materialFound = container.find(aimaterial->GetName().C_Str());
                std::reference_wrapper<OvGL::Material> material = OvGL::Material::empty;
                if (materialFound != Ov::Object::empty)
                {
                    OV_LOG_WARN("Material already loaded in loader with id= %d.", getId());
                    material = dynamic_cast<OvGL::Material&>(materialFound);
                }
                else {
                    OvGL::Material m;

                    if (m.getLutPos() == OvGL::Material::maxNrOfMaterials)
                    {
                        OV_LOG_ERROR("Too many materials: default one used instead in the loader with id= %d.", getId());
                        reserved->tooManyMaterialsFlag = true;
                        material = const_cast<OvGL::Material&>(OvGL::Material::getDefault());
                    }
                    else if (this->loadMaterial(aimaterial, m) == false)
                    {
                        OV_LOG_ERROR("Fail to load material name= %s: default one used instead in the loader with id= %d.",
                            aimaterial->GetName().C_Str(), getId());
                        material = const_cast<OvGL::Material&>(OvGL::Material::getDefault());
                    }
                    else {
                        container.add(m);
                        material = container.getLastMaterial();


                        /////////////
                        // Texture //
                        /////////////
                        
                        // Emission Texture
                        //aimaterial->GetTexture(aiTextureType_EMISSION_COLOR, 0, &nnn);
                        // Texture to load if present
                        std::vector<aiTextureType> textureTypeToLoad = {
                            aiTextureType_BASE_COLOR,
                            aiTextureType_NORMAL_CAMERA,
                            aiTextureType_HEIGHT,
                            aiTextureType_DIFFUSE_ROUGHNESS,
                            aiTextureType_METALNESS
                        };

                        // Check all type of texture to load
                        for (aiTextureType textureType : textureTypeToLoad)
                        {
                            // Check if the material has one texture for te he current type.
                            if (aimaterial->GetTextureCount(textureType) >= 1)
                            {
                                // Get texture type in overv
                                Ov::Texture::Type overvTextureType;
                                switch (textureType)
                                {
                                case aiTextureType_BASE_COLOR:
                                    overvTextureType = Ov::Texture::Type::albedo;
                                    break;
                                case aiTextureType_NORMAL_CAMERA:
                                    overvTextureType = Ov::Texture::Type::normal;
                                    break;
                                case aiTextureType_HEIGHT:
                                    overvTextureType = Ov::Texture::Type::height;
                                    break;
                                case aiTextureType_DIFFUSE_ROUGHNESS:
                                    overvTextureType = Ov::Texture::Type::roughness;
                                    break;
                                case aiTextureType_METALNESS:
                                    overvTextureType = Ov::Texture::Type::metalness;
                                    break;
                                default:
                                    OV_LOG_ERROR("Texture type not found in the loader with id= %d.", getId());
                                    continue;
                                }

                                // Needed parameters
                                aiString path = {};
                                aiTextureMapping* mapping = nullptr;
                                unsigned int* uvindex = nullptr;
                                float* blend = nullptr;
                                aiTextureOp* op = nullptr;
                                aiTextureMapMode* mapMode = nullptr;

                                // Get info on the first texture other one are ignored.
                                aimaterial->GetTexture(textureType, 0, &path,
                                    mapping, uvindex, blend, op, mapMode);

                                // Check if texture is already present
                                std::string imageName(path.C_Str());
                                std::size_t found = imageName.find_last_of("/\\");
                                imageName = imageName.substr(found + 1);
                                Ov::Object& textureFound = container.find(imageName);

                                if (textureFound == Ov::Object::empty) {
                                    
                                    OvGL::Texture2D texture;

                                    //Set default texture if maximum number is reached.
                                    if (texture.getLutPos() == OvGL::Texture::maxNrOfTextures)
                                    {
                                        OV_LOG_ERROR("Too many textures: default one used instead in the loader with id= %d", getId());
                                        material.get().setTexture(OvGL::Texture2D::getDefault(), overvTextureType);
                                    }
                                    else if (this->loadTexture2D(imageName, overvTextureType, texture))
                                    {
                                        // Add texture :
                                        container.add(texture);
                                        material.get().setTexture(container.getLastTexture2D(), overvTextureType);

                                    }
                                    else
                                    {
                                        OV_LOG_ERROR("Fail to load texture with name %s in the loader with id= %d.", imageName, getId());
                                        material.get().setTexture(OvGL::Texture2D::getDefault(), overvTextureType);
                                    }
                                }
                                else {
                                    material.get().setTexture(dynamic_cast<OvGL::Texture2D&>(textureFound), overvTextureType);
                                }
                            }
                        }
                    }
                }
                // Add to the node
                n.addRenderable(geom, material);
			}
		}
		}

    // Store reference and get new value:
    container.add(n);
    Ov::Node& _n = container.getLastNode();

    // Iterate through the scenegraph:
    for (uint32_t c = 0; c < _node->mNumChildren; c++)
        _n.addChild(loadScene(container, _node->mChildren[c]));

    // Done:   
    return _n;
}

#pragma endregion

#pragma region Debug

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Print on the console the Assimp post processing flags contained in the passed value.
 * @param pFlags flags for post processing.
 */
void OVGL_API OvGL::Loader::showParamsUsed(unsigned int pFlags) {
    if (pFlags & aiProcess_CalcTangentSpace)
        std::cout << "aiProcess_CalcTangentSpace" << std::endl;
    if (pFlags & aiProcess_FindInvalidData)
        std::cout << "aiProcess_FindInvalidData" << std::endl;
    if (pFlags & aiProcess_FixInfacingNormals)
        std::cout << "aiProcess_FixInfacingNormals" << std::endl;
    if (pFlags & aiProcess_GenSmoothNormals)
        std::cout << "aiProcess_GenSmoothNormals" << std::endl;
    if (pFlags & aiProcess_JoinIdenticalVertices)
        std::cout << "aiProcess_JoinIdenticalVertices" << std::endl;
    if (pFlags & aiProcess_SortByPType)
        std::cout << "aiProcess_SortByPType" << std::endl;
    if (pFlags & aiProcess_Triangulate)
        std::cout << "aiProcess_Triangulate" << std::endl;
    if (pFlags & aiProcess_ValidateDataStructure)
        std::cout << "aiProcess_ValidateDataStructure" << std::endl;
    if (pFlags & aiProcess_MakeLeftHanded)
        std::cout << "aiProcess_MakeLeftHanded" << std::endl;
    if (pFlags & aiProcess_FlipWindingOrder)
        std::cout << "aiProcess_FlipWindingOrder" << std::endl;
    if (pFlags & aiProcess_FlipUVs)
        std::cout << "aiProcess_FlipUVs" << std::endl;
    if (pFlags & aiProcess_PreTransformVertices)
        std::cout << "aiProcess_PreTransformVertices" << std::endl;
    if (pFlags & aiProcess_OptimizeMeshes)
        std::cout << "aiProcess_OptimizeMeshes" << std::endl;
}

#pragma endregion