/**
 * @file	overv_gl_elemarraybuffer.cpp
 * @brief	OpenGL elem array buffers
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief ElemArrayBuffer reserved structure.
 */
struct OvGL::ElemArrayBuffer::Reserved
{
    uint32_t nrOfFaces;  ///< Nr. of faces

    /**
     * Constructor.
     */
    Reserved() : nrOfFaces{ 0 }
    {}
};


///////////////////////////////////
// BODY OF CLASS ElemArrayBuffer //
///////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::ElemArrayBuffer::ElemArrayBuffer() : reserved(std::make_unique<OvGL::ElemArrayBuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the element buffer
 */
OVGL_API OvGL::ElemArrayBuffer::ElemArrayBuffer(const std::string& name) : OvGL::Buffer(name), reserved(std::make_unique<OvGL::ElemArrayBuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::ElemArrayBuffer::ElemArrayBuffer(ElemArrayBuffer&& other) noexcept : OvGL::Buffer(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::ElemArrayBuffer::~ElemArrayBuffer()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::ElemArrayBuffer::init()
{
    if (this->OvGL::Buffer::init() == false)
        return false;

    reserved->nrOfFaces = 0;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::ElemArrayBuffer::free()
{
    if (this->OvGL::Buffer::free() == false)
        return false;

    reserved->nrOfFaces = 0;

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the nr. of faces loaded in this EBO.
 * @return nr. of faces
 */
uint32_t OVGL_API OvGL::ElemArrayBuffer::getNrOfFaces() const
{
    return reserved->nrOfFaces;
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create buffer by allocating the required storage.
 * @param nfOfFaces number of faces to store
 * @param data pointer to the data to copy into the buffer
 * @return TF
 */
bool OVGL_API OvGL::ElemArrayBuffer::create(uint64_t nrOfFaces, const void* data)
{
    // Init buffer:
    if (!this->isInitialized())
        this->init();

    uint64_t size = nrOfFaces * sizeof(Ov::Geometry::FaceData);
    this->OvGL::Buffer::setSize(size);

    // Create it:		              
    const GLuint oglId = this->getOglHandle();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, oglId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

    // Done:
    reserved->nrOfFaces = nrOfFaces;
    return true;
}

#pragma endregion