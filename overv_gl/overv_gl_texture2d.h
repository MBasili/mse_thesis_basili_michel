/**
 * @file	overv_gl_texture2d.h
 * @brief	OpenGL 2D-specific texture properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief Class for modeling an OpenGL 2D texture.
  */
class OVGL_API Texture2D final : public OvGL::Texture
{
//////////
public: //
//////////

	// Special values:
	static Texture2D empty;

	// Const/dest:
	Texture2D();
	Texture2D(Texture2D&& other) noexcept;
	Texture2D(Texture2D const&) = delete;
	Texture2D(const Ov::Bitmap& bitmap);
	~Texture2D();

	// Default materials:
	static const Texture2D& getDefault();

	// Bitmap:
	bool load(const Ov::Bitmap& bitmap, bool isLinearEncoded = true);
	bool create(uint32_t sizeX, uint32_t sizeY, Format format);

	// Get/set:   
	uint32_t getSizeZ() const = delete;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Texture2D(const std::string& name);

	// Get/set:
	void setSizeZ(uint32_t sizeZ) = delete;
};
