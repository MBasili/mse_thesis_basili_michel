/**
 * @file	overv_gl_quad.h
 * @brief	OpenGL fullscreen quad renderer
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


/**
 * @brief Class for rendering a fullscreen quad.
 */
class OVGL_API Quad final : public Ov::Renderable
{
//////////
public: //
//////////

	// Const/dest:
	Quad();
	Quad(Quad&& other) noexcept;
	Quad(Quad const&) = delete;
	~Quad();

	// Rendering methods:   
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Quad(const std::string& name);
};