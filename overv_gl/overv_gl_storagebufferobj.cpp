/**
 * @file	overv_gl_storagebufferobj.cpp
 * @brief	OpenGL shader storage buffer object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLEW
#include <GLFW/glfw3.h>

////////////
// STATIC //
////////////

// Special values:
OvGL::StorageBufferObj OvGL::StorageBufferObj::empty("[empty]");

/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief StorageBufferObj reserved structure.
 */
struct OvGL::StorageBufferObj::Reserved
{
    void* dataPtr;       ///< Mapped data pointer   

    /**
     * Constructor.
     */
    Reserved() : dataPtr{ nullptr }
    {}
};


////////////////////////////////////
// BODY OF CLASS StorageBufferObj //
////////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::StorageBufferObj::StorageBufferObj() : reserved(std::make_unique<OvGL::StorageBufferObj::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the storage buffer.
 */
OVGL_API OvGL::StorageBufferObj::StorageBufferObj(const std::string& name) : Ov::Renderable(name),
OvGL::Buffer(name), reserved(std::make_unique<OvGL::StorageBufferObj::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::StorageBufferObj::StorageBufferObj(StorageBufferObj&& other) noexcept : Ov::Renderable(std::move(other)),
OvGL::Buffer(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::StorageBufferObj::~StorageBufferObj()
{
    OV_LOG_DETAIL("[-]");

    if (reserved->dataPtr)
    {
        const GLuint oglId = this->getOglHandle();
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, oglId);
        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    }
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::StorageBufferObj::init()
{
    if (reserved->dataPtr != nullptr) {
        glUnmapBuffer(this->getOglHandle());
        reserved->dataPtr = nullptr;
    }

    if (this->OvGL::Buffer::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::StorageBufferObj::free()
{
    if (reserved->dataPtr != nullptr) {
        glUnmapBuffer(this->getOglHandle());
        reserved->dataPtr = nullptr;
    }

    if (this->OvGL::Buffer::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create buffer by allocating the required storage.
 * @param nfOfBytes number of bytes to store
 * @param data (optional) initialization data
 * @return TF
 */
bool OVGL_API OvGL::StorageBufferObj::create(uint64_t nrOfBytes, void* data)
{
    // Init buffer:
    if (this->isInitialized() == false)
        this->init();

    // Safety net:
    // Get max size SBO
    GLint maxSize;
    glGetIntegerv(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &maxSize);

    if (maxSize < nrOfBytes) {
        OV_LOG_ERROR("Fail to create SBO because maximum size exceeded. Max size is %d bytes", maxSize);
        return false;
    }

    uint64_t size = nrOfBytes;
    this->OvGL::Buffer::setSize(size);

    // Create it:		              
    const GLuint oglId = this->getOglHandle();
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, oglId);
    GLbitfield flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT; // GL_MAP_READ_BIT?   
    glBufferStorage(GL_SHADER_STORAGE_BUFFER, nrOfBytes, data, flags);

    // Map buffer forever:
    reserved->dataPtr = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, nrOfBytes, flags);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Unbind any uniform buffer object.
 */
void OVGL_API OvGL::StorageBufferObj::reset()
{
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get SSBO max size in bytes.
 * @return max SSBO size in bytes
 */
uint64_t OVGL_API OvGL::StorageBufferObj::getMaxSize()
{
    GLint64 size;
    glGetInteger64v(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &size);
    return static_cast<uint64_t>(size);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get access to the mapped data.
 * @return pointer to mapped data
 */
void OVGL_API* OvGL::StorageBufferObj::getDataPtr() const
{
    return reserved->dataPtr;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data GLuint pointer to the binding index
 * @return TF
 */
bool OVGL_API OvGL::StorageBufferObj::render(uint32_t value, void* data) const
{
    const GLuint oglId = this->getOglHandle();
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, value, oglId);

    // Done:
    return true;
}

#pragma endregion