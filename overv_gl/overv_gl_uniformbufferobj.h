/**
 * @file	overv_gl_uniformbufferobj.h
 * @brief	OpenGL uniform buffer object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief OpenGL uniform buffer object
  */
class OVGL_API UniformBufferObj final : public OvGL::Buffer, Ov::Renderable
{
//////////
public: //
//////////

	// Const/dest:
	UniformBufferObj();
	UniformBufferObj(UniformBufferObj&& other) noexcept;
	UniformBufferObj(UniformBufferObj const&) = delete;
	~UniformBufferObj();

	// Get/set:
	static uint64_t getMaxSize();

	// Management:
	static void reset();
	bool create(uint64_t nrOfBytes, void* data = nullptr);
	void* getDataPtr() const;

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	UniformBufferObj(const std::string& name);
};
