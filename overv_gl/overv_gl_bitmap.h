#pragma once
/**
 * @file	overv_gl_bitmap.h
 * @brief	Basic generic bitmap properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 /**
  * @brief Class for modeling a generic bitmap.
  */
class OVGL_API Bitmap : public Ov::Image
{
//////////
public: //
//////////

	// Special values:
	static Bitmap empty;

	// Const/dest:
	Bitmap();
	Bitmap(Bitmap&& other) noexcept;
	Bitmap(Bitmap const&) = delete;
	virtual ~Bitmap();

	// Operators
	Bitmap& operator=(const Bitmap&) = delete;
	Bitmap& operator=(Bitmap&&) = delete;

	// Loader:
	bool loadKTX(const std::string& filename, Format format) override;

	// Save:
	bool saveAsKTX(bool supercompress = true, bool preserveHighQuality = true) const override;

/////////////
protected: //
/////////////

   // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Bitmap(const std::string& name);
};