/**
 * @file	overv_gl_framebuffer.h
 * @brief	OpenGL framebuffer properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */
#pragma once


/**
 * @brief Class for modeling an OpenGL framebuffer.
 */
class OVGL_API Framebuffer final : public Ov::Framebuffer
{
//////////
public: //
//////////

	// Const/dest:
	Framebuffer();
	Framebuffer(Framebuffer&& other) noexcept;
	Framebuffer(Framebuffer const&) = delete;
	~Framebuffer();

	// Managed:
	bool init() override;
	bool free() override;

	// Attachments:
	bool attachTexture(const Ov::Texture& texture, uint32_t level = 0, uint32_t side = 0);
	bool attachColorBuffer(uint32_t sizeX, uint32_t sizeY);
	bool attachDepthBuffer(uint32_t sizeX, uint32_t sizeY);
	bool validate() const;

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;
	static void reset(uint32_t viewportSizeX, uint32_t viewportSizeY);
	bool blit(uint32_t viewportSizeX, uint32_t viewportSizeY) const;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Framebuffer(const std::string& name);

	// Get/set:
	uint32_t getOglHandle() const;

	// Management:
	bool updateMrtCache();
};