/**
 * @file	overv_gl_shader.cpp
 * @brief	OpenGL shader properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// Magic enum:
#include "magic_enum.hpp"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// C/C++:
#include <algorithm>

////////////
// STATIC //
////////////

// Special values:
OvGL::Shader OvGL::Shader::empty("[empty]");

/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Shader reserved structure.
 */
struct OvGL::Shader::Reserved
{
    GLuint oglId;     ///< OpenGL shader ID   


    /**
     * Constructor.
     */
    Reserved() : oglId{ 0 }
    {}
};


////////////
// STATIC //
////////////

// Global preprocessor:   
Ov::Preprocessor OvGL::Shader::preprocessor;


//////////////////////////
// BODY OF CLASS Shader //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Shader::Shader() : reserved(std::make_unique<OvGL::Shader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the shader.
 */
OVGL_API OvGL::Shader::Shader(const std::string& name) : Ov::Shader(name), reserved(std::make_unique<OvGL::Shader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Shader::Shader(Shader&& other) noexcept : Ov::Shader(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Shader::~Shader()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized()) // Because of the move constructor      
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL shader.
 * @return TF
 */
bool OVGL_API OvGL::Shader::init()
{
    if (this->Ov::Managed::init() == false)
        return false;

    // Free shader if stored:
    if (reserved->oglId)
    {
        glDeleteShader(reserved->oglId);
        reserved->oglId = 0;
    }

    // Create it:		        
    GLuint glKind = 0;
    switch (this->getType())
    {
    case Ov::Shader::Type::vertex:
        glKind = GL_VERTEX_SHADER;
        break;

    case Ov::Shader::Type::tessellation_ctrl:
        glKind = GL_TESS_CONTROL_SHADER;
        break;

    case Ov::Shader::Type::tessellation_eval:
        glKind = GL_TESS_EVALUATION_SHADER;
        break;

    case Ov::Shader::Type::geometry:
        glKind = GL_GEOMETRY_SHADER;
        break;

    case Ov::Shader::Type::fragment:
        glKind = GL_FRAGMENT_SHADER;
        break;

    case Ov::Shader::Type::compute:
        glKind = GL_COMPUTE_SHADER;
        break;

    default:
        OV_LOG_ERROR("Invalid shader type");
        this->setInitialized(false);
        return false;
    }

    // Load program:
    reserved->oglId = glCreateShader(glKind);
    if (reserved->oglId == 0)
    {
        OV_LOG_ERROR("Unable to create shader");
        this->setInitialized(false);
        return false;
    }

    // Done:      
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Shader::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Free shader if stored:
    if (reserved->oglId)
    {
        glDeleteShader(reserved->oglId);
        reserved->oglId = 0;
    }

    // Done:      
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the GLuint shader ID.
 * @return shader ID or 0 if not valid
 */
uint32_t OVGL_API OvGL::Shader::getOglHandle() const
{
    return reserved->oglId;
}

#pragma endregion

#pragma region AssessingData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL shader.
 * @return TF
 */
bool OVGL_API OvGL::Shader::load(Type kind, const std::string& code)
{
    // Preprocess:
    std::string _code = preprocessor.preprocess(code);
    OV_LOG_DETAIL("[preprocessed] %s", _code.c_str());

    // Preload:
    if (this->Ov::Shader::load(kind, _code) == false)
        return false;

    // Init shader:
    if (this->init() == false)
        return false;

    // Prepare code:
    std::string kindMacro = std::string(OV_LOG_ENUM(kind)) + "_SHADER";
    std::transform(kindMacro.begin(), kindMacro.end(), kindMacro.begin(), ::toupper);
    kindMacro = "#define OV_" + kindMacro + "\n";

    const char* sources[3] = { OvGL::Shader::glslVersion, kindMacro.c_str(), this->getCode().c_str() };
    glShaderSource(reserved->oglId, 3, sources, nullptr);
    glCompileShader(reserved->oglId);

    // Check status:
    GLint status;
    glGetShaderiv(reserved->oglId, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        // Verify shader:
        char buffer[OvGL::Shader::maxLogSize];
        int32_t length = 0;
        memset(buffer, 0, OvGL::Shader::maxLogSize);

        glGetShaderInfoLog(reserved->oglId, OvGL::Shader::maxLogSize, &length, buffer);
        if (length > 0)
        {
            OV_LOG_ERROR("Shader not compiled (%s)", buffer);

            return false;
        }
    }
    else
        OV_LOG_DEBUG("Shader compiled");

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Shader::render(uint32_t value, void* data) const
{
    // Done:
    return true;
}

#pragma endregion