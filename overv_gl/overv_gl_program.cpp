/**
 * @file	overv_gl_program.cpp
 * @brief	OpenGL program properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW:
#include <GLFW/glfw3.h>

// C/C++:
#include<unordered_map>


////////////
// STATIC //
////////////

// Special values:
OvGL::Program OvGL::Program::empty("[empty]");

// Cache:
std::reference_wrapper<OvGL::Program> OvGL::Program::cache = OvGL::Program::empty;

/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Program reserved structure.
 */
struct OvGL::Program::Reserved
{
    GLuint oglId;                                                ///< OpenGL program ID   
    std::vector<std::reference_wrapper<Ov::Shader>> shader;    ///< Shaders used by the program
    std::unordered_map<std::string, GLint> location;            ///< Lookup table for uniform locations

    /**
     * Constructor.
     */
    Reserved() : oglId{ 0 }
    {}
};


///////////////////////////
// BODY OF CLASS Program //
///////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Program::Program() : reserved(std::make_unique<OvGL::Program::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the program
 */
OVGL_API OvGL::Program::Program(const std::string& name) : Ov::Program(name), reserved(std::make_unique<OvGL::Program::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Program::Program(Program&& other) noexcept : Ov::Program(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Program::~Program()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized()) // Because of the move constructor   
        this->free();
}


#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an OpenGL program.
 * @return TF
 */
bool OVGL_API OvGL::Program::init()
{
    if (this->Ov::Managed::init() == false)
        return false;

    // Free program if stored:
    if (reserved->oglId)
    {
        glDeleteProgram(reserved->oglId);
        reserved->oglId = 0;
    }

    // Create program:
    reserved->oglId = glCreateProgram();
    if (reserved->oglId == 0)
    {
        OV_LOG_ERROR("Unable to create program");
        this->setInitialized(false);
        return false;
    }

    // Done:      
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an OpenGL instance.
 * @return TF
 */
bool OVGL_API OvGL::Program::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Free shader if stored:
    if (reserved->oglId)
    {
        glDeleteProgram(reserved->oglId);
        reserved->oglId = 0;
    }

    // Done:      
    return true;
}

#pragma endregion

#pragma region Building

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Build program.
 * @param args variadic list of arguments
 * @return TF
 */
bool OVGL_API OvGL::Program::build(std::initializer_list<std::reference_wrapper<Ov::Shader>> args)
{
    if (this->Ov::Program::build(args) == false)
        return false;

    reserved->shader.clear();
    for (auto& arg : args)
    {
        if (arg.get() == Ov::Shader::empty)
        {
            OV_LOG_ERROR("Invalid params (empty shader)");
            return false;
        }
        reserved->shader.push_back(arg);
    }

    // Validate possible configurations:
    if (this->getNrOfShaders() >= 3)
    {
        OV_LOG_ERROR("Invalid/unsupported shader sequence");
        return false;
    }

    // Init program:
    this->init();

    // Link shaders:
    for (uint32_t c = 0; c < this->getNrOfShaders(); c++)
    {
        const OvGL::Shader& s = dynamic_cast<const OvGL::Shader&>(this->getShader(c));
        glAttachShader(reserved->oglId, s.getOglHandle());
    }
    glLinkProgram(reserved->oglId);

    // Check:
    GLint success;
    glGetProgramiv(reserved->oglId, GL_LINK_STATUS, &success);
    if (!success)
    {
        OV_LOG_ERROR("Program link status error, message:");
        char buffer[OvGL::Shader::maxLogSize];
        int32_t length;
        glGetProgramInfoLog(reserved->oglId, OvGL::Shader::maxLogSize, &length, buffer);
        if (length > 0)
        {
            OV_LOG_ERROR("%s", buffer);
        }
        else
        {
            OV_LOG_ERROR("[no message]");
        }
        return false;
    }

    // Validate:
    glValidateProgram(reserved->oglId);
    glGetProgramiv(reserved->oglId, GL_VALIDATE_STATUS, &success);
    if (success == GL_FALSE)
    {
        OV_LOG_ERROR("Unable to validate program");
        char buffer[OvGL::Shader::maxLogSize];
        int32_t length;
        glGetProgramInfoLog(reserved->oglId, OvGL::Shader::maxLogSize, &length, buffer);
        if (length > 0)
        {
            OV_LOG_ERROR("Program validation error: %s", buffer);
        }
        return false;
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the GLuint shader ID.
 * @return shader ID or 0 if not valid
 */
int32_t OVGL_API OvGL::Program::getShaderID() {
    return reserved->oglId;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get number of shaders.
 * @return number of shaders
 */
uint32_t OVGL_API OvGL::Program::getNrOfShaders() const
{
    return static_cast<uint32_t>(reserved->shader.size());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the shader at the specified position.
 * @param id shader position in the list
 * @return shader reference
 */
const Ov::Shader OVGL_API& OvGL::Program::getShader(uint32_t id) const
{
    // Safety net:
    if (id >= reserved->shader.size())
    {
        OV_LOG_ERROR("Invalid params");
        return OvGL::Shader::empty;
    }
    return reserved->shader[id];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type float.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setFloat(const std::string& name, float value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    glUniform1f(location, value);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type int.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setInt(const std::string& name, int32_t value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniform1i(location, value);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type unsigned int.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setUInt(const std::string& name, uint32_t value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniform1ui(location, value);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type unsigned int 64.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setUInt64(const std::string& name, uint64_t value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniformHandleui64ARB(location, value);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type vec3.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setVec3(const std::string& name, const glm::vec3& value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniform3fv(location, 1, glm::value_ptr(value));
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type vec4.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setVec4(const std::string& name, const glm::vec4& value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniform4fv(location, 1, glm::value_ptr(value));
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type mat3.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setMat3(const std::string& name, const glm::mat3& value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type mat4.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setMat4(const std::string& name, const glm::mat4& value)
{
    GLint location = getParamLocation(name);
    if (location == -1)
        return false;

    // Done:
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set a uniform value of type mat4.
 * @param name variable name
 * @param value variable value
 * @return TF
 */
bool OVGL_API OvGL::Program::setUInt64Array(const std::string& name, const uint64_t* value, const uint32_t count)
{
    GLint location = getParamLocationARB(name);
    if (location == -1)
        return false;

    // Done:
    glUniformHandleui64vARB(location, count, value);
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get parameter location given its name.
 * @param name variable name
 * @return param location or -1 if not found
 */
int32_t OVGL_API OvGL::Program::getParamLocation(const std::string& name)
{
    // Safety net:
    if (name.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return -1;
    }

    this->render();

    // Use or add?
    auto location = reserved->location.find(name);
    if (location == reserved->location.end())
    {
        GLint position = glGetUniformLocation(reserved->oglId, name.c_str());
        if (position == -1)
        {
            OV_LOG_ERROR("Variable '%s' not found", name.c_str());
            return position;
        }
        reserved->location.insert(std::make_pair(name, position));
        location = reserved->location.find(name);
    }

    // Done:      
    return location->second;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get parameter location given its name.
 * @param name variable name
 * @return param location or -1 if not found
 */
int32_t OVGL_API OvGL::Program::getParamLocationARB(const std::string& name)
{
    // Safety net:
    if (name.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return -1;
    }

    this->render();

    // Use or add?
    auto location = reserved->location.find(name);
    if (location == reserved->location.end())
    {
        GLint position = glGetUniformLocationARB(reserved->oglId, name.c_str());
        if (position == -1)
        {
            OV_LOG_WARN("Variable '%s' not found", name.c_str());
            return position;
        }
        reserved->location.insert(std::make_pair(name, position));
        location = reserved->location.find(name);
    }

    // Done:      
    return location->second;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::Program::render(uint32_t value, void* data) const
{
    // Render only if necessary:   
    if (OvGL::Program::cache.get() != *this)
    {
        glUseProgram(reserved->oglId);
        OvGL::Program::cache = const_cast<OvGL::Program&>(*this);
    }

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Detach program.
 */
void OVGL_API OvGL::Program::reset()
{
    OvGL::Program::cache = OvGL::Program::empty;
    glUseProgram(0);
}

#pragma endregion

#pragma region Compute-only

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Dispatch compute for a compute shader.
 * @param sizeX workgroup size for X
 * @param sizeY (optional) workgroup size for Y
 * @param sizeZ (optional) workgroup size for Z
 * @return TF
 */
bool OVGL_API OvGL::Program::compute(uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ) const
{
    // Check a compute shader is really attached
    bool isComputeShaderPresent = false;
    for (int c = 0; c < reserved->shader.size(); c++)
        if (reserved->shader[c].get().getType() == Ov::Shader::Type::compute)
            isComputeShaderPresent = true;

    if (isComputeShaderPresent == false) {
        OV_LOG_ERROR("There is not a compute shader in the program with id= %d", getId());
        return false;
    }

    // Run kernel:
    render();
    glDispatchCompute(sizeX, sizeY, sizeZ);

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Wait for the termination of the current compute shader.
 * @return TF
 */
bool OVGL_API OvGL::Program::wait() const
{
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    glFinish();

    // Done:
    return true;
}

#pragma endregion

#pragma region Cache

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the last rendered program.
 * @return last rendered program
 */
OvGL::Program OVGL_API& OvGL::Program::getCached()
{
    return cache;
}

#pragma endregion