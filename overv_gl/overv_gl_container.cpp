/**
 * @file	overv_gl_container.cpp
 * @brief	Basic generic, centralized data container
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// C/C++:
#include <algorithm>

// GLFW
#include <variant>


////////////
// STATIC //
////////////

// Special values:
OvGL::Container OvGL::Container::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Container reserved structure.
 */
struct OvGL::Container::Reserved
{
    std::list<Ov::Node> allNodes;
    std::list<OvGL::Geometry> allGeoms;
    std::list<OvGL::Material> allMats;
    std::list<OvGL::Texture> allTexs;
    std::list<OvGL::Texture2D> allTexs2D;
    std::list<OvGL::Light> allLights;


    /**
     * Constructor.
     */
    Reserved()
    {}
};


/////////////////////////////
// BODY OF CLASS Container //
/////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Container::Container() : reserved(std::make_unique<OvGL::Container::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the container
 */
OVGL_API OvGL::Container::Container(const std::string& name) : Ov::Container(name), reserved(std::make_unique<OvGL::Container::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Container::Container(Container&& other) noexcept : Ov::Container(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Container::~Container()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Manager

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Resets the content of the container.
 * @return TF
 */
bool OVGL_API OvGL::Container::reset()
{
    reserved->allNodes.clear();
    reserved->allGeoms.clear();
    reserved->allMats.clear();
    reserved->allTexs.clear();
    reserved->allTexs2D.clear();
    reserved->allLights.clear();

    // Done:
    setDirty(true);
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds the given object to the proper container.
 * @return TF
 */
bool OVGL_API OvGL::Container::add(Ov::Object& obj)
{
    // Safety net:
    if (obj == Ov::Object::empty)
    {
		OV_LOG_ERROR("Invalid params");
		return false;
	}

	// Sort by type:
	if (dynamic_cast<OvGL::Geometry*>(&obj))
	{
		reserved->allGeoms.push_back(std::move(dynamic_cast<OvGL::Geometry&>(obj)));
		this->Ov::Container::add(reserved->allGeoms.back());
		return true;
	}
	else if (dynamic_cast<Ov::Node*>(&obj))
	{
		reserved->allNodes.push_back(std::move(dynamic_cast<Ov::Node&>(obj)));
		this->Ov::Container::add(reserved->allNodes.back());
		return true;
	}
	else if (dynamic_cast<OvGL::Material*>(&obj))
	{
		reserved->allMats.push_back(std::move(dynamic_cast<OvGL::Material&>(obj)));
		this->Ov::Container::add(reserved->allMats.back());
		return true;
	}
    else if (dynamic_cast<OvGL::Texture2D*>(&obj))
    {
        reserved->allTexs2D.push_back(std::move(dynamic_cast<OvGL::Texture2D&>(obj)));
        this->Ov::Container::add(reserved->allTexs2D.back());
        return true;
    }
	else if (dynamic_cast<OvGL::Texture*>(&obj))
	{
		reserved->allTexs.push_back(std::move(dynamic_cast<OvGL::Texture&>(obj)));
		this->Ov::Container::add(reserved->allTexs.back());
		return true;
	}
	else if (dynamic_cast<OvGL::Light*>(&obj))
	{
		reserved->allLights.push_back(std::move(dynamic_cast<OvGL::Light&>(obj)));
		this->Ov::Container::add(reserved->allLights.back());
		return true;
	}

    // Done:
    OV_LOG_ERROR("Unsupported type");
    return false;
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added node.
 * @return last added node
 */
Ov::Node OVGL_API& OvGL::Container::getLastNode() const
{
    // Safety net:
    if (reserved->allNodes.empty())
        return Ov::Node::empty;
    return reserved->allNodes.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added geometry.
 * @return last added geometry
 */
OvGL::Geometry OVGL_API& OvGL::Container::getLastGeometry() const
{
    // Safety net:
    if (reserved->allGeoms.empty())
        return OvGL::Geometry::empty;
    return reserved->allGeoms.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added material.
 * @return last added material
 */
OvGL::Material OVGL_API& OvGL::Container::getLastMaterial() const
{
    // Safety net:
    if (reserved->allMats.empty())
        return OvGL::Material::empty;
    return reserved->allMats.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added light.
 * @return last added light
 */
OvGL::Light OVGL_API& OvGL::Container::getLastLight() const
{
    // Safety net:
    if (reserved->allLights.empty())
        return OvGL::Light::empty;
    return reserved->allLights.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added texture.
 * @return last added texture
 */
OvGL::Texture OVGL_API& OvGL::Container::getLastTexture() const
{
    // Safety net:
    if (reserved->allTexs.empty())
        return OvGL::Texture::empty;
    return reserved->allTexs.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets last added texture.
 * @return last added texture
 */
OvGL::Texture2D OVGL_API& OvGL::Container::getLastTexture2D() const
{
    // Safety net:
    if (reserved->allTexs2D.empty())
        return OvGL::Texture2D::empty;
    return reserved->allTexs2D.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of nodes.
 * @return list of nodes
 */
std::list<Ov::Node> OVGL_API& OvGL::Container::getNodeList()
{
    return reserved->allNodes;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of materials.
 * @return list of materials
 */
std::list<OvGL::Material> OVGL_API& OvGL::Container::getMaterialList()
{
    return reserved->allMats;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of textures2D.
 * @return list of texture2D
 */
std::list<OvGL::Texture> OVGL_API& OvGL::Container::getTextureList()
{
    return reserved->allTexs;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of textures2D.
 * @return list of texture2D
 */
std::list<OvGL::Texture2D> OVGL_API& OvGL::Container::getTexture2DList()
{
    return reserved->allTexs2D;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of geometries.
 * @return list of geometries
 */
std::list<OvGL::Geometry> OVGL_API& OvGL::Container::getGeometryList()
{
    return reserved->allGeoms;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets direct access to the list of lights.
 * @return list of lights
 */
std::list<OvGL::Light> OVGL_API& OvGL::Container::getLightList()
{
    return reserved->allLights;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a raw pointer to the final class (to be casted accordingly).
 * @return pointer to this
 */
void OVGL_API* OvGL::Container::getFinalClass()
{
    // Done:   
    return this;
}

#pragma endregion

#pragma region Finders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns, if existing, the first object with the given name among its various lists.
 * @param name object name
 * @return found object or empty
 */
Ov::Object OVGL_API& OvGL::Container::find(const std::string& name) const
{
    // Safety net:
    if (name.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Object::empty;
    }

    // Seach in materials:
    for (auto& c : reserved->allMats)
        if (c.getName() == name)
            return c;

    // Seach in textures:
    for (auto& c : reserved->allTexs)
        if (c.getName() == name)
            return c;

    for (auto& c : reserved->allTexs2D)
        if (c.getName() == name)
            return c;


    // Seach in geometries:
    for (auto& c : reserved->allGeoms)
        if (c.getName() == name)
            return c;

    // Seach in nodes:
    for (auto& c : reserved->allNodes)
        if (c.getName() == name)
            return c;

    // Seach in lights:
    for (auto& c : reserved->allLights)
        if (c.getName() == name)
            return c;

    // Not found:
    return Ov::Object::empty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns, if existing, the first object with the given ID among its various lists.
 * @param id object id
 * @return found object or empty
 */
Ov::Object OVGL_API& OvGL::Container::find(const uint32_t id) const
{
    // Fast lane:
    if (id == 0)
        return Ov::Object::empty;

    // Seach in materials:
    for (auto& c : reserved->allMats)
        if (c.getId() == id)
            return c;

    // Search in textures:
    for (auto& c : reserved->allTexs)
        if (c.getId() == id)
            return c;

    for (auto& c : reserved->allTexs2D)
        if (c.getId() == id)
            return c;

    // Seach in geometries:
    for (auto& c : reserved->allGeoms)
        if (c.getId() == id)
            return c;

    // Seach in nodes:
    for (auto& c : reserved->allNodes)
        if (c.getId() == id)
            return c;

    // Seach in lights:
    for (auto& c : reserved->allLights)
        if (c.getId() == id)
            return c;

    // Not found:
    return Ov::Object::empty;
}

#pragma endregion