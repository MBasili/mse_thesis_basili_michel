/**
 * @file	overv_gl_arraybuffer.cpp
 * @brief	OpenGL array buffers
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief ArrayBuffer reserved structure.
 */
struct OvGL::ArrayBuffer::Reserved
{
    uint32_t nrOfVertices;  ///< Nr. of vertices

    /**
     * Constructor.
     */
    Reserved() : nrOfVertices{ 0 }
    {}
};


///////////////////////////////
// BODY OF CLASS ArrayBuffer //
///////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::ArrayBuffer::ArrayBuffer() : reserved(std::make_unique<OvGL::ArrayBuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of arraybuffer.
 */
OVGL_API OvGL::ArrayBuffer::ArrayBuffer(const std::string& name) : OvGL::Buffer(name), reserved(std::make_unique<OvGL::ArrayBuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::ArrayBuffer::ArrayBuffer(ArrayBuffer&& other) noexcept : OvGL::Buffer(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::ArrayBuffer::~ArrayBuffer()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::ArrayBuffer::init()
{
    if (this->OvGL::Buffer::init() == false)
        return false;

    reserved->nrOfVertices = 0;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::ArrayBuffer::free()
{
    if (this->OvGL::Buffer::free() == false)
        return false;

    reserved->nrOfVertices = 0;

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the nr. of vertices loaded in this VBO.
 * @return nr. of vertices
 */
uint32_t OVGL_API OvGL::ArrayBuffer::getNrOfVertices() const
{
    return reserved->nrOfVertices;
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create buffer by allocating the required storage.
 * @param nfOfVertices number of vertices to store
 * @param data pointer to the data to copy into the buffer
 * @return TF
 */
bool OVGL_API OvGL::ArrayBuffer::create(uint64_t nrOfVertices, const void* data, bool vertexFlag, bool normalFlag, bool uvFlag, bool tangentFlag)
{
    // Unit size:
    uint64_t unitSize = 0;
    if (vertexFlag)   unitSize += sizeof(Ov::Geometry::VertexData::vertex);
    if (normalFlag)   unitSize += sizeof(Ov::Geometry::VertexData::normal);
    if (uvFlag)       unitSize += sizeof(Ov::Geometry::VertexData::uv);
    if (tangentFlag)  unitSize += sizeof(Ov::Geometry::VertexData::tangent);
    if (unitSize == 0)
    {
        OV_LOG_DETAIL("Invalid params (flags can't be all set to false)");
        return false;
    }

    // Init buffer:
    if (!this->isInitialized())
        this->init();

    uint64_t size = nrOfVertices * unitSize;
    this->Ov::Buffer::setSize(size);

    // Create it:		              
    const GLuint oglId = this->getOglHandle();
    glBindBuffer(GL_ARRAY_BUFFER, oglId);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

    glBindVertexBuffer(0, oglId, 0, static_cast<GLsizei>(unitSize));

    uint32_t offset = 0;
    if (vertexFlag)
    {
        glVertexAttribFormat(static_cast<GLuint>(Attrib::vertex), 3, GL_FLOAT, GL_FALSE, offset);
        glVertexAttribBinding(static_cast<GLuint>(Attrib::vertex), 0);
        glEnableVertexAttribArray(static_cast<GLuint>(Attrib::vertex));
        offset += sizeof(Ov::Geometry::VertexData::vertex);
    }

    if (normalFlag)
    {
        glVertexAttribFormat(static_cast<GLuint>(Attrib::normal), 4, GL_INT_2_10_10_10_REV, GL_TRUE, offset);
        glVertexAttribBinding(static_cast<GLuint>(Attrib::normal), 0);
        glEnableVertexAttribArray(static_cast<GLuint>(Attrib::normal));
        offset += sizeof(Ov::Geometry::VertexData::normal);
    }

    if (uvFlag)
    {
        glVertexAttribFormat(static_cast<GLuint>(Attrib::texcoord), 2, GL_HALF_FLOAT, GL_FALSE, offset);
        glVertexAttribBinding(static_cast<GLuint>(Attrib::texcoord), 0);
        glEnableVertexAttribArray(static_cast<GLuint>(Attrib::texcoord));
        offset += sizeof(Ov::Geometry::VertexData::uv);
    }

    if (tangentFlag)
    {
        glVertexAttribFormat(static_cast<GLuint>(Attrib::tangent), 4, GL_INT_2_10_10_10_REV, GL_TRUE, offset);
        glVertexAttribBinding(static_cast<GLuint>(Attrib::tangent), 0);
        glEnableVertexAttribArray(static_cast<GLuint>(Attrib::tangent));
        offset += sizeof(Ov::Geometry::VertexData::tangent);
    }

    // Done:
    reserved->nrOfVertices = nrOfVertices;
    return true;
}

#pragma endregion