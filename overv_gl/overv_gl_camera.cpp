/**
 * @file	overv_gl_camera.cpp
 * @brief	OGL camera class
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Camera class reserved structure.
 */
struct OvGL::Camera::Reserved
{
    /**
     * Constructor
     */
    Reserved()
    {}
};


//////////////////////////
// BODY OF CLASS Camera //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Camera::Camera() : reserved(std::make_unique<OvGL::Camera::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the camera.
 */
OVGL_API OvGL::Camera::Camera(const std::string& name) : Ov::Camera(name), reserved(std::make_unique<OvGL::Camera::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Camera::Camera(Camera&& other) noexcept : Ov::Camera(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Camera::~Camera()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion