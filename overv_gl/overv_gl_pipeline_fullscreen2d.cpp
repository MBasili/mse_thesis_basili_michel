/**
 * @file		engine_pipeline_fullscreen2d.cpp
 * @brief	A pipeline for rendering a texture to the fullscreen in 2D
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) SUPSI
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>
#include <GLFW/glfw3.h>


/////////////
// SHADERS //
/////////////

#pragma region Shaders

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default pipeline vertex shader.
 */
static const std::string pipeline_vs = R"(
// Out:
out vec2 texCoord;

void main()
{   
   float x = -1.0f + float((gl_VertexID & 1) << 2);
   float y = -1.0f + float((gl_VertexID & 2) << 1);
   
   texCoord.x = (x + 1.0f) * 0.5f;
   texCoord.y = (y + 1.0f) * 0.5f;
   
   gl_Position = vec4(x, y, 1.0f, 1.0f);
})";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default pipeline fragment shader.
 */
static const std::string pipeline_fs = R"(
// In:   
in vec2 texCoord;
   
// Out:
out vec4 outFragment;

// Uniform:
layout (bindless_sampler) uniform sampler2D texture0;

void main()
{
    vec4 texture_value = texture(texture0, texCoord); 
    outFragment = vec4(texture_value.xyz, 1.0f);
})";

#pragma endregion


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief PipelineFullscreen2D reserved structure.
 */
struct OvGL::PipelineFullscreen2D::Reserved
{
    OvGL::Shader vs;
    OvGL::Shader fs;
    OvGL::Program program;
    OvGL::VertexArrayObj vao;   ///< Dummy VAO, always required by context profiles


    /**
     * Constructor.
     */
    Reserved()
    {}
};


////////////////////////////////////////
// BODY OF CLASS PipelineFullscreen2D //
////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::PipelineFullscreen2D::PipelineFullscreen2D() : reserved(std::make_unique<OvGL::PipelineFullscreen2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    this->setProgram(reserved->program);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVGL_API OvGL::PipelineFullscreen2D::PipelineFullscreen2D(const std::string& name) : Ov::Pipeline(name), reserved(std::make_unique<OvGL::PipelineFullscreen2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    this->setProgram(reserved->program);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::PipelineFullscreen2D::PipelineFullscreen2D(PipelineFullscreen2D&& other) : Ov::Pipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::PipelineFullscreen2D::~PipelineFullscreen2D()
{
    OV_LOG_DETAIL("[-]");
    if (this->isInitialized() && reserved)
        free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::PipelineFullscreen2D::init()
{
    // Already initialized?
    if (this->Ov::Managed::init() == false)
        return false;
    if (!this->isDirty())
        return false;

    // Build:
    reserved->vs.load(OvGL::Shader::Type::vertex, pipeline_vs);
    reserved->fs.load(OvGL::Shader::Type::fragment, pipeline_fs);
    if (reserved->program.build({ reserved->vs, reserved->fs }) == false)
    {
        OV_LOG_ERROR("Unable to build fullscreen2D program");
        return false;
    }
    this->setProgram(reserved->program);

    // Init dummy VAO:
    if (reserved->vao.init() == false)
    {
        OV_LOG_ERROR("Unable to init VAO for fullscreen2D");
        return false;
    }

    // Done: 
    this->setDirty(false);
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases this pipeline.
 * @return TF
 */
bool OVGL_API OvGL::PipelineFullscreen2D::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Main rendering method for the pipeline.
 * @param camera view camera
 * @param list list of renderables
 * @return TF
 */
bool OVGL_API OvGL::PipelineFullscreen2D::render(const OvGL::Texture& texture, const Ov::List& list)
{
    // Safety net:
    if (texture == Ov::Texture::empty || list == Ov::List::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Just to update the cache
    this->Ov::Pipeline::render(list);

    // Lazy-loading:
    if (this->isDirty())
        if (!this->init())
        {
            OV_LOG_ERROR("Unable to render (initialization failed)");
            return false;
        }

    // Apply program:
    if (reserved->program == OvGL::Program::empty)
    {
        OV_LOG_ERROR("Invalid program");
        return false;
    }

    reserved->program.render();
    texture.render(0);


    OvGL::Engine& eng = OvGL::Engine::getInstance();
    OvGL::Framebuffer::reset(eng.getWidth(), eng.getHeight());

    // Smart trick:   
    reserved->vao.render();
    glDrawArrays(GL_TRIANGLES, 0, 3);

    // Done:   
    return true;
}

#pragma endregion
