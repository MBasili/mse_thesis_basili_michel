/**
 * @file	overv_gl_vertexarrayobj.cpp
 * @brief	OpenGL vertex array object
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020 - 2021
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_gl.h"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Vertex array object reserved structure.
 */
struct OvGL::VertexArrayObj::Reserved
{
    GLuint oglId;  ///< OpenGL handle


    /**
     * Constructor.
     */
    Reserved() : oglId{ 0 }
    {}
};


//////////////////////////////////
// BODY OF CLASS VertexArrayObj //
//////////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::VertexArrayObj::VertexArrayObj() : reserved(std::make_unique<OvGL::VertexArrayObj::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the vao.
 */
OVGL_API OvGL::VertexArrayObj::VertexArrayObj(const std::string& name) : Ov::Renderable(name), reserved(std::make_unique<OvGL::VertexArrayObj::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::VertexArrayObj::VertexArrayObj(VertexArrayObj&& other) noexcept : Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::VertexArrayObj::~VertexArrayObj()
{
    OV_LOG_DETAIL("[-]");
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes an OpenGL VAO.
 * @return TF
 */
bool OVGL_API OvGL::VertexArrayObj::init()
{
    if (this->Ov::Managed::init() == false)
        return false;

    // Free buffer if already stored:
    if (reserved->oglId)
    {
        glDeleteVertexArrays(1, &reserved->oglId);
        reserved->oglId = 0;
    }

    // Create it:		       
    glGenVertexArrays(1, &reserved->oglId);

    // Done:   
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases an OpenGL VAO.
 * @return TF
 */
bool OVGL_API OvGL::VertexArrayObj::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Free VAO if stored:
    if (reserved->oglId)
    {
        glDeleteVertexArrays(1, &reserved->oglId);
        reserved->oglId = 0;
    }

    // Done:   
    return true;
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create the VAO.
 * @return TF
 */
bool OVGL_API OvGL::VertexArrayObj::create()
{
    return init();
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the GLuint buffer ID.
 * @return buffer ID or 0 if not valid
 */
uint32_t OVGL_API OvGL::VertexArrayObj::getOglHandle() const
{
    return reserved->oglId;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Unbind any vertex array object.
 */
void OVGL_API OvGL::VertexArrayObj::reset()
{
    glBindVertexArray(0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVGL_API OvGL::VertexArrayObj::render(uint32_t value, void* data) const
{
    glBindVertexArray(reserved->oglId);

    // Done:
    return true;
}

#pragma endregion