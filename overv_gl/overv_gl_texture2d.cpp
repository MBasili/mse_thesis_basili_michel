/**
 * @file	overv_gl_texture2d.cpp
 * @brief	OpenGL 2D-specific texture properties
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020-2021
 */


 //////////////
 // #INCLUDE //
 //////////////

 // Main include:
#include "overv_gl.h"

// Magic enum
#include "magic_enum.hpp"

// OGL:      
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


////////////
// STATIC //
////////////

// Special values:
OvGL::Texture2D OvGL::Texture2D::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Texture2D reserved structure.
 */
struct OvGL::Texture2D::Reserved
{

    /**
     * Constructor.
     */
    Reserved()
    {}
};


/////////////////////////////
// BODY OF CLASS Texture2D //
/////////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVGL_API OvGL::Texture2D::Texture2D() : reserved(std::make_unique<OvGL::Texture2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor from bitmap.
 * @param bitmap working bitmap
 */
OVGL_API OvGL::Texture2D::Texture2D(const Ov::Bitmap& bitmap) : reserved(std::make_unique<OvGL::Texture2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
    load(bitmap);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The name of the texture.
 */
OVGL_API OvGL::Texture2D::Texture2D(const std::string& name) : OvGL::Texture(name), reserved(std::make_unique<OvGL::Texture2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVGL_API OvGL::Texture2D::Texture2D(Texture2D&& other) noexcept : OvGL::Texture(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVGL_API OvGL::Texture2D::~Texture2D()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Default

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets default texture2D, which is instantiated on first usage.
 * @return default texture
 */
const OvGL::Texture2D OVGL_API& OvGL::Texture2D::getDefault()
{
    uint8_t data[] = { 255, 255, 255, 255 };
    static Ov::Bitmap dfltBitmap(Ov::Bitmap::Format::r8g8b8a8, 1, 1, data);
    static Texture2D dfltTexture2D(dfltBitmap);
    return dfltTexture2D;
}

#pragma endregion

#pragma region Bitmap

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load the content of the texture from the given bitmap.
 * @param bitmap bitmap
 * @return TF
 */
bool OVGL_API OvGL::Texture2D::load(const Ov::Bitmap& bitmap, bool isLinearEncoded)
{
    // Safety net:
    if (bitmap == Ov::Bitmap::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Bind texture and copy content:   
    GLuint intFormat;
    GLuint extFormat;
    GLuint extType = GL_UNSIGNED_BYTE;
    GLuint nrOfComponents;
    Format _format = Format::none;
    switch (bitmap.getFormat())
    {
        /////////////////////////////////////////////////
    case Ov::Bitmap::Format::r8g8b8a8: //
    case Ov::Bitmap::Format::r8g8b8a8_compressed: //		   
        intFormat = (isLinearEncoded) ? GL_COMPRESSED_RGBA_S3TC_DXT5_EXT : GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT;
        extFormat = GL_RGBA;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 4;
        _format = Format::r8g8b8a8_compressed;
        break;

        ///////////////////////////////////////////////
    case Ov::Bitmap::Format::r8g8b8: //  
    case Ov::Bitmap::Format::r8g8b8_compressed: //		   
        intFormat = (isLinearEncoded) ? GL_COMPRESSED_RGB_S3TC_DXT1_EXT : GL_COMPRESSED_SRGB_S3TC_DXT1_EXT;
        extFormat = GL_RGB;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 3;
        _format = Format::r8g8b8_compressed;
        break;

        /////////////////////////////////////////////
    case Ov::Bitmap::Format::r8g8: //
    case Ov::Bitmap::Format::r8g8_compressed: //		   
        intFormat = GL_COMPRESSED_RG_RGTC2;
        extFormat = GL_RG;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 2;
        _format = Format::r8g8_compressed;
        break;

        ///////////////////////////////////////////
    case Ov::Bitmap::Format::r8: // 
    case Ov::Bitmap::Format::r8_compressed: //		   
        intFormat = GL_COMPRESSED_RED_RGTC1;
        extFormat = GL_RED;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 1;
        _format = Format::r8_compressed;
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Unexpected bitmap type (%s)", OV_LOG_ENUM(bitmap.getFormat()));
        return false;
    }

    // Init texture:
    this->OvGL::Texture::init();

    // Create it:		              
    const GLuint oglId = this->getOglHandle();
    glBindTexture(GL_TEXTURE_2D, oglId);
    if (bitmap.getNrOfLevels() > 1)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, bitmap.getNrOfLevels());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 16); // FIX THIS @TODO

    // Load data:   
    for (uint32_t c = 0; c < bitmap.getNrOfLevels(); c++)
    {
        OV_LOG_DEBUG("Type: 2D, Level: %d/%d, IntFormat: 0x%x, x: %u, y: %u", c + 1, bitmap.getNrOfLevels(), intFormat, bitmap.getSizeX(c), bitmap.getSizeY(c));
        switch (bitmap.getFormat())
        {
            // Compressed:
        case Ov::Bitmap::Format::r8g8b8a8_compressed:
        case Ov::Bitmap::Format::r8g8b8_compressed:
        case Ov::Bitmap::Format::r8g8_compressed:
        case Ov::Bitmap::Format::r8_compressed:
            glCompressedTexImage2D(GL_TEXTURE_2D, c, intFormat, bitmap.getSizeX(c), bitmap.getSizeY(c), 0, bitmap.getNrOfBytes(c), bitmap.getData(c));
            break;

            // Uncompressed:
        default:
            glTexImage2D(GL_TEXTURE_2D, c, intFormat, bitmap.getSizeX(c), bitmap.getSizeY(c), 0, extFormat, extType, bitmap.getData(c));
            break;
        }
    }

    if (bitmap.getNrOfLevels() <= 1)
        glGenerateMipmap(GL_TEXTURE_2D);

    // Resident:
    this->OvGL::Texture::makeResident();

    // Done:   
    this->setBitmap(bitmap);
    this->setFormat(_format);
    this->setInternalFormat(intFormat);
    this->setSizeX(bitmap.getSizeX(0));
    this->setSizeY(bitmap.getSizeY(0));
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
/**
 * Allocate memory and initialize an empty texture.
 * @param sizeX texture width
 * @param sizeY texture height
 * @param format pixel layout
 * @return TF
 */
bool OVGL_API OvGL::Texture2D::create(uint32_t sizeX, uint32_t sizeY, Format format)
{
    // Safety net:
    if (sizeX == 0 || sizeY == 0 || format == Format::none)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Init texture:
    this->OvGL::Texture::init();

    // Bind texture and copy content:   
    GLuint intFormat;
    GLuint extFormat;
    GLuint extType;
    GLuint nrOfComponents;
    switch (format)
    {
        ///////////////////////
    case Format::r8g8b8: //    
        intFormat = GL_RGB8;
        extFormat = GL_RGB;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 3;
        break;

        /////////////////////////
    case Format::r8g8b8a8: //
        intFormat = GL_RGBA8;
        extFormat = GL_RGBA;
        extType = GL_UNSIGNED_BYTE;
        nrOfComponents = 4;
        break;

    case Format::r32ui:
        intFormat = GL_R32UI;
        extFormat = GL_RED_INTEGER;
        extType = GL_UNSIGNED_INT;
        nrOfComponents = 1;
        break;

        //////////////////////
    case Format::depth: //
        intFormat = GL_DEPTH_COMPONENT32;
        extFormat = GL_DEPTH_COMPONENT;
        extType = GL_FLOAT;
        nrOfComponents = 1;
        break;

        ///////////
    default: //
        OV_LOG_ERROR("Unexpected format type");
        return false;
    }

    // Create it:		    
    const GLuint oglId = this->getOglHandle();
    glBindTexture(GL_TEXTURE_2D, oglId);
    glTexImage2D(GL_TEXTURE_2D, 0, intFormat, sizeX, sizeY, 0, extFormat, extType, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    if (format == Format::depth)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    }

    // Resident:
    this->OvGL::Texture::makeResident();

    // Done:
    this->setFormat(format);
    this->setInternalFormat(intFormat);
    this->setSizeX(sizeX);
    this->setSizeY(sizeY);
    return true;
}

#pragma endregion