/**
 * @file	overv_gl_elemarraybuffer.h
 * @brief	OpenGL element array buffer
 *
 * @author	Achille Peternier (achille.peternier@supsi.ch), (C) 2020
 */
#pragma once


 /**
  * @brief OpenGL element array buffer
  */
class OVGL_API ElemArrayBuffer final : public OvGL::Buffer
{
//////////
public: //
//////////

	// Const/dest:
	ElemArrayBuffer();
	ElemArrayBuffer(ElemArrayBuffer&& other) noexcept;
	ElemArrayBuffer(ElemArrayBuffer const&) = delete;
	~ElemArrayBuffer();

	// Managed:
	bool init() override;
	bool free() override;

	// Get/set:   
	uint32_t getNrOfFaces() const;

	// Management:
	bool create(uint64_t nrOfFaces, const void* data = nullptr);

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	ElemArrayBuffer(const std::string& name);
};