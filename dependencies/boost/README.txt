Boost (lib and headers) v1.73
13/05/20 FA

Under Linux use:

./b2 cxxflags=-fPIC cflags=-fPIC -a threading=multi link=static variant=release install

Under Windows use:

for 64 bit:   
   b2 toolset=msvc-14.2 --build-type=complete link=static threading=multi architecture=x86 runtime-link=shared address-model=64 stage -j12

for 32 bit:
   b2 toolset=msvc-14.2 --build-type=complete link=static threading=multi architecture=x86 runtime-link=shared address-model=32 stage -j12




