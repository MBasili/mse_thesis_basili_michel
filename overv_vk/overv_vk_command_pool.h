#pragma once
/**
 * @file	overv_vk_command_pool.h
 * @brief	Vulkan command pool properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Class to represent Vulkan command pool
  */
class OVVK_API CommandPool final : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Static
	static CommandPool empty;

	/**
     * @brief Enum listing the types of commands supported by a command pool.
     */
	enum class CommandTypesFlagBits : uint32_t
	{
		none = 0,

		Transfer = 1,
		Compute = 2,
		Graphic = 4,
		Present = 8,

		last
	};

	// Const/dest:
	CommandPool();
	CommandPool(CommandPool&& other) noexcept;
	CommandPool(CommandPool const&) = delete;
	virtual ~CommandPool();

	// Operators
	CommandPool& operator=(const CommandPool&) = delete;
	CommandPool& operator=(CommandPool&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// Sync Obj.
	bool isLock() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool create(CommandTypesFlagBits typeCP, VkCommandPoolCreateFlags flags = 0x00000000);
	bool reset(VkCommandPoolResetFlags flags = 0x00000000);

	OvVK::CommandBuffer& allocateCommandBuffer(VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	bool freeCommandBuffer(const OvVK::CommandBuffer& commandBuffer);

	// Get/set:   
	VkCommandPool getVkCommandPool() const;
	CommandTypesFlagBits getCommandTypesSupported() const;
	VkCommandPoolCreateFlags getCommandPoolCreateFlags() const;

	std::optional<uint32_t> getQueueFamilyIndex() const;
#endif

	std::vector<std::reference_wrapper<OvVK::CommandBuffer>> getCommandBuffers() const;
	OvVK::CommandBuffer& getCommandBuffer(uint32_t index) const;
	uint32_t getNrCommandBuffers() const;

///////////
private: //
///////////

	//Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	CommandPool(const std::string& name);
};

// overloaded bitwise operators
inline CommandPool::CommandTypesFlagBits operator|(
	CommandPool::CommandTypesFlagBits lhs, CommandPool::CommandTypesFlagBits rhs) {
	return static_cast<CommandPool::CommandTypesFlagBits>(
		static_cast<std::underlying_type_t<CommandPool::CommandTypesFlagBits>>(lhs) |
		static_cast<std::underlying_type_t<CommandPool::CommandTypesFlagBits>>(rhs)
		);
}
inline CommandPool::CommandTypesFlagBits operator&(
	CommandPool::CommandTypesFlagBits lhs, CommandPool::CommandTypesFlagBits rhs) {
	return static_cast<CommandPool::CommandTypesFlagBits>(
		static_cast<std::underlying_type_t<CommandPool::CommandTypesFlagBits>>(lhs) &
		static_cast<std::underlying_type_t<CommandPool::CommandTypesFlagBits>>(rhs)
		);
}
