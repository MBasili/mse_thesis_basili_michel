/**
 * @file	overv_vk_shader_group.h
 * @brief	Vulkan generic shader group properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling an Vulkan shader.
  */
class OVVK_API RTShaderGroup : public Ov::Program 
{
//////////
public: //
//////////

	// Static special value
	static RTShaderGroup empty;

	/**
	 * @brief Types of shader.
	 */
	enum class Type : uint32_t
	{
		none,

		// RT ShadersGroup
		ray_generation,
		hit,
		intersection,
		miss,
		callable,

		// Terminator:
		last
	};

	// Const/dest:
	RTShaderGroup();
	RTShaderGroup(RTShaderGroup&& other);
	RTShaderGroup(RTShaderGroup const&) = delete;
	virtual ~RTShaderGroup();

	// Operators
	RTShaderGroup& operator=(const RTShaderGroup&) = delete;
	RTShaderGroup& operator=(RTShaderGroup&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// General:
	virtual bool build(std::initializer_list<std::reference_wrapper<Ov::Shader>> shaders) override;
	
	// Get/Set
	Type getShaderGroupType() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkRayTracingShaderGroupTypeKHR getVKRTShaderGroupType() const;
#endif

	const OvVK::Shader& getShader(OvVK::Shader::Type type) const;

	std::optional<uint64_t> getHandleDeviceAddress() const;
	std::optional<uint32_t> getPositionInPipeline() const;

	// Rendering methods:
	virtual bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////   

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	RTShaderGroup(const std::string& name);

	// Get/Set
	void setHandleDeviceAddress(uint64_t address);
	void setPositionInPipeline(uint32_t pos);

	friend class RayTracingPipeline;
};