/**
 * @file	overv_vk_pipeline_lighting.h
 * @brief	Lighting pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modelling Lighting Pipeline
  */
class OVVK_API WhittedRTwithPBRPipeline final : public OvVK::RayTracingPipeline, public Ov::DrawGUI
{
//////////
public: //
//////////

	constexpr static uint64_t defaultLightingValueStatusSyncObj = 0;
	constexpr static uint64_t LightingValueStatusSyncObj = 1;
	constexpr static uint64_t doneLightingValueStatusSyncObj = 2;
	constexpr static uint64_t startDependenciesProcessValueStatusSyncObj = 3;
	constexpr static uint64_t finishDependenciesProcessValueStatusSyncObj = 4;

	/**
     * @brief Definition of bind sets.
     */
	enum class BindSet : uint32_t
	{
		storage = 0,
		texture = 1,
		lighting = 2,
		rt = 3
	};

	/**
	 * @brief PushConstant hold parameters needed inside the shaders (keep it small size < 128 bytes).
	 */
	struct PushConstant
	{
		// Bool are convert into uint32_t for alignament
		// For bool it is possible to use a bit flag but then it is needed to
		// check the little or big endian encoding. Since the push constant can
		// be up to a size of 128 bytes, uint32_t are used to represent the boolean.
		// 
		// This struct size need to be a multiple of 4
		uint32_t rayRecursionDepth;
		uint32_t nrOfAASamples;
		uint32_t nrOfAOSamples;
		uint32_t nrOfFramesAO; 
		uint32_t nrOfReflSamples;
		uint32_t isAmbientOcclusionEnabled;
		uint32_t useFirstVariationOfAO;
		uint32_t showAmbientOcclusionImage; // 32bytes
		uint32_t useNormalMapForReflection;
		uint32_t useNormalMapForRefraction;
		uint32_t useMultipleReflectionRays;
		uint32_t useRoghnessTexture;
		uint32_t useMetalnessTexture;
		uint32_t useAlbedoTexture;
		uint32_t isHDRCorrectionEnabled;
		uint32_t useNDFStandard; //64 bytes
		uint32_t scaleReflectionInvRoughness;
		uint32_t showProblemNDFFunction;
		uint32_t alwaysCalculateRefractedRays;

		PushConstant() : rayRecursionDepth{ 0 },
			nrOfAASamples{ 1 },
			nrOfAOSamples{ 4 },
			nrOfFramesAO{ 0 },
			nrOfReflSamples{ 1 },
			isAmbientOcclusionEnabled{ 0 },
			useFirstVariationOfAO{ 0 },
			showAmbientOcclusionImage{ 0 },
			useNormalMapForReflection{ 1 },
			useNormalMapForRefraction{ 1 },
			useMultipleReflectionRays{ 0 },
			useRoghnessTexture{ 1 },
			useMetalnessTexture{ 1 },
			useAlbedoTexture{ 1 },
			isHDRCorrectionEnabled{ 0 },
			useNDFStandard{ 0 },
			scaleReflectionInvRoughness{ 0 },
			showProblemNDFFunction{ 0 },
			alwaysCalculateRefractedRays{ 0 }
		{}

		static std::string getShaderStruct()
		{
				return R"(
struct PushConstant
{
	uint rayRecursionDepth;
	uint nrOfAASamples;
	uint nrOfAOSamples;
	uint nrOfFramesAO;
	uint nrOfReflSamples;
	bool isAmbientOcclusionEnabled;
	bool useFirstVariationOfAO;
	bool showAmbientOcclusionImage;
	bool useNormalMapForReflection;
	bool useNormalMapForRefraction;
	bool useMultipleReflectionRays;
	bool useRoghnessTexture;
	bool useMetalnessTexture;
	bool useAlbedoTexture;
	bool isHDRCorrectionEnabled;
	bool useNDFStandard;
	bool scaleReflectionInvRoughness;
	bool showProblemNDFFunction;
	bool alwaysCalculateRefractedRays;
};
)";
		}
	};

	/**
     * @brief Struct used as a payload to calculate the value of a pixel.
     */
	struct HitPayload
	{
		glm::vec3 hitValue;
		uint32_t depth;

		HitPayload() : hitValue{ 0.0f },
			depth{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct HitPayload
{
    vec3 hitValue;
    uint depth;
};)";
		}
	};

	/**
	 * @brief Struct used as a payload to calculate the shadow.
	 */
	struct OcclusionPayload
	{
		glm::vec3 value;
		glm::vec3 startColor;
		int geomID;
		int instanceID;
		int primitiveID;

		OcclusionPayload() : value{ 0.0f }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct OcclusionPayload
{
	vec3 value;
    vec3 startColor;
    int geomID;
    int instanceID;
	int primitiveID;
	float pDotLFirst;
};)";
		}
	};

	/**
     * @brief Struct used as a payload to calculate ambient occlusions.
     */
	struct AOPayload
	{
		float hitSky;

		AOPayload() : hitSky{ 0.0f }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct AOPayload
{
		float hitSky;
};)";
		}
	};

	/**
     * @brief Struct used as an attribute to get the bary coordinates of the hit.
     */
	struct HitAttribute
	{
		glm::vec2 baryCoord;

		HitAttribute() : baryCoord{ 0.0f }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct HitAttribute
{
	vec2 baryCoord;
};)";
		}
	};
	
	// Const/dest:
	WhittedRTwithPBRPipeline();
	WhittedRTwithPBRPipeline(WhittedRTwithPBRPipeline&& other);
	WhittedRTwithPBRPipeline(WhittedRTwithPBRPipeline const&) = delete;
	virtual ~WhittedRTwithPBRPipeline();

	// Operators
	WhittedRTwithPBRPipeline& operator=(const WhittedRTwithPBRPipeline&) = delete;
	WhittedRTwithPBRPipeline& operator=(WhittedRTwithPBRPipeline&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// General
	virtual bool create() override;

	// SurfaceResizableObject
	virtual bool windowSizeCallback(int width, int height) override;

	// Get:
	uint32_t getRayRecursionDepth() const;
	bool setRayRecursionDepth(uint32_t rayRecursionDepth);
	uint32_t getNrOfAASamples() const;
	bool setNrOfAASamples(uint32_t nrOfAASamples);

	bool isAmbientOcclusionEnable() const;
	bool enableDisableAmbientOcclusion(bool value);
	bool resetAmbientOcclusionNrOfFrames();

	// DrawGUI
	virtual void drawGUI() override;

	// Rendering methods:
	bool render(const OvVK::RTScene& rtScene);

	// Preprocessor
	static bool addShaderPreproc();

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// General
	bool createTargetRenderImages();
	bool createAOImages();

	// Const/dest:
	WhittedRTwithPBRPipeline(const std::string& name);
};