/**
 * @file	overv_vk_buffer.cpp
 * @brief	Vulkan buffer
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::Buffer OvVK::Buffer::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief OvVK buffer reserved structure.
 */
struct OvVK::Buffer::Reserved
{
    VkBuffer buffer;                                ///< Vulkan buffer handle
    VmaAllocation allocation;                       ///< Vma allocation handle
    VmaAllocationInfo allocationInfo;               ///< Vma allocation info

    VkFence lockSyncObj;                            ///< Vulkan sync object
    VkSemaphore statusSyncObj;                      ///< Vulkan sync object

    std::optional<uint32_t> queueFamilyIndex;       ///< Queue family index owner of the buffer

    /**
     * Constructor.
     */
    Reserved() : buffer{ VK_NULL_HANDLE },
        allocation{ nullptr },
        allocationInfo{ {} },
        lockSyncObj{ VK_NULL_HANDLE },
        statusSyncObj{ VK_NULL_HANDLE },
        queueFamilyIndex{ std::nullopt }
    {}
};


//////////////////////////
// BODY OF CLASS Buffer //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Buffer::Buffer() : reserved(std::make_unique<OvVK::Buffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The buffer name.
 */
OVVK_API OvVK::Buffer::Buffer(const std::string &name) : Ov::Buffer(name), 
    reserved(std::make_unique<OvVK::Buffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Buffer::Buffer(Buffer&& other) noexcept : Ov::Buffer(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Buffer::~Buffer()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes buffer.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::init()
{
    // Call base method
    if (this->Ov::Buffer::init() == false)
        return false;


    // Destroy buffer if present
    if (reserved->buffer != VK_NULL_HANDLE)
    {
        // Retrive the Allocator
        VmaAllocator allocator = Engine::getInstance().getLogicalDevice().getVmaAllocator();

        vmaDestroyBuffer(allocator, reserved->buffer, reserved->allocation);
        reserved->buffer = VK_NULL_HANDLE;
        reserved->allocation = nullptr;
        reserved->allocationInfo = {};

        this->Ov::Buffer::setSize(0);

        reserved->queueFamilyIndex = std::nullopt;
    }

    // Destroy sync obj for status
    if (reserved->statusSyncObj != VK_NULL_HANDLE)
    {
        // Logicel device
        VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, NULL);
        reserved->statusSyncObj = VK_NULL_HANDLE;
    }
    
    // Create sync obj for status.
    VkSemaphoreTypeCreateInfo syncObjTypeCreateInfo;
    syncObjTypeCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    syncObjTypeCreateInfo.pNext = NULL;
    syncObjTypeCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    syncObjTypeCreateInfo.initialValue = OvVK::Buffer::defaultValueStatusSyncObj;

    VkSemaphoreCreateInfo syncObjCreateInfo;
    syncObjCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    syncObjCreateInfo.pNext = &syncObjTypeCreateInfo;
    syncObjCreateInfo.flags = 0;

    if (vkCreateSemaphore(Engine::getInstance().getLogicalDevice().getVkDevice(),
        &syncObjCreateInfo, VK_NULL_HANDLE, &reserved->statusSyncObj) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create status synchronization object for buffer with id= %d", getId());
        this->free();
        return false;
    }

    // Reset sync obj for lock (Not the owner Of the object)
    reserved->lockSyncObj = VK_NULL_HANDLE;


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the OvVK buffer.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::free()
{
    // Call base method
    if (this->Ov::Managed::free() == false)
        return false;


    // Destroy buffer if present
    if (reserved->buffer != VK_NULL_HANDLE)
    {
        // Retrive the Allocator
        VmaAllocator allocator = Engine::getInstance().getLogicalDevice().getVmaAllocator();

        vmaDestroyBuffer(allocator, reserved->buffer, reserved->allocation);
        reserved->buffer = VK_NULL_HANDLE;
        reserved->allocation = nullptr;
        reserved->allocationInfo = {};

        this->Ov::Buffer::setSize(0);

        reserved->queueFamilyIndex = std::nullopt;
    }

    // Destroy sync obj for status
    if (reserved->statusSyncObj != VK_NULL_HANDLE)
    {
        // Logicel device
        VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, NULL);
        reserved->statusSyncObj = VK_NULL_HANDLE;
    }

    // Reset sync obj for lock (Not the owner Of the object)
    reserved->lockSyncObj = VK_NULL_HANDLE;


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncObj

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the buffer is locked.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::isLock() const
{
    // Safety net:
    if (reserved->lockSyncObj == VK_NULL_HANDLE)
        return false;

    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // vkGetFenceStatus == VK_SUCCESS => The fence specified by fence is signaled.
    if (vkGetFenceStatus(logicalDevice, reserved->lockSyncObj) != VK_SUCCESS)
        return true;

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the buffer is owned from a queue family.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::isOwned() const
{
    return reserved->queueFamilyIndex.has_value();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create a buffer by allocating the required storage throught VMA.
 * @param vkBufferCreateInfo Structure specifying the parameters to create a new buffer in Vulkan.
 * @param vmaAllocationCreateInfo Structure specifying the parameters to create a new allocation in VMA.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::create(VkBufferCreateInfo vkBufferCreateInfo,
    VmaAllocationCreateInfo vmaAllocationCreateInfo)
{
    // Safety net:
    if (vkBufferCreateInfo.size == 0)
    {
        OV_LOG_ERROR("Size is 0 in the vkBufferCreateInfo structure in the buffer with id= %d.", getId());
        return false;
    }
    if (reserved->buffer != VK_NULL_HANDLE && isLock()) {
        OV_LOG_ERROR("Can't recreate the buffer with id= %d because is locked.", getId());
        return false;
    }

    if (!this->init())
        return false;

    // Set buffer size
    this->Ov::Buffer::setSize((uint64_t)vkBufferCreateInfo.size);

    // Al the value are present in the engine class. If not presente the engine doesn't start.
    std::reference_wrapper<const Engine> engine = Engine::getInstance();
    // Allocator
    VmaAllocator allocator = engine.get().getLogicalDevice().getVmaAllocator();
    // Logical Device
    VkDevice logicalDevice = engine.get().getLogicalDevice().getVkDevice();

    // Create buffer
    if (vmaCreateBuffer(allocator, &vkBufferCreateInfo, &vmaAllocationCreateInfo, &reserved->buffer,
        &reserved->allocation, &reserved->allocationInfo) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create buffer with id= %d", getId());
        this->free();
        return false;
    }

    // Done
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns Vulkan buffer handle.
 * @return Vulkan buffer handle.
 */
VkBuffer OVVK_API OvVK::Buffer::getVkBuffer() const
{
    return reserved->buffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns Vma allocation handle.
 * @return Vma allocation handle.
 */
VmaAllocation OVVK_API OvVK::Buffer::getVmaAllocation() const
{
    return reserved->allocation;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a VmaAllocationInfo containing the info about the vma allocation.
 * @return Vma allocation info structure.
 */
VmaAllocationInfo OVVK_API OvVK::Buffer::getVmaAllocationInfo() const
{
    return reserved->allocationInfo;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the sync obj for lock.
 * @return Fence Vulkan object action as lock.
 */
VkFence OVVK_API OvVK::Buffer::getLockSyncObj() const
{
    return reserved->lockSyncObj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the sync obj for status.
 * @return Semaphore Vulkan object action as status keeper.
 */
VkSemaphore OVVK_API OvVK::Buffer::getStatusSyncObj() const
{
    return reserved->statusSyncObj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the queue family index of the queue owninig the buffer if one.
 * @return Optional std object containing null or the index of the family to which the buffer belong.
 */
std::optional<uint32_t> OVVK_API OvVK::Buffer::getQueueFamilyIndex() const
{
    return reserved->queueFamilyIndex;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a 64-bit buffer device address value through which buffer memory can be accessed in a shader or be mapped.
 * @return Optional std object containing null or the device address object of the buffer.
 */
std::optional<VkDeviceAddress> OVVK_API OvVK::Buffer::getBufferDeviceAddress() const
{
    // Declare obj. to return
    std::optional<VkDeviceAddress> deviceAddress;

    // Safety net:
    if (reserved->buffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Can't retriver the address because buffer not created.");
    }
    else 
    {
        VkBufferDeviceAddressInfo vkBufferDeviceAddressInfo{};
        vkBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        vkBufferDeviceAddressInfo.pNext = NULL;
        vkBufferDeviceAddressInfo.buffer = reserved->buffer;

        deviceAddress = vkGetBufferDeviceAddress(Engine::getInstance().getLogicalDevice().getVkDevice(), &vkBufferDeviceAddressInfo);
        
    }

    // Done:
    return deviceAddress;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the queue family index of the queue owning the buffer.
 * @param queueFamilyIndex Index of the queue owning the buffer.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::setQueueFamilyIndex(uint32_t queueFamilyIndex)
{
    // Safety net:
    if (reserved->buffer == VK_NULL_HANDLE) {
        OV_LOG_ERROR("Buffer not created!");
        return false;
    }
    if (isLock()) {
        OV_LOG_ERROR("Can't set queue family because the buffer with id= %d is locked.", getId());
        return false;
    }
    
    reserved->queueFamilyIndex = queueFamilyIndex;

    //Done
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the buffer.
 * @param value The uint value rapresenting the status the buffer is in.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::setStatusSyncObjValue(uint64_t value)
{
    // Safety net:
    if (reserved->statusSyncObj == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Sync obj not created!");
        return false;
    }

    // Logicel device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Retrievers current sempahore value
    uint64_t currentValue;
    vkGetSemaphoreCounterValue(logicalDevice, reserved->statusSyncObj, &currentValue);

    if (currentValue > value) {
        // Destroy semaphore because can't decrease the value
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, nullptr);
        reserved->statusSyncObj = VK_NULL_HANDLE;

        // Create new timeline semaphore and delete previous one.
        VkSemaphoreTypeCreateInfo timelineCreateInfo;
        timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        timelineCreateInfo.pNext = NULL;
        timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        timelineCreateInfo.initialValue = value;

        VkSemaphoreCreateInfo semaphoreInfo;
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreInfo.pNext = &timelineCreateInfo;
        semaphoreInfo.flags = 0;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &reserved->statusSyncObj) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status synchronization object for Buffer with id= %d", getId());
            return false;
        }
    }
    else if (currentValue < value) {
        VkSemaphoreSignalInfo signalInfo;
        signalInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
        signalInfo.pNext = NULL;
        signalInfo.semaphore = reserved->statusSyncObj;
        signalInfo.value = value;

        if (vkSignalSemaphore(logicalDevice, &signalInfo) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status synchronization object for Buffer with id= %d", getId());
            return false;
        }
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sync obj for the lock.
 * @param fence Vulkan object to check to know whether the buffer is locked or not.
 * @return TF
 */
bool OVVK_API OvVK::Buffer::setLockSyncObj(VkFence fence) 
{
    // Safety net:
    if (reserved->buffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Buffer not created!");
        return false;
    }

    reserved->lockSyncObj = fence;

    //Done:
    return true;
}

#pragma endregion