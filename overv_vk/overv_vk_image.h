/**
 * @file	overv_vk_image.h
 * @brief	Vulkan image
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


/**
 * @brief Class for modeling a Vulkan image
 */
class OVVK_API Image : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Static:
	static Image empty;

	// Values for the status syncObj
	constexpr static uint64_t defaultValueStatusSyncObj = 0;

	// Const/dest:
	Image();
	Image(Image&& other) noexcept;
	Image(Image const&) = delete;
	virtual ~Image();

	// Operators
	Image& operator=(const Image&) = delete;
	Image& operator=(Image&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// SyncObj:
	bool isLock() const;
	bool isOwned() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool create(VkImageCreateInfo vkImageCreateInfo,
		VmaAllocationCreateInfo vmaImageAllocationCreateInfo);
	const OvVK::ImageView& createImageView(VkImageViewCreateInfo vkImageViewCreateInfo);
#endif

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Get/set:   
	VkImage getVkImage() const;
	VmaAllocation getVmaAllocation() const;
	VmaAllocationInfo getVmaAllocationInfo() const;
	VkFormat getFormat() const;
	VkExtent3D getExtend3D() const;
	VkImageLayout getImageLayout() const;
	std::optional<uint32_t> getQueueFamilyIndex() const;
	VkFence getLockSyncObj() const;
	VkSemaphore getStatusSyncObj() const;
	uint32_t getNrOfVkImageView() const;
	const OvVK::ImageView& getVkImageView(uint32_t pos) const;
	const std::vector<OvVK::ImageView>& getVkImageViews() const;

	bool setImageLayout(VkImageLayout imageLayout);
	bool setQueueFamilyIndex(uint32_t familyIndex);
	bool setStatusSyncObjValue(uint64_t value);
	bool setLockSyncObj(VkFence fence);
#endif

	uint32_t getNrOfLevels() const;
	uint32_t getNrOfLayers() const;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Image(const std::string& name);
};