/**
 * @file	overv_vk_instance.h
 * @brief	Vulkan instance properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for manage Vulkan instance.
  */
class OVVK_API VulkanInstance final : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Special values;
	static VulkanInstance empty;

	// Const/dest:
	VulkanInstance();
	VulkanInstance(VulkanInstance&& other);
	VulkanInstance(VulkanInstance const&) = delete;
	virtual ~VulkanInstance();

	// Operators
	VulkanInstance& operator=(const VulkanInstance&) = delete;
	VulkanInstance& operator=(VulkanInstance&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool create(VkApplicationInfo appInfo, std::vector<const char*> requiredExtensions = {},
		bool enableDebugMessanger = false, bool enableValidationLayers = false,
		std::vector<const char*> requiredValidationLayers = {});
#endif

	// Get/Set:
	std::vector<const char*> getEnabledExtensions() const;
	std::vector<const char*> getEnabledValidationLayers() const;
	std::vector<std::reference_wrapper<const OvVK::PhysicalDevice>> getPhysicalDevices() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkInstance getVkInstance() const;
	VkApplicationInfo getVkApplicationInfo() const;

	VkDebugUtilsMessengerEXT getDebugMessanger() const;
#endif

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Layers:
	static std::vector<VkLayerProperties> getAvailableLayers();
	static bool areLayersSupported(std::vector<const char*> const& validationLayers);

	// Extensions:
	static std::vector<VkExtensionProperties> getSupportedExtesions();
	static bool areExtensionsSupported(std::vector<const char*> const& requiredInstanceExtensions);

	// Debug messanger:
	static void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData);
#endif

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	VulkanInstance(const std::string& name);
};