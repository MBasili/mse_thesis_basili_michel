#pragma once
/**
 * @file    overv_vk_ac.h
 * @brief	Acceleration structure
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Class rapresenting Acceleration structure
  */
class OVVK_API AccelerationStructure : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

	// Special values:
	static AccelerationStructure empty;

	constexpr static uint64_t readyToBuildValueStatusSyncObj = 1;
	constexpr static uint64_t buildingValueStatusSyncObj = 2;
	constexpr static uint64_t updatingValueStatusSyncObj = 3;
	constexpr static uint64_t compactingValueStatusSyncObj = 4;
	constexpr static uint64_t readyValueStatusSyncObj = 5;

	// Const/dest:
	AccelerationStructure();
	AccelerationStructure(AccelerationStructure&& other) noexcept;
	AccelerationStructure(AccelerationStructure const&) = delete;
	~AccelerationStructure();

	// Operators
	AccelerationStructure& operator=(const AccelerationStructure&) = delete;
	AccelerationStructure& operator=(AccelerationStructure&&) = delete;

	// Managed
	virtual bool init() override;
	virtual bool free() override;

	// Sync Obj
	bool isLock() const;
	bool isOwned() const;

	// Rendering
	virtual bool render(uint32_t value = 0, void* data = nullptr) const override;

	// Get/set:
	const OvVK::Buffer& getScratchBuffer() const;
	const OvVK::Buffer& getASBuffer() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkAccelerationStructureKHR getVkAS() const;
	std::optional<VkDeviceAddress> getVkASDeviceAddress() const;
	const VkAccelerationStructureBuildGeometryInfoKHR& getAccelerationStructureBuildGeometryInfo() const;
	VkBuildAccelerationStructureFlagsKHR getVkBuildAccelerationStructureFlagsKHR() const;

	bool setQueueFamilyIndex(uint32_t queueFamilyIndex);
	bool setStatusSyncObjValue(uint64_t value);
	bool setLockSyncObj(VkFence fence);
#endif

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool setUp(VkAccelerationStructureTypeKHR type,
		std::vector<VkAccelerationStructureGeometryKHR>& accelerationStructureGeometriesKHR,
		std::vector<VkAccelerationStructureBuildRangeInfoKHR>& accelerationStructureBuildRangeInfosKHR,
		VkBuildAccelerationStructureModeKHR mode,
		VkBuildAccelerationStructureFlagsKHR flags);
#endif

	bool freeScratchBuffer();

	// Const/dest:
	AccelerationStructure(const std::string& name);

///////////
private: //
///////////

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool createAS(VkDeviceSize asBufferSize, VkAccelerationStructureTypeKHR type);
#endif

};
