/**
 * @file	overv_vk_command_buffer.h
 * @brief	Vulkan command buffer properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::CommandBuffer OvVK::CommandBuffer::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief OvVK CommandBuffer reserved structure.
 */
struct OvVK::CommandBuffer::Reserved
{
    std::reference_wrapper<OvVK::CommandPool> commandPool;              ///< OvVK CommandPool where the commandBuffer is allocate. 

    VkCommandBuffer commandBuffer;                                          ///< Vulkan commandBuffer handle.
    VkCommandBufferLevel level;                                             ///< Vulkan commandBuffer level.
    VkCommandBufferUsageFlags commandBufferUsageFlags;                      ///< Vulkan commandBuffer usage flags.

    VkSemaphore statusSyncObj;                                              ///< Vulkan sync Obj.
    VkFence lockSyncObj;                                                    ///< Vulkan sync Obj.

    /**
     * Constructor.
     */
    Reserved() : commandPool{ OvVK::CommandPool::empty },
        commandBuffer{ VK_NULL_HANDLE },
        level{ VK_COMMAND_BUFFER_LEVEL_PRIMARY },
        commandBufferUsageFlags{ 0x00000000 },
        statusSyncObj{ VK_NULL_HANDLE },
        lockSyncObj{ VK_NULL_HANDLE }
    {}
};


/////////////////////////////////
// BODY OF CLASS CommandBuffer //
/////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::CommandBuffer::CommandBuffer() : reserved(std::make_unique<OvVK::CommandBuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The command buffer name.
 */
OVVK_API OvVK::CommandBuffer::CommandBuffer(const std::string& name) : Ov::Object(name),
reserved(std::make_unique<OvVK::CommandBuffer::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::CommandBuffer::CommandBuffer(CommandBuffer&& other) noexcept : Ov::Object(std::move(other)),
     reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::CommandBuffer::~CommandBuffer()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region SyncObj

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the command buffer is locked.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::isLock() const
{
    // Safety net:
    if (reserved->lockSyncObj == VK_NULL_HANDLE)
        return false;

    // vkGetFenceStatus == VK_SUCCESS => The fence specified by fence is signaled.
    if (vkGetFenceStatus(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->lockSyncObj) != VK_SUCCESS)
        return true;

    // Done:
    return false;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Allocates a command buffer from the passed command pool.
 * @param commandPool The command pool use to allocate the command buffer.
 * @param level The level of the command buffer
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::allocate(OvVK::CommandPool& commandPool,
    VkCommandBufferLevel level)
{
    // Safety net:
    if (reserved->commandBuffer != VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The VKCommandBuffer is already allocated in the command buffer with id= %d.", getId());
        return false;
    }

    if (commandPool == OvVK::CommandPool::empty)
    {
        OV_LOG_ERROR("Empty command pool is not valid in the command buffer with id= %d.", getId());
        return false;
    }

    if (commandPool.getVkCommandPool() == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The command pool (id=%d) passed as a null VkCommandPool in the command buffer with id= %d.",
            commandPool.getId(), getId());
        return false;
    }

    // Init
    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Free command buffer
    if (reserved->commandBuffer != VK_NULL_HANDLE)
    {
        vkFreeCommandBuffers(logicalDevice, reserved->commandPool.get().getVkCommandPool(),
            1, &reserved->commandBuffer);

        reserved->commandBuffer = VK_NULL_HANDLE;
        reserved->level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        reserved->commandBufferUsageFlags = 0x00000000;
        reserved->commandPool = OvVK::CommandPool::empty;
    }

    // Destroy status sync obj.
    if (reserved->statusSyncObj != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, NULL);
        reserved->statusSyncObj = VK_NULL_HANDLE;
    }

    // Create status sync obj.
    VkSemaphoreTypeCreateInfo syncStatusObjTypeCreateInfo;
    syncStatusObjTypeCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    syncStatusObjTypeCreateInfo.pNext = NULL;
    syncStatusObjTypeCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    syncStatusObjTypeCreateInfo.initialValue = OvVK::CommandBuffer::defaultValueStatusSyncObj;

    VkSemaphoreCreateInfo syncStatusObjCreateInfo;
    syncStatusObjCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    syncStatusObjCreateInfo.pNext = &syncStatusObjTypeCreateInfo;
    syncStatusObjCreateInfo.flags = 0;

    if (vkCreateSemaphore(logicalDevice, &syncStatusObjCreateInfo, NULL, &reserved->statusSyncObj) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create status synchronization object for command buffer with id=%d", getId());
        this->free();
        return false;
    }

    // Reset lock sync obj
    reserved->lockSyncObj = VK_NULL_HANDLE;


    // Create commandBuffer
    VkCommandBufferAllocateInfo commandBufferAllocationInfo;
    commandBufferAllocationInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferAllocationInfo.pNext = NULL;
    commandBufferAllocationInfo.commandPool = commandPool.getVkCommandPool();
    commandBufferAllocationInfo.level = level;
    commandBufferAllocationInfo.commandBufferCount = 1;

    if (vkAllocateCommandBuffers(logicalDevice,
        &commandBufferAllocationInfo, &reserved->commandBuffer) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create the command buffer with id= %d", getId());
        this->free();
        return false;
    }

    reserved->commandPool = commandPool;
    reserved->level = level;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Starts recording of the command buffer.
 * @param commandBufferBeginInfo Specifying the command buffer begin operation.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::beginVkCommandBuffer(VkCommandBufferBeginInfo commandBufferBeginInfo)
{
    // Safety net:
    if (reserved->commandBuffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The VkCommandBuffer is not allocate in the command buffer with id= %d.", getId());
        return false;
    }

    if (vkBeginCommandBuffer(reserved->commandBuffer, &commandBufferBeginInfo) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to begin recording commands in the command buffer with id = %d.", getId());
        this->free();
        return false;
    }

    reserved->commandBufferUsageFlags = commandBufferBeginInfo.flags;

    // Done:
    return setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Ends recording of the command buffer.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::endVkCommandBuffer()
{
    // Safety net:
    if (reserved->commandBuffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The VkCommandBuffer is not allocate in the command buffer with id= %d.", getId());
        return false;
    }

    if (vkEndCommandBuffer(reserved->commandBuffer) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to end recording commands in the command buffer with id = %d.", getId());
        this->free();
        return false;
    }

    // Done:
    return setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Resets the command buffer to the initial state.
 * @param flags Controlling behavior of a command buffer reset.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::resetVkCommandBuffer(VkCommandBufferResetFlags flags)
{
    // Safety net:
    if (reserved->commandBuffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The VkCommandBuffer is not allocate in the command buffer with id= %d.", getId());
        return false;
    }

    std::optional<VkCommandPoolCreateFlags> commandPoolCreateFlags =
        reserved->commandPool.get().getCommandPoolCreateFlags();

    if (commandPoolCreateFlags.has_value() == false)
    {
        OV_LOG_ERROR("Failed to get creation flag of the command pool with id= %d in the command buffer with id= %d.",
            reserved->commandPool.get().getId(), getId());
        return false;
    }

    if ((commandPoolCreateFlags.value() & VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
        != VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
    {
        OV_LOG_ERROR("The command pool used to allocate the command buffer with id= %d does not provide individually reset.",
            getId());
        return false;
    }

    vkResetCommandBuffer( reserved->commandBuffer, flags);

	//Done:
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the OvVK CommandBuffer.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::free()
{
    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Free command buffer
    if (reserved->commandBuffer != VK_NULL_HANDLE)
    {
        vkFreeCommandBuffers(logicalDevice, reserved->commandPool.get().getVkCommandPool(),
            1, &reserved->commandBuffer);

        reserved->commandBuffer = VK_NULL_HANDLE;
        reserved->level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        reserved->commandBufferUsageFlags = 0x00000000;
        reserved->commandPool = OvVK::CommandPool::empty;
    }

    // Destroy status sync obj.
    if (reserved->statusSyncObj != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, NULL);
        reserved->statusSyncObj = VK_NULL_HANDLE;
    }

    // Reset lock sync obj
    reserved->lockSyncObj = VK_NULL_HANDLE;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan handle of the command buffer.
 * @return Vulkan handle to the command buffer.
 */
VkCommandBuffer OVVK_API OvVK::CommandBuffer::getVkCommandBuffer() const
{
    return reserved->commandBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the lock sync object.
 * @return The fence obj used to lock and unlock the command buffer.
 */
VkFence OVVK_API OvVK::CommandBuffer::getLockSyncObj() const 
{
    return reserved->lockSyncObj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the status sync object.
 * @return The semaphore obj used to know the status of the command buffer.
 */
VkSemaphore OVVK_API OvVK::CommandBuffer::getStatusSyncObj() const
{
    return reserved->statusSyncObj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the index of the queue to which the command buffer can be sent. Present only if the command buffer is allocated.
 * @return Optional std obj containing the index of the queue  to which the command buffer can be sent.
 */
std::optional<uint32_t> OVVK_API OvVK::CommandBuffer::getQueueFamilyIndex() const
{
    // Safety Net:
    if(reserved->commandBuffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The command buffer with id= %d is not allocated.", getId());
        return std::nullopt;
    }

    return reserved->commandPool.get().getQueueFamilyIndex();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the usage flag used to begin the command buffer.
 * @return The usage flag used to begin the command buffer.
 */
VkCommandBufferUsageFlags OVVK_API OvVK::CommandBuffer::getVkCommandBufferUsageFlags() const
{
    return reserved->commandBufferUsageFlags;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the command buffer.
 * @param value the uint value rapresenting the status the command buffer is in.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::setStatusSyncObjValue(uint64_t value)
{
    // Safety net:
    if (reserved->statusSyncObj == VK_NULL_HANDLE) 
    {
        OV_LOG_ERROR("The status sync obj is null in the command buffer with id= %d.", getId());
        return false;
    }

    // Logicel device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Retrievers current sempahore value
    uint64_t currentValue;
    vkGetSemaphoreCounterValue(logicalDevice, reserved->statusSyncObj, &currentValue);

    if (currentValue > value) {
        // Destroy semaphore because can't decrease the value
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, nullptr);
        reserved->statusSyncObj = VK_NULL_HANDLE;

        // Create new timeline semaphore and delete previous one.
        VkSemaphoreTypeCreateInfo timelineCreateInfo;
        timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        timelineCreateInfo.pNext = NULL;
        timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        timelineCreateInfo.initialValue = value;

        VkSemaphoreCreateInfo semaphoreInfo;
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreInfo.pNext = &timelineCreateInfo;
        semaphoreInfo.flags = 0;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &reserved->statusSyncObj) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status synchronization object of the command buffer with id= %d", getId());
            return false;
        }
    }
    else if (currentValue < value) {
        VkSemaphoreSignalInfo signalInfo;
        signalInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
        signalInfo.pNext = NULL;
        signalInfo.semaphore = reserved->statusSyncObj;
        signalInfo.value = value;

        if (vkSignalSemaphore(logicalDevice, &signalInfo) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status synchronization object of the command buffer with id= %d", getId());
            return false;
        }
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sync obj used to know if the command buffer is lock or unlock..
 * @param fence The sync obj used to know if the command buffer is lock or unlock.
 * @return TF
 */
bool OVVK_API OvVK::CommandBuffer::setLockSyncObj(VkFence fence)
{
    // Safety net:
    if (reserved->commandBuffer == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The command buffer with id= %d is not allocated.", getId());
        return false;
    }

    reserved->lockSyncObj = fence;

    //Done:
    return true;
}

#pragma endregion
