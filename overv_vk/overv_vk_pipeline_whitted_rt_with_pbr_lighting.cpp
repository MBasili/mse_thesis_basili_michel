/**
 * @file	overv_vk_lighting_pipeline.cpp
 * @brief	Vulkan Lighting pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum:
#include "magic_enum.hpp"

// C/ C++
#include <math.h>

// ImGui
#include "imgui.h"


/////////////
// SHADERS //
/////////////

#pragma region Shaders

#pragma region RGen

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline ray generation.
 */
static const std::string default_rgen = R"(

$include Constants$
$include Storage$
$include LightingPipeline$

// PushConstant
layout(push_constant) uniform PushConstantLighting { PushConstant pc; };

// Payloads
layout(location = 0) rayPayloadEXT HitPayload prd;

void main()
{   
    // Initialize the random number
    uint seed = tea(gl_LaunchIDEXT.y * gl_LaunchSizeEXT.x + gl_LaunchIDEXT.x, 0);

    prd.depth = 0;
    vec3 hitValues = vec3(0);

    for(int smpl = 0; smpl < pc.nrOfAASamples; smpl++)
    {
        float r1 = rnd(seed);
        float r2 = rnd(seed);
        // Subpixel jitter: send the ray through a different position inside the pixel
        // each time, to provide antialiasing.
        vec2 subpixel_jitter = smpl == 0 ? vec2(0.5f, 0.5f) : vec2(r1, r2);

        const vec2 pixelCenter = vec2(gl_LaunchIDEXT.xy) + subpixel_jitter;
        const vec2 inUV        = pixelCenter / vec2(gl_LaunchSizeEXT.xy);
        vec2       d           = inUV * 2.0 - 1.0;

        vec4 origin    = globalUniform.viewInverse * vec4(0, 0, 0, 1);
        vec4 target    = globalUniform.projInverse * vec4(d.x, d.y, 1, 1);
        vec4 direction = globalUniform.viewInverse * vec4(normalize(target.xyz), 0);

        uint  rayFlags = gl_RayFlagsNoneEXT;
        float tMin     = 0.001;
        float tMax     = 10000.0;

        traceRayEXT(topLevelAS,         // acceleration structure
                    rayFlags,           // rayFlags
                    0xFF,               // cullMask
                    0,                  // sbtRecordOffset
                    $sbtRecordStride$,  // sbtRecordStride
                    0,                  // missIndex
                    origin.xyz,         // ray origin
                    tMin,               // ray min range
                    direction.xyz,      // ray direction
                    tMax,               // ray max range
                    0                   // payload (location = 0)
        );

        hitValues += prd.hitValue;
    }

    prd.hitValue = hitValues / pc.nrOfAASamples;

    if(pc.isHDRCorrectionEnabled == true) {
        // Reinhard operator, preserving the high dynamic range of a possibly highly varying irradiance
        prd.hitValue = prd.hitValue / (prd.hitValue + vec3(1.0));
        prd.hitValue = pow(prd.hitValue.xyz, vec3(1.0/2.2)); 
    }

    imageStore(outputImage, ivec2(gl_LaunchIDEXT.xy), vec4(prd.hitValue, 1.0f));
})";

#pragma endregion

#pragma region RAHits

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline any hit shaders. Empty
 */
static const std::string empty_rahit = R"( 

void main() 
{ 

})";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline any hit shaders. Empty
 */
static const std::string default_rahit = R"( 

$include Constants$
$include Geometry$
$include RTObjDesc$
$include Storage$
$include LightingPipeline$

// Payloads
hitAttributeEXT HitAttribute hitAtt;

void main()
{   
    /////////////////
    // Object data //
    /////////////////

    RTObjDescData rtObjDesc = rtObjDescs.data[gl_InstanceCustomIndexEXT];
    GeometriesData geometriesData = GeometriesData(rtObjDesc.geometryDataAddress);
    GeometryData geometryData = geometriesData.geometriesData[gl_GeometryIndexEXT];

    Vertices vertices = Vertices(geometryData.vertexAddress);
    Faces faces = Faces(geometryData.indexAddress);
    MaterialData material = materials.data[geometryData.materialIndex];

    // Indices of the triangle
    FaceData ind = faces.faces[gl_PrimitiveID];

    // Vertex of the triangle
    VertexData v0 = vertices.vertices[ind.a];
    vec4 v0Normal = unpackSnorm4x8(v0.normal);
    vec2 v0TexCoord = unpackHalf2x16(v0.uv);

    VertexData v1 = vertices.vertices[ind.b];
    vec4 v1Normal = unpackSnorm4x8(v1.normal);
    vec2 v1TexCoord = unpackHalf2x16(v1.uv);

    VertexData v2 = vertices.vertices[ind.c];
    vec4 v2Normal = unpackSnorm4x8(v2.normal);
    vec2 v2TexCoord = unpackHalf2x16(v2.uv);

    const vec3 barycentrics = vec3(1.0 - hitAtt.baryCoord.x - hitAtt.baryCoord.y, hitAtt.baryCoord.x, hitAtt.baryCoord.y);

    // Texture coordinates
    vec2 texCoord = v0TexCoord * barycentrics.x + v1TexCoord * barycentrics.y + v2TexCoord * barycentrics.z;

    // Albedo
    if(material.albedoTexture > 0)
    {
        uint id = material.albedoTexture;
        if(texture(textureSamplers[nonuniformEXT(id)], texCoord).a <= 0)
        {
            ignoreIntersectionEXT;
        }
    }
})";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline any hit shaders. Empty
 */
static const std::string occlusion_rahit = R"( 

$include Constants$
$include Geometry$
$include RTObjDesc$
$include Storage$
$include LightingPipeline$

// PushConstant
layout(push_constant) uniform PushConstantLighting { PushConstant pc; };

// Payloads
layout(location = 1) rayPayloadInEXT OcclusionPayload prdOC;
hitAttributeEXT HitAttribute hitAtt;

void main()
{   
    /////////////////
    // Object data //
    /////////////////

    RTObjDescData rtObjDesc = rtObjDescs.data[gl_InstanceCustomIndexEXT];
    GeometriesData geometriesData = GeometriesData(rtObjDesc.geometryDataAddress);
    GeometryData geometryData = geometriesData.geometriesData[gl_GeometryIndexEXT];

    Vertices vertices = Vertices(geometryData.vertexAddress);
    Faces faces = Faces(geometryData.indexAddress);
    MaterialData material = materials.data[geometryData.materialIndex];

    // Indices of the triangle
    FaceData ind = faces.faces[gl_PrimitiveID];

    // Vertex of the triangle
    VertexData v0 = vertices.vertices[ind.a];
    vec4 v0Normal = unpackSnorm4x8(v0.normal);
    vec2 v0TexCoord = unpackHalf2x16(v0.uv);

    VertexData v1 = vertices.vertices[ind.b];
    vec4 v1Normal = unpackSnorm4x8(v1.normal);
    vec2 v1TexCoord = unpackHalf2x16(v1.uv);

    VertexData v2 = vertices.vertices[ind.c];
    vec4 v2Normal = unpackSnorm4x8(v2.normal);
    vec2 v2TexCoord = unpackHalf2x16(v2.uv);

    const vec3 barycentrics = vec3(1.0 - hitAtt.baryCoord.x - hitAtt.baryCoord.y, hitAtt.baryCoord.x, hitAtt.baryCoord.y);

    // Computing the coordinates of the hit position (object space)
    const vec3 pos = v0.vertex * barycentrics.x + v1.vertex * barycentrics.y + v2.vertex * barycentrics.z;
    // Specify the current object-to-world matrix gl_ObjectToWorldEXT
    vec3 worldPos = vec3(gl_ObjectToWorldEXT * vec4(pos, 1.0));  // Transforming the position to world space

    // Texture coordinates
    vec2 texCoord = v0TexCoord * barycentrics.x + v1TexCoord * barycentrics.y + v2TexCoord * barycentrics.z;

    // Normal extrapolated from vectors
    vec3 Nv = v0Normal.xyz * barycentrics.x + v1Normal.xyz * barycentrics.y + v2Normal.xyz * barycentrics.z;
    Nv = normalize(vec3(Nv * gl_WorldToObjectEXT));

    // Albedo
    vec4 albedo = vec4(material.albedo, 1.0);
    if(material.albedoTexture > 0 && pc.useAlbedoTexture == true)
    {
        uint id = material.albedoTexture;
        albedo = texture(textureSamplers[nonuniformEXT(id)], texCoord);
    }

    float NdotL = max(dot(Nv, normalize(gl_WorldRayDirectionEXT)), 0.0f);   

    ////////////////////
    // Compute Shadow //
    ////////////////////

    if(albedo.a <= 0)
    {
        ignoreIntersectionEXT;
    }
    else if(prdOC.geomID != gl_GeometryIndexEXT || gl_InstanceCustomIndexEXT != prdOC.instanceID)
    {
        prdOC.startColor = albedo.rgb;
        prdOC.geomID = gl_GeometryIndexEXT;
        prdOC.instanceID = gl_InstanceCustomIndexEXT;
        prdOC.primitiveID = gl_PrimitiveID;
        prdOC.pDotLFirst = NdotL;
    }
    else if(gl_PrimitiveID != prdOC.primitiveID &&
    ((prdOC.pDotLFirst >= 0.0f && NdotL <= 0.0f) || (prdOC.pDotLFirst <= 0.0f && NdotL >= 0.0f)))
    {
        vec3 finalColor = (prdOC.startColor + albedo.rgb) / 2 * (1 - material.opacity);
        finalColor = mix(finalColor, vec3(1.0f), (1 - material.opacity));
        prdOC.value *= finalColor; 
    }

    ignoreIntersectionEXT;

})";

#pragma endregion

#pragma region RCHits

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline closest hit shader. Empty
 */
static const std::string empty_rchit = R"(

void main() 
{ 

})";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline closest hit shader for primary and secondary rays.
 */
static const std::string default_rchit = R"(

#define MAX_SHADOW_HIT  16

$include Constants$
$include Geometry$
$include RTObjDesc$
$include Storage$
$include LightingPipeline$

// PushConstant
layout(push_constant) uniform PushConstantLighting { PushConstant pc; };

// Payloads
hitAttributeEXT HitAttribute hitAtt;
layout(location = 0) rayPayloadInEXT HitPayload prd;
layout(location = 1) rayPayloadEXT OcclusionPayload prdOC;
layout(location = 2) rayPayloadEXT AOPayload prdAO;

float ShadowRay(vec3 orig , vec3 dir)
{
    prdAO.hitSky = 0.0f; // Assume ray is occluded

    uint  rayFlags  = gl_RayFlagsOpaqueEXT | gl_RayFlagsSkipClosestHitShaderEXT |
                   gl_RayFlagsTerminateOnFirstHitEXT;
    float tMin   = 0.001;
    float tMax   = 1000;

    traceRayEXT(topLevelAS,     // acceleration structure
                rayFlags,       // rayFlags
                0xFF,           // cullMask
                4,              // sbtRecordOffset
                0,              // sbtRecordStride
                2,              // missIndex
                orig.xyz,       // ray origin
                tMin,           // ray min range
                dir.xyz,        // ray direction
                tMax,           // ray max range
                2               // payload (location = 0)
    );

    return prdAO.hitSky;
}

void main() 
{ 
    /////////////////
    // Object data //
    /////////////////

    RTObjDescData rtObjDesc = rtObjDescs.data[gl_InstanceCustomIndexEXT];
    GeometriesData geometriesData = GeometriesData(rtObjDesc.geometryDataAddress);
    GeometryData geometryData = geometriesData.geometriesData[gl_GeometryIndexEXT];

    Vertices vertices = Vertices(geometryData.vertexAddress);
    Faces faces = Faces(geometryData.indexAddress);
    MaterialData material = materials.data[geometryData.materialIndex];

    // Indices of the triangle
    FaceData ind = faces.faces[gl_PrimitiveID];

    // Vertex of the triangle
    VertexData v0 = vertices.vertices[ind.a];
    vec4 v0Normal = unpackSnorm4x8(v0.normal);
    vec4 v0Tangent = unpackSnorm4x8(v0.tangent);
    vec2 v0TexCoord = unpackHalf2x16(v0.uv);

    VertexData v1 = vertices.vertices[ind.b];
    vec4 v1Normal = unpackSnorm4x8(v1.normal);
    vec4 v1Tangent = unpackSnorm4x8(v1.tangent);
    vec2 v1TexCoord = unpackHalf2x16(v1.uv);

    VertexData v2 = vertices.vertices[ind.c];
    vec4 v2Normal = unpackSnorm4x8(v2.normal);
    vec4 v2Tangent = unpackSnorm4x8(v2.tangent);
    vec2 v2TexCoord = unpackHalf2x16(v2.uv);

    const vec3 barycentrics = vec3(1.0 - hitAtt.baryCoord.x - hitAtt.baryCoord.y, hitAtt.baryCoord.x, hitAtt.baryCoord.y);

    // Computing the coordinates of the hit position (object space)
    const vec3 pos = v0.vertex * barycentrics.x + v1.vertex * barycentrics.y + v2.vertex * barycentrics.z;
    // Specify the current object-to-world matrix gl_ObjectToWorldEXT
    vec3 worldPos = vec3(gl_ObjectToWorldEXT * vec4(pos, 1.0f));  // Transforming the position to world space

    // Texture coordinates
    vec2 texCoord = v0TexCoord * barycentrics.x + v1TexCoord * barycentrics.y + v2TexCoord * barycentrics.z;

    // Albedo
    vec3 albedo = vec3(0.0f);
    if(material.albedoTexture > 0 && pc.useAlbedoTexture == true)
    {
        uint id = material.albedoTexture;
        albedo = texture(textureSamplers[nonuniformEXT(id)], texCoord).rgb;
    }
    else { albedo = material.albedo; }

    // Roughness
    float roughness = 0;
    if(material.roughnessTexture > 0 && pc.useRoghnessTexture == true)
    {
        uint id = material.roughnessTexture;
        roughness = texture(textureSamplers[nonuniformEXT(id)], texCoord).r;
    }
    else { roughness = material.roughness; }

    // Metalness
    float metalness = 0;
    if(material.metalnessTexture > 0 && pc.useMetalnessTexture == true)
    {
        uint id = material.metalnessTexture;
        metalness = texture(textureSamplers[nonuniformEXT(id)], texCoord).r;
    }
    else { metalness = material.metalness; }

    ///////////////////////////////////////
    // Normals and Tangent and Bitangent //
    ///////////////////////////////////////

    // Normal extrapolated from vectors
    vec3 Nv = v0Normal.xyz * barycentrics.x + v1Normal.xyz * barycentrics.y + v2Normal.xyz * barycentrics.z;
    Nv = normalize(vec3(Nv * gl_WorldToObjectEXT));

    vec3 T = v0Tangent.xyz * barycentrics.x + v1Tangent.xyz * barycentrics.y + v2Tangent.xyz * barycentrics.z;
    T = normalize(vec3(T * gl_WorldToObjectEXT));

    // Normal
    vec3 N = Nv;
    if(material.normalTexture > 0)
    {
        ////////////////////
        // Normal mapping //
        ////////////////////

        uint id = material.normalTexture;
        vec3 normal = vec3(texture(textureSamplers[nonuniformEXT(id)], texCoord).rg, 1.0f);
        normal = vec3(normal.r, normal.g, sqrt(pow(1.0,2.0) - pow(normal.r,2.0) - pow(normal.g,2.0)));
        N = normal.rgb * 2.0 - 1.0;
    }

    // Correction => if tangent and normal are perpendicular dot product equal to 0.
    T = normalize(T - dot(T, Nv) * Nv);

    // BiTangent
    vec3 B = cross(Nv, T);

    // inverse = transpose iff the matrix is orthogonal 
    mat3 TBN = mat3(T, B, Nv);

    // Normal mapping
    if(material.normalTexture > 0)
    {
        N = normalize(TBN * N);
    }

    // Face normal
    vec3 v0v1 = v1.vertex - v0.vertex;
    vec3 v0v2 = v2.vertex - v0.vertex;
    vec4 Nf_obj = vec4(normalize(cross(v0v1, v0v2)), 0.0f);
    vec3 Nf = normalize(vec3(Nf_obj.xyz * gl_WorldToObjectEXT));
    
    vec3 Nf_original = Nf;
    vec3 N_original = N;
    vec3 Nv_original = Nv;

    bool isInside = false;
    // Coll. from inside
    if (dot(Nv.xyz, normalize(-gl_WorldRayDirectionEXT)) < 0.0f) 
    {
        isInside = true;
        Nv = -Nv;
        Nf = -Nf;
        T = -T;
        N = -N;
    }

    ////////
    // AO //
    ////////

    float aoValue = 1.0f;
    if(prd.depth == 0) {
        if(pc.isAmbientOcclusionEnabled == true && pc.nrOfAOSamples < UINT_MAX) {
            aoValue = 0.0f;
            vec3 aoPos = OffsetPositionAlongNormal(worldPos , Nv);
            
            // The seed of the random number sequence is initialized using the TEA algorithm, while the random number themselves will 
            // be generated using PCG. This is a fine when many random numbers are generated from this seed, but tea isn't a
            // random number generator and if you use only one sample per pixel, you will see correlation and the AO will 
            // not look fine because it won't sample uniformly the entire hemisphere. This could be resolved if the 
            // seed was kept over frame, but for this example, we will use this simple technique.
            // source: https://github.com/nvpro-samples/vk_raytracing_tutorial_KHR/tree/master/ray_tracing_ao
            // Initialize the random number
            uint seed = tea(gl_LaunchSizeEXT.x * gl_LaunchIDEXT.y + gl_LaunchIDEXT.x, pc.nrOfFramesAO);


            // Compute ambient occlusion.
            for(uint i = 0; i < pc.nrOfAOSamples; i++) {

                // Cosine sampling
                float r1        = rnd(seed);
                float r2        = rnd(seed);
                float sq        = sqrt(1.0 - r2);
                float phi       = 2 * PI * r1;
                vec3  direction = vec3(cos(phi) * sq, sin(phi) * sq, sqrt(r2));
                direction       = direction.x * T + direction.y * B + direction.z * Nv;

                aoValue += ShadowRay(aoPos , direction) / float(pc.nrOfAOSamples);
            }

            // Writting out the AO
            if(pc.nrOfFramesAO == 0)
            {
                imageStore(aoImage, ivec2(gl_LaunchIDEXT.xy), vec4(aoValue));
            }
            else
            {
                // Accumulating over time
                float old_ao     = imageLoad(aoImage, ivec2(gl_LaunchIDEXT.xy)).x;
                float new_result = mix(old_ao, aoValue, 1.0f / float(pc.nrOfFramesAO));
                aoValue = new_result;
                imageStore(aoImage, ivec2(gl_LaunchIDEXT.xy), vec4(new_result));
            }
        }

        if(pc.showAmbientOcclusionImage == true) {
            prd.hitValue = vec3(aoValue);
            return;
        }
    }

    // View direction
    vec3 V = normalize(-gl_WorldRayDirectionEXT);

    // reflectance equation
    vec3 Lo = vec3(0.0);

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metalness workflow)    
    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metalness);

    ////////////////
    // Reflection //
    ////////////////

    vec3 kD = vec3(1.0);

    vec3 reflectionNormal = (pc.useNormalMapForReflection == true) ? N : Nv;
       
    vec3 originRefl = worldPos + Nf * 0.001f;
    vec3 directionRefl = normalize(reflect(normalize(gl_WorldRayDirectionEXT), reflectionNormal));

    if(pc.showProblemNDFFunction == true) {
        
        vec3 H = normalize(V + directionRefl);

        // NDF Function
        float NDF = DistributionGGX(reflectionNormal, H, roughness);
        prd.hitValue = (NDF > 1) ? vec3(1.0f,0.0f,0.0f) : vec3(NDF);
        return;
    }

    if(prd.depth < pc.rayRecursionDepth)
    {
        prd.depth++;

        uint  rayFlags = gl_RayFlagsNoneEXT;
        float tMin     = 0.0f;
        float tMax     = 10000.0;

        vec3 diffuseValue = vec3(0.0f);
        uint seed = tea(gl_LaunchSizeEXT.x * gl_LaunchIDEXT.y + gl_LaunchIDEXT.x, 0);

        uint nrOfReflectionRay = (pc.useMultipleReflectionRays == true) ? pc.nrOfReflSamples : 1;

        for(uint i = 0; i < nrOfReflectionRay; i++) {

            // Cosine sampling
            float r1        = rnd(seed);
            float r2        = rnd(seed);
            float sq        = sqrt(1.0 - r2);
            float phi       = 2 * PI * r1;
            vec3 direction = vec3(cos(phi) * sq, sin(phi) * sq, sqrt(r2));
            direction       = direction.x * T + direction.y * B + direction.z * reflectionNormal;

            if(pc.useMultipleReflectionRays == true) {
                direction = normalize(directionRefl * (1.0f - roughness) + roughness * direction);
            }
            else{ direction = directionRefl; }

            traceRayEXT(topLevelAS,     // acceleration structure
                    rayFlags,           // rayFlags
                    0xFF,               // cullMask
                    1,                  // sbtRecordOffset
                    $sbtRecordStride$,  // sbtRecordStride
                    0,                  // missIndex
                    originRefl.xyz,     // ray origin
                    tMin,               // ray min range
                    direction.xyz,      // ray direction
                    tMax,               // ray max range
                    0                   // payload (location = 0)
            );

            vec3 H = normalize(V + direction);

            // cook-torrance brdf
            float NDF = DistributionGGX(reflectionNormal, H, roughness);

            if(pc.useNDFStandard == false) {
                NDF = clamp(NDF, 0.0f, 1.0f);
            }

            float G   = GeometrySmith(reflectionNormal, V, direction, roughness);   
            vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0); 

            vec3 kS = F;
            kD = vec3(1.0) - kS;
            kD *= 1.0 - metalness;
    
            vec3 numerator    = NDF * G * F;
            float denominator = 4.0 * max(dot(reflectionNormal, V), 0.0) * max(dot(reflectionNormal, direction), 0.0) + 0.0001;
            vec3 specular     = numerator / denominator;

            diffuseValue += specular * prd.hitValue *
                ((pc.scaleReflectionInvRoughness == true) ? 1 - roughness : 1.0f);
        }

        Lo += diffuseValue / nrOfReflectionRay;
        prd.depth--;
    }


    ////////////////
    // Refraction //
    ////////////////

    if(prd.depth < pc.rayRecursionDepth && 
        (material.opacity < 1.0f || pc.alwaysCalculateRefractedRays == true))
    {
        prd.depth++;
           
        vec3 origin = worldPos - Nf * 0.001f;

        float refractionRatio = generalRefractionIndex/material.refractionIndex;
        if(isInside) {
            refractionRatio = material.refractionIndex/generalRefractionIndex;
        }

        vec3 direction = refract(normalize(gl_WorldRayDirectionEXT), 
                            ((pc.useNormalMapForRefraction == true) ? N : Nv), refractionRatio);

        if(direction != vec3(0.0f)) {
            uint  rayFlags = gl_RayFlagsNoneEXT;
            float tMin     = 0.0;
            float tMax     = 10000.0;

            traceRayEXT(topLevelAS,         // acceleration structure
                        rayFlags,           // rayFlags
                        0xFF,               // cullMask
                        1,                  // sbtRecordOffset
                        $sbtRecordStride$,  // sbtRecordStride
                        0,                  // missIndex
                        origin.xyz,         // ray origin
                        tMin,               // ray min range
                        direction.xyz,      // ray direction
                        tMax,               // ray max range
                        0                   // payload (location = 0)
            );

            Lo += kD * (1 - material.opacity) * prd.hitValue;
        }
        prd.depth--;
    }

    $include PBRLightingPipeline$

    // Add ambient light
    if(pc.isAmbientOcclusionEnabled == true) {
        if(pc.useFirstVariationOfAO == true) {
            Lo += vec3(0.03) * albedo.xyz * aoValue;
        }
        else{ Lo *= aoValue; }
    }

    prd.hitValue =  Lo;
})";


#pragma endregion

#pragma region RMiss

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline miss shader. Default
 */
static const std::string default_rmiss = R"(

$include PayloadsStructsLightingPipeline$

// Payloads
layout(location = 0) rayPayloadInEXT HitPayload prd;

void main()
{   
    prd.hitValue = vec3(0.0);
})";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline shadow miss shader. Occlusion
 */
static const std::string occlusion_rmiss = R"(

$include PayloadsStructsLightingPipeline$

// Payloads
layout(location = 1) rayPayloadInEXT OcclusionPayload prd;

void main()
{
    prd.value *= vec3(1.0); 
})";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline shadow miss shader. Ambient Occlusion
 */
static const std::string ao_rmiss = R"(

$include PayloadsStructsLightingPipeline$

// Payloads
layout(location = 2) rayPayloadInEXT AOPayload prd;

void main()
{
    prd.hitSky = 1.0f; 
})";

#pragma endregion

#pragma endregion


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Pipeline class reserved structure.
 */
struct OvVK::WhittedRTwithPBRPipeline::Reserved
{
    // Descriptor sets
    OvVK::WhittedRTwithPBRDescriptorSetsManager whittedRTwithPBRDescriptorSetsManager;

    // Image where the rendering is saved
    OvVK::Image rtRenderImages[OvVK::Engine::nrFramesInFlight];
    OvVK::Image aoImages[OvVK::Engine::nrFramesInFlight];

    // Data used in the shaders
    PushConstant pushConstant;

    // Sync Obj. to sync GPU operation
    VkSemaphore transferPreviousRTStatusSyncObjs[OvVK::Engine::nrFramesInFlight];
    VkSemaphore graphicPreviousRTStatusSyncObjs[OvVK::Engine::nrFramesInFlight];
    VkSemaphore presentPreviousRTStatusSyncObjs[OvVK::Engine::nrFramesInFlight];

    // Nr of frames used by AO for each frame in flight
    uint32_t nrOfFramesAOUse[OvVK::Engine::nrFramesInFlight];

    /**
     * Constructor
     */
    Reserved() : transferPreviousRTStatusSyncObjs{ VK_NULL_HANDLE },
        graphicPreviousRTStatusSyncObjs{ VK_NULL_HANDLE },
        presentPreviousRTStatusSyncObjs{ VK_NULL_HANDLE }
    {
        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) {
            nrOfFramesAOUse[c] = 0;
        }
    }
};


////////////////////////////////////
// BODY OF CLASS WhittedRTwithPBRPipeline //
////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::WhittedRTwithPBRPipeline::WhittedRTwithPBRPipeline() :
    reserved(std::make_unique<OvVK::WhittedRTwithPBRPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name Lighting pipeline name
 */
OVVK_API OvVK::WhittedRTwithPBRPipeline::WhittedRTwithPBRPipeline(const std::string& name) : OvVK::RayTracingPipeline(name),
reserved(std::make_unique<OvVK::WhittedRTwithPBRPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::WhittedRTwithPBRPipeline::WhittedRTwithPBRPipeline(WhittedRTwithPBRPipeline&& other) :
    OvVK::RayTracingPipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::WhittedRTwithPBRPipeline::~WhittedRTwithPBRPipeline()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes pipeline.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::init()
{
    if (this->OvVK::RayTracingPipeline::init() == false)
        return false;


    // Free DSM
    reserved->whittedRTwithPBRDescriptorSetsManager.free();

    // Free images
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) {
        // RT image target
        if (reserved->rtRenderImages[c].free() == false)
            return false;
        // AO image
        if (reserved->aoImages[c].free() == false)
            return false;
    }



    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees pipeline.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::free()
{
    if (this->OvVK::RayTracingPipeline::free() == false)
        return false;


    // Free DSM
    reserved->whittedRTwithPBRDescriptorSetsManager.free();

    // Free images
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) {
        // RT image target
        if (reserved->rtRenderImages[c].free() == false)
            return false;
        // AO image
        if (reserved->aoImages[c].free() == false)
            return false;
    }


    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates lighting pipeline.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::create()
{
    // Init
    if (this->init() == false)
        return false;

    // Engine
    std::reference_wrapper<OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Presentation surface
    std::reference_wrapper<OvVK::Surface> presentSurface = engine.get().getPresentSurface();


    // Init rayTracing Descriptor Sets
    if (reserved->whittedRTwithPBRDescriptorSetsManager.create() == false)
    {
        OV_LOG_ERROR("Fail to initialize Whitted ray tracing with PBR DSM in the pipeline with id= %d.", getId());
        this->free();
        return false;
    }


    // Create image to render to 
    if (createTargetRenderImages() == false || createAOImages() == false)
    {
        this->free();
        return false;
    }


    //////////////////
    // Shaders init //
    //////////////////

    // Raygen shader
    std::reference_wrapper<OvVK::Shader> raygenShader = getRayGenShader();
    raygenShader.get().setName("raygenShader");
    if (raygenShader.get().load(Ov::Shader::Type::ray_generation, default_rgen) == false)
    {
        this->free();
        return false;
    }

    // AnyHit Shaders
    std::reference_wrapper<std::map<std::string, OvVK::Shader>> anyhitShaders = getAnyHitShaders();
    std::reference_wrapper<OvVK::Shader> emptyRAHit = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> defaultRAHit = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> occlusionRAHit = OvVK::Shader::empty;
    {
        OvVK::Shader emptyAnyHitShader;
        emptyAnyHitShader.setName("emptyAnyHitShader");
        if (emptyAnyHitShader.load(Ov::Shader::Type::any_hit, empty_rahit) == false)
        {
            this->free();
            return false;
        }

        anyhitShaders.get().insert(std::make_pair("emptyAnyHitShader", std::move(emptyAnyHitShader)));
        emptyRAHit = anyhitShaders.get()["emptyAnyHitShader"];

        OvVK::Shader defaultAnyHitShader;
        defaultAnyHitShader.setName("defaultAnyHitShader");
        if (defaultAnyHitShader.load(Ov::Shader::Type::any_hit, default_rahit) == false)
        {
            this->free();
            return false;
        }

        anyhitShaders.get().insert(std::make_pair("defaultAnyHitShader", std::move(defaultAnyHitShader)));
        defaultRAHit = anyhitShaders.get()["defaultAnyHitShader"];

        OvVK::Shader occlusionAnyHitShader;
        occlusionAnyHitShader.setName("occlusionAnyHitShader");
        if (occlusionAnyHitShader.load(Ov::Shader::Type::any_hit, occlusion_rahit) == false)
        {
            this->free();
            return false;
        }

        anyhitShaders.get().insert(std::make_pair("occlusionAnyHitShader", std::move(occlusionAnyHitShader)));
        occlusionRAHit = anyhitShaders.get()["occlusionAnyHitShader"];
    }

    // ClosestHit shaders
    std::reference_wrapper<std::map<std::string, OvVK::Shader>> closesthitShaders = getClosestHitShaders();
    std::reference_wrapper<OvVK::Shader> defaultRCHit = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> emptyRCHit = OvVK::Shader::empty;
    {
        // Default close hit shader for primary and secondary rays
        OvVK::Shader defaultCloseHitShader;
        defaultCloseHitShader.setName("defaultCloseHitShader");
        if (defaultCloseHitShader.load(Ov::Shader::Type::closes_hit, default_rchit) == false)
        {
            this->free();
            return false;
        }

        closesthitShaders.get().insert(std::make_pair("defaultCloseHitShader", std::move(defaultCloseHitShader)));
        defaultRCHit = closesthitShaders.get()["defaultCloseHitShader"];

        // Empty close hit shader
        OvVK::Shader emptyCloseHitShader;
        emptyCloseHitShader.setName("emptyCloseHitShader");
        if (emptyCloseHitShader.load(Ov::Shader::Type::closes_hit, empty_rchit) == false)
        {
            this->free();
            return false;
        }

        closesthitShaders.get().insert(std::make_pair("emptyCloseHitShader", std::move(emptyCloseHitShader)));
        emptyRCHit = closesthitShaders.get()["emptyCloseHitShader"];
    }

    // Miss shaders
    std::reference_wrapper<std::map<std::string, OvVK::Shader>> misshitShaders = getMissHitShaders();
    std::reference_wrapper<OvVK::Shader> defaultRMiss = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> occlusionRMiss = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> aoRMiss = OvVK::Shader::empty;
    {
        // Default miss shader for all rays
        OvVK::Shader defaultMissShader;
        defaultMissShader.setName("defaultMissShader");
        if (defaultMissShader.load(Ov::Shader::Type::miss, default_rmiss) == false)
        {
            this->free();
            return false;
        }

        misshitShaders.get().insert(std::make_pair("defaultMissShader", std::move(defaultMissShader)));
        defaultRMiss = misshitShaders.get()["defaultMissShader"];

        // occlusion miss shader
        OvVK::Shader occlusionMissShader;
        occlusionMissShader.setName("occlusionMissShader");
        if (occlusionMissShader.load(Ov::Shader::Type::miss, occlusion_rmiss) == false)
        {
            this->free();
            return false;
        }

        misshitShaders.get().insert(std::make_pair("occlusionMissShader", std::move(occlusionMissShader)));
        occlusionRMiss = misshitShaders.get()["occlusionMissShader"];

        // ambient occlusion miss shader
        OvVK::Shader aoMissShader;
        aoMissShader.setName("aoMissShader");
        if (aoMissShader.load(Ov::Shader::Type::miss, ao_rmiss) == false)
        {
            this->free();
            return false;
        }

        misshitShaders.get().insert(std::make_pair("aoMissShader", std::move(aoMissShader)));
        aoRMiss = misshitShaders.get()["aoMissShader"];
    }

    // Raygen shader group
    std::reference_wrapper<OvVK::RTShaderGroup> rayGenShaderGroup = getRayGenShaderGroup();
    rayGenShaderGroup.get().setName("raygenShaderGroup");
    if (rayGenShaderGroup.get().build({ raygenShader }) == false)
    {
        this->free();
        return false;
    }

    // Hit shader groups
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> hitShaderGroups = getHitShaderGroups();
    {
        // Default hit shader group for primary and secondary rays
		OvVK::RTShaderGroup defaultHitGroup;
		defaultHitGroup.setName("defaultHitShaderGroup");
		if (defaultHitGroup.build({ defaultRCHit, defaultRAHit }) == false)
		{
			this->free();
			return false;
		}
		hitShaderGroups.get().insert(std::make_pair("defaultHitShaderGroup", std::move(defaultHitGroup)));

		// Hit shader for occlusion rays
		OvVK::RTShaderGroup occlusionHitShaderGroup;
		occlusionHitShaderGroup.setName("occlusionHitShaderGroup");
		if (occlusionHitShaderGroup.build({ occlusionRAHit, emptyRCHit }) == false)
		{
			this->free();
			return false;
		}
		hitShaderGroups.get().insert(std::make_pair("occlusionHitShaderGroup", std::move(occlusionHitShaderGroup)));

		// Hit shader for ambient occlusion
		OvVK::RTShaderGroup aoHitShaderGroup;
		aoHitShaderGroup.setName("aoHitShaderGroup");
		if (aoHitShaderGroup.build({ emptyRCHit, emptyRAHit }) == false)
		{
			this->free();
			return false;
		}
		hitShaderGroups.get().insert(std::make_pair("aoHitShaderGroup", std::move(aoHitShaderGroup)));
	}

    // Miss shader Group
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> missShaderGroups = getMissShaderGroups();
	{
		// Add default miss shader group
		OvVK::RTShaderGroup missShaderGroup;
		missShaderGroup.setName("defaultMissShaderGroup");
		if (missShaderGroup.build({ defaultRMiss }) == false)
		{
			this->free();
			return false;
		}
		missShaderGroups.get().insert(std::make_pair("defaultMissShaderGroup", std::move(missShaderGroup)));

		// Add occlusion miss shader group
		OvVK::RTShaderGroup occlusionMissShaderGroup;
		occlusionMissShaderGroup.setName("occlusionMissShaderGroup");
		if (occlusionMissShaderGroup.build({ occlusionRMiss }) == false)
		{
			this->free();
			return false;
		}
		missShaderGroups.get().insert(std::make_pair("occlusionMissShaderGroup", std::move(occlusionMissShaderGroup)));

		// Add ao miss shader group
		OvVK::RTShaderGroup aoMissShaderGroup;
		aoMissShaderGroup.setName("aoMissShaderGroup");
		if (aoMissShaderGroup.build({ aoRMiss }) == false)
		{
			this->free();
			return false;
		}
		missShaderGroups.get().insert(std::make_pair("aoMissShaderGroup", std::move(aoMissShaderGroup)));
	}


    ///////////////////
    // PipelineLayot //
    ///////////////////

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
    pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreateInfo.pNext = NULL;
    pipelineLayoutCreateInfo.flags = 0x00000000;

    VkDescriptorSetLayout descriptorSetLayouts[3];
    descriptorSetLayouts[0] = engine.get().getStorage().getStorageDescriptorSetsManager().getVkDescriptorSetLayout();
    descriptorSetLayouts[1] = OvVK::TexturesDescriptorSetsManager::getInstance().getVkDescriptorSetLayout();
    descriptorSetLayouts[2] = reserved->whittedRTwithPBRDescriptorSetsManager.getVkDescriptorSetLayout();

    pipelineLayoutCreateInfo.setLayoutCount = 3;
    pipelineLayoutCreateInfo.pSetLayouts = descriptorSetLayouts;

    // Push constant used to know the current frame to use anti-aliasing.
    VkPushConstantRange pushConstantRange = {};
    pushConstantRange.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;
    // Both offset and size are in units of bytes and must be a multiple of 4.
    pushConstantRange.offset = 0;
    pushConstantRange.size = sizeof(PushConstant);

    pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
    pipelineLayoutCreateInfo.pPushConstantRanges = &pushConstantRange;


    // Create rtpipeline pipeline
    // VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR specifies that a closest hit shader
    // will always be present when a closest hit shader would be executed.A NULL closest hit shader is a closest
    // hit shader which is effectively VK_SHADER_UNUSED_KHR, such as from a shader group consisting entirely of zeros.
    // VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR specifies that a miss shader will always 
    // be present when a miss shader would be executed.A NULL miss shader is a miss shader which is effectively 
    // VK_SHADER_UNUSED_KHR, such as from a shader group consisting entirely of zeros.

    if (createRTPipeline(pipelineLayoutCreateInfo, getMaxRayRecursionDepth(),
        VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR |
        VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR) == false) {
        this->free();
        return false;
    }



    // add Pipeline to engine
    engine.get().addPipeline(*this);

    // Record call back function for windows resize
    OvVK::Surface::SurfaceCallbackTypes activeCallback = OvVK::Surface::SurfaceCallbackTypes::windowSize;
    setActiveCallback(activeCallback);
    presentSurface.get().addSurfaceDependentObject(getId(), *this);
    setIsActive(true);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the images in which the rendering result is saved.
 * @return TF.
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::createTargetRenderImages()
{
    // Safety net:
    if (isInitialized() == false)
    {
        OV_LOG_ERROR("The lighting pipeline with id= %d is not initialized.", getId());
        return false;
    }

    // Get Swapchain
    std::reference_wrapper<OvVK::Swapchain> swapchain = OvVK::Engine::getInstance().getSwapchain();
    VkExtent2D extent2d = swapchain.get().getSwapChainExtent();

    // RT target image
    VkImageCreateInfo outputImageCreateInfo = {};
    outputImageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    outputImageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    outputImageCreateInfo.extent.width = extent2d.width;
    outputImageCreateInfo.extent.height = extent2d.height;
    outputImageCreateInfo.extent.depth = 1;
    outputImageCreateInfo.mipLevels = 1;
    outputImageCreateInfo.arrayLayers = 1;
    outputImageCreateInfo.format = VK_FORMAT_B8G8R8A8_UNORM;
    // VK_IMAGE_TILING_OPTIMAL specifies optimal tiling (texels are laid out in an implementation-dependent
    // arrangement, for more efficient memory access).
    outputImageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    outputImageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_GENERAL;
    outputImageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
    outputImageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    outputImageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    // Create allocation info
    VmaAllocationCreateInfo vmaAllocationCreateInfo{};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    vmaAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Update Image
    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++)
    {
        // Recreate the render target image.
        if (reserved->rtRenderImages[c].free() == false)
        {
            OV_LOG_ERROR("Fail to free render target image for frame nr. %d in the lighting pipeline with id= %d", c, getId());
            return false;
        }
        if (reserved->rtRenderImages[c].create(outputImageCreateInfo, vmaAllocationCreateInfo) == false)
        {
            OV_LOG_ERROR("Fail to create render target image for the frame in flight %d in the lighting pipeline with id= %d", c, getId());
            return false;
        }

        // Create image view used by ray tracing
        VkImageViewCreateInfo imageViewCreateInfo = {};
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = reserved->rtRenderImages[c].getVkImage();
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = VK_FORMAT_B8G8R8A8_UNORM;

        if (swapchain.get().getSwapchainImageFormat() == VK_FORMAT_B8G8R8A8_SRGB) {
            imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_B;
            imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_R;
        }
        else {
            imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        }

        imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        // Get image view to access the image
        const OvVK::ImageView& rtRenderImageView = reserved->rtRenderImages[c].createImageView(imageViewCreateInfo);

       
        // Update the descriptor set with the new information of the created image.
        OvVK::WhittedRTwithPBRDescriptorSetsManager::UpdateData updateData = {};
        updateData.targetFrame = c;

        VkDescriptorImageInfo imageInfo = {};
        imageInfo.sampler = VK_NULL_HANDLE;
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        imageInfo.imageView = rtRenderImageView.getVkImageView();
        reserved->rtRenderImages[c].setImageLayout(VK_IMAGE_LAYOUT_GENERAL);
        updateData.descriptorOutputImageInfo = imageInfo;

        reserved->whittedRTwithPBRDescriptorSetsManager.addToUpdateList(updateData);
    }

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the images in which the ambient occlusion result it is saved.
 * @return TF.
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::createAOImages()
{
    // Safety net:
    if (isInitialized() == false)
    {
        OV_LOG_ERROR("The lighting pipeline with id= %d is not initialized.", getId());
        return false;
    }

    // Get Swapchain
    std::reference_wrapper<OvVK::Swapchain> swapchain = OvVK::Engine::getInstance().getSwapchain();
    VkExtent2D extent2d = swapchain.get().getSwapChainExtent();

    // AO image
    VkImageCreateInfo aoImageCreateInfo = {};
    aoImageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    aoImageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    aoImageCreateInfo.extent.width = extent2d.width;
    aoImageCreateInfo.extent.height = extent2d.height;
    aoImageCreateInfo.extent.depth = 1;
    aoImageCreateInfo.mipLevels = 1;
    aoImageCreateInfo.arrayLayers = 1;
    aoImageCreateInfo.format = VK_FORMAT_R8_UNORM;
    // VK_IMAGE_TILING_OPTIMAL specifies optimal tiling (texels are laid out in an implementation-dependent
    // arrangement, for more efficient memory access).
    aoImageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    aoImageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_GENERAL;
    aoImageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
    aoImageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    aoImageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    // Create allocation info
    VmaAllocationCreateInfo vmaAllocationCreateInfo{};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    vmaAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Update Image
    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++)
    {
        // Recreate the ao image.
        if (reserved->aoImages[c].free() == false)
        {
            OV_LOG_ERROR("Fail to free ao image for frame nr. %d in the lighting pipeline with id= %d", c, getId());
            return false;
        }
        if (reserved->aoImages[c].create(aoImageCreateInfo, vmaAllocationCreateInfo) == false)
        {
            OV_LOG_ERROR("Fail to create ao image for the frame in flight %d in the lighting pipeline with id= %d", c, getId());
            return false;
        }


        // Create image view used by ray tracing
        VkImageViewCreateInfo imageViewCreateInfo = {};
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = reserved->aoImages[c].getVkImage();
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = VK_FORMAT_R8_UNORM;
        imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        // Get image view to access the image
        const OvVK::ImageView& aoImageView = reserved->aoImages[c].createImageView(imageViewCreateInfo);


        // Update the descriptor set with the new information of the created image.
        OvVK::WhittedRTwithPBRDescriptorSetsManager::UpdateData updateData = {};
        updateData.targetFrame = c;

        VkDescriptorImageInfo imageInfo = {};
        imageInfo.sampler = VK_NULL_HANDLE;
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        imageInfo.imageView = aoImageView.getVkImageView();
        reserved->aoImages[c].setImageLayout(VK_IMAGE_LAYOUT_GENERAL);
        updateData.descriptorAOImageInfo = imageInfo;

        reserved->whittedRTwithPBRDescriptorSetsManager.addToUpdateList(updateData);

        reserved->nrOfFramesAOUse[c] = 0;
    }

    // Done:
    return true;
}


#pragma endregion

#pragma region SurfaceResized

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the window size callback is forwarded.
 * @param width New width.
 * @param height New heigth.
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::windowSizeCallback(int width, int height)
{
    if (createTargetRenderImages() == false)
        return false;

    if (createAOImages() == false)
        return false;
    
    // update date for the push constant for next frame
    reserved->pushConstant.nrOfFramesAO = 0;

    //  Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current recursion depth.
 * @return The current recursion depth.
 */
uint32_t OVVK_API OvVK::WhittedRTwithPBRPipeline::getRayRecursionDepth() const
{
    return reserved->pushConstant.rayRecursionDepth;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the recursion depth.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::setRayRecursionDepth(uint32_t rayRecursionDepth)
{
    if (rayRecursionDepth > getMaxRayRecursionDepth()) {
        OV_LOG_ERROR("The passed ray recursion depth exceed the maximum recursion depth of %d.",
            getMaxRayRecursionDepth());
        return false;
    }

    reserved->pushConstant.rayRecursionDepth = rayRecursionDepth;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current nrOfAASamples.
 * @return The current nrOfAASamples.
 */
uint32_t OVVK_API OvVK::WhittedRTwithPBRPipeline::getNrOfAASamples() const
{
    return reserved->pushConstant.nrOfAASamples;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the nrOfAASamples.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::setNrOfAASamples(uint32_t nrOfAASamples)
{
    reserved->pushConstant.nrOfAASamples = nrOfAASamples;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if ambient occlusion is enable.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::isAmbientOcclusionEnable() const {
    return reserved->pushConstant.isAmbientOcclusionEnabled ? true : false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Enable or disable ambient occlusion calculation.
 * @param value TF
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::enableDisableAmbientOcclusion(bool value) {
    
    if (value && reserved->pushConstant.isAmbientOcclusionEnabled == 1) {
        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) {
            reserved->nrOfFramesAOUse[c] = 0;
        }
    }
    
    reserved->pushConstant.isAmbientOcclusionEnabled = value ? 1 : 0;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Reset the nr of frames used by the AO to solve the noise pattern.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::resetAmbientOcclusionNrOfFrames() {
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) {
        reserved->nrOfFramesAOUse[c] = 0;
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param rtScene The rtScene containing the scene to render.
 * @return TF
 */

bool OVVK_API OvVK::WhittedRTwithPBRPipeline::render(const OvVK::RTScene& rtScene)
{
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Logical Device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Storage
    std::reference_wrapper<OvVK::Storage> storage = engine.get().getStorage();
    // Swapchain
    std::reference_wrapper<OvVK::Swapchain> swapchain = engine.get().getSwapchain();
    // frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();
    // Retrieve queue info
    std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamilyIndices =
        engine.get().getLogicalDevice().getQueueFamiliesIndices();
    if (queueFamilyIndices.has_value() == false)
    {
        OV_LOG_ERROR(R"(Fail to retriever queue family indices needed to transfer ownership in the lighting
 pipeline with id= %d.)", getId());
        return false;
    }

    // Wait CPU render sync object
    // Wait fences to know the work is finished
    VkFence fenceToWait = getStatusSyncObjCPU();
    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &fenceToWait,
        VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait render Fence in lighting pipeline with id= %d.", getId());
        return false;
    }


    ////////////////////
    // Reset syncObjs //
    ////////////////////
    
    if(getSBTHostBuffer().getVkBuffer() != VK_NULL_HANDLE)
        getSBTHostBuffer().setLockSyncObj(VK_NULL_HANDLE);
    if (getSBTDeviceBuffer().getVkBuffer() != VK_NULL_HANDLE)
        getSBTDeviceBuffer().setLockSyncObj(VK_NULL_HANDLE);

    // Create new timeline semaphore and delete previous one.
    VkSemaphoreTypeCreateInfo timelineCreateInfo = {};
    timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    timelineCreateInfo.pNext = NULL;
    timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    timelineCreateInfo.initialValue = OvVK::WhittedRTwithPBRPipeline::startDependenciesProcessValueStatusSyncObj;

    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfo.pNext = &timelineCreateInfo;
    semaphoreInfo.flags = 0;

    // Destroy GPU sync objs.
    vkDestroySemaphore(logicalDevice.get().getVkDevice(),
        reserved->transferPreviousRTStatusSyncObjs[frameInFlight], nullptr);
    if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfo,
        nullptr, &reserved->transferPreviousRTStatusSyncObjs[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to create semaphore to sync transfer ownership for transfer
 queue in lighting pipeline with id= %d)", getId());
        return false;
    }
    vkDestroySemaphore(logicalDevice.get().getVkDevice(),
        reserved->graphicPreviousRTStatusSyncObjs[frameInFlight], nullptr);
    if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfo,
        nullptr, &reserved->graphicPreviousRTStatusSyncObjs[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to create semaphore to sync transfer ownership for graphic
 queue in lighting pipeline with id= %d)", getId());
        return false;
    }
    vkDestroySemaphore(logicalDevice.get().getVkDevice(),
        reserved->presentPreviousRTStatusSyncObjs[frameInFlight], nullptr);
    if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfo,
        nullptr, &reserved->presentPreviousRTStatusSyncObjs[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to create semaphore to sync transfer ownership for present
 queue in lighting pipeline with id= %d)", getId());
        return false;
    }

    //////////////////////////////////////////
    // Calculate SBT Buffer size (in bytes) //
    //////////////////////////////////////////

    uint32_t shaderGroupHandleStride = getShaderGroupHandleStride();
    uint32_t shaderGroupBaseAlignment = getShaderGroupBaseAlignment();
    uint32_t shaderGroupHandleSize = getShaderGroupHandleSize();
    uint32_t numberOfClosestHitShaderHandle = rtScene.getNrOfHitShaderGroupsNeeded();

    VkDeviceSize sbtSize = 0;
    uint32_t baseAlignamentNeeded = 0;
    uint32_t baseAlignamentOverflow = 0;


    // RayGen shaderGroup
    VkDeviceSize sbtRayGenShaderGroupSize = 0;
    baseAlignamentNeeded = shaderGroupHandleStride / shaderGroupBaseAlignment;
    baseAlignamentOverflow = shaderGroupHandleStride % shaderGroupBaseAlignment;

    if (baseAlignamentNeeded == 0)
        sbtRayGenShaderGroupSize = shaderGroupBaseAlignment;
    else
        sbtRayGenShaderGroupSize = baseAlignamentNeeded * shaderGroupBaseAlignment +
        ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);

    sbtSize += sbtRayGenShaderGroupSize;

    // Hit shaderGroup
    VkDeviceSize sbtHitShaderGroupSize = 0;
    baseAlignamentNeeded = (numberOfClosestHitShaderHandle * shaderGroupHandleStride)
        / shaderGroupBaseAlignment;
    baseAlignamentOverflow = (numberOfClosestHitShaderHandle * shaderGroupHandleStride)
        % shaderGroupBaseAlignment;

    if (baseAlignamentNeeded == 0)
        sbtHitShaderGroupSize = shaderGroupBaseAlignment;
    else
        sbtHitShaderGroupSize = (baseAlignamentNeeded * shaderGroupBaseAlignment) +
        ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);


    sbtSize += sbtHitShaderGroupSize;

    //// Callable shaderGroup
    //VkDeviceSize sbtCallableShaderGroupSize = 0;
    //std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> callableShaderGroups =
    //    getCallableShaderGroups();
    //baseAlignamentNeeded = (callableShaderGroups.get().size() * shaderGroupHandleStride) / shaderGroupBaseAlignment;
    //baseAlignamentOverflow = (callableShaderGroups.get().size() * shaderGroupHandleStride) % shaderGroupBaseAlignment;

    //if (baseAlignamentNeeded == 0)
    //    sbtCallableShaderGroupSize = shaderGroupBaseAlignment;
    //else
    //    sbtCallableShaderGroupSize = (baseAlignamentNeeded * shaderGroupBaseAlignment) +
    //    ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);

    //sbtSize += sbtCallableShaderGroupSize;

    // Miss shaderGroup
    VkDeviceSize sbtMissShaderGroupSize = 0;
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> missShaderGroups =
        getMissShaderGroups();
    baseAlignamentNeeded = (missShaderGroups.get().size() * shaderGroupHandleStride) /
        shaderGroupBaseAlignment;
    baseAlignamentOverflow = (missShaderGroups.get().size() * shaderGroupHandleStride) %
        shaderGroupBaseAlignment;

    if (baseAlignamentNeeded == 0)
        sbtMissShaderGroupSize = shaderGroupBaseAlignment;
    else
        sbtMissShaderGroupSize = (baseAlignamentNeeded * shaderGroupBaseAlignment) +
        ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);

    sbtSize += sbtMissShaderGroupSize;


    // Staging Buffer
    // Create info buffer
    VkBufferCreateInfo sbtStagingBufferCreateInfo = {};
    sbtStagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    sbtStagingBufferCreateInfo.size = sbtSize;
    // VK_BUFFER_USAGE_TRANSFER_SRC_BIT => specifies that the buffer can be used as the source of a transfer command 
    sbtStagingBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    sbtStagingBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    sbtStagingBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    sbtStagingBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaSBTStagingBufferAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
    //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
    //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
    //      of allocation time.
    vmaSBTStagingBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
        VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
    //      mapped for host access using vkMapMemory.
    vmaSBTStagingBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

    if (getSBTHostBuffer().getVkBuffer() != VK_NULL_HANDLE && getSBTHostBuffer().getSize() == sbtSize) {
        if (getSBTHostBuffer().setStatusSyncObjValue(OvVK::WhittedRTwithPBRPipeline::LightingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to change status of the staging buffer for lighting pipeline with id= %d.", getId());
            return false;
        }
    }
    else {
        getSBTHostBuffer().free();
        if (getSBTHostBuffer().create(sbtStagingBufferCreateInfo, vmaSBTStagingBufferAllocationCreateInfo) == false ||
            getSBTHostBuffer().setStatusSyncObjValue(OvVK::WhittedRTwithPBRPipeline::LightingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to create sbt staging buffer for lighting pipeline with id= %d.", getId());
            return false;
        }
    } 


    // Device Buffer
    // Create info buffer
    VkBufferCreateInfo sbtDeviceBufferCreateInfo{};
    sbtDeviceBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    sbtDeviceBufferCreateInfo.size = sbtSize;
    // VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR => specifies that the buffer is suitable for use as a Shader Binding Table.
    // VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
    //      of a transfer command.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device
    //      address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    sbtDeviceBufferCreateInfo.usage = VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR
        | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    sbtDeviceBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    sbtDeviceBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    sbtDeviceBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaSBTDeviceBufferAllocationCreateInfo{};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    vmaSBTDeviceBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaSBTDeviceBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    if (getSBTDeviceBuffer().getVkBuffer() != VK_NULL_HANDLE && getSBTDeviceBuffer().getSize() == sbtSize) {
        if (getSBTDeviceBuffer().setStatusSyncObjValue(OvVK::WhittedRTwithPBRPipeline::LightingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to change status of the device buffer for lighting pipeline with id= %d.", getId());
            return false;
        }
    }
    else {
        getSBTDeviceBuffer().free();
        // Create buffer to contains vertex data
        if (getSBTDeviceBuffer().create(sbtDeviceBufferCreateInfo, vmaSBTDeviceBufferAllocationCreateInfo) == false ||
            getSBTDeviceBuffer().setStatusSyncObjValue(OvVK::WhittedRTwithPBRPipeline::LightingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to create SBT device buffer for lighting pipeline with id= %d.", getId());
            return false;
        }
    }


    /////////////////////////////
    // Fill SBT Staging Buffer //
    /////////////////////////////

    std::reference_wrapper<std::vector<uint8_t>> shaderGroupsHandleStorage = getShaderGroupsHandleStorage();

    // Get pointer to the memory where copy the data
    uint8_t* sbtStagingCurrentPosition = ((uint8_t*)getSBTHostBuffer().getVmaAllocationInfo().pMappedData);
    VkDeviceAddress sbtDeviceCurrentPosition = getSBTDeviceBuffer().getBufferDeviceAddress().value();


    // Copy RayGen shaderGroups on staging buffer
    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[getRayGenShaderGroup().getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize);

    VkStridedDeviceAddressRegionKHR& raygenShaderBindingTable = getRaygenShaderBindingTable();
    raygenShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    // The correct stride is reserved->strideShaderGroupHandle but there will be only one ray Gen.
    // The vulkan spec say that stride and size need to be equal in the pRaygenShaderBindingTable.
    //reserved->rgenShaderBindingTable.stride = reserved->strideShaderGroupHandle;
    raygenShaderBindingTable.stride = sbtRayGenShaderGroupSize;
    //Is reserved->shaderGroupBaseAlignment
    raygenShaderBindingTable.size = sbtRayGenShaderGroupSize;

    sbtStagingCurrentPosition += sbtRayGenShaderGroupSize;
    sbtDeviceCurrentPosition += sbtRayGenShaderGroupSize;


    // Get default hit shader group
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> hitShaderGroups = getHitShaderGroups();
    std::reference_wrapper<OvVK::RTShaderGroup> defaultHitGroup = OvVK::RTShaderGroup::empty;
    std::reference_wrapper<OvVK::RTShaderGroup> occlusionHitShaderGroup = OvVK::RTShaderGroup::empty;
    std::reference_wrapper<OvVK::RTShaderGroup> aoHitShaderGroup = OvVK::RTShaderGroup::empty;
    if (hitShaderGroups.get().size() == 0)
    {
        OV_LOG_ERROR("In the lighting pipeline with id= %d there are no hit shader groups.", getId());
        return false;
    }

    defaultHitGroup = hitShaderGroups.get()["defaultHitShaderGroup"];
    occlusionHitShaderGroup = hitShaderGroups.get()["occlusionHitShaderGroup"];
    aoHitShaderGroup = hitShaderGroups.get()["aoHitShaderGroup"];

    if (rtScene.getAreGeometriesSharingSameShadersGroups()) {
        // Primary ray
		memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[defaultHitGroup.get().getPositionInPipeline().value()
			* shaderGroupHandleSize],
			(size_t)shaderGroupHandleSize);

		sbtStagingCurrentPosition += shaderGroupHandleStride;

        // Secondary ray
        memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[defaultHitGroup.get().getPositionInPipeline().value()
            * shaderGroupHandleSize],
            (size_t)shaderGroupHandleSize);

        sbtStagingCurrentPosition += shaderGroupHandleStride;

        // Occlusion ray
        memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[occlusionHitShaderGroup.get().getPositionInPipeline().value()
            * shaderGroupHandleSize],
            (size_t)shaderGroupHandleSize);

        sbtStagingCurrentPosition += shaderGroupHandleStride;

        // AmbientOcclusion ray
        memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[aoHitShaderGroup.get().getPositionInPipeline().value()
            * shaderGroupHandleSize],
            (size_t)shaderGroupHandleSize);

        sbtStagingCurrentPosition += shaderGroupHandleStride;
    }
    else {
        // Copy hit shaderGroups on staging buffer
        const std::vector<OvVK::RTScene::RenderableElem>& renderableElems = rtScene.getRenderableElems();
        for (uint32_t i = rtScene.getNrOfLights(); i < rtScene.getNrOfRenderableElems(); i++)
        {
            // All RTObjDesc
            std::reference_wrapper<OvVK::RTObjDesc> renderableRTObjDesc =
                dynamic_cast<OvVK::RTObjDesc&>(renderableElems[i].renderable.get());

            for (uint32_t j = 0; j < renderableRTObjDesc.get().getNrGeoemtriesInBLAS(); j++)
            {
                // Primary ray
                memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[defaultHitGroup.get().getPositionInPipeline().value()
                    * shaderGroupHandleSize],
                    (size_t)shaderGroupHandleSize);

                sbtStagingCurrentPosition += shaderGroupHandleStride;

                // Secondary ray
                memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[defaultHitGroup.get().getPositionInPipeline().value()
                    * shaderGroupHandleSize],
                    (size_t)shaderGroupHandleSize);

                sbtStagingCurrentPosition += shaderGroupHandleStride;

                // Occlusion ray
                memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[occlusionHitShaderGroup.get().getPositionInPipeline().value()
                    * shaderGroupHandleSize],
                    (size_t)shaderGroupHandleSize);

                sbtStagingCurrentPosition += shaderGroupHandleStride;

                // AmbientOcclusion ray
                memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[aoHitShaderGroup.get().getPositionInPipeline().value()
                    * shaderGroupHandleSize],
                    (size_t)shaderGroupHandleSize);

                sbtStagingCurrentPosition += shaderGroupHandleStride;
            }
        }
    }

    VkStridedDeviceAddressRegionKHR& hitShaderBindingTable = getHitShaderBindingTable();
    hitShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    hitShaderBindingTable.stride = shaderGroupHandleStride;
    hitShaderBindingTable.size = sbtHitShaderGroupSize;

    sbtStagingCurrentPosition += (sbtHitShaderGroupSize - (numberOfClosestHitShaderHandle * shaderGroupHandleStride));
    sbtDeviceCurrentPosition += sbtHitShaderGroupSize;


    //// Copy callable shaderGroups on staging buffer
    //for (auto& [key, callableShaderGroup] : getCallableShaderGroups())
    //{
    //    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[callableShaderGroup.getPositionInPipeline().value()
    //        * shaderGroupHandleSize],
    //        (size_t)shaderGroupHandleSize);

    //    sbtStagingCurrentPosition += shaderGroupHandleStride;
    //}

    VkStridedDeviceAddressRegionKHR& callableShaderBindingTable = getCallableShaderBindingTable();
    callableShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    callableShaderBindingTable.stride = 0;
    callableShaderBindingTable.size = 0;

    //sbtStagingCurrentPosition += (sbtCallableShaderGroupSize - (getCallableShaderGroups().size() * shaderGroupHandleStride));
    //sbtDeviceCurrentPosition += sbtCallableShaderGroupSize;


    // Copy miss shaderGroups on staging buffer
    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[missShaderGroups.get()["defaultMissShaderGroup"].getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize);

    sbtStagingCurrentPosition += shaderGroupHandleStride;

    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[missShaderGroups.get()["occlusionMissShaderGroup"].getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize); 

    sbtStagingCurrentPosition += shaderGroupHandleStride;

    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[missShaderGroups.get()["aoMissShaderGroup"].getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize); 

    sbtStagingCurrentPosition += shaderGroupHandleStride;


    VkStridedDeviceAddressRegionKHR& missShaderBindingTable = getMissShaderBindingTable();
    missShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    missShaderBindingTable.stride = shaderGroupHandleStride;
    missShaderBindingTable.size = sbtMissShaderGroupSize;

    sbtStagingCurrentPosition += (sbtMissShaderGroupSize - (getMissShaderGroups().size() * shaderGroupHandleStride));
    sbtDeviceCurrentPosition += sbtMissShaderGroupSize;


    ////////////////
    // Update DSM //
    ////////////////

    reserved->whittedRTwithPBRDescriptorSetsManager.render();
    OvVK::TexturesDescriptorSetsManager::getInstance().render();


    ///////////////////////
    // Performs commands //
    ///////////////////////

    bool exit = false;
    bool transferCommandBufferToExecute = false;
    bool presentCommandBufferToExecute = false;
    bool graphicCommandBufferToExecute = false;
    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    std::reference_wrapper<OvVK::CommandPool> computeCommandPool = engine.get().getPrimaryComputeCmdPool();
    std::reference_wrapper<OvVK::CommandPool> transferCommandPool = engine.get().getPrimaryTransferCmdPool();
    std::reference_wrapper<OvVK::CommandPool> graphicCommandPool = engine.get().getPrimaryGraphicCmdPool();
    std::reference_wrapper<OvVK::CommandPool> presentCommandPool = engine.get().getPrimaryPresentCmdPool();

    std::reference_wrapper<OvVK::CommandBuffer> computeCommandBuffer = computeCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> transferCommandBuffer = transferCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> graphicCommandBuffer = graphicCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> blitCommandBuffer = graphicCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> presentCommandBuffer = presentCommandPool.get().allocateCommandBuffer();

    if (computeCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        computeCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the compute command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (transferCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the transfer command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (graphicCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the graphic command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (blitCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        blitCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the blit command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (presentCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        presentCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the present command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }

    if (exit) {
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
        presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
        return false;
    }


    /////////
    // SBT //
    /////////

    // SetUp copy sbt buffer
    VkBufferCopy sbtBufferCopy = {};
    // The regions are defined in VkBufferCopy structs and consist of a source buffer offset, destination
    // buffer offset and size. It is not possible to specify VK_WHOLE_SIZE here, unlike the vkMapMemory command.
    sbtBufferCopy.srcOffset = 0; // Optional
    sbtBufferCopy.dstOffset = 0; // Optional
    sbtBufferCopy.size = sbtSize;

    // Copy AS Instances
    vkCmdCopyBuffer(computeCommandBuffer.get().getVkCommandBuffer(), getSBTHostBuffer().getVkBuffer(),
        getSBTDeviceBuffer().getVkBuffer(), 1, &sbtBufferCopy);

    //getSBTHostBuffer().setLockSyncObj(getStatusSyncObjCPU());
    //getSBTDeviceBuffer().setLockSyncObj(getStatusSyncObjCPU());

    // AS Instances buffer
    VkBufferMemoryBarrier2 sbtBufferMemoryBarrier2 = {};
    sbtBufferMemoryBarrier2.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    sbtBufferMemoryBarrier2.pNext = VK_NULL_HANDLE;
    // Need to wait previous stage 
    sbtBufferMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // Need to wait on specific memory access.
    sbtBufferMemoryBarrier2.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
        VK_ACCESS_2_TRANSFER_READ_BIT;
    // All ray tracing command need to wait the SBT buffer transfer.
    sbtBufferMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // All ray tracing access AS command need to wait the SBT buffer transfer.
    sbtBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;
    // Need to transfer ownership
    sbtBufferMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    sbtBufferMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // buffer
    sbtBufferMemoryBarrier2.buffer = getSBTDeviceBuffer().getVkBuffer();
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    sbtBufferMemoryBarrier2.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    sbtBufferMemoryBarrier2.size = VK_WHOLE_SIZE;

    // Barrier specification
    VkDependencyInfo sbtDependencyInfo = {};
    sbtDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    sbtDependencyInfo.dependencyFlags = 0x00000000;
    sbtDependencyInfo.memoryBarrierCount = 0;
    sbtDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    sbtDependencyInfo.bufferMemoryBarrierCount = 1;
    sbtDependencyInfo.pBufferMemoryBarriers = &sbtBufferMemoryBarrier2;
    sbtDependencyInfo.imageMemoryBarrierCount = 0;
    sbtDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register command for transition.
    vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &sbtDependencyInfo);


    /////////////
    // Storage //
    /////////////

    std::vector<VkBufferMemoryBarrier2> graphicVkBufferMemoryBarrier2;
    std::vector<VkBufferMemoryBarrier2> transferVkBufferMemoryBarrier2;

    // Memory barrier for storage buffers
    VkBufferMemoryBarrier2 memoryBarrier;
    memoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    memoryBarrier.pNext = VK_NULL_HANDLE;
    // No need to wait previous stage 
    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
    memoryBarrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
    // No need to wait on specific memory access.
    memoryBarrier.srcAccessMask = VK_ACCESS_2_NONE;
    // All ray tracing command need to wait the SBT buffer transfer.
    memoryBarrier.dstStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // All ray tracing access AS command need to wait the SBT buffer transfer.
    memoryBarrier.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;
    // Need to transfer ownership
    memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    memoryBarrier.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    memoryBarrier.size = VK_WHOLE_SIZE;


    // Global uniform buffer change ownership
    if (storage.get().getGlobalUniformDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getGlobalUniformDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getGlobalUniformDeviceBuffer().getVkBuffer();

        if (storage.get().getGlobalUniformDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }

    // Materials buffer change ownership
    if (storage.get().getMaterialsDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getMaterialsDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getMaterialsDeviceBuffer().getVkBuffer();

        if (storage.get().getMaterialsDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }

    // Lights buffer change ownership
    if (storage.get().getLightsDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getLightsDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getLightsDeviceBuffer().getVkBuffer();

        if (storage.get().getLightsDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }

    // RTObjDesc uniform buffer change ownership
    if (storage.get().getRTObjDescsDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getRTObjDescsDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getRTObjDescsDeviceBuffer().getVkBuffer();

        if (storage.get().getRTObjDescsDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }


    // Barrier specification
    VkDependencyInfo storageDependencyInfo = {};
    storageDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    storageDependencyInfo.dependencyFlags = 0x00000000;
    storageDependencyInfo.memoryBarrierCount = 0;
    storageDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    storageDependencyInfo.bufferMemoryBarrierCount = graphicVkBufferMemoryBarrier2.size();
    storageDependencyInfo.pBufferMemoryBarriers = graphicVkBufferMemoryBarrier2.data();
    storageDependencyInfo.imageMemoryBarrierCount = 0;
    storageDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register command for transition.
    if (graphicVkBufferMemoryBarrier2.size() > 0) {
        vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
        vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
        graphicCommandBufferToExecute = true;
    }

    storageDependencyInfo.bufferMemoryBarrierCount = transferVkBufferMemoryBarrier2.size();
    storageDependencyInfo.pBufferMemoryBarriers = transferVkBufferMemoryBarrier2.data();

    // Register command for transition.
    if (transferVkBufferMemoryBarrier2.size() > 0) {
        vkCmdPipelineBarrier2(transferCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
        vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
        transferCommandBufferToExecute = true;
    }


    //////////////
    // image RT //
    //////////////
    
    // RTimage change layout
    VkImageMemoryBarrier2 rtImageBarrier;
    rtImageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    rtImageBarrier.pNext = VK_NULL_HANDLE;
    // No need to wait previous stage for changing the texture layout
    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
    rtImageBarrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
    // No need to wait on specific memory access.
    rtImageBarrier.srcAccessMask = VK_ACCESS_2_NONE;
    // All ray tracing command need to wait the SBT buffer transfer.
    rtImageBarrier.dstStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // All ray tracing access AS command need to wait the SBT buffer transfer.
    rtImageBarrier.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;
    // Current texture layout
    rtImageBarrier.oldLayout = reserved->rtRenderImages[frameInFlight].getImageLayout();
    // Set the layout that allow a fresh write to the texture.
    rtImageBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
    // Default value for not chaging the queue family index.
    rtImageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    rtImageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // The image and subresourceRange specify the image that is affected and the specific part of the image.
    rtImageBarrier.image = reserved->rtRenderImages[frameInFlight].getVkImage();
    rtImageBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    // baseMipLevel is the first mipmap level accessible to the view.
    rtImageBarrier.subresourceRange.baseMipLevel = 0;
    rtImageBarrier.subresourceRange.levelCount = 1;
    rtImageBarrier.subresourceRange.baseArrayLayer = 0;
    rtImageBarrier.subresourceRange.layerCount = 1;


    // Barrier specification
    VkDependencyInfo rtImageDependencyInfo = {};
    rtImageDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    rtImageDependencyInfo.dependencyFlags = 0x00000000;
    rtImageDependencyInfo.memoryBarrierCount = 0;
    rtImageDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    rtImageDependencyInfo.bufferMemoryBarrierCount = 0;
    rtImageDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
    rtImageDependencyInfo.imageMemoryBarrierCount = 1;
    rtImageDependencyInfo.pImageMemoryBarriers = &rtImageBarrier;


    if (reserved->rtRenderImages[frameInFlight].getQueueFamilyIndex().has_value() &&
        reserved->rtRenderImages[frameInFlight].getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value())
    {
        // The src family is the graphics family. No need to record barrier for the graphic family because already do at the end.
        rtImageBarrier.srcQueueFamilyIndex = reserved->rtRenderImages[frameInFlight].getQueueFamilyIndex().value();
        rtImageBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();

        // Register command for transition.
        if (reserved->rtRenderImages[frameInFlight].getQueueFamilyIndex().value() == queueFamilyIndices.value().graphicsFamily.value()) {
            vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &rtImageDependencyInfo);
            graphicCommandBufferToExecute = true;
        }
        else {
            vkCmdPipelineBarrier2(transferCommandBuffer.get().getVkCommandBuffer(), &rtImageDependencyInfo);
            transferCommandBufferToExecute = true;
        }
    }

    vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &rtImageDependencyInfo);

    /////////////////
    // Ray tracing //
    /////////////////

    // Bind Pipeline
    vkCmdBindPipeline(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, getVkPipeline());

    // Descriptor sets
    VkDescriptorSet descriptorSet;
    VkPipelineLayout pipelineLayout = getVkPipelineLayout();

    // Bind Storage Descriptor Set
    descriptorSet = engine.get().getStorage().getStorageDescriptorSetsManager().getVkDescriptorSet(frameInFlight);
    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelineLayout,
        magic_enum::enum_integer(OvVK::WhittedRTwithPBRPipeline::BindSet::storage), 1,
        &descriptorSet, 0, 0);

    // Bind Texture Descriptor Set
    descriptorSet = OvVK::TexturesDescriptorSetsManager::getInstance().getVkDescriptorSet(frameInFlight);
    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelineLayout,
        magic_enum::enum_integer(OvVK::WhittedRTwithPBRPipeline::BindSet::texture), 1,
        &descriptorSet, 0, 0);

    // Bind Lighting Descriptor Set
    descriptorSet = reserved->whittedRTwithPBRDescriptorSetsManager.getVkDescriptorSet(frameInFlight);
    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelineLayout,
        magic_enum::enum_integer(OvVK::WhittedRTwithPBRPipeline::BindSet::lighting), 1,
        &descriptorSet, 0, 0);

    // Bind the RT pipeline DSM 
    std::reference_wrapper<OvVK::RayTracingDescriptorSetsManager> rtDSM =
        getRayTracingDescriptorSetsManager();

    // Update DSM with the new TLAS
    OvVK::RayTracingDescriptorSetsManager::UpdateData updateRTDSM;
    updateRTDSM.targetFrame = frameInFlight;
    updateRTDSM.tlas = rtScene.getTLAS().getVkAS();

    rtDSM.get().addToUpdateList(updateRTDSM);
    rtDSM.get().render();

    // Bind RT Descriptor Set
    descriptorSet = getRayTracingDescriptorSetsManager().getVkDescriptorSet(frameInFlight);
    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelineLayout,
        magic_enum::enum_integer(OvVK::WhittedRTwithPBRPipeline::BindSet::rt), 1,
        &descriptorSet, 0, 0);

    // Set value for AO for current frame in flight
    reserved->pushConstant.nrOfFramesAO = reserved->nrOfFramesAOUse[frameInFlight];

    // Upload the push constant
    vkCmdPushConstants(computeCommandBuffer.get().getVkCommandBuffer(), pipelineLayout,
        VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR
        | VK_SHADER_STAGE_ANY_HIT_BIT_KHR , 0, sizeof(PushConstant), &reserved->pushConstant);

    // update date for the push constant for next frame
    if(reserved->pushConstant.isAmbientOcclusionEnabled == 1 && reserved->nrOfFramesAOUse[frameInFlight] < UINT32_MAX)
        reserved->nrOfFramesAOUse[frameInFlight] += 1;

    // Record Ray tracing command
    PFN_vkCmdTraceRaysKHR pvkCmdTraceRaysKHR = (PFN_vkCmdTraceRaysKHR)vkGetDeviceProcAddr(
        engine.get().getLogicalDevice().getVkDevice(), "vkCmdTraceRaysKHR");

    pvkCmdTraceRaysKHR(computeCommandBuffer.get().getVkCommandBuffer(),
        &getRaygenShaderBindingTable(),
        &getMissShaderBindingTable(),
        &getHitShaderBindingTable(),
        &getCallableShaderBindingTable(),
        reserved->rtRenderImages[engine.get().getFrameInFlight()].getExtend3D().width,
        reserved->rtRenderImages[engine.get().getFrameInFlight()].getExtend3D().height,
        reserved->rtRenderImages[engine.get().getFrameInFlight()].getExtend3D().depth);


    //////////////////////////////
    // Save result in Swapchain //
    //////////////////////////////

    // Change target image layout
    rtImageBarrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
    // Set the layout that allow a fresh write to the texture.
    rtImageBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    // No need to wait previous stage for changing the swapchain image layout
    rtImageBarrier.srcStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // No need to wait on specific memory access.
    rtImageBarrier.srcAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;
    // All blit operation need to wait the ray tracing execution is terminated.
    rtImageBarrier.dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
    rtImageBarrier.dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT |
        VK_ACCESS_2_TRANSFER_WRITE_BIT;
    // Change queue family for blit operation
    rtImageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    rtImageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    if (queueFamilyIndices.value().graphicsFamily.value() != queueFamilyIndices.value().computeFamily.value())
    {
        // Wait all ray tracing commands finish execution.
        //rtImageBarrier.srcStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
        //rtImageBarrier.srcAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;

        rtImageBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
        rtImageBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();

        // Register command for transition.
        vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &rtImageDependencyInfo);
    }

    vkCmdPipelineBarrier2(blitCommandBuffer.get().getVkCommandBuffer(), &rtImageDependencyInfo);


    // Retrieve swapchain data
    uint32_t presentableImageIndex = engine.get().getPresentableImageIndex();
    VkImage swapchainImage = swapchain.get().getSwapchainImage(presentableImageIndex);
    VkImageView swapchainImageView = swapchain.get().getSwapchainImageView(presentableImageIndex);
    std::optional<uint32_t> swapchainImageOwnershipQueue =
        swapchain.get().getSwapchainImageOwnership(presentableImageIndex);

    // Swapchain image take ownership
    VkImageMemoryBarrier2 swapchainImageMemoryBarrier2 = {};
    swapchainImageMemoryBarrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    swapchainImageMemoryBarrier2.pNext = VK_NULL_HANDLE;
    // No need to wait previous stage for changing the swapchain image layout
    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
    swapchainImageMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
    // No need to wait on specific memory access.
    swapchainImageMemoryBarrier2.srcAccessMask = VK_ACCESS_2_NONE;
    // All blit commands need to wait that the swapchain image change layout.
    swapchainImageMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
    swapchainImageMemoryBarrier2.dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT |
        VK_ACCESS_2_TRANSFER_WRITE_BIT;
    // Current texture layout
    // Not interested in the current layout
    if (swapchain.get().getSwapchainImageLayout(presentableImageIndex).has_value())
        swapchainImageMemoryBarrier2.oldLayout = swapchain.get().getSwapchainImageLayout(presentableImageIndex).value();
    else
        swapchainImageMemoryBarrier2.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    // Set the layout that allow a fresh write to the texture.
    swapchainImageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    // No need to transfer ownership because first time used.
    swapchainImageMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    swapchainImageMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // The image and subresourceRange specify the image that is affected and the specific part of the image.
    swapchainImageMemoryBarrier2.image = swapchainImage;
    swapchainImageMemoryBarrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    // baseMipLevel is the first mipmap level accessible to the view.
    swapchainImageMemoryBarrier2.subresourceRange.baseMipLevel = 0;
    swapchainImageMemoryBarrier2.subresourceRange.levelCount = 1;
    swapchainImageMemoryBarrier2.subresourceRange.baseArrayLayer = 0;
    swapchainImageMemoryBarrier2.subresourceRange.layerCount = 1;

    // Barrier specification
    VkDependencyInfo swapchainImageDependencyInfo = {};
    swapchainImageDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    swapchainImageDependencyInfo.dependencyFlags = 0x00000000;
    swapchainImageDependencyInfo.memoryBarrierCount = 0;
    swapchainImageDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    swapchainImageDependencyInfo.bufferMemoryBarrierCount = 0;
    swapchainImageDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
    swapchainImageDependencyInfo.imageMemoryBarrierCount = 1;
    swapchainImageDependencyInfo.pImageMemoryBarriers = &swapchainImageMemoryBarrier2;

    if (swapchainImageOwnershipQueue.has_value() &&
        swapchainImageOwnershipQueue.value() != queueFamilyIndices.value().graphicsFamily.value())
    {
        // The src family is the graphics family. No need to record barrier for the graphic family because already do at the end.
        swapchainImageMemoryBarrier2.srcQueueFamilyIndex = swapchainImageOwnershipQueue.value();
        swapchainImageMemoryBarrier2.dstQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();

        // Register command for transition.
        if (swapchainImageOwnershipQueue == queueFamilyIndices.value().computeFamily.value()) {
            vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &swapchainImageDependencyInfo);
        }
        else if(swapchainImageOwnershipQueue == queueFamilyIndices.value().transferFamily.value()) {
            vkCmdPipelineBarrier2(transferCommandBuffer.get().getVkCommandBuffer(), &swapchainImageDependencyInfo);
            transferCommandBufferToExecute = true;
        }
		else {
			vkCmdPipelineBarrier2(presentCommandBuffer.get().getVkCommandBuffer(), &swapchainImageDependencyInfo);
			presentCommandBufferToExecute = true;
        }
    }

    // Register command for transition.
    vkCmdPipelineBarrier2(blitCommandBuffer.get().getVkCommandBuffer(), &swapchainImageDependencyInfo);


    ////////////////
    // Blit Image //
    ////////////////
    
    // Need blit because rt image and swapchain image are of different format.
    VkImageSubresourceLayers subresourceLayers = {};
    subresourceLayers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceLayers.mipLevel = 0;
    subresourceLayers.baseArrayLayer = 0;
    subresourceLayers.layerCount = 1;

    VkOffset3D offset1 = {};
    offset1.x = 0;
    offset1.y = 0;
    offset1.z = 0;

    VkExtent3D rtExtent3D = reserved->rtRenderImages[frameInFlight].getExtend3D();
    VkOffset3D offset2 = {};
    offset2.x = rtExtent3D.width;
    offset2.y = rtExtent3D.height;
    offset2.z = rtExtent3D.depth;

    VkImageBlit2 imageBlit = {};
    imageBlit.sType = VK_STRUCTURE_TYPE_IMAGE_BLIT_2;
    imageBlit.pNext = VK_NULL_HANDLE;
    imageBlit.srcSubresource = subresourceLayers;
    imageBlit.srcOffsets[0] = offset1;
    imageBlit.srcOffsets[1] = offset2;
    imageBlit.dstSubresource = subresourceLayers;
    imageBlit.dstOffsets[0] = offset1;
    imageBlit.dstOffsets[1] = offset2;

    VkBlitImageInfo2 blitImageInfo2 = {};
    blitImageInfo2.sType = VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2;
    blitImageInfo2.pNext = VK_NULL_HANDLE;
    blitImageInfo2.srcImage = reserved->rtRenderImages[frameInFlight].getVkImage();
    blitImageInfo2.srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    blitImageInfo2.dstImage = swapchainImage;
    blitImageInfo2.dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    blitImageInfo2.regionCount = 1;
    blitImageInfo2.pRegions = &imageBlit;
    blitImageInfo2.filter = VK_FILTER_NEAREST;

    vkCmdBlitImage2(blitCommandBuffer.get().getVkCommandBuffer(), &blitImageInfo2);


    ///////////////////////////////
    // End registration commands //
    ///////////////////////////////

    // End primary commandBuffers
    exit = false;
    if (computeCommandBuffer.get().endVkCommandBuffer() == false ||
        computeCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the compute command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (transferCommandBuffer.get().endVkCommandBuffer() == false ||
        transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the transfer command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (graphicCommandBuffer.get().endVkCommandBuffer() == false ||
        graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the graphic command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (blitCommandBuffer.get().endVkCommandBuffer() == false ||
        blitCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the blit command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (presentCommandBuffer.get().endVkCommandBuffer() == false ||
        presentCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the present command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }

    if (exit) {
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
        presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
        return false;
    }


    ///////////////
    //SetUp sync //
    ///////////////

    // Set before the value because if the value is smaller the sync obj are recreated.
    setStatusSyncObjGPU(OvVK::Pipeline::renderingValueStatusSyncObj);


    ////////////
    // Submit //
    ////////////

    // Retrieve queue handle
    std::optional<OvVK::LogicalDevice::QueueHandles> queueHandles =
        engine.get().getLogicalDevice().getQueueHandles();


    // Wait all pipeline the pipeline depend on.
    std::vector<VkSemaphore> toWaitSemaphore = getStatusSyncObjsGPUToWait();
    std::vector<uint64_t> waitValue(toWaitSemaphore.size(), OvVK::Pipeline::renderingDoneValueStatusSyncObj);
    // This pipeline sync obj
    toWaitSemaphore.push_back(rtScene.getRederingStatusSyncObjGPU());
    waitValue.push_back(OvVK::RTScene::finishValueRederingStatusSyncObj);
    toWaitSemaphore.push_back(getStatusSyncObjGPU());
    waitValue.push_back(OvVK::Pipeline::renderingValueStatusSyncObj);

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(toWaitSemaphore.size());
    for (uint32_t c = 0; c < toWaitSemaphore.size(); c++) {
        pWaitSemaphoreInfos[c].sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pWaitSemaphoreInfos[c].pNext = VK_NULL_HANDLE;
        pWaitSemaphoreInfos[c].semaphore = toWaitSemaphore[c];
        pWaitSemaphoreInfos[c].value = waitValue[c];
        // All command s need to wait
        // VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT 
        pWaitSemaphoreInfos[c].stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pWaitSemaphoreInfos[c].deviceIndex = 0;
    }

    // Present command buffer submit
    if (presentCommandBufferToExecute) {

        // Submit
        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = presentCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos;
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.semaphore = reserved->presentPreviousRTStatusSyncObjs[frameInFlight];
        // Ignore because presentPreviousRTStatusSyncObjs binary
        pSignalSemaphoreInfos.value = OvVK::WhittedRTwithPBRPipeline::finishDependenciesProcessValueStatusSyncObj;
        // All command s need to wait
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        submitInfo2.signalSemaphoreInfoCount = 1;
        submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        if (vkQueueSubmit2(queueHandles.value().presentQueue, 1, &submitInfo2, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit present commandBuffer in lighting Pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }
    }
    // Transfer command buffer submit
    if (transferCommandBufferToExecute) {

        // Submit
        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = transferCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos;
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.semaphore = reserved->transferPreviousRTStatusSyncObjs[frameInFlight];
        // Ignore because presentPreviousRTStatusSyncObjs binary
        pSignalSemaphoreInfos.value = OvVK::WhittedRTwithPBRPipeline::finishDependenciesProcessValueStatusSyncObj;
        // All command s need to wait
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        submitInfo2.signalSemaphoreInfoCount = 1;
        submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        if (vkQueueSubmit2(queueHandles.value().transferQueue, 1, &submitInfo2, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit transfer commandBuffer in lighting Pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }
    }
    // Graphic command buffer submit
    if (graphicCommandBufferToExecute) {

        // Submit
        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = graphicCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos;
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.semaphore = reserved->graphicPreviousRTStatusSyncObjs[frameInFlight];
        // Ignore because presentPreviousRTStatusSyncObjs binary
        pSignalSemaphoreInfos.value = OvVK::WhittedRTwithPBRPipeline::finishDependenciesProcessValueStatusSyncObj;
        // All command s need to wait
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        submitInfo2.signalSemaphoreInfoCount = 1;
        submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        if (vkQueueSubmit2(queueHandles.value().graphicsQueue, 1, &submitInfo2, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit transfer commandBuffer in lighting Pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }
    }

    // RT
    VkSubmitInfo2 submitInfo2RT = {};
    submitInfo2RT.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    submitInfo2RT.pNext = VK_NULL_HANDLE;
    submitInfo2RT.flags = 0x00000000;

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfosRT;
    // Copying vector by copy function�
    pWaitSemaphoreInfosRT.insert(pWaitSemaphoreInfosRT.begin(), pWaitSemaphoreInfos.begin(), pWaitSemaphoreInfos.end());

    // Set semaphore to wait on ray tracing operation
    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoRT;
    vkSemaphoreSubmitInfoRT.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfoRT.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfoRT.value = OvVK::WhittedRTwithPBRPipeline::finishDependenciesProcessValueStatusSyncObj;
    // Ray tracing need to wait the change of ownership of the resources.
    vkSemaphoreSubmitInfoRT.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfoRT.deviceIndex = 0;

    if (transferCommandBufferToExecute) {
        vkSemaphoreSubmitInfoRT.semaphore = reserved->transferPreviousRTStatusSyncObjs[frameInFlight];
        pWaitSemaphoreInfosRT.push_back(vkSemaphoreSubmitInfoRT);
    }
    if (presentCommandBufferToExecute) {
        vkSemaphoreSubmitInfoRT.semaphore = reserved->presentPreviousRTStatusSyncObjs[frameInFlight];
        pWaitSemaphoreInfosRT.push_back(vkSemaphoreSubmitInfoRT);
    }
    if (graphicCommandBufferToExecute) {
        vkSemaphoreSubmitInfoRT.semaphore = reserved->graphicPreviousRTStatusSyncObjs[frameInFlight];
        pWaitSemaphoreInfosRT.push_back(vkSemaphoreSubmitInfoRT);
    }

    submitInfo2RT.waitSemaphoreInfoCount = pWaitSemaphoreInfosRT.size();
    submitInfo2RT.pWaitSemaphoreInfos = pWaitSemaphoreInfosRT.data();

    VkSemaphoreSubmitInfo pSignalSemaphoreInfosRT = {};
    pSignalSemaphoreInfosRT.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    pSignalSemaphoreInfosRT.pNext = VK_NULL_HANDLE;
    // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
    pSignalSemaphoreInfosRT.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    pSignalSemaphoreInfosRT.deviceIndex = 0;
    // both binary semaphores
    pSignalSemaphoreInfosRT.value = OvVK::Pipeline::renderingDoneValueStatusSyncObj;
    pSignalSemaphoreInfosRT.semaphore = getStatusSyncObjGPU();

    submitInfo2RT.signalSemaphoreInfoCount = 1;
    submitInfo2RT.pSignalSemaphoreInfos = &pSignalSemaphoreInfosRT;

    // Command buffer set
    VkCommandBufferSubmitInfo pCommandBufferInfosRT;
    pCommandBufferInfosRT.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfosRT.pNext = VK_NULL_HANDLE;
    pCommandBufferInfosRT.commandBuffer = computeCommandBuffer.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfosRT.deviceMask = 0;

    submitInfo2RT.commandBufferInfoCount = 1;
    submitInfo2RT.pCommandBufferInfos = &pCommandBufferInfosRT;


    // Createa fence to measure processing time need to perform ray tracing and compute the lighting model.
    VkFence fenceToGatherProcessingTime = VK_NULL_HANDLE;

    if (getAnalysingTimeRequired()) {
        // To create fences
        VkFenceCreateInfo fenceCreateInfo = {};
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreateInfo.pNext = VK_NULL_HANDLE;
        fenceCreateInfo.flags = 0x00000000;

        if (vkCreateFence(logicalDevice.get().getVkDevice(), &fenceCreateInfo,
            NULL, &fenceToGatherProcessingTime) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the render fence to measure performance of the frame in flight number
%d of the lighting pipeline with id= %d.)", frameInFlight, getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }

    }


    // Reset sync obj CPU.
    if (vkResetFences(logicalDevice.get().getVkDevice(), 1,
        &fenceToWait) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to reset the render fence of the frame in flight
 number %d of the lighting pipeline with id= %d.)", frameInFlight, getId());
        return false;
    }

    // Submit
    if (vkQueueSubmit2(queueHandles.value().computeQueue, 1, &submitInfo2RT,
        fenceToGatherProcessingTime) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit compute commandBuffer in lighting Pipeline with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
        presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
        return false;
    }

    // If enabled, commands that perform ray tracing and calculate the lighting model are waited for.
    // This is only necessary if the time taken to process the commands submitted to the device needs to be measured.
    if (getAnalysingTimeRequired()) {
        // Wait to finish the ray tracing command only if needed to measure the processing time. 
        if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &fenceToGatherProcessingTime, VK_TRUE, UINT64_MAX) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to wait the command buffer used to process ray tracing in lighting pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }

        vkDestroyFence(logicalDevice.get().getVkDevice(), fenceToGatherProcessingTime, VK_NULL_HANDLE);
    }


    // Blit operation 
    VkSubmitInfo2 submitInfo2 = {};
    submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    submitInfo2.pNext = VK_NULL_HANDLE;
    submitInfo2.flags = 0x00000000;

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos1;
    // Copying vector by copy function�
    pWaitSemaphoreInfos1.insert(pWaitSemaphoreInfos1.begin(), pWaitSemaphoreInfos.begin(), pWaitSemaphoreInfos.end());
    pWaitSemaphoreInfos1[pWaitSemaphoreInfos1.size() - 1].value = OvVK::Pipeline::renderingDoneValueStatusSyncObj;
    // Blit operation need to wait that ray tracing is finished
    pWaitSemaphoreInfos1[pWaitSemaphoreInfos1.size() - 1].stageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    pWaitSemaphoreInfos1[pWaitSemaphoreInfos1.size() - 1].deviceIndex = 0;
    submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos1.size();
    submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos1.data();

    // Command buffer set
    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = blitCommandBuffer.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;
    submitInfo2.commandBufferInfoCount = 1;
    submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

    VkSemaphoreSubmitInfo pSignalSemaphoreInfos = {};
    pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
    // Blit command
    //VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT
    pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    pSignalSemaphoreInfos.deviceIndex = 0;
    // both binary semaphores
    pSignalSemaphoreInfos.value = 0;
    pSignalSemaphoreInfos.semaphore = getStatusSyncObjPresentation();

    submitInfo2.signalSemaphoreInfoCount = 1;
    submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

    // Submit
    if (vkQueueSubmit2(queueHandles.value().graphicsQueue, 1, &submitInfo2, getStatusSyncObjCPU()) != VK_SUCCESS) {
        OV_LOG_ERROR("Fail to submit graphic commandBuffer in lighting Pipeline with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(blitCommandBuffer.get());
        presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
        return false;
    }

    // Set some value
    swapchain.get().setSwapchainImageLayout(presentableImageIndex, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    swapchain.get().setSwapchainImageOwnership(presentableImageIndex, queueFamilyIndices.value().graphicsFamily.value());

    reserved->rtRenderImages[frameInFlight].setImageLayout(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    reserved->rtRenderImages[frameInFlight].setQueueFamilyIndex(queueFamilyIndices.value().graphicsFamily.value());

    storage.get().setDeviceBufferQueueFamilyIndex(queueFamilyIndices.value().computeFamily.value());

    //Done:
    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method to draw a gui on a windows with the ImGUI library.
 */
void OVVK_API OvVK::WhittedRTwithPBRPipeline::drawGUI()
{
    if (!ImGui::CollapsingHeader("Lighting pipeline"))
        return;

    int rayRecursionDepth = reserved->pushConstant.rayRecursionDepth;
    ImGui::SliderInt("Ray recursion depth", &rayRecursionDepth, 0, floor(getMaxRayRecursionDepth()), "%d");
    reserved->pushConstant.rayRecursionDepth = rayRecursionDepth;

    int jittering = reserved->pushConstant.nrOfAASamples;
    ImGui::SliderInt("Anti aliasing jittering", &jittering, 1, 10);
    reserved->pushConstant.nrOfAASamples = jittering;

    bool boolStatus = false;
    int intStatus = 0;

    boolStatus = (reserved->pushConstant.isHDRCorrectionEnabled == 1) ? true : false;
    ImGui::Checkbox("Enable/Disable HDR correction", &boolStatus);
    reserved->pushConstant.isHDRCorrectionEnabled = (boolStatus) ? 1 : 0;

    if (ImGui::TreeNode("Reflections"))
    {
        boolStatus = (reserved->pushConstant.useNormalMapForReflection == 1) ? true : false;
        ImGui::Checkbox("Use normal map for reflection", &boolStatus);
        reserved->pushConstant.useNormalMapForReflection = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.useNDFStandard == 1) ? true : false;
        ImGui::Checkbox("Use NDF Standard", &boolStatus);
        reserved->pushConstant.useNDFStandard = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.showProblemNDFFunction == 1) ? true : false;
        ImGui::Checkbox("Show NDF function problem", &boolStatus);
        reserved->pushConstant.showProblemNDFFunction = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.scaleReflectionInvRoughness == 1) ? true : false;
        ImGui::Checkbox("Scale reflection with inverse roughness", &boolStatus);
        reserved->pushConstant.scaleReflectionInvRoughness = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.useMultipleReflectionRays == 1) ? true : false;
        ImGui::Checkbox("Use multiple reflection rays", &boolStatus);
        reserved->pushConstant.useMultipleReflectionRays = (boolStatus) ? 1 : 0;

        intStatus = reserved->pushConstant.nrOfReflSamples;
        ImGui::SliderInt("Reflections  rays samples", &intStatus, 1, 64, "%d");
        reserved->pushConstant.nrOfReflSamples = intStatus;

        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Refractions"))
    {

        boolStatus = (reserved->pushConstant.useNormalMapForRefraction == 1) ? true : false;
        ImGui::Checkbox("Use normal map for refraction", &boolStatus);
        reserved->pushConstant.useNormalMapForRefraction = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.alwaysCalculateRefractedRays == 1) ? true : false;
        ImGui::Checkbox("Always calculate refracted rays", &boolStatus);
        reserved->pushConstant.alwaysCalculateRefractedRays = (boolStatus) ? 1 : 0;

        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Ambient occlusion"))
    {
        boolStatus = (reserved->pushConstant.isAmbientOcclusionEnabled == 1) ? true : false;
        ImGui::Checkbox("Enable/Disable AO", &boolStatus);
        reserved->pushConstant.isAmbientOcclusionEnabled = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.useFirstVariationOfAO == 1) ? true : false;
        ImGui::Checkbox("First variation of AO", &boolStatus);
        reserved->pushConstant.useFirstVariationOfAO = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.showAmbientOcclusionImage == 1) ? true : false;
        ImGui::Checkbox("Show ambient occlusion image", &boolStatus);
        reserved->pushConstant.showAmbientOcclusionImage = (boolStatus) ? 1 : 0;

        int nrOfAOsamples = reserved->pushConstant.nrOfAOSamples;
        ImGui::SliderInt("Ambient occlusion samples", &nrOfAOsamples, 1, 64, "%d");
        reserved->pushConstant.nrOfAOSamples = nrOfAOsamples;

        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Materials"))
    {
        boolStatus = (reserved->pushConstant.useAlbedoTexture == 1) ? true : false;
        ImGui::Checkbox("Use albedo texture", &boolStatus);
        reserved->pushConstant.useAlbedoTexture = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.useRoghnessTexture == 1) ? true : false;
        ImGui::Checkbox("Use roughness texture", &boolStatus);
        reserved->pushConstant.useRoghnessTexture = (boolStatus) ? 1 : 0;

        boolStatus = (reserved->pushConstant.useMetalnessTexture == 1) ? true : false;
        ImGui::Checkbox("Use metalness texture", &boolStatus);
        reserved->pushConstant.useMetalnessTexture = (boolStatus) ? 1 : 0;

        ImGui::TreePop();
    }
}

#pragma endregion

#pragma region Shader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds to the preprocessor of the Shader class the lighting pipeline include to access the info.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRPipeline::addShaderPreproc()
{

    //////////////////
    // PushConstant //
    //////////////////

    std::string pushConstantPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_PUSHCONSTANT_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_PUSHCONSTANT_INCLUDED
)";

    pushConstantPreproc += PushConstant::getShaderStruct() + "\n";

    pushConstantPreproc += R"(
#endif
)";


    ////////////////
    // HitPayload //
    ////////////////

    std::string hitPayloadPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_HITPAYLOAD_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_HITPAYLOAD_INCLUDED
)";

    hitPayloadPreproc += HitPayload::getShaderStruct() + "\n";
    hitPayloadPreproc += R"(
#endif
)";


    //////////////////////
    // OcclusionPayload //
    //////////////////////

    std::string occlusionPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_OCCLUSION_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_OCCLUSION_INCLUDED
)";

    occlusionPreproc += OcclusionPayload::getShaderStruct() + "\n";
    occlusionPreproc += R"(
#endif
)";


    ////////
    // AO //
    ////////

    std::string aoPayloadPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_PAYLOAD_AO_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_PAYLOAD_AO_INCLUDED
)";

    aoPayloadPreproc += AOPayload::getShaderStruct() + "\n";
    aoPayloadPreproc += R"(
#endif
)";

    std::string aoPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_AO_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_AO_INCLUDED
)";

    aoPreproc += AOPayload::getShaderStruct() + "\n";
    aoPreproc += R"(
/*
 * Copyright (c) 2019-2021, NVIDIA CORPORATION.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-FileCopyrightText: Copyright (c) 2019-2021 NVIDIA CORPORATION
 * SPDX-License-Identifier: Apache-2.0
 */

// offsetPositionAlongNormal shifts a point on a triangle surface so that a
// ray bouncing off the surface with tMin = 0.0 is no longer treated as
// intersecting the surface it originated from.
//
// This code uses an improved technique by Carsten W�chter and
// Nikolaus Binder from "A Fast and Robust Method for Avoiding
// Self-Intersection" from Ray Tracing Gems (verion 1.7, 2020).
// The normal can be negated if one wants the ray to pass through
// the surface instead.
vec3 OffsetPositionAlongNormal(vec3 worldPosition, vec3 normal)
{
  // Convert the normal to an integer offset.
  const float int_scale = 256.0f;
  const ivec3 of_i      = ivec3(int_scale * normal);

  // Offset each component of worldPosition using its binary representation.
  // Handle the sign bits correctly.
  const vec3 p_i = vec3(  //
      intBitsToFloat(floatBitsToInt(worldPosition.x) + ((worldPosition.x < 0) ? -of_i.x : of_i.x)),
      intBitsToFloat(floatBitsToInt(worldPosition.y) + ((worldPosition.y < 0) ? -of_i.y : of_i.y)),
      intBitsToFloat(floatBitsToInt(worldPosition.z) + ((worldPosition.z < 0) ? -of_i.z : of_i.z)));

  // Use a floating-point offset instead for points near (0,0,0), the origin.
  const float origin     = 1.0f / 32.0f;
  const float floatScale = 1.0f / 65536.0f;
  return vec3(  //
      abs(worldPosition.x) < origin ? worldPosition.x + floatScale * normal.x : p_i.x,
      abs(worldPosition.y) < origin ? worldPosition.y + floatScale * normal.y : p_i.y,
      abs(worldPosition.z) < origin ? worldPosition.z + floatScale * normal.z : p_i.z);
}

//-----------------------------------------------------------------------------
// Implementation of GetRandDir

uint rngState = gl_LaunchIDEXT.x * 2000 + gl_LaunchIDEXT.y;

// Steps the random number generator and returns a floating-point value between
// 0 and 1 inclusive.
float StepAndOutputRNGFloat()
{
  // Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
  rngState  = rngState * 747796405 + 1;
  uint word = ((rngState >> ((rngState >> 28) + 4)) ^ rngState) * 277803737;
  word      = (word >> 22) ^ word;
  return float(word) / 4294967295.0f;
}

// Gets a randomly chosen cosine-weighted direction within the unit hemisphere
// defined by the surface normal.
vec3 GetRandCosDir(vec3 norm)
{
  // To generate a cosine-weighted normal, generate a random point on a sphere:
  float theta      = 6.2831853 * StepAndOutputRNGFloat();  // Random in [0, 2pi]
  float z          = 2 * StepAndOutputRNGFloat() - 1.0;    // Random in [-1, 1]
  float r          = sqrt(1.0 - z * z);
  vec3  ptOnSphere = vec3(r * cos(theta), r * sin(theta), z);
  // Then add the normal to it and normalize to make it cosine-weighted on a
  // hemisphere:
  return normalize(ptOnSphere + norm);
}

#endif
)";


    //////////////////
    // HitAttribute //
    //////////////////

    std::string hitAttributePreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_HITATTRIBUTE_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_HITATTRIBUTE_INCLUDED
)";

    hitAttributePreproc += HitAttribute::getShaderStruct() + "\n";
    hitAttributePreproc += R"(
#endif
)";

    /////////
    // PBR //
    /////////

    std::string pbrPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_PBR_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_PBR_INCLUDED

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}

#endif
)";

    //////////////////
    // AntiAliasing //
    //////////////////

    std::string antiAlisingPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_ANTI_ALIASING_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_ANTI_ALIASING_INCLUDED

/*
 * Copyright (c) 2019-2021, NVIDIA CORPORATION.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-FileCopyrightText: Copyright (c) 2019-2021 NVIDIA CORPORATION
 * SPDX-License-Identifier: Apache-2.0
 */

// Generate a random unsigned int from two unsigned int values, using 16 pairs
// of rounds of the Tiny Encryption Algorithm. See Zafar, Olano, and Curtis,
// "GPU Random Numbers via the Tiny Encryption Algorithm"
uint tea(uint val0, uint val1)
{
  uint v0 = val0;
  uint v1 = val1;
  uint s0 = 0;

  for(uint n = 0; n < 16; n++)
  {
    s0 += 0x9e3779b9;
    v0 += ((v1 << 4) + 0xa341316c) ^ (v1 + s0) ^ ((v1 >> 5) + 0xc8013ea4);
    v1 += ((v0 << 4) + 0xad90777d) ^ (v0 + s0) ^ ((v0 >> 5) + 0x7e95761e);
  }

  return v0;
}

// Generate a random unsigned int in [0, 2^24) given the previous RNG state
// using the Numerical Recipes linear congruential generator
uint lcg(inout uint prev)
{
  uint LCG_A = 1664525u;
  uint LCG_C = 1013904223u;
  prev       = (LCG_A * prev + LCG_C);
  return prev & 0x00FFFFFF;
}

// Generate a random float in [0, 1) given the previous RNG state
float rnd(inout uint prev)
{
  return (float(lcg(prev)) / float(0x01000000));
}

#endif
)";


    ////////////////
    // StorageDSM //
    ////////////////

    std::string storageBindPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_STORAGE_DSM_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_STORAGE_DSM_INCLUDED
)";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::globalUniform)) +
        ") uniform GlobalUniform { GlobalUniformData globalUniform; };\n";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::materials)) +
        ") buffer Materials { MaterialData data[]; } materials;\n";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::lights)) +
        ") buffer Lights { LightData data[]; } lights;\n";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::rtObjDescs)) +
        ") buffer RTObjDescs { RTObjDescData data[]; } rtObjDescs;\n";

    storageBindPreproc += R"(
#endif
)";


    /////////////////
    // TexturesDSM //
    /////////////////

    std::string textureBindPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_TEXTURES_DSM_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_TEXTURES_DSM_INCLUDED
)";

    textureBindPreproc += "layout(set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::texture)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::TexturesDescriptorSetsManager::Binding::textures)) +
        ") uniform sampler2D textureSamplers[];\n";

    textureBindPreproc += R"(
#endif
 )";


    ///////////
    // RTDSM //
    ///////////

    std::string rtBindPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_RT_DSM_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_RT_DSM_INCLUDED
)";

    rtBindPreproc += "layout(set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::rt)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::RayTracingDescriptorSetsManager::Binding::tlas)) +
        ") uniform accelerationStructureEXT topLevelAS;\n";

    rtBindPreproc += R"(
#endif
)";


    /////////////////
    // LightingDSM //
    /////////////////

    std::string lightingBindPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_LIGHTING_DSM_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_LIGHTING_DSM_INCLUDED
)";

    lightingBindPreproc += "layout(set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::lighting)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRDescriptorSetsManager::Binding::outputImage)) +
        ", rgba8) uniform image2D outputImage;\n";

    lightingBindPreproc += "layout(set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::lighting)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRDescriptorSetsManager::Binding::aoImage)) +
        ", r8) uniform image2D aoImage;\n";

    lightingBindPreproc += R"(
#endif
 )";


    /////////////
    // GENERAL //
    /////////////

    std::string generalPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_RT_GENERAL_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_RT_GENERAL_INCLUDED
)";

    generalPreproc += R"(

/////////////
// #DEFINE //
/////////////

#define K_EPSILON       1e-4f               // Tolerance around zero   
#define FLT_MAX         3.402823466e+38f    // Max float value
#define UINT_MAX        4294967295          // Max uint (32 bit) value
#define INT_MAX         2147483647          // Max uint (32 bit) value
)";

    generalPreproc += R"(
#endif
)";


    /////////////
    // GENERAL //
    /////////////

    std::string collisionPointPBRPreproc = R"(
#ifndef OVVULKAN_LIGHTING_PIPELINE_RT_PBR_INCLUDED
#define OVVULKAN_LIGHTING_PIPELINE_RT_PBR_INCLUDED
)";

    collisionPointPBRPreproc += R"(
    /////////
    // PBR //
    /////////

    for(int i = 0; i < globalUniform.nrOfLights; i++)
    {
        // Gather current Light
        LightData lightData = lights.data[i];

        // Data to compute
        vec3 L = vec3(0);
        float distance = 0;
        float intensity = 0;
        
        // 1 => directional / 2 => spot / 3 =>omni
        if(lightData.type == 1)
        {
            L         = normalize(-lightData.direction);
            distance  = 100000;
            intensity = 1.0f/distance;
        }
        else if(lightData.type == 3)
        {
            vec3 lDir = lightData.position - worldPos;
            L         = normalize(lDir);
            distance  = length(lDir);

            intensity = 1.0f / ((distance > 0) ? distance*distance : 1.0f);
        }
        else if(lightData.type == 2)
        {
            vec3 lDir = lightData.position - worldPos;
            L         = normalize(lDir);
            distance  = length(lDir);

            intensity = 1.0f / ((distance > 0) ? distance*distance : 1.0f);

            // https://learnopengl.com/Lighting/Light-casters
            float theta          = dot(L , normalize(-lightData.direction));
            float outerCutoffCos = cos(lightData.spotCutOff) - lightData.spotExponent;
            float spotIntensity  = clamp((theta - outerCutoffCos) / lightData.spotExponent, 0.0, 1.0);
            intensity *= spotIntensity;
        }

        vec3 radiance = lightData.color * intensity * lightData.power;  
        vec3 H = normalize(V + L);

        // cook-torrance brdf
        float NDF = DistributionGGX(N, H, roughness);        
        float G   = GeometrySmith(N, V, L, roughness);      
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);    

        vec3 kS = F;
        kD = vec3(1.0) - kS;
        kD *= 1.0 - metalness;

        vec3 numerator    = NDF * G * F;
        // Note that we add 0.0001 to the denominator to prevent a divide by zero in case any dot product ends up 0.0. 
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001f;
        vec3 specular     = numerator / denominator;  

        // add to outgoing radiance Lo
        float NdotL = max(dot(Nv, L), 0.0);   

        ////////////
        // Shadow //
        ////////////

        prdOC.value = vec3(1.0f);
        // Shadow
        if(NdotL > 0)
        {
            // 3 time otherwise self iteraction
            vec3  origin = worldPos;
            vec3  rayDir = L;

            uint  flags  = gl_RayFlagsSkipClosestHitShaderEXT;
            float tMin   = 0.001;
            float tMax   = distance;

            prdOC.geomID =        INT_MAX;
            prdOC.instanceID =    INT_MAX;
            prdOC.primitiveID =   INT_MAX;
            prdOC.startColor = vec3(0.0f);

            traceRayEXT(topLevelAS,         // acceleration structure
                        flags,              // rayFlags
                        0xFF,               // cullMask
                        2,                  // sbtRecordOffset
                        $sbtRecordStride$,  // sbtRecordStride
                        1,                  // missIndex
                        origin,             // ray origin
                        tMin,               // ray min range
                        rayDir,             // ray direction
                        tMax,               // ray max range
                        1                   // payload (location = 1)
            );
        }
        
        Lo += (kD * (albedo / PI) * material.opacity + specular) * radiance * ((isInside) ? 1.0f : NdotL) * prdOC.value;
    }
)";

    collisionPointPBRPreproc += R"(
#endif
)";


    std::string lightingPipelineDescSetsPreproc = pushConstantPreproc + hitPayloadPreproc + occlusionPreproc +
        aoPayloadPreproc + hitAttributePreproc + generalPreproc;

    std::string allLightingPipelineDescSetsPreproc = pushConstantPreproc + hitPayloadPreproc + occlusionPreproc +
        aoPreproc + hitAttributePreproc + pbrPreproc + antiAlisingPreproc + storageBindPreproc + textureBindPreproc +
        rtBindPreproc + lightingBindPreproc + generalPreproc;

    return OvVK::Shader::preprocessor.set("include LightingPipeline", allLightingPipelineDescSetsPreproc) && 
        OvVK::Shader::preprocessor.set("include PayloadsStructsLightingPipeline", lightingPipelineDescSetsPreproc) &&
        OvVK::Shader::preprocessor.set("include PBRLightingPipeline", collisionPointPBRPreproc);
}

#pragma endregion