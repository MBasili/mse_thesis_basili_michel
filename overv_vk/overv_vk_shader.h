/**
 * @file	overv_vk_shader.h
 * @brief	Vulkan generic shader properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling an Vulkan shader.
  */
class OVVK_API Shader : public Ov::Shader
{
//////////
public: //
//////////

	// Consts:
	static constexpr char glslVersion[] = "#version 460\n#extension GL_EXT_ray_tracing : require\n#extension GL_EXT_nonuniform_qualifier : enable\n#extension GL_EXT_scalar_block_layout : enable\n#extension GL_EXT_shader_explicit_arithmetic_types_int32 : require\n#extension GL_EXT_shader_explicit_arithmetic_types_int64 : require\n#extension GL_EXT_buffer_reference2 : require\n";   ///< This is the first line of all the shaders used
	static constexpr uint32_t maxLogSize = 8192; ///< Max shader compiler log size in bytes

	// Special value
	static Shader empty;
	static Ov::Preprocessor preprocessor;        ///< This preprocessor is always used on every shader

	// Const/dest:
	Shader();
	Shader(Shader&& other);
	Shader(Shader const&) = delete;
	virtual ~Shader();

	// Operators
	Shader& operator=(const Shader&) = delete;
	Shader& operator=(Shader&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	bool load(Type kind, const std::string& code);

	// Get/Set
	std::vector<uint32_t>& getSPIRVBinary() const;
	uint32_t getNrOfBytes() const;

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const;

/////////////
protected: //
/////////////   

   // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Shader(const std::string& name);
};