/**
 * @file	overv_vk_image_view.h
 * @brief	Vulkan image view
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


/////////////
// #STATIC //
/////////////

// Special values:
OvVK::ImageView OvVK::ImageView::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Image reserved structure.
 */
struct OvVK::ImageView::Reserved
{
    std::reference_wrapper<const OvVK::Image> image;    ///< Reference to the image on which the view will be created.

    VkImageView imageView;                                  ///< Vulkan image view handle
   
    VkImageViewCreateFlags imageViewCreateFlags;            ///< Specifying additional parameters
    VkImageViewType imageViewType;                          ///< Specifying the type
    VkFormat format;                                        ///< Specifying the format and type used to interpret texel blocks of the image.
    VkComponentMapping componentMapping;                    ///< Specifying a remapping of color components
    VkImageSubresourceRange imageSubresourceRange;          ///< Selecting the set of mipmap levels and array layers to be accessible to the view.

    /**
     * Constructor.
     */
    Reserved() : image{ OvVK::Image::empty },
        imageView{ VK_NULL_HANDLE },
        imageViewCreateFlags{ 0x00000000 },
        imageViewType{ VK_IMAGE_VIEW_TYPE_MAX_ENUM },
        format{ VK_FORMAT_UNDEFINED },
        componentMapping{ {} },
        imageSubresourceRange{ {} }
    {}
};


/////////////////////////////
// BODY OF CLASS ImageView //
/////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::ImageView::ImageView() : reserved(std::make_unique<OvVK::ImageView::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::ImageView::ImageView(const std::string& name) : Ov::Object(name),
reserved(std::make_unique<OvVK::ImageView::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::ImageView::ImageView(ImageView&& other) noexcept : Ov::Object(std::move(other)),
Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::ImageView::~ImageView()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize image
 * @return TF
 */
bool OVVK_API OvVK::ImageView::init()
{
    // Default initialization method
    if (this->Ov::Managed::init() == false)
        return false;


    if (reserved->imageView != VK_NULL_HANDLE) {

        // Destroy imageView
        vkDestroyImageView(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->imageView, NULL);
        reserved->imageView = VK_NULL_HANDLE;

        reserved->image = OvVK::Image::empty;
        reserved->imageView = VK_NULL_HANDLE;
        reserved->imageViewCreateFlags = 0x00000000;
        reserved->imageViewType = VK_IMAGE_VIEW_TYPE_MAX_ENUM;
        reserved->format = VK_FORMAT_UNDEFINED;
        reserved->componentMapping = {};
        reserved->imageSubresourceRange = {};
    }


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free image.
 * @return TF
 */
bool OVVK_API OvVK::ImageView::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    if (reserved->imageView != VK_NULL_HANDLE) {

        // Destroy imageView
        vkDestroyImageView(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->imageView, NULL);
        reserved->imageView = VK_NULL_HANDLE;

        reserved->image = OvVK::Image::empty;
        reserved->imageView = VK_NULL_HANDLE;
        reserved->imageViewCreateFlags = 0x00000000;
        reserved->imageViewType = VK_IMAGE_VIEW_TYPE_MAX_ENUM;
        reserved->format = VK_FORMAT_UNDEFINED;
        reserved->componentMapping = {};
        reserved->imageSubresourceRange = {};
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the Vulkan handle image view.
 * return The Vulkan handle image view.
 */
VkImageView OVVK_API OvVK::ImageView::getVkImageView() const 
{
    return reserved->imageView;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the flags used to create the image view.
 * return The flags used to create the image view.
 */
VkImageViewCreateFlags OVVK_API OvVK::ImageView::getCreateFlags() const
{
    return reserved->imageViewCreateFlags;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the type of the image view.
 * return The type of the image view.
 */
VkImageViewType OVVK_API OvVK::ImageView::getType() const 
{
    return reserved->imageViewType;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the format and type used to interpret texel blocks of the image.
 * return The format and type used to interpret texel blocks of the image.
 */
VkFormat OVVK_API OvVK::ImageView::getFormat() const 
{
    return reserved->format;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the component mapping.
 * return The component mapping.
 */
VkComponentMapping OVVK_API OvVK::ImageView::getComponentMapping() const 
{
    return reserved->componentMapping;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the image subresource range selecting the set of mipmap levels and array layers to be accessible to the view.
 * return The image subresource range.
 */
VkImageSubresourceRange OVVK_API OvVK::ImageView::getSubresourceRange() const 
{
    return reserved->imageSubresourceRange;
}

#pragma endregion

#pragma region Create

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create a image view.
 * return TF
 */
bool OVVK_API OvVK::ImageView::create(OvVK::Image& image, VkImageViewCreateInfo vkImageViewCreateInfo)
{
    // Init
    if (init() == false)
        return false;

    if (vkCreateImageView(Engine::getInstance().getLogicalDevice().getVkDevice(), &vkImageViewCreateInfo, NULL,
        &reserved->imageView) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create image view with id= %d", getId());
        return false;
    }

    reserved->image = image;
    reserved->imageViewCreateFlags = vkImageViewCreateInfo.flags;
    reserved->imageViewType = vkImageViewCreateInfo.viewType;
    reserved->format = vkImageViewCreateInfo.format;
    reserved->componentMapping = vkImageViewCreateInfo.components;
    reserved->imageSubresourceRange = vkImageViewCreateInfo.subresourceRange;

    // Done:
    return true;
}

#pragma endregion
