/**
 * @file	overv_vk_surface.h
 * @brief	Vulkan surface properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

// declaration of the class that rapresent a surface dependent object.
class SurfaceDependentObject;

/**
  * @brief Class for manage Vulkan surface
  */
class OVVK_API Surface final : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Special values;
	static Surface empty;

	/**
	 * @brief SurfaceCallbackTypes types of a surface.
	 */
	enum class SurfaceCallbackTypes : uint32_t {
		none = 0,
		keyboard = 1,
		mouseCursor = 2,
		mouseButton = 4,
		mouseScroll = 8,
		windowSize = 16
	};

	// SurfaceCallbackTypes signatures:
	typedef void (*KeyboardCallback)   (int key, int scancode, int action, int mods);
	typedef void (*MouseCursorCallback)(double mouseX, double mouseY);
	typedef void (*MouseButtonCallback)(int button, int action, int mods);
	typedef void (*MouseScrollCallback)(double scrollX, double scrollY);
	typedef void (*WindowSizeCallback) (int width, int height);

	// Const/dest:
	Surface();
	Surface(Surface&& other);
	Surface(Surface const&) = delete;
	virtual ~Surface();

	// Operators
	Surface& operator=(const Surface&) = delete;
	Surface& operator=(Surface&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// SurfaceCallbackTypes
	bool setKeyboardCallback(KeyboardCallback cb);
	bool setMouseCursorCallback(MouseCursorCallback cb);
	bool setMouseButtonCallback(MouseButtonCallback cb);
	bool setMouseScrollCallback(MouseScrollCallback cb);
	bool setWindowSizeCallback(WindowSizeCallback cb);

	// General:
	bool create(std::string title, uint32_t width, uint32_t height);
	bool isClosed() const;
	bool isReady() const;
	bool isMinimize() const;
	bool queryKey(int keyCode) const;
	bool addSurfaceDependentObject(uint32_t id, SurfaceDependentObject& object);
	bool removeSurfaceDependentObject(uint32_t id);

	// Get/Set:
	void* getGLFWWindow() const;
	uint32_t getWidth() const;
	uint32_t getHeight() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkSurfaceKHR getVkSurfaceKHR() const;
#endif

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// SurfaceCallbackTypes:
	void keyboardCallback(int key, int scancode, int action, int mods);
	void mouseCursorCallback(double mouseX, double mouseY);
	void mouseButtonCallback(int button, int action, int mods);
	void mouseScrollCallback(double scrollX, double scrollY);
	void windowSizeCallback(int _width, int _height);

	// Const/dest:
	Surface(const std::string& name);
};

// overloaded bitwise operators
inline Surface::SurfaceCallbackTypes operator|(
	Surface::SurfaceCallbackTypes lhs, Surface::SurfaceCallbackTypes rhs) {
	return static_cast<Surface::SurfaceCallbackTypes>(
		static_cast<std::underlying_type_t<Surface::SurfaceCallbackTypes>>(lhs) |
		static_cast<std::underlying_type_t<Surface::SurfaceCallbackTypes>>(rhs)
		);
}
inline Surface::SurfaceCallbackTypes operator&(
	Surface::SurfaceCallbackTypes lhs, Surface::SurfaceCallbackTypes rhs) {
	return static_cast<Surface::SurfaceCallbackTypes>(
		static_cast<std::underlying_type_t<Surface::SurfaceCallbackTypes>>(lhs) &
		static_cast<std::underlying_type_t<Surface::SurfaceCallbackTypes>>(rhs)
		);
}
