#pragma once
/**
 * @file	overv_vk_storage.h
 * @brief	Vulkan GPU storage
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

class OVVK_API Storage final : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

	// Static special value
	static Storage empty;

	// Sync obj value:
	constexpr static uint64_t defaultValueStatusSyncObj = 0;
	constexpr static uint64_t uploadingValueStatusSyncObj = 1;
	constexpr static uint64_t readyValueStatusSyncObj = 2;

	/**
	 * @brief Types of obj stored.
	 */
	enum class StoredType : uint32_t
   {
	  none = 0,
	  globalUniform = 1,
	  lights = 2,
	  materials = 3,
	  rtObjDescs = 4,
	  all,
   };

	// Uniform buffer set at each frame
	struct GlobalUniformData
	{
		glm::mat4 viewProj;     // Camera view * projection
		// ...64 bytes
		glm::mat4 viewInverse;  // Camera inverse view matrix
		// ...64 bytes
		glm::mat4 projInverse;  // Camera inverse projection matrix
		// ...64 bytes

		uint32_t nrOfLights;

		GlobalUniformData() : viewProj{ 1.0f },
			viewInverse{ 1.0f },
			projInverse{ 1.0f },
			nrOfLights{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct GlobalUniformData
{
	mat4 viewProj;
	mat4 viewInverse;    
	mat4 projInverse;
	uint32_t nrOfLights;
};
)";
		}
	};

	// Light
	struct LightData {

		glm::vec3 position;
		uint32_t type;
		glm::vec3 color;
		float power;
		glm::vec3 direction;
		float spotCutOff;
		float spotExponent;

		/**
		 * Constructor.
		*/
		LightData() : position{ 0.0f, 0.0f, 0.0f },
			type{ 0 },
			color{ 1.0f },
			power{ 0.0f },
			direction{ 0.0f },
			spotCutOff{ 0.0f },
			spotExponent{ 0.0f }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct LightData
{
	vec3 position;
	uint32_t type;
	vec3 color;
	float power;
	vec3 direction;
	float spotCutOff;
	float spotExponent;
};
)";
		}
	};

	// Material
	struct MaterialData {
		// Keep these vars first and in this order...:
		glm::vec3 emission;                                   ///< Emissive term
		float opacity;                                        ///< Transparency (1 = solid, 0 = invisible)
		glm::vec3 albedo;                                     ///< Albedo color
		float roughness;                                      ///< Roughness   
		float metalness;                                      ///< Metalness
		float refractionIndex;                                ///< Refraction Index
		float rimIntensity;                                   ///< Rim lighting intensity
		float absorptionThreshold;                            ///< AbsorptionThreshold  

		uint32_t albedoTexture;
		uint32_t normalTexture;
		uint32_t heightTexture;
		uint32_t roughnessTexture;
		uint32_t metalnessTexture;

		/**
		 * Constructor.
		 */
		MaterialData() noexcept : emission{ 0.0f },
			opacity{ 1.0f },
			albedo{ 0.6f },
			roughness{ 0.5f },
			metalness{ 0.01f },
			refractionIndex{ 1.0f },
			rimIntensity{ 0.7f },
			absorptionThreshold{ 0.0f },
			albedoTexture{ 0 },
			normalTexture{ 0 },
			heightTexture{ 0 },
			roughnessTexture{ 0 },
			metalnessTexture{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct MaterialData
{
	vec3 emission;
	float opacity;
	vec3 albedo;
	float roughness;
	float metalness;
	float refractionIndex;
	float rimIntensity;
	float absorptionThreshold;

	uint32_t albedoTexture;
	uint32_t normalTexture;
	uint32_t heightTexture;
	uint32_t roughnessTexture;
	uint32_t metalnessTexture;
};
)";
		}
	};

	// Information of a ray tracing obj model when referenced in a shader
	struct RTObjDescData
	{
		uint64_t geometryDataAddress;         // Address of the Geometry Data

		RTObjDescData() : geometryDataAddress{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct RTObjDescData
{
	uint64_t geometryDataAddress; 
};
)";
		}
	};

	// Const/Dest:
	Storage();
	Storage(Storage&& other);
	Storage(Storage const&) = delete;
	virtual ~Storage();

	// Operators
	Storage& operator=(const Storage&) = delete;
	Storage& operator=(Storage&&) = delete;

	// Init/free:
	bool init() override;
	bool free() override;

	// Synch data:
	bool isLock() const;
	bool isOwned() const;

	// General
	bool create();
	bool addToUpdateList(const Ov::Renderable& renderable);

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool upload(VkFence signalFence = VK_NULL_HANDLE,
		std::initializer_list<VkSemaphore> waitSemaphores = {},
		std::initializer_list<uint64_t> waitSemaphoresValues = {},
		std::initializer_list<VkSemaphore> signalSemaphores = {},
		std::initializer_list<uint64_t> signalSemaphoresValues = {});
#endif

	// Get/set:
	const OvVK::Buffer &getGlobalUniformHostBuffer() const;
	const OvVK::Buffer &getGlobalUniformDeviceBuffer() const;
	OvVK::Storage::GlobalUniformData *getGlobalUniformData();

	const OvVK::Buffer& getLightsHostBuffer() const;
	const OvVK::Buffer& getLightsDeviceBuffer() const;
	OvVK::Storage::LightData* getLightData(uint32_t id);

	const OvVK::Buffer &getMaterialsHostBuffer() const;
	const OvVK::Buffer &getMaterialsDeviceBuffer() const;
	OvVK::Storage::MaterialData *getMaterialData(uint32_t id);

	const OvVK::Buffer &getRTObjDescsHostBuffer() const;
	const OvVK::Buffer &getRTObjDescsDeviceBuffer() const;
	OvVK::Storage::RTObjDescData *getRTObjDescData(uint32_t id);

	uint32_t getNrOfInstances(OvVK::Storage::StoredType type) const;

	OvVK::StorageDescriptorSetsManager& getStorageDescriptorSetsManager() const;
	OvVK::CommandBuffer& getSecondaryTransferCB() const;

	bool setNrOfInstances(uint32_t nrOfInstances, OvVK::Storage::StoredType type);


#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool setDeviceBufferQueueFamilyIndex(uint32_t queueFamilyIndex);
	bool setDeviceBufferStatusSyncObjValue(uint64_t value);
	bool setDeviceBufferLockSyncObj(VkFence fence);
#endif

	// Rendering:
	bool render(uint32_t value = 0, void *data = nullptr) const;

	// Shader methods
	static bool addShaderPreproc();

///////////
private: //
///////////	  

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Storage(const std::string& name);
};