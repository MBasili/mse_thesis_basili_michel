#pragma once
/**
 * @file	overv_vk_thread_pool.h
 * @brief	ThreadPool to manage multiple threads
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/**
 * @brief Class for represent a threadPool inside the overv_vulkan engine.
 */
class OVVK_API ThreadPool : public Ov::Object
{
//////////
public: //
//////////

	// Static
	static ThreadPool empty;

	// Const/dest:
	ThreadPool();
	ThreadPool(ThreadPool&& other) noexcept;
	ThreadPool(ThreadPool const&) = delete;
	virtual ~ThreadPool();

	// Operators
	ThreadPool& operator=(const ThreadPool&) = delete;
	ThreadPool& operator=(ThreadPool&&) = delete;

	// General:
	void wait();

	// Get/Set
	std::vector<OvVK::Thread>& getThreads() const;
	uint32_t getThreadCount() const;

	void setThreadCount(uint32_t count);

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	ThreadPool(const std::string& name);
};