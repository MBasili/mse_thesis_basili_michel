/**
 * @file	overv_vk_swapchain.cpp
 * @brief	Vulkan swapchain
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"

// C/C++
#include <algorithm>


////////////
// STATIC //
////////////

// Special values
OvVK::Swapchain OvVK::Swapchain::empty("empty");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Class Swapchain reserved structure.
 */
struct OvVK::Swapchain::Reserved
{
    VkSwapchainKHR swapchain;                                               ///< Vulkan swapchain handler.
    VkFormat swapchainImageFormat;                                          ///< Format chosen for the swap chain images.
    VkExtent2D swapchainExtent;                                             ///< Extent chosen for the swap chain images.

    std::vector<VkImage> swapchainImages;                                   ///< Handles of the VkImages in swap chain.
    std::vector<VkImageView> swapchainImageViews;                           ///< Handles of the VkImageViews in swap chain.
    std::vector<std::optional<uint32_t>> swapchainImagesQueueFamilyIndex;   ///< Queue family index owner of the image.
    std::vector<VkImageLayout> swapchainImagesLayouts;                      ///< Layouts of the images in the swapchain. 
    std::vector<VkSemaphore> imagesAvailableSyncObjsGPU;                    ///< GPU sync objs to know if the swapchain images are available to use.
    std::vector<VkFence> imagesAvailableSyncObjsCPU;                        ///< CPU sync objs to know if the swapchain images are available to use.
    std::vector<VkSemaphore> imagesPresentationSyncObjsGPU;                 ///< GPU sync objs to know if the swapchain images are ready to be presentend.
    std::vector<VkFence> imagesPresentationSyncObjsCPU;                     ///< CPU sync objs to know if the swapchain images are ready to be presentend.
    std::vector<VkSemaphore> imagesOwnershipSyncObjsGPU;                    ///< GPU sync objs to know if need to wait change of ownership before presentation.

    bool isResizing;                                                        ///< Check if the swapchain is resizing.

    /**
     * Constructor.
     */
    Reserved() : swapchain{ VK_NULL_HANDLE },
        swapchainImageFormat{ VK_FORMAT_UNDEFINED },
        swapchainExtent{ {} },
        isResizing{ false }
    {}
};


/////////////////////////////
// BODY OF CLASS Swapchain //
/////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Swapchain::Swapchain() :
    reserved(std::make_unique<OvVK::Swapchain::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::Swapchain::Swapchain(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::Swapchain::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Swapchain::Swapchain(Swapchain&& other) : Ov::Object(std::move(other)),
    Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Swapchain::~Swapchain()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes swapchain
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::init()
{

    if (this->Ov::Managed::init() == false)
        return false;


    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Destroy swapchain images view
    if (reserved->swapchainImageViews.size() > 0)
    {
        for (VkImageView view : reserved->swapchainImageViews)
            vkDestroyImageView(logicalDevice, view, NULL);

        reserved->swapchainImageViews.clear();
    }

    // Destroy swapchain availability semaphore.
    if (reserved->imagesAvailableSyncObjsGPU.size() > 0)
    {
        for (VkSemaphore availability : reserved->imagesAvailableSyncObjsGPU)
            vkDestroySemaphore(logicalDevice, availability, NULL);

        reserved->imagesAvailableSyncObjsGPU.clear();
    }

    // Destroy swapchain availability fence.
    if (reserved->imagesAvailableSyncObjsCPU.size() > 0)
    {
        for (VkFence availability : reserved->imagesAvailableSyncObjsCPU)
            vkDestroyFence(logicalDevice, availability, NULL);

        reserved->imagesAvailableSyncObjsCPU.clear();
    }

    // Destroy swapchain images present semaphore.
    if (reserved->imagesPresentationSyncObjsGPU.size() > 0)
    {
        for (VkSemaphore readyToPresent : reserved->imagesPresentationSyncObjsGPU)
            vkDestroySemaphore(logicalDevice, readyToPresent, NULL);

        reserved->imagesPresentationSyncObjsGPU.clear();
    }

    // Destroy swapchain images present fence.
    if (reserved->imagesPresentationSyncObjsCPU.size() > 0)
    {
        for (VkFence readyToPresent : reserved->imagesPresentationSyncObjsCPU)
            vkDestroyFence(logicalDevice, readyToPresent, NULL);

        reserved->imagesPresentationSyncObjsCPU.clear();
    }

    // Destroy swapchain ownershipRelease semaphore.
    if (reserved->imagesOwnershipSyncObjsGPU.size() > 0)
    {
        for (VkSemaphore ownershipRelease : reserved->imagesOwnershipSyncObjsGPU)
            vkDestroySemaphore(logicalDevice, ownershipRelease, NULL);

        reserved->imagesOwnershipSyncObjsGPU.clear();
    }

    // Destroy swapchain
    if (reserved->swapchain != VK_NULL_HANDLE)
    {
        // Destroy swapchain images
        vkDestroySwapchainKHR(logicalDevice, reserved->swapchain, nullptr);
        reserved->swapchain = VK_NULL_HANDLE;
        reserved->swapchainImages.clear();
    }

    // Remove call back function for surface resize
    if (!reserved->isResizing && getIsActive()) 
    {
        Engine::getInstance().getPresentSurface().removeSurfaceDependentObject(getId());
        setIsActive(false);
    }


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases swapchain
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::free()
{

    if (this->Ov::Managed::free() == false)
        return false;

    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Destroy swapchain images view
    if (reserved->swapchainImageViews.size() > 0)
    {
        for (VkImageView view : reserved->swapchainImageViews)
            vkDestroyImageView(logicalDevice, view, NULL);

        reserved->swapchainImageViews.clear();
    }

    // Destroy swapchain availability semaphore.
    if (reserved->imagesAvailableSyncObjsGPU.size() > 0)
    {
        for (VkSemaphore availability : reserved->imagesAvailableSyncObjsGPU)
            vkDestroySemaphore(logicalDevice, availability, NULL);

        reserved->imagesAvailableSyncObjsGPU.clear();
    }

    // Destroy swapchain availability fence.
    if (reserved->imagesAvailableSyncObjsCPU.size() > 0)
    {
        for (VkFence availability : reserved->imagesAvailableSyncObjsCPU)
            vkDestroyFence(logicalDevice, availability, NULL);

        reserved->imagesAvailableSyncObjsCPU.clear();
    }

    // Destroy swapchain images present semaphore.
    if (reserved->imagesPresentationSyncObjsGPU.size() > 0)
    {
        for (VkSemaphore readyToPresent : reserved->imagesPresentationSyncObjsGPU)
            vkDestroySemaphore(logicalDevice, readyToPresent, NULL);

        reserved->imagesPresentationSyncObjsGPU.clear();
    }

    // Destroy swapchain images present fence.
    if (reserved->imagesPresentationSyncObjsCPU.size() > 0)
    {
        for (VkFence readyToPresent : reserved->imagesPresentationSyncObjsCPU)
            vkDestroyFence(logicalDevice, readyToPresent, NULL);

        reserved->imagesPresentationSyncObjsCPU.clear();
    }

    // Destroy swapchain ownershipRelease semaphore.
    if (reserved->imagesOwnershipSyncObjsGPU.size() > 0)
    {
        for (VkSemaphore ownershipRelease : reserved->imagesOwnershipSyncObjsGPU)
            vkDestroySemaphore(logicalDevice, ownershipRelease, NULL);

        reserved->imagesOwnershipSyncObjsGPU.clear();
    }

    // Destroy swapchain
    if (reserved->swapchain != VK_NULL_HANDLE)
    {
        // Destroy swapchain images
        vkDestroySwapchainKHR(logicalDevice, reserved->swapchain, nullptr);
        reserved->swapchain = VK_NULL_HANDLE;
        reserved->swapchainImages.clear();
    }

    // Remove call back function for surface resize
    if (!reserved->isResizing && getIsActive())
    {
        Engine::getInstance().getPresentSurface().removeSurfaceDependentObject(getId());
        setIsActive(false);
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region SurfaceDependentObject

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the window size callback is forwarded for surface dependent obj.
 * @param width The width of the window.
 * @param height The height of the window.
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::windowSizeCallback(int width, int height)
{
    reserved->isResizing = true;

    if (this->free() == false)
        return false;

    if (this->create(width, height) == false)
        return false;

    reserved->isResizing = false;

    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates swapchain.
 * @param width The width of the image of the swapchain.
 * @param height The height of the image of the swapchain.
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::create(uint32_t width, uint32_t height)
{
    // Safety net
    if (width <= 0 || height <= 0) 
    {
        OV_LOG_INFO("The passed width or height is 0 in swapchain with id= %d.", getId());
        return false;
    }

    if (!this->init())
        return false;

    
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice
        = engine.get().getLogicalDevice();
    // Surface
    std::reference_wrapper<OvVK::Surface> presentSurface
        = engine.get().getPresentSurface();


    //////////////////////
    // Create Swapchain //
    //////////////////////

#pragma region SwapChain

    // Choose the settings with help functions
    std::optional<OvVK::PhysicalDevice::SwapChainSupportedParameters> swapChainSupport 
        = logicalDevice.get().getPhysicalDevice().getSwapChainSupportInfo(presentSurface);

    if (swapChainSupport.has_value() == false)
        return false;


    VkSurfaceFormatKHR surfaceFormat = rateSwapchainSurfaceFormat(swapChainSupport.value().formats)[0].second;
    VkPresentModeKHR presentMode = rateSwapchainPresentMode(swapChainSupport.value().presentModes)[0].second;
    VkExtent2D extent = chooseSwapchainExtent(swapChainSupport.value().capabilities, width, height);
    VkCompositeAlphaFlagBitsKHR compositeAlpha = chooseSwapchainSurfaceCompositeAlpha(
        swapChainSupport.value().capabilities.supportedCompositeAlpha);

    // Sure to not exceed the maximum number of images while doing this, where 0 is 
    // a special value that means that there is no maximum
    uint32_t imageCount = std::max(swapChainSupport.value().capabilities.minImageCount, std::min(OvVK::Engine::nrFramesInFlight,
                swapChainSupport.value().capabilities.maxImageCount));

    // Creating the swap chain object requires filling in a large structur
    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    // which surface the swap chain should be tied to
    createInfo.surface = presentSurface.get().getVkSurfaceKHR();
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    // The imageArrayLayers specifies the amount of layers each image consists of.
    createInfo.imageArrayLayers = 1;
    // The imageUsage bit field specifies what kind of operations we'll use the images in the swap chain for.
    // It is also possible that you'll render images to a separate image first to perform operations like post-processing.
    // In that case you may use a value like VK_IMAGE_USAGE_TRANSFER_DST_BIT instead and use a memory operation
    // to transfer the rendered image to a swap chain image
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

    // Specify how to handle swap chain images that will be used across multiple queue families.
    // That will be the case in our application if the graphics queue family is different from 
    // the presentation queue. We'll be drawing on the images in the swap chain from the graphics
    // queue and then submitting them on the presentation queue.
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

    // Queue family
    createInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    createInfo.queueFamilyIndexCount = 0;

    // We can specify that a certain transform should be applied to images in the swap chain
    // if it is supported (supportedTransforms in capabilities), like a 90 degree clockwise rotation or horizontal flip.
    // To specify that you do not want any transformation, simply specify the current transformation.
    createInfo.preTransform = swapChainSupport.value().capabilities.currentTransform;

    // The compositeAlpha field specifies if the alpha channel should be used for blending with other windows in
    // the window system. You'll almost always want to simply ignore the alpha channel, hence
    // VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR.
    createInfo.presentMode = presentMode;

    // If the clipped member is set to VK_TRUE then that means that we don't care about the color
    // of pixels that are obscured, for example because another window is in front of them. 
    // Unless you really need to be able to read these pixels back and get predictable results,
    // you'll get the best performance by enabling clipping.
    createInfo.clipped = VK_TRUE;

    // With Vulkan it's possible that your swap chain becomes invalid or unoptimized while your application is running,
    // for example because the window was resized. In that case the swap chain actually needs to be recreated from
    // scratch and a reference to the old one must be specified in this field.
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    // compositeAlpha is a VkCompositeAlphaFlagBitsKHR value indicating the alpha compositing mode to use when this
    // surface is composited together with other surfaces on certain window systems.
    createInfo.compositeAlpha = compositeAlpha;

    // Creating the swap chain
    // The parameters are the logical device, swap chain creation info, optional custom allocators
    // and a pointer to the variable to store the handle in.
    if (vkCreateSwapchainKHR(logicalDevice.get().getVkDevice(), &createInfo, nullptr, &reserved->swapchain) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create the swapchain with id= %d", getId());
        this->free();
        return false;
    }

    reserved->swapchainImageFormat = surfaceFormat.format;
    reserved->swapchainExtent = extent;

    // Remember that we only specified a minimum number of images in the swap chain, 
    // so the implementation is allowed to create a swap chain with more.
    // That's why we'll first query the final number of images with vkGetSwapchainImagesKHR,
    // then resize the container and finally call it again to retrieve the handles.
    if (vkGetSwapchainImagesKHR(logicalDevice.get().getVkDevice(), reserved->swapchain, &imageCount, nullptr) != VK_SUCCESS) 
    {
        OV_LOG_INFO("Fail to retrieve the number of swapchain's images in swapchain with id= %d.", getId());
        this->free();
        return false;
    }

    // Get swapchain's images.
    reserved->swapchainImages.resize(imageCount);
    if (vkGetSwapchainImagesKHR(logicalDevice.get().getVkDevice(), reserved->swapchain, &imageCount,
        reserved->swapchainImages.data()) != VK_SUCCESS)
    {
        OV_LOG_INFO("Fail to retrieve the swapchain's images in swapchain with id= %d.", getId());
        this->free();
        return false;
    }

#pragma endregion


    ///////////////////////////////////
    // Create Swapchain images views //
    ///////////////////////////////////

#pragma region ImageViews

    reserved->swapchainImageViews.resize(imageCount);

    // Create images view for the swapchainn images
    for (uint32_t c = 0; c < reserved->swapchainImages.size(); c++) {
        VkImageViewCreateInfo imageViewCreateInfo = {};
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = reserved->swapchainImages[c];
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = reserved->swapchainImageFormat;
        imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        if (vkCreateImageView(logicalDevice.get().getVkDevice(), &imageViewCreateInfo, NULL,
            &reserved->swapchainImageViews[c]) != VK_SUCCESS) {
            OV_LOG_ERROR("Failed to create image view for the swapchain's images in swapchain with id= %d.", c);
            this->free();
            return false;
        }
    }

#pragma endregion


    /////////////////////
    // Create Sync obj //
    /////////////////////

#pragma region SyncObjs

    // Sync obj
    reserved->imagesAvailableSyncObjsGPU.resize((imageCount + 1), VK_NULL_HANDLE);
    reserved->imagesAvailableSyncObjsCPU.resize((imageCount + 1), VK_NULL_HANDLE);
    reserved->imagesPresentationSyncObjsGPU.resize(imageCount, VK_NULL_HANDLE);
    reserved->imagesPresentationSyncObjsCPU.resize(imageCount, VK_NULL_HANDLE);
    reserved->imagesOwnershipSyncObjsGPU.resize(imageCount, VK_NULL_HANDLE);

    // Create new timeline semaphore and delete previous one.
    VkSemaphoreTypeCreateInfo timelineCreateInfo = {};
    timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    timelineCreateInfo.pNext = VK_NULL_HANDLE;
    timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    timelineCreateInfo.initialValue = OvVK::RTScene::defaultValueStatusSyncObj;

    VkSemaphoreCreateInfo semaphoreInfoTimeline = {};
    semaphoreInfoTimeline.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfoTimeline.pNext = &timelineCreateInfo;
    semaphoreInfoTimeline.flags = 0;

    VkSemaphoreTypeCreateInfo ggg = {};
    ggg.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    ggg.pNext = VK_NULL_HANDLE;
    ggg.semaphoreType = VK_SEMAPHORE_TYPE_BINARY;
    ggg.initialValue = 0;

    VkSemaphoreCreateInfo semaphoreInfoBinary = {};
    semaphoreInfoBinary.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfoBinary.pNext = &ggg;
    semaphoreInfoBinary.flags = 0;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    // Image Availability sync obj
    // We create an extra semaphore to use during acquiring next image.
    // We know the index of an image only after the acquisition.
    for (uint32_t c = 0; c <= imageCount; c++)
    {
        // Create SyncObj
        if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfoBinary, NULL,
            &reserved->imagesAvailableSyncObjsGPU[c]) != VK_SUCCESS ||
            vkCreateFence(logicalDevice.get().getVkDevice(), &fenceInfo, NULL,
            &reserved->imagesAvailableSyncObjsCPU[c]) != VK_SUCCESS) {
            OV_LOG_ERROR("Failed to create image Availability sync objs for image with id= %d in swapchain with id= %d.",
                c, getId());
            this->free();
            return false;
        }

        if (c < imageCount) 
        {
            if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfoBinary, NULL,
                &reserved->imagesPresentationSyncObjsGPU[c]) != VK_SUCCESS ||
                vkCreateFence(logicalDevice.get().getVkDevice(), &fenceInfo, NULL,
                    &reserved->imagesPresentationSyncObjsCPU[c]) != VK_SUCCESS)
            {
                OV_LOG_ERROR("Failed to create image presentation sync obj for image with id= %d in swapchain with id= %d.",
                    c, getId());
                this->free();
                return false;
            }

            if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfoTimeline, NULL,
                &reserved->imagesOwnershipSyncObjsGPU[c]) != VK_SUCCESS)
            {
                OV_LOG_ERROR("Failed to create image ownership sync obj for image with id= %d in swapchain with id= %D.",
                    c, getId());
                this->free();
                return false;
            }
        }
    }

#pragma endregion

    // Set ownership of the images
    std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamilies =
        logicalDevice.get().getQueueFamiliesIndices();
    
    if (queueFamilies.has_value() == false ||
        queueFamilies.value().presentFamily.has_value() == false) 
    {
        OV_LOG_ERROR("Failed to retriever queue families indices in swapchain with id= %d.", getId());
        this->free();
        return false;
    }

    reserved->swapchainImagesQueueFamilyIndex.resize(imageCount, std::nullopt);
    //for (uint32_t c = 0; c < imageCount; c++)
    //    reserved->swapchainImagesQueueFamilyIndex[c] =
    //    queueFamilies.value().presentFamily.value();

    // Set layout of the images
    reserved->swapchainImagesLayouts.resize(imageCount, VK_IMAGE_LAYOUT_UNDEFINED);

    // Surface dependent
    setActiveCallback(OvVK::Surface::SurfaceCallbackTypes::windowSize);
    if (!reserved->isResizing) 
    {
        Engine::getInstance().getPresentSurface().addSurfaceDependentObject(getId(), *this);
        setIsActive(true);
    }


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the index of the next avaiable image. 
 * @param imageIndex The reference to the variable that will contains the next available image index.
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::acquireNextImage(uint32_t& imageIndex)
{
    // Safety net:
    if (reserved->swapchain == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The swapchain is null in the swapchain with id= %d. Maybe not initialized or created.", getId());
        return false;
    }
    if (reserved->isResizing)
    {
        OV_LOG_ERROR("The swapchain is not ready because resizing in the swapchain with id= %d.", getId());
        return false;
    }


    // Engine
    std::reference_wrapper<OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();

    // reset cpu syncObj
    vkResetFences(logicalDevice.get().getVkDevice(), 1, &reserved->imagesAvailableSyncObjsCPU[reserved->imagesAvailableSyncObjsCPU.size() - 1]);
    // semaphore is automatically reset back to being unsignaled,

    // Retrieve the index of the next available presentable image
    VkResult result = vkAcquireNextImageKHR(logicalDevice.get().getVkDevice(), reserved->swapchain, UINT64_MAX,
        reserved->imagesAvailableSyncObjsGPU[reserved->imagesAvailableSyncObjsGPU.size() - 1],
        reserved->imagesAvailableSyncObjsCPU[reserved->imagesAvailableSyncObjsCPU.size() - 1], &imageIndex);

    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    {
        OV_LOG_ERROR("Swapchain is no more valid or failure occurred.");
        return false;
    }

    // Swap the syncObjs (needed because we know the index of the image only after the acquisition)
    VkSemaphore tmpSemaphore = reserved->imagesAvailableSyncObjsGPU[imageIndex];
    reserved->imagesAvailableSyncObjsGPU[imageIndex] =
        reserved->imagesAvailableSyncObjsGPU[reserved->imagesAvailableSyncObjsGPU.size() - 1];
    reserved->imagesAvailableSyncObjsGPU[reserved->imagesAvailableSyncObjsGPU.size() - 1] = tmpSemaphore;

    VkFence tmpFence = reserved->imagesAvailableSyncObjsCPU[imageIndex];
    reserved->imagesAvailableSyncObjsCPU[imageIndex] =
        reserved->imagesAvailableSyncObjsCPU[reserved->imagesAvailableSyncObjsCPU.size() - 1];
    reserved->imagesAvailableSyncObjsCPU[reserved->imagesAvailableSyncObjsCPU.size() - 1] = tmpFence;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Presents an image in the swapchain
 * @param imageIndex The index of the swapchain's image to present.
 * @param waitSemaphores A vector of Vulkan binary semaphore to wait before presenting the image.
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::presentImage(uint32_t imageIndex, std::vector<VkSemaphore> waitSemaphores)
{
    // Safety net:
    if (reserved->swapchain == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The swapchain is null in the swapchain with id= %d. Maybe not initialized or created.", getId());
        return false;
    }
    if (reserved->isResizing)
    {
        OV_LOG_ERROR("The swapchain with id= %d is not ready now, do to a resize of the window.", getId());
        return false;
    }
    if (imageIndex >= reserved->swapchainImages.size())
    {
        OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id = % d", getId());
        return false;
    }

    // Engine
    std::reference_wrapper<OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Queue Handles
    std::optional<OvVK::LogicalDevice::QueueHandles> queuesHandles
        = logicalDevice.get().getQueueHandles();

    if (queuesHandles.has_value() == false || queuesHandles.value().presentQueue == VK_NULL_HANDLE)
    {
        OV_LOG_INFO(R"(Can't retriever the queues handles or present queue handle is null for the logical device
 in swapchain with id= %d.)", getId());
        return false;
    }
    
    // Queue families indices
    std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamilyIndices
        = logicalDevice.get().getQueueFamiliesIndices();

    if (queueFamilyIndices.has_value() == false || 
        queueFamilyIndices.value().presentFamily.has_value() == false)
    {
        OV_LOG_INFO(R"(Can't retriever queues families indeces or present queue family index is null for the logical device
            in swapchain with id= %d.)", getId());
        return false;
    }


    // Create new timeline semaphore and delete previous one.
    VkSemaphoreTypeCreateInfo timelineCreateInfo = {};
    timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    timelineCreateInfo.pNext = VK_NULL_HANDLE;
    timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    timelineCreateInfo.initialValue = OvVK::RTScene::defaultValueStatusSyncObj;

    VkSemaphoreCreateInfo semaphoreInfoTimeline = {};
    semaphoreInfoTimeline.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfoTimeline.pNext = &timelineCreateInfo;
    semaphoreInfoTimeline.flags = 0;




    /////////////////
    // Preparation //
    /////////////////

    bool needToBeProcessed = false;
    bool needToChangeOwnership = false;

    // Change layout and/or ownership if needed
    if (reserved->swapchainImagesLayouts[imageIndex] != VK_IMAGE_LAYOUT_PRESENT_SRC_KHR ||
        reserved->swapchainImagesQueueFamilyIndex[imageIndex].has_value() == true &&
        reserved->swapchainImagesQueueFamilyIndex[imageIndex].value() != queueFamilyIndices.value().presentFamily.value())
    {
        needToBeProcessed = true;

        ////////////////////////////
        // Record/Submit commands //
        ////////////////////////////

        VkCommandBufferBeginInfo commandBufferBeginInfo{};
        commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
        //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        // Create command buffer to change image layout. If needed also change the ownership queue
        std::reference_wrapper<OvVK::CommandPool> presentCommandPool = engine.get().getPrimaryPresentCmdPool();
        std::reference_wrapper<OvVK::CommandBuffer> presentCommandBuffer = presentCommandPool.get().allocateCommandBuffer();

        if (presentCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
            presentCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR(R"(Fail to begin or change status to the present command buffer in swapchain with id= %d
 in presentImage method.)", getId());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }

        // Swapchain image take ownership
        VkImageMemoryBarrier2 imageMemoryBarrier2 = {};
        imageMemoryBarrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        imageMemoryBarrier2.pNext = VK_NULL_HANDLE;
        // Wait for all previous stage before changing the image layout
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
        imageMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
        // No need to wait on specific memory access.
        imageMemoryBarrier2.srcAccessMask = VK_ACCESS_2_NONE;
        // All commands need to wait that the image change layout.
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
        imageMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_NONE;
        // No need to wait on specific memory access.
        imageMemoryBarrier2.dstAccessMask = VK_ACCESS_2_NONE;

        if (reserved->swapchainImagesLayouts[imageIndex] != VK_IMAGE_LAYOUT_PRESENT_SRC_KHR) 
        {
            // Current image layout
            imageMemoryBarrier2.oldLayout = reserved->swapchainImagesLayouts[imageIndex];
            // Set the layout that allow to present the image
            imageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        }
        else 
        {
            // Current image layout. No need to change layout
            imageMemoryBarrier2.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            imageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        }

        // No need to transfer ownership for now. Check below.
        imageMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        // The image and subresourceRange specify the image that is affected and the specific part of the image.
        imageMemoryBarrier2.image = reserved->swapchainImages[imageIndex];
        imageMemoryBarrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        // baseMipLevel is the first mipmap level accessible to the view.
        imageMemoryBarrier2.subresourceRange.baseMipLevel = 0;
        imageMemoryBarrier2.subresourceRange.levelCount = 1;
        imageMemoryBarrier2.subresourceRange.baseArrayLayer = 0;
        imageMemoryBarrier2.subresourceRange.layerCount = 1;

        // Barrier specification
        VkDependencyInfo imageDependencyInfo = {};
        imageDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
        imageDependencyInfo.dependencyFlags = 0x00000000;
        imageDependencyInfo.memoryBarrierCount = 0;
        imageDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
        imageDependencyInfo.bufferMemoryBarrierCount = 0;
        imageDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
        imageDependencyInfo.imageMemoryBarrierCount = 1;
        imageDependencyInfo.pImageMemoryBarriers = &imageMemoryBarrier2;


        // Change ownership of the image
        if (reserved->swapchainImagesQueueFamilyIndex[imageIndex].has_value() == true &&
            reserved->swapchainImagesQueueFamilyIndex[imageIndex].value() != queueFamilyIndices.value().presentFamily.value())
        {
            needToChangeOwnership = true;

            vkDestroySemaphore(logicalDevice.get().getVkDevice(), reserved->imagesOwnershipSyncObjsGPU[imageIndex], NULL);
            if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfoTimeline, NULL,
                &reserved->imagesOwnershipSyncObjsGPU[imageIndex]) != VK_SUCCESS)
            {
                OV_LOG_ERROR("Failed to create image ownership sync obj for image with id= %d in swapchain with id= %D.",
                    imageIndex, getId());
                this->free();
                return false;
            }

            std::reference_wrapper<OvVK::CommandPool> ownershipCommandPool = OvVK::CommandPool::empty;

            // Select command pool from which allocate the command buffer to change ownership.
            if (queueFamilyIndices.value().graphicsFamily.has_value() == true &&
                queueFamilyIndices.value().graphicsFamily.value() == reserved->swapchainImagesQueueFamilyIndex[imageIndex].value())
                ownershipCommandPool = engine.get().getPrimaryGraphicCmdPool();
            else if (queueFamilyIndices.value().transferFamily.has_value() == true &&
                queueFamilyIndices.value().transferFamily.value() == reserved->swapchainImagesQueueFamilyIndex[imageIndex].value())
                ownershipCommandPool = engine.get().getPrimaryTransferCmdPool();
            else if (queueFamilyIndices.value().computeFamily.has_value() == true &&
                queueFamilyIndices.value().computeFamily.value() == reserved->swapchainImagesQueueFamilyIndex[imageIndex].value())
                ownershipCommandPool = engine.get().getPrimaryComputeCmdPool();
            else {
                OV_LOG_ERROR(R"(In the queue family indices retriever from the logical device there is not the queue owning the image
 with id= %d in the swapchain with id= %d in presentImage method.)", imageIndex, getId());
                presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
                return false;
            }

            std::reference_wrapper<OvVK::CommandBuffer> releaseOwnershipCommandBuffer =
                ownershipCommandPool.get().allocateCommandBuffer();

            // Record buffer
            if (releaseOwnershipCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
                releaseOwnershipCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
            {
                OV_LOG_ERROR(R"(Fail to begin or change status to the release ownership command buffer in the swapchain with
 id= %d in presentImage method.)", getId());
                ownershipCommandPool.get().freeCommandBuffer(releaseOwnershipCommandBuffer.get());
                presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
                return false;
            }

            // Set transfer ownership
            imageMemoryBarrier2.srcQueueFamilyIndex = reserved->swapchainImagesQueueFamilyIndex[imageIndex].value();
            imageMemoryBarrier2.dstQueueFamilyIndex = queueFamilyIndices.value().presentFamily.value();

            // Register commands for transition.
            vkCmdPipelineBarrier2(releaseOwnershipCommandBuffer.get().getVkCommandBuffer(), &imageDependencyInfo);

            // End commandBuffer
            if (releaseOwnershipCommandBuffer.get().endVkCommandBuffer() == false ||
                releaseOwnershipCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
            {
                OV_LOG_ERROR(R"(Fail to submit the release ownership command buffer in the swapchain with
 id= %d in presentImage method.)", getId());
                ownershipCommandPool.get().freeCommandBuffer(releaseOwnershipCommandBuffer.get());
                presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
                return false;
            }

            ////////////
            // Submit //
            ////////////

            VkSubmitInfo2 submitInfo2 = {};
            submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
            submitInfo2.pNext = VK_NULL_HANDLE;
            submitInfo2.flags = 0x00000000;

            std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(waitSemaphores.size());
            for (uint32_t c = 0; c < waitSemaphores.size(); c++) {
                pWaitSemaphoreInfos[c].sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
                pWaitSemaphoreInfos[c].pNext = VK_NULL_HANDLE;
                pWaitSemaphoreInfos[c].semaphore = waitSemaphores[c];
                // Binary
                pWaitSemaphoreInfos[c].value = 0;
                // All command s need to wait
                // VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT 
                pWaitSemaphoreInfos[c].stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                // If the device that semaphore was created on is not a device group, deviceIndex must be 0
                pWaitSemaphoreInfos[c].deviceIndex = 0;
            }
            submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
            submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

            VkSemaphoreSubmitInfo pSignalSemaphoreInfos = {};
            pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
            pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
            // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT 
            pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
            pSignalSemaphoreInfos.semaphore = reserved->imagesOwnershipSyncObjsGPU[imageIndex];
            // binary semaphores
            pSignalSemaphoreInfos.value = OvVK::RTScene::finishValueRederingStatusSyncObj;
            // If the device that semaphore was created on is not a device group, deviceIndex must be 0
            pSignalSemaphoreInfos.deviceIndex = 0;

            submitInfo2.signalSemaphoreInfoCount = 1;
            submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

            VkCommandBufferSubmitInfo pCommandBufferInfos;
            pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
            pCommandBufferInfos.pNext = VK_NULL_HANDLE;
            pCommandBufferInfos.commandBuffer = releaseOwnershipCommandBuffer.get().getVkCommandBuffer();
            // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
            pCommandBufferInfos.deviceMask = 0;
            submitInfo2.commandBufferInfoCount = 1;
            submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

            // Submit command to transfer ownership
            if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(reserved->swapchainImagesQueueFamilyIndex[imageIndex].value()),
                1, &submitInfo2, NULL) != VK_SUCCESS)
            {
                OV_LOG_INFO("Fail to submit ownership change for image with id= %d in Swapchain with id= %d in presentImage method.",
                    imageIndex, getId());
                ownershipCommandPool.get().freeCommandBuffer(releaseOwnershipCommandBuffer.get());
                presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
                return false;
            }
        }

        // Register barrier in the present command buffer.
        vkCmdPipelineBarrier2(presentCommandBuffer.get().getVkCommandBuffer(), &imageDependencyInfo);

        // End present command buffer
        if (presentCommandBuffer.get().endVkCommandBuffer() == false ||
            presentCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR(R"(Fail to end or change status to the present command buffer in the Swapchain with id= %d
 in presentImage method.)", getId());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }

        ////////////
        // Submit //
        ////////////

        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;

        std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos;

        if (needToChangeOwnership) {
            VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo = {};
            vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
            vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
            // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT 
            vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
            vkSemaphoreSubmitInfo.semaphore = reserved->imagesOwnershipSyncObjsGPU[imageIndex];
            // binary semaphores
            vkSemaphoreSubmitInfo.value = OvVK::RTScene::finishValueRederingStatusSyncObj;
            // If the device that semaphore was created on is not a device group, deviceIndex must be 0
            vkSemaphoreSubmitInfo.deviceIndex = 0;

            pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
        }
        else {
            pWaitSemaphoreInfos.resize(waitSemaphores.size());
            for (uint32_t c = 0; c < waitSemaphores.size(); c++) {
                pWaitSemaphoreInfos[c].sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
                pWaitSemaphoreInfos[c].pNext = VK_NULL_HANDLE;
                pWaitSemaphoreInfos[c].semaphore = waitSemaphores[c];
                // Binary
                pWaitSemaphoreInfos[c].value = 0;
                // All command s need to wait
                // VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT 
                pWaitSemaphoreInfos[c].stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                // If the device that semaphore was created on is not a device group, deviceIndex must be 0
                pWaitSemaphoreInfos[c].deviceIndex = 0;
            }
        }
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos = {};
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT 
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        pSignalSemaphoreInfos.semaphore = reserved->imagesPresentationSyncObjsGPU[imageIndex];
        // binary semaphores
        pSignalSemaphoreInfos.value = 0;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        submitInfo2.signalSemaphoreInfoCount = 1;
        submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = presentCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;
        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        // Reset sync CPU Obj.
        vkResetFences(logicalDevice.get().getVkDevice(), 1, &reserved->imagesPresentationSyncObjsCPU[imageIndex]);
        // Submit command buffer to change ownership or layout of the swapchain image
        if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(queueFamilyIndices.value().presentFamily.value()),
            1, &submitInfo2, reserved->imagesPresentationSyncObjsCPU[imageIndex]) != VK_SUCCESS)
        {
            OV_LOG_INFO(R"(Fail to submit present command buffer to prepare image for presentation in the Swapchain with id= %d
 in presentImage method.)", getId());
            presentCommandPool.get().freeCommandBuffer(presentCommandBuffer.get());
            return false;
        }
    }

    reserved->swapchainImagesQueueFamilyIndex[imageIndex] = queueFamilyIndices.value().presentFamily.value();
    reserved->swapchainImagesLayouts[imageIndex] = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    /////////////
    // Present //
    /////////////

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.pNext = NULL;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &reserved->swapchain;
    presentInfo.pImageIndices = &imageIndex;
    if (needToBeProcessed)
    {
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &reserved->imagesPresentationSyncObjsGPU[imageIndex];
    }
    else
    {
        presentInfo.waitSemaphoreCount = waitSemaphores.size();
        presentInfo.pWaitSemaphores = waitSemaphores.data();
    }

    // There is one last optional parameter called pResults. It allows you to specify an array of VkResult values to check 
    // for every individual swap chain if presentation was successful. It's not necessary if you're only using a single swap 
    // chain, because you can simply use the return value of the present function.
    presentInfo.pResults = nullptr; // Optional

    VkResult result = vkQueuePresentKHR(queuesHandles.value().presentQueue, &presentInfo);

    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    {
        OV_LOG_ERROR("Fail to present the image of the swapchain with id= %d", imageIndex);
        return false;
    }



    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the Vulkan Swapchain handle.
 * @return The Vulkan Swapchain handle.
 */
VkSwapchainKHR OVVK_API OvVK::Swapchain::getSwapchain() const
{
    return reserved->swapchain;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets a vector containing the Vulkan swapchain's images handles.
 * @return The Vulkan swapchain's images handles.
 */
std::vector<VkImage> OVVK_API OvVK::Swapchain::getSwapchainImages() const
{
    return reserved->swapchainImages;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets a vector containing the Vulkan swapchain's images viewa handles.
 * @return The Vulkan swapchain's images viewa handles.
 */
std::vector<VkImageView> OVVK_API OvVK::Swapchain::getSwapchainImageViews() const
{
    return reserved->swapchainImageViews;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the Vulkan swapchain's image handle of the image with the passed index.
 * @param imageIndex The index of the Vulkan image handle to retriever.
 * @return The Vulkan swapchain's image handle of the image with the passed index.
 */
VkImage OVVK_API OvVK::Swapchain::getSwapchainImage(uint32_t imageIndex) const
{
    if (imageIndex < reserved->swapchainImages.size())
        return reserved->swapchainImages[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return VK_NULL_HANDLE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the Vulkan swapchain's image view handle of the image with the passed index.
 * @param imageIndex The index of the Vulkan image view handle to retriever.
 * @return The Vulkan swapchain's image view handle of the image with the passed index.
 */
VkImageView OVVK_API OvVK::Swapchain::getSwapchainImageView(uint32_t imageIndex) const
{
    if (imageIndex < reserved->swapchainImageViews.size())
        return reserved->swapchainImageViews[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return VK_NULL_HANDLE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the index of the queue owning the swapchain's image with the passed index.
 * @param imageIndex The index of the Vulkan image for which retriver the ownership.
 * @return Std optional object containing the index of the queue owning the swapchain's image with the passed index if one.
 */
std::optional<uint32_t> OVVK_API OvVK::Swapchain::getSwapchainImageOwnership(uint32_t imageIndex) const
{
    if (imageIndex < reserved->swapchainImagesQueueFamilyIndex.size())
        return reserved->swapchainImagesQueueFamilyIndex[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return std::nullopt;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the layout of the swapchain's image with the passed index.
 * @param imageIndex The index of the Vulkan image for which retriver the layout.
 * @return Std optional object containing the layout of the swapchain's image with the passed index.
 */
std::optional<VkImageLayout> OVVK_API OvVK::Swapchain::getSwapchainImageLayout(uint32_t imageIndex) const
{
    if (imageIndex < reserved->swapchainImagesLayouts.size())
        return reserved->swapchainImagesLayouts[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return std::nullopt;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the number of images in the swapchain
 * @return The number of images in the swapchain.
 */
uint32_t OVVK_API OvVK::Swapchain::getNrOfSwapchainImages() const
{
    return static_cast<uint32_t>(reserved->swapchainImages.size());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the format of the swapchain's images.
 * @return The Vulkan obj describing the format of the swapchain's images.
 */
VkFormat OVVK_API OvVK::Swapchain::getSwapchainImageFormat() const
{
    return reserved->swapchainImageFormat;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the extent of the swapchain's images.
 * @return The Vulkan obj describing the extent of the swapchain's images.
 */
VkExtent2D OVVK_API OvVK::Swapchain::getSwapChainExtent() const
{
    return reserved->swapchainExtent;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the available GPU sync obj of the image with the passed index.
 * @param imageIndex The index of the swapchain's image available GPU sync obj to retriever.
 * @return The Vulkan handle of the available GPU sync obj of the image with the passed index.
 */
VkSemaphore OVVK_API OvVK::Swapchain::getImageAvailableSyncObjGPU(uint32_t imageIndex) const
{
    if (imageIndex < reserved->imagesAvailableSyncObjsGPU.size())
        return reserved->imagesAvailableSyncObjsGPU[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return VK_NULL_HANDLE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the available CPU sync obj of the image with the passed index.
 * @param imageIndex The index of the swapchain's image available CPU sync obj to retriever.
 * @return The Vulkan handle of the available CPU sync obj of the image with the passed index.
 */
VkFence OVVK_API OvVK::Swapchain::getImageAvailableSyncObjCPU(uint32_t imageIndex) const
{
    if (imageIndex < reserved->imagesAvailableSyncObjsCPU.size())
        return reserved->imagesAvailableSyncObjsCPU[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return VK_NULL_HANDLE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the presentation GPU sync obj of the image with the passed index.
 * @param imageIndex The index of the swapchain's image presentation GPU sync obj to retriever.
 * @return The Vulkan handle of the presentation GPU sync obj of the image with the passed index.
 */
VkSemaphore OVVK_API OvVK::Swapchain::getPresentationSyncObjGPU(uint32_t imageIndex) const
{
    if (imageIndex < reserved->imagesPresentationSyncObjsGPU.size())
        return reserved->imagesPresentationSyncObjsGPU[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return VK_NULL_HANDLE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the presentation CPU sync obj of the image with the passed index.
 * @param imageIndex The index of the swapchain's image presentation CPU sync obj to retriever.
 * @return The Vulkan handle of the presentation CPU sync obj of the image with the passed index.
 */
VkFence OVVK_API OvVK::Swapchain::getPresentationSyncObjCPU(uint32_t imageIndex) const
{
    if (imageIndex < reserved->imagesPresentationSyncObjsCPU.size())
        return reserved->imagesPresentationSyncObjsCPU[imageIndex];

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return VK_NULL_HANDLE;
}

bool OVVK_API OvVK::Swapchain::isResizing() const {
    return reserved->isResizing;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the queue owning the swapchain's image with the passed index.
 * @param imageIndex The index of the swapchain's image to which set ownership.
 * @param queueFamilyIndex The index of the queue family that own the image with the passed index.
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::setSwapchainImageOwnership(
    uint32_t imageIndex, uint32_t queueFamilyIndex) const
{
    if (imageIndex < reserved->swapchainImagesQueueFamilyIndex.size()) 
    {
        reserved->swapchainImagesQueueFamilyIndex[imageIndex] = queueFamilyIndex;
        return true;
    }

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the layout of the swapchain's image with the passed index.
 * @param imageIndex The index of the swapchain's image to which set the layout.
 * @param imageLayout The layout in which the image with the passed index is.
 * @return TF
 */
bool OVVK_API OvVK::Swapchain::setSwapchainImageLayout(
    uint32_t imageIndex, VkImageLayout imageLayout) const
{
    if (imageIndex < reserved->swapchainImagesLayouts.size())
    {
        reserved->swapchainImagesLayouts[imageIndex] = imageLayout;
        return true;
    }

    OV_LOG_ERROR("The image Index passed is out of range in the swapchain with id= %d", getId());
    return false;
}

#pragma endregion

#pragma region Rating

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets swapchain surface format scores.
 * @param availableFormats The surface formats to rate.
 * @return A vector containing std pairs with score in first place and the Vulkan surface format handle in the latter.
 */
std::vector<std::pair<uint32_t, VkSurfaceFormatKHR>> OVVK_API OvVK::Swapchain::rateSwapchainSurfaceFormat(
    const std::vector<VkSurfaceFormatKHR>& availableFormats) {

    std::vector<std::pair<uint32_t, VkSurfaceFormatKHR>> formatsRate;

    // Each VkSurfaceFormatKHR entry contains a format and a colorSpace member. 
    // The format member specifies the color channels and types.
    // The colorSpace member indicates supported color spaces of a presentation engine
    for (const VkSurfaceFormatKHR& availableFormat : availableFormats) {

        // Desired settings
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM
            && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            formatsRate.push_back(std::make_pair(10, availableFormat));
        }
        else 
        {
            // Default value
            formatsRate.push_back(std::make_pair(0, availableFormat));
        }
    }

    // Sort using comparator function
    std::sort(formatsRate.begin(), formatsRate.end(),
        [](const std::pair<uint32_t, VkSurfaceFormatKHR>& a,
            const std::pair<uint32_t, VkSurfaceFormatKHR>& b) -> bool
        {
            return a.first > b.first;
        });

    return formatsRate;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets present modes scores.
 * @param availableFormats The present modes to rate.
 * @return A vector containing std pairs with score in first place and the Vulkan present mode handle in the latter.
 */
std::vector<std::pair<uint32_t, VkPresentModeKHR>>  OVVK_API OvVK::Swapchain::rateSwapchainPresentMode(
    const std::vector<VkPresentModeKHR>& availablePresentModes) {

    std::vector<std::pair<uint32_t, VkPresentModeKHR>> presentModeRate;

    // VK_PRESENT_MODE_IMMEDIATE_KHR: Images submitted by your application are transferred to the screen right away,
    //      which may result in tearing.
    // VK_PRESENT_MODE_FIFO_KHR: The swap chain is a queue where the display takes an image from the front of the 
    //      queue when the display is refreshed and the program inserts rendered images at the back of the queue.
    //      If the queue is full then the program has to wait. This is most similar to vertical sync as found in
    //      modern games. The moment that the display is refreshed is known as "vertical blank".
    // VK_PRESENT_MODE_FIFO_RELAXED_KHR: This mode only differs from the previous one if the application is late
    //      and the queue was empty at the last vertical blank. Instead of waiting for the next vertical blank,
    //      the image is transferred right away when it finally arrives. This may result in visible tearing.
    // VK_PRESENT_MODE_MAILBOX_KHR: This is another variation of the second mode. Instead of blocking the application
    //      when the queue is full, the images that are already queued are simply replaced with the newer ones. 
    //      This mode can be used to render frames as fast as possible while still avoiding tearing, resulting
    //      in fewer latency issues than standard vertical sync. This is commonly known as "triple buffering",
    //      although the existence of three buffers alone does not necessarily mean that the framerate is unlocked.
    //      On mobile devices, where energy usage is more important, you will probably want to use 
    //      VK_PRESENT_MODE_FIFO_KHR instead.


    // look through the list to see if VK_PRESENT_MODE_MAILBOX_KHR is available
    for (const VkPresentModeKHR& availablePresentMode : availablePresentModes) {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
            presentModeRate.push_back(std::make_pair(10, VK_PRESENT_MODE_MAILBOX_KHR));
        }
        else if (availablePresentMode == VK_PRESENT_MODE_FIFO_KHR)
        {
            presentModeRate.push_back(std::make_pair(5, VK_PRESENT_MODE_FIFO_KHR));
        }
        else 
        {
            // Default value
            presentModeRate.push_back(std::make_pair(0, availablePresentMode));
        }
    }

    // Sort using comparator function
    std::sort(presentModeRate.begin(), presentModeRate.end(),
        [](const std::pair<uint32_t, VkPresentModeKHR>& a,
            const std::pair<uint32_t, VkPresentModeKHR>& b) -> bool
        {
            return a.first > b.first;
        });


    return presentModeRate;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets extent of the swapchain.
 * @param capabilities The reference to a Vulkan surface capabilities handle describing surface properties. 
 * @param width The possible width of the swapchain's images.
 * @param height The possible height of the swapchain's images.
 * @return Vulkan handle for extent.
 */
VkExtent2D OvVK::Swapchain::chooseSwapchainExtent(const VkSurfaceCapabilitiesKHR& capabilities,
    uint32_t width, uint32_t height) {

    // The swap extent is the resolution of the swap chain images and it's almost always exactly equal to the
    // resolution of the window that we're drawing to

    // Vulkan tells us to match the resolution of the window by setting the width and height in the 
    // currentExtent member.
    // However, some window managers do allow us to differ here and this is indicated by setting the 
    // width and height in currentExtent to a special value: the maximum value of uint32_t
    // In that case we'll pick the resolution that best matches the window within the minImageExtent and 
    // maxImageExtent bounds.
    if (capabilities.currentExtent.width != UINT32_MAX) {
        return capabilities.currentExtent;
    }
    else {

        VkExtent2D actualExtent = { width, height };

        // bound the values of width and height between the allowed minimum and maximum extents that 
        // are supported by the implementation
        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(actualExtent.width,
            capabilities.maxImageExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(actualExtent.height,
            capabilities.maxImageExtent.height));

        return actualExtent;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Chosses the best available composite alpha.
 * @param availableFormats The reference to a Vulkan composite alpha handle describing the possible composition.
 * @return The Vulkan flag describing the composite alpha choose.
 */
VkCompositeAlphaFlagBitsKHR OvVK::Swapchain::chooseSwapchainSurfaceCompositeAlpha(
    const VkCompositeAlphaFlagsKHR& availableCompositions) {

    // The rules to choose the composite Alpha

    // VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR: The alpha component, if it exists, of the images is ignored in 
    // the compositing process. Instead, the image is treated as if it has a constant alpha of 1.0.
    if (availableCompositions & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR) { return VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; }
    // VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR: The alpha component, if it exists, of the images is respected
    // in the compositing process. The non-alpha components of the image are expected to already be multiplied
    // by the alpha component by the application.
    else if (availableCompositions & VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR) {
        return VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR;
    }
    // VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR: The alpha component, if it exists, of the images is respected 
    // in the compositing process. The non-alpha components of the image are not expected to already be multiplied
    // by the alpha component by the application; instead, the compositor will multiply the non-alpha components
    // of the image by the alpha component during compositing.
    else if (availableCompositions & VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR) {
        return VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR;
    }
    else {
        // VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR: The way in which the presentation engine treats the alpha 
        // component in the images is unknown to the Vulkan API. Instead, the application is responsible
        // for setting the composite alpha blending mode using native window system commands. If the application
        // does not set the blending mode using native window system commands, then a platform-specific 
        // default will be used.
        return VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
    }
}

#pragma endregion