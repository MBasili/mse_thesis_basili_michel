/**
 * @file	overv_vk_thread.h
 * @brief	Definition of thread in overv_vulkan
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 //////////////
 // #INCLUDE //
 //////////////

 // Main include
#include "overv_vk.h"

// C/C++
#include <functional>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>


/////////////
// #STATIC //
/////////////

// Special values:
OvVK::Thread OvVK::Thread::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief ThreadPool reserved structure.
 */
struct OvVK::Thread::Reserved
{
    bool destroying;
    std::thread worker;
    std::queue<std::function<void()>> jobQueue;
    std::mutex queueMutex;
    std::condition_variable condition;

    /**
     * Constructor.
     */
    Reserved() : destroying{ false }
    { }
};


//////////////////////////
// BODY OF CLASS Thread //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Thread::Thread() : reserved(std::make_unique<OvVK::Thread::Reserved>())
{
    OV_LOG_DETAIL("[+]");

    reserved->worker = std::thread(&Thread::queueLoop, this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Thread::Thread(const std::string& name) : Ov::Object(name),
reserved(std::make_unique<OvVK::Thread::Reserved>())
{
    OV_LOG_DETAIL("[+]");

    reserved->worker = std::thread(&Thread::queueLoop, this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Thread::Thread(Thread&& other) noexcept :
    Ov::Object(std::move(other))
{
    OV_LOG_DETAIL("[M]");

    reserved->worker = std::thread(&Thread::queueLoop, this);

    //Wait previos job
    other.wait();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Thread::~Thread()
{
    OV_LOG_DETAIL("[-]");

    if (reserved->worker.joinable())
    {
        wait();
        reserved->queueMutex.lock();
        reserved->destroying = true;
        reserved->condition.notify_one();
        reserved->queueMutex.unlock();
        reserved->worker.join();
    }
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a new job to the thread's queue.
 * @param function A function pointer to the function to add to the thread's jobs.
 */
void OVVK_API OvVK::Thread::addJob(std::function<void()> function)
{
    // Lock the mutex to add a new job.
    std::lock_guard<std::mutex> lock(reserved->queueMutex);
    reserved->jobQueue.push(std::move(function));
    reserved->condition.notify_one();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Wait until all work items have been finished.
 */
void OVVK_API OvVK::Thread::wait()
{
    // general-purpose mutex ownership wrapper allowing deferred locking
    std::unique_lock<std::mutex> lock(reserved->queueMutex);
    // atomically releases the mutex and suspends thread execution until the condition variable is notified
    reserved->condition.wait(lock, [this]() { return reserved->jobQueue.empty(); });
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loop through all remaining jobs
 */
void OVVK_API OvVK::Thread::queueLoop()
{
    while (true)
    {
        std::function<void()> job;

        // scoped block for the mutex variable
        {
            std::unique_lock<std::mutex> lock(reserved->queueMutex);
            // Atomically unlocks lock, blocks the current executing thread, and adds 
            // it to the list of threads waiting on* this.The thread will be unblocked when 
            // notify_all() or notify_one() is executed.It may also be unblocked spuriously.
            // When unblocked, regardless of the reason, lock is reacquiredand wait exits.
            reserved->condition.wait(lock, [this] { return !reserved->jobQueue.empty() || reserved->destroying; });
            if (reserved->destroying)
            {
                break;
            }
            job = reserved->jobQueue.front();
        }

        job();

        // scoped block for the mutex variable
        {
            std::lock_guard<std::mutex> lock(reserved->queueMutex);
            // remove first element
            reserved->jobQueue.pop();
            reserved->condition.notify_one();
        }
    }
}

#pragma endregion
