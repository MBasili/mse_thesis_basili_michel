/**
 * @file    overv_vk_ac.cpp
 * @brief	Acceleration structure
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum:
#include "magic_enum.hpp"


////////////
// STATIC //
////////////

// Special values:   
OvVK::AccelerationStructure OvVK::AccelerationStructure::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief AccelerationStructure class reserved structure.
 */
struct OvVK::AccelerationStructure::Reserved
{
    OvVK::Buffer scratchBuffer;                                                     ///< Scratch buffer.
    OvVK::Buffer accelerationStructureBuffer;                                       ///< AS buffer.
    
    VkAccelerationStructureKHR accelerationStructure;                                   ///< Vulkan handle to the AS.
    VkAccelerationStructureBuildGeometryInfoKHR accelerationStructureBuildGeometryInfo; ///< Structure specifying the geometry data used to build an acceleration structure.
    VkBuildAccelerationStructureFlagsKHR buildAccelerationStructureFlagsKHR;            ///< Flag specifying additional parameters for acceleration structure builds
    
    /**
     * Constructor
     */
    Reserved() : accelerationStructure { VK_NULL_HANDLE },
        accelerationStructureBuildGeometryInfo{ {} },
        buildAccelerationStructureFlagsKHR{ 0x00000000 }
    {}
};


/////////////////////////////////////////
// BODY OF CLASS AccelerationStructure //
/////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::AccelerationStructure::AccelerationStructure() : 
    reserved(std::make_unique<OvVK::AccelerationStructure::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The AS name.
 */
OVVK_API OvVK::AccelerationStructure::AccelerationStructure(const std::string& name) : Ov::Renderable(name),
    reserved(std::make_unique<OvVK::AccelerationStructure::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::AccelerationStructure::AccelerationStructure(AccelerationStructure&& other) noexcept: 
    Ov::Renderable(std::move(other)), Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::AccelerationStructure::~AccelerationStructure()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes acceleration struture
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Frees AS if created
    if (reserved->accelerationStructure != VK_NULL_HANDLE)
    {
        // Logical device
        VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

        PFN_vkDestroyAccelerationStructureKHR pvkDestroyAccelerationStructureKHR =
            (PFN_vkDestroyAccelerationStructureKHR)vkGetDeviceProcAddr(logicalDevice,
                "vkDestroyAccelerationStructureKHR");

        pvkDestroyAccelerationStructureKHR(logicalDevice, reserved->accelerationStructure, NULL);

        reserved->accelerationStructure = VK_NULL_HANDLE;
        reserved->accelerationStructureBuildGeometryInfo = {};
        reserved->buildAccelerationStructureFlagsKHR = 0x00000000;
    }

    // Frees buffers if allocated 
    if (reserved->accelerationStructureBuffer.free() == false)
        OV_LOG_WARN("Fail to free the AS OvVK buffer in the AS with id= %d", getId());

    if (reserved->scratchBuffer.free() == false)
        OV_LOG_WARN("Fail to free the AS scratch OvVK buffer in the AS with id= %d", getId());


    // Done: 
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases acceleration struture.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::free()
{

    if (this->Ov::Managed::free() == false)
        return false;


    // Frees AS if created
    if (reserved->accelerationStructure != VK_NULL_HANDLE)
    {
        // Logical device
        VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

        PFN_vkDestroyAccelerationStructureKHR pvkDestroyAccelerationStructureKHR =
            (PFN_vkDestroyAccelerationStructureKHR)vkGetDeviceProcAddr(logicalDevice,
                "vkDestroyAccelerationStructureKHR");

        pvkDestroyAccelerationStructureKHR(logicalDevice, reserved->accelerationStructure, NULL);

        reserved->accelerationStructure = VK_NULL_HANDLE;
        reserved->accelerationStructureBuildGeometryInfo = {};
        reserved->buildAccelerationStructureFlagsKHR = 0x00000000;
    }

    // Frees buffers if allocated 
    if (reserved->accelerationStructureBuffer.free() == false)
        OV_LOG_WARN("Fail to free the AS OvVK buffer in the AS with id= %d", getId());

    if (reserved->scratchBuffer.free() == false)
        OV_LOG_WARN("Fail to free the AS scratch OvVK buffer in the AS with id= %d", getId());


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncObj

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the AS is locked.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::isLock() const
{
    return reserved->accelerationStructureBuffer.isLock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the AS is owned from a queue family.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::isOwned() const
{
    return reserved->accelerationStructureBuffer.getQueueFamilyIndex().has_value();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the scratch buffer allocated to create the AS.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::freeScratchBuffer() 
{
    // Safety net:
    if (reserved->scratchBuffer.getVkBuffer() == VK_NULL_HANDLE) 
    {
        OV_LOG_INFO("The VKBuffer is null in the scratch buffer in the AS id= %d.", getId());
        return true;
    }

    VkSemaphore scratchBufferSemaphore = reserved->accelerationStructureBuffer.getStatusSyncObj();
    if (scratchBufferSemaphore != VK_NULL_HANDLE)
    {
        // Retriver the status of the AS
        VkDevice logicalDevice = OvVK::Engine::getInstance().getLogicalDevice().getVkDevice();
        uint64_t value;

        if (vkGetSemaphoreCounterValue(logicalDevice, scratchBufferSemaphore, &value) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to retriever the value of the status of the AS with id= %d.", getId());
            return false;
        }

        if (value != OvVK::AccelerationStructure::readyValueStatusSyncObj &&
            value != OvVK::Buffer::defaultValueStatusSyncObj) {
            OV_LOG_ERROR("Can't free scratch buffer because of the status of the AS with id= %d.", getId());
            return false;
        }
    }
    return reserved->scratchBuffer.free();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets up the AS for build or update.
 * @param type The type of the AS.
 * @param accelerationStructureGeometriesKHR Vector containing the structures specifying geometries of the AS.
 * @param accelerationStructureBuildRangeInfosKHR Vector containing the structures specifying offsets and counts for the AS build or update
 * @param flag The flag specifing the AS "properties".
 * @param mode The mode that defines whether to build or update the AS.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::setUp(VkAccelerationStructureTypeKHR type,
    std::vector<VkAccelerationStructureGeometryKHR>& accelerationStructureGeometriesKHR,
    std::vector<VkAccelerationStructureBuildRangeInfoKHR>& accelerationStructureBuildRangeInfosKHR,
    VkBuildAccelerationStructureModeKHR mode,
    VkBuildAccelerationStructureFlagsKHR flag)
{
    // Safety Net
    if (accelerationStructureGeometriesKHR.size() == 0)
    {
        OV_LOG_INFO("accelerationStructureGeometriesKHR is empty in AS with id= %d.", getId());
        return false;
    }
    if (accelerationStructureBuildRangeInfosKHR.size() == 0)
    {
        OV_LOG_INFO("accelerationStructureBuildRangeInfosKHR is empty in AS with id= %d.", getId());
        return false;
    }
    if (mode & VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR == VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR)
    {
        if (reserved->accelerationStructure == VK_NULL_HANDLE) {
            OV_LOG_ERROR("Can't update the AS with id= %d because the VkAccelerationStructureKHR is null.", getId());
            return false;
        }

        if (reserved->buildAccelerationStructureFlagsKHR & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR
            != VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR) {
            OV_LOG_ERROR("Can't update the AS with id= %d because the flag was not set during build.", getId());
            return false;
        }
    }

    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // Logical Device
    VkDevice logicalDevice = engine.get().getLogicalDevice().getVkDevice();

    // Structure specifying the geometry data used to build an acceleration structure.
    reserved->accelerationStructureBuildGeometryInfo.sType =
        VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    reserved->accelerationStructureBuildGeometryInfo.pNext = NULL;
    reserved->accelerationStructureBuildGeometryInfo.type = type;
    reserved->accelerationStructureBuildGeometryInfo.flags = flag;
    reserved->accelerationStructureBuildGeometryInfo.mode = mode;
    reserved->accelerationStructureBuildGeometryInfo.geometryCount =
        static_cast<uint32_t>(accelerationStructureGeometriesKHR.size());
    // The index of each element of the pGeometries or ppGeometries members of VkAccelerationStructureBuildGeometryInfoKHR
    // is used as the geometry index during ray traversal. The geometry index is available in ray shaders via the
    // RayGeometryIndexKHR built - in, and is used to determine hit and intersection shaders executed 
    // during traversal. The geometry index is available to ray queries via the OpRayQueryGetIntersectionGeometryIndexKHR instruction.
    reserved->accelerationStructureBuildGeometryInfo.pGeometries = accelerationStructureGeometriesKHR.data();
    reserved->accelerationStructureBuildGeometryInfo.ppGeometries = NULL;
    reserved->accelerationStructureBuildGeometryInfo.scratchData = {};

    // Structure specifying build sizes for an acceleration structure
    VkAccelerationStructureBuildSizesInfoKHR accelerationStructureBuildSizesInfo = {};
    accelerationStructureBuildSizesInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    accelerationStructureBuildSizesInfo.pNext = NULL;
    accelerationStructureBuildSizesInfo.accelerationStructureSize = 0;
    accelerationStructureBuildSizesInfo.updateScratchSize = 0;
    accelerationStructureBuildSizesInfo.buildScratchSize = 0;

    // Retrieve the required size for an acceleration structure
    PFN_vkGetAccelerationStructureBuildSizesKHR pvkGetAccelerationStructureBuildSizesKHR =
        (PFN_vkGetAccelerationStructureBuildSizesKHR)vkGetDeviceProcAddr(logicalDevice,
            "vkGetAccelerationStructureBuildSizesKHR");

    std::vector<uint32_t> primitiveCount(accelerationStructureBuildRangeInfosKHR.size());
    for (uint32_t c = 0; c < accelerationStructureBuildRangeInfosKHR.size(); c++)
        primitiveCount[c] = accelerationStructureBuildRangeInfosKHR[c].primitiveCount;

    // VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR => requests the memory requirement for operations
    // performed by the device.
    pvkGetAccelerationStructureBuildSizesKHR(logicalDevice,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &reserved->accelerationStructureBuildGeometryInfo,
        primitiveCount.data(),
        &accelerationStructureBuildSizesInfo);


    ////////////////////
    // Scratch buffer //
    ////////////////////

#pragma region ScratchBuffer 

    ///////////////////////////
    // Create scratch buffer //
    ///////////////////////////

    // Create info buffer
    VkBufferCreateInfo vkScratchBufferCreateInfo{};
    vkScratchBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    vkScratchBufferCreateInfo.size = accelerationStructureBuildSizesInfo.buildScratchSize;
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable for
    //      use as a read-only input to an acceleration structure build.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT => specifies that the buffer can be used to retrieve a buffer device address
    //      via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    vkScratchBufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
        | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
    // from multiple queue families is supported.
    vkScratchBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    vkScratchBufferCreateInfo.pQueueFamilyIndices = NULL;
    vkScratchBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaScratchBufferAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_MAPPED_BIT => Set this flag to use a memory that will be persistently 
    // mappedand retrieve pointer to it. It is valid to use this flag for allocation made from memory
    // type that is not HOST_VISIBLE. This flag is then ignored and memory is not mapped. This is useful
    // if you need an allocation that is efficient to use on GPU (DEVICE_LOCAL) and still want to map it
    // directly if possible on platforms that support it (e.g. Intel GPU).
    vmaScratchBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // Flags that must be set in a Memory Type chosen for an allocation. 
    vmaScratchBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    if (!reserved->scratchBuffer.create(vkScratchBufferCreateInfo, vmaScratchBufferAllocationCreateInfo))
    {
        OV_LOG_ERROR("Fail to create the scrath buffer in the AS with id= %d.", getId());
        this->free();
        return false;
    }


    ////////////////////////
    // Set scratch buffer //
    ////////////////////////

    std::optional<VkDeviceAddress> deviceAddress = reserved->scratchBuffer.getBufferDeviceAddress();
    if (deviceAddress.has_value() == false)
    {
        OV_LOG_ERROR("Fail to retrieve the scratch buffer device addressin in the AS with id= %d.", getId());
        this->free();
        return false;
    }

    VkDeviceOrHostAddressKHR scratchDeviceOrHostAddress = {};
    scratchDeviceOrHostAddress.deviceAddress = deviceAddress.value();

    // ScratchData is the device or host address to memory that will be used as scratch memory for the build.
    reserved->accelerationStructureBuildGeometryInfo.scratchData = scratchDeviceOrHostAddress;

#pragma endregion


    ////////
    // AS //
    ////////

    if (mode == VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR)
    {
        if (createAS(accelerationStructureBuildSizesInfo.accelerationStructureSize, type) == false)
            return false;

        reserved->accelerationStructureBuildGeometryInfo.srcAccelerationStructure = VK_NULL_HANDLE;
        reserved->accelerationStructureBuildGeometryInfo.dstAccelerationStructure = reserved->accelerationStructure;
    }
    else if (mode == VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR)
    {
        // Inplace update
        reserved->accelerationStructureBuildGeometryInfo.srcAccelerationStructure = reserved->accelerationStructure;
        reserved->accelerationStructureBuildGeometryInfo.dstAccelerationStructure = reserved->accelerationStructure;
    }

    // Set Status value
    if (setStatusSyncObjValue(OvVK::AccelerationStructure::readyToBuildValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to set the value of the status sync obj in the AS with id= %d.", getId());
        this->free();
        return false;
    }

    // Set parameters
    reserved->buildAccelerationStructureFlagsKHR = flag;

    this->Ov::Object::setDirty(true);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the AS.
 * @param asBufferSize The size of the buffer containing the AS.
 * @param type The type of the AS
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::createAS(VkDeviceSize asBufferSize,
    VkAccelerationStructureTypeKHR type)
{
    ///////////////
    // AS buffer //
    ///////////////

#pragma region ASBuffer

    // Create info buffer
    VkBufferCreateInfo vkBLASBufferCreateInfo{};
    vkBLASBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    vkBLASBufferCreateInfo.size = asBufferSize;
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    // VK_BUFFER_USAGE_TRANSFER_SRC_BIT specifies that the buffer can be used as the source of a transfer command.
    vkBLASBufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
    // from multiple queue families is supported.
    vkBLASBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    vkBLASBufferCreateInfo.pQueueFamilyIndices = NULL;
    vkBLASBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaBLASBufferAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible free range for the allocation
    // to minimize memory usage and fragmentation, possibly at the expense of allocation time.
    vmaBLASBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // 	Flags that must be set in a Memory Type chosen for an allocation. 
    vmaBLASBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    if (!reserved->accelerationStructureBuffer.create(vkBLASBufferCreateInfo, vmaBLASBufferAllocationCreateInfo))
    {
        OV_LOG_ERROR("Fail to create the AS buffer in the AS with id= %d", getId());
        this->free();
        return false;
    }

#pragma endregion


    //////////////
    // Build AS //
    //////////////

#pragma region AS

    // Structure specifying the parameters of a newly created acceleration structure object
    VkAccelerationStructureCreateInfoKHR accelerationStructureCreateInfo = {};
    accelerationStructureCreateInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    accelerationStructureCreateInfo.pNext = NULL;
    accelerationStructureCreateInfo.createFlags = 0;
    accelerationStructureCreateInfo.buffer = reserved->accelerationStructureBuffer.getVkBuffer();
    accelerationStructureCreateInfo.offset = 0;
    accelerationStructureCreateInfo.size = asBufferSize;
    accelerationStructureCreateInfo.type = type;
    accelerationStructureCreateInfo.deviceAddress = 0;

    // Create a new acceleration structure object
    VkDevice logicalDevice = OvVK::Engine::getInstance().getLogicalDevice().getVkDevice();
    PFN_vkCreateAccelerationStructureKHR pvkCreateAccelerationStructureKHR =
        (PFN_vkCreateAccelerationStructureKHR)vkGetDeviceProcAddr(logicalDevice, "vkCreateAccelerationStructureKHR");

    if (pvkCreateAccelerationStructureKHR(logicalDevice, &accelerationStructureCreateInfo, NULL,
        &reserved->accelerationStructure) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create AS with id: %d.", getId());
        this->free();
        return false;
    }

#pragma endregion

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the scrath buffer use to build the AS.
 * @return The scratch buffer.
 */
const OvVK::Buffer OVVK_API& OvVK::AccelerationStructure::getScratchBuffer() const
{
    return reserved->scratchBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the buffer use to contain the AS.
 * @return The AS buffer
 */
const OvVK::Buffer OVVK_API &OvVK::AccelerationStructure::getASBuffer() const
{
    return reserved->accelerationStructureBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the AS Vulkan handle.
 * @return The Vulkan AS obj.
 */
VkAccelerationStructureKHR OVVK_API OvVK::AccelerationStructure::getVkAS() const
{
    return reserved->accelerationStructure;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the 64-bit device address of the AS buffer.
 * @return Optional std obj containing the device address of the AS buffer.
 */
std::optional<VkDeviceAddress> OVVK_API OvVK::AccelerationStructure::getVkASDeviceAddress() const {

	// Safety net
	if (reserved->accelerationStructure == VK_NULL_HANDLE)
	{
		OV_LOG_ERROR("Can't retriver device address in the AS with id= %d because the VkAccelerationStructureKHR is null.", getId());
	}

	std::optional<VkDeviceAddress> deviceAddress;

	// Get logical device
	VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

	PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR =
		(PFN_vkGetAccelerationStructureDeviceAddressKHR)vkGetDeviceProcAddr(logicalDevice,
			"vkGetAccelerationStructureDeviceAddressKHR");

	VkAccelerationStructureDeviceAddressInfoKHR vkAccelerationStructureDeviceAddressInfoKHR = {};
	vkAccelerationStructureDeviceAddressInfoKHR.sType =
		VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
	vkAccelerationStructureDeviceAddressInfoKHR.accelerationStructure = reserved->accelerationStructure;

	deviceAddress = vkGetAccelerationStructureDeviceAddressKHR(logicalDevice,
		&vkAccelerationStructureDeviceAddressInfoKHR);

	// Done:
	return deviceAddress;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the structure specifying the geometry data used to build or update the AS.
 * @return The structure specifying the geometry data used to build or update the AS.
 */
const VkAccelerationStructureBuildGeometryInfoKHR OVVK_API&
OvVK::AccelerationStructure::getAccelerationStructureBuildGeometryInfo() const
{
    return reserved->accelerationStructureBuildGeometryInfo;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the flag specifing the AS "properties".
 * @return The flag specifing the AS "properties".
 */
VkBuildAccelerationStructureFlagsKHR OVVK_API OvVK::AccelerationStructure::getVkBuildAccelerationStructureFlagsKHR() const
{
    return reserved->buildAccelerationStructureFlagsKHR;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the queue family index of the queue owning the AS buffers.
 * @param queueFamilyIndex The index of the queue owning the AS buffers.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::setQueueFamilyIndex(uint32_t queueFamilyIndex)
{
    if (reserved->scratchBuffer.getVkBuffer() != VK_NULL_HANDLE)
    {
        return reserved->scratchBuffer.setQueueFamilyIndex(queueFamilyIndex) &&
            reserved->accelerationStructureBuffer.setQueueFamilyIndex(queueFamilyIndex);
    }

    return reserved->accelerationStructureBuffer.setQueueFamilyIndex(queueFamilyIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the AS buffers.
 * @param value The value representing the state in which the AS buffers are.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::setStatusSyncObjValue(uint64_t value)
{
    if (reserved->scratchBuffer.getVkBuffer() != VK_NULL_HANDLE)
    {
        return reserved->scratchBuffer.setStatusSyncObjValue(value) &&
            reserved->accelerationStructureBuffer.setStatusSyncObjValue(value);
    }

    return reserved->accelerationStructureBuffer.setStatusSyncObjValue(value);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sync obj for knowing if the AS is lock or unlock.
 * @param fence The sync obj for knowing if the AS is lock or unlock.
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::setLockSyncObj(VkFence fence)
{
    if (reserved->scratchBuffer.getVkBuffer() != VK_NULL_HANDLE)
        return reserved->scratchBuffer.setLockSyncObj(fence) &&
            reserved->accelerationStructureBuffer.setLockSyncObj(fence);

    return reserved->accelerationStructureBuffer.setLockSyncObj(fence);
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value Generic value
 * @param data Generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::AccelerationStructure::render(uint32_t value, void* data) const 
{
    OV_LOG_ERROR("The render method of the AS base class was called.");

    // Done:
    return false;
}

#pragma endregion
