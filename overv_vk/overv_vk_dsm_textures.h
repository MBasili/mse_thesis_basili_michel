/**
 * @file	overv_vk_dsm_textures.h
 * @brief	Vulkan textures descriptor set manager
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling an Vulkan descriptor set manager for textures.
  */
class OVVK_API TexturesDescriptorSetsManager final : public OvVK::DescriptorSetsManager
{
//////////
public: //
//////////

	/**
	 * @brief Definition of binding points.
	 */
	enum class Binding : uint32_t
	{
		textures = 0
	};

	// Const/dest:
	TexturesDescriptorSetsManager(TexturesDescriptorSetsManager const&) = delete;
	virtual ~TexturesDescriptorSetsManager();

	// Operators
	TexturesDescriptorSetsManager& operator=(const TexturesDescriptorSetsManager&) = delete;
	TexturesDescriptorSetsManager& operator=(TexturesDescriptorSetsManager&&) = delete;

	// Singleton:
	static TexturesDescriptorSetsManager& getInstance();

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	bool create() override;
	bool addToUpdateList(const OvVK::Texture& texture);

	// Rendering:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	TexturesDescriptorSetsManager();
};