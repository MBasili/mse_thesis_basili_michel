/**
 * @file	overv_vk_thread_pool.h
 * @brief	ThreadPool to manage multiple threads
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"


/////////////
// #STATIC //
/////////////

// Special values:
OvVK::ThreadPool OvVK::ThreadPool::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief ThreadPool reserved structure.
 */
struct OvVK::ThreadPool::Reserved
{
    std::vector<Thread> threads;

    /**
     * Constructor.
     */
    Reserved()
    {}
};


//////////////////////////////
// BODY OF CLASS ThreadPool //
//////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::ThreadPool::ThreadPool() : reserved(std::make_unique<OvVK::ThreadPool::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::ThreadPool::ThreadPool(const std::string& name) : Ov::Object(name),
reserved(std::make_unique<OvVK::ThreadPool::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::ThreadPool::ThreadPool(ThreadPool&& other) noexcept :
    Ov::Object(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::ThreadPool::~ThreadPool()
{
    OV_LOG_DETAIL("[-]");

    reserved->threads.clear();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Wait until all threads have finished their work items.
 */
void OVVK_API OvVK::ThreadPool::ThreadPool::wait()
{
    for (auto &thread : reserved->threads)
        thread.wait();
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the allocated threads.
 * @param The reference to the vector containing the allocated threads.
 */
std::vector<OvVK::Thread> OVVK_API& OvVK::ThreadPool::ThreadPool::getThreads() const
{
    return reserved->threads;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the number of allocated threads.
 * @return The number of allocated threads.
 */
uint32_t OVVK_API OvVK::ThreadPool::ThreadPool::getThreadCount() const
{
    return reserved->threads.size();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the number of threads to be allocated in this pool.
 * @param count The number of threads to allocate.
 */
void OVVK_API OvVK::ThreadPool::ThreadPool::setThreadCount(uint32_t count)
{
    reserved->threads.clear();
    reserved->threads.resize(count);
}

#pragma endregion