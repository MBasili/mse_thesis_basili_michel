/**
 * @file	overv_vk_dsm_rt.h
 * @brief	Descriptor set manager for ray tracing
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling a descriptor set manager for ray tracing.
  */
class OVVK_API RayTracingDescriptorSetsManager final : public OvVK::DescriptorSetsManager
{
//////////
public: //
//////////

	/**
	 * @brief Definition of binding points.
	 */
	enum class Binding : uint32_t
	{
		tlas = 0
	};

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Struct used to update
	 */
	struct UpdateData
	{
		uint32_t targetFrame;
		std::optional<VkAccelerationStructureKHR> tlas;

		UpdateData() : targetFrame{ 0 }, 
			tlas { std::nullopt }
		{}
	};
#endif

	// Const/dest:
	RayTracingDescriptorSetsManager();
	RayTracingDescriptorSetsManager(RayTracingDescriptorSetsManager&& other);
	RayTracingDescriptorSetsManager(RayTracingDescriptorSetsManager const&) = delete;
	virtual ~RayTracingDescriptorSetsManager();

	// Operators
	RayTracingDescriptorSetsManager& operator=(const RayTracingDescriptorSetsManager&) = delete;
	RayTracingDescriptorSetsManager& operator=(RayTracingDescriptorSetsManager&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	virtual bool create() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool addToUpdateList(const UpdateData& data);
#endif

	// Rendering:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	RayTracingDescriptorSetsManager(const std::string& name);
};
