#pragma once
/**
 * @file	overv_vk_loader.h
 * @brief	Base 3D model loading functionality
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Base 3D model loader.
  */
class OVVK_API Loader final: public Ov::Object
{
//////////
public: //
//////////	   

	// Struct to use multiple thread to record commands.
	struct ThreadData;

	// Struct used to upload objects into the GPU
	struct UploadData {
		// Use id and not reference because the Ovo loader in the overv_core
		// move the object inside the container after calling update method.
		// So lost the reference.

		// Size (bytes) of the staging buffer for the geometries to laod.
		uint64_t geometriesStagingBufferSize;
		// References to geometries to uplaod
		std::list<std::reference_wrapper<OvVK::Geometry>> geometries;
		// Reference to nodes
		std::list<std::reference_wrapper<Ov::Node>> nodes;
		// Reference to materials
		std::list<std::reference_wrapper<OvVK::Material>> materials;
		// Reference to lights
		std::list<std::reference_wrapper<OvVK::Light>> lights;
		// Reference to Textures
		std::list<std::reference_wrapper<OvVK::Texture>> textures;

		UploadData() : geometriesStagingBufferSize{ 0 }
		{}
	};

	// Const/Dest:
	Loader();
	Loader(Loader&& other) noexcept;
	Loader(Loader const&) = delete;
	~Loader();

	// Operators:
	Loader& operator=(const Loader&) = delete;
	Loader& operator=(Loader&&) = delete;

	// Get/set:
	bool tooManyMaterials();

	// Loading methods:
	bool load(const std::string& filename, unsigned int pFlags);
	bool loadLight(void* light, OvVK::Light& l, Ov::Node& node);
	bool loadGeometry(void* mesh, OvVK::Geometry& g);
	bool loadMaterial(void* material, OvVK::Material& m);
	bool loadTexture2D(std::string imageName, OvVK::Texture2D& t, Ov::Texture::Type type);
	bool loadSampler(OvVK::Texture& t, OvVK::Sampler& s, void* textureMapMode = nullptr);
	Ov::Node& loadScene(OvVK::Container& container, void* current = nullptr);

	// Just tmp debug:
	void showParamsUsed(unsigned int pFlags);

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Upload
	bool uploadToGpu(OvVK::Container& container);

	// SetUp for upload
	bool uploadGeometries(OvVK::Loader::ThreadData& threadData,
		std::list<std::reference_wrapper<OvVK::Geometry>>& geometries,
		OvVK::Buffer& geometriesStagingBuffer);
	bool uploadTexture(OvVK::Loader::ThreadData& threadData,
		std::list<std::reference_wrapper<OvVK::Texture>>& textures);

	// Const/dest:
	Loader(const std::string& name);
};