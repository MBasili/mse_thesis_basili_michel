/**
 * @file	overv_vk_dsm_storage.cpp
 * @brief	Descriptor set manager for storage
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Storage descriptor set manager reserved structure.
 */
struct OvVK::StorageDescriptorSetsManager::Reserved
{
    std::vector<std::pair<std::reference_wrapper<const OvVK::Buffer>, bool>> globalUniformDeviceBuffers;
    std::vector<std::pair<std::reference_wrapper<const OvVK::Buffer>, bool>> materialsDeviceBuffers;
    std::vector<std::pair<std::reference_wrapper<const OvVK::Buffer>, bool>> lightsDeviceBuffers;
    std::vector<std::pair<std::reference_wrapper<const OvVK::Buffer>, bool>> rtObjDescsDeviceBuffers;

    /**
     * Constructor.
     */
    Reserved()
    {
        std::reference_wrapper<const OvVK::Buffer> buffer = OvVK::Buffer::empty;

        // Init vectors
        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) 
        {
            globalUniformDeviceBuffers.push_back(std::make_pair(buffer, false));
            materialsDeviceBuffers.push_back(std::make_pair(buffer, false));
            lightsDeviceBuffers.push_back(std::make_pair(buffer, false));
            rtObjDescsDeviceBuffers.push_back(std::make_pair(buffer, false));
        }
    }
};


////////////////////////////////////////////////
// BODY OF CLASS StorageDescriptorSetsManager //
////////////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::StorageDescriptorSetsManager::StorageDescriptorSetsManager() :
    reserved(std::make_unique<OvVK::StorageDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::StorageDescriptorSetsManager::StorageDescriptorSetsManager(const std::string& name) :
    OvVK::DescriptorSetsManager(name), reserved(std::make_unique<OvVK::StorageDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::StorageDescriptorSetsManager::StorageDescriptorSetsManager(StorageDescriptorSetsManager&& other) :
    OvVK::DescriptorSetsManager(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::StorageDescriptorSetsManager::~StorageDescriptorSetsManager()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::StorageDescriptorSetsManager::init()
{
    if (this->OvVK::DescriptorSetsManager::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::StorageDescriptorSetsManager::free()
{
    if (this->OvVK::DescriptorSetsManager::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets and descriptor sets layouts.
 * @return TF
 */
bool OVVK_API OvVK::StorageDescriptorSetsManager::create() 
{
    // Init
    if (this->init() == false)
        return false;

    /////////////////////
    // Descriptor Pool //
    /////////////////////

    // Create descriptorPool
    std::vector<VkDescriptorPoolSize> descriptorPoolSizes(4);

    // GlobalUniform
    descriptorPoolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorPoolSizes[0].descriptorCount = OvVK::Engine::nrFramesInFlight;

    // Material
    descriptorPoolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorPoolSizes[1].descriptorCount = OvVK::Engine::nrFramesInFlight;

    // Light
    descriptorPoolSizes[2].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorPoolSizes[2].descriptorCount = OvVK::Engine::nrFramesInFlight;

    // ObjDesc
    descriptorPoolSizes[3].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorPoolSizes[3].descriptorCount = OvVK::Engine::nrFramesInFlight;


    ///////////////////////////
    // Descriptor set layout //
    ///////////////////////////

    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings(4);

    // GlobalUniform
    descriptorSetLayoutBindings[0].binding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::globalUniform);
    descriptorSetLayoutBindings[0].descriptorCount = 1;
    descriptorSetLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorSetLayoutBindings[0].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[0].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;

    // Material
    descriptorSetLayoutBindings[1].binding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::materials);
    descriptorSetLayoutBindings[1].descriptorCount = 1;
    descriptorSetLayoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorSetLayoutBindings[1].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[1].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;

    // Light
    descriptorSetLayoutBindings[2].binding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::lights);
    descriptorSetLayoutBindings[2].descriptorCount = 1;
    descriptorSetLayoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorSetLayoutBindings[2].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[2].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;

    // ObjDesc
    descriptorSetLayoutBindings[3].binding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::rtObjDescs);
    descriptorSetLayoutBindings[3].descriptorCount = 1;
    descriptorSetLayoutBindings[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorSetLayoutBindings[3].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[3].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;


    // Create
    return this->OvVK::DescriptorSetsManager::create(descriptorPoolSizes, descriptorSetLayoutBindings,
        OvVK::Engine::nrFramesInFlight);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds an update to update the storage DSM.
 * @param data The reference to a structure containing the new buffers address.
 * @return TF
 */
bool OVVK_API OvVK::StorageDescriptorSetsManager::addToUpdateList(const UpdateData& data)
{
    // Safety net
    if (data.targetFrame >= OvVK::Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed target frame is out of range in storageDSM with id= %d.", getId());
        return false;
    }

    // Set buffers
    if (data.globalUniformDeviceBuffer.has_value())
    {
        reserved->globalUniformDeviceBuffers[data.targetFrame] =
            std::make_pair(data.globalUniformDeviceBuffer.value(), true);
    }

    if (data.rtObjDescsDeviceBuffer.has_value())
    {
        reserved->rtObjDescsDeviceBuffers[data.targetFrame] =
            std::make_pair(data.rtObjDescsDeviceBuffer.value(), true);
    }

    if (data.materialsDeviceBuffer.has_value())
    {
        reserved->materialsDeviceBuffers[data.targetFrame] =
            std::make_pair(data.materialsDeviceBuffer.value(), true);
    }

    if (data.lightsDeviceBuffer.has_value())
    {
        reserved->lightsDeviceBuffers[data.targetFrame] =
            std::make_pair(data.lightsDeviceBuffer.value(), true);
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::StorageDescriptorSetsManager::render(uint32_t value, void* data) const
{
	// Engine
	std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Frame in flight
    uint32_t frameInFlight = engine.get().getFrameInFlight();


	/////////////////////////////
	// Write to descriptor set //
	/////////////////////////////

	std::vector<VkWriteDescriptorSet> writeDescriptorSets;

    VkDescriptorBufferInfo globalUniformDescriptorBufferInfo = {};
    VkDescriptorBufferInfo materialsDescriptorBufferInfo = {};
    VkDescriptorBufferInfo lightsDescriptorBufferInfo = {};
    VkDescriptorBufferInfo rtObjDescsDescriptorBufferInfo = {};


	///////////////////
	// GlobalUniform //
	///////////////////

	if (reserved->globalUniformDeviceBuffers[frameInFlight].second)
	{

        globalUniformDescriptorBufferInfo.buffer =
            reserved->globalUniformDeviceBuffers[frameInFlight].first.get().getVkBuffer();
        globalUniformDescriptorBufferInfo.offset = 0;
        globalUniformDescriptorBufferInfo.range = VK_WHOLE_SIZE;

		VkWriteDescriptorSet& globalUniformWriteDescriptorSet = writeDescriptorSets.emplace_back();
		globalUniformWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		globalUniformWriteDescriptorSet.pNext = NULL;
		globalUniformWriteDescriptorSet.dstSet = getVkDescriptorSet(frameInFlight);
		globalUniformWriteDescriptorSet.dstBinding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::globalUniform);
		globalUniformWriteDescriptorSet.dstArrayElement = 0;
		globalUniformWriteDescriptorSet.descriptorCount = 1;
		globalUniformWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		globalUniformWriteDescriptorSet.pImageInfo = NULL;
		globalUniformWriteDescriptorSet.pBufferInfo = &globalUniformDescriptorBufferInfo;
		globalUniformWriteDescriptorSet.pTexelBufferView = NULL;

		reserved->globalUniformDeviceBuffers[frameInFlight].second = false;
	}

    ///////////////
    // Materials //
    ///////////////

    if (reserved->materialsDeviceBuffers[frameInFlight].second)
    {
        materialsDescriptorBufferInfo.buffer =
            reserved->materialsDeviceBuffers[frameInFlight].first.get().getVkBuffer();
        materialsDescriptorBufferInfo.offset = 0;
        materialsDescriptorBufferInfo.range = VK_WHOLE_SIZE;

        VkWriteDescriptorSet& globalUniformWriteDescriptorSet = writeDescriptorSets.emplace_back();
        globalUniformWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        globalUniformWriteDescriptorSet.pNext = NULL;
        globalUniformWriteDescriptorSet.dstSet = getVkDescriptorSet(frameInFlight);
        globalUniformWriteDescriptorSet.dstBinding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::materials);
        globalUniformWriteDescriptorSet.dstArrayElement = 0;
        globalUniformWriteDescriptorSet.descriptorCount = 1;
        globalUniformWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;;
        globalUniformWriteDescriptorSet.pImageInfo = NULL;
        globalUniformWriteDescriptorSet.pBufferInfo = &materialsDescriptorBufferInfo;
        globalUniformWriteDescriptorSet.pTexelBufferView = NULL;

        reserved->materialsDeviceBuffers[frameInFlight].second = false;
    }

    ///////////
    // Light //
    ///////////

    if (reserved->lightsDeviceBuffers[frameInFlight].second)
    {
        lightsDescriptorBufferInfo.buffer =
            reserved->lightsDeviceBuffers[frameInFlight].first.get().getVkBuffer();
        lightsDescriptorBufferInfo.offset = 0;
        lightsDescriptorBufferInfo.range = VK_WHOLE_SIZE;

        VkWriteDescriptorSet& globalUniformWriteDescriptorSet = writeDescriptorSets.emplace_back();
        globalUniformWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        globalUniformWriteDescriptorSet.pNext = NULL;
        globalUniformWriteDescriptorSet.dstSet = getVkDescriptorSet(frameInFlight);
        globalUniformWriteDescriptorSet.dstBinding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::lights);
        globalUniformWriteDescriptorSet.dstArrayElement = 0;
        globalUniformWriteDescriptorSet.descriptorCount = 1;
        globalUniformWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;;
        globalUniformWriteDescriptorSet.pImageInfo = NULL;
        globalUniformWriteDescriptorSet.pBufferInfo = &lightsDescriptorBufferInfo;
        globalUniformWriteDescriptorSet.pTexelBufferView = NULL;

        reserved->lightsDeviceBuffers[frameInFlight].second = false;
    }

	////////////////
	// RtObjDescs //
	////////////////

	if (reserved->rtObjDescsDeviceBuffers[frameInFlight].second)
	{
		rtObjDescsDescriptorBufferInfo.buffer =
			reserved->rtObjDescsDeviceBuffers[frameInFlight].first.get().getVkBuffer();
		rtObjDescsDescriptorBufferInfo.offset = 0;
		rtObjDescsDescriptorBufferInfo.range = VK_WHOLE_SIZE;

		VkWriteDescriptorSet& globalUniformWriteDescriptorSet = writeDescriptorSets.emplace_back();
		globalUniformWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		globalUniformWriteDescriptorSet.pNext = NULL;
		globalUniformWriteDescriptorSet.dstSet = getVkDescriptorSet(frameInFlight);
		globalUniformWriteDescriptorSet.dstBinding = static_cast<uint32_t>(StorageDescriptorSetsManager::Binding::rtObjDescs);
		globalUniformWriteDescriptorSet.dstArrayElement = 0;
		globalUniformWriteDescriptorSet.descriptorCount = 1;
		globalUniformWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;;
		globalUniformWriteDescriptorSet.pImageInfo = NULL;
		globalUniformWriteDescriptorSet.pBufferInfo = &rtObjDescsDescriptorBufferInfo;
		globalUniformWriteDescriptorSet.pTexelBufferView = NULL;

		reserved->rtObjDescsDeviceBuffers[frameInFlight].second = false;
	}


    // Update
    if (writeDescriptorSets.size() > 0)
    {
        // This can generate a race condition and undefined behavior if the descriptor set to 
        // update is in use by a command buffer that�s being executed in a queue.
        // cannot modify a descriptor set unless you know that the GPU is finished with it.
        // Update ad the beginning of the frame.
        vkUpdateDescriptorSets(engine.get().getLogicalDevice().getVkDevice(), static_cast<uint32_t>(writeDescriptorSets.size()),
            writeDescriptorSets.data(), 0, NULL);
    }

    // Done:
    return true;
}

#pragma endregion