/**
 * @file	overv_vk_rt_pipeline.cpp
 * @brief	Vulkan RayTracing pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum:
#include "magic_enum.hpp"

// Thread:
#include "overv_vk_thread_pool.h"

// C/C++
#include <functional>


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Pipeline class reserved structure.
 */
struct OvVK::RayTracingPipeline::Reserved
{
    // Pipeline shaders
    OvVK::Shader rayGenShader;
    std::map<std::string, OvVK::Shader> intersectionShaders;
    std::map<std::string, OvVK::Shader> anyHitShaders;
    std::map<std::string, OvVK::Shader> closestHitShaders;
    std::map<std::string, OvVK::Shader> callableShaders;
    std::map<std::string, OvVK::Shader> missShaders;

    // ShaderGroups
    OvVK::RTShaderGroup rayGenShaderGroup;
    std::map<std::string, OvVK::RTShaderGroup> hitShaderGroups;
    std::map<std::string, OvVK::RTShaderGroup> callableShaderGroups;
    std::map<std::string, OvVK::RTShaderGroup> missShaderGroups;

    // GroupShader Handles
    std::vector<uint8_t> shaderGroupsHandleStorage;

    // ShaderBindingTable
    OvVK::Buffer sbtHostBuffer[OvVK::Engine::nrFramesInFlight];
    OvVK::Buffer sbtDeviceBuffer[OvVK::Engine::nrFramesInFlight];

    // STB beginning for all the type of shaders
    VkStridedDeviceAddressRegionKHR raygenShaderBindingTable;
    VkStridedDeviceAddressRegionKHR hitShaderBindingTable;
    VkStridedDeviceAddressRegionKHR callableShaderBindingTable;
    VkStridedDeviceAddressRegionKHR missShaderBindingTable;

    // Descriptor Set Manager
    OvVK::RayTracingDescriptorSetsManager rayTracingDescriptorSetsManager;

    /**
     * Constructor
     */
    Reserved() : raygenShaderBindingTable{ {} },
        hitShaderBindingTable{ {} },
        callableShaderBindingTable{ {} },
        missShaderBindingTable{ {} }
    {}
};


//////////////////////////////////////
// BODY OF CLASS RayTracingPipeline //
//////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::RayTracingPipeline::RayTracingPipeline() :
    reserved(std::make_unique<OvVK::RayTracingPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name RTPipeline name
 */
OVVK_API OvVK::RayTracingPipeline::RayTracingPipeline(const std::string& name) : OvVK::Pipeline(name),
reserved(std::make_unique<OvVK::RayTracingPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::RayTracingPipeline::RayTracingPipeline(RayTracingPipeline&& other) :
    OvVK::Pipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::RayTracingPipeline::~RayTracingPipeline()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes pipeline.
 * @return TF
 */
bool OVVK_API OvVK::RayTracingPipeline::init()
{
    if (this->OvVK::Pipeline::init() == false)
        return false;


    // Clear shaders
    reserved->closestHitShaders.clear();
    reserved->anyHitShaders.clear();
    reserved->intersectionShaders.clear();
    reserved->callableShaders.clear();
    reserved->missShaders.clear();

    // Clear groups
    reserved->hitShaderGroups.clear();
    reserved->callableShaderGroups.clear();
    reserved->missShaderGroups.clear();

    // Clear the shader handle storage
    reserved->shaderGroupsHandleStorage.clear();

    // Free buffer SBT
    for (uint32_t i = 0; i < OvVK::Engine::nrFramesInFlight; i++) {
        reserved->sbtDeviceBuffer[i].free();
        reserved->sbtHostBuffer[i].free();
    }

    // Free Descriptor set managet
    reserved->rayTracingDescriptorSetsManager.free();


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees pipeline.
 * @return TF
 */
bool OVVK_API OvVK::RayTracingPipeline::free() 
{
    if (this->OvVK::Pipeline::free() == false)
        return false;


    // Clear shaders
    reserved->closestHitShaders.clear();
    reserved->anyHitShaders.clear();
    reserved->intersectionShaders.clear();
    reserved->callableShaders.clear();
    reserved->missShaders.clear();

    // Clear groups
    reserved->hitShaderGroups.clear();
    reserved->callableShaderGroups.clear();
    reserved->missShaderGroups.clear();

    // Clear the shader handle storage
    reserved->shaderGroupsHandleStorage.clear();

    // Free buffer SBT
    for (uint32_t i = 0; i < OvVK::Engine::nrFramesInFlight; i++) {
        reserved->sbtDeviceBuffer[i].free();
        reserved->sbtHostBuffer[i].free();
    }

    // Free Descriptor set managet
    reserved->rayTracingDescriptorSetsManager.free();


    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create the rt pipeline obj.
 * @param maxPipelineRayPayloadSize The maximum payload size in bytes used by any shader in the pipeline.
 * @param maxPipelineRayHitAttributeSize The maximum attribute structure size in bytes used by any
 * shader in the pipeline.
 * @param maxPipelineRayRecursionDepth The maximum recursion depth of shaders executed by this pipeline.
 * @param flags Controlling how a pipeline is created
 * @return TF
 */
bool OVVK_API OvVK::RayTracingPipeline::createRTPipeline(VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo,
    uint32_t maxPipelineRayRecursionDepth, VkPipelineCreateFlags flags)
{
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // LogicalDevice
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();


    uint32_t maxRayRecursionDepth = getMaxRayRecursionDepth();
    uint32_t shaderGroupHandleStride = getShaderGroupHandleStride();
    uint32_t shaderGroupBaseAlignment = getShaderGroupBaseAlignment();
    uint32_t shaderGroupHandleSize = getShaderGroupHandleSize();


    ///////////////////
    // Shader module //
    ///////////////////

    // Shaders modules
    VkShaderModule raygenShaderModule;
    std::map<uint32_t, VkShaderModule> intersectionShadersModules;
    std::map<uint32_t, VkShaderModule> closestHitShadersModules;
    std::map<uint32_t, VkShaderModule> anyHitShadersModules;
    std::map<uint32_t, VkShaderModule> callableShadersModules;
    std::map<uint32_t, VkShaderModule> missShadersModules;

#pragma region ShaderModules

    bool exit = false;


    // Raygen module
    VkShaderModuleCreateInfo rgenShaderModuleCreateInfo = {};
    rgenShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    rgenShaderModuleCreateInfo.codeSize = reserved->rayGenShader.getNrOfBytes();
    rgenShaderModuleCreateInfo.pCode = reserved->rayGenShader.getSPIRVBinary().data();
    if (vkCreateShaderModule(logicalDevice.get().getVkDevice(),
        &rgenShaderModuleCreateInfo, NULL, &raygenShaderModule) != VK_SUCCESS) {
        OV_LOG_ERROR("Fail to create module for rayGen shader in the pipeline with id= %d.", getId());
        exit = true;
    }

    // Intersection hit modules
    VkShaderModuleCreateInfo intersectionShaderModuleCreateInfo = {};
    intersectionShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    for (auto const& [key, shader] : reserved->intersectionShaders)
    {
        intersectionShaderModuleCreateInfo.codeSize = shader.getNrOfBytes();
        intersectionShaderModuleCreateInfo.pCode = shader.getSPIRVBinary().data();

        VkShaderModule intersectionShaderModule;
        if (vkCreateShaderModule(logicalDevice.get().getVkDevice(),
            &intersectionShaderModuleCreateInfo, NULL, &intersectionShaderModule) != VK_SUCCESS) {
            OV_LOG_ERROR(R"(Fail to create module for intersection shader with id= %d
 in the pipeline with id= %d.)", shader.getId(), getId());
            exit = true;
            break;
        }

        intersectionShadersModules.insert(std::make_pair(shader.getId(), intersectionShaderModule));
    }

    // Any hit modules
    VkShaderModuleCreateInfo rahitShaderModuleCreateInfo = {};
    rahitShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    for (auto const& [key, shader] : reserved->anyHitShaders)
    {
        rahitShaderModuleCreateInfo.codeSize = shader.getNrOfBytes();
        rahitShaderModuleCreateInfo.pCode = shader.getSPIRVBinary().data();

        VkShaderModule rahitShaderModule;
        if (vkCreateShaderModule(logicalDevice.get().getVkDevice(),
            &rahitShaderModuleCreateInfo, NULL, &rahitShaderModule) != VK_SUCCESS) {
            OV_LOG_ERROR(R"(Fail to create module for anyHit shader with id= %d
in the pipeline with id = % d.)", shader.getId(), getId());
            exit = true;
            break;
        }

        anyHitShadersModules.insert(std::make_pair(shader.getId(), rahitShaderModule));
    }

    // Closest hit modules
    VkShaderModuleCreateInfo rchitShaderModuleCreateInfo = {};
    rchitShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    for (auto const& [key, shader] : reserved->closestHitShaders)
    {
        rchitShaderModuleCreateInfo.codeSize = shader.getNrOfBytes();
        rchitShaderModuleCreateInfo.pCode = shader.getSPIRVBinary().data();

        VkShaderModule rchitShaderModule;
        if (vkCreateShaderModule(logicalDevice.get().getVkDevice(),
            &rchitShaderModuleCreateInfo, NULL, &rchitShaderModule) != VK_SUCCESS) {
            OV_LOG_ERROR(R"(Fail to create module for closestHit shader with id= %d
in the pipeline with id = % d.)", shader.getId(), getId());
            exit = true;
            break;
        }

        closestHitShadersModules.insert(std::make_pair(shader.getId(), rchitShaderModule));
    }

    // Callable modules
    VkShaderModuleCreateInfo callableShaderModuleCreateInfo = {};
    callableShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    for (auto const& [key, shader] : reserved->callableShaders)
    {
        callableShaderModuleCreateInfo.codeSize = shader.getNrOfBytes();
        callableShaderModuleCreateInfo.pCode = shader.getSPIRVBinary().data();

        VkShaderModule callableShaderModule;
        if (vkCreateShaderModule(logicalDevice.get().getVkDevice(),
            &callableShaderModuleCreateInfo, NULL, &callableShaderModule) != VK_SUCCESS) {
            OV_LOG_ERROR(R"(Fail to create module for callable shader with id= %d
in the pipeline with id = % d.)", shader.getId(), getId());
            exit = true;
            break;
        }

        callableShadersModules.insert(std::make_pair(shader.getId(), callableShaderModule));
    }

    // miss modules
    VkShaderModuleCreateInfo rmissShaderModuleCreateInfo = {};
    rmissShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    for (auto const& [key, shader] : reserved->missShaders)
    {
        rmissShaderModuleCreateInfo.codeSize = shader.getNrOfBytes();
        rmissShaderModuleCreateInfo.pCode = shader.getSPIRVBinary().data();

        VkShaderModule rmissShaderModule;
        if (vkCreateShaderModule(logicalDevice.get().getVkDevice(),
            &rmissShaderModuleCreateInfo, NULL, &rmissShaderModule) != VK_SUCCESS) {
            OV_LOG_ERROR(R"(Fail to create module for miss shader with id= %d
in the pipeline with id = % d.)", shader.getId(), getId());
            exit = true;
            break;
        }

        missShadersModules.insert(std::make_pair(shader.getId(), rmissShaderModule));
    }

    // Destroy modules
    if (exit)
    {
        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), raygenShaderModule, NULL);

        for (auto [key, value] : intersectionShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : anyHitShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : closestHitShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : callableShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : missShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        this->free();
        return false;
    }

#pragma endregion


    ///////////////////////
    // Shader Stage Info //
    ///////////////////////

    std::map<uint32_t, uint32_t> shaderStagesPos;
    std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

#pragma region ShaderStageInfo

    // RayGen shader stage info
    VkPipelineShaderStageCreateInfo raygenShaderStageInfo = {};
    raygenShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    raygenShaderStageInfo.stage = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
    raygenShaderStageInfo.module = raygenShaderModule;
    raygenShaderStageInfo.pName = "main";

    shaderStagesPos.insert(std::make_pair(reserved->rayGenShader.getId(), shaderStages.size()));
    shaderStages.push_back(raygenShaderStageInfo);

    // Intersection shader stage info
    VkPipelineShaderStageCreateInfo intersectionShaderStageInfo = {};
    intersectionShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    intersectionShaderStageInfo.stage = VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
    intersectionShaderStageInfo.pName = "main";

    for (auto const& [key, value] : intersectionShadersModules)
    {
        intersectionShaderStageInfo.module = value;

        shaderStagesPos.insert(std::make_pair(key, shaderStages.size()));
        shaderStages.push_back(intersectionShaderStageInfo);
    }

    // Any hit shader stage info
	VkPipelineShaderStageCreateInfo anyhitShaderStageInfo = {};
	anyhitShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	anyhitShaderStageInfo.stage = VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
    anyhitShaderStageInfo.pName = "main";

	for (auto const& [key, value] : anyHitShadersModules)
	{
		anyhitShaderStageInfo.module = value;

		shaderStagesPos.insert(std::make_pair(key, shaderStages.size()));
		shaderStages.push_back(anyhitShaderStageInfo);
	}

    // Closest hit shader stage info
    VkPipelineShaderStageCreateInfo closesthitShaderStageInfo = {};
    closesthitShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    closesthitShaderStageInfo.stage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
    closesthitShaderStageInfo.pName = "main";

    for (auto const& [key, value] : closestHitShadersModules)
    {
        closesthitShaderStageInfo.module = value;

        shaderStagesPos.insert(std::make_pair(key, shaderStages.size()));
        shaderStages.push_back(closesthitShaderStageInfo);
    }

    // Callable shader stage info 
    VkPipelineShaderStageCreateInfo callableShaderStageInfo = {};
    callableShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    callableShaderStageInfo.stage = VK_SHADER_STAGE_CALLABLE_BIT_KHR;
    callableShaderStageInfo.pName = "main";

    for (auto const& [key, value] : callableShadersModules)
    {
        callableShaderStageInfo.module = value;

        shaderStagesPos.insert(std::make_pair(key, shaderStages.size()));
        shaderStages.push_back(callableShaderStageInfo);
    }

    // Miss shader stage info 
    VkPipelineShaderStageCreateInfo rmissShaderStageInfo = {};
    rmissShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    rmissShaderStageInfo.stage = VK_SHADER_STAGE_MISS_BIT_KHR;
    rmissShaderStageInfo.pName = "main";

    for (auto const& [key, value] : missShadersModules)
    {
        rmissShaderStageInfo.module = value;

        shaderStagesPos.insert(std::make_pair(key, shaderStages.size()));
        shaderStages.push_back(rmissShaderStageInfo);
    }

#pragma endregion


    //////////////////
    // shaderGroups //
    //////////////////

    std::vector<VkRayTracingShaderGroupCreateInfoKHR> shaderGroupCreateInfos;

#pragma region ShaderGroupsInfos


    // Raygen shaderGroupsInfo
    VkRayTracingShaderGroupCreateInfoKHR rayGenShaderGroupCreateInfos;
    rayGenShaderGroupCreateInfos.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
    rayGenShaderGroupCreateInfos.pNext = NULL;
    rayGenShaderGroupCreateInfos.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
    rayGenShaderGroupCreateInfos.generalShader = shaderStagesPos[reserved->rayGenShader.getId()];
    rayGenShaderGroupCreateInfos.closestHitShader = VK_SHADER_UNUSED_KHR;
    rayGenShaderGroupCreateInfos.anyHitShader = VK_SHADER_UNUSED_KHR;
    rayGenShaderGroupCreateInfos.intersectionShader = VK_SHADER_UNUSED_KHR;
    rayGenShaderGroupCreateInfos.pShaderGroupCaptureReplayHandle = NULL;

    reserved->rayGenShaderGroup.setPositionInPipeline(shaderGroupCreateInfos.size());
    shaderGroupCreateInfos.push_back(rayGenShaderGroupCreateInfos);

    // Hit shaderGroupsInfo
	VkRayTracingShaderGroupCreateInfoKHR hitShaderGroupCreateInfos;
	hitShaderGroupCreateInfos.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
	hitShaderGroupCreateInfos.pNext = NULL;
	hitShaderGroupCreateInfos.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
	hitShaderGroupCreateInfos.generalShader = VK_SHADER_UNUSED_KHR;
	hitShaderGroupCreateInfos.anyHitShader = VK_SHADER_UNUSED_KHR;
	hitShaderGroupCreateInfos.intersectionShader = VK_SHADER_UNUSED_KHR;
	hitShaderGroupCreateInfos.pShaderGroupCaptureReplayHandle = NULL;

	for (auto& [key, shaderGroup] : reserved->hitShaderGroups)
	{
        // Intersection
        std::reference_wrapper<const OvVK::Shader> shader = shaderGroup.getShader(Ov::Shader::Type::intersection);
        if (shader.get() != OvVK::Shader::empty)
            hitShaderGroupCreateInfos.intersectionShader = shaderStagesPos[shader.get().getId()];

        // Any hit
        shader = shaderGroup.getShader(Ov::Shader::Type::any_hit);
        if (shader.get() != OvVK::Shader::empty)
            hitShaderGroupCreateInfos.anyHitShader = shaderStagesPos[shader.get().getId()];

		// Closest hit
		shader = shaderGroup.getShader(Ov::Shader::Type::closes_hit);
		if (shader.get() != OvVK::Shader::empty)
			hitShaderGroupCreateInfos.closestHitShader = shaderStagesPos[shader.get().getId()];

		// Set shaderGroupInfo
		shaderGroup.setPositionInPipeline(shaderGroupCreateInfos.size());
		shaderGroupCreateInfos.push_back(hitShaderGroupCreateInfos);
	}


    // Callable shaderGroupsInfos
    VkRayTracingShaderGroupCreateInfoKHR callableShaderGroupCreateInfos;
    callableShaderGroupCreateInfos.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
    callableShaderGroupCreateInfos.pNext = NULL;
    callableShaderGroupCreateInfos.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
    callableShaderGroupCreateInfos.closestHitShader = VK_SHADER_UNUSED_KHR;
    callableShaderGroupCreateInfos.anyHitShader = VK_SHADER_UNUSED_KHR;
    callableShaderGroupCreateInfos.intersectionShader = VK_SHADER_UNUSED_KHR;
    callableShaderGroupCreateInfos.pShaderGroupCaptureReplayHandle = NULL;

    for (auto& [key, shaderGroup] : reserved->callableShaderGroups)
    {
        std::reference_wrapper<const OvVK::Shader> shader = shaderGroup.getShader(Ov::Shader::Type::callable);
        if (shader.get() != OvVK::Shader::empty)
            callableShaderGroupCreateInfos.generalShader = shaderStagesPos[shader.get().getId()];

        shaderGroup.setPositionInPipeline(shaderGroupCreateInfos.size());
        shaderGroupCreateInfos.push_back(callableShaderGroupCreateInfos);
    }


    // Miss shaderGroupsInfos
    VkRayTracingShaderGroupCreateInfoKHR missHitShaderGroupCreateInfos;
	missHitShaderGroupCreateInfos.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
	missHitShaderGroupCreateInfos.pNext = NULL;
	missHitShaderGroupCreateInfos.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
	missHitShaderGroupCreateInfos.closestHitShader = VK_SHADER_UNUSED_KHR;
	missHitShaderGroupCreateInfos.anyHitShader = VK_SHADER_UNUSED_KHR;
	missHitShaderGroupCreateInfos.intersectionShader = VK_SHADER_UNUSED_KHR;
	missHitShaderGroupCreateInfos.pShaderGroupCaptureReplayHandle = NULL;

	for (auto& [key, shaderGroup] : reserved->missShaderGroups)
	{
		std::reference_wrapper<const OvVK::Shader> shader = shaderGroup.getShader(Ov::Shader::Type::miss);
		if (shader.get() != OvVK::Shader::empty)
			missHitShaderGroupCreateInfos.generalShader = shaderStagesPos[shader.get().getId()];

		shaderGroup.setPositionInPipeline(shaderGroupCreateInfos.size());
		shaderGroupCreateInfos.push_back(missHitShaderGroupCreateInfos);
	}


#pragma endregion


    ////////////////////
    // PipelineLayout //
    ////////////////////

    // Create the ray tracing descriptor set manager
    reserved->rayTracingDescriptorSetsManager.create();

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts(pipelineLayoutCreateInfo.setLayoutCount + 1);
    for (uint32_t c = 0; c < descriptorSetLayouts.size(); c++) {
        descriptorSetLayouts[c] = pipelineLayoutCreateInfo.pSetLayouts[c];
    }
    descriptorSetLayouts[descriptorSetLayouts.size() - 1] = reserved->rayTracingDescriptorSetsManager.getVkDescriptorSetLayout();

    pipelineLayoutCreateInfo.setLayoutCount = descriptorSetLayouts.size();
    pipelineLayoutCreateInfo.pSetLayouts = descriptorSetLayouts.data();

    if (vkCreatePipelineLayout(logicalDevice.get().getVkDevice(),
        &pipelineLayoutCreateInfo, NULL, getVkPipelineLayoutPointer()) != VK_SUCCESS) {
        OV_LOG_ERROR("Fail to create pipelineLayout in pipeline with id= %d.", getId());
        this->free();
        return false;
    }


    //////////////
    // Pipeline //
    //////////////

#pragma region Pipeline

    // Destroy previous pipeline if any
    VkPipeline rtPipeline = getVkPipeline();
    if (rtPipeline != VK_NULL_HANDLE) 
        vkDestroyPipeline(Engine::getInstance().getLogicalDevice().getVkDevice(), rtPipeline, NULL);
   
    VkPipelineLibraryCreateInfoKHR pipelineLibraryCreateInfo = {};
    pipelineLibraryCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR;
    pipelineLibraryCreateInfo.pNext = NULL;
    pipelineLibraryCreateInfo.libraryCount = 0;
    pipelineLibraryCreateInfo.pLibraries = NULL;

    VkRayTracingPipelineCreateInfoKHR rayPipelineCreateInfo = {};
    rayPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
    rayPipelineCreateInfo.pNext = NULL;
    rayPipelineCreateInfo.flags = flags;
    rayPipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
    rayPipelineCreateInfo.pStages = shaderStages.data();
    rayPipelineCreateInfo.groupCount = static_cast<uint32_t>(shaderGroupCreateInfos.size());
    rayPipelineCreateInfo.pGroups = shaderGroupCreateInfos.data();
    rayPipelineCreateInfo.maxPipelineRayRecursionDepth = maxPipelineRayRecursionDepth;
    rayPipelineCreateInfo.pLibraryInfo = &pipelineLibraryCreateInfo;
    rayPipelineCreateInfo.pLibraryInterface = VK_NULL_HANDLE;
    rayPipelineCreateInfo.layout = getVkPipelineLayout();
    rayPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
    rayPipelineCreateInfo.basePipelineIndex = -1;

    PFN_vkCreateRayTracingPipelinesKHR pvkCreateRayTracingPipelinesKHR =
        (PFN_vkCreateRayTracingPipelinesKHR)vkGetDeviceProcAddr(logicalDevice.get().getVkDevice(),
            "vkCreateRayTracingPipelinesKHR");

    if (pvkCreateRayTracingPipelinesKHR(logicalDevice.get().getVkDevice(), VK_NULL_HANDLE, VK_NULL_HANDLE,
        1, &rayPipelineCreateInfo, NULL, getVkPipelinePointer()) != VK_SUCCESS) {
        OV_LOG_ERROR("Fail to create the pipeline with id= %d.", getId());

        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), raygenShaderModule, NULL);

        for (auto [key, value] : intersectionShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : anyHitShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : closestHitShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : callableShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        for (auto [key, value] : missShadersModules)
            vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

        this->free();
        return false;
    }


#pragma endregion


    //////////////////////////////////
    // Retrieve ShaderGroupSHandles //
    //////////////////////////////////

#pragma region ShaderGroupSHandles


    PFN_vkGetRayTracingShaderGroupHandlesKHR pvkGetRayTracingShaderGroupHandlesKHR =
        (PFN_vkGetRayTracingShaderGroupHandlesKHR)vkGetDeviceProcAddr(logicalDevice.get().getVkDevice(),
            "vkGetRayTracingShaderGroupHandlesKHR");

    reserved->shaderGroupsHandleStorage.resize(shaderGroupHandleSize * shaderGroupCreateInfos.size());

    if (pvkGetRayTracingShaderGroupHandlesKHR(logicalDevice.get().getVkDevice(), getVkPipeline(), 0, shaderGroupCreateInfos.size(),
        reserved->shaderGroupsHandleStorage.size(), reserved->shaderGroupsHandleStorage.data()) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to retrieve shaderGroupsHandle the pipeline with id= %d", getId());
        this->free();
        return false;
    }


#pragma endregion


    // Clear
    vkDestroyShaderModule(logicalDevice.get().getVkDevice(), raygenShaderModule, NULL);

    for (auto [key, value] : intersectionShadersModules)
        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

    for (auto [key, value] : anyHitShadersModules)
        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

    for (auto [key, value] : closestHitShadersModules)
        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

    for (auto [key, value] : callableShadersModules)
        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);

    for (auto [key, value] : missShadersModules)
        vkDestroyShaderModule(logicalDevice.get().getVkDevice(), value, NULL);
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the DSM of the ray tracing pipeline.
 * @return The DSM of the ray tracing pipeline.
 */
OvVK::RayTracingDescriptorSetsManager OVVK_API& 
OvVK::RayTracingPipeline::getRayTracingDescriptorSetsManager() const 
{
    return reserved->rayTracingDescriptorSetsManager;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the OvVK shader representing the ray gen shader of the pipelien.
 * @return The reference to the OvVK shader representing the ray gen shader of the pipeline.
 */
OvVK::Shader OVVK_API& OvVK::RayTracingPipeline::getRayGenShader() const
{
    return reserved->rayGenShader;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shaders representing the closest hit shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shaders representing the closest hit shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::Shader> OVVK_API& OvVK::RayTracingPipeline::getClosestHitShaders() const
{
	return reserved->closestHitShaders;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shaders representing the any hit shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shaders representing the any hit shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::Shader> OVVK_API& OvVK::RayTracingPipeline::getAnyHitShaders() const
{
    return reserved->anyHitShaders;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shaders representing the intersection hit shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shaders representing the intersection hit shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::Shader> OVVK_API& OvVK::RayTracingPipeline::getIntersectionHitShaders() const
{
    return reserved->intersectionShaders;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shaders representing the callable hit shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shaders representing the callable hit shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::Shader> OVVK_API& OvVK::RayTracingPipeline::getCallableHitShaders() const
{
    return reserved->callableShaders;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shaders representing the miss hit shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shaders representing the miss hit shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::Shader> OVVK_API& OvVK::RayTracingPipeline::getMissHitShaders() const
{
    return reserved->missShaders;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the OvVK RTShaderGroup representing the ray gen shader group of the pipelien.
 * @return The reference to the OvVK RTShaderGroup representing the ray gen shader group of the pipelien.
 */
OvVK::RTShaderGroup OVVK_API& OvVK::RayTracingPipeline::getRayGenShaderGroup() const
{
    return reserved->rayGenShaderGroup;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shader groups representing the hit group shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shader groups representing the hit group shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::RTShaderGroup> OVVK_API& OvVK::RayTracingPipeline::getHitShaderGroups() const
{
	return reserved->hitShaderGroups;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shader groups representing the callable group shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shader groups representing the callable group shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::RTShaderGroup> OVVK_API& OvVK::RayTracingPipeline::getCallableShaderGroups() const
{
    return reserved->callableShaderGroups;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the vector containing the OvVK shader groups representing the miss group shaders
 * of the pipeline.
 * @return The reference to the vector containing the OvVK shader groups representing the miss group shaders
 * of the pipeline.
 */
std::map<std::string, OvVK::RTShaderGroup> OVVK_API& OvVK::RayTracingPipeline::getMissShaderGroups() const
{
    return reserved->missShaderGroups;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the structure specifying a region of device addresses with a stride of the raygen shader SBT.
 * @return The structure specifying a region of device addresses with a stride of the raygen shader SBT.
 */
VkStridedDeviceAddressRegionKHR OVVK_API& OvVK::RayTracingPipeline::getRaygenShaderBindingTable() const
{
    return reserved->raygenShaderBindingTable;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the structure specifying a region of device addresses with a stride of the hit shader SBT.
 * @return The structure specifying a region of device addresses with a stride of the hit shader SBT.
 */
VkStridedDeviceAddressRegionKHR OVVK_API& OvVK::RayTracingPipeline::getHitShaderBindingTable() const
{
    return reserved->hitShaderBindingTable;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the structure specifying a region of device addresses with a stride of the callable shader SBT.
 * @return The structure specifying a region of device addresses with a stride of the callable shader SBT.
 */
VkStridedDeviceAddressRegionKHR OVVK_API& OvVK::RayTracingPipeline::getCallableShaderBindingTable() const
{
    return reserved->callableShaderBindingTable;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the structure specifying a region of device addresses with a stride of the miss shader SBT.
 * @return The structure specifying a region of device addresses with a stride of the miss shader SBT.
 */
VkStridedDeviceAddressRegionKHR OVVK_API& OvVK::RayTracingPipeline::getMissShaderBindingTable() const
{
    return reserved->missShaderBindingTable;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference vector containing the handle to the shader groups in the pipeline.
 * @return The reference vector containing the handle to the shader groups in the pipeline.
 */
std::vector<uint8_t> OVVK_API& OvVK::RayTracingPipeline::getShaderGroupsHandleStorage() const 
{
    return reserved->shaderGroupsHandleStorage;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the host OvVK buffer of the Shader Binding Table.
 * @return The host OvVK buffer of the Shader Binding Table.
 */
OvVK::Buffer OVVK_API& OvVK::RayTracingPipeline::getSBTHostBuffer() const
{
    return reserved->sbtHostBuffer[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the device OvVK buffer of the Shader Binding Table. 
 * @return The device OvVK buffer of the Shader Binding Table. 
 */
OvVK::Buffer OVVK_API& OvVK::RayTracingPipeline::getSBTDeviceBuffer() const 
{
    return reserved->sbtDeviceBuffer[OvVK::Engine::getInstance().getFrameInFlight()];
}

#pragma endregion

#pragma region Static

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the number of max ray recursion depth allowed.
 * @return The number of max ray recursion depth allowed.
 */
uint32_t OVVK_API OvVK::RayTracingPipeline::getMaxRayRecursionDepth()
{
    // Properties of the physical device for ray tracing
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR rayTracingProperties = {};
    rayTracingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;

    std::optional<VkPhysicalDeviceProperties2> properties = Engine::getInstance().getLogicalDevice()
        .getPhysicalDevice().getProperties(&rayTracingProperties);

    if (properties.has_value() == false)
    {
        OV_LOG_ERROR(R"(Fail to get information about the physical device to execute the ray tracing
 in pipeline with id= %d.)");
        return 0;
    }

    // MaxRayRecursionDepth can be see as how many bounce can do a ray.
    return rayTracingProperties.maxRayRecursionDepth;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the stride in bytes between shader groups in the shader binding table.
 * @return The stride in bytes between shader groups in the shader binding table.
 */
uint32_t OVVK_API OvVK::RayTracingPipeline::getShaderGroupHandleStride()
{
    // Properties of the physical device for ray tracing
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR rayTracingProperties = {};
    rayTracingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;

    std::optional<VkPhysicalDeviceProperties2> properties = Engine::getInstance().getLogicalDevice()
        .getPhysicalDevice().getProperties(&rayTracingProperties);

    if (properties.has_value() == false)
    {
        OV_LOG_ERROR(R"(Fail to get information about the physical device to execute the ray tracing
 in pipeline with id= %d.)");
        return 0;
    }

    // Compute stride between shaderGroup handles (in bytes)
    uint32_t shaderGroupHandleStride = rayTracingProperties.shaderGroupHandleAlignment;
    while (true)
    {
        if (rayTracingProperties.shaderGroupHandleSize <= shaderGroupHandleStride)
            break;

        shaderGroupHandleStride += rayTracingProperties.shaderGroupHandleAlignment;
    }
    if (shaderGroupHandleStride > rayTracingProperties.maxShaderGroupStride)
    {
        OV_LOG_ERROR(R"(Can't find a valid shaderGroupHandle stride for the shader binding table 
in the pipeline with id= %d)");
        return 0;
    }

    return shaderGroupHandleStride;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the required alignment in bytes for the base of the shader binding table.
 * @return The required alignment in bytes for the base of the shader binding table.
 */
uint32_t OVVK_API OvVK::RayTracingPipeline::getShaderGroupBaseAlignment()
{
    // Properties of the physical device for ray tracing
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR rayTracingProperties = {};
    rayTracingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;

    std::optional<VkPhysicalDeviceProperties2> properties = Engine::getInstance().getLogicalDevice()
        .getPhysicalDevice().getProperties(&rayTracingProperties);

    if (properties.has_value() == false)
    {
        OV_LOG_ERROR(R"(Fail to get information about the physical device to execute the ray tracing
 in pipeline with id= %d.)");
        return 0;
    }

    return rayTracingProperties.shaderGroupBaseAlignment;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the size in bytes of the shader header.
 * @return The size in bytes of the shader header.
 */
uint32_t OVVK_API OvVK::RayTracingPipeline::getShaderGroupHandleSize()
{
    // Properties of the physical device for ray tracing
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR rayTracingProperties = {};
    rayTracingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;

    std::optional<VkPhysicalDeviceProperties2> properties = Engine::getInstance().getLogicalDevice()
        .getPhysicalDevice().getProperties(&rayTracingProperties);

    if (properties.has_value() == false)
    {
        OV_LOG_ERROR(R"(Fail to get information about the physical device to execute the ray tracing
 in pipeline with id= %d.)");
        return 0;
    }

    return rayTracingProperties.shaderGroupHandleSize;
}

#pragma endregion