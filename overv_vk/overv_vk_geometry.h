#pragma once
/**
 * @file	overv_vk_geometry.h
 * @brief	Geometry
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


/**
 * @brief Class representing a geometry.
 */
class OVVK_API Geometry : public Ov::Geometry
{
//////////
public: //
//////////	   

    // Static values:
    static Geometry empty;

    // Sync obj value:
    constexpr static uint64_t readyToUploadValueStatusSyncObj = 1;
    constexpr static uint64_t uploadingValueStatusSyncObj = 2;
    constexpr static uint64_t readyValueStatusSyncObj = 3;

    // Const/Dest:
    Geometry();
    Geometry(Geometry&& other) noexcept;
    Geometry(Geometry const&) = delete;
    virtual ~Geometry();

    // Operators
    Geometry& operator=(const Geometry&) = delete;
    Geometry& operator=(Geometry&&) = delete;

    // Managed:
    virtual bool init() override;
    virtual bool free() override;

    // Synch data:
    bool isLock() const;
    bool isOwned() const;

    // General
    bool setUp();
    virtual bool upload() override;

    // Get/set:
    const OvVK::Buffer& getVertexBuffer() const;
    const OvVK::Buffer& getFaceBuffer() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
    bool setQueueFamilyIndex(uint32_t queueFamilyIndex);
    bool setStatusSyncObjValue(uint64_t value);
    bool setLockSyncObj(VkFence fence);
#endif

    // Rendering methods:
    bool render(uint32_t value = 0, void* data = nullptr) const;

    // Shader methods
    static bool addShaderPreproc();

/////////////
protected: //
/////////////

    // Reserved:
    struct Reserved;
    std::unique_ptr<Reserved> reserved;

    // Const/dest:
    Geometry(const std::string& name);
};