/**
 * @file	overv_vk_rt_scene.cpp
 * @brief	Vulkan ray tracing scene
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/////////////
// INCLUDE //
/////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum:
#include "magic_enum.hpp"

// Thread:
#include "overv_vk_thread_pool.h"

// C/C++
#include <functional>

// ImGui
#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_vulkan.h"
#include "imgui_internal.h"
#include "imconfig.h"


///////////
// ENUMS //
///////////

// Enum used to idenfy a thread 
enum class ThreadIdentifier : uint32_t
{
    blass = 0,
    rtObjDescs = 1,
    tlass = 2,
    all
};


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Struct to use multiple thread to record commands.
 */
struct OvVK::RTScene::ThreadData {
    // Thread commandPool
    OvVK::CommandPool commandPool;
    // CommandBuffer per obj
    std::vector<std::reference_wrapper<OvVK::CommandBuffer>> commandBuffers;
};

/**
 * @brief RTScene class reserved structure.
 */
struct OvVK::RTScene::Reserved
{
    uint32_t nrOfLights[OvVK::Engine::nrFramesInFlight];                                    ///< Nr of lights.
    uint32_t nrOfHitShaderGroupsNeeded[OvVK::Engine::nrFramesInFlight];                     ///< Nr of hit shader group needed. Used to build SBT

    std::vector<RenderableElem> renderableElements[OvVK::Engine::nrFramesInFlight];         ///< Array of vector containing all renderable element in the scene per frame in flight.
    OvVK::TopLevelAccelerationStructure allTlas[OvVK::Engine::nrFramesInFlight];        ///< TLASs obj containing the scene per frame in flight.

    // All the following variables are needed because the rendering is send to the GPU in one go.
    // So need to keep alive the resourses and in the next same frame in flught free them.
    std::list<std::reference_wrapper<OvVK::BottomLevelAccelerationStructure>>
        blassToBuild[OvVK::Engine::nrFramesInFlight];                                       ///< BLASs obj. to build if not already present in the passed container.

    // Buffers
    OvVK::Buffer stagingTransformsBLASsDataBuffers[OvVK::Engine::nrFramesInFlight];     ///< Stating buffer containing the transforms matrix of the geometries of the BLASs to build.
    OvVK::Buffer stagingGeometriesRTObjDescsDataBuffers[OvVK::Engine::nrFramesInFlight];///< Staging buffer containing the geometries data structures of the RTObjDescs,
    OvVK::Buffer stagingASInstanceTLASsDataBuffers[OvVK::Engine::nrFramesInFlight];     ///< Staging buffer containing the AS instances to add to the TLASs

    // Sync Objs
    VkSemaphore passStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];                       ///< Pass scene graph GPU sync objs
    VkFence passStatusSyncObjCPU[OvVK::Engine::nrFramesInFlight];                           ///< Pass scene graph CPU sync objs
    VkSemaphore rederingStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];                   ///< Rencer GPU sync objs
    VkFence rederingStatusSyncObjCPU[OvVK::Engine::nrFramesInFlight];                       ///< Rencer CPU sync objs
    VkSemaphore ownershipGraphicStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];           ///< Geometries change ownership from graphic queue
    VkSemaphore ownershipTransferStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];          ///< Geometries change ownership from transfer queue
    VkEvent rtObjDescStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];
    VkEvent blasStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];
    VkEvent tlasStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];

    OvVK::RTScene::ThreadData threadsData[OvVK::Engine::nrFramesInFlight][magic_enum::enum_integer(ThreadIdentifier::all)];

    // To know the how to build the SBT
    bool areGeometriesSharingSameShadersGroups[OvVK::Engine::nrFramesInFlight];             ///< Geometries share the same shaders groups flag.

    // GUI light target
    uint32_t targetLight;
    OvVK::Buffer pickedDataBuffer;

    /**
     * Constructor
     */
    Reserved() : targetLight{ 0 },
        nrOfLights { 0 },
        areGeometriesSharingSameShadersGroups{ true }
    {
        for (uint32_t i = 0; i < Engine::nrFramesInFlight; i++)
        {
            passStatusSyncObjGPU[i] = VK_NULL_HANDLE;
            passStatusSyncObjCPU[i] = VK_NULL_HANDLE;
            rederingStatusSyncObjGPU[i] = VK_NULL_HANDLE;
            rederingStatusSyncObjCPU[i] = VK_NULL_HANDLE;
            ownershipGraphicStatusSyncObjGPU[i] = VK_NULL_HANDLE;
            ownershipTransferStatusSyncObjGPU[i] = VK_NULL_HANDLE;
            rtObjDescStatusSyncObjGPU[i] = VK_NULL_HANDLE;
            blasStatusSyncObjGPU[i] = VK_NULL_HANDLE;
            tlasStatusSyncObjGPU[i] = VK_NULL_HANDLE;
        }
    }
};


///////////////////////////
// BODY OF CLASS RTScene //
///////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::RTScene::RTScene() :
    reserved(std::make_unique<OvVK::RTScene::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name RTScene name
 */
OVVK_API OvVK::RTScene::RTScene(const std::string& name) : Ov::Object(name), Ov::Managed(),
reserved(std::make_unique<OvVK::RTScene::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::RTScene::RTScene(RTScene&& other) noexcept : Ov::Object(std::move(other)), Ov::Managed(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::RTScene::~RTScene()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes buffer.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // To create fences
    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.pNext = VK_NULL_HANDLE;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    //// Binary semaphore
    //VkSemaphoreCreateInfo createInfo;
    //createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    //createInfo.pNext = VK_NULL_HANDLE;
    //createInfo.flags = 0;

    // Create new timeline semaphore and delete previous one.
    VkSemaphoreTypeCreateInfo timelineCreateInfo = {};
    timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    timelineCreateInfo.pNext = VK_NULL_HANDLE;
    timelineCreateInfo.initialValue = OvVK::RTScene::defaultValueStatusSyncObj;
    timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;

    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfo.pNext = &timelineCreateInfo;
    semaphoreInfo.flags = 0;

    VkEventCreateInfo vkEventCreateInfo = {};
    vkEventCreateInfo.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
    vkEventCreateInfo.pNext = VK_NULL_HANDLE;
    vkEventCreateInfo.flags = 0;

    // Create sync Objs.
    for (uint32_t i = 0; i < Engine::nrFramesInFlight; i++)
    {
        vkDestroySemaphore(logicalDevice, reserved->passStatusSyncObjGPU[i], nullptr);
        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo,
            NULL, &reserved->passStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the pass sync obj GPU of the frame in flight 
                number %d of the rtscene with id= %d.)", i, getId());
            this->free();
            return false;
        }

        vkDestroyFence(logicalDevice, reserved->passStatusSyncObjCPU[i], nullptr);
        if (vkCreateFence(logicalDevice, &fenceCreateInfo, 
            NULL, &reserved->passStatusSyncObjCPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the pass fence of the frame in flight number
 %d of the rtscene with id= %d.)", i, getId());
            this->free();
            return false;
        }

        vkDestroySemaphore(logicalDevice, reserved->rederingStatusSyncObjGPU[i], nullptr);
        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo,
            NULL, &reserved->rederingStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the render sync obj GPU of the frame in flight
 number %d of the rtscene with id= %d.)", i, getId());
            this->free();
            return false;
        }

        vkDestroyFence(logicalDevice, reserved->rederingStatusSyncObjCPU[i], nullptr);
        if (vkCreateFence(logicalDevice, &fenceCreateInfo,
            NULL, &reserved->rederingStatusSyncObjCPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the render fence of the frame in flight
 number %d of the rtscene with id= %d.)",i, getId());
            this->free();
            return false;
        }

        vkDestroySemaphore(logicalDevice, reserved->ownershipGraphicStatusSyncObjGPU[i], nullptr);
        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo,
            NULL, &reserved->ownershipGraphicStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create semaphore to sync geometries transfer ownership
 for graphic queue in rtscene with id= %d)", getId());
            this->free();
            return false;
        }

        vkDestroySemaphore(logicalDevice, reserved->ownershipTransferStatusSyncObjGPU[i], nullptr);
        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo,
            NULL, &reserved->ownershipTransferStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create semaphore to sync geometries transfer
 ownership for transfer queue in rtscene with id= %d)", getId());
            this->free();
            return false;
        }

        vkDestroyEvent(logicalDevice, reserved->blasStatusSyncObjGPU[i], nullptr);
        if (vkCreateEvent(logicalDevice, &vkEventCreateInfo,
            NULL, &reserved->blasStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create event to sync BLAS build
 for compute queue in rtscene with id= %d)", getId());
            this->free();
            return false;
        }

        vkDestroyEvent(logicalDevice, reserved->rtObjDescStatusSyncObjGPU[i], nullptr);
        if (vkCreateEvent(logicalDevice, &vkEventCreateInfo,
            NULL, &reserved->rtObjDescStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create event to sync RTObjDescs build
 for compute queue in rtscene with id= %d)", getId());
            this->free();
            return false;
        }

        vkDestroyEvent(logicalDevice, reserved->tlasStatusSyncObjGPU[i], nullptr);
        if (vkCreateEvent(logicalDevice, &vkEventCreateInfo,
            NULL, &reserved->tlasStatusSyncObjGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create event to sync TLAS build
 for compute queue in rtscene with id= %d)", getId());
            this->free();
            return false;
        }
    }

    // Create buffer to know witch obj is picking
    VkBufferCreateInfo vkBufferCreateInfo{};
    vkBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    vkBufferCreateInfo.size = sizeof(OvVK::PickingPipeline::PickingData);
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable for
    //      use as a read-only input to an acceleration structure build.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT => specifies that the buffer can be used to retrieve a buffer device address
    //      via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    vkBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
    // from multiple queue families is supported.
    vkBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    vkBufferCreateInfo.pQueueFamilyIndices = NULL;
    vkBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
    //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
    //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
    //      of allocation time.
    vmaAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
        VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
    //      mapped for host access using vkMapMemory.
    vmaAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

    if (reserved->pickedDataBuffer.create(
        vkBufferCreateInfo, vmaAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create picking buffer to know the pick obj in the RTScene with id= %d.", getId());
        return false;
    }

    OvVK::PickingPipeline::PickingData* pickingData =
        reinterpret_cast<OvVK::PickingPipeline::PickingData*>(
            ((uint8_t*)reserved->pickedDataBuffer.getVmaAllocationInfo().pMappedData));

    pickingData->geometryId = 0;
    pickingData->rtObjDescId = 0;
    pickingData->pickedSomething = 0;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the OvVK buffer.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++)
    {
        vkDestroySemaphore(logicalDevice, reserved->passStatusSyncObjGPU[c], nullptr);
        reserved->passStatusSyncObjGPU[c] = VK_NULL_HANDLE;

        vkDestroyFence(logicalDevice, reserved->passStatusSyncObjCPU[c], nullptr);
        reserved->passStatusSyncObjCPU[c] = VK_NULL_HANDLE;

        vkDestroySemaphore(logicalDevice, reserved->rederingStatusSyncObjGPU[c], nullptr);
        reserved->rederingStatusSyncObjGPU[c] = VK_NULL_HANDLE;

        vkDestroyFence(logicalDevice, reserved->rederingStatusSyncObjCPU[c], nullptr);
        reserved->rederingStatusSyncObjCPU[c] = VK_NULL_HANDLE;

        vkDestroySemaphore(logicalDevice, reserved->ownershipGraphicStatusSyncObjGPU[c], nullptr);
        reserved->ownershipGraphicStatusSyncObjGPU[c] = VK_NULL_HANDLE;

        vkDestroySemaphore(logicalDevice, reserved->ownershipTransferStatusSyncObjGPU[c], nullptr);
        reserved->ownershipTransferStatusSyncObjGPU[c] = VK_NULL_HANDLE;

        vkDestroyEvent(logicalDevice, reserved->rtObjDescStatusSyncObjGPU[c], nullptr);
        reserved->rtObjDescStatusSyncObjGPU[c] = VK_NULL_HANDLE;

        vkDestroyEvent(logicalDevice, reserved->blasStatusSyncObjGPU[c], nullptr);
        reserved->blasStatusSyncObjGPU[c] = VK_NULL_HANDLE;

        vkDestroyEvent(logicalDevice, reserved->tlasStatusSyncObjGPU[c], nullptr);
        reserved->tlasStatusSyncObjGPU[c] = VK_NULL_HANDLE;
    }



    // Done:
    return true;
}

#pragma endregion

#pragma region SceneGraph

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Recursively parse the scenegraph starting at the given node and append the parsed elements to this rtscene.
 * @param node The node starting node
 * @param container The container containing all the resources used by the nodes in the scene graph.
 * @param prevMatrix The previous node matrix
 * @return TF
 */
bool OVVK_API OvVK::RTScene::pass(const Ov::Node& node,
    OvVK::Container& container, bool geometriesSharingSameShadersGroups, glm::mat4 prevMatrix)
{
    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // PhysicalDevice
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

	// Wait CPU pass sync object
	// Wait fences to know the work is finished
	if (reserved->passStatusSyncObjCPU[frameInFlight] != VK_NULL_HANDLE &&
		vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &reserved->passStatusSyncObjCPU[frameInFlight],
			VK_TRUE, UINT64_MAX) != VK_SUCCESS)
	{
		OV_LOG_ERROR("Fail to wait pass Fence in rtscene with id= %d.", getId());
		return false;
	}

    // Clean old same frame in flight.
    this->clearPassData();
    this->reset();


    /////////////////
    // Preparation //
    /////////////////

    // Creates lookup for already create BLASs Objs.
    std::vector<std::reference_wrapper<OvVK::BottomLevelAccelerationStructure>> blassContainer;
    std::unordered_map<std::size_t, uint32_t> hashBLASsLookUp;
    {
        // Retrievers the list of the already created BLASs objs.
        std::reference_wrapper<std::list<OvVK::BottomLevelAccelerationStructure>> blassContainerList
            = container.getBLASList();
        // Resize and reserve the needed memory to the lookup system.
        blassContainer.resize(blassContainerList.get().size(), OvVK::BottomLevelAccelerationStructure::empty);
        hashBLASsLookUp.reserve(blassContainerList.get().size());
        // Fill the lookUp system
        uint32_t counterBlasList = 0;
        for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas : blassContainerList.get())
        {
            blassContainer[counterBlasList] = blas;
            hashBLASsLookUp[blas.get().getHash()] = counterBlasList;
            counterBlasList++;
        }
    }

    // Creates lookup for already create RTObjDescs Objs.
    std::vector<std::reference_wrapper<OvVK::RTObjDesc>> rtObjDescsContainer;
    std::unordered_map<std::size_t, uint32_t> hashRTObjDescsLookUp;
    {
        // Retrievers the list of the already created RTObjDescs objs.
        std::reference_wrapper<std::list<OvVK::RTObjDesc>> rtObjDescsContainerList
            = container.getRTObjDescList();
        // Resize and reserve the needed memory to the lookup system.
        rtObjDescsContainer.resize(rtObjDescsContainerList.get().size(), OvVK::RTObjDesc::empty);
        hashRTObjDescsLookUp.reserve(rtObjDescsContainerList.get().size());
        // Fill the lookUp system
        uint32_t counterRTObjDescsList = 0;
        for (std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc : rtObjDescsContainerList.get())
        {
            rtObjDescsContainer[counterRTObjDescsList] = rtObjDesc;
            hashRTObjDescsLookUp[rtObjDesc.get().getHash()] = counterRTObjDescsList;
            counterRTObjDescsList++;
        }
    }

    // Clear thread datas
    reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::rtObjDescs)].commandPool.free();
    reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::rtObjDescs)].commandBuffers.clear();

    reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::blass)].commandPool.free();
    reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::blass)].commandBuffers.clear();

    reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::tlass)].commandPool.free();
    reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::tlass)].commandBuffers.clear();

    ///////////////////////
    // SetUp Renderables //
    ///////////////////////

    // List of RTObjDescs to upload the data.
    std::list<std::reference_wrapper<OvVK::RTObjDesc>>
        rtObjDescsToUpload;
    // Retrievers all needed geometries.
    std::list<std::reference_wrapper<OvVK::Geometry>> neededGeometries;
    std::set<uint32_t> neededGeometriesIds;
    // Size staging buffer for all the geometries matrix in the BLASs.
    VkDeviceSize sizeStagingBuildBLASBuffer = 0;
    // Size staging buffer for all the BLASs geometries data in the RTObjDecs.
    VkDeviceSize sizeStagingUploadRTObjDescsBuffer = 0;

    // Lambda to process all the node in the scene graph
    std::function<bool(const Ov::Node& node, const glm::mat4& prevMatrix)> passSceneGraph;
    passSceneGraph = [&passSceneGraph, &hashBLASsLookUp, &blassContainer,
        &hashRTObjDescsLookUp, &rtObjDescsContainer, &rtObjDescsToUpload, &container,
        &neededGeometriesIds, &neededGeometries, &frameInFlight,
        &sizeStagingBuildBLASBuffer, &sizeStagingUploadRTObjDescsBuffer, this]
        (const Ov::Node& node, const glm::mat4& prevMatrix) -> bool
    {
        // Safety net:
        if (node == Ov::Node::empty)
        {
            OV_LOG_ERROR("Invalid node in the rtscene with id= %d.", getId());
            return false;
        }

        // Renderable element
        OvVK::RTScene::RenderableElem re;
        re.matrix = prevMatrix * node.getMatrix();
        re.reference = node;
        re.visibleFlag = node.isVisible();

        // Used if node contains geometries
        OvVK::BottomLevelAccelerationStructure newBLAS;
        std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> refBLAS = newBLAS;
        std::list<std::pair<uint32_t, std::reference_wrapper<OvVK::Material>>> geomToMat;

        // Store renderable:
        for (auto& r : node.getListOfRenderables())
        {
            // Lights:
            if (OvVK::Light* light = dynamic_cast<OvVK::Light*>(&r.first.get()))
            {
                re.renderable = *light;
                reserved->renderableElements[frameInFlight].insert(reserved->renderableElements[frameInFlight].begin(), 1, re);
                reserved->nrOfLights[frameInFlight]++;
            }
            // Geoms
            else if (OvVK::Geometry* geom = dynamic_cast<OvVK::Geometry*>(&r.first.get()))
            {
                // VK_GEOMETRY_OPAQUE_BIT_KHR indicates that this geometry does not invoke the any-hit shaders even if present in a hit group.
                // To call any hit shader need to change this parameters flag.
                //blas.addGeometry(*geom, VK_GEOMETRY_OPAQUE_BIT_KHR);
                //blas.addGeometry(*geom, VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_KHR);
                newBLAS.addGeometry(*geom);
                if (OvVK::Material* mat = dynamic_cast<OvVK::Material*>(&r.second.get())) {
                    geomToMat.push_back(std::make_pair<uint32_t, std::reference_wrapper<OvVK::Material>>(geom->getId(), *mat));
                }
            }
        }

        // If there is some geometry
        if (newBLAS.getNrOfGeometries() > 0)
        {
            // Create RTObjDesc
            OvVK::RTObjDesc rtObjDesc;

            // BLAS
            std::size_t hashBLAS = newBLAS.getHash();
            auto itBlas = hashBLASsLookUp.find(hashBLAS);
            if (itBlas != hashBLASsLookUp.end()) {
                refBLAS = blassContainer[itBlas->second];
            }
            else
            {
                container.add(newBLAS);
                refBLAS = container.getLastBLAS();

                hashBLASsLookUp.insert(std::make_pair(hashBLAS, static_cast<uint32_t>(blassContainer.size())));
                blassContainer.push_back(refBLAS);
                reserved->blassToBuild[frameInFlight].push_back(refBLAS);

                if (refBLAS.get().setUpBuild(VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR) == false)
                {
                    OV_LOG_ERROR("Fail to setUp the BLAS with id= %d and name= %s for build in rtscene with id= %d",
                        refBLAS.get().getId(), refBLAS.get().getName(), getId());
                    return false;
                }

                sizeStagingBuildBLASBuffer += sizeof(VkTransformMatrixKHR) * refBLAS.get().getNrOfGeometries();
            }

            rtObjDesc.setBLAS(refBLAS);

            // Gets needed geometries
            std::vector<std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData>>
                geometriesData = refBLAS.get().getGeometriesData();

            for (std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData> geometryData 
                : geometriesData)
                if (neededGeometriesIds.count(geometryData.get().geometry.get().getId()) == 0)
                {
                    neededGeometriesIds.insert(geometryData.get().geometry.get().getId());
                    neededGeometries.push_back(geometryData.get().geometry);
                }


            // Materials
            for (auto& [key, value] : geomToMat)
                rtObjDesc.setGeometryMaterial(key, value);


            // RTObjDesc
            std::size_t hashRTObjDesc = rtObjDesc.getHash();
            auto itRTObjDesc = hashRTObjDescsLookUp.find(hashRTObjDesc);
            if (itRTObjDesc != hashRTObjDescsLookUp.end())
                re.renderable = rtObjDescsContainer[itRTObjDesc->second];
            else
            {
                container.add(rtObjDesc);
                std::reference_wrapper<OvVK::RTObjDesc> lastRTObjDesc = container.getLastRTObjDesc();

                hashRTObjDescsLookUp.insert(std::make_pair(hashRTObjDesc,
                    static_cast<uint32_t>(rtObjDescsContainer.size())));
                rtObjDescsContainer.push_back(lastRTObjDesc);
                rtObjDescsToUpload.push_back(lastRTObjDesc);

                if (lastRTObjDesc.get().setUp() == false)
                {
                    OV_LOG_ERROR("Fail to setUp the rtObjDesc with id= %d and name= %s for uplaod in the RTScene with id= %d.",
                        lastRTObjDesc.get().getId(), lastRTObjDesc.get().getName(), getId());
                    return false;
                }

                sizeStagingUploadRTObjDescsBuffer += sizeof(OvVK::RTObjDesc::GeometryData) *
                    lastRTObjDesc.get().getNrGeoemtriesInBLAS();

                re.renderable = lastRTObjDesc;
            }

            // Adds renderable
            reserved->renderableElements[frameInFlight].push_back(re);
        }

        // Parse hiearchy recursively:
        for (auto& n : node.getListOfChildren())
            if (passSceneGraph(n, re.matrix) == false)
                return false;

        // Done:
        return true;
    };

    // Pass the graph
    if (passSceneGraph(node, prevMatrix) == false)
    {
        OV_LOG_ERROR("Fail to pass the passed scene graph, in the rtscene with id= %d and name= %s",
            getId(), getName());
        this->reset();
        return false;
    }


    ///////////////////////////
    // setUp staging buffers //
    ///////////////////////////

    // BLASs
    bool thereIsSomeBLASToBuild = false;
    if (sizeStagingBuildBLASBuffer != 0) {
        // Create info buffer
        VkBufferCreateInfo vkBLASStagingBufferCreateInfo{};
        vkBLASStagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        vkBLASStagingBufferCreateInfo.pNext = VK_NULL_HANDLE;
        vkBLASStagingBufferCreateInfo.flags = 0X00000000;
        // The second field of the struct is size, which specifies the size of the buffer in bytes
        vkBLASStagingBufferCreateInfo.size = sizeStagingBuildBLASBuffer;
        // The third field is usage, which indicates for which purposes the data in the buffer 
        // is going to be used. It is possible to specify multiple purposes using a bitwise or.
        // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable for
        //      use as a read-only input to an acceleration structure build.
        // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT => specifies that the buffer can be used to retrieve a buffer device address
        //      via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
        vkBLASStagingBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        // buffers can also be owned by a specific queue family or be shared between multiple 
        // at the same time. 
        // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
        // from multiple queue families is supported.
        vkBLASStagingBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        // From which queue family the buffer will be accessed.
        vkBLASStagingBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
        vkBLASStagingBufferCreateInfo.queueFamilyIndexCount = 0;

        // Create allocation info
        VmaAllocationCreateInfo vmaBLASStagingBufferAllocationCreateInfo = {};
        // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
        //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
        // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
        //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
        //      of allocation time.
        vmaBLASStagingBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
            VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
        // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
        //      mapped for host access using vkMapMemory.
        vmaBLASStagingBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

        if (reserved->stagingTransformsBLASsDataBuffers[frameInFlight].create(
            vkBLASStagingBufferCreateInfo, vmaBLASStagingBufferAllocationCreateInfo) == false)
        {
            OV_LOG_ERROR("Fail to create transforms data staging buffer to build BLASs in the RTScene with id= %d.", getId());
            this->reset();
            return false;
        }
        
        thereIsSomeBLASToBuild = true;
    }

    // RTObjDescs
    bool thereIsSomeRTObjDescToBuild = false;
    if (sizeStagingUploadRTObjDescsBuffer != 0) {
        // Create info buffer
        VkBufferCreateInfo vkRTObjDescStagingBufferCreateInfo{};
        vkRTObjDescStagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        vkRTObjDescStagingBufferCreateInfo.pNext = VK_NULL_HANDLE;
        vkRTObjDescStagingBufferCreateInfo.flags = 0X00000000;
        // The second field of the struct is size, which specifies the size of the buffer in bytes
        vkRTObjDescStagingBufferCreateInfo.size = sizeStagingUploadRTObjDescsBuffer;
        // The third field is usage, which indicates for which purposes the data in the buffer 
        // is going to be used. It is possible to specify multiple purposes using a bitwise or.
        // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable for
        //      use as a read-only input to an acceleration structure build.
        // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT => specifies that the buffer can be used to retrieve a buffer device address
        //      via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
        vkRTObjDescStagingBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        // buffers can also be owned by a specific queue family or be shared between multiple 
        // at the same time. 
        // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
        // from multiple queue families is supported.
        vkRTObjDescStagingBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        // From which queue family the buffer will be accessed.
        vkRTObjDescStagingBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
        vkRTObjDescStagingBufferCreateInfo.queueFamilyIndexCount = 0;

        // Create allocation info
        VmaAllocationCreateInfo vmaRTObjDescStagingBufferAllocationCreateInfo = {};
        // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
        //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
        // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
        //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
        //      of allocation time.
        vmaRTObjDescStagingBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
            VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
        // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
        //      mapped for host access using vkMapMemory.
        vmaRTObjDescStagingBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

        if (reserved->stagingGeometriesRTObjDescsDataBuffers[frameInFlight].create(
            vkRTObjDescStagingBufferCreateInfo, vmaRTObjDescStagingBufferAllocationCreateInfo) == false)
        {
            OV_LOG_ERROR("Fail creation geometries data staging buffer to upload RTObjDesc in the RTScene with id= %d.", getId());
            this->reset();
            return false;
        }

        thereIsSomeRTObjDescToBuild = true;
    }

    /////////////////
    // setUp TLASs //
    /////////////////

    // This vector contains all the instance definition that will be used to build the tlas
    std::vector<VkAccelerationStructureInstanceKHR> accelerationStructureInstanceKHR;

    // The offset where each geometry's shader group region begins (in bytes)
    uint32_t offset = 0;

    // loop over all the renderable elements
    for (std::reference_wrapper<RenderableElem> renderableElement : reserved->renderableElements[frameInFlight])
    {
        // Interested only in RTObjDesc
        if (OvVK::RTObjDesc* rtObjDesc = dynamic_cast<OvVK::RTObjDesc*>(&renderableElement.get().renderable.get()))
        {
            // Skip empty BLAS
            if (rtObjDesc->getBLAS() == OvVK::BottomLevelAccelerationStructure::empty)
                continue;

            VkAccelerationStructureInstanceKHR instance = {};

            // glm::mat4 => column major
            // VkTransformMatrixKHR => row major
            VkTransformMatrixKHR transformMatrix = {};
            transformMatrix.matrix[0][0] = renderableElement.get().matrix[0][0];
            transformMatrix.matrix[1][0] = renderableElement.get().matrix[0][1];
            transformMatrix.matrix[2][0] = renderableElement.get().matrix[0][2];

            transformMatrix.matrix[0][1] = renderableElement.get().matrix[1][0];
            transformMatrix.matrix[1][1] = renderableElement.get().matrix[1][1];
            transformMatrix.matrix[2][1] = renderableElement.get().matrix[1][2];

            transformMatrix.matrix[0][2] = renderableElement.get().matrix[2][0];
            transformMatrix.matrix[1][2] = renderableElement.get().matrix[2][1];
            transformMatrix.matrix[2][2] = renderableElement.get().matrix[2][2];

            transformMatrix.matrix[0][3] = renderableElement.get().matrix[3][0];
            transformMatrix.matrix[1][3] = renderableElement.get().matrix[3][1];
            transformMatrix.matrix[2][3] = renderableElement.get().matrix[3][2];

            instance.transform = transformMatrix;
            instance.instanceCustomIndex = static_cast<uint32_t>(accelerationStructureInstanceKHR.size());
            instance.mask = renderableElement.get().visibleFlag ? 0xFF : 0x00;
            instance.instanceShaderBindingTableRecordOffset = offset;
            // Need for refraction
            // VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR disables face culling for this instance.
            instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;

            std::optional<VkDeviceAddress> blasDeviceAddress = rtObjDesc->getBLAS().getVkASDeviceAddress();
            if (blasDeviceAddress.has_value() == false)
            {
                OV_LOG_ERROR("Fail to retriever the device address of the blas with id= %d in rtscene with id= %d",
                    rtObjDesc->getBLAS().getId(), getId());
                this->reset();
                return false;
            }
            instance.accelerationStructureReference = blasDeviceAddress.value();

            if(!geometriesSharingSameShadersGroups)
                offset += magic_enum::enum_integer(OvVK::RayTracingPipeline::RayType::last) * rtObjDesc->getBLAS().getGeometriesData().size();
            accelerationStructureInstanceKHR.push_back(instance);
        }
    }
    // Set the nr of hit shader groups needed.
    if (!geometriesSharingSameShadersGroups)
        reserved->nrOfHitShaderGroupsNeeded[frameInFlight] = offset;
    else
        reserved->nrOfHitShaderGroupsNeeded[frameInFlight] = magic_enum::enum_integer(OvVK::RayTracingPipeline::RayType::last);

    // Size of the stagingASInstanceTLASsDataBuffers
    VkDeviceSize sizeStagingASInstanceTLASsDataBuffer = sizeof(VkAccelerationStructureInstanceKHR) *
        accelerationStructureInstanceKHR.size();

    // Create info buffer
    VkBufferCreateInfo hostASInstancesBufferCreateInfo = {};
    hostASInstancesBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    hostASInstancesBufferCreateInfo.pNext = VK_NULL_HANDLE;
    hostASInstancesBufferCreateInfo.flags = 0X00000000;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    hostASInstancesBufferCreateInfo.size = sizeStagingASInstanceTLASsDataBuffer;
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable for
    //      use as a read-only input to an acceleration structure build.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT => specifies that the buffer can be used to retrieve a buffer device address
    //      via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    hostASInstancesBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
    // from multiple queue families is supported.
    hostASInstancesBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    hostASInstancesBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    hostASInstancesBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmahostASInstancesAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
    //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
    //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
    //      of allocation time.
    vmahostASInstancesAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
        VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
    //      mapped for host access using vkMapMemory.
    vmahostASInstancesAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

    if (reserved->stagingASInstanceTLASsDataBuffers[frameInFlight].create(
        hostASInstancesBufferCreateInfo, vmahostASInstancesAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create host AS instances buffer for TLASs in RTScene with id= %d.", getId());
        this->reset();
        return false;
    }

    // Copy data from vector to host memory
    // Get pointer to the memory where copy the data
    uint8_t* currentPositionASInstanceBuffer = ((uint8_t*)reserved->stagingASInstanceTLASsDataBuffers[frameInFlight]
        .getVmaAllocationInfo().pMappedData);
    // Copy data on staging buffer
    memcpy(currentPositionASInstanceBuffer, accelerationStructureInstanceKHR.data(), 
        (size_t)sizeStagingASInstanceTLASsDataBuffer);

    // Create setUp TLAS
    if (reserved->allTlas[frameInFlight].setUpBuild(accelerationStructureInstanceKHR.size(), 0x00000000,
        VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR) == false)
    {
        OV_LOG_ERROR("Fail to setUp the TLAS for the frame nr. %d in the RTScene with id= %d", frameInFlight, getId());
        this->reset();
        return false;
    }

    ////////////////////////
    // SetUp Multi Thread //
    ////////////////////////

    std::vector<VkBufferMemoryBarrier2> rtObjDescBufferMemoryBarrier2s;

    for (std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc : rtObjDescsToUpload)
    {
        VkBufferMemoryBarrier2& deviceBufferMemoryBarrier = rtObjDescBufferMemoryBarrier2s.emplace_back();
        deviceBufferMemoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        deviceBufferMemoryBarrier.pNext = VK_NULL_HANDLE;
        // Need to wait previous stage 
        deviceBufferMemoryBarrier.srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
        // Need to wait on specific memory access.
        deviceBufferMemoryBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
            VK_ACCESS_2_TRANSFER_READ_BIT;
        // All AC commands need to wait that the texture change layout.
        deviceBufferMemoryBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
        // All AC commands need to wait that the texture change layout.
        deviceBufferMemoryBarrier.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
            VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
        // Need to transfer ownership
        deviceBufferMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        deviceBufferMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        // buffer
        deviceBufferMemoryBarrier.buffer = rtObjDesc.get().getGeometryDataDeviceBuffer().getVkBuffer();
        // offset is an offset in bytes into the backing memory for buffer;
        // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
        deviceBufferMemoryBarrier.offset = 0;
        // size is a size in bytes of the affected area of backing memory for buffer,
        // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
        deviceBufferMemoryBarrier.size = VK_WHOLE_SIZE;
    }

    std::vector<VkBufferMemoryBarrier2> blassBufferMemoryBarrier2s;

    for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas : reserved->blassToBuild[frameInFlight])
    {
        VkBufferMemoryBarrier2& blassBufferMemoryBarrier2 = blassBufferMemoryBarrier2s.emplace_back();
        blassBufferMemoryBarrier2.sType =
            VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        blassBufferMemoryBarrier2.pNext =
            VK_NULL_HANDLE;
        // Need to wait previous stage 
        blassBufferMemoryBarrier2.srcStageMask =
            VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
        // Need to wait on specific memory access.
        blassBufferMemoryBarrier2.srcAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
            VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
        // All build commands need to wait that the texture change layout.
        blassBufferMemoryBarrier2.dstStageMask =
            VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
        // All write/read commands need to wait that the texture change layout.
        blassBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
            VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
        // Need to transfer ownership
        blassBufferMemoryBarrier2.srcQueueFamilyIndex =
            VK_QUEUE_FAMILY_IGNORED;
        blassBufferMemoryBarrier2.dstQueueFamilyIndex =
            VK_QUEUE_FAMILY_IGNORED;
        // buffer
        blassBufferMemoryBarrier2.buffer =
            blas.get().getASBuffer().getVkBuffer();
        // offset is an offset in bytes into the backing memory for buffer;
        // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
        blassBufferMemoryBarrier2.offset = 0;
        // size is a size in bytes of the affected area of backing memory for buffer,
        // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
        blassBufferMemoryBarrier2.size =
            VK_WHOLE_SIZE;

        blassBufferMemoryBarrier2 = blassBufferMemoryBarrier2s.emplace_back();

        blassBufferMemoryBarrier2.sType =
            VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        blassBufferMemoryBarrier2.pNext =
            VK_NULL_HANDLE;
        // Need to wait previous stage 
        blassBufferMemoryBarrier2.srcStageMask =
            VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
        // Need to wait on specific memory access.
        blassBufferMemoryBarrier2.srcAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
            VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
        // All build commands need to wait that the texture change layout.
        blassBufferMemoryBarrier2.dstStageMask =
            VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
        // All write/read commands need to wait that the texture change layout.
        blassBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
            VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
        // Need to transfer ownership
        blassBufferMemoryBarrier2.srcQueueFamilyIndex =
            VK_QUEUE_FAMILY_IGNORED;
        blassBufferMemoryBarrier2.dstQueueFamilyIndex =
            VK_QUEUE_FAMILY_IGNORED;
        // buffer
        blassBufferMemoryBarrier2.buffer =
            blas.get().getScratchBuffer().getVkBuffer();
        // offset is an offset in bytes into the backing memory for buffer;
        // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
        blassBufferMemoryBarrier2.offset = 0;
        // size is a size in bytes of the affected area of backing memory for buffer,
        // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
        blassBufferMemoryBarrier2.size =
            VK_WHOLE_SIZE;
    }

    VkBufferMemoryBarrier2 tlasBufferMemoryBarrier2 = {};
    tlasBufferMemoryBarrier2.sType =
        VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    tlasBufferMemoryBarrier2.pNext =
        VK_NULL_HANDLE;
    // Need to wait previous stage 
    tlasBufferMemoryBarrier2.srcStageMask =
        VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
    // Need to wait on specific memory access.
    tlasBufferMemoryBarrier2.srcAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
        VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    // All build commands need to wait that the texture change layout.
    tlasBufferMemoryBarrier2.dstStageMask =
        VK_PIPELINE_STAGE_2_NONE;
    // All write/read commands need to wait that the texture change layout.
    tlasBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_NONE;
    // Need to transfer ownership
    tlasBufferMemoryBarrier2.srcQueueFamilyIndex =
        VK_QUEUE_FAMILY_IGNORED;
    tlasBufferMemoryBarrier2.dstQueueFamilyIndex =
        VK_QUEUE_FAMILY_IGNORED;
    // buffer
    tlasBufferMemoryBarrier2.buffer =
        reserved->allTlas[frameInFlight].getASBuffer().getVkBuffer();
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    tlasBufferMemoryBarrier2.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    tlasBufferMemoryBarrier2.size =
        VK_WHOLE_SIZE;


    {
        ThreadPool threadPool;
        threadPool.setThreadCount(magic_enum::enum_integer(ThreadIdentifier::all));

        // Command to build AS.
        PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR =
            (PFN_vkCmdBuildAccelerationStructuresKHR)vkGetInstanceProcAddr(
                Engine::getInstance().getVulkanInstance().getVkInstance(), "vkCmdBuildAccelerationStructuresKHR");

        if (thereIsSomeRTObjDescToBuild)
            threadPool.getThreads()[magic_enum::enum_integer(ThreadIdentifier::rtObjDescs)].addJob(
                [&] { uploadRTObjDesc(reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::rtObjDescs)],
                    rtObjDescsToUpload,
                    reserved->stagingGeometriesRTObjDescsDataBuffers[frameInFlight],
                    reserved->rtObjDescStatusSyncObjGPU[frameInFlight],
                    rtObjDescBufferMemoryBarrier2s); });

        if (thereIsSomeBLASToBuild)
            threadPool.getThreads()[magic_enum::enum_integer(ThreadIdentifier::blass)].addJob(
                [&] { buildBLASs(reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::blass)],
                    reserved->blassToBuild[frameInFlight],
                    reserved->stagingTransformsBLASsDataBuffers[frameInFlight],
                    pvkCmdBuildAccelerationStructuresKHR,
                    reserved->rtObjDescStatusSyncObjGPU[frameInFlight],
                    rtObjDescBufferMemoryBarrier2s,
                    reserved->blasStatusSyncObjGPU[frameInFlight],
                    blassBufferMemoryBarrier2s); });


        std::list<std::reference_wrapper<OvVK::TopLevelAccelerationStructure>> tlassToBuild;
        tlassToBuild.push_back(reserved->allTlas[frameInFlight]);

        threadPool.getThreads()[magic_enum::enum_integer(ThreadIdentifier::tlass)].addJob(
            [&] { buildTLASs(reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::tlass)],
                tlassToBuild,
                reserved->stagingASInstanceTLASsDataBuffers[frameInFlight],
                pvkCmdBuildAccelerationStructuresKHR,
                reserved->blasStatusSyncObjGPU[frameInFlight],
                blassBufferMemoryBarrier2s,
                reserved->tlasStatusSyncObjGPU[frameInFlight],
                tlasBufferMemoryBarrier2); });

        // Process all the object to upload
        threadPool.wait();
    }

    //////////////////////
    // Records Commands //
    //////////////////////

#pragma region RecordsCommands

    // Perform all operation on the Pool of the current frame in flight.
    std::reference_wrapper<OvVK::CommandPool> computeCommandPool = engine.get().getPrimaryComputeCmdPool();
    std::reference_wrapper<OvVK::CommandPool> graphicCommandPool = engine.get().getPrimaryGraphicCmdPool();
    std::reference_wrapper<OvVK::CommandPool> transferCommandPool = engine.get().getPrimaryTransferCmdPool();

    std::reference_wrapper<OvVK::CommandBuffer> computeCB = computeCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> ownershipTransferCB = transferCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> ownershipGraphicCB = graphicCommandPool.get().allocateCommandBuffer();

    VkCommandBufferBeginInfo commandBufferBeginInfo = {};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    bool exit = false;
    if (computeCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        computeCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to begin or change status to the primary compute command buffer in rtscene with id= %d", getId());
        exit = true;
    }
    if (ownershipTransferCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        ownershipTransferCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to begin or change status to the ownership transfer command buffer in rtscene with id= %d", getId());
        exit = true;
    }
    if (ownershipGraphicCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        ownershipGraphicCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to begin or change status to the ownership graphic command buffer in rtscene with id= %d", getId());
        exit = true;
    }

    if (exit)
    {
        computeCommandPool.get().freeCommandBuffer(computeCB.get());
        transferCommandPool.get().freeCommandBuffer(ownershipTransferCB.get());
        graphicCommandPool.get().freeCommandBuffer(ownershipGraphicCB.get());
        this->reset();
        return false;
    }


    //////////////
    // Barriers //
    //////////////

    // Needed to change the ownership of the geometries if necessary
    std::vector<VkBufferMemoryBarrier2> geometriesBufferMemoryBarrier2TransferCB;
    std::vector<VkBufferMemoryBarrier2> geometriesBufferMemoryBarrier2GraphicCB;
    // Get geometries semaphore
    std::vector<VkSemaphore> geometriesSemaphoresTransferCB;
    std::vector<VkSemaphore> geometriesSemaphoresGraphicCB;


    ////////////////
    // Geometries //
    ////////////////

    // Change ownership of the buffer after geometry uploaded to queue compute.
    for (std::reference_wrapper<OvVK::Geometry> geometry : neededGeometries)
    {
        VkBufferMemoryBarrier2 geometryVertexBufferMemoryBarrier2 = {};
        geometryVertexBufferMemoryBarrier2.sType =
            VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        geometryVertexBufferMemoryBarrier2.pNext =
            VK_NULL_HANDLE;
        // No need to wait previous stage 
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
        geometryVertexBufferMemoryBarrier2.srcStageMask =
            VK_PIPELINE_STAGE_2_NONE;
        // No need to wait on specific memory access.
        geometryVertexBufferMemoryBarrier2.srcAccessMask =
            VK_ACCESS_2_NONE;
        // All commands need to wait that the texture change layout.
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
        geometryVertexBufferMemoryBarrier2.dstStageMask =
            VK_PIPELINE_STAGE_2_TRANSFER_BIT;
        // All commands need to wait that the texture change layout.
        geometryVertexBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
            VK_ACCESS_2_TRANSFER_READ_BIT;
        geometryVertexBufferMemoryBarrier2.dstQueueFamilyIndex =
            computeCommandPool.get().getQueueFamilyIndex().value();
        // buffer
        geometryVertexBufferMemoryBarrier2.buffer =
            geometry.get().getVertexBuffer().getVkBuffer();
        // offset is an offset in bytes into the backing memory for buffer;
        // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
        geometryVertexBufferMemoryBarrier2.offset = 0;
        // size is a size in bytes of the affected area of backing memory for buffer,
        // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
        geometryVertexBufferMemoryBarrier2.size =
            VK_WHOLE_SIZE;


        VkBufferMemoryBarrier2 geometryFaceBufferMemoryBarrier2 = {};
        geometryFaceBufferMemoryBarrier2.sType =
            VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        geometryFaceBufferMemoryBarrier2.pNext =
            VK_NULL_HANDLE;
        // No need to wait previous stage 
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
        geometryFaceBufferMemoryBarrier2.srcStageMask =
            VK_PIPELINE_STAGE_2_NONE;
        // No need to wait on specific memory access.
        geometryFaceBufferMemoryBarrier2.srcAccessMask =
            VK_ACCESS_2_NONE;
        // All commands need to wait that the texture change layout.
        geometryFaceBufferMemoryBarrier2.dstStageMask =
            VK_PIPELINE_STAGE_2_TRANSFER_BIT;
        // All commands need to wait that the texture change layout.
        geometryFaceBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
            VK_ACCESS_2_TRANSFER_READ_BIT;
        geometryFaceBufferMemoryBarrier2.dstQueueFamilyIndex =
            computeCommandPool.get().getQueueFamilyIndex().value();
        // buffer
        geometryFaceBufferMemoryBarrier2.buffer =
            geometry.get().getFaceBuffer().getVkBuffer();
        // offset is an offset in bytes into the backing memory for buffer;
        // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
        geometryFaceBufferMemoryBarrier2.offset = 0;
        // size is a size in bytes of the affected area of backing memory for buffer,
        // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
        geometryFaceBufferMemoryBarrier2.size =
            VK_WHOLE_SIZE;


        if (geometry.get().getVertexBuffer().getQueueFamilyIndex().value() == transferCommandPool.get().getQueueFamilyIndex().value())
        {
            // Need to transfer ownership
            geometryVertexBufferMemoryBarrier2.srcQueueFamilyIndex =
                transferCommandPool.get().getQueueFamilyIndex().value();
            geometryFaceBufferMemoryBarrier2.srcQueueFamilyIndex =
                transferCommandPool.get().getQueueFamilyIndex().value();

            geometriesBufferMemoryBarrier2TransferCB.push_back(geometryVertexBufferMemoryBarrier2);
            geometriesBufferMemoryBarrier2TransferCB.push_back(geometryFaceBufferMemoryBarrier2);

            geometriesSemaphoresTransferCB.push_back(geometry.get().getVertexBuffer().getStatusSyncObj());
            geometriesSemaphoresTransferCB.push_back(geometry.get().getFaceBuffer().getStatusSyncObj());
        }
        else if (geometry.get().getVertexBuffer().getQueueFamilyIndex().value() == graphicCommandPool.get().getQueueFamilyIndex().value())
        {
            // Need to transfer ownership
            geometryVertexBufferMemoryBarrier2.srcQueueFamilyIndex =
                graphicCommandPool.get().getQueueFamilyIndex().value();
            geometryFaceBufferMemoryBarrier2.srcQueueFamilyIndex =
                graphicCommandPool.get().getQueueFamilyIndex().value();

            geometriesBufferMemoryBarrier2GraphicCB.push_back(geometryVertexBufferMemoryBarrier2);
            geometriesBufferMemoryBarrier2GraphicCB.push_back(geometryFaceBufferMemoryBarrier2);

            geometriesSemaphoresGraphicCB.push_back(geometry.get().getVertexBuffer().getStatusSyncObj());
            geometriesSemaphoresGraphicCB.push_back(geometry.get().getFaceBuffer().getStatusSyncObj());
        }
    }

    // Barrier specification
    VkDependencyInfo geometriesDependencyInfo = {};
    geometriesDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    geometriesDependencyInfo.dependencyFlags = 0x00000000;
    geometriesDependencyInfo.memoryBarrierCount = 0;
    geometriesDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    geometriesDependencyInfo.bufferMemoryBarrierCount = geometriesBufferMemoryBarrier2TransferCB.size();
    geometriesDependencyInfo.pBufferMemoryBarriers = geometriesBufferMemoryBarrier2TransferCB.data();
    geometriesDependencyInfo.imageMemoryBarrierCount = 0;
    geometriesDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register barrier for ownership transfer
    if (geometriesBufferMemoryBarrier2TransferCB.size() > 0) {
        vkCmdPipelineBarrier2(ownershipTransferCB.get().getVkCommandBuffer(), &geometriesDependencyInfo);
        vkCmdPipelineBarrier2(computeCB.get().getVkCommandBuffer(), &geometriesDependencyInfo);
    }

    // Barrier specification
    geometriesDependencyInfo.bufferMemoryBarrierCount = geometriesBufferMemoryBarrier2GraphicCB.size();
    geometriesDependencyInfo.pBufferMemoryBarriers = geometriesBufferMemoryBarrier2GraphicCB.data();

    // Register barrier for ownership transfer
    if (geometriesBufferMemoryBarrier2GraphicCB.size() > 0) {
        vkCmdPipelineBarrier2(ownershipGraphicCB.get().getVkCommandBuffer(), &geometriesDependencyInfo);
        vkCmdPipelineBarrier2(computeCB.get().getVkCommandBuffer(), &geometriesDependencyInfo);
    }


    ///////////////
    // RTObjDesc //
    ///////////////

    if (thereIsSomeRTObjDescToBuild) {

        vkResetEvent(logicalDevice.get().getVkDevice(), reserved->rtObjDescStatusSyncObjGPU[frameInFlight]);

        // Transfer command
        std::vector<VkCommandBuffer> rtObjDescSecondaryCBs;

        // Geometries
        for (std::reference_wrapper<OvVK::CommandBuffer> commandBuffer :
            reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::rtObjDescs)].commandBuffers)
            rtObjDescSecondaryCBs.push_back(commandBuffer.get().getVkCommandBuffer());

        // Execute render commands from the secondary command buffer
        vkCmdExecuteCommands(computeCB.get().getVkCommandBuffer(),
            static_cast<uint32_t>(rtObjDescSecondaryCBs.size()), rtObjDescSecondaryCBs.data());
    }
    else {
        vkSetEvent(logicalDevice.get().getVkDevice(), reserved->rtObjDescStatusSyncObjGPU[frameInFlight]);
    }

    ////////
    // AS //
    ////////

    if (thereIsSomeBLASToBuild) {

        vkResetEvent(logicalDevice.get().getVkDevice(), reserved->blasStatusSyncObjGPU[frameInFlight]);

        // Transfer secondary CBs
        std::vector<VkCommandBuffer> blasSecondaryCBs;

        // Blass
        for (std::reference_wrapper<OvVK::CommandBuffer> commandBuffer :
            reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::blass)].commandBuffers)
            blasSecondaryCBs.push_back(commandBuffer.get().getVkCommandBuffer());

        // No need to transfer ownership because first time used.
        // Execute render commands from the secondary command buffer.
        vkCmdExecuteCommands(computeCB.get().getVkCommandBuffer(),
            static_cast<uint32_t>(blasSecondaryCBs.size()), blasSecondaryCBs.data());
    }
    else {
        vkSetEvent(logicalDevice.get().getVkDevice(), reserved->blasStatusSyncObjGPU[frameInFlight]);
    }

    vkResetEvent(logicalDevice.get().getVkDevice(), reserved->tlasStatusSyncObjGPU[frameInFlight]);

    // Transfer secondary CBs
    std::vector<VkCommandBuffer> tlasSecondaryCBs;

    // Tlass
    for (std::reference_wrapper<OvVK::CommandBuffer> commandBuffer :
        reserved->threadsData[frameInFlight][magic_enum::enum_integer(ThreadIdentifier::tlass)].commandBuffers)
        tlasSecondaryCBs.push_back(commandBuffer.get().getVkCommandBuffer());

    // No need to transfer ownership because first time used.
    // Execute render commands from the secondary command buffer.
    vkCmdExecuteCommands(computeCB.get().getVkCommandBuffer(),
        static_cast<uint32_t>(tlasSecondaryCBs.size()), tlasSecondaryCBs.data());

    // Barrier specification
    VkDependencyInfo tlasDependencyInfo = {};
    tlasDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    tlasDependencyInfo.dependencyFlags = 0x00000000;
    tlasDependencyInfo.memoryBarrierCount = 0;
    tlasDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    tlasDependencyInfo.bufferMemoryBarrierCount = 1;
    tlasDependencyInfo.pBufferMemoryBarriers = &tlasBufferMemoryBarrier2;
    tlasDependencyInfo.imageMemoryBarrierCount = 0;
    tlasDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register barrier for ownership transfer
    vkCmdWaitEvents2(computeCB.get().getVkCommandBuffer(), 1, &reserved->tlasStatusSyncObjGPU[frameInFlight], &tlasDependencyInfo);

    // End primary commandBuffers
    if (computeCB.get().endVkCommandBuffer() == false ||
        computeCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to end or change status of the primary compute command buffer in rtscene with id= %d", getId());
        exit = true;
    }
    if (ownershipTransferCB.get().endVkCommandBuffer() == false ||
        ownershipTransferCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to end or change status of the ownership transfer command buffer in rtscene with id= %d", getId());
        exit = true;
    }
    if (ownershipGraphicCB.get().endVkCommandBuffer() == false ||
        ownershipGraphicCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to end or change status of the ownership graphic command buffer in rtscene with id= %d.", getId());
        exit = true;
    }

    if (exit)
    {
        computeCommandPool.get().freeCommandBuffer(computeCB.get());
        transferCommandPool.get().freeCommandBuffer(ownershipTransferCB.get());
        graphicCommandPool.get().freeCommandBuffer(ownershipGraphicCB.get());
        this->reset();
        return false;
    }


    /////////////////////////////////
    // Set SyncObj to check status //
    /////////////////////////////////
    
    // RTObjDesc
    for (std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc : rtObjDescsToUpload)
        rtObjDesc.get().setStatusSyncObjValue(OvVK::RTObjDesc::uploadingValueStatusSyncObj);

    // BLAS
    for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas :
        reserved->blassToBuild[frameInFlight])
        blas.get().setStatusSyncObjValue(OvVK::AccelerationStructure::buildingValueStatusSyncObj);

    // TLAS
    reserved->allTlas[frameInFlight].setStatusSyncObjValue(OvVK::AccelerationStructure::buildingValueStatusSyncObj);

    // PASS
    setPassStatusSyncObjGPUValue(OvVK::RTScene::startValuePassStatusSyncObj);

    ////////////
    // Submit //
    ////////////

    // transfer
    if (geometriesBufferMemoryBarrier2TransferCB.size() > 0) {

        // transfer
        VkSubmitInfo2 geometriesOwnershipTransferSubmitInfo = {};
        geometriesOwnershipTransferSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        geometriesOwnershipTransferSubmitInfo.pNext = NULL;

        std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(geometriesSemaphoresTransferCB.size());
        VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo = {};
        vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
        vkSemaphoreSubmitInfo.value = OvVK::Geometry::readyValueStatusSyncObj;
        vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        vkSemaphoreSubmitInfo.deviceIndex = 0;
        for (uint32_t c = 0; c < pWaitSemaphoreInfos.size(); c++) {
            vkSemaphoreSubmitInfo.semaphore = geometriesSemaphoresTransferCB[c];
            pWaitSemaphoreInfos[c] = vkSemaphoreSubmitInfo;
        }

        geometriesOwnershipTransferSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        geometriesOwnershipTransferSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos = {};
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = ownershipTransferCB.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        geometriesOwnershipTransferSubmitInfo.commandBufferInfoCount = 1;
        geometriesOwnershipTransferSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos = {};
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.value = OvVK::RTScene::finishValueRederingStatusSyncObj;
        pSignalSemaphoreInfos.semaphore = reserved->ownershipTransferStatusSyncObjGPU[frameInFlight];
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        geometriesOwnershipTransferSubmitInfo.signalSemaphoreInfoCount = 1;
        geometriesOwnershipTransferSubmitInfo.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        // Submit
        if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(transferCommandPool.get().getQueueFamilyIndex().value()),
            1, &geometriesOwnershipTransferSubmitInfo, NULL) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit transfer command buffer in rtscene with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCB.get());
            transferCommandPool.get().freeCommandBuffer(ownershipTransferCB.get());
            graphicCommandPool.get().freeCommandBuffer(ownershipGraphicCB.get());
            this->reset();
            return false;
        }
    }

    // graphic
    if (geometriesBufferMemoryBarrier2GraphicCB.size() > 0) {

        // transfer
        VkSubmitInfo2 geometriesOwnershipGraphicSubmitInfo = {};
        geometriesOwnershipGraphicSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        geometriesOwnershipGraphicSubmitInfo.pNext = NULL;

        std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(geometriesSemaphoresGraphicCB.size());
        VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo;
        vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
        vkSemaphoreSubmitInfo.value = OvVK::Geometry::readyValueStatusSyncObj;
        vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        vkSemaphoreSubmitInfo.deviceIndex = 0;
        for (uint32_t c = 0; c < pWaitSemaphoreInfos.size(); c++) {
            vkSemaphoreSubmitInfo.semaphore = geometriesSemaphoresGraphicCB[c];
            pWaitSemaphoreInfos[c] = vkSemaphoreSubmitInfo;
        }

        geometriesOwnershipGraphicSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        geometriesOwnershipGraphicSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos = {};
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = ownershipGraphicCB.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        geometriesOwnershipGraphicSubmitInfo.commandBufferInfoCount = 1;
        geometriesOwnershipGraphicSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos = {};
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.value = OvVK::RTScene::finishValueRederingStatusSyncObj;
        pSignalSemaphoreInfos.semaphore = reserved->ownershipGraphicStatusSyncObjGPU[frameInFlight];
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        geometriesOwnershipGraphicSubmitInfo.signalSemaphoreInfoCount = 1;
        geometriesOwnershipGraphicSubmitInfo.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        // Submit
        if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(graphicCommandPool.get().getQueueFamilyIndex().value()),
            1, &geometriesOwnershipGraphicSubmitInfo, NULL) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit graphic command buffer in rtscene with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCB.get());
            transferCommandPool.get().freeCommandBuffer(ownershipTransferCB.get());
            graphicCommandPool.get().freeCommandBuffer(ownershipGraphicCB.get());
            this->reset();
            return false;
        }
    }

    // Compute
    VkSubmitInfo2 asSubmitInfo = {};
    asSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    asSubmitInfo.pNext = NULL;

    // Gather VkSemaphores
    std::vector<VkSemaphore> semaphoreHandle(rtObjDescsToUpload.size() +
        reserved->blassToBuild[frameInFlight].size() + 2, VK_NULL_HANDLE);
    uint64_t asSemaphoreCounter = 0;
    // Use scratch buffer because we will update AS and rimeline semaphore value can be only increased.
    for (std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc : rtObjDescsToUpload)
    {
        // Get sync status obj.
        semaphoreHandle[asSemaphoreCounter] = rtObjDesc.get().getGeometryDataDeviceBuffer().getStatusSyncObj();
        asSemaphoreCounter++;
    }
    for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas : reserved->blassToBuild[frameInFlight])
    {
        // Get sync status obj.
        semaphoreHandle[asSemaphoreCounter] = blas.get().getASBuffer().getStatusSyncObj();
        asSemaphoreCounter++;
    }

    // Get sync status obj.
    semaphoreHandle[asSemaphoreCounter] = reserved->allTlas[frameInFlight].getASBuffer().getStatusSyncObj();
    asSemaphoreCounter++;
    // Adds sync obj rtscene pass
    semaphoreHandle[asSemaphoreCounter] = reserved->passStatusSyncObjGPU[frameInFlight];


    // To wait semaphore
    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(semaphoreHandle.size());

    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoWait = {};
    vkSemaphoreSubmitInfoWait.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfoWait.pNext = VK_NULL_HANDLE;
    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT => deprecated
    vkSemaphoreSubmitInfoWait.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfoWait.deviceIndex = 0;

    asSemaphoreCounter = 0;

    vkSemaphoreSubmitInfoWait.value = OvVK::RTObjDesc::uploadingValueStatusSyncObj;
    for (uint32_t c = 0; c < rtObjDescsToUpload.size(); c++) {
        vkSemaphoreSubmitInfoWait.semaphore = semaphoreHandle[asSemaphoreCounter];
        pWaitSemaphoreInfos[asSemaphoreCounter] = vkSemaphoreSubmitInfoWait;
        asSemaphoreCounter++;
    }

    vkSemaphoreSubmitInfoWait.value = OvVK::AccelerationStructure::buildingValueStatusSyncObj;
    for (uint32_t c = 0; c < (reserved->blassToBuild[frameInFlight].size() + 1); c++) {
        vkSemaphoreSubmitInfoWait.semaphore = semaphoreHandle[asSemaphoreCounter];
        pWaitSemaphoreInfos[asSemaphoreCounter] = vkSemaphoreSubmitInfoWait;
        asSemaphoreCounter++;
    }

    vkSemaphoreSubmitInfoWait.value = OvVK::RTScene::startValuePassStatusSyncObj;
    vkSemaphoreSubmitInfoWait.semaphore = semaphoreHandle[asSemaphoreCounter];
    pWaitSemaphoreInfos[asSemaphoreCounter] = vkSemaphoreSubmitInfoWait;

    // Change ownership geometries
    if (geometriesBufferMemoryBarrier2TransferCB.size() > 0)
    {
        vkSemaphoreSubmitInfoWait.semaphore = reserved->ownershipTransferStatusSyncObjGPU[frameInFlight];
        // Ignored by vulkan but need to be set
        vkSemaphoreSubmitInfoWait.value = OvVK::RTScene::finishValueRederingStatusSyncObj;
        pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfoWait);
    }
    if (geometriesBufferMemoryBarrier2GraphicCB.size() > 0)
    {
        vkSemaphoreSubmitInfoWait.semaphore = reserved->ownershipGraphicStatusSyncObjGPU[frameInFlight];
        // Ignored by vulkan but need to be set
        vkSemaphoreSubmitInfoWait.value = OvVK::RTScene::finishValueRederingStatusSyncObj;
        pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfoWait);
    }

    asSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
    asSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();


    // To signal semaphore
    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(semaphoreHandle.size());
    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoSignal = {};
    vkSemaphoreSubmitInfoSignal.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfoSignal.pNext = VK_NULL_HANDLE;
    // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT => deprecated
    vkSemaphoreSubmitInfoSignal.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfoSignal.deviceIndex = 0;

    asSemaphoreCounter = 0;

    vkSemaphoreSubmitInfoSignal.value = OvVK::RTObjDesc::readyValueStatusSyncObj;
    for (uint32_t c = 0; c < rtObjDescsToUpload.size(); c++) {
        vkSemaphoreSubmitInfoSignal.semaphore = semaphoreHandle[asSemaphoreCounter];
        pSignalSemaphoreInfos[asSemaphoreCounter] = vkSemaphoreSubmitInfoSignal;
        asSemaphoreCounter++;
    }

    vkSemaphoreSubmitInfoSignal.value = OvVK::AccelerationStructure::readyValueStatusSyncObj;
    for (uint32_t c = 0; c < (reserved->blassToBuild[frameInFlight].size() + 1); c++) {
        vkSemaphoreSubmitInfoSignal.semaphore = semaphoreHandle[asSemaphoreCounter];
        pSignalSemaphoreInfos[asSemaphoreCounter] = vkSemaphoreSubmitInfoSignal;
        asSemaphoreCounter++;
    }

    vkSemaphoreSubmitInfoSignal.value = OvVK::RTScene::finishValuePassStatusSyncObj;
    vkSemaphoreSubmitInfoSignal.semaphore = semaphoreHandle[asSemaphoreCounter];
    pSignalSemaphoreInfos[asSemaphoreCounter] = vkSemaphoreSubmitInfoSignal;

    asSubmitInfo.signalSemaphoreInfoCount = pSignalSemaphoreInfos.size();
    asSubmitInfo.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();

    
    // Set command buffer
    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = computeCB.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;

    asSubmitInfo.commandBufferInfoCount = 1;
    asSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

    // Submit
    // Reset sync obj CPU.
    if (reserved->passStatusSyncObjCPU[frameInFlight] != VK_NULL_HANDLE &&
        vkResetFences(logicalDevice.get().getVkDevice(), 1, &reserved->passStatusSyncObjCPU[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to reset the pass fence of the frame in flight number %d of the rtscene with id= %d.",
            frameInFlight, getId());
        return false;
    }

    VkResult resultSubmitCommandBuffer = vkQueueSubmit2(logicalDevice.get().getQueueHandle(computeCommandPool.get().getQueueFamilyIndex().value()),
        1, &asSubmitInfo, reserved->passStatusSyncObjCPU[frameInFlight]);
    if(resultSubmitCommandBuffer != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit compute commandBuffer in rtscene with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCB.get());
        transferCommandPool.get().freeCommandBuffer(ownershipTransferCB.get());
        graphicCommandPool.get().freeCommandBuffer(ownershipGraphicCB.get());
        this->reset();
        return false;
    }

    ///////////////////
    // Set Ownership //
    ///////////////////

    // Geometries
    for (std::reference_wrapper<OvVK::Geometry> geometry : neededGeometries) {
        geometry.get().setLockSyncObj(VK_NULL_HANDLE);
        geometry.get().setQueueFamilyIndex(computeCommandPool.get().getQueueFamilyIndex().value());
        //geometry.get().setLockSyncObj(reserved->passStatusSyncObjCPU[frameInFlight]);
    }

    // RTObjDesc
    for (std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc : rtObjDescsToUpload)
    {
        rtObjDesc.get().setLockSyncObj(VK_NULL_HANDLE);
        rtObjDesc.get().setQueueFamilyIndex(computeCommandPool.get().getQueueFamilyIndex().value());
        //rtObjDesc.get().setLockSyncObj(reserved->passStatusSyncObjCPU[frameInFlight]);
    }

    // BLAS
    for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas :
        reserved->blassToBuild[frameInFlight]) {
        blas.get().setLockSyncObj(VK_NULL_HANDLE);
        blas.get().setQueueFamilyIndex(computeCommandPool.get().getQueueFamilyIndex().value());
        //blas.get().setLockSyncObj(reserved->passStatusSyncObjCPU[frameInFlight]);
    }

    // TLAS
    reserved->allTlas[frameInFlight].setLockSyncObj(VK_NULL_HANDLE);
    reserved->allTlas[frameInFlight].setQueueFamilyIndex(computeCommandPool.get().getQueueFamilyIndex().value());
    //reserved->allTlas[frameInFlight].setLockSyncObj(reserved->passStatusSyncObjCPU[frameInFlight]);

#pragma endregion

    // Set to how to build the SBT
    reserved->areGeometriesSharingSameShadersGroups[frameInFlight] = geometriesSharingSameShadersGroups;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Update the renderable elements present in the TLAS of the rtscene of the current frame in flight.
 * @param node The node starting node
 * @param prevMatrix The previous node matrix
 * @return TF
 */
bool OVVK_API OvVK::RTScene::update(const Ov::Node& node, glm::mat4 prevMatrix)
{
    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // PhysicalDevice
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

    // Safety net:
    if (reserved->allTlas[frameInFlight].getVkAS() == VK_NULL_HANDLE) {
        OV_LOG_ERROR(R"(Impossible to update the TLAS of the frame in flight %d of rtscene with id= %d because
 not present. Need to call pass method.)", frameInFlight, getId());
        return false;
    }

    // Wait CPU pass sync object
    // Wait fences to know the work is finished
    std::vector<VkFence> toWaitFences;

    if (reserved->passStatusSyncObjCPU[frameInFlight] != VK_NULL_HANDLE)
        toWaitFences.push_back(reserved->passStatusSyncObjCPU[frameInFlight]);
    if (reserved->allTlas[frameInFlight].getASBuffer().getLockSyncObj() != VK_NULL_HANDLE)
        toWaitFences.push_back(reserved->allTlas[frameInFlight].getASBuffer().getLockSyncObj());

    if (vkWaitForFences(logicalDevice.get().getVkDevice(), toWaitFences.size(), toWaitFences.data(),
        VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait pass Fence in rtscene with id= %d.", getId());
        return false;
    }

    // Clean old same frame in flight.
    this->clearPassData();


    ////////////////////
    // Reset syncObjs //
    ////////////////////

    // Reset sync obj CPU.
    if (vkResetFences(logicalDevice.get().getVkDevice(), 1, &reserved->passStatusSyncObjCPU[frameInFlight])
        != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to reset the pass fence of the frame in flight number %d of the rtscene with id= %d.",
            frameInFlight, getId());
        return false;
    }

    reserved->allTlas[frameInFlight].setLockSyncObj(VK_NULL_HANDLE);


    /////////////
    // Updates //
    /////////////

    // update matrix
    std::map<uint32_t, OvVK::RTScene::RenderableElem> nodeMatrix;
    bool isRoot = false;

    // Lambda to process all the node in the scene graph
    std::function<bool(const Ov::Node& node, const glm::mat4& prevMatrix)> passSceneGraph;
    passSceneGraph = [&passSceneGraph, &nodeMatrix, &isRoot, this](const Ov::Node& node,
        const glm::mat4& prevMatrix) -> bool
    {
        // Safety net:
        if (node == Ov::Node::empty)
        {
            OV_LOG_ERROR("Invalid params");
            return false;
        }

        // Renderable element
        OvVK::RTScene::RenderableElem re;
        if (isRoot)
        {
            re.matrix = prevMatrix * node.getWorldMatrix();
            isRoot = false;
        }
        else { re.matrix = prevMatrix * node.getMatrix(); }

        re.visibleFlag = node.isVisible();

        nodeMatrix.insert({ node.getId(), re });

        // Parse hiearchy recursively:
        for (auto& n : node.getListOfChildren())
            if (passSceneGraph(n, re.matrix) == false)
                return false;

        // Done:
        return true;
    };

    isRoot = true;
    // Pass the graph
    if (passSceneGraph(node, prevMatrix) == false)
    {
        OV_LOG_ERROR("Fail to pass the scene graph releated to the passed node in the rtscene with id= %d",
            getId());
        return false;
    }

    // Update Renderables
    uint32_t currentRTObjDescToRetrive = 0;
    for (auto& re : reserved->renderableElements[frameInFlight])
    {
        // Update renderable info
        OvVK::RTScene::RenderableElem updateInfo = nodeMatrix[re.reference.get().getId()];
        re.visibleFlag = updateInfo.visibleFlag;
        re.matrix = updateInfo.matrix;

        // Update TLAS instance
        if (OvVK::RTObjDesc* rtObjDesc = dynamic_cast<OvVK::RTObjDesc*>(&re.renderable.get())) {

            // Skip empty BLAS
            if (rtObjDesc->getBLAS() == OvVK::BottomLevelAccelerationStructure::empty)
                continue;

			VkAccelerationStructureInstanceKHR* instance =
				reinterpret_cast<VkAccelerationStructureInstanceKHR*>(((uint8_t*)
                    reserved->stagingASInstanceTLASsDataBuffers[frameInFlight].getVmaAllocationInfo().pMappedData) +
					(currentRTObjDescToRetrive * sizeof(VkAccelerationStructureInstanceKHR)));

            //glm::mat4 => column major
            //VkTransformMatrixKHR => row major
            VkTransformMatrixKHR transformMatrix = {};
            transformMatrix.matrix[0][0] = re.matrix[0][0];
            transformMatrix.matrix[1][0] = re.matrix[0][1];
            transformMatrix.matrix[2][0] = re.matrix[0][2];

            transformMatrix.matrix[0][1] = re.matrix[1][0];
            transformMatrix.matrix[1][1] = re.matrix[1][1];
            transformMatrix.matrix[2][1] = re.matrix[1][2];

            transformMatrix.matrix[0][2] = re.matrix[2][0];
            transformMatrix.matrix[1][2] = re.matrix[2][1];
            transformMatrix.matrix[2][2] = re.matrix[2][2];

            transformMatrix.matrix[0][3] = re.matrix[3][0];
            transformMatrix.matrix[1][3] = re.matrix[3][1];
            transformMatrix.matrix[2][3] = re.matrix[3][2];

            instance->transform = transformMatrix;
            instance->mask = (re.visibleFlag ? 0xFF : 0x00);

            currentRTObjDescToRetrive++;
        }
    }

    // SetUp Update TLAS
    if (reserved->allTlas[frameInFlight].setUpUpdate() == false)
    {
        OV_LOG_ERROR(R"(Fail to setUp the TLAS update in rtscene with id= %d for the frame in flight %d)"
            , getId(), frameInFlight);
        return false;
    }


    ///////////////////////
    // Performs commands //
    ///////////////////////

    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    std::reference_wrapper<OvVK::CommandPool> computeCommandPool = engine.get().getPrimaryComputeCmdPool();
    std::reference_wrapper<OvVK::CommandBuffer> computeCommandBuffer = computeCommandPool.get().allocateCommandBuffer();

    if (computeCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        computeCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to begin or change status to the compute command buffer in rtscene with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        return false;
    }


    VkBufferCopy copyBuffer = {};
    copyBuffer.srcOffset = 0;
    copyBuffer.dstOffset = 0;

    copyBuffer.size = reserved->stagingASInstanceTLASsDataBuffers[frameInFlight].getSize();
    vkCmdCopyBuffer(computeCommandBuffer.get().getVkCommandBuffer(),
        reserved->stagingASInstanceTLASsDataBuffers[frameInFlight].getVkBuffer(),
        reserved->allTlas[frameInFlight].getDeviceASInstancesBuffer().getVkBuffer(), 1, &copyBuffer);

    VkBufferMemoryBarrier2 vkBufferMemoryBarrier2 = {};
    vkBufferMemoryBarrier2.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    vkBufferMemoryBarrier2.pNext = VK_NULL_HANDLE;
    // Need to wait previous stage 
    vkBufferMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // Need to wait on specific memory access.
    vkBufferMemoryBarrier2.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
        VK_ACCESS_2_TRANSFER_READ_BIT;
    // All transfer commands need to wait that the texture change layout.
    vkBufferMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
    // All write commands need to wait that the texture change layout.
    vkBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
        VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    // Need to transfer ownership
    vkBufferMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    vkBufferMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // buffer
    vkBufferMemoryBarrier2.buffer = reserved->allTlas[frameInFlight].getDeviceASInstancesBuffer().getVkBuffer();
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    vkBufferMemoryBarrier2.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    vkBufferMemoryBarrier2.size = VK_WHOLE_SIZE;

    // Barrier specification
    VkDependencyInfo VkDependencyInfo = {};
    VkDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    VkDependencyInfo.dependencyFlags = 0x00000000;
    VkDependencyInfo.memoryBarrierCount = 0;
    VkDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    VkDependencyInfo.bufferMemoryBarrierCount = 1;
    VkDependencyInfo.pBufferMemoryBarriers = &vkBufferMemoryBarrier2;
    VkDependencyInfo.imageMemoryBarrierCount = 0;
    VkDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &VkDependencyInfo);

    // Build TLAS
    const VkAccelerationStructureBuildGeometryInfoKHR& accelerationStructureBuildGeometryInfoKHR
        = reserved->allTlas[frameInFlight].getAccelerationStructureBuildGeometryInfo();

    const VkAccelerationStructureBuildRangeInfoKHR* accelerationStructureBuildRangeInfo =
        &reserved->allTlas[frameInFlight].getAccelerationStructureBuildRangeInfosKHR().value().get();
    const VkAccelerationStructureBuildRangeInfoKHR** accelerationStructureBuildRangeInfos =
        &accelerationStructureBuildRangeInfo;

    // Build an acceleration structure
    PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR =
        (PFN_vkCmdBuildAccelerationStructuresKHR)vkGetInstanceProcAddr(
            Engine::getInstance().getVulkanInstance().getVkInstance(), "vkCmdBuildAccelerationStructuresKHR");

    pvkCmdBuildAccelerationStructuresKHR(computeCommandBuffer.get().getVkCommandBuffer(), 1,
        &accelerationStructureBuildGeometryInfoKHR,
        accelerationStructureBuildRangeInfos);


    // End primary commandBuffers
    if (computeCommandBuffer.get().endVkCommandBuffer() == false ||
        computeCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to end or change status to the compute command buffer in rtscene with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        return false;
    }


    /////////////////////////////////
    // Set SyncObj to check status //
    /////////////////////////////////
    
    // TLAS
    reserved->allTlas[frameInFlight].setStatusSyncObjValue(OvVK::AccelerationStructure::buildingValueStatusSyncObj);

    // PASS
    setPassStatusSyncObjGPUValue(OvVK::RTScene::startValuePassStatusSyncObj);


    ////////////
    // Submit //
    ////////////

    // Compute
    VkSubmitInfo2 asSubmitInfo = {};
    asSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    asSubmitInfo.pNext = NULL;

    // Gather VkSemaphores
    std::vector<VkSemaphore> semaphoreHandle(2, VK_NULL_HANDLE);
    // Get sync status obj.
    semaphoreHandle[0] = reserved->allTlas[frameInFlight].getScratchBuffer().getStatusSyncObj();
    // Adds sync obj rtscene pass
    semaphoreHandle[1] = reserved->passStatusSyncObjGPU[frameInFlight];


    // To signal semaphore
    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(semaphoreHandle.size());
    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfo.deviceIndex = 0;

    vkSemaphoreSubmitInfo.value = OvVK::AccelerationStructure::readyValueStatusSyncObj;
    vkSemaphoreSubmitInfo.semaphore = semaphoreHandle[0];
    pSignalSemaphoreInfos[0] = vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.value = OvVK::RTScene::finishValuePassStatusSyncObj;
    vkSemaphoreSubmitInfo.semaphore = semaphoreHandle[1];
    pSignalSemaphoreInfos[1] = vkSemaphoreSubmitInfo;

    asSubmitInfo.signalSemaphoreInfoCount = pSignalSemaphoreInfos.size();
    asSubmitInfo.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();

    // To wait semaphore
    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(semaphoreHandle.size());
    vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;

    vkSemaphoreSubmitInfo.value = OvVK::AccelerationStructure::buildingValueStatusSyncObj;
    vkSemaphoreSubmitInfo.semaphore = semaphoreHandle[0];
    pWaitSemaphoreInfos[0] = vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.value = OvVK::RTScene::startValuePassStatusSyncObj;
    vkSemaphoreSubmitInfo.semaphore = semaphoreHandle[1];
    pWaitSemaphoreInfos[1] = vkSemaphoreSubmitInfo;

    asSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
    asSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

    // Set command buffer
    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = computeCommandBuffer.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;

    asSubmitInfo.commandBufferInfoCount = 1;
    asSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

    // Submit
    if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(computeCommandPool.get().getQueueFamilyIndex().value()),
        1, &asSubmitInfo, reserved->passStatusSyncObjCPU[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit compute commandBuffer in rtscene with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        return false;
    }


    //////////////
    // Set Lock //
    //////////////

    // TLAS
    reserved->allTlas[frameInFlight].setQueueFamilyIndex(computeCommandPool.get().getQueueFamilyIndex().value());
    reserved->allTlas[frameInFlight].setLockSyncObj(reserved->passStatusSyncObjCPU[frameInFlight]);


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Reset rtscene for current frame in flight.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::reset()
{
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Current frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

    // Reset renderables
    reserved->renderableElements[frameInFlight].clear();
    // Free TLAS
    reserved->allTlas[frameInFlight].free();
    // Free TLAS staging buffer
    reserved->stagingASInstanceTLASsDataBuffers[frameInFlight].free();
    // Reset light numbers
    reserved->nrOfLights[frameInFlight] = 0;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear pass data for current frame in flight.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::clearPassData()
{
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Current frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

    // Free staging and scrath buffers
    for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas : reserved->blassToBuild[frameInFlight])
        blas.get().clearSetUpData();

    // Clear blass to build
    reserved->blassToBuild[frameInFlight].clear();
    // Free BLAS staging buffer
    reserved->stagingTransformsBLASsDataBuffers[frameInFlight].free();
    // Free RTObjDesc staging buffer
    reserved->stagingGeometriesRTObjDescsDataBuffers[frameInFlight].free();

    // Done:
    return true;
}

#pragma endregion

#pragma region SyncData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to records command buffer to upload rtObjDescs using a differente thread.
 * @param threadData The data used by the thread processing the rtObjDescs.
 * @param rtObjDescs List of rtObjDescs reference to build.
 * @param stagingGeometriesDataBuffer The staging OvVK buffer that will contains the rtObjDescs data to upload.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::uploadRTObjDesc(
    OvVK::RTScene::ThreadData& threadData,
    std::list<std::reference_wrapper<OvVK::RTObjDesc>>& rtObjDescs,
    OvVK::Buffer& stagingGeometriesDataBuffer,
    VkEvent eventToSignal, 
    std::vector<VkBufferMemoryBarrier2>& rtObjDescBufferMemoryBarrier2s)
{
    // Create command pool
    if (threadData.commandPool.create(OvVK::CommandPool::CommandTypesFlagBits::Transfer) == false)
    {
        OV_LOG_ERROR("Fail to create RTObjDesc commandPool for thread in rtscene with id= %d.", getId());
        return false;
    }

    // Structure defines any state that will be inherited from the primary command buffer
    VkCommandBufferInheritanceInfo commandBufferInheritanceInfo = {};
    commandBufferInheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    commandBufferInheritanceInfo.pNext = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.renderPass = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.subpass = 0;
    commandBufferInheritanceInfo.framebuffer = VK_NULL_HANDLE;
    // occlusionQueryEnable specifies whether the command buffer can be executed while an occlusion query
    // is active in the primary command buffer. If this is VK_TRUE, then this command buffer can be executed
    // whether the primary command buffer has an occlusion query active or not. If this is VK_FALSE, 
    // then the primary command buffer must not have an occlusion query active.
    // Occlusion queries are only available on queue families supporting graphics operations.
    commandBufferInheritanceInfo.occlusionQueryEnable = VK_FALSE;
    commandBufferInheritanceInfo.queryFlags = 0x00000000;
    commandBufferInheritanceInfo.pipelineStatistics = 0x00000000;

    // Begin CommandBuffer
    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    commandBufferBeginInfo.pInheritanceInfo = &commandBufferInheritanceInfo;

    // Allocate commandBuffer
    std::reference_wrapper<OvVK::CommandBuffer> commandBuffer =
        threadData.commandPool.allocateCommandBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY);

    if (commandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        commandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        threadData.commandPool.freeCommandBuffer(commandBuffer.get());
        OV_LOG_ERROR(R"(Fail to begin or set status of secondary command buffer to upload rtObjDesc
 with name = %s and id = %d in rtscene with id= %d.)", getId());
        return false;
    }

    // Get commandBuffer
    VkCommandBuffer vkCommandBuffer = commandBuffer.get().getVkCommandBuffer();


    // pointer to staging buffer memeory
    uint8_t* currentPosition = ((uint8_t*)stagingGeometriesDataBuffer.getVmaAllocationInfo().pMappedData);
    // Offset to ready from staging buffer
    uint64_t offset = 0;
    // Copy info
    VkBufferCopy bufferCopy = {};
    bufferCopy.dstOffset = 0;


    // Record commandBuffer for all the rtObjDesc
    for (std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc : rtObjDescs)
    {
        std::vector<OvVK::RTObjDesc::GeometryData> geometriesData = rtObjDesc.get().getGeometriesData();
        size_t sizeInBytes = static_cast<size_t>(geometriesData.size() * sizeof(RTObjDesc::GeometryData));

        // Copy data on staging buffer
        memcpy(currentPosition, geometriesData.data(), sizeInBytes);
        // Move pointer to the memory where copy the data
        currentPosition += sizeInBytes;

        // Register copy Command
        bufferCopy.srcOffset = offset;
        bufferCopy.size = sizeInBytes;
        vkCmdCopyBuffer(vkCommandBuffer, stagingGeometriesDataBuffer.getVkBuffer(),
            rtObjDesc.get().getGeometryDataDeviceBuffer().getVkBuffer(), 1, &bufferCopy);

        // Increment the offset
        offset += sizeInBytes;
    }

    // Barrier specification
    VkDependencyInfo rtObjDescsdependencyInfo = {};
    rtObjDescsdependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    rtObjDescsdependencyInfo.dependencyFlags = 0x00000000;
    rtObjDescsdependencyInfo.memoryBarrierCount = 0;
    rtObjDescsdependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    rtObjDescsdependencyInfo.bufferMemoryBarrierCount = rtObjDescBufferMemoryBarrier2s.size();
    rtObjDescsdependencyInfo.pBufferMemoryBarriers = rtObjDescBufferMemoryBarrier2s.data();
    rtObjDescsdependencyInfo.imageMemoryBarrierCount = 0;
    rtObjDescsdependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    vkCmdSetEvent2(vkCommandBuffer, eventToSignal, &rtObjDescsdependencyInfo);

    //End command buffer
    if (commandBuffer.get().endVkCommandBuffer() == false ||
        commandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        threadData.commandPool.freeCommandBuffer(commandBuffer.get());
        OV_LOG_ERROR(R"(Fail to end or set status of secondary command buffer to upload rtObjDescs
 in rtscene with id= %d.)", getId());
        return false;
    }

    threadData.commandBuffers.push_back(commandBuffer.get());

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to records command buffer to build the BLASs using a differente thread.
 * @param threadData The data used by the thread processing the BLASs.
 * @param blassToBuild List of BLASs reference to build.
 * @param stagingTransformsDataBuffer The staging OvVK buffer that will contains the BLASs data to load. 
 * @param pvkCmdBuildAccelerationStructuresKHR Function pointer to the Vulkan command to build BLASs.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::buildBLASs(
    OvVK::RTScene::ThreadData& threadData,
    std::list<std::reference_wrapper<OvVK::BottomLevelAccelerationStructure>>& blassToBuild,
    OvVK::Buffer& stagingTransformsDataBuffer,
    PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR,
    VkEvent eventToWait, std::vector<VkBufferMemoryBarrier2>& rtObjDescBufferMemoryBarrier2s,
    VkEvent eventToSignal, std::vector<VkBufferMemoryBarrier2>& blassBufferMemoryBarrier2s)
{
    // Create command pool (compute command pool has transfer property)
    if (threadData.commandPool.create(OvVK::CommandPool::CommandTypesFlagBits::Compute) == false)
    {
        OV_LOG_ERROR("Fail to create geometry command pool for thread in rtscene with id= %d.", getId());
        return false;
    }

    // Structure defines any state that will be inherited from the primary command buffer
    VkCommandBufferInheritanceInfo commandBufferInheritanceInfo = {};
    commandBufferInheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    commandBufferInheritanceInfo.pNext = NULL;
    commandBufferInheritanceInfo.renderPass = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.subpass = 0;
    commandBufferInheritanceInfo.framebuffer = VK_NULL_HANDLE;
    // occlusionQueryEnable specifies whether the command buffer can be executed while an occlusion query
    // is active in the primary command buffer. If this is VK_TRUE, then this command buffer can be executed
    // whether the primary command buffer has an occlusion query active or not. If this is VK_FALSE, 
    // then the primary command buffer must not have an occlusion query active.
    // Occlusion queries are only available on queue families supporting graphics operations.
    commandBufferInheritanceInfo.occlusionQueryEnable = VK_FALSE;
    commandBufferInheritanceInfo.queryFlags = 0x00000000;
    commandBufferInheritanceInfo.pipelineStatistics = 0x00000000;

    // Begin CommandBuffer info
    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    commandBufferBeginInfo.pInheritanceInfo = &commandBufferInheritanceInfo;

    // Allocate commandBuffer
    std::reference_wrapper<OvVK::CommandBuffer> commandBuffer =
        threadData.commandPool.allocateCommandBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY);

    if (commandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        commandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        threadData.commandPool.freeCommandBuffer(commandBuffer.get());
        OV_LOG_ERROR("Fail to begin or set the status of the commandBuffer with id= %d to build BLAS in the rtscene with id= %d",
            commandBuffer.get().getId(), getId());
        return false;
    }

    // Get commandBuffer
    VkCommandBuffer vkCommandBuffer = commandBuffer.get().getVkCommandBuffer();


    // pointer to staging buffer memeory
    uint8_t* currentPosition = ((uint8_t*)stagingTransformsDataBuffer.getVmaAllocationInfo().pMappedData);
    // Offset to ready from staging buffer
    uint64_t offset = 0;
    // Size of a transform matrix
    size_t sizeTransformMatrix = sizeof(VkTransformMatrixKHR);
    // Copy info
    VkBufferCopy bufferCopy = {};
    bufferCopy.dstOffset = 0;

    // Define Barrier to wait the transfer is finish
    VkBufferMemoryBarrier2 memoryBarrier;
    // Device transformMatrix data Buffer
    memoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    memoryBarrier.pNext = VK_NULL_HANDLE;
    // Need to wait previous stage 
    memoryBarrier.srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // Need to wait on specific memory access.
    memoryBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
        VK_ACCESS_2_TRANSFER_READ_BIT;
    // All transfer commands need to wait that the texture change layout.
    memoryBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
    // All write commands need to wait that the texture change layout.
    memoryBarrier.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
        VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    // Need to transfer ownership
    memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    memoryBarrier.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    memoryBarrier.size = VK_WHOLE_SIZE;

    // Barrier specification
    VkDependencyInfo dependencyInfo = {};
    dependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    dependencyInfo.dependencyFlags = 0x00000000;
    dependencyInfo.memoryBarrierCount = 0;
    dependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    dependencyInfo.bufferMemoryBarrierCount = 1;
    dependencyInfo.pBufferMemoryBarriers = &memoryBarrier;
    dependencyInfo.imageMemoryBarrierCount = 0;
    dependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;


    // Barrier specification
    VkDependencyInfo rtObjDescsdependencyInfo = {};
    rtObjDescsdependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    rtObjDescsdependencyInfo.dependencyFlags = 0x00000000;
    rtObjDescsdependencyInfo.memoryBarrierCount = 0;
    rtObjDescsdependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    rtObjDescsdependencyInfo.bufferMemoryBarrierCount = rtObjDescBufferMemoryBarrier2s.size();
    rtObjDescsdependencyInfo.pBufferMemoryBarriers = rtObjDescBufferMemoryBarrier2s.data();
    rtObjDescsdependencyInfo.imageMemoryBarrierCount = 0;
    rtObjDescsdependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    vkCmdWaitEvents2(vkCommandBuffer, 1, &eventToWait, &rtObjDescsdependencyInfo);


    // Record commandBuffer for all the geometries
    for (std::reference_wrapper<OvVK::BottomLevelAccelerationStructure> blas : blassToBuild)
    {
        // Copy Geometries TransformMatrix
        std::vector<std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData>> geometries
            = blas.get().getGeometriesData();

        for (uint32_t c = 0; c < geometries.size(); c++)
        {
            // Copy data on staging buffer
            memcpy(currentPosition, &geometries[c].get().transformsMatrix, sizeTransformMatrix);
            // Move pointer to the memory where copy the data
            currentPosition += sizeTransformMatrix;
        }

        // Register copy Command
        bufferCopy.srcOffset = offset;
        bufferCopy.size = blas.get().getNrOfGeometries() * sizeTransformMatrix;
        vkCmdCopyBuffer(vkCommandBuffer, stagingTransformsDataBuffer.getVkBuffer(),
            blas.get().getTransformsDataBuffer().getVkBuffer(), 1, &bufferCopy);

        // Increment the offset
        offset += bufferCopy.size;

        // set target buffer
        memoryBarrier.buffer = blas.get().getTransformsDataBuffer().getVkBuffer();

        // Register command for transition.
        vkCmdPipelineBarrier2(vkCommandBuffer, &dependencyInfo);

        // Record command to build the BLAS
        const VkAccelerationStructureBuildGeometryInfoKHR& accelerationStructureBuildGeometryInfoKHR =
            blas.get().getAccelerationStructureBuildGeometryInfo();

        const VkAccelerationStructureBuildRangeInfoKHR* accelerationStructureBuildRangeInfo =
            blas.get().getAccelerationStructureBuildRangeInfosKHR().data();
        const VkAccelerationStructureBuildRangeInfoKHR** accelerationStructureBuildRangeInfos =
            &accelerationStructureBuildRangeInfo;
        // Build an acceleration structure
        pvkCmdBuildAccelerationStructuresKHR(vkCommandBuffer, 1,
            &accelerationStructureBuildGeometryInfoKHR,
            accelerationStructureBuildRangeInfos);
    }


    // Barrier specification
    VkDependencyInfo asDependencyInfo = {};
    asDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    asDependencyInfo.dependencyFlags = 0x00000000;
    asDependencyInfo.memoryBarrierCount = 0;
    asDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    asDependencyInfo.bufferMemoryBarrierCount = blassBufferMemoryBarrier2s.size();
    asDependencyInfo.pBufferMemoryBarriers = blassBufferMemoryBarrier2s.data();
    asDependencyInfo.imageMemoryBarrierCount = 0;
    asDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register barrier for ownership transfer
    vkCmdSetEvent2(vkCommandBuffer, eventToSignal, &asDependencyInfo);

    //End command buffer
    if (commandBuffer.get().endVkCommandBuffer() == false ||
        commandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        threadData.commandPool.freeCommandBuffer(commandBuffer.get());
        OV_LOG_ERROR("Fail to end or set the status of the commandBuffer with id= %d to build BLAS in rtscene with id= %d",
            commandBuffer.get().getId(), getId());
        return false;
    }

    threadData.commandBuffers.push_back(commandBuffer.get());

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to records command buffer to build the TLASs using a differente thread.
 * @param threadData The data used by the thread processing the TLASs.
 * @param tlassToBuild List of TLASs reference to build.
 * @param stagingTransformsDataBuffer The staging OvVK buffer that will contains the TLASs data to load.
 * @param pvkCmdBuildAccelerationStructuresKHR Function pointer to the Vulkan command to build TLASs.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::buildTLASs(
    OvVK::RTScene::ThreadData& threadData,
    std::list<std::reference_wrapper<OvVK::TopLevelAccelerationStructure>>& tlassToBuild,
    OvVK::Buffer& stagingASInstanceBuffer,
    PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR,
    VkEvent eventToWait, std::vector<VkBufferMemoryBarrier2>& blassBufferMemoryBarrier2s,
    VkEvent eventToSignal, VkBufferMemoryBarrier2& tlasBufferMemoryBarrier2)
{
    // Create command pool (compute command pool has transfer property)
    if (threadData.commandPool.create(OvVK::CommandPool::CommandTypesFlagBits::Compute) == false)
    {
        OV_LOG_ERROR("Fail to create geometry commandPool for thread in rtscene with id= %d.", getId());
        return false;
    }

    // Structure defines any state that will be inherited from the primary command buffer
    VkCommandBufferInheritanceInfo commandBufferInheritanceInfo = {};
    commandBufferInheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    commandBufferInheritanceInfo.pNext = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.renderPass = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.subpass = 0;
    commandBufferInheritanceInfo.framebuffer = VK_NULL_HANDLE;
    // occlusionQueryEnable specifies whether the command buffer can be executed while an occlusion query
    // is active in the primary command buffer. If this is VK_TRUE, then this command buffer can be executed
    // whether the primary command buffer has an occlusion query active or not. If this is VK_FALSE, 
    // then the primary command buffer must not have an occlusion query active.
    // Occlusion queries are only available on queue families supporting graphics operations.
    commandBufferInheritanceInfo.occlusionQueryEnable = VK_FALSE;
    commandBufferInheritanceInfo.queryFlags = 0x00000000;
    commandBufferInheritanceInfo.pipelineStatistics = 0x00000000;

    // Begin CommandBuffer info
    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    commandBufferBeginInfo.pInheritanceInfo = &commandBufferInheritanceInfo;

    // Allocate commandBuffer
    std::reference_wrapper<OvVK::CommandBuffer> commandBuffer =
        threadData.commandPool.allocateCommandBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY);

    if (commandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        commandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        threadData.commandPool.freeCommandBuffer(commandBuffer.get());
        OV_LOG_ERROR("Fail to begin or set the status of the commandBuffer with id= %d to build TLAS in rtscene with id= %d",
            commandBuffer.get().getId(), getId());
        return false;
    }

    // Get commandBuffer
    VkCommandBuffer vkCommandBuffer = commandBuffer.get().getVkCommandBuffer();


    // Copy info
    VkBufferCopy bufferCopy = {};
    bufferCopy.dstOffset = 0;
    // Register copy Command
    bufferCopy.srcOffset = 0;
    bufferCopy.size = stagingASInstanceBuffer.getSize();


    // Define Barrier to wait the transfer is finish
    VkBufferMemoryBarrier2 memoryBarrier = {};
    // Device transformMatrix data Buffer
    memoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    memoryBarrier.pNext = VK_NULL_HANDLE;
    // Need to wait previous stage 
    memoryBarrier.srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // Need to wait on specific memory access.
    memoryBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
        VK_ACCESS_2_TRANSFER_READ_BIT;
    // All AS commands need to wait that the texture change layout.
    memoryBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
    // All write commands need to wait that the texture change layout.
    memoryBarrier.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
        VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    // Need to transfer ownership
    memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    memoryBarrier.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    memoryBarrier.size = VK_WHOLE_SIZE;

    // Barrier specification
    VkDependencyInfo dependencyInfo = {};
    dependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    dependencyInfo.dependencyFlags = 0x00000000;
    dependencyInfo.memoryBarrierCount = 0;
    dependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    dependencyInfo.bufferMemoryBarrierCount = 1;
    dependencyInfo.pBufferMemoryBarriers = &memoryBarrier;
    dependencyInfo.imageMemoryBarrierCount = 0;
    dependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;


    // Barrier specification
    VkDependencyInfo blasesDependencyInfo = {};
    blasesDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    blasesDependencyInfo.dependencyFlags = 0x00000000;
    blasesDependencyInfo.memoryBarrierCount = 0;
    blasesDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    blasesDependencyInfo.bufferMemoryBarrierCount = blassBufferMemoryBarrier2s.size();
    blasesDependencyInfo.pBufferMemoryBarriers = blassBufferMemoryBarrier2s.data();
    blasesDependencyInfo.imageMemoryBarrierCount = 0;
    blasesDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register barrier for ownership transfer
    vkCmdWaitEvents2(vkCommandBuffer, 1, &eventToWait, &blasesDependencyInfo);

    // Record commandBuffer for all the geometries
    for (std::reference_wrapper<OvVK::TopLevelAccelerationStructure> tlas : tlassToBuild)
    {
        // Copy staging buffer
        vkCmdCopyBuffer(vkCommandBuffer, stagingASInstanceBuffer.getVkBuffer(),
            tlas.get().getDeviceASInstancesBuffer().getVkBuffer(), 1, &bufferCopy);

        // Set target buffer
        memoryBarrier.buffer = tlas.get().getDeviceASInstancesBuffer().getVkBuffer();

        // Register command for transition.
        vkCmdPipelineBarrier2(vkCommandBuffer, &dependencyInfo);

        // Build TLAS
        const VkAccelerationStructureBuildGeometryInfoKHR& accelerationStructureBuildGeometryInfoKHR
            = tlas.get().getAccelerationStructureBuildGeometryInfo();

        const VkAccelerationStructureBuildRangeInfoKHR* accelerationStructureBuildRangeInfo =
            &tlas.get().getAccelerationStructureBuildRangeInfosKHR().value().get();
        const VkAccelerationStructureBuildRangeInfoKHR** accelerationStructureBuildRangeInfos =
            &accelerationStructureBuildRangeInfo;

        // Build an acceleration structure
        PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR =
            (PFN_vkCmdBuildAccelerationStructuresKHR)vkGetInstanceProcAddr(
                Engine::getInstance().getVulkanInstance().getVkInstance(), "vkCmdBuildAccelerationStructuresKHR");

        pvkCmdBuildAccelerationStructuresKHR(vkCommandBuffer, 1,
            &accelerationStructureBuildGeometryInfoKHR,
            accelerationStructureBuildRangeInfos);
    }

    // Barrier specification
    VkDependencyInfo asDependencyInfo = {};
    asDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    asDependencyInfo.dependencyFlags = 0x00000000;
    asDependencyInfo.memoryBarrierCount = 0;
    asDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    asDependencyInfo.bufferMemoryBarrierCount = 1;
    asDependencyInfo.pBufferMemoryBarriers = &tlasBufferMemoryBarrier2;
    asDependencyInfo.imageMemoryBarrierCount = 0;
    asDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register barrier for ownership transfer
    vkCmdSetEvent2(vkCommandBuffer, eventToSignal, &asDependencyInfo);


    //End command buffer
    if (commandBuffer.get().endVkCommandBuffer() == false ||
        commandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        threadData.commandPool.freeCommandBuffer(commandBuffer.get());
        OV_LOG_ERROR("Fail to end or set the status of the commandBuffer with id= %d to build TLAS in rtscene with id= %d",
            commandBuffer.get().getId(), getId());
        return false;
    }

    threadData.commandBuffers.push_back(commandBuffer.get());

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the TLAS of the current frame in flight.
 * @return OvVK TLAS
 */
const OvVK::TopLevelAccelerationStructure OVVK_API& OvVK::RTScene::getTLAS() const
{
    return reserved->allTlas[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns all the renderable elements.
 * @return Vector containing all the renderable elements.
 */
const std::vector<OvVK::RTScene::RenderableElem> OVVK_API& OvVK::RTScene::getRenderableElems() const
{
    return reserved->renderableElements[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the renderable element in the passed position.
 * @param elemNr The position of the renderable element.
 * @return The renderable element in the passed position.
 */
const OvVK::RTScene::RenderableElem OVVK_API& OvVK::RTScene::getRenderableElem(uint32_t elemNr) const
{
    if (elemNr >= reserved->renderableElements[OvVK::Engine::getInstance().getFrameInFlight()].size())
    {
        OV_LOG_ERROR("The passed elemNr is out of range in rtscene with id= %d.", getId());
        return OvVK::RTScene::RenderableElem();
    }

    return reserved->renderableElements[OvVK::Engine::getInstance().getFrameInFlight()].at(elemNr);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the number of renderable elements.
 * @return The number of renderable elements.
 */
uint32_t OVVK_API OvVK::RTScene::getNrOfRenderableElems() const
{
    return reserved->renderableElements[OvVK::Engine::getInstance().getFrameInFlight()].size();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the number of lights.
 * @return The number of lights.
 */
uint32_t OVVK_API OvVK::RTScene::getNrOfLights() const
{
    return reserved->nrOfLights[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the number shader groups needed.
 * @return The number shader groups needed.
 */
uint32_t OVVK_API OvVK::RTScene::getNrOfHitShaderGroupsNeeded() const
{
    return reserved->nrOfHitShaderGroupsNeeded[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the geometries instance in the TLAS pf the current frame-in-flight are using the same shaders groups.
 * @return TF.
 */
bool OVVK_API OvVK::RTScene::getAreGeometriesSharingSameShadersGroups() const
{
    return reserved->areGeometriesSharingSameShadersGroups[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returts the pass GPU sync obj.
 * @return The pass GPU sync obj.
 */
VkSemaphore OVVK_API OvVK::RTScene::getPassStatusSyncObjGPU() const
{
    return reserved->passStatusSyncObjGPU[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returts the pass CPU sync obj.
 * @return The pass CPU sync obj.
 */
VkFence OVVK_API OvVK::RTScene::getPassStatusSyncObjCPU() const 
{
    return reserved->passStatusSyncObjCPU[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returts the render GPU sync obj.
 * @return The render GPU sync obj.
 */
VkSemaphore OVVK_API OvVK::RTScene::getRederingStatusSyncObjGPU() const 
{
    return reserved->rederingStatusSyncObjGPU[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returts the render CPU sync obj.
 * @return The render CPU sync obj.
 */
VkFence OVVK_API OvVK::RTScene::getRederingStatusSyncObjCPU() const 
{
    return reserved->rederingStatusSyncObjCPU[OvVK::Engine::getInstance().getFrameInFlight()];
}


VkEvent OVVK_API OvVK::RTScene::getTlasBuildedEvent() const {
    return reserved->tlasStatusSyncObjGPU[Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the rendering GPU sync obj.
 * @param value The uint value rapresenting the status the GPU sync Obj is in.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::setRederingStatusSyncObjGPUValue(uint64_t value)
{
    // Logicel device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();
    // Frame in flight
    uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

    // Safety net:
    if (reserved->rederingStatusSyncObjGPU[frameInFlight] == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("GPU render sync obj not created in rtscene with id= %d!", getId());
        return false;
    }


    // Retrievers current sempahore value
    uint64_t currentValue;
    vkGetSemaphoreCounterValue(logicalDevice, reserved->rederingStatusSyncObjGPU[frameInFlight], &currentValue);

    if (currentValue > value) {
        // Destroy semaphore because can't decrease the value
        vkDestroySemaphore(logicalDevice, reserved->rederingStatusSyncObjGPU[frameInFlight], nullptr);
        reserved->rederingStatusSyncObjGPU[frameInFlight] = VK_NULL_HANDLE;

        // Create new timeline semaphore and delete previous one.
        VkSemaphoreTypeCreateInfo timelineCreateInfo;
        timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        timelineCreateInfo.pNext = NULL;
        timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        timelineCreateInfo.initialValue = value;

        VkSemaphoreCreateInfo semaphoreInfo;
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreInfo.pNext = &timelineCreateInfo;
        semaphoreInfo.flags = 0;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr,
            &reserved->rederingStatusSyncObjGPU[frameInFlight]) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status GPU render synchronization object for rtscene with id= %d", getId());
            return false;
        }
    }
    else if (currentValue < value) {
        VkSemaphoreSignalInfo signalInfo;
        signalInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
        signalInfo.pNext = NULL;
        signalInfo.semaphore = reserved->rederingStatusSyncObjGPU[frameInFlight];
        signalInfo.value = value;

        if (vkSignalSemaphore(logicalDevice, &signalInfo) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status GPU render synchronization object for rtscene with id= %d", getId());
            return false;
        }
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the pass GPU sync obj.
 * @param value The uint value rapresenting the status the GPU sync Obj is in.
 * @return TF
 */
bool OVVK_API OvVK::RTScene::setPassStatusSyncObjGPUValue(uint64_t value)
{
    // Logicel device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();
    // Frame in flight
    uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

    // Safety net:
    if (reserved->passStatusSyncObjGPU[frameInFlight] == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("GPU pass sync obj not created in rtscene with id= %d!", getId());
        return false;
    }


    // Retrievers current sempahore value
    uint64_t currentValue;
    vkGetSemaphoreCounterValue(logicalDevice, reserved->passStatusSyncObjGPU[frameInFlight], &currentValue);

    if (currentValue > value) {
        // Destroy semaphore because can't decrease the value
        vkDestroySemaphore(logicalDevice, reserved->passStatusSyncObjGPU[frameInFlight], nullptr);
        reserved->passStatusSyncObjGPU[frameInFlight] = VK_NULL_HANDLE;

        // Create new timeline semaphore and delete previous one.
        VkSemaphoreTypeCreateInfo timelineCreateInfo;
        timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        timelineCreateInfo.pNext = NULL;
        timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        timelineCreateInfo.initialValue = value;

        VkSemaphoreCreateInfo semaphoreInfo;
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreInfo.pNext = &timelineCreateInfo;
        semaphoreInfo.flags = 0;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr,
            &reserved->passStatusSyncObjGPU[frameInFlight]) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status GPU pass synchronization object for rtscene with id= %d", getId());
            return false;
        }
    }
    else if (currentValue < value) {
        VkSemaphoreSignalInfo signalInfo;
        signalInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
        signalInfo.pNext = NULL;
        signalInfo.semaphore = reserved->passStatusSyncObjGPU[frameInFlight];
        signalInfo.value = value;

        if (vkSignalSemaphore(logicalDevice, &signalInfo) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status GPU pass synchronization object for rtscene with id= %d", getId());
            return false;
        }
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the buffer containing the data of the picking.
 * @return The buffer containing the data of the picking.
 */
OvVK::Buffer OVVK_API& OvVK::RTScene::getPickingDataBuffer() const 
{
    return reserved->pickedDataBuffer;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method. Object data in the RTScene is stored in world coordinates: if other spaces are needed, compute them
 * directly in the shader. All the data to do that is provided.
 * @param cameraMatrix camera matrix to use
 * @param projectionMatrix projection matrix to use
 * @return TF
 */
bool OVVK_API OvVK::RTScene::render(const glm::mat4& cameraMatrix, const glm::mat4& projectionMatrix)
{
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Storage
    std::reference_wrapper<OvVK::Storage> storage = engine.get().getStorage();
    // Swapchain
    std::reference_wrapper<OvVK::Swapchain> swapchain = engine.get().getSwapchain();
    // Logical Device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

    // Wait CPU render sync object
    // Wait fences to know the work is finished
    if (reserved->rederingStatusSyncObjCPU[frameInFlight] != VK_NULL_HANDLE &&
        vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &reserved->rederingStatusSyncObjCPU[frameInFlight],
            VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait render Fence in rtscene with id= %d.", getId());
        return false;
    }


    ////////////////////
    // Update Storage //
    ////////////////////

    // Array with instances nr.:
    uint32_t curInstance[magic_enum::enum_integer(OvVK::Storage::StoredType::all)];
    for (uint32_t c = 0; c < magic_enum::enum_integer(OvVK::Storage::StoredType::all); c++)
        curInstance[c] = 0;

    // Update Lights and RTObjDesc
    for (auto& re : reserved->renderableElements[frameInFlight])
    {
        // Lights:      
        if (typeid(re.renderable.get()) == typeid(OvVK::Light) && re.visibleFlag)
        {
            const uint32_t type = static_cast<uint32_t>(magic_enum::enum_integer(
                OvVK::Storage::StoredType::lights));

            OvVK::Light& light = dynamic_cast<OvVK::Light&>(re.renderable.get());
            light.render();

            // Get light from storage
            OvVK::Storage::LightData* lightInstance = storage.get().getLightData(curInstance[type]);

            // Fill light data:
            lightInstance->position = re.matrix[3]; // In world coordinates 
            lightInstance->color = light.getColor();
            lightInstance->direction = glm::vec4(light.getDirection(), 1.0f) * re.matrix;
            lightInstance->type = magic_enum::enum_integer(light.getLightType());
            lightInstance->power = light.getPower();
            lightInstance->spotCutOff = light.getSpotCutoff();
            lightInstance->spotExponent = light.getSpotExponent();

            // Next:
            curInstance[type]++;
            continue;
        }

        // RTObjDesc:
        if (typeid(re.renderable.get()) == typeid(OvVK::RTObjDesc) && re.visibleFlag)
        {
            const uint32_t type = static_cast<uint32_t>(magic_enum::enum_integer(
                OvVK::Storage::StoredType::rtObjDescs));

            // RTObjDesc:
            std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc =
                dynamic_cast<OvVK::RTObjDesc&>(re.renderable.get());
            rtObjDesc.get().render();

            // Get RTObjDesc from storage
            OvVK::Storage::RTObjDescData* rtObjDescData = storage.get().getRTObjDescData(curInstance[type]);
            // Fill RTObjDescData
            {
                std::optional<VkDeviceAddress> deviceAddress =
                    rtObjDesc.get().getGeometryDataDeviceBuffer().getBufferDeviceAddress();
                if (deviceAddress.has_value() == false)
                {
                    OV_LOG_ERROR("Fail to retriever device address of the rtObjDesc with id= %d", rtObjDesc.get().getId());
                    return false;
                }

                rtObjDescData->geometryDataAddress = static_cast<uint64_t>(deviceAddress.value());
            }

            // Next:
            curInstance[type]++;
            continue;
        }
    }

    storage.get().setNrOfInstances(curInstance[static_cast<uint32_t>(OvVK::Storage::StoredType::lights)],
        OvVK::Storage::StoredType::lights);

    storage.get().setNrOfInstances(curInstance[static_cast<uint32_t>(OvVK::Storage::StoredType::rtObjDescs)],
        OvVK::Storage::StoredType::rtObjDescs);

    // Update uniform
    glm::mat4 viewMatrix = glm::inverse(cameraMatrix);
    glm::mat4 invProjMatrix = glm::inverse(projectionMatrix);

    OvVK::Storage::GlobalUniformData* globalUniform = storage.get().getGlobalUniformData();
    globalUniform->viewProj = viewMatrix * projectionMatrix;
    globalUniform->viewInverse = cameraMatrix;
    globalUniform->projInverse = invProjMatrix;
    globalUniform->nrOfLights = curInstance[static_cast<uint32_t>(OvVK::Storage::StoredType::lights)];

    // Set timeline semaphore so that if necessary recreate it.
    setRederingStatusSyncObjGPUValue(OvVK::RTScene::startValueRederingStatusSyncObj);

    // Render the updated storage.
    storage.get().render();

    // Reset sync obj CPU.
    if (reserved->rederingStatusSyncObjCPU[frameInFlight] != VK_NULL_HANDLE &&
        vkResetFences(logicalDevice.get().getVkDevice(), 1,
            &reserved->rederingStatusSyncObjCPU[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to reset the render fence of the frame in flight number %d of the rtscene with id= %d.",
            frameInFlight, getId());
        return false;
    }

    // Upload data from host buffers to device buffers
    return storage.get().upload(reserved->rederingStatusSyncObjCPU[frameInFlight],
        { reserved->rederingStatusSyncObjGPU[frameInFlight], reserved->passStatusSyncObjGPU[frameInFlight] },
        { OvVK::RTScene::startValueRederingStatusSyncObj, OvVK::RTScene::finishValuePassStatusSyncObj },
        { reserved->rederingStatusSyncObjGPU[frameInFlight] },
        { OvVK::RTScene::finishValueRederingStatusSyncObj });
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method to draw a gui on a windows with the ImGUI library.
 */
void OVVK_API OvVK::RTScene::drawGUI()
{
    // Frame in flight
    uint32_t frameInFlight = OvVK::Engine::getInstance().getFrameInFlight();

    if (!ImGui::CollapsingHeader("RTScene"))
        return;

    // Lights
    if (ImGui::TreeNode("Lights") && reserved->nrOfLights[frameInFlight] > 0)
    {
        // Get TargetLight Light
        // Light are all hat the beginning
        std::reference_wrapper<OvVK::RTScene::RenderableElem> re = 
            reserved->renderableElements[frameInFlight][reserved->targetLight];
        std::reference_wrapper<OvVK::Light> light = dynamic_cast<OvVK::Light&>(re.get().renderable.get());

        // Switch Light
        ImGui::Text("Switch light");

        if (ImGui::ArrowButton("arrowbtn01RT", 0))
        {
            if (reserved->targetLight > 0)
                reserved->targetLight -= 1;
            else
                reserved->targetLight = reserved->nrOfLights[frameInFlight] - 1;
        }

        ImGui::SameLine();

        ImGui::Text("%d", reserved->targetLight);

        ImGui::SameLine();

        if (ImGui::ArrowButton("arrowbtn02RT", 1))
        {
            reserved->targetLight += 1;
            reserved->targetLight %= reserved->nrOfLights[frameInFlight];
        }

        ImGui::NewLine();

        // Target light
        ImGui::Text("Target light: Name= %s ,Id= %d", light.get().getName(), light.get().getId());

        // Set visible or not
        ImGui::Checkbox("Enable/Disable", &re.get().reference.get().getRenderable(0).visibleFlag);
        re.get().visibleFlag = re.get().reference.get().getRenderable(0).visibleFlag;

        // Possibility to add position and rotation

        // light properties
        light.get().drawGUI();

        ImGui::TreePop();
    }

    OvVK::PickingPipeline::PickingData* pickingData = 
        reinterpret_cast<OvVK::PickingPipeline::PickingData*>(
        ((uint8_t*)reserved->pickedDataBuffer.getVmaAllocationInfo().pMappedData));

    if (pickingData->pickedSomething == 1)
    {
        std::reference_wrapper<OvVK::Geometry> geometry = OvVK::Geometry::empty;
        std::reference_wrapper<OvVK::Material> material = OvVK::Material::empty;

        // Get material and geometry
        for (uint32_t c = reserved->nrOfLights[frameInFlight]; c < reserved->renderableElements[frameInFlight].size(); c++)
        {
            if (reserved->renderableElements[frameInFlight][c].renderable.get().getId() == pickingData->rtObjDescId)
            {
                // Get RTObjDesc
                std::reference_wrapper<OvVK::RTObjDesc> rtObjDesc =
                    dynamic_cast<OvVK::RTObjDesc&>(reserved->renderableElements[frameInFlight][c].renderable.get());

                // Get material and geometry
                for (std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData> geometryData
                    : rtObjDesc.get().getBLAS().getGeometriesData())
                {
                    if (geometryData.get().geometry.get().getId() == pickingData->geometryId)
                    {
                        geometry = geometryData.get().geometry;
                        material = rtObjDesc.get().getGeometryMaterial(geometry);

                        break;
                    }
                }
                break;
            }
        }

        // Draw Geometry
        if (geometry.get() != OvVK::Geometry::empty && ImGui::TreeNode("Geometry")) {

            // Picked Geometry
            ImGui::Text("Geometry: Name= %s ,Id= %d", geometry.get().getName(), geometry.get().getId());


            // Draw Material
            if (material.get() != OvVK::Material::empty &&
                material.get().getId() != OvVK::Material::getDefault().getId() &&
                ImGui::TreeNode("Material"))
            {

                // Target Material
                ImGui::Text("Material: Name= %s ,Id= %d", material.get().getName(), material.get().getId());

                material.get().drawGUI();

                ImGui::TreePop();
            }


            ImGui::TreePop();
        }
    }
}

#pragma endregion