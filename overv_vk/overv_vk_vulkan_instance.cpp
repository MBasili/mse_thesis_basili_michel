/**
 * @file	overv_vk_instance.cpp
 * @brief	Vulkan instance properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"

// C/C++
#include <unordered_set>


////////////
// STATIC //
////////////

// Special values
OvVK::VulkanInstance OvVK::VulkanInstance::empty("empty");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Instance class reserved structure.
 */
struct OvVK::VulkanInstance::Reserved
{
    VkInstance vulkanInstance;                                      ///< Opaque handle to an instance object
    mutable std::vector<OvVK::PhysicalDevice> physicalDevices;  ///< Vector of all physical devices releated to the instance. 

    VkApplicationInfo appInfo;                                      ///< Structure specifying application information
    std::vector<const char*> enabledExtensions;                     ///< Vector containing enabled extensions.
    std::vector<const char*> enabledValidationLayers;               ///< Vector containing enanled validation layers.

    VkDebugUtilsMessengerEXT debugMessenger;                        ///< Opaque handle to a debug messenger object.

    /**
     * Constructor.
     */
    Reserved() : vulkanInstance { VK_NULL_HANDLE },
        appInfo{ {} },
        debugMessenger{ VK_NULL_HANDLE }
    {}
};


////////////////////////////
// BODY OF CLASS Instance //
////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::VulkanInstance::VulkanInstance() :
    reserved(std::make_unique<OvVK::VulkanInstance::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The instance name.
 */
OVVK_API OvVK::VulkanInstance::VulkanInstance(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::VulkanInstance::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::VulkanInstance::VulkanInstance(VulkanInstance&& other) : Ov::Object(std::move(other)),
    Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::VulkanInstance::~VulkanInstance()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes instance
 * @return TF
 */
bool OVVK_API OvVK::VulkanInstance::init()
{
    // Safety net:
    if (this->Ov::Managed::init() == false)
        return false;

    // Debug messanger should be destroyed
    if (reserved->debugMessenger != VK_NULL_HANDLE) 
    {
        PFN_vkDestroyDebugUtilsMessengerEXT pvkDestroyDebugUtilsMessengerEXT = 
            (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(reserved->vulkanInstance, "vkDestroyDebugUtilsMessengerEXT");

        pvkDestroyDebugUtilsMessengerEXT(reserved->vulkanInstance, reserved->debugMessenger, nullptr);
        reserved->debugMessenger = VK_NULL_HANDLE;
    }

    // Instance should be destroyed
    if (reserved->vulkanInstance != VK_NULL_HANDLE)
    {
        vkDestroyInstance(reserved->vulkanInstance, nullptr);
        reserved->vulkanInstance = VK_NULL_HANDLE;
    }

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases instance
 * @return TF
 */
bool OVVK_API OvVK::VulkanInstance::free()
{
    // Safety net:
    if (this->Ov::Managed::free() == false)
        return false;

    // Debug messanger should be destroyed
    if (reserved->debugMessenger != VK_NULL_HANDLE)
    {
        PFN_vkDestroyDebugUtilsMessengerEXT pvkDestroyDebugUtilsMessengerEXT =
            (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(reserved->vulkanInstance, "vkDestroyDebugUtilsMessengerEXT");

        pvkDestroyDebugUtilsMessengerEXT(reserved->vulkanInstance, reserved->debugMessenger, nullptr);
        reserved->debugMessenger = VK_NULL_HANDLE;
    }

    // Instance should be destroyed
    if (reserved->vulkanInstance != VK_NULL_HANDLE)
    {
        vkDestroyInstance(reserved->vulkanInstance, nullptr);
        reserved->vulkanInstance = VK_NULL_HANDLE;
    }

    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates a Vulkan instance.
 * @param appInfo Structure specifying application information.
 * @param requiredExtensions Vector of extension names to be enabled for the instance.
 * @param enableDebugMessanger Enable the debug messanger (LOG system).
 * @param enableValidationLayers Enable the validation layers.
 * @param requiredValidationLayers Vector of layers names to be enabled for the instance.
 * @return TF
 */
bool OVVK_API OvVK::VulkanInstance::create(VkApplicationInfo appInfo, std::vector<const char*> requiredExtensions,
    bool enableDebugMessanger, bool enableValidationLayers, std::vector<const char*> requiredValidationLayers)
{
    // Vulkan instance info
    reserved->appInfo = appInfo;

    // Check if the required extensions are supported
    if (requiredExtensions.size() > 0 && !areExtensionsSupported(requiredExtensions)) {
        OV_LOG_ERROR("Extensions requested, but not supported!");
        return false;
    }
    reserved->enabledExtensions = requiredExtensions;

    // Enable validation layers if enable
    if (enableValidationLayers && !areLayersSupported(requiredValidationLayers)) {
        OV_LOG_ERROR("Validation layers requested, but not supported!");
        return false;
    }
    reserved->enabledValidationLayers = requiredValidationLayers;

    if (init() == false)
        return false;

    // This next struct is not optional and tells the Vulkan driver 
    // which global extensions and validation layers we want to use.
    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    // Include the validation layers names if enable.
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(reserved->enabledValidationLayers.size());
        createInfo.ppEnabledLayerNames = reserved->enabledValidationLayers.data();
    }
    else {
        createInfo.enabledLayerCount = 0;
        createInfo.ppEnabledLayerNames = VK_NULL_HANDLE;
    }

    // Debug Messanger info
    // structure specifying parameters of a newly created debug messenger
    VkDebugUtilsMessengerCreateInfoEXT dmCreateInfo = {};
    if (enableDebugMessanger) {
        // Creating an additional debug messenger this way it will automatically be used
        // during vkCreateInstance and vkDestroyInstance and cleaned up after that.
        populateDebugMessengerCreateInfo(dmCreateInfo);
        // The vkCreateDebugUtilsMessengerEXT call requires a valid instance to have been created and 
        // vkDestroyDebugUtilsMessengerEXT must be called before the instance is destroyed. 
        // This currently leaves us unable to debug any issues in the vkCreateInstance
        // and vkDestroyInstance calls.
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&dmCreateInfo;
        reserved->enabledExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }
    else 
    {
        createInfo.pNext = VK_NULL_HANDLE;
    }

    // Set the extensions to enable.
    createInfo.enabledExtensionCount = static_cast<uint32_t>(reserved->enabledExtensions.size());
    createInfo.ppEnabledExtensionNames = reserved->enabledExtensions.data();

    // Create Vulkan instace
    if (vkCreateInstance(&createInfo, nullptr, &reserved->vulkanInstance) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to initialize vulkan instance!");
        this->free();
        return false;
    }

    // Create a debug messenger
    if (enableDebugMessanger)
    {
        // Create a debug messenger object
        PFN_vkCreateDebugUtilsMessengerEXT pvkCreateDebugUtilsMessengerEXT =
            (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(reserved->vulkanInstance, "vkCreateDebugUtilsMessengerEXT");

        if (pvkCreateDebugUtilsMessengerEXT(reserved->vulkanInstance, &dmCreateInfo, nullptr,
            &reserved->debugMessenger) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to create debug messanger.");
            this->free();
            return false;
        }
    }

    return true; // Done init vulkan instance
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns Vulkan instance.
 * @return TF
 */
VkInstance OVVK_API OvVK::VulkanInstance::getVkInstance() const
{
    return reserved->vulkanInstance;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns structure specifying application information.
 * @return Structure containig application info.
 */
VkApplicationInfo OVVK_API OvVK::VulkanInstance::getVkApplicationInfo() const
{
    return reserved->appInfo;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns vector containing the names of the enable extensions.
 * @return Vector containing the names of the enable extensions.
 */
std::vector<const char*> OVVK_API OvVK::VulkanInstance::getEnabledExtensions() const
{
    return reserved->enabledExtensions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns vector containing the names of the enable validation layers.
 * @return Vector containing the names of the enable validation layers.
 */
std::vector<const char*> OVVK_API OvVK::VulkanInstance::getEnabledValidationLayers() const
{
    return reserved->enabledValidationLayers;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns opaque handle to the debug messenger object
 * @return Handle to the debug messenger object.
 */
VkDebugUtilsMessengerEXT OVVK_API OvVK::VulkanInstance::getDebugMessanger() const
{
    return reserved->debugMessenger;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns an optional std obj containing a vector of all the available OvVK::PhysicalDevice objs.
 * @return Optional std obj containing a vector collecting all the available OvVK::PhysicalDevice objs.
 */
std::vector<std::reference_wrapper<const OvVK::PhysicalDevice>> OVVK_API
    OvVK::VulkanInstance::getPhysicalDevices() const
{
    std::vector<std::reference_wrapper<const OvVK::PhysicalDevice>> physicalDevices;

    // Safety net:
    if (reserved->vulkanInstance == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No Vulkan instance found.");
        return physicalDevices;
    }

    // Picking physical device
    OV_LOG_PLAIN("Picking a physical device: ...");

    if (reserved->physicalDevices.size() <= 0) 
    {
        // Get number of physical device that support Vulkan
        uint32_t deviceCount = 0;
        if (vkEnumeratePhysicalDevices(reserved->vulkanInstance, &deviceCount, nullptr) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to retriver physical device!");
            return physicalDevices;
        }

        // If no device found
        if (deviceCount == 0) {
            OV_LOG_ERROR("Failed to find GPUs with Vulkan support!");
            return physicalDevices;
        }

        // Vector containing all available physical devices.
        std::vector<VkPhysicalDevice> vkPhysicalDevices(deviceCount);
        if (vkEnumeratePhysicalDevices(reserved->vulkanInstance, &deviceCount, vkPhysicalDevices.data()) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to retriver physical device!");
            return physicalDevices;
        }

        reserved->physicalDevices.resize(deviceCount);
        for (uint32_t c = 0; c < deviceCount; c++)
            if (reserved->physicalDevices[c].connect(vkPhysicalDevices[c]) == false) 
            {
                OV_LOG_ERROR("Fail to connect a vkPhysicalDevice to the OvVK::PhysicalDevice with id= %d.",
                    reserved->physicalDevices[c].getId());
                return physicalDevices;
            }
    }

    physicalDevices.resize(reserved->physicalDevices.size(), OvVK::PhysicalDevice::empty);
    for (uint32_t c = 0; c < reserved->physicalDevices.size(); c++)
        physicalDevices[c] = reserved->physicalDevices[c];

    // Done:
    return physicalDevices;
}

#pragma endregion

#pragma region Layers

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets all available layers that can be enable.
 * @return Vector containig a VkLayerProperties for each available layer.
 */
std::vector<VkLayerProperties> OVVK_API OvVK::VulkanInstance::getAvailableLayers() {

    //Get number of available layers
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    // Get the available layers
    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    return availableLayers;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Checks if the passed layers are supported.
 * @param validationLayers Vector containing all the names of the layers to be tested.
 * @return TF
 */
bool OVVK_API OvVK::VulkanInstance::areLayersSupported(std::vector<const char*> const& validationLayers) {

    // Get all suported layers
    std::vector<VkLayerProperties> availableLayers = getAvailableLayers();

    for (const char* layerName : validationLayers) {
        bool layerFound = false;

        for (const auto& layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            OV_LOG_ERROR("The layer %s is not supported.", layerName);
            return false;
        }
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Extensions

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the properties of supported instance extensions.
 * @return Vector containing structures describing the properties of supported instance extensions.
 */
std::vector<VkExtensionProperties> OVVK_API OvVK::VulkanInstance::getSupportedExtesions() {

    //request just the number of extensions
    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

    //Now allocate an array to hold the extension details
    std::vector<VkExtensionProperties> availableExtensions(extensionCount);

    //query the extension details
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableExtensions.data());

    return availableExtensions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * CheckS if the passed instance extensions are supported.
 * @param requiredInstanceExtensions Vector containing all the names of instance extensions to be tested.
 * @return TF
 */
bool OVVK_API OvVK::VulkanInstance::areExtensionsSupported(
    std::vector<const char*> const& requiredExtensions) {

    // Create hashset so the search is more fast (search in O(1))
    std::unordered_set<std::string> supportedExtensionsName;

    for (auto& supportedExtension : getSupportedExtesions())
        supportedExtensionsName.insert(supportedExtension.extensionName);

    // loop over required extensions to check if they are supported
    for (auto& requiredExtension : requiredExtensions) {

        //Need a std::string to comparison
        std::string requiredExtensionName(requiredExtension);

        //check if the extension is supported
        if (supportedExtensionsName.find(requiredExtensionName) == supportedExtensionsName.end()) {
            OV_LOG_ERROR("Extension %s not supported.", requiredExtension);
            return false;
        }
    }

    return true;
}

#pragma endregion

#pragma region DebugMessanger

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Populates the passed structure that will be used to create the debug messenger.
 * @param createInfo Structure specifying parameters to create a debug messenger
 */
void OVVK_API OvVK::VulkanInstance::populateDebugMessengerCreateInfo(
    VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    
    // Clean parameter
    createInfo = {};

    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;

    //specify all the types of severities you would like your callback to be called for.
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

    // filter which types of messages your callback is notified about
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    // specifies the pointer to the callback function
    createInfo.pfnUserCallback = debugCallback;

    //  field which will be passed along to the callback function via the pUserData parameter
    createInfo.pUserData = nullptr; // Optional
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Call back function for debug messanger
 * The callback returns a boolean that indicates if the Vulkan call that triggered the validation layer message
 * should be aborted. If the callback returns true, then the call is aborted with the 
 * VK_ERROR_VALIDATION_FAILED_EXT error. This is normally only used to test the validation layers themselves,
 * so you should always return VK_FALSE.
 * @param messageSeverity Specifies the severity of the message.
 * @param messageType Type of message.
 * @param pCallbackData Containing the details of the message itself.
 * @param pUserData Contains a pointer that was specified during the setup of the callback and allows you
 *		to pass your own data to it.
 * @return VkBool32
 */
VKAPI_ATTR VkBool32 VKAPI_CALL OvVK::VulkanInstance::debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData) {

    if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
        == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        OV_LOG_WARN("Validation layer: %s", pCallbackData->pMessage);
    }
    
    if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
        == VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT) {
        OV_LOG_INFO("Validation layer: %s", pCallbackData->pMessage);
    }
    
    if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
        == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
        OV_LOG_ERROR("Validation layer: %s", pCallbackData->pMessage);
    }

    return VK_FALSE;
}

#pragma endregion