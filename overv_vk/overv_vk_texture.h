/**
 * @file	overv_vk_texture.h
 * @brief	Vulkan texture properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling an Vulkan texture.
  */
class OVVK_API Texture : public Ov::Texture
{
//////////
public: //
//////////

    // Special values:
	static Texture empty;
	constexpr static uint32_t maxNrOfTextures = 500;	///< Max number of textures per material	

	constexpr static uint64_t readyToUploadValueStatusSyncObj = 1;
	constexpr static uint64_t uploadingValueStatusSyncObj = 2;
	constexpr static uint64_t readyValueStatusSyncObj = 3;
	constexpr static uint64_t freeValueStatusSyncObj = 4;

	// Const/dest:
	Texture();
	Texture(Texture&& other);
	Texture(Texture const&) = delete;
	virtual ~Texture();

	// Operators
	Texture& operator=(const Texture&) = delete;
	Texture& operator=(Texture&&) = delete;

	// Managed:
	bool virtual init() override;
	bool virtual free() override;

	// Sync Obj
	bool isLock() const;
	bool isOwned() const;

	// General:
	virtual bool load(const Ov::Bitmap& bitmap, bool isLinearEncoded = true, bool holdReferenceBitmap = false);
	virtual bool setUp(const Ov::Bitmap& bitmap, bool isLinearEncoded = true, bool holdReferenceBitmap = false);
	bool freeBitmapBuffer();

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	virtual bool create(uint32_t width, uint32_t height, uint32_t nrOfLevels, uint32_t nrOfLayers,
		Format format, bool isLinearEncoded = true, VkImageViewType imageViewType = VK_IMAGE_VIEW_TYPE_2D,
		VkImageCreateFlags flagsImage = 0x00000000, VkImageViewCreateFlags flagsImageView = 0x00000000);
#endif

	// Get/set:
	const OvVK::Image& getImage() const;
	const OvVK::ImageView& getImageView() const;
	const OvVK::Buffer& getBitmapBuffer() const;
	const OvVK::Sampler& getSampler() const;
	uint32_t getLutPos() const;
	bool getNeedGenerateMipmap() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	const std::vector<VkBufferImageCopy>& getBufferImageCopyRegions() const;

	bool setImageLayout(VkImageLayout imageLayout);
	bool setQueueFamilyIndex(uint32_t queueFamilyIndex);
	bool setLockSyncObj(VkFence fence);
	bool setStatusSyncObjValue(uint64_t value);
#endif

	bool setSampler(const OvVK::Sampler& sampler);

	// Rendering methods:
	virtual bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool createBitmapBuffer(VkBufferCreateInfo bufferCreateInfo,
		VmaAllocationCreateInfo bufferAllocationCreateInfo);
	bool createImage(VkImageCreateInfo imageCreateInfo,
		VmaAllocationCreateInfo imageAllocationCreateInfo, VkImageViewCreateInfo imageViewCreateInfo);
#endif

	// Get/Set
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool setBufferImageCopyRegions(std::vector<VkBufferImageCopy> &regions);
#endif
	void setNeedGenerateMipmap(bool value);

	// Const/dest:
	Texture(const std::string& name);

	// LUT:
	static bool lut[OvVK::Texture::maxNrOfTextures];
};