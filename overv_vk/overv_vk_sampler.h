/**
 * @file	overv_vk_sampler.h
 * @brief	Vulkan sampler
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

/**
 * @brief Class for modeling an Vulkan Sampler.
 */
class OVVK_API Sampler final : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Special values:
	static Sampler empty;

	// Const/dest:
	Sampler();
	Sampler(Sampler&& other);
	Sampler(Sampler const&) = delete;
	virtual ~Sampler();

	// Operators
	Sampler& operator=(const Sampler&) = delete;
	Sampler& operator=(Sampler&&) = delete;

	// Default sampler:
	static const Sampler& getDefault();

	// Managed
	bool init() override;
	bool free() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool create(VkSamplerCreateInfo samplerInfo);
#endif

	// Get/set:
	bool isAnisotropyEnabled() const;
	float getMaxAnisotropy() const;
	bool isUnnormalizedCoordinatesEnbled() const;
	bool isCompareEnabled() const;
	float getMinLevelOfDetail() const;
	float getMaxLevelOfDetail() const;
	float getMipLevelOfDetailBias() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkSampler getVkSampler() const;
	VkFilter getMagFilter() const;
	VkFilter getMinFilter() const;
	VkSamplerAddressMode getAddressModeU() const;
	VkSamplerAddressMode getAddressModeV() const;
	VkSamplerAddressMode getAddressModeW() const;
	VkBorderColor getBorderColor() const;
	VkCompareOp getCompareOperation() const;
	VkSamplerMipmapMode getMipmapMode() const;
#endif

	// Static
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	static VkSamplerCreateInfo getDefaultSamplerCreateInfo();
#endif

///////////
private: //
///////////   

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Sampler(const std::string& name);
};
