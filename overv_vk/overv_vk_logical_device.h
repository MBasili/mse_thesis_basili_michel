#pragma once
/**
 * @file	overv_vk_device.h
 * @brief	Vulkan device properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Class for modeling a Vulkan device.
  */
class OVVK_API LogicalDevice final : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Special values:
	static LogicalDevice empty;

	/**
	 * @brief Struct to collects the indices of the different types of queues.
	 */
	struct QueueFamilyIndices {
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> computeFamily;
		std::optional<uint32_t> transferFamily;
		std::optional<uint32_t> presentFamily;

		QueueFamilyIndices() : graphicsFamily{ std::nullopt },
			computeFamily{ std::nullopt },
			transferFamily{ std::nullopt },
			presentFamily{ std::nullopt }
		{}
	};

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Struct to collects the handles of the different types of queues.
	 */
	struct QueueHandles {

		VkQueue graphicsQueue;
		VkQueue computeQueue;
		VkQueue transferQueue;
		VkQueue presentQueue;

		QueueHandles() : graphicsQueue{ VK_NULL_HANDLE },
			computeQueue{ VK_NULL_HANDLE },
			transferQueue{ VK_NULL_HANDLE },
			presentQueue{ VK_NULL_HANDLE }
		{}

	};
#endif

	// The signature of the function to pick a suitable PhysicalDevice.
	typedef uint32_t(*RatePhysicalDevice) (const OvVK::PhysicalDevice& physicalDevice);

	// Const/dest:
	LogicalDevice();
	LogicalDevice(LogicalDevice&& other) noexcept;
	LogicalDevice(LogicalDevice const&) = delete;
	virtual ~LogicalDevice();

	// Operators
	LogicalDevice& operator=(const LogicalDevice&) = delete;
	LogicalDevice& operator=(LogicalDevice&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool create(VkPhysicalDeviceFeatures enabledFeatures,
		VmaAllocatorCreateFlags vmaAllocatorCreateFlags,
		void* pNextChain = nullptr,
		VkQueueFlags requestedQueueTypes = VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT);
#endif

	// Set/Get:
	const OvVK::PhysicalDevice& getPhysicalDevice() const;
	std::optional<QueueFamilyIndices> getQueueFamiliesIndices() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkDevice getVkDevice() const;
	VmaAllocator getVmaAllocator() const;
	std::optional<QueueHandles> getQueueHandles() const;
	VkQueue getQueueHandle(uint32_t queueFamilyIndex) const;

	bool setFuncRatePhysicalDevice(RatePhysicalDevice ratePhysicalDevice);
#endif

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	LogicalDevice(const std::string& name);
};