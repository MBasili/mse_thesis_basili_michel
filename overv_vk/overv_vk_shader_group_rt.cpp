/**
 * @file	overv_vk_shader_group.cpp
 * @brief	Vulkan generic shader group properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum
#include "magic_enum.hpp"


/////////////
// #STATIC //
/////////////

// Special value
OvVK::RTShaderGroup OvVK::RTShaderGroup::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief AccelerationStructure class reserved structure.
 */
struct OvVK::RTShaderGroup::Reserved
{
    VkRayTracingShaderGroupTypeKHR vkRTShaderGroupType; ///< Vulkan shader group type
    Type rtShaderGroupType;                             ///< OvVK shader group type

    std::optional<VkDeviceAddress> handleAddress;       ///< The shader group rt handle device address
    std::optional<uint32_t> positionInPipeline;         ///< The position of the shader group rt in the pipeline

    /**
     * Constructor
     */
    Reserved() : vkRTShaderGroupType{ VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR },
        rtShaderGroupType{ Type::none },
        handleAddress{ std::nullopt },
        positionInPipeline{ std::nullopt }
    {}
};


/////////////////////////////////
// BODY OF CLASS RTShaderGroup //
/////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::RTShaderGroup::RTShaderGroup() : reserved(std::make_unique<OvVK::RTShaderGroup::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::RTShaderGroup::RTShaderGroup(const std::string& name) : Ov::Program(name),
    reserved(std::make_unique<OvVK::RTShaderGroup::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::RTShaderGroup::RTShaderGroup(RTShaderGroup&& other) : Ov::Program(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::RTShaderGroup::~RTShaderGroup()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())  
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an Vulkan shader.
 * @return TF
 */
bool OVVK_API OvVK::RTShaderGroup::init()
{
    if (this->Ov::Program::init() == false)
        return false;

    // Done:      
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an Vulkan instance.
 * @return TF
 */
bool OVVK_API OvVK::RTShaderGroup::free()
{
    if (this->Ov::Program::free() == false)
        return false;

    // Done:      
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Builds the rt shader group
 * @param shaders A list of OvVK shaders to insert in the rt shader group.
 * @return TF
 */
bool OVVK_API OvVK::RTShaderGroup::build(std::initializer_list<std::reference_wrapper<Ov::Shader>> shaders) 
{
    // Safety net:
    if (shaders.size() == 0 || shaders.size() > 3)
    {
        OV_LOG_ERROR(R"(Too many or too few shaders in the shader group rt with id= %d. Max shader of 
diffenrete type supported per group shader rt is 3.)", getId());
        return false;
    }

    if (this->init() == false)
        return false;

    uint8_t shadersCounter = 0;
    bool closestHit = false;
    bool anyHit = false;
    for (std::reference_wrapper<Ov::Shader> shader : shaders)
    {
        switch (shader.get().getType())
        {
        case Ov::Shader::Type::ray_generation:
            if (shadersCounter > 0)
            {
                OV_LOG_ERROR("Shader combination not supported in RT shader group with id= %d.", getId());
                return false;
            }
            reserved->vkRTShaderGroupType = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
            reserved->rtShaderGroupType = Type::ray_generation;
            shadersCounter++;
            break;

        case Ov::Shader::Type::intersection:
            if (reserved->vkRTShaderGroupType == VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR ||
                shadersCounter > 0 &&
                reserved->vkRTShaderGroupType == VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR)
            {
                OV_LOG_ERROR("Shader combination not supported in RT shader group with id= %d.", getId());
                return false;
            }
            reserved->vkRTShaderGroupType = VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR;
            reserved->rtShaderGroupType = Type::hit;
            shadersCounter++;
            break;

        case Ov::Shader::Type::any_hit:
            if (anyHit || shadersCounter > 0 &&
                reserved->vkRTShaderGroupType == VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR)
            {
                OV_LOG_ERROR("Shader combination not supported in RT shader group with id= %d.", getId());
                return false;
            }

            if (reserved->vkRTShaderGroupType != VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR)
                reserved->vkRTShaderGroupType = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;

            reserved->rtShaderGroupType = Type::hit;
            anyHit = true;
            shadersCounter++;
            break;

        case Ov::Shader::Type::closes_hit:
            if (closestHit || shadersCounter > 0 &&
                reserved->vkRTShaderGroupType == VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR)
            {
                OV_LOG_ERROR("Shader combination not supported in RT shader group with id= %d.", getId());
                return false;
            }

            if (reserved->vkRTShaderGroupType != VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR)
                reserved->vkRTShaderGroupType = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;

            reserved->rtShaderGroupType = Type::hit;
            closestHit = true;
            shadersCounter++;
            break;

        case Ov::Shader::Type::callable:
            if (shadersCounter > 0)
            {
                OV_LOG_ERROR("Shader combination not supported in RT shader group with id= %d.", getId());
                return false;
            }
            reserved->vkRTShaderGroupType = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
            reserved->rtShaderGroupType = Type::callable;
            shadersCounter++;
            break;

        case Ov::Shader::Type::miss:
            if (shadersCounter > 0)
            {
                OV_LOG_ERROR("Shader combination not supported in RT shader group with id= %d.", getId());
                return false;
            }
            reserved->vkRTShaderGroupType = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
            reserved->rtShaderGroupType = Type::miss;
            shadersCounter++;
            break;

        default:
            OV_LOG_ERROR("The passed shader type is not supported for create RT shader group with id= %d.", getId());
            return false;
            break;
        }
    }

    if (this->Ov::Program::build(shaders) == false)
        return false;

    //Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a Vulkan enum flag describing shader type.
 * @return Gets a Vulkan enum flag describing shader type.
 */
VkRayTracingShaderGroupTypeKHR OVVK_API OvVK::RTShaderGroup::getVKRTShaderGroupType() const 
{
    return reserved->vkRTShaderGroupType;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a OvVK enum flag describing shader type.
 * @return Gets a OvVK enum flag describing shader type.
 */
OvVK::RTShaderGroup::Type OVVK_API OvVK::RTShaderGroup::getShaderGroupType() const
{
    return reserved->rtShaderGroupType;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a OvVK shader obj of the passed OvVK shader type.
 * @param type A OvVK shader type.
 * @return Gets a OvVK shader obj of the passed OvVK shader type.
 */
const OvVK::Shader OVVK_API& OvVK::RTShaderGroup::getShader(OvVK::Shader::Type type) const 
{
    uint32_t nrOfShader = getNrOfShaders();

    for (uint32_t c = 0; c < nrOfShader; c++)
    {
        std::reference_wrapper<const Ov::Shader> shader = this->Ov::Program::getShader(c);

        if (shader.get().getType() == type)
            if (const OvVK::Shader* toReturnShader = dynamic_cast<const OvVK::Shader*>(&shader.get()))
                return *toReturnShader;
    }

    OV_LOG_DEBUG("The rt shader group with id= %d doesn't have a shader of the passed type %s", getId(),
        magic_enum::enum_name(type));
    return OvVK::Shader::empty;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the rt shader group handle device address to access the rt shader group in the GPU memory.
 * @return Std optional obj with the rt shader group handle device address to access the rt shader group in the GPU memory.
 */
std::optional<VkDeviceAddress> OVVK_API OvVK::RTShaderGroup::getHandleDeviceAddress() const
{
    return reserved->handleAddress;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the position of the rt shader group in the pipeline.
 * @return Std optional obj with the position of the rt shader group in the pipeline if set.
 */
std::optional<uint32_t> OVVK_API OvVK::RTShaderGroup::getPositionInPipeline() const
{
    return reserved->positionInPipeline;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the rt shader group handle device address to access the rt shader group in the GPU memory.
 */
void OVVK_API OvVK::RTShaderGroup::setHandleDeviceAddress(VkDeviceAddress address)
{
    reserved->handleAddress = address;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the position of the rt shader group in the pipeline.
 */
void OVVK_API OvVK::RTShaderGroup::setPositionInPipeline(uint32_t pos)
{
    reserved->positionInPipeline = pos;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::RTShaderGroup::render(uint32_t value, void* data) const
{
    if(isDirty())
        this->setDirty(true);

    // Done:
    return true;
}

#pragma endregion