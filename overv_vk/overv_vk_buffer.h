#pragma once
/**
 * @file	overv_vk_buffer.h
 * @brief	Vulkan buffer
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/**
 * @brief Class to represent Vulkan Buffer
 */
class OVVK_API Buffer : public Ov::Buffer
{
//////////
public: //
//////////

	// Static:
	static Buffer empty;

	// Values for the status syncObj
	constexpr static uint64_t defaultValueStatusSyncObj = 0;

	// Const/dest:
	Buffer();
	Buffer(Buffer&& other) noexcept;
	Buffer(Buffer const&) = delete;
	virtual ~Buffer();

	// Operators
	Buffer& operator=(const Buffer&) = delete;
	Buffer& operator=(Buffer&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// SyncObj
	bool isLock() const;
	bool isOwned() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool create(VkBufferCreateInfo vkBufferCreateInfo,
		VmaAllocationCreateInfo vmaBufferAllocationCreateInfo);

	// Get/set:   
	VkBuffer getVkBuffer() const;
	VmaAllocation getVmaAllocation() const;
	VmaAllocationInfo getVmaAllocationInfo() const;
	std::optional<VkDeviceAddress> getBufferDeviceAddress() const;
	std::optional<uint32_t> getQueueFamilyIndex() const;
	VkFence getLockSyncObj() const;
	VkSemaphore getStatusSyncObj() const;

	bool setQueueFamilyIndex(uint32_t queueFamilyIndex);
	bool setLockSyncObj(VkFence fence);
	bool setStatusSyncObjValue(uint64_t value);
#endif

/////////////
protected: //
/////////////

	//Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Buffer(const std::string &name);
};
