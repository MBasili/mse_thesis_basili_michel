#pragma once
/**
 * @file    overv_vk_tlas.h
 * @brief	Top Level Acceleration Structure
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 /**
  * @brief TopLevelAccelerationStructure TLAS in vukan
  */
class OVVK_API TopLevelAccelerationStructure final : public OvVK::AccelerationStructure
{
//////////
public: //
//////////

	// Special values:
	static TopLevelAccelerationStructure empty;

	// Const/dest:
	TopLevelAccelerationStructure();
	TopLevelAccelerationStructure(TopLevelAccelerationStructure&& other) noexcept;
	TopLevelAccelerationStructure(TopLevelAccelerationStructure const&) = delete;
	~TopLevelAccelerationStructure();

	// Operators
	TopLevelAccelerationStructure& operator=(const TopLevelAccelerationStructure&) = delete;
	TopLevelAccelerationStructure& operator=(TopLevelAccelerationStructure&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	bool setUpUpdate();
	bool clearSetUpData();

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool setUpBuild(uint32_t nrOfASInstance, VkGeometryFlagsKHR geometriesFlag = VK_GEOMETRY_OPAQUE_BIT_KHR,
		VkBuildAccelerationStructureFlagsKHR asFlags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR);
#endif

	// Get/Set
	const OvVK::Buffer& getDeviceASInstancesBuffer() const;
	uint32_t getNrOfASInstances() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkGeometryFlagsKHR getASGeometriesFlag() const;
	std::optional<std::reference_wrapper<const VkAccelerationStructureGeometryKHR>> getAccelerationStructureGeometriesKHR() const;
	std::optional<std::reference_wrapper<const VkAccelerationStructureBuildRangeInfoKHR>> getAccelerationStructureBuildRangeInfosKHR() const;
#endif

	// Render:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool setUp(uint32_t nrOfASInstance, VkGeometryFlagsKHR geometriesFlag);
#endif

	// Const/dest:
	TopLevelAccelerationStructure(const std::string& name);
};