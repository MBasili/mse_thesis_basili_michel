/**
 * @file	overv_vk_pipeline_whitted_rt_with_pbr_lighting.cpp
 * @brief	Descriptor set manager for Whitted ray tracing with PBR
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 //////////////
 // #INCLUDE //
 //////////////

 // Main include:
#include "overv_vk.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Lighting descriptor set manager reserved structure.
 */
struct OvVK::WhittedRTwithPBRDescriptorSetsManager::Reserved
{
    std::pair<VkDescriptorImageInfo, bool> descriptorOutputImageInfos[OvVK::Engine::nrFramesInFlight];
    std::pair<VkDescriptorImageInfo, bool> descriptorAOImageInfos[OvVK::Engine::nrFramesInFlight];

    /**
     * Constructor.
     */
    Reserved()
    {}
};


/////////////////////////////////////////////////
// BODY OF CLASS WhittedRTwithPBRDescriptorSetsManager //
/////////////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::WhittedRTwithPBRDescriptorSetsManager() :
    reserved(std::make_unique<OvVK::WhittedRTwithPBRDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::WhittedRTwithPBRDescriptorSetsManager(const std::string& name) :
    OvVK::DescriptorSetsManager(name),
    reserved(std::make_unique<OvVK::WhittedRTwithPBRDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::WhittedRTwithPBRDescriptorSetsManager(
    WhittedRTwithPBRDescriptorSetsManager&& other) :
    OvVK::DescriptorSetsManager(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::~WhittedRTwithPBRDescriptorSetsManager()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::init()
{
    if (this->OvVK::DescriptorSetsManager::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::free()
{
    if (this->OvVK::DescriptorSetsManager::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets and descriptor sets layouts.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::create()
{
    // Init
    if (this->init() == false)
        return false;


    /////////////////////
    // Descriptor Pool //
    /////////////////////

    // Create descriptorPool
    std::vector<VkDescriptorPoolSize> descriptorPoolSizes(2);

    // RT output Image
    descriptorPoolSizes[0].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descriptorPoolSizes[0].descriptorCount = OvVK::Engine::nrFramesInFlight;

    // AO Image
    descriptorPoolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descriptorPoolSizes[1].descriptorCount = OvVK::Engine::nrFramesInFlight;


    ///////////////////////////
    // Descriptor set layout //
    ///////////////////////////

    // Create descriptorSetLayout
    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings(2);

    // RT Image
    descriptorSetLayoutBindings[0].binding = static_cast<uint32_t>(OvVK::WhittedRTwithPBRDescriptorSetsManager::Binding::outputImage);
    descriptorSetLayoutBindings[0].descriptorCount = 1;
    descriptorSetLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descriptorSetLayoutBindings[0].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[0].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;

    // AO Image
    descriptorSetLayoutBindings[1].binding = static_cast<uint32_t>(OvVK::WhittedRTwithPBRDescriptorSetsManager::Binding::aoImage);
    descriptorSetLayoutBindings[1].descriptorCount = 1;
    descriptorSetLayoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descriptorSetLayoutBindings[1].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[1].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;


    // Create
    return this->OvVK::DescriptorSetsManager::create(descriptorPoolSizes, descriptorSetLayoutBindings,
        OvVK::Engine::nrFramesInFlight);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds an update to update the raytracing DSM.
 * @param data The reference to a structure containing the new tlas and image.
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::addToUpdateList(const UpdateData& data)
{
    // Safety net
    if (data.targetFrame >= OvVK::Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed target frame is out of range in the lighting DSM with id= %d.", getId());
        return false;
    }

    if (data.descriptorOutputImageInfo.has_value())
        reserved->descriptorOutputImageInfos[data.targetFrame] = std::make_pair(data.descriptorOutputImageInfo.value(), true);

    if (data.descriptorAOImageInfo.has_value())
        reserved->descriptorAOImageInfos[data.targetFrame] = std::make_pair(data.descriptorAOImageInfo.value(), true);

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::WhittedRTwithPBRDescriptorSetsManager::render(uint32_t value, void* data) const
{
    // Engine
    std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Frame in flight
    uint32_t frameInFlight = engine.get().getFrameInFlight();


    /////////////////////////////
    // Write to descriptor set //
    /////////////////////////////

    std::vector<VkWriteDescriptorSet> writeDescriptorSets;


    //////////////////
    // Output Image //
    //////////////////

    if (reserved->descriptorOutputImageInfos[frameInFlight].second)
    {
        VkWriteDescriptorSet& imageWriteDescriptorSets = writeDescriptorSets.emplace_back();
        imageWriteDescriptorSets.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        imageWriteDescriptorSets.pNext = NULL;
        imageWriteDescriptorSets.dstSet = getVkDescriptorSet(frameInFlight);
        imageWriteDescriptorSets.dstBinding = static_cast<uint32_t>(WhittedRTwithPBRDescriptorSetsManager::Binding::outputImage);
        imageWriteDescriptorSets.dstArrayElement = 0;
        imageWriteDescriptorSets.descriptorCount = 1;
        imageWriteDescriptorSets.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        imageWriteDescriptorSets.pImageInfo = &reserved->descriptorOutputImageInfos[frameInFlight].first;
        imageWriteDescriptorSets.pBufferInfo = NULL;
        imageWriteDescriptorSets.pTexelBufferView = NULL;

        reserved->descriptorOutputImageInfos[frameInFlight].second = false;
    }

    //////////////
    // AO Image //
    //////////////

    if (reserved->descriptorAOImageInfos[frameInFlight].second)
    {
        VkWriteDescriptorSet& imageWriteDescriptorSets = writeDescriptorSets.emplace_back();
        imageWriteDescriptorSets.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        imageWriteDescriptorSets.pNext = NULL;
        imageWriteDescriptorSets.dstSet = getVkDescriptorSet(frameInFlight);
        imageWriteDescriptorSets.dstBinding = static_cast<uint32_t>(WhittedRTwithPBRDescriptorSetsManager::Binding::aoImage);
        imageWriteDescriptorSets.dstArrayElement = 0;
        imageWriteDescriptorSets.descriptorCount = 1;
        imageWriteDescriptorSets.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        imageWriteDescriptorSets.pImageInfo = &reserved->descriptorAOImageInfos[frameInFlight].first;
        imageWriteDescriptorSets.pBufferInfo = NULL;
        imageWriteDescriptorSets.pTexelBufferView = NULL;

        reserved->descriptorAOImageInfos[frameInFlight].second = false;
    }


    // Update
    if (writeDescriptorSets.size() > 0)
    {
        // This can generate a race condition and undefined behavior if the descriptor set to 
        // update is in use by a command buffer that�s being executed in a queue.
        // cannot modify a descriptor set unless you know that the GPU is finished with it.
        // Update ad the beginning of the frame.
        vkUpdateDescriptorSets(engine.get().getLogicalDevice().getVkDevice(), static_cast<uint32_t>(writeDescriptorSets.size()),
            writeDescriptorSets.data(), 0, NULL);
    }

    // Done:
    return true;
}

#pragma endregion