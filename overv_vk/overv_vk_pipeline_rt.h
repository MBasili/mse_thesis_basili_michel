/**
 * @file	overv_vk_rt_pipeline.h
 * @brief	Vulkan RayTracing pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modelling RayTracing Pipeline
  */
class OVVK_API RayTracingPipeline : public OvVK::Pipeline
{
//////////
public: //
//////////

	constexpr static uint64_t defaultSBTValueStatusSyncObj = 0;
	constexpr static uint64_t readyToUploadSBTValueStatusSyncObj = 1;
	constexpr static uint64_t uploadingSBTValueStatusSyncObj = 2;
	constexpr static uint64_t readySBTValueStatusSyncObj = 3;

	/**
	 * @brief Type of ray that can be used to trace a ray.
	 */
	enum class RayType : uint32_t 
	{
		primary = 0,
		secondary = 1,
		occlusion = 2,
		ao = 3,
		last
	};

	// Const/dest:
	RayTracingPipeline();
	RayTracingPipeline(RayTracingPipeline&& other);
	RayTracingPipeline(RayTracingPipeline const&) = delete;
	virtual ~RayTracingPipeline();

	// Operators
	RayTracingPipeline& operator=(const RayTracingPipeline&) = delete;
	RayTracingPipeline& operator=(RayTracingPipeline&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// Get/Set
	static uint32_t getMaxRayRecursionDepth();
	static uint32_t getShaderGroupHandleStride();
	static uint32_t getShaderGroupBaseAlignment();
	static uint32_t getShaderGroupHandleSize();

/////////////
protected: //
/////////////

    // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// General:
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool createRTPipeline(VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo,
		uint32_t maxPipelineRayRecursionDepth,
		VkPipelineCreateFlags flags);
#endif

	// Get/set:
	OvVK::RayTracingDescriptorSetsManager& getRayTracingDescriptorSetsManager() const;

	OvVK::Shader& getRayGenShader() const;
	std::map<std::string, OvVK::Shader>& getIntersectionHitShaders() const;
	std::map<std::string, OvVK::Shader>& getAnyHitShaders() const;
	std::map<std::string, OvVK::Shader>& getClosestHitShaders() const;
	std::map<std::string, OvVK::Shader>& getCallableHitShaders() const;
	std::map<std::string, OvVK::Shader>& getMissHitShaders() const;

	OvVK::RTShaderGroup& getRayGenShaderGroup() const;
	std::map<std::string, OvVK::RTShaderGroup>& getHitShaderGroups() const;
	std::map<std::string, OvVK::RTShaderGroup>& getCallableShaderGroups() const;
	std::map<std::string, OvVK::RTShaderGroup>& getMissShaderGroups() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkStridedDeviceAddressRegionKHR& getRaygenShaderBindingTable() const;
	VkStridedDeviceAddressRegionKHR& getHitShaderBindingTable() const;
	VkStridedDeviceAddressRegionKHR& getCallableShaderBindingTable() const;
	VkStridedDeviceAddressRegionKHR& getMissShaderBindingTable() const;
#endif

	std::vector<uint8_t>& getShaderGroupsHandleStorage() const;

	OvVK::Buffer& getSBTHostBuffer() const;
	OvVK::Buffer& getSBTDeviceBuffer() const;

	// Const/dest:
	RayTracingPipeline(const std::string& name);
};