#pragma once
/**
 * @file	overv_vk.h
 * @brief	RayTracing Vulkan Library
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Abstract implementation
#include "overv_core.h"

// C/C++
#include <optional>
#include <set>
#include <map>
#include <algorithm>
#include <functional>

// This includes are not included for application that use the libraries.
// This approach is necessary since most of the configurations within the
// engine are done via Vulkan structures or enums.
// A general restructuring could solve the need to include these headers
// here but would require changing the logic of how the elements within the engine were designed.
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Include VMA header and implicit also vulkan header
	#include "vk_mem_alloc.h"
	// nclude GLFW library
	#include "GLFW/glfw3.h"
#endif

/////////////
// VERSION //
/////////////

// Export API:
#ifdef OV_WINDOWS
// Specifies i/o linkage (VC++ spec):
#ifdef OVERV_VULKAN_EXPORTS
#define OVVK_API __declspec(dllexport)
#else
#define OVVK_API __declspec(dllimport)
#endif      
#endif
#ifdef OV_LINUX
#define OVVK_API       
#endif

///////////////
// NAMESPACE //
///////////////

namespace OvVK {

//////////////
// #INCLUDE //
//////////////

	// Entities:
	#include "overv_vk_command_buffer.h"
	#include "overv_vk_command_pool.h"
	#include "overv_vk_surface.h"
	#include "overv_vk_surface_dependent_obj.h"
	#include "overv_vk_physical_device.h"
	#include "overv_vk_vulkan_instance.h"
	#include "overv_vk_logical_device.h"
	#include "overv_vk_swapchain.h"
	#include "overv_vk_buffer.h"
	#include "overv_vk_image_view.h"
	#include "overv_vk_image.h"
	#include "overv_vk_bitmap.h"
	#include "overv_vk_sampler.h"
	#include "overv_vk_camera.h"
	#include "overv_vk_thread.h"
	#include "overv_vk_thread_pool.h"
	#include "overv_vk_rt_scene.h"

	// Renderables
	#include "overv_vk_shader.h"
	#include "overv_vk_shader_group_rt.h"
	#include "overv_vk_geometry.h"
	#include "overv_vk_texture.h"
	#include "overv_vk_texture2d.h"
	#include "overv_vk_material.h"
	#include "overv_vk_light.h"
	#include "overv_vk_as.h"
	#include "overv_vk_blas.h"
	#include "overv_vk_tlas.h"
	#include "overv_vk_rt_obj_desc.h"

	// Descriptor Set
	#include "overv_vk_dsm.h"
	#include "overv_vk_dsm_textures.h"
	#include "overv_vk_dsm_storage.h"
	#include "overv_vk_dsm_rt.h"
	#include "overv_vk_dsm_picking.h"
	#include "overv_vk_dsm_whitted_rt_with_pbr_lighting.h"
	
	// Storage
	#include "overv_vk_container.h"
	#include "overv_vk_storage.h"

	// Loader
	#include "overv_vk_ovo.h"
	#include "overv_vk_loader.h"

	// Pipeline
	#include "overv_vk_pipeline.h"
	#include "overv_vk_pipeline_gui.h"
	#include "overv_vk_pipeline_rt.h"
	#include "overv_vk_pipeline_picking.h"
	#include "overv_vk_pipeline_whitted_rt_with_pbr_lighting.h"
	
	/**
	 * @brief Base engine main class. This class is a singleton.
	 */
	class OVVK_API Engine : public Ov::Core {

	//////////
	public: //
	//////////

		// Static value

		// This variable indicates the number of frames processed at the same time.
		// The minimum number of frames in flight is 2, which is the minimum number of images a swapchain can contain.
		// If you set the value 1, the application will crash.

		/////////////////
		// WARNING!!!! //
		/////////////////
		// In addition, if you wish to use more than three frames in flight, 
		// you must set the static variable waitCommandsAtTheEndOfAFrame to TRUE.
		// This is due to the fact that more than three frames in flight cause the application to crash for
		// both release and debug version if one switch the scene to render in the client application. 
		// Where the host can no longer send commands as it thinks it has lost control over the device.
		// I am not sure what is causing the problem. Probably the cause is the fact that the
		// device has to perform all the operations of the various frames in flight.
		// However, a more in depth analysis must be carried out to verify or discover the origins of the problem.
		constexpr static uint32_t nrFramesInFlight = 3;	
		// This variable is used to tell the engine to wait until all commands submitted to the
		// device during the processing of a frame are finished before processing a new frame.
		constexpr static bool waitCommandsAtTheEndOfAFrame = true;

		// Enable or disable validation layers.
#ifdef OV_DEBUG
		static const bool enableValidationLayers = true;
#else
		static const bool enableValidationLayers = false;
#endif

		// Enable or disable debug messanger.
#ifdef OV_DEBUG
		static const bool enableDebugMessanger = true;
#else
		static const bool enableDebugMessanger = false;
#endif

		inline static const std::string texturesSourceFolder = "..\\resources\\textures";	///< Folder containing all the textures
		inline static const std::string modelsSourceFolder = "..\\resources\\models";     ///< Folder containing all the models

		// Const/Dest (static class, can't be used):
		Engine(Engine const&) = delete; // Disallow copying construct
		virtual ~Engine();

		// Operators:
		Engine& operator=(const Engine&) = delete;
		Engine& operator=(Engine&&) = delete;

		// Singleton:
		static Engine& getInstance();

		// Init/free:
		bool init(const Ov::Config& config = Ov::Config::empty);
		bool free();

		// Get/Set:
		uint64_t getFrameNr() const;
		uint32_t getFrameInFlight() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
		VkSampleCountFlagBits getNrOfAASamples() const;
#endif

		OvVK::VulkanInstance& getVulkanInstance() const;
		OvVK::Surface& getPresentSurface() const;
		OvVK::LogicalDevice& getLogicalDevice() const;
		OvVK::Swapchain& getSwapchain() const;
		OvVK::Storage& getStorage() const;

		OvVK::CommandPool& getPrimaryGraphicCmdPool(uint32_t frameInFlight =
			Engine::getInstance().getFrameInFlight()) const;
		OvVK::CommandPool& getPrimaryTransferCmdPool(uint32_t frameInFlight =
			Engine::getInstance().getFrameInFlight()) const;
		OvVK::CommandPool& getPrimaryComputeCmdPool(uint32_t frameInFlight =
			Engine::getInstance().getFrameInFlight()) const;
		OvVK::CommandPool& getPrimaryPresentCmdPool(uint32_t frameInFlight =
			Engine::getInstance().getFrameInFlight()) const;

		std::list< std::reference_wrapper<OvVK::Pipeline>>& getPipelines() const;

		uint32_t getPresentableImageIndex(uint32_t frameInFlight =
			Engine::getInstance().getFrameInFlight()) const;

		// Management:
		bool processEvents();
		bool acquireFrameImage();
		bool drawFrame();
		void addPipeline(OvVK::Pipeline& pipeline);
		void removePipeline(OvVK::Pipeline& pipeline);

		// Debug:
		static bool areValidationLayersEnable();
		static bool isDebugMessangerEnable();

		// Extensions:
		static std::vector<const char*> getRequiredInstanceExtensions();
		static std::vector<const char*> getRequiredDeviceExtensions();
		static std::vector<const char*> getRequiredValidationLayers();

	///////////
	private: //
	///////////

		// Reserved:
		struct Reserved;
		std::unique_ptr<Reserved> reserved;

		// InitComponenet
		bool initGLFW();
		bool initInstance(std::string appName);
		bool initSurface(std::string title, uint32_t width, uint32_t height);
		bool initLogicalDevice();
		bool initSwapChain();
		bool initCommandPools();
		bool initDescriptorSets();
		bool initStorage();
		bool initShaderPreprocessor();

		// Const/dest:
		Engine();
	};
}; // end of namespace OvVK::