/**
 * @file	overv_vk_physical_device.h
 * @brief	Vulkan physical device properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for manage Vulkan physical device.
  */
class OVVK_API PhysicalDevice final : public Ov::Object
{
//////////
public: //
//////////

	// Special values;
	static PhysicalDevice empty;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Structure to store the supported parameters for the swapchain.
	 */
	struct SwapChainSupportedParameters {
		VkSurfaceCapabilitiesKHR capabilities;		///< Basic surface capabilities (min/max number of images in a swapchain, min/max width and height of images)
		std::vector<VkSurfaceFormatKHR> formats;	///< Surface formats(pixel format, color space)
		std::vector<VkPresentModeKHR> presentModes; ///< Available presentation modes
	};
#endif

	// Const/dest:
	PhysicalDevice();
	PhysicalDevice(PhysicalDevice&& other);
	PhysicalDevice(PhysicalDevice const& other) = delete;
	virtual ~PhysicalDevice();

	// Operators:
	PhysicalDevice& operator=(const PhysicalDevice&) = delete;
	PhysicalDevice& operator=(PhysicalDevice&&) = delete;

	// General:
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool connect(VkPhysicalDevice physicalDevice);
#endif

	bool isExtensionSupported(std::string extension) const;

	// Get/Set:
	std::vector<std::string> getSupportedExtensions() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkPhysicalDevice getPhysicalDevice() const;
	std::optional<VkPhysicalDeviceProperties2> getProperties(void* pNext = VK_NULL_HANDLE) const;
	std::optional<VkPhysicalDeviceFeatures2> getFeatures(void* pNext = VK_NULL_HANDLE) const;
	std::optional<VkFormatProperties2> getFormatProperties(VkFormat format, void* pNext = VK_NULL_HANDLE) const;
	std::optional<VkImageFormatProperties2> getImageFormatProperties(VkPhysicalDeviceImageFormatInfo2* physicalDeviceImageFormatInfo2,
		void* pNext = VK_NULL_HANDLE) const;
	std::vector<VkQueueFamilyProperties2> getQueueFamilyProperties(void* pNext = VK_NULL_HANDLE) const;
	std::optional<uint32_t> getQueueFamilyIndex(VkQueueFlags queueFlags) const;
	std::optional<uint32_t> getPresentQueueFamilyIndex(const OvVK::Surface& presentSurface) const;
	std::optional<VkSampleCountFlags> getMaxUsableSampleCount() const;
	std::optional<SwapChainSupportedParameters> getSwapChainSupportInfo(const OvVK::Surface& presentSurface) const;
#endif

	// Static
	static uint32_t defaultRatePhysicalDevice(const PhysicalDevice& physicalDevice);

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	PhysicalDevice(const std::string& name);
};