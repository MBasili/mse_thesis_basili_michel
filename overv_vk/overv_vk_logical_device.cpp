/**
 * @file	overv_vk_logical_device.cpp
 * @brief	Vulkan device properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main include
#include "overv_vk.h"

// C/C++
#include <algorithm>


////////////
// STATIC //
////////////

// Special values
OvVK::LogicalDevice OvVK::LogicalDevice::empty("empty");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief LogicalDevice class reserved structure.
 */
struct OvVK::LogicalDevice::Reserved
{
    std::reference_wrapper<const OvVK::PhysicalDevice> physicalDevice;  ///< Reference to OvVK Physical device instance.

    VkDevice logicalDevice;                                                 ///< Vulkan logical device handle.
    VmaAllocator allocator;                                                 ///< VMA allocator handle.
    QueueFamilyIndices queueFamilyIndices;                                  ///< Struct containig a queue family index for each type.
    QueueHandles queueHandles;                                              ///< Struct containig a queue handle for each type.

    RatePhysicalDevice pointerRatePhysicalDevice;                           ///< Pointer to the function to rate the instance physical device.

    /**
     * Constructor.
     */
    Reserved() : physicalDevice{ OvVK::PhysicalDevice::empty },
        logicalDevice{ VK_NULL_HANDLE },
        allocator{ nullptr },
        queueFamilyIndices{},
        queueHandles{},
        pointerRatePhysicalDevice{ nullptr }
    {}
};


/////////////////////////////////
// BODY OF CLASS LogicalDevice //
/////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::LogicalDevice::LogicalDevice() :
    reserved(std::make_unique<OvVK::LogicalDevice::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The logical device name.
 */
OVVK_API OvVK::LogicalDevice::LogicalDevice(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::LogicalDevice::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::LogicalDevice::LogicalDevice(LogicalDevice&& other) noexcept : Ov::Object(std::move(other)),
    Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::LogicalDevice::~LogicalDevice()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes device
 * @return TF
 */
bool OVVK_API OvVK::LogicalDevice::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Destroy allocator
    if (reserved->allocator != nullptr)
    {
        // Destroy allocator VMA
        vmaDestroyAllocator(reserved->allocator);
        reserved->allocator = nullptr;
    }

    // Destroy logical device
    if (reserved->logicalDevice != VK_NULL_HANDLE)
    {
        vkDestroyDevice(reserved->logicalDevice, nullptr);
        reserved->logicalDevice = VK_NULL_HANDLE;
    }

    reserved->queueFamilyIndices = {};
    reserved->queueHandles = {};

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases device
 * @return TF
 */
bool OVVK_API OvVK::LogicalDevice::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Destroy allocator
    if (reserved->allocator != nullptr)
    {
        // Destroy allocator VMA
        vmaDestroyAllocator(reserved->allocator);
        reserved->allocator = nullptr;
    }

    // Destroy logical device
    if (reserved->logicalDevice != VK_NULL_HANDLE)
    {
        if (vkDeviceWaitIdle(reserved->logicalDevice) != VK_SUCCESS)
            OV_LOG_ERROR("Fail to wait all queue to be in idle.");

        VkDevice logicalDevice = reserved->logicalDevice;
        vkDestroyDevice(logicalDevice, VK_NULL_HANDLE);
        reserved->logicalDevice = VK_NULL_HANDLE;
    }

    reserved->queueFamilyIndices = {};
    reserved->queueHandles = {};


    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create a Logical Device
 * @param enabledFeatures Physical device features to enable.
 * @param vmaAllocatorCreateFlags Flag to create a VmaAllocator.
 * @param pNextChain A pointer to a structure extending VkPhysicalDeviceFeatures2.
 * @param requestedQueueTypes The queues types the engine will use.
 * @return TF
 */
bool OVVK_API OvVK::LogicalDevice::create(VkPhysicalDeviceFeatures enabledFeatures,
    VmaAllocatorCreateFlags vmaAllocatorCreateFlag,
    void* pNextChain,
    VkQueueFlags requestedQueueTypes)
{

    // Init:
    if (this->init() == false)
        return false;


    // Engine
    std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Vulkan instance
    std::reference_wrapper<const OvVK::VulkanInstance> vulkanInstance = engine.get().getVulkanInstance();
    // Surface
    std::reference_wrapper<const OvVK::Surface> presentSurface = engine.get().getPresentSurface();


    /////////////////////
    // Physical device //
    /////////////////////

    // Get physical devices
    std::vector<std::reference_wrapper<const OvVK::PhysicalDevice>> physicalDevices
        = vulkanInstance.get().getPhysicalDevices();

    if (physicalDevices.size() <= 0) 
    {
        OV_LOG_ERROR("No physical device found on the engine in logical device with id= %d.", getId());
        return false;
    }

    // Check the rate function.
    if (reserved->pointerRatePhysicalDevice == nullptr)
        reserved->pointerRatePhysicalDevice = OvVK::PhysicalDevice::defaultRatePhysicalDevice;

    // Rate physical device
    std::reference_wrapper<const OvVK::PhysicalDevice> empty = OvVK::PhysicalDevice::empty;
    std::vector<std::pair<uint32_t, std::reference_wrapper<const OvVK::PhysicalDevice>>>
        physicalDevicesScores(physicalDevices.size(), std::make_pair(0, empty));

    for (uint32_t c = 0; c < physicalDevices.size(); c++)
    {
        physicalDevicesScores[c].first = reserved->pointerRatePhysicalDevice(physicalDevices[c]);
        physicalDevicesScores[c].second = physicalDevices[c];
    }

    // Sort descending order rated physical device
    std::sort(physicalDevicesScores.begin(), physicalDevicesScores.end(),
        [](std::pair<uint32_t, std::reference_wrapper<const OvVK::PhysicalDevice>>& a,
        std::pair<uint32_t, std::reference_wrapper<const OvVK::PhysicalDevice>>& b) -> bool
        {
            return a.first > b.first;
        });

    reserved->physicalDevice = physicalDevicesScores[0].second.get();


    ///////////
    // Queue //
    ///////////

    // Queue family index
    std::set<uint32_t> uniqueQueueFamilyIndices;
    std::optional<uint32_t> queueFamilyIndex;
    float queuePriority = 0.0f;

    // Transfer queue
    if ((requestedQueueTypes & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
    {
        queueFamilyIndex = reserved->physicalDevice.get().getQueueFamilyIndex(VK_QUEUE_TRANSFER_BIT);
        if (queueFamilyIndex.has_value() == false)
        {
            OV_LOG_ERROR("No transfer queue found in physical device with id= %d.", reserved->physicalDevice.get().getId());
            return false;
        }
        else
        {
            reserved->queueFamilyIndices.transferFamily = queueFamilyIndex.value();
            uniqueQueueFamilyIndices.insert(queueFamilyIndex.value());
            queueFamilyIndex = std::nullopt;
        }
    }

    // Compute queue
    if ((requestedQueueTypes & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
    {
        queueFamilyIndex = reserved->physicalDevice.get().getQueueFamilyIndex(VK_QUEUE_COMPUTE_BIT);
        if (queueFamilyIndex.has_value() == false)
        {
            OV_LOG_ERROR("No compute queue found in physical device with id= %d.", reserved->physicalDevice.get().getId());
            return false;
        }
        else
        {
            reserved->queueFamilyIndices.computeFamily = queueFamilyIndex.value();
            uniqueQueueFamilyIndices.insert(queueFamilyIndex.value());
            queueFamilyIndex = std::nullopt;
        }
    }

    // Graphic queue
    if ((requestedQueueTypes & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
    {
        queueFamilyIndex = reserved->physicalDevice.get().getQueueFamilyIndex(VK_QUEUE_GRAPHICS_BIT);
        if (queueFamilyIndex.has_value() == false)
        {
            OV_LOG_ERROR("No graphic queue found in physical device with id= %d.", reserved->physicalDevice.get().getId());
            return false;
        }
        else
        {
            reserved->queueFamilyIndices.graphicsFamily = queueFamilyIndex.value();
            uniqueQueueFamilyIndices.insert(queueFamilyIndex.value());
            queueFamilyIndex = std::nullopt;
        }
    }

    // Present queue
    queueFamilyIndex = reserved->physicalDevice.get().getPresentQueueFamilyIndex(presentSurface.get());
    if (queueFamilyIndex.has_value() == false)
    {
        OV_LOG_ERROR("No present queue found for the surface with id= %d in physical device with id= %d.",
            presentSurface.get().getId(), reserved->physicalDevice.get().getId());
        return false;
    }
    else
    {
        reserved->queueFamilyIndices.presentFamily = queueFamilyIndex.value();
        uniqueQueueFamilyIndices.insert(queueFamilyIndex.value());
        queueFamilyIndex = std::nullopt;
    }

    // Create queue info struct for create logical device.
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    for (uint32_t familyIndex : uniqueQueueFamilyIndices)
    {
        VkDeviceQueueCreateInfo& queueCreateInfo = queueCreateInfos.emplace_back();
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = familyIndex;
        queueCreateInfo.queueCount = 1;
        // Vulkan lets you assign priorities to queues to influence the scheduling of command buffer execution 
        // using floating point numbers between 0.0 and 1.0
        queueCreateInfo.pQueuePriorities = &queuePriority;
    }


    ///////////////////
    // Loical Device //
    ///////////////////

    std::vector<const char*> requiredExtensions = engine.get().getRequiredDeviceExtensions();

    // Structure describing the fine-grained features that can be supported by an implementation
    VkPhysicalDeviceFeatures2 deviceFeatures2{};
    deviceFeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    deviceFeatures2.features = enabledFeatures;
    deviceFeatures2.pNext = pNextChain;

    // Start filling in the main VkDeviceCreateInfo structure.
    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pNext = &deviceFeatures2;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());;
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    // Previous implementations of Vulkan made a distinction between instance and device specific 
    // validation layers, but this is no longer the case. That means that the enabledLayerCount and
    // ppEnabledLayerNames fields of VkDeviceCreateInfo are ignored by up-to-date implementations.
    createInfo.enabledLayerCount = 0;
    createInfo.ppEnabledLayerNames = nullptr;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
    createInfo.ppEnabledExtensionNames = requiredExtensions.data();
    createInfo.pEnabledFeatures = nullptr;

    // Instantiate the logical device
    if (vkCreateDevice(reserved->physicalDevice.get().getPhysicalDevice(), &createInfo, nullptr,
        &reserved->logicalDevice) != VK_SUCCESS) {
        // can return errors based on enabling non-existent extensions or specifying the desired 
        // usage of unsupported feature
        OV_LOG_ERROR("Failed to initialize logical device with id= %d!", getId());
        this->free();
        return false;
    }

    // Retrieve queue handles for each queue family. 
    // Transfer queue
    if (reserved->queueFamilyIndices.transferFamily.has_value())
        vkGetDeviceQueue(reserved->logicalDevice, reserved->queueFamilyIndices.transferFamily.value(),
            0, &reserved->queueHandles.transferQueue);

    // Compute queue
    if (reserved->queueFamilyIndices.computeFamily.has_value())
        vkGetDeviceQueue(reserved->logicalDevice, reserved->queueFamilyIndices.computeFamily.value(),
            0, &reserved->queueHandles.computeQueue);

    // Graphic queue
    if (reserved->queueFamilyIndices.graphicsFamily.has_value())
        vkGetDeviceQueue(reserved->logicalDevice, reserved->queueFamilyIndices.graphicsFamily.value(),
            0, &reserved->queueHandles.graphicsQueue);

    // Present queue
    if (reserved->queueFamilyIndices.presentFamily.has_value())
        vkGetDeviceQueue(reserved->logicalDevice, reserved->queueFamilyIndices.presentFamily.value(),
            0, &reserved->queueHandles.presentQueue);


    /////////
    // VMA //
    /////////

    VmaAllocatorCreateInfo allocatorInfo = {};
    // which Vulkan version do you use
    allocatorInfo.vulkanApiVersion = VK_API_VERSION_1_3;
    // VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT  => Allocator and all objects created from it will not be
    //      synchronized internally, so you must guarantee they are used from only one thread at a time or
    //      synchronized externally by you.
    // VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT => Enables usage of "buffer device address" feature, which allows
    //      you to use function vkGetBufferDeviceAddress* to get raw GPU pointer to a buffer and pass
    //      it for usage inside a shader.
    allocatorInfo.flags = vmaAllocatorCreateFlag;
    allocatorInfo.instance = vulkanInstance.get().getVkInstance();
    allocatorInfo.physicalDevice = reserved->physicalDevice.get().getPhysicalDevice();
    allocatorInfo.device = reserved->logicalDevice;

    if (vmaCreateAllocator(&allocatorInfo, &reserved->allocator) != VK_SUCCESS) {
        OV_LOG_ERROR(" failed to create VMA allocator in logical device with id= %d.", getId());
        this->free();
        return false;
    }


    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the reference to the Overv::Vulkan physical device use to create the logical device.
 * @return The reference of the Overv::Vulkan physical device used to create the logical device.
 */
const OvVK::PhysicalDevice OVVK_API& OvVK::LogicalDevice::getPhysicalDevice() const
{
    return reserved->physicalDevice;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets Vulkan handle of the logical device.
 * @return The Vulkan handle to the logical device.
 */
VkDevice OVVK_API OvVK::LogicalDevice::getVkDevice() const
{
    return reserved->logicalDevice;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets VMA handle to the allocator.
 * @return The VMA handle to the allocator.
 */
VmaAllocator OVVK_API OvVK::LogicalDevice::getVmaAllocator() const
{
    return reserved->allocator;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets a QueueFamilyIndices structure containing the queues family indices for each type of queue,
 * speficied during logical device creation.
 * @return A structure QueueFamilyIndices containing the queues indices. Null optinal if the logical device is not create. 
 */
std::optional<OvVK::LogicalDevice::QueueFamilyIndices> OVVK_API
OvVK::LogicalDevice::getQueueFamiliesIndices() const
{
    // Safety net
    if (reserved->logicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Logical device with id= %d not created.", getId());
        return std::nullopt;
    }

    // Done:
    return reserved->queueFamilyIndices;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the handle of the queue with the passed index family.
 * @param queueFamilyIndex Index of the queue whose handle is to be retrieve
 * @return Handle to a queue or null if index is not found.
 */
VkQueue OVVK_API OvVK::LogicalDevice::getQueueHandle(uint32_t queueFamilyIndex) const 
{
    if (reserved->queueFamilyIndices.graphicsFamily.has_value() &&
        reserved->queueFamilyIndices.graphicsFamily.value() == queueFamilyIndex) 
    {
        return reserved->queueHandles.graphicsQueue;
    }
    else if (reserved->queueFamilyIndices.transferFamily.has_value() &&
        reserved->queueFamilyIndices.transferFamily.value() == queueFamilyIndex) 
    {
        return reserved->queueHandles.transferQueue;
    }
    else if (reserved->queueFamilyIndices.presentFamily.has_value() &&
        reserved->queueFamilyIndices.presentFamily.value() == queueFamilyIndex)
    {
        return reserved->queueHandles.presentQueue;
    }
    else if (reserved->queueFamilyIndices.computeFamily.has_value() &&
        reserved->queueFamilyIndices.computeFamily.value() == queueFamilyIndex) 
    {
        return reserved->queueHandles.computeQueue;
    }


    // Done:
    return VK_NULL_HANDLE;
}

/**
 * Gets a QueueHandles structure containing the queues handles for each type of queue,
 * speficied during logical device creation.
 * @return A structure QueueHandles containing the queues handles. Null optinal if the logical device is not create.
 */
std::optional<OvVK::LogicalDevice::QueueHandles> OVVK_API OvVK::LogicalDevice::getQueueHandles() const
{
    // Safety net
    if (reserved->logicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Logical device not created.");
        return std::nullopt;
    }

    // Done:
    return reserved->queueHandles;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the function pointer to rate a physical device.
 * @param ratePhysicalDevice Function poiter
 * @return TF
 */
bool OVVK_API OvVK::LogicalDevice::setFuncRatePhysicalDevice(RatePhysicalDevice ratePhysicalDevice)
{
    reserved->pointerRatePhysicalDevice = ratePhysicalDevice;

    // Done:
    return true;
}

#pragma endregion