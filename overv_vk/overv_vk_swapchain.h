#pragma once
/**
 * @file	overv_vk_swapchain.h
 * @brief	Vulkan swapchain properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Class to modeling a Vulkan swapchain.
  */
class OVVK_API Swapchain final : public Ov::Object, public Ov::Managed, public OvVK::SurfaceDependentObject
{
//////////
public: //
//////////

	// Static
	static Swapchain empty;

	// Const/dest:
	Swapchain();
	Swapchain(Swapchain&& other);
	Swapchain(Swapchain const&) = delete;
	~Swapchain();

	// Operators:
	Swapchain& operator=(const Swapchain&) = delete;
	Swapchain& operator=(Swapchain&&) = delete;

	// Managed
	bool init() override;
	bool free() override;

	// SurfaceDependentObject override method
	virtual bool windowSizeCallback(int width, int height) override;

	// General
	bool create(uint32_t width, uint32_t height);
	bool acquireNextImage(uint32_t& imageIndex);

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool presentImage(uint32_t imageIndex, std::vector<VkSemaphore> waitSemaphores = {});
#endif

	// Get/Set
	bool isResizing() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkSwapchainKHR getSwapchain() const;
	std::vector<VkImage> getSwapchainImages() const;
	std::vector<VkImageView> getSwapchainImageViews() const;
	VkImage getSwapchainImage(uint32_t imageIndex) const;
	VkImageView getSwapchainImageView(uint32_t imageIndex) const;
	std::optional<uint32_t> getSwapchainImageOwnership(uint32_t imageIndex) const;
	std::optional<VkImageLayout> getSwapchainImageLayout(uint32_t imageIndex) const;
	uint32_t getNrOfSwapchainImages() const;

	VkFormat getSwapchainImageFormat() const;
	VkExtent2D getSwapChainExtent() const;

	VkSemaphore getImageAvailableSyncObjGPU(uint32_t imageIndex) const;
	VkFence getImageAvailableSyncObjCPU(uint32_t imageIndex) const;
	VkSemaphore getPresentationSyncObjGPU(uint32_t imageIndex) const;
	VkFence getPresentationSyncObjCPU(uint32_t imageIndex) const;
#endif

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Are used inside the render method that is constant
	bool setSwapchainImageOwnership(uint32_t imageIndex, uint32_t queueFamilyIndex) const;
	bool setSwapchainImageLayout(uint32_t imageIndex, VkImageLayout imageLayout) const;

	// SwapChain rate specification
	static std::vector<std::pair<uint32_t, VkSurfaceFormatKHR>> rateSwapchainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	static std::vector<std::pair<uint32_t, VkPresentModeKHR>> rateSwapchainPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
	static VkExtent2D chooseSwapchainExtent(const VkSurfaceCapabilitiesKHR& capabilities, uint32_t width, uint32_t height);
	static VkCompositeAlphaFlagBitsKHR chooseSwapchainSurfaceCompositeAlpha(const VkCompositeAlphaFlagsKHR& availableFormats);
#endif

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Swapchain(const std::string& name);
};