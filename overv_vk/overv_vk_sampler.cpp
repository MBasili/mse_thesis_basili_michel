/**
 * @file	overv_vk_sampler.cpp
 * @brief	Vulkan sampler
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/////////////
// Include //
/////////////

// Main include
#include "overv_vk.h"


////////////
// Static //
////////////

// Special values:
OvVK::Sampler OvVK::Sampler::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

#pragma region Structures

/**
 * @brief Sampler class reserved structure.
 */
struct OvVK::Sampler::Reserved
{
    VkSampler sampler;
    VkSamplerCreateInfo samplerInfo;

    /**
     * Constructor
     */
    Reserved() : sampler{ VK_NULL_HANDLE },
        samplerInfo{ {} }
    {}
};

#pragma endregion


///////////////////////////
// BODY OF CLASS Sampler //
///////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Sampler::Sampler() : reserved(std::make_unique<OvVK::Sampler::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::Sampler::Sampler(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::Sampler::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Sampler::Sampler(Sampler&& other) : Ov::Object(std::move(other)), Ov::Managed(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Sampler::~Sampler()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes an Vulkan sampler.
 * @return TF
 */
bool OVVK_API OvVK::Sampler::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Destroy sampler
    if (reserved->sampler != VK_NULL_HANDLE) 
    {
        vkDestroySampler(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->sampler, NULL);
        reserved->sampler = VK_NULL_HANDLE;
    }


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases an Vulkan sampler.
 * @return TF
 */
bool OVVK_API OvVK::Sampler::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Destroy sampler
    if (reserved->sampler != VK_NULL_HANDLE)
    {
        vkDestroySampler(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->sampler, NULL);
        reserved->sampler = VK_NULL_HANDLE;
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region Default

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the default generic sampler.
 * @return The OvVK sampler obj.
 */
const OvVK::Sampler OVVK_API& OvVK::Sampler::getDefault()
{
    static Sampler defaultSampler;

    if (!defaultSampler.isInitialized())
    {
        if (defaultSampler.create(OvVK::Sampler::getDefaultSamplerCreateInfo()) == false)
            OV_LOG_ERROR("Fail to create default sampler.");
    }

    return defaultSampler;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the sampler
 * @param samplerInfo The structure specifing the properties of the sampler.
 * @return TF
 */
bool OVVK_API OvVK::Sampler::create(const VkSamplerCreateInfo samplerInfo)
{
    // Al the value are present in the engine class. If not presente the engine doesn't start.
    std::reference_wrapper<const Engine> engine = Engine::getInstance();
    // Retrive logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Physical device
    std::reference_wrapper<const OvVK::PhysicalDevice> physicalDevice = logicalDevice.get().getPhysicalDevice();

    // Safety net:
    // Get features of the connected device
    std::optional<VkPhysicalDeviceFeatures2> deviceFeatures2 = physicalDevice.get().getFeatures();
    if (deviceFeatures2.has_value() == false) {
        OV_LOG_ERROR("Fail to retriver physical device features in the sampler with id= %d", getId());
        return false;
    }

    // Check for Anisotropy device feature support
    if (!deviceFeatures2.value().features.samplerAnisotropy) {
        OV_LOG_ERROR("The physical device doesn't support Anisotropy.");
        return false;
    }

    // Init
    if (this->init() == false)
        return false;

    // Create sampler
    if (vkCreateSampler(logicalDevice.get().getVkDevice(), &samplerInfo, nullptr, &reserved->sampler) != VK_SUCCESS) {
        OV_LOG_ERROR("Fail to create the sampler with id= %d.", getId());
        this->free();
        return false;
    }

    reserved->samplerInfo = samplerInfo;

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan sampler handle 
 * @return The Vulkan sampler handle 
 */
VkSampler OVVK_API OvVK::Sampler::getVkSampler() const 
{
    return reserved->sampler;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the mag Vulkan enum filter used for texture lookups
 * @return The mag Vulkan enum filter used for texture lookups
 */
VkFilter OVVK_API OvVK::Sampler::getMagFilter() const 
{
    return reserved->samplerInfo.magFilter;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the min Vulkan enum filter used for texture lookups
 * @return The min Vulkan enum filter used for texture lookups
 */
VkFilter OVVK_API OvVK::Sampler::getMinFilter() const 
{
    return reserved->samplerInfo.minFilter;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan enum specify behavior of sampling with texture coordinates u outside an image
 * @return The Vulkan enum specify behavior of sampling with texture coordinates u outside an image
 */
VkSamplerAddressMode OVVK_API OvVK::Sampler::getAddressModeU() const
{
    return reserved->samplerInfo.addressModeU;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan enum specify behavior of sampling with texture coordinates v outside an image
 * @return The Vulkan enum specify behavior of sampling with texture coordinates v outside an image
 */
VkSamplerAddressMode OVVK_API OvVK::Sampler::getAddressModeV() const
{
    return reserved->samplerInfo.addressModeV;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan enum specify behavior of sampling with texture coordinates w outside an image
 * @return The Vulkan enum specify behavior of sampling with texture coordinates w outside an image
 */
VkSamplerAddressMode OVVK_API OvVK::Sampler::getAddressModeW() const
{
    return reserved->samplerInfo.addressModeW;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the Anisotropy filter is enable.
 * @return TF
 */
bool OVVK_API OvVK::Sampler::isAnisotropyEnabled() const 
{
    return static_cast<bool>(reserved->samplerInfo.anisotropyEnable);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the anisotropy value clamp used by the sampler
 * @return The anisotropy value clamp used by the sampler
 */
float OVVK_API OvVK::Sampler::getMaxAnisotropy() const 
{
    return reserved->samplerInfo.maxAnisotropy;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the predefined border color to use.
 * @return The Vulkan enum with the predefined border color to use.
 */
VkBorderColor OVVK_API OvVK::Sampler::getBorderColor() const 
{
    return reserved->samplerInfo.borderColor;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if unormalized texel coordinates to address texels of the image are enable.
 * @return TF
 */
bool OVVK_API OvVK::Sampler::isUnnormalizedCoordinatesEnbled() const 
{
    return static_cast<bool>(reserved->samplerInfo.unnormalizedCoordinates);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if comparison against a reference value during lookups is enable.
 * @return TF
 */
bool OVVK_API OvVK::Sampler::isCompareEnabled() const 
{
    return static_cast<bool>(reserved->samplerInfo.compareEnable);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan enum specifying the comparison operator to apply to fetched data before filtering.
 * @return The Vulkan enum specifying the comparison operator to apply to fetched data before filtering.
 */
VkCompareOp OVVK_API OvVK::Sampler::getCompareOperation() const 
{
    return reserved->samplerInfo.compareOp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan enum specifying the mipmap filter to apply to lookups.
 * @return The Vulkan enum specifying the mipmap filter to apply to lookups.
 */
VkSamplerMipmapMode OVVK_API OvVK::Sampler::getMipmapMode() const 
{
    return reserved->samplerInfo.mipmapMode;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the min level of details value.
 * @return The min level of details value.
 */
float OVVK_API OvVK::Sampler::getMinLevelOfDetail() const
{
    return reserved->samplerInfo.minLod;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the max level of details value.
 * @return The max level of details value.
 */
float OVVK_API OvVK::Sampler::getMaxLevelOfDetail() const 
{
    return reserved->samplerInfo.maxLod;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the bias to be added to mipmap LOD(level-of-detail) calculation
 * @return The bias to be added to mipmap LOD(level-of-detail) calculation
 */
float OVVK_API OvVK::Sampler::getMipLevelOfDetailBias() const 
{
    return reserved->samplerInfo.mipLodBias;
}

#pragma endregion

#pragma region Static

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan struct that can be use to create a default sampler.
 * @return The Vulkan struct with default parameters
 */
VkSamplerCreateInfo OVVK_API OvVK::Sampler::getDefaultSamplerCreateInfo()
{
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.pNext = NULL;
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    // The magFilter and minFilter fields specify how to interpolate texels that are magnified or minified. 
    // The choices are VK_FILTER_NEAREST and VK_FILTER_LINEAR
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    // The addressing mode can be specified per axis using the addressMode fields. 
    // The available values are listed below.
    // Note that the axes are called U, V and W instead of X, Y and Z. 
    // This is a convention for texture space coordinates.
    // - VK_SAMPLER_ADDRESS_MODE_REPEAT: Repeat the texture when going beyond the image dimensions.
    // - VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT : Like repeat, but inverts the coordinates to mirror the image when going beyond the dimensions.
    // - VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE : Take the color of the edge closest to the coordinate beyond the image dimensions.
    // - VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE : Like clamp to edge, but instead uses the edge opposite to the closest edge.
    // - VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER : Return a solid color when sampling beyond the dimensions of the image.
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

    // These two fields specify if anisotropic filtering should be used.
    // There is no reason not to use this unless performance is a concern.
    std::optional<VkPhysicalDeviceFeatures2> features = 
        Engine::getInstance().getLogicalDevice().getPhysicalDevice().getFeatures();
    samplerInfo.anisotropyEnable = features.value().features.samplerAnisotropy;

    // The maxAnisotropy field limits the amount of texel samples that can be used to calculate the 
    // final color. A lower value results in better performance, but lower quality results. 
    // To figure out which value we can use, we need to retrieve the properties of the physical device
    std::optional<VkPhysicalDeviceProperties2> properties =
        Engine::getInstance().getLogicalDevice().getPhysicalDevice().getProperties();

    // Not used in raytracing
    samplerInfo.maxAnisotropy = properties.value().properties.limits.maxSamplerAnisotropy;

    // The borderColor field specifies which color is returned when sampling beyond the image with clamp
    // to border addressing mode. It is possible to return black, white or transparent in either float
    // or int formats. You cannot specify an arbitrary color.
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;

    // The unnormalizedCoordinates field specifies which coordinate system you want to use to address texels
    // in an image. If this field is VK_TRUE, then you can simply use coordinates within the [0, texWidth) 
    // and [0, texHeight) range. If it is VK_FALSE, then the texels are addressed using the [0, 1) range 
    // on all axes. Real-world applications almost always use normalized coordinates, because then
    // it's possible to use textures of varying resolutions with the exact same coordinates.
    samplerInfo.unnormalizedCoordinates = VK_FALSE;

    // If a comparison function is enabled, then texels will first be compared to a value, 
    // and the result of that comparison is used in filtering operations. 
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

    // All of these fields apply to mipmapping. 
    // Vulkan allows us to specify minLod, maxLod, mipLodBias, and mipmapMode ("Lod" means "Level of Detail"). 
    // If samplerInfo.mipmapMode is VK_SAMPLER_MIPMAP_MODE_NEAREST, lod selects the mip level to sample from. 
    // If the mipmap mode is VK_SAMPLER_MIPMAP_MODE_LINEAR, lod is used to select two mip levels to be sampled.
    // Those levels are sampled and the results are linearly blended.
    // If the object is close to the camera, magFilter is used as the filter. 
    // If the object is further from the camera, minFilter is used. 
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    // To allow the full range of mip levels to be used, we set minLod to 0.0f, and maxLod to the number
    // of mip levels. We have no reason to change the lod value , so we set mipLodBias to 0.0f.
    samplerInfo.minLod = 0.0f; // Optional
    samplerInfo.maxLod = 1.0f;
    // mipLodBias lets us force Vulkan to use lower lod and level than it would normally use.
    samplerInfo.mipLodBias = 0.0f; // Optional

    return samplerInfo;
}

#pragma endregion