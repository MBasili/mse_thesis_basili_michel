#pragma once
/**
 * @file	overv_vk_command_buffer.h
 * @brief	Vulkan command buffer properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/////////////////////////
// FORWARD DECLARATION //
/////////////////////////

// forward declaration
typedef class CommandPool;

 /**
  * @brief Class to represent Vulkan command buffer
  */
class OVVK_API CommandBuffer final : public Ov::Object
{
//////////
public: //
//////////

	// Static
	static CommandBuffer empty;

	// Sync obj value:
	constexpr static uint64_t defaultValueStatusSyncObj = 0;
	constexpr static uint64_t recordingValueStatusSyncObj = 1;
	constexpr static uint64_t readyValueStatusSyncObj = 2;

	// Const/dest:
	CommandBuffer();
	CommandBuffer(CommandBuffer&& other) noexcept;
	CommandBuffer(CommandBuffer const&) = delete;
	virtual ~CommandBuffer();

	// Operators
	CommandBuffer& operator=(const CommandBuffer&) = delete;
	CommandBuffer& operator=(CommandBuffer&&) = delete;

	// Sync Obj.
	bool isLock() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool beginVkCommandBuffer(VkCommandBufferBeginInfo commandBufferBeginInfo);
	bool endVkCommandBuffer();
	bool resetVkCommandBuffer(VkCommandBufferResetFlags flags = VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

	// Get/set:
	VkCommandBuffer getVkCommandBuffer() const;
	VkFence getLockSyncObj() const;
	VkSemaphore getStatusSyncObj() const;
	std::optional<uint32_t> getQueueFamilyIndex() const;
	VkCommandBufferUsageFlags getVkCommandBufferUsageFlags() const;

	bool setStatusSyncObjValue(uint64_t value);
	bool setLockSyncObj(VkFence fence);
#endif

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	CommandBuffer(const std::string& name);

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool allocate(CommandPool& commandPool, VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
#endif

	bool free();

	friend class CommandPool;
};
