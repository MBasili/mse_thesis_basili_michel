/**
 * @file	overv_vk_physical_device.cpp
 * @brief	Vulkan physical device properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values
OvVK::PhysicalDevice OvVK::PhysicalDevice::empty("empty");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Physical device class reserved structure.
 */
struct OvVK::PhysicalDevice::Reserved
{
    VkPhysicalDevice physicalDevice;                                    ///< Opaque handle to a physical device object.

    /**
     * Constructor.
     */
    Reserved() : physicalDevice { VK_NULL_HANDLE }
    {}
};


//////////////////////////////////
// BODY OF CLASS PhysicalDevice //
//////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::PhysicalDevice::PhysicalDevice() :
    reserved(std::make_unique<OvVK::PhysicalDevice::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The physicalDevice name.
 */
OVVK_API OvVK::PhysicalDevice::PhysicalDevice(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::PhysicalDevice::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::PhysicalDevice::PhysicalDevice(PhysicalDevice&& other) : Ov::Object(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::PhysicalDevice::~PhysicalDevice()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Connect the obj to a Vulkan physical device.
 * @param physicalDevice Opaque Vulkan handle to a physical device object.
 * @return TF
 */
bool OVVK_API OvVK::PhysicalDevice::connect(VkPhysicalDevice physicalDevice)
{
    // Safety net:
    if (reserved->physicalDevice != VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Already connected to a physical device.");
        return false;
    }

    if (physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The passed physical device is not valid.");
        return false;
    }

    reserved->physicalDevice = physicalDevice;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Check if the passed physical device extension name is supported.
 * @param extension Name of the extension to check.
 * @return TF
 */
bool OVVK_API OvVK::PhysicalDevice::isExtensionSupported(std::string extension) const
{
    std::vector<std::string> supportedExtensions = getSupportedExtensions();

    return (std::find(supportedExtensions.begin(), supportedExtensions.end(), extension)
        != supportedExtensions.end());
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the Vulkan physical device connected.
 * @return Opaque Vulkan handle to a physical device object.
 */
VkPhysicalDevice OVVK_API OvVK::PhysicalDevice::getPhysicalDevice() const 
{
    return reserved->physicalDevice;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the properties of the physical device connected.
 * @param pNext Pointer to a structure extending VkPhysicalDeviceProperties2.
 * @return Optional std object containing the properties of the connected physical
 * device if present.
 */
std::optional<VkPhysicalDeviceProperties2> OVVK_API OvVK::PhysicalDevice::getProperties(void* pNext) const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE) 
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

	// Device properties also contain limits and sparse properties
	VkPhysicalDeviceProperties2 physicalDeviceProperties;
    physicalDeviceProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
	physicalDeviceProperties.pNext = pNext;
	vkGetPhysicalDeviceProperties2(reserved->physicalDevice, &physicalDeviceProperties);

    // Done:
    return physicalDeviceProperties;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the features of the physical device connected.
 * @param pNext Pointer to a structure extending VkPhysicalDeviceFeatures2.
 * @return Optional std object containing the features of the connected physical
 * device if present.
 */
std::optional<VkPhysicalDeviceFeatures2> OVVK_API OvVK::PhysicalDevice::getFeatures(void* pNext) const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

	// Device properties also contain limits and sparse properties
    VkPhysicalDeviceFeatures2 physicalDeviceFeatures = {};
    physicalDeviceFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    physicalDeviceFeatures.pNext = pNext;
	vkGetPhysicalDeviceFeatures2(reserved->physicalDevice, &physicalDeviceFeatures);

    // Done:
    return physicalDeviceFeatures;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the properties of the passed format on the connected physical device.
 * @param format Format on which to collect properties.
 * @param pNext Pointer to a structure extending VkFormatProperties2.
 * @return Optional std object containing the properties of the passed format on the
 * connected physical device.
 */
std::optional<VkFormatProperties2> OVVK_API OvVK::PhysicalDevice::getFormatProperties(VkFormat format,
    void* pNext) const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

    VkFormatProperties2 formatProperties2 = {};
    formatProperties2.sType = VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2;
    formatProperties2.pNext = pNext;

    // Lists physical device's format capabilities
    vkGetPhysicalDeviceFormatProperties2(reserved->physicalDevice, format, &formatProperties2);

    return formatProperties2;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the format properties of the passed image creation parameters structure. This method checks if the parameters 
 * and their combination in the image creation structure are supported.
 * @param physicalDeviceImageFormatInfo2 Pointer to a structure specifying image creation parameters.
 * @param pNext Pointer to a structure extending VkImageFormatProperties2.
 * @return Optional std object containing the properties of the passed format on the
 * connected physical device.
 */
std::optional<VkImageFormatProperties2> OVVK_API OvVK::PhysicalDevice::getImageFormatProperties(
    VkPhysicalDeviceImageFormatInfo2* physicalDeviceImageFormatInfo2,
    void* pNext) const 
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

    VkImageFormatProperties2 imageFormatProperties;
    imageFormatProperties.sType = VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2;
    imageFormatProperties.pNext = pNext;

    VkResult supportedResult = vkGetPhysicalDeviceImageFormatProperties2(reserved->physicalDevice,
        physicalDeviceImageFormatInfo2, &imageFormatProperties);

    // Check result
    switch (supportedResult)
    {
    case VK_SUCCESS:
        break;
    case VK_ERROR_FORMAT_NOT_SUPPORTED:
        OV_LOG_ERROR("The format used is not supported, or the combination of format, type, tiling, usage, and flags is not valid for images.");
        return std::nullopt;
        break;
    default:
        OV_LOG_ERROR("Error in checking whether the image format is supported.");
        return std::nullopt;
        break;
    };

    return imageFormatProperties;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get queues families properties of the connected physical device.
 * @param pNext Pointer to a structure extending VkQueueFamilyProperties2.
 * @return Vector containing all the properties of the connected physical device
 * queues families.
 */
std::vector<VkQueueFamilyProperties2> OVVK_API
    OvVK::PhysicalDevice::getQueueFamilyProperties(void* pNext) const
{
    std::vector<VkQueueFamilyProperties2> queuesFamiliesProperties2;

    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return queuesFamiliesProperties2;
    }

	// Queue family properties, used for setting up requested queues upon device creation
	uint32_t queueFamilyCount;
	vkGetPhysicalDeviceQueueFamilyProperties2(reserved->physicalDevice, &queueFamilyCount, nullptr);

	if (queueFamilyCount <= 0)
	{
		OV_LOG_INFO("No queues families found in the connected physical device");
		return queuesFamiliesProperties2;
	}

    queuesFamiliesProperties2.resize(queueFamilyCount);
    for (uint32_t c = 0; c < queueFamilyCount; c++) 
    {
        queuesFamiliesProperties2[c].sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
        queuesFamiliesProperties2[c].pNext = pNext;
    }

	vkGetPhysicalDeviceQueueFamilyProperties2(reserved->physicalDevice, &queueFamilyCount,
        queuesFamiliesProperties2.data());

    // Done:
    return queuesFamiliesProperties2;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get connected physical device supported extensions.
 * @return Vector containing the name fo the supported extensions.
 */
std::vector<std::string> OVVK_API OvVK::PhysicalDevice::getSupportedExtensions() const
{
    std::vector<std::string> supportedExtensions;

    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return supportedExtensions;
    }

	// Get list of supported extensions
	uint32_t extCount = 0;
	if (vkEnumerateDeviceExtensionProperties(reserved->physicalDevice, nullptr, &extCount, nullptr)
		!= VK_SUCCESS)
	{
		OV_LOG_ERROR("Could not get the properties of the extensions.");
		return supportedExtensions;
	}

	if (extCount <= 0)
	{
		OV_LOG_INFO("No supported extension found in the connected physical device.");
		return supportedExtensions;
	}

	std::vector<VkExtensionProperties> extensions(extCount);
	if (vkEnumerateDeviceExtensionProperties(reserved->physicalDevice, nullptr, &extCount, &extensions.front())
		!= VK_SUCCESS)
	{
		OV_LOG_ERROR("Could not get the properties of the extensions.");
		return supportedExtensions;
	}

	for (auto &ext : extensions)
		supportedExtensions.push_back(ext.extensionName);

    // Done:
    return supportedExtensions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the queue family index with the passed capabilities.
 * @param queueFlags Bitmask specifying capabilities of queues in a queue family.
 * @return Optional std obj containing the index of the queue family suited for the passed capabilities.
 */
std::optional<uint32_t> OVVK_API OvVK::PhysicalDevice::getQueueFamilyIndex(VkQueueFlags queueFlags) const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

    // Get properties of the queue family
    std::vector<VkQueueFamilyProperties2> queuesFamiliesProperties = getQueueFamilyProperties();

    // Dedicated queue for compute
    // Try to find a queue family index that supports compute but not graphics (dedicate queue)
    if (queueFlags == VK_QUEUE_COMPUTE_BIT)
    {
        for (uint32_t i = 0; i < static_cast<uint32_t>(queuesFamiliesProperties.size()); i++)
        {
            if ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & queueFlags) == queueFlags
                && ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0))
            {
                return i;
            }
        }
    }

    // Dedicated queue for graphics
    // Try to find a queue family index that supports graphics but not compute (dedicate queue)
    if (queueFlags == VK_QUEUE_GRAPHICS_BIT)
    {
        for (uint32_t i = 0; i < static_cast<uint32_t>(queuesFamiliesProperties.size()); i++)
        {
            if ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & queueFlags) == queueFlags
                && ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) == 0))
            {
                return i;
            }
        }
    }

    // Dedicated queue for transfer
    // Try to find a queue family index that supports transfer but not graphics and compute (dedicate queue)
    // Queue family with VK_QUEUE_GRAPHICS_BIT or VK_QUEUE_COMPUTE_BIT capabilities already implicitly
    // support VK_QUEUE_TRANSFER_BIT operations.
    if (queueFlags == VK_QUEUE_TRANSFER_BIT)
    {
        for (uint32_t i = 0; i < static_cast<uint32_t>(queuesFamiliesProperties.size()); i++)
        {
            if ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & queueFlags) == queueFlags
                && ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0)
                && ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) == 0))
            {
                return i;
            }
        }
    }

    // For other queue types or if no separate compute queue is present, return the first one to support the requested flags
    for (uint32_t i = 0; i < static_cast<uint32_t>(queuesFamiliesProperties.size()); i++)
    {
        if ((queuesFamiliesProperties[i].queueFamilyProperties.queueFlags & queueFlags) == queueFlags) 
        {
            return i;
        }
    }

    // Done:
    OV_LOG_WARN("No queue family found.");
    return std::nullopt;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the queue family index that support the presentation on the passed surface.
 * @param presentSurface The VkSurfaceKHR where the swapchain's frames are presented.
 * @return Optional std obj containing the index of the queue family used to present swapchain's frames on the passed surface.
 */
std::optional<uint32_t> OVVK_API OvVK::PhysicalDevice::getPresentQueueFamilyIndex(
    const OvVK::Surface &presentSurface) const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

    if (presentSurface == OvVK::Surface::empty)
    {
        OV_LOG_ERROR("The empty OvVK surface is not valid.");
        return std::nullopt;
    }

    VkSurfaceKHR vkPresentSurface = presentSurface.getVkSurfaceKHR();
    if (vkPresentSurface == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The passed surface is invalid.");
        return std::nullopt;
    }

    // Get properties of the queue family
    std::vector<VkQueueFamilyProperties2> queuesFamiliesProperties = getQueueFamilyProperties();
	// Presentation Queue
	for (uint32_t i = 0; i < static_cast<uint32_t>(queuesFamiliesProperties.size()); i++)
	{
		VkBool32 presentSupport = false;
		if (vkGetPhysicalDeviceSurfaceSupportKHR(reserved->physicalDevice, i, vkPresentSurface, &presentSupport)
			!= VK_SUCCESS)
		{
			OV_LOG_ERROR("Fail to check if a queue support the passed presentation surface.");
		}

		if (presentSupport)
            return i;
	}

    // Done:
    OV_LOG_WARN("No queue family found.");
    return std::nullopt;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the maximum number of samples supported for a pixel.
 * @return Optional std obj containing number of supported samples
 */
std::optional<VkSampleCountFlags> OVVK_API OvVK::PhysicalDevice::getMaxUsableSampleCount() const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

    std::optional<VkPhysicalDeviceProperties2> physicalDeviceProperties = getProperties();
    if (physicalDeviceProperties.has_value() == false)
        return std::nullopt;

    // Have to take into account the sample count for both color and depth. 
    // The highest sample count that is supported by both (&) will be the maximum we can support. 
    VkSampleCountFlags counts = physicalDeviceProperties.value().properties.limits.framebufferColorSampleCounts
        & physicalDeviceProperties.value().properties.limits.framebufferDepthSampleCounts;

    if ((counts & VK_SAMPLE_COUNT_64_BIT) == VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
    else if ((counts & VK_SAMPLE_COUNT_32_BIT) == VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
    else if ((counts & VK_SAMPLE_COUNT_16_BIT) == VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
    else if ((counts & VK_SAMPLE_COUNT_8_BIT) == VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
    else if ((counts & VK_SAMPLE_COUNT_4_BIT) == VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
    else if ((counts & VK_SAMPLE_COUNT_2_BIT) == VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }
    else return VK_SAMPLE_COUNT_1_BIT;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the parameters that the swapchain support. 
 * @param presentSurface The VkSurfaceKHR that need to be supported from a swapchain.
 * @return Optional std obj containing a structure storing supported parameters.
 */
std::optional<OvVK::PhysicalDevice::SwapChainSupportedParameters> OVVK_API 
    OvVK::PhysicalDevice::getSwapChainSupportInfo(const OvVK::Surface& presentSurface) const
{
    // Safety net:
    if (reserved->physicalDevice == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("No physical device connected.");
        return std::nullopt;
    }

    if (presentSurface == OvVK::Surface::empty)
    {
        OV_LOG_ERROR("The empty OvVK surface is not valid.");
        return std::nullopt;
    }

    VkSurfaceKHR vkPresentSurface = presentSurface.getVkSurfaceKHR();
    if (vkPresentSurface == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The passed surface is invalid.");
        return std::nullopt;
    }

    SwapChainSupportedParameters swapChainSupportedParameters;


    // Query surface capabilities
    if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(reserved->physicalDevice, vkPresentSurface,
        &swapChainSupportedParameters.capabilities) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Could not get the surface capabilities from the physical device.");
        return std::nullopt;
    }


    // Querying the supported surface formats.
    uint32_t formatCount;
    if (vkGetPhysicalDeviceSurfaceFormatsKHR(reserved->physicalDevice, vkPresentSurface, &formatCount, nullptr)
        != VK_SUCCESS)
    {
        OV_LOG_ERROR("Could not get the surface formats from the physical device.");
        return std::nullopt;
    }

    if (formatCount <= 0)
    {
        OV_LOG_ERROR("No formats surface found.");
        return std::nullopt;
    }

    swapChainSupportedParameters.formats.resize(formatCount);
    if (vkGetPhysicalDeviceSurfaceFormatsKHR(reserved->physicalDevice, vkPresentSurface, &formatCount,
        swapChainSupportedParameters.formats.data()) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Could not get the surface formats from the physical device.");
        return std::nullopt;
    }


    // Query supported presentation modes
    uint32_t presentModeCount;
    if (vkGetPhysicalDeviceSurfacePresentModesKHR(reserved->physicalDevice, vkPresentSurface, &presentModeCount, nullptr)
        != VK_SUCCESS)
    {
        OV_LOG_ERROR("Could not get the surface presentation modes from the physical device.");
        return std::nullopt;
    }

    if (presentModeCount <= 0)
    {
        OV_LOG_ERROR("No presentation modes surface found.");
        return std::nullopt;
    }

    swapChainSupportedParameters.presentModes.resize(presentModeCount);
    if (vkGetPhysicalDeviceSurfacePresentModesKHR(reserved->physicalDevice, vkPresentSurface, &presentModeCount,
        swapChainSupportedParameters.presentModes.data()) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Could not get the surface presentation modes from the physical device.");
        return std::nullopt;
    }


    // Done:
    return swapChainSupportedParameters;
}

#pragma endregion

#pragma region Static

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Default function to rate a physical device.
 * @param physicalDevice OvVK Physical device handle.
 * @return Score of the physical device passed.
 */
uint32_t OVVK_API OvVK::PhysicalDevice::defaultRatePhysicalDevice(const OvVK::PhysicalDevice& physicalDevice) 
{
    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // Surface
    std::reference_wrapper<const OvVK::Surface> presentSurface = engine.get().getPresentSurface();

    // Safety net
    if (physicalDevice.getPhysicalDevice() == VK_NULL_HANDLE) 
    {
        OV_LOG_ERROR("The passed physical device contains a null VkPhysicalDevice.");
        return 0;
    }

    if (presentSurface.get().getVkSurfaceKHR() == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The engine VkSurfaceKHR is null.");
        return 0;
    }

    // Get properties of the connected device
    std::optional<VkPhysicalDeviceProperties2> physicalDeviceProperties = physicalDevice.getProperties();
    if (physicalDeviceProperties.has_value() == false)
        return 0;

    //////////////////////////
    // Rate physical device //
    //////////////////////////

    uint32_t score = 0;

    VkPhysicalDeviceVulkan13Features physicalDeviceVulkan13Features = {};
    physicalDeviceVulkan13Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;

    VkPhysicalDeviceVulkan12Features physicalDeviceVulkan12Features = {};
    physicalDeviceVulkan12Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    physicalDeviceVulkan12Features.pNext = &physicalDeviceVulkan13Features;

    VkPhysicalDeviceVulkan11Features physicalDeviceVulkan11Features = {};
    physicalDeviceVulkan11Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
    physicalDeviceVulkan11Features.pNext = &physicalDeviceVulkan12Features;

    // Ray tracing structs

    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayTracingPipelineFeaturesKHR.html
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR rayTracingPipelineFeatures{
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR };
    rayTracingPipelineFeatures.pNext = &physicalDeviceVulkan11Features;

    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceAccelerationStructureFeaturesKHR.html
    VkPhysicalDeviceAccelerationStructureFeaturesKHR accelerationStructureFeatures{
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR };
    // pNext is NULL or a pointer to a structure extending this structure.
    accelerationStructureFeatures.pNext = &rayTracingPipelineFeatures;

    // Get features of the connected device
    std::optional<VkPhysicalDeviceFeatures2> deviceFeatures2 = physicalDevice.getFeatures(&accelerationStructureFeatures);
    if (deviceFeatures2.has_value() == false)
        return 0;


    ////////////
    // Rating //
    ////////////

    // Check features required by raytracing (needed)
    if (!physicalDeviceVulkan12Features.bufferDeviceAddress || !rayTracingPipelineFeatures.rayTracingPipeline
        || !accelerationStructureFeatures.accelerationStructure
        || !accelerationStructureFeatures.accelerationStructureCaptureReplay) {

        OV_LOG_INFO("The physical device doesn't support RayTracing needed functionality.");
        return 0;
    }

	// Check if the update of descriptorset binded are avaiallable (needed for tectures)
	if (physicalDeviceVulkan12Features.descriptorBindingSampledImageUpdateAfterBind != VK_TRUE ||
	    physicalDeviceVulkan12Features.descriptorBindingStorageBufferUpdateAfterBind != VK_TRUE ||
	    physicalDeviceVulkan12Features.descriptorBindingStorageImageUpdateAfterBind != VK_TRUE ||
	    physicalDeviceVulkan12Features.descriptorBindingUniformBufferUpdateAfterBind != VK_TRUE)
	{
        OV_LOG_INFO("Update binded descriptorSet not possible.");
	    return 0;
	}

    // Check if synchronization 2 is supported
    if (physicalDeviceVulkan12Features.timelineSemaphore != VK_TRUE ||
        physicalDeviceVulkan13Features.synchronization2 != VK_TRUE)
    {
        OV_LOG_INFO("The synchronization2 not supported.");
        return 0;
    }

    // Need to find suitables queues to send commands.
    VkQueueFlags queueFamiliesCom =  VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT;
    VkQueueFlags queueFamiliesGra = VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT;
    std::optional<uint32_t> queueIndexCom = physicalDevice.getQueueFamilyIndex(queueFamiliesCom);
    std::optional<uint32_t> queueIndexGra = physicalDevice.getQueueFamilyIndex(queueFamiliesGra);
    if (queueIndexCom.has_value() == false || queueIndexGra.has_value() == false)
        return 0;

    std::optional<uint32_t> queueIndex = physicalDevice.getPresentQueueFamilyIndex(presentSurface);
    if (queueIndex.has_value() == false)
        return 0;

    // Check if the device support the needed extensions
    for (const char* requiredDeviceExtension : engine.get().getRequiredDeviceExtensions())
        if (!physicalDevice.isExtensionSupported(requiredDeviceExtension))
        {
            OV_LOG_INFO("The physical device doesn't support the %s extension", requiredDeviceExtension);
            return 0;
        }

    // Check for Anisotropy device feature support
    if (deviceFeatures2.value().features.samplerAnisotropy != VK_TRUE) {
        OV_LOG_INFO("The physical device doesn't support Anisotropy.");
        score += 100;
    }

    // Check samples count for anti_aliasing
    std::optional<VkSampleCountFlags> aaSamples = physicalDevice.getMaxUsableSampleCount();

    if (aaSamples.has_value() == false)
        return 0;

    if (aaSamples.value() < engine.get().getNrOfAASamples()) 
    {
        OV_LOG_INFO("The number of samples count for anti_aliasing is not supported.");
        return 0;
    }

    // Discrete GPUs have a significant performance advantage
    if (physicalDeviceProperties.value().properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
        score += 1000;

    // Maximum possible size of textures affects graphics quality
    score += physicalDeviceProperties.value().properties.limits.maxImageDimension2D;


    //Done:
    return score;
}

#pragma endregion