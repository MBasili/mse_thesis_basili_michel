/**
 * @file	overv_vk_ovo.cpp
 * @brief	OVO file format support
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main include:
#include "overv_vk.h"


///////////////////////
// BODY OF CLASS Ovo //
///////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
void OVVK_API* OvVK::Ovo::createByType(ChunkId type) const
{
    switch (type)
    {
    case ChunkId::node:     return new Ov::Node();
    case ChunkId::material: return new OvVK::Material();
    case ChunkId::geometry: return new OvVK::Geometry();
    case ChunkId::light:    return new OvVK::Light();

    default:
        OV_LOG_ERROR("Unsupported object for the given chunk type");
        return nullptr;
    }
}