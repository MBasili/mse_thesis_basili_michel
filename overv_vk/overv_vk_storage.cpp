/**
 * @file	overv_vk_storage.cpp
 * @brief	Vulkan GPU storage
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// Includes //
//////////////

// Main include
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::Storage OvVK::Storage::empty("[empty]");


////////////////
// Structures //
////////////////

struct OVVK_API OvVK::Storage::Reserved
{
	// Buffers:
	OvVK::Buffer globalUniformHostBuffer;																	///< Global uniform host buffer on GPU
	OvVK::Buffer globalUniformDeviceBuffers[OvVK::Engine::nrFramesInFlight];							///< Global uniform device buffers on GPU

	OvVK::Buffer lightsHostBuffer;																			///< Lights host buffer on GPU
	OvVK::Buffer lightsDeviceBuffers[OvVK::Engine::nrFramesInFlight];									///< Lights device buffers on GPU

	OvVK::Buffer materialsHostBuffer;																		///< Materials host buffer on GPU
	OvVK::Buffer materialsDeviceBuffers[OvVK::Engine::nrFramesInFlight];								///< Materials device buffers on GPU

	OvVK::Buffer rtObjDescsHostBuffer;																		///< RTObjDesc host buffer on GPU
	OvVK::Buffer rtObjDescsDeviceBuffers[OvVK::Engine::nrFramesInFlight];								///< RTObjDesc device buffers on GPU

	// Counters
	std::map<OvVK::Storage::StoredType, uint32_t> nrOfInstances[OvVK::Engine::nrFramesInFlight];		///< Sorage type counter nr

	// Update
	std::list<std::reference_wrapper<const Ov::Renderable>> renderableToUpdate;									///< List of objects to be updated

	// Descriptor sets
	OvVK::StorageDescriptorSetsManager storageDescritorSetsManager;											///< Descriptor sets manager for binding.

	// Command Pool
	OvVK::CommandPool transferCP;																			///< Command pool to allocate command buffers to copy data from host to device buffers
	std::vector<std::reference_wrapper<OvVK::CommandBuffer>> secondaryTransferCB;							///< Command buffers to copy data from host to device buffers

	VkSemaphore ownershipGraphicStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];							///< Device buffers change ownership from graphic queue
	VkSemaphore ownershipComputeStatusSyncObjGPU[OvVK::Engine::nrFramesInFlight];							///< Device buffers change ownership from compute queue

	/**
	 * Constructor
	 */
	Reserved() : ownershipGraphicStatusSyncObjGPU{ VK_NULL_HANDLE }, ownershipComputeStatusSyncObjGPU{ VK_NULL_HANDLE }
	{
		secondaryTransferCB.resize(OvVK::Engine::nrFramesInFlight, OvVK::CommandBuffer::empty);

		for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) 
		{
			nrOfInstances[c][OvVK::Storage::StoredType::lights] = 0;
			nrOfInstances[c][OvVK::Storage::StoredType::rtObjDescs] = 0;
		}
	}
};


///////////////////////////////
// BODY OF THE CLASS Storage //
///////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Storage::Storage() : reserved(std::make_unique<OvVK::Storage::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::Storage::Storage(const std::string& name) : Ov::Renderable(name),
	reserved(std::make_unique<OvVK::Storage::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Storage::Storage(Storage&& other) : Ov::Renderable(std::move(other)),
	reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Storage::~Storage()
{
	OV_LOG_DETAIL("[-]");

	if (reserved && isInitialized())
		this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization method.
 * @return TF
 */
bool OVVK_API OvVK::Storage::init()
{
	// Init
	if (Ov::Managed::init() == false)
		return false;


	if (reserved->transferCP.free() == false) 
	{
		OV_LOG_ERROR("Fail to free transfer command pool in the storage with id= %d.", getId());
		return false;
	}

	// Free host Buffers
	if (reserved->globalUniformHostBuffer.free() == false ||
		reserved->rtObjDescsHostBuffer.free() == false ||
		reserved->materialsHostBuffer.free() == false ||
		reserved->lightsHostBuffer.free() == false)
		return false;

	// Free device Buffers
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
	{
		if (reserved->globalUniformDeviceBuffers[c].free() == false ||
			reserved->rtObjDescsDeviceBuffers[c].free() == false ||
			reserved->materialsDeviceBuffers[c].free() == false ||
			reserved->lightsDeviceBuffers[c].free() == false)
			return false;
	}

	// Destroy storage DescritorSet
	if (reserved->storageDescritorSetsManager.free() == false)
		return false;


	// Done:
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization method.
 * @return TF
 */
bool OVVK_API OvVK::Storage::free()
{
	// Init
	if (Ov::Managed::free() == false)
		return false;


	if (reserved->transferCP.free() == false)
	{
		OV_LOG_ERROR("Fail to free transfer command pool in the storage with id= %d.", getId());
		return false;
	}

	// Free host Buffers
	if (reserved->globalUniformHostBuffer.free() == false ||
		reserved->rtObjDescsHostBuffer.free() == false ||
		reserved->materialsHostBuffer.free() == false ||
		reserved->lightsHostBuffer.free() == false)
		return false;

	// Free device Buffers
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
	{
		if (reserved->globalUniformDeviceBuffers[c].free() == false ||
			reserved->rtObjDescsDeviceBuffers[c].free() == false ||
			reserved->materialsDeviceBuffers[c].free() == false ||
			reserved->lightsDeviceBuffers[c].free() == false)
			return false;
	}

	// Destroy storage DescritorSet
	if (reserved->storageDescritorSetsManager.free() == false)
		return false;


	// Done:
	return true;
}

#pragma endregion

#pragma region SyncData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the storage is locked.
 * @return TF
 */
bool OVVK_API OvVK::Storage::isLock() const
{
	uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

	return reserved->globalUniformDeviceBuffers[frameInFlight].isLock() ||
		reserved->lightsDeviceBuffers[frameInFlight].isLock() ||
		reserved->materialsDeviceBuffers[frameInFlight].isLock() ||
		reserved->rtObjDescsDeviceBuffers[frameInFlight].isLock();

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the storage is owned from a queue family.
 * @return TF
 */
bool OVVK_API OvVK::Storage::isOwned() const
{
	uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

	return reserved->globalUniformDeviceBuffers[frameInFlight].isOwned() ||
		reserved->lightsDeviceBuffers[frameInFlight].isOwned() ||
		reserved->materialsDeviceBuffers[frameInFlight].isOwned() ||
		reserved->rtObjDescsDeviceBuffers[frameInFlight].isOwned();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the storage.
 * @return TF
 */
bool OVVK_API OvVK::Storage::create() 
{
	// Init
	if (init() == false)
		return false;


	///////////////////////////////////////////
	// Create commandPool and commandBuffers //
	///////////////////////////////////////////

#pragma region CommandBuffers

	if (reserved->transferCP.create(OvVK::CommandPool::CommandTypesFlagBits::Transfer) == false) 
	{
		OV_LOG_ERROR(R"(Fail to create command pool to transfer data from storage's host buffers to device
 buffers in the storage with id= %d.)", getId());
		this->free();
		return false;
	}

	// Structure defines any state that will be inherited from the primary command buffer
	VkCommandBufferInheritanceInfo commandBufferInheritanceInfo = {};
	commandBufferInheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
	commandBufferInheritanceInfo.pNext = NULL;
	commandBufferInheritanceInfo.renderPass = VK_NULL_HANDLE;
	commandBufferInheritanceInfo.subpass = 0;
	commandBufferInheritanceInfo.framebuffer = VK_NULL_HANDLE;
	// occlusionQueryEnable specifies whether the command buffer can be executed while an occlusion query
	// is active in the primary command buffer. If this is VK_TRUE, then this command buffer can be executed
	// whether the primary command buffer has an occlusion query active or not. If this is VK_FALSE, 
	// then the primary command buffer must not have an occlusion query active.
	// Occlusion queries are only available on queue families supporting graphics operations.
	commandBufferInheritanceInfo.occlusionQueryEnable = VK_FALSE;
	commandBufferInheritanceInfo.queryFlags = 0x00000000;
	commandBufferInheritanceInfo.pipelineStatistics = 0x00000000;


	VkCommandBufferBeginInfo commandBufferBeginInfo{};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	// VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
	//      only be submitted once, and the command buffer will be reset and recorded again between each submission.
	commandBufferBeginInfo.flags = 0x00000000;
	commandBufferBeginInfo.pInheritanceInfo = &commandBufferInheritanceInfo;

	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) 
	{
		reserved->secondaryTransferCB[c] = reserved->transferCP.allocateCommandBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY);

		if (reserved->secondaryTransferCB[c].get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
			reserved->secondaryTransferCB[c].get().setStatusSyncObjValue(
				OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
		{
			OV_LOG_ERROR(R"(Fail to begin secondary command buffer to transfer storage's host buffers to device
				buffers in the storage with id = % d.)", getId());
			this->free();
			return false;
		}
	}

#pragma endregion


	/////////////////////////
	// Create host buffers //
	/////////////////////////

#pragma region HostBuffer

	// Create info buffer
	VkBufferCreateInfo hostVkBufferCreateInfo = {};
	hostVkBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	// VK_BUFFER_USAGE_TRANSFER_SRC_BIT => specifies that the buffer can be used as the source of a transfer command 
	hostVkBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	// buffers can also be owned by a specific queue family or be shared between multiple 
	// at the same time. 
	// VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
	// from multiple queue families is supported.
	hostVkBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	// From which queue family the buffer will be accessed.
	hostVkBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
	hostVkBufferCreateInfo.queueFamilyIndexCount = 0;

	// Create allocation info
	VmaAllocationCreateInfo hostBufferVmaAllocationCreateInfo = {};
	// VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
	//      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
	// VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
	//      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
	//      of allocation time.
	hostBufferVmaAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
		VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
	// VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
	//      mapped for host access using vkMapMemory.
	hostBufferVmaAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;


	// Initialize host buffer
	// Global Uniform
	hostVkBufferCreateInfo.size = sizeof(OvVK::Storage::GlobalUniformData);
	if (reserved->globalUniformHostBuffer.create(hostVkBufferCreateInfo, hostBufferVmaAllocationCreateInfo) == false ||
		reserved->globalUniformHostBuffer.setStatusSyncObjValue( OvVK::Storage::readyValueStatusSyncObj) == false ||
		reserved->globalUniformHostBuffer.setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
	{
		OV_LOG_ERROR("Fail to create global uniform host buffer in storage with id= %d.", getId());
		this->free();
		return false;
	}

	// Material
	hostVkBufferCreateInfo.size = OvVK::Material::maxNrOfMaterials * sizeof(OvVK::Storage::MaterialData);
	if (reserved->materialsHostBuffer.create(hostVkBufferCreateInfo, hostBufferVmaAllocationCreateInfo) == false ||
		reserved->materialsHostBuffer.setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
		reserved->materialsHostBuffer.setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
	{
		OV_LOG_ERROR("Fail to create materials host buffer in storage with id= %d.", getId());
		this->free();
		return false;
	}

	// Light
	hostVkBufferCreateInfo.size = OvVK::Light::maxNrOfLights * sizeof(OvVK::Storage::LightData);
	if (reserved->lightsHostBuffer.create(hostVkBufferCreateInfo, hostBufferVmaAllocationCreateInfo) == false ||
		reserved->lightsHostBuffer.setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
		reserved->lightsHostBuffer.setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
	{
		OV_LOG_ERROR("Fail to create lights host buffer in storage with id= %d.", getId());
		this->free();
		return false;
	}

	// RTObjDesc
	hostVkBufferCreateInfo.size = OvVK::RTObjDesc::maxNrOfRTObjDescs * sizeof(OvVK::Storage::RTObjDescData);
	if (reserved->rtObjDescsHostBuffer.create(hostVkBufferCreateInfo, hostBufferVmaAllocationCreateInfo) == false ||
		reserved->rtObjDescsHostBuffer.setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
		reserved->rtObjDescsHostBuffer.setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
	{
		OV_LOG_ERROR("Fail to create RTObjDescs host buffer in storage with id= %d.", getId());
		this->free();
		return false;
	}

#pragma endregion


	///////////////////////////
	// Create device buffers //
	///////////////////////////

#pragma region DeviceBuffer

	// Create info buffer
	VkBufferCreateInfo deviceVkBufferCreateInfo{};
	deviceVkBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	// VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
	//      of a transfer command.
	// VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT => specifies that the buffer can be used in a VkDescriptorBufferInfo suitable
	//		for occupying a VkDescriptorSet slot either of type VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER or 
	deviceVkBufferCreateInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	// buffers can also be owned by a specific queue family or be shared between multiple 
	// at the same time. 
	// VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
	// from multiple queue families is supported.
	deviceVkBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	// From which queue family the buffer will be accessed.
	deviceVkBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
	deviceVkBufferCreateInfo.queueFamilyIndexCount = 0;

	// Create allocation info
	VmaAllocationCreateInfo DeviceBufferVmaAllocationCreateInfo = {};
	// VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
	// free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
	// of allocation time.
	DeviceBufferVmaAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
	// 	Flags that must be set in a Memory Type chosen for an allocation. 
	DeviceBufferVmaAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	VkBufferCopy bufferCopy = {};
	bufferCopy.srcOffset = 0;
	bufferCopy.dstOffset = 0;

	// Initialize device buffer
	// Global Uniform
	deviceVkBufferCreateInfo.size = sizeof(OvVK::Storage::GlobalUniformData);
	bufferCopy.size = deviceVkBufferCreateInfo.size;
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) 
	{
		if (reserved->globalUniformDeviceBuffers[c].create(deviceVkBufferCreateInfo, DeviceBufferVmaAllocationCreateInfo) == false ||
			reserved->globalUniformDeviceBuffers[c].setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
			reserved->globalUniformDeviceBuffers[c].setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
		{
			OV_LOG_ERROR("Fail to create an global uniform device buffer in storage with id= %d.", getId());
			this->free();
			return false;
		}

		// Add copy command
		// Update Storage GlobalUniform
		vkCmdCopyBuffer(reserved->secondaryTransferCB[c].get().getVkCommandBuffer(),
			reserved->globalUniformHostBuffer.getVkBuffer(),
			reserved->globalUniformDeviceBuffers[c].getVkBuffer(),
			1,
			&bufferCopy);
	}


	// VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
	//      of a transfer command.
	// VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device
	//      address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
	// VK_BUFFER_USAGE_STORAGE_BUFFER_BIT specifies that the buffer can be used in a VkDescriptorBufferInfo suitable
	//      for occupying a VkDescriptorSet slot either of type VK_DESCRIPTOR_TYPE_STORAGE_BUFFER 
	//      or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC.
	deviceVkBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
		VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

	// Material
	deviceVkBufferCreateInfo.size = OvVK::Material::maxNrOfMaterials * sizeof(OvVK::Storage::MaterialData);
	bufferCopy.size = deviceVkBufferCreateInfo.size;
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
	{
		if (reserved->materialsDeviceBuffers[c].create(deviceVkBufferCreateInfo, DeviceBufferVmaAllocationCreateInfo) == false ||
			reserved->materialsDeviceBuffers[c].setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
			reserved->materialsDeviceBuffers[c].setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
		{
			OV_LOG_ERROR("Fail to create a material device buffer in storage with id= %d.", getId());
			this->free();
			return false;
		}

		// Add copy command
		// Update Storage materials
		vkCmdCopyBuffer(reserved->secondaryTransferCB[c].get().getVkCommandBuffer(),
			reserved->materialsHostBuffer.getVkBuffer(),
			reserved->materialsDeviceBuffers[c].getVkBuffer(),
			1,
			&bufferCopy);
	}

	// Light
	deviceVkBufferCreateInfo.size = OvVK::Light::maxNrOfLights * sizeof(OvVK::Storage::LightData);
	bufferCopy.size = deviceVkBufferCreateInfo.size;
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
	{
		if (reserved->lightsDeviceBuffers[c].create(deviceVkBufferCreateInfo, DeviceBufferVmaAllocationCreateInfo) == false ||
			reserved->lightsDeviceBuffers[c].setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
			reserved->lightsDeviceBuffers[c].setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
		{
			OV_LOG_ERROR("Fail to create a light device buffer in storage with id= %d.", getId());
			this->free();
			return false;
		}

		// Add copy command
		// Update Storage lights
		vkCmdCopyBuffer(reserved->secondaryTransferCB[c].get().getVkCommandBuffer(),
			reserved->lightsHostBuffer.getVkBuffer(),
			reserved->lightsDeviceBuffers[c].getVkBuffer(),
			1,
			&bufferCopy);
	}

	// RTObjDesc
	deviceVkBufferCreateInfo.size = OvVK::RTObjDesc::maxNrOfRTObjDescs * sizeof(OvVK::Storage::RTObjDescData);
	bufferCopy.size = deviceVkBufferCreateInfo.size;
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
	{
		if (reserved->rtObjDescsDeviceBuffers[c].create(deviceVkBufferCreateInfo, DeviceBufferVmaAllocationCreateInfo) == false ||
			reserved->rtObjDescsDeviceBuffers[c].setStatusSyncObjValue(OvVK::Storage::readyValueStatusSyncObj) == false ||
			reserved->rtObjDescsDeviceBuffers[c].setQueueFamilyIndex(reserved->transferCP.getQueueFamilyIndex().value()) == false)
		{
			OV_LOG_ERROR("Fail to create a light device buffer in storage with id= %d.", getId());
			this->free();
			return false;
		}

		// Add copy command
		// Update Storage RTObjDescs
		vkCmdCopyBuffer(reserved->secondaryTransferCB[c].get().getVkCommandBuffer(),
			reserved->rtObjDescsHostBuffer.getVkBuffer(),
			reserved->rtObjDescsDeviceBuffers[c].getVkBuffer(),
			1,
			&bufferCopy);
	}

#pragma endregion


	// End commandBuffer
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
	{
		if (reserved->secondaryTransferCB[c].get().endVkCommandBuffer() == false ||
			reserved->secondaryTransferCB[c].get().setStatusSyncObjValue(
				OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
		{
			OV_LOG_ERROR(R"(Fail to end secondary commandBuffer to transfer storage's host buffers to
 device host buffers in storage with id= %d.)", getId());
			this->free();
			return false;
		}
	}

	// Create storage descriptorSets
	if (reserved->storageDescritorSetsManager.create() == false)
	{
		OV_LOG_ERROR("Fail to create storage descriptorSetsManager in the sotrage with id= %d.", getId());
		this->free();
		return false;
	}

	// Update storage descriptorSet
	for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) 
	{
		OvVK::StorageDescriptorSetsManager::UpdateData updateData;
		updateData.targetFrame = c;

		updateData.globalUniformDeviceBuffer = reserved->globalUniformDeviceBuffers[c];
		updateData.lightsDeviceBuffer = reserved->lightsDeviceBuffers[c];
		updateData.materialsDeviceBuffer = reserved->materialsDeviceBuffers[c];
		updateData.rtObjDescsDeviceBuffer = reserved->rtObjDescsDeviceBuffers[c];

		reserved->storageDescritorSetsManager.addToUpdateList(updateData);
	}

	//Done:
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a renderable to the list of objects to update.
 * @param renderable object to update
 * @return TF
 */
bool OVVK_API OvVK::Storage::addToUpdateList(const Ov::Renderable& renderable)
{
	// Safety net:
	if (renderable == Ov::Renderable::empty)
	{
		OV_LOG_ERROR("Invalid params in addToUpdateList method in the storage with id= %d", getId());
		return false;
	}

	// Alread in the list?
	for (auto& c : reserved->renderableToUpdate)
		if (c.get() == renderable)
			return false;

	// Store it:
	reserved->renderableToUpdate.push_back(renderable);

	// Done:
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Upload host buffers data to device buffers.
 * @return TF
 */
bool OVVK_API OvVK::Storage::upload(VkFence signalFence,
	std::initializer_list<VkSemaphore> waitSemaphores,
	std::initializer_list<uint64_t> waitSemaphoresValues,
	std::initializer_list<VkSemaphore> signalSemaphores,
	std::initializer_list<uint64_t> signalSemaphoresValues) {

	// Safety net:
	if (waitSemaphores.size() != waitSemaphoresValues.size()) {
		OV_LOG_ERROR(R"(The size of the passed list of semaphores to wait doesn't match the size of the passed
 list of values to wait in storage with id= %d.)", getId());
		return false;
	}
	if (signalSemaphores.size() != signalSemaphoresValues.size()) {
		OV_LOG_ERROR(R"(The size of the passed list of semaphores to signal doesn't match the size of the passed
 list of values to signal in storage with id= %d.)", getId());
		return false;
	}

	// Engine
	std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
	// PhysicalDevice
	std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
	// Frame in flight
	uint64_t frameInFlight = engine.get().getFrameInFlight();


	//// Wait device buffers if used.
	//std::vector<VkFence> syncCPUToWait;
	//if (reserved->globalUniformDeviceBuffers[frameInFlight].getLockSyncObj() != VK_NULL_HANDLE)
	//	syncCPUToWait.push_back(reserved->globalUniformDeviceBuffers[frameInFlight].getLockSyncObj());
	//if (reserved->lightsDeviceBuffers[frameInFlight].getLockSyncObj() != VK_NULL_HANDLE)
	//	syncCPUToWait.push_back(reserved->lightsDeviceBuffers[frameInFlight].getLockSyncObj());
	//if (reserved->materialsDeviceBuffers[frameInFlight].getLockSyncObj() != VK_NULL_HANDLE)
	//	syncCPUToWait.push_back(reserved->materialsDeviceBuffers[frameInFlight].getLockSyncObj());
	//if(reserved->rtObjDescsDeviceBuffers[frameInFlight].getLockSyncObj() != VK_NULL_HANDLE)
	//	syncCPUToWait.push_back(reserved->rtObjDescsDeviceBuffers[frameInFlight].getLockSyncObj());

	//// Wait fences to know the work is finished
	//if (syncCPUToWait.size() > 0 &&
	//	vkWaitForFences(logicalDevice.get().getVkDevice(), syncCPUToWait.size(), syncCPUToWait.data(),
	//		VK_TRUE, UINT64_MAX) != VK_SUCCESS)
	//{
	//	OV_LOG_ERROR("Fail to wait upload Fence in storage with id= %d.", getId());
	//	return false;
	//}

		// Create new timeline semaphore and delete previous one.
	VkSemaphoreTypeCreateInfo timelineCreateInfo = {};
	timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
	timelineCreateInfo.pNext = NULL;
	timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
	timelineCreateInfo.initialValue = OvVK::Storage::defaultValueStatusSyncObj;

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	semaphoreInfo.pNext = &timelineCreateInfo;
	semaphoreInfo.flags = 0;

	// Destroy GPU sync objs.
	vkDestroySemaphore(logicalDevice.get().getVkDevice(),
		reserved->ownershipGraphicStatusSyncObjGPU[frameInFlight], nullptr);
	if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfo,
		nullptr, &reserved->ownershipGraphicStatusSyncObjGPU[frameInFlight]) != VK_SUCCESS)
	{
		OV_LOG_ERROR(R"(Fail to create semaphore to sync device buffer transfer ownership for graphic
 queue in storage with id= %d)", getId());
		return false;
	}
	vkDestroySemaphore(logicalDevice.get().getVkDevice(),
		reserved->ownershipComputeStatusSyncObjGPU[frameInFlight], nullptr);
	if (vkCreateSemaphore(logicalDevice.get().getVkDevice(), &semaphoreInfo,
		nullptr, &reserved->ownershipComputeStatusSyncObjGPU[frameInFlight]) != VK_SUCCESS)
	{
		OV_LOG_ERROR(R"(Fail to create semaphore to sync device buffer transfer ownership
 for compute queue in storage with id= %d)", getId());
		return false;
	}

	// Reset device buffer status sync objs.
	reserved->globalUniformDeviceBuffers[frameInFlight].setStatusSyncObjValue(OvVK::Storage::uploadingValueStatusSyncObj);
	reserved->lightsDeviceBuffers[frameInFlight].setStatusSyncObjValue(OvVK::Storage::uploadingValueStatusSyncObj);
	reserved->materialsDeviceBuffers[frameInFlight].setStatusSyncObjValue(OvVK::Storage::uploadingValueStatusSyncObj);
	reserved->rtObjDescsDeviceBuffers[frameInFlight].setStatusSyncObjValue(OvVK::Storage::uploadingValueStatusSyncObj);

	//////////////////////
	// Records Commands //
	//////////////////////

	// Perform all operation on the Pool of the current frame in flight.
	std::reference_wrapper<OvVK::CommandPool> computeCommandPool = engine.get().getPrimaryComputeCmdPool();
	std::reference_wrapper<OvVK::CommandPool> graphicCommandPool = engine.get().getPrimaryGraphicCmdPool();
	std::reference_wrapper<OvVK::CommandPool> transferCommandPool = engine.get().getPrimaryTransferCmdPool();

	std::reference_wrapper<OvVK::CommandBuffer> computeCB = computeCommandPool.get().allocateCommandBuffer();
	std::reference_wrapper<OvVK::CommandBuffer> transferCB = transferCommandPool.get().allocateCommandBuffer();
	std::reference_wrapper<OvVK::CommandBuffer> graphicCB = graphicCommandPool.get().allocateCommandBuffer();

	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	// VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
	//      only be submitted once, and the command buffer will be reset and recorded again between each submission.
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	bool exit = false;
	if (computeCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
		computeCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
	{
		OV_LOG_ERROR("Fail to begin or change status to the compute command buffer in storage with id= %d", getId());
		exit = true;
	}
	if (transferCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
		transferCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
	{
		OV_LOG_ERROR("Fail to begin or change status to the transfer command buffer in storage with id= %d", getId());
		exit = true;
	}
	if (graphicCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
		graphicCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
	{
		OV_LOG_ERROR("Fail to begin or change status to the graphic command buffer in storage with id= %d", getId());
		exit = true;
	}

	if (exit)
	{
		computeCommandPool.get().freeCommandBuffer(computeCB.get());
		transferCommandPool.get().freeCommandBuffer(transferCB.get());
		graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
		return false;
	}

	// Get queue families index
	std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamilyIndices =
		logicalDevice.get().getQueueFamiliesIndices();

	// barriers
	std::vector<VkBufferMemoryBarrier2> memoryBarrierChangeOwnershipGraphic;
	std::vector<VkBufferMemoryBarrier2> memoryBarrierChangeOwnershipCompute;

	// Global uniform device buffer change ownership barrier
	if (reserved->globalUniformDeviceBuffers[frameInFlight].getQueueFamilyIndex().has_value() &&
		reserved->globalUniformDeviceBuffers[frameInFlight].getQueueFamilyIndex().value()
		!= reserved->globalUniformHostBuffer.getQueueFamilyIndex().value())
	{
		VkBufferMemoryBarrier2 barrier = {};
		// GlobaUniform buffer
		barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
		// pNext is NULL or a pointer to a structure extending this structure.
		barrier.pNext = VK_NULL_HANDLE;
		// No need to wait previous stage 
		// VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
		barrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
		// No need to wait on specific memory access.
		barrier.srcAccessMask = VK_ACCESS_2_NONE;
		// All transfer commands need to wait.
		barrier.dstStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
		// All write commands need to wait.
		barrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
			VK_ACCESS_2_TRANSFER_READ_BIT;
		// Need to transfer ownership
		barrier.srcQueueFamilyIndex = reserved->globalUniformDeviceBuffers[frameInFlight].getQueueFamilyIndex().value();
		barrier.dstQueueFamilyIndex = reserved->globalUniformHostBuffer.getQueueFamilyIndex().value();
		// buffer
		barrier.buffer = reserved->globalUniformDeviceBuffers[frameInFlight].getVkBuffer();
		// offset is an offset in bytes into the backing memory for buffer;
		// this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
		barrier.offset = 0;
		// size is a size in bytes of the affected area of backing memory for buffer,
		// or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
		barrier.size = VK_WHOLE_SIZE;

		if (queueFamilyIndices.value().graphicsFamily.has_value() == true && 
			queueFamilyIndices.value().graphicsFamily.value() ==
			reserved->globalUniformDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipGraphic.push_back(barrier);
		else if (queueFamilyIndices.value().computeFamily.has_value() == true && 
			queueFamilyIndices.value().computeFamily.value() ==
			reserved->globalUniformDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipCompute.push_back(barrier);
		else {
			OV_LOG_ERROR(R"(In the queue family indices retriever from the logical device there is not the queue owning
 the global uniform device buffer with id= %d in the storage with id= %d.)",
				reserved->globalUniformDeviceBuffers[frameInFlight].getId(), getId());
			computeCommandPool.get().freeCommandBuffer(computeCB.get());
			transferCommandPool.get().freeCommandBuffer(transferCB.get());
			graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
			return false;
		}
	}

	// Lights device buffer change ownership barrier
	if (reserved->lightsDeviceBuffers[frameInFlight].getQueueFamilyIndex().has_value() &&
		reserved->lightsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value()
		!= reserved->lightsHostBuffer.getQueueFamilyIndex().value())
	{
		VkBufferMemoryBarrier2 barrier = {};
		// GlobaUniform buffer
		barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
		// pNext is NULL or a pointer to a structure extending this structure.
		barrier.pNext = VK_NULL_HANDLE;
		// No need to wait previous stage 
		// VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
		barrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
		// No need to wait on specific memory access.
		barrier.srcAccessMask = VK_ACCESS_2_NONE;
		// All transfer commands need to wait.
		barrier.dstStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
		// All write commands need to wait.
		barrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
			VK_ACCESS_2_TRANSFER_READ_BIT;
		// Need to transfer ownership
		barrier.srcQueueFamilyIndex = reserved->lightsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value();
		barrier.dstQueueFamilyIndex = reserved->lightsHostBuffer.getQueueFamilyIndex().value();
		// buffer
		barrier.buffer = reserved->lightsDeviceBuffers[frameInFlight].getVkBuffer();
		// offset is an offset in bytes into the backing memory for buffer;
		// this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
		barrier.offset = 0;
		// size is a size in bytes of the affected area of backing memory for buffer,
		// or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
		barrier.size = VK_WHOLE_SIZE;

		if (queueFamilyIndices.value().graphicsFamily.has_value() == true && 
			queueFamilyIndices.value().graphicsFamily.value() ==
			reserved->lightsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipGraphic.push_back(barrier);
		else if (queueFamilyIndices.value().computeFamily.has_value() == true && 
			queueFamilyIndices.value().computeFamily.value() ==
			reserved->lightsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipCompute.push_back(barrier);
		else {
			OV_LOG_ERROR(R"(In the queue family indices retriever from the logical device there is not the queue owning
 the lights device buffer with id= %d in the storage with id= %d.)",
				reserved->lightsDeviceBuffers[frameInFlight].getId(), getId());
			computeCommandPool.get().freeCommandBuffer(computeCB.get());
			transferCommandPool.get().freeCommandBuffer(transferCB.get());
			graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
			return false;
		}
	}

	// Materials device buffer change ownership barrier
	if (reserved->materialsDeviceBuffers[frameInFlight].getQueueFamilyIndex().has_value() &&
		reserved->materialsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value()
		!= reserved->materialsHostBuffer.getQueueFamilyIndex().value())
	{
		VkBufferMemoryBarrier2 barrier = {};
		// GlobaUniform buffer
		barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
		// pNext is NULL or a pointer to a structure extending this structure.
		barrier.pNext = VK_NULL_HANDLE;
		// No need to wait previous stage 
		// VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
		barrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
		// No need to wait on specific memory access.
		barrier.srcAccessMask = VK_ACCESS_2_NONE;
		// All transfer commands need to wait.
		barrier.dstStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
		// All write commands need to wait.
		barrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
			VK_ACCESS_2_TRANSFER_READ_BIT;
		// Need to transfer ownership
		barrier.srcQueueFamilyIndex = reserved->materialsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value();
		barrier.dstQueueFamilyIndex = reserved->materialsHostBuffer.getQueueFamilyIndex().value();
		// buffer
		barrier.buffer = reserved->materialsDeviceBuffers[frameInFlight].getVkBuffer();
		// offset is an offset in bytes into the backing memory for buffer;
		// this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
		barrier.offset = 0;
		// size is a size in bytes of the affected area of backing memory for buffer,
		// or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
		barrier.size = VK_WHOLE_SIZE;

		if (queueFamilyIndices.value().graphicsFamily.has_value() == true &&
			queueFamilyIndices.value().graphicsFamily.value() ==
			reserved->materialsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipGraphic.push_back(barrier);
		else if (queueFamilyIndices.value().computeFamily.has_value() == true &&
			queueFamilyIndices.value().computeFamily.value() ==
			reserved->materialsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipCompute.push_back(barrier);
		else {
			OV_LOG_ERROR(R"(In the queue family indices retriever from the logical device there is not the queue owning
 the materials device buffer with id= %d in the storage with id= %d.)",
				reserved->materialsDeviceBuffers[frameInFlight].getId(), getId());
			computeCommandPool.get().freeCommandBuffer(computeCB.get());
			transferCommandPool.get().freeCommandBuffer(transferCB.get());
			graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
			return false;
		}
	}

	// RTObjDescs device buffer change ownership barrier
	if (reserved->rtObjDescsDeviceBuffers[frameInFlight].getQueueFamilyIndex().has_value() &&
		reserved->rtObjDescsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value()
		!= reserved->rtObjDescsHostBuffer.getQueueFamilyIndex().value())
	{
		VkBufferMemoryBarrier2 barrier = {};
		// GlobaUniform buffer
		barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
		// pNext is NULL or a pointer to a structure extending this structure.
		barrier.pNext = VK_NULL_HANDLE;
		// No need to wait previous stage 
		// VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
		barrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
		// No need to wait on specific memory access.
		barrier.srcAccessMask = VK_ACCESS_2_NONE;
		// All transfer commands need to wait.
		barrier.dstStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
		// All write commands need to wait.
		barrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
			VK_ACCESS_2_TRANSFER_READ_BIT;
		// Need to transfer ownership
		barrier.srcQueueFamilyIndex = reserved->rtObjDescsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value();
		barrier.dstQueueFamilyIndex = reserved->rtObjDescsHostBuffer.getQueueFamilyIndex().value();
		// buffer
		barrier.buffer = reserved->rtObjDescsDeviceBuffers[frameInFlight].getVkBuffer();
		// offset is an offset in bytes into the backing memory for buffer;
		// this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
		barrier.offset = 0;
		// size is a size in bytes of the affected area of backing memory for buffer,
		// or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
		barrier.size = VK_WHOLE_SIZE;

		if (queueFamilyIndices.value().graphicsFamily.has_value() == true &&
			queueFamilyIndices.value().graphicsFamily.value() ==
			reserved->rtObjDescsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipGraphic.push_back(barrier);
		else if (queueFamilyIndices.value().computeFamily.has_value() == true &&
			queueFamilyIndices.value().computeFamily.value() ==
			reserved->rtObjDescsDeviceBuffers[frameInFlight].getQueueFamilyIndex().value())
			memoryBarrierChangeOwnershipCompute.push_back(barrier);
		else {
			OV_LOG_ERROR(R"(In the queue family indices retriever from the logical device there is not the queue owning
 the rtObjDescs device buffer with id= %d in the storage with id= %d.)",
				reserved->rtObjDescsDeviceBuffers[frameInFlight].getId(), getId());
			computeCommandPool.get().freeCommandBuffer(computeCB.get());
			transferCommandPool.get().freeCommandBuffer(transferCB.get());
			graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
			return false;
		}
	}

	// Records barrier into the command buffers.
	if (memoryBarrierChangeOwnershipGraphic.size() > 0) {
		// Barrier specification graphic
		VkDependencyInfo ownershipDependencyInfo = {};
		ownershipDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
		ownershipDependencyInfo.dependencyFlags = 0x00000000;
		ownershipDependencyInfo.memoryBarrierCount = 0;
		ownershipDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
		ownershipDependencyInfo.bufferMemoryBarrierCount = memoryBarrierChangeOwnershipGraphic.size();
		ownershipDependencyInfo.pBufferMemoryBarriers = memoryBarrierChangeOwnershipGraphic.data();
		ownershipDependencyInfo.imageMemoryBarrierCount = 0;
		ownershipDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

		// Register command for transition.
		vkCmdPipelineBarrier2(graphicCB.get().getVkCommandBuffer(), &ownershipDependencyInfo);
		vkCmdPipelineBarrier2(transferCB.get().getVkCommandBuffer(), &ownershipDependencyInfo);
	}
	if (memoryBarrierChangeOwnershipCompute.size() > 0) {
		// Barrier specification compute
		VkDependencyInfo ownershipDependencyInfo = {};
		ownershipDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
		ownershipDependencyInfo.dependencyFlags = 0x00000000;
		ownershipDependencyInfo.memoryBarrierCount = 0;
		ownershipDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
		ownershipDependencyInfo.bufferMemoryBarrierCount = memoryBarrierChangeOwnershipCompute.size();
		ownershipDependencyInfo.pBufferMemoryBarriers = memoryBarrierChangeOwnershipCompute.data();
		ownershipDependencyInfo.imageMemoryBarrierCount = 0;
		ownershipDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

		// Register command for transition.
		vkCmdPipelineBarrier2(computeCB.get().getVkCommandBuffer(), &ownershipDependencyInfo);
		vkCmdPipelineBarrier2(transferCB.get().getVkCommandBuffer(), &ownershipDependencyInfo);
	}


	////////////////////
	// Storage update //
	////////////////////

	VkBufferCopy copyBuffer = {};
	copyBuffer.srcOffset = 0;
	copyBuffer.dstOffset = 0;

	// Update Storage GlobalUniform
	copyBuffer.size = reserved->globalUniformHostBuffer.getSize();
	vkCmdCopyBuffer(transferCB.get().getVkCommandBuffer(), reserved->globalUniformHostBuffer.getVkBuffer(),
		reserved->globalUniformDeviceBuffers[frameInFlight].getVkBuffer(), 1, &copyBuffer);

	// Update Storage lights
	copyBuffer.size = reserved->lightsHostBuffer.getSize();
	vkCmdCopyBuffer(transferCB.get().getVkCommandBuffer(), reserved->lightsHostBuffer.getVkBuffer(),
		reserved->lightsDeviceBuffers[frameInFlight].getVkBuffer(), 1, &copyBuffer);

	// Update Storage materials
	copyBuffer.size = reserved->materialsHostBuffer.getSize();
	vkCmdCopyBuffer(transferCB.get().getVkCommandBuffer(), reserved->materialsHostBuffer.getVkBuffer(),
		reserved->materialsDeviceBuffers[frameInFlight].getVkBuffer(), 1, &copyBuffer);

	// Update Storage RTObjDescs
	copyBuffer.size = reserved->rtObjDescsHostBuffer.getSize();
	vkCmdCopyBuffer(transferCB.get().getVkCommandBuffer(), reserved->rtObjDescsHostBuffer.getVkBuffer(),
		reserved->rtObjDescsDeviceBuffers[frameInFlight].getVkBuffer(), 1, &copyBuffer);


	// End primary commandBuffers
	if (computeCB.get().endVkCommandBuffer() == false ||
		computeCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
	{
		OV_LOG_ERROR("Fail to end or change status of the compute command buffer in storage with id= %d", getId());
		exit = true;
	}
	if (transferCB.get().endVkCommandBuffer() == false ||
		transferCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
	{
		OV_LOG_ERROR("Fail to end or change status of the transfer command buffer in storage with id= %d", getId());
		exit = true;
	}
	if (graphicCB.get().endVkCommandBuffer() == false ||
		graphicCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
	{
		OV_LOG_ERROR("Fail to end or change status of the graphic command buffer in storage with id= %d.", getId());
		exit = true;
	}

	if (exit)
	{
		computeCommandPool.get().freeCommandBuffer(computeCB.get());
		transferCommandPool.get().freeCommandBuffer(transferCB.get());
		graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
		return false;
	}


	////////////
	// Submit //
	////////////

	// Get semaphore to wait
	std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(waitSemaphores.size());
	VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo = {};
	vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
	vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
	vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
	// If the device that semaphore was created on is not a device group, deviceIndex must be 0
	vkSemaphoreSubmitInfo.deviceIndex = 0;
	std::initializer_list<VkSemaphore>::iterator it1 = waitSemaphores.begin();
	std::initializer_list<uint64_t>::iterator it2 = waitSemaphoresValues.begin();
	uint32_t counter = 0;
	for (; it1 != waitSemaphores.end() && it2 != waitSemaphoresValues.end(); ++it1, ++it2)
	{
		vkSemaphoreSubmitInfo.semaphore = *it1;
		vkSemaphoreSubmitInfo.value = *it2;
		pWaitSemaphoreInfos[counter] = vkSemaphoreSubmitInfo;
		counter++;
	}

	// Get semaphore to signal
	std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(signalSemaphores.size());
	vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
	vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
	vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
	// If the device that semaphore was created on is not a device group, deviceIndex must be 0
	vkSemaphoreSubmitInfo.deviceIndex = 0;
	std::initializer_list<VkSemaphore>::iterator it3 = signalSemaphores.begin();
	std::initializer_list<uint64_t>::iterator it4 = signalSemaphoresValues.begin();
	counter = 0;
	for (; it3 != signalSemaphores.end() && it4 != signalSemaphoresValues.end(); ++it3, ++it4)
	{
		vkSemaphoreSubmitInfo.semaphore = *it3;
		vkSemaphoreSubmitInfo.value = *it4;
		pSignalSemaphoreInfos[counter] = vkSemaphoreSubmitInfo;
		counter++;
	}

	// graphic
	if (memoryBarrierChangeOwnershipGraphic.size() > 0) {

		// transfer
		VkSubmitInfo2 ownershipGraphicSubmitInfo = {};
		ownershipGraphicSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
		ownershipGraphicSubmitInfo.pNext = NULL;
		ownershipGraphicSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
		ownershipGraphicSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

		VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoSignal = {};
		vkSemaphoreSubmitInfoSignal.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
		vkSemaphoreSubmitInfoSignal.pNext = VK_NULL_HANDLE;
		vkSemaphoreSubmitInfoSignal.value = OvVK::Storage::readyValueStatusSyncObj;
		vkSemaphoreSubmitInfoSignal.semaphore = reserved->ownershipGraphicStatusSyncObjGPU[frameInFlight];
		vkSemaphoreSubmitInfoSignal.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
		// If the device that semaphore was created on is not a device group, deviceIndex must be 0
		vkSemaphoreSubmitInfoSignal.deviceIndex = 0;

		ownershipGraphicSubmitInfo.signalSemaphoreInfoCount = 1;
		ownershipGraphicSubmitInfo.pSignalSemaphoreInfos = &vkSemaphoreSubmitInfoSignal;

		VkCommandBufferSubmitInfo pCommandBufferInfos = {};
		pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
		pCommandBufferInfos.pNext = VK_NULL_HANDLE;
		pCommandBufferInfos.commandBuffer = graphicCB.get().getVkCommandBuffer();
		// A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
		pCommandBufferInfos.deviceMask = 0;

		ownershipGraphicSubmitInfo.commandBufferInfoCount = 1;
		ownershipGraphicSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

		// Submit
		if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(graphicCommandPool.get().getQueueFamilyIndex().value()),
			1, &ownershipGraphicSubmitInfo, NULL) != VK_SUCCESS)
		{
			OV_LOG_INFO("Fail to submit graphic command buffer in storage with id= %d.", getId());
			computeCommandPool.get().freeCommandBuffer(computeCB.get());
			transferCommandPool.get().freeCommandBuffer(transferCB.get());
			graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
			return false;
		}
	}

	// Compute
	if (memoryBarrierChangeOwnershipCompute.size() > 0) {
		// transfer
		VkSubmitInfo2 ownershipComputeSubmitInfo = {};
		ownershipComputeSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
		ownershipComputeSubmitInfo.pNext = NULL;
		ownershipComputeSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
		ownershipComputeSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

		VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoSignal = {};
		vkSemaphoreSubmitInfoSignal.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
		vkSemaphoreSubmitInfoSignal.pNext = VK_NULL_HANDLE;
		vkSemaphoreSubmitInfoSignal.value = OvVK::Storage::readyValueStatusSyncObj;
		vkSemaphoreSubmitInfoSignal.semaphore = reserved->ownershipComputeStatusSyncObjGPU[frameInFlight];
		vkSemaphoreSubmitInfoSignal.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
		// If the device that semaphore was created on is not a device group, deviceIndex must be 0
		vkSemaphoreSubmitInfoSignal.deviceIndex = 0;

		ownershipComputeSubmitInfo.signalSemaphoreInfoCount = 1;
		ownershipComputeSubmitInfo.pSignalSemaphoreInfos = &vkSemaphoreSubmitInfoSignal;

		VkCommandBufferSubmitInfo pCommandBufferInfos;
		pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
		pCommandBufferInfos.pNext = VK_NULL_HANDLE;
		pCommandBufferInfos.commandBuffer = computeCB.get().getVkCommandBuffer();
		// A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
		pCommandBufferInfos.deviceMask = 0;

		ownershipComputeSubmitInfo.commandBufferInfoCount = 1;
		ownershipComputeSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

		// Submit
		if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(computeCommandPool.get().getQueueFamilyIndex().value()),
			1, &ownershipComputeSubmitInfo, NULL) != VK_SUCCESS)
		{
			OV_LOG_INFO("Fail to submit compute command buffer in storage with id= %d.", getId());
			computeCommandPool.get().freeCommandBuffer(computeCB.get());
			transferCommandPool.get().freeCommandBuffer(transferCB.get());
			graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
			return false;
		}
	}

	// Transfer
	VkSubmitInfo2 uploadSubmitInfo = {};
	uploadSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
	uploadSubmitInfo.pNext = VK_NULL_HANDLE;

	VkCommandBufferSubmitInfo pCommandBufferInfos = {};
	pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
	pCommandBufferInfos.pNext = VK_NULL_HANDLE;
	pCommandBufferInfos.commandBuffer = transferCB.get().getVkCommandBuffer();
	// A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
	pCommandBufferInfos.deviceMask = 0;

	uploadSubmitInfo.commandBufferInfoCount = 1;
	uploadSubmitInfo.pCommandBufferInfos = &pCommandBufferInfos;

	// Set semaphore to wait
	vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;

	vkSemaphoreSubmitInfo.semaphore = reserved->globalUniformDeviceBuffers[frameInFlight].getStatusSyncObj();
	vkSemaphoreSubmitInfo.value = OvVK::Storage::uploadingValueStatusSyncObj;
	pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
	vkSemaphoreSubmitInfo.value = OvVK::Storage::readyValueStatusSyncObj;
	pSignalSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);

	vkSemaphoreSubmitInfo.semaphore = reserved->lightsDeviceBuffers[frameInFlight].getStatusSyncObj();
	vkSemaphoreSubmitInfo.value = OvVK::Storage::uploadingValueStatusSyncObj;
	pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
	vkSemaphoreSubmitInfo.value = OvVK::Storage::readyValueStatusSyncObj;
	pSignalSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);

	vkSemaphoreSubmitInfo.semaphore = reserved->materialsDeviceBuffers[frameInFlight].getStatusSyncObj();
	vkSemaphoreSubmitInfo.value = OvVK::Storage::uploadingValueStatusSyncObj;
	pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
	vkSemaphoreSubmitInfo.value = OvVK::Storage::readyValueStatusSyncObj;
	pSignalSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);

	vkSemaphoreSubmitInfo.semaphore = reserved->rtObjDescsDeviceBuffers[frameInFlight].getStatusSyncObj();
	vkSemaphoreSubmitInfo.value = OvVK::Storage::uploadingValueStatusSyncObj;
	pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
	vkSemaphoreSubmitInfo.value = OvVK::Storage::readyValueStatusSyncObj;
	pSignalSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);

	// Change ownership geometries
	if (memoryBarrierChangeOwnershipGraphic.size() > 0)
	{
		vkSemaphoreSubmitInfo.value = OvVK::Storage::readyValueStatusSyncObj;
		vkSemaphoreSubmitInfo.semaphore = reserved->ownershipGraphicStatusSyncObjGPU[frameInFlight];
		pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
	}
	if (memoryBarrierChangeOwnershipCompute.size() > 0)
	{
		vkSemaphoreSubmitInfo.value = OvVK::Storage::readyValueStatusSyncObj;
		vkSemaphoreSubmitInfo.semaphore = reserved->ownershipComputeStatusSyncObjGPU[frameInFlight];
		pWaitSemaphoreInfos.push_back(vkSemaphoreSubmitInfo);
	}

	uploadSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
	uploadSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();
	uploadSubmitInfo.signalSemaphoreInfoCount = pSignalSemaphoreInfos.size();
	uploadSubmitInfo.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();

	// Submit
	if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(computeCommandPool.get().getQueueFamilyIndex().value()),
		1, &uploadSubmitInfo, signalFence) != VK_SUCCESS)
	{
		OV_LOG_INFO("Fail to submit trasnfer command buffer in storage with id= %d.", getId());
		computeCommandPool.get().freeCommandBuffer(computeCB.get());
		transferCommandPool.get().freeCommandBuffer(transferCB.get());
		graphicCommandPool.get().freeCommandBuffer(graphicCB.get());
		return false;
	}


	///////////////////
	// Set Ownership //
	///////////////////

	reserved->globalUniformDeviceBuffers[frameInFlight].setQueueFamilyIndex(transferCommandPool.get().getQueueFamilyIndex().value());
	reserved->lightsDeviceBuffers[frameInFlight].setQueueFamilyIndex(transferCommandPool.get().getQueueFamilyIndex().value());
	reserved->materialsDeviceBuffers[frameInFlight].setQueueFamilyIndex(transferCommandPool.get().getQueueFamilyIndex().value());
	reserved->rtObjDescsDeviceBuffers[frameInFlight].setQueueFamilyIndex(transferCommandPool.get().getQueueFamilyIndex().value());


	// Done:
	return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the global uniform host OvVK buffer reference.
 * @return The global uniform host OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API &OvVK::Storage::getGlobalUniformHostBuffer() const
{
	return reserved->globalUniformHostBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current frame in flight global uniform device OvVK buffer reference.
 * @return The current frame in flight global uniform device OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API &OvVK::Storage::getGlobalUniformDeviceBuffer() const
{
	return reserved->globalUniformDeviceBuffers[Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the global uniform structure data.
 * @return The global uniform storage's structure data.
 */
OvVK::Storage::GlobalUniformData OVVK_API *OvVK::Storage::getGlobalUniformData()
{
	return reinterpret_cast<OvVK::Storage::GlobalUniformData*>(
		reserved->globalUniformHostBuffer.getVmaAllocationInfo().pMappedData);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the lights host OvVK buffer reference.
 * @return The lights host OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API& OvVK::Storage::getLightsHostBuffer() const
{
	return reserved->lightsHostBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current frame in flight lights device OvVK buffer reference.
 * @return The current frame in flight lights device OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API& OvVK::Storage::getLightsDeviceBuffer() const
{
	return reserved->lightsDeviceBuffers[Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the light structure data.
 * @param id A index of a light in the storage's host buffer.
 * @return The light storage's structure data.
 */
OvVK::Storage::LightData OVVK_API* OvVK::Storage::getLightData(uint32_t id)
{
	// Safety net:
	if (id >= OvVK::Light::maxNrOfLights)
	{
		OV_LOG_ERROR("Out of range light index in storage with id= %d", getId());
		return nullptr;
	}

	return reinterpret_cast<OvVK::Storage::LightData*>(
		((uint8_t*)reserved->lightsHostBuffer.getVmaAllocationInfo().pMappedData)
		+ id * sizeof(OvVK::Storage::LightData));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the materials host OvVK buffer reference.
 * @return The materials host OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API &OvVK::Storage::getMaterialsHostBuffer() const 
{
	return reserved->materialsHostBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current frame in flight materials device OvVK buffer reference.
 * @return The current frame in flight materials device OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API &OvVK::Storage::getMaterialsDeviceBuffer() const 
{
	return reserved->materialsDeviceBuffers[Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the material structure data.
 * @param id The lutpos index of a material.
 * @return The material storage's structure data.
 */
OvVK::Storage::MaterialData OVVK_API* OvVK::Storage::getMaterialData(uint32_t id) 
{
	// Safety net:
	if (id >= OvVK::Material::maxNrOfMaterials)
	{
		OV_LOG_ERROR("Out of range material index lutpos in storage with id= %d", getId());
		return nullptr;
	}

	return reinterpret_cast<OvVK::Storage::MaterialData*>(
		((uint8_t*)reserved->materialsHostBuffer.getVmaAllocationInfo().pMappedData)
		+ id * sizeof(OvVK::Storage::MaterialData));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the rtObjDescs host OvVK buffer reference.
 * @return The rtObjDescs host OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API& OvVK::Storage::getRTObjDescsHostBuffer() const
{
	return reserved->rtObjDescsHostBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current frame in flight rtObjDescs device OvVK buffer reference.
 * @return The current frame in flight rtObjDescs device OvVK buffer reference.
 */
const OvVK::Buffer OVVK_API& OvVK::Storage::getRTObjDescsDeviceBuffer() const 
{
	return reserved->rtObjDescsDeviceBuffers[Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the light structure data.
 * @param id A index of a rtObjDesc in the storage's host buffer.
 * @return The light storage's structure data.
 */
OvVK::Storage::RTObjDescData OVVK_API* OvVK::Storage::getRTObjDescData(uint32_t id)
{
	// Safety net:
	if (id >= OvVK::RTObjDesc::maxNrOfRTObjDescs)
	{
		OV_LOG_ERROR("Out of range rtObjDesc index in storage with id= %d", getId());
		return nullptr;
	}

	return reinterpret_cast<OvVK::Storage::RTObjDescData*>(
		((uint8_t*)reserved->rtObjDescsHostBuffer.getVmaAllocationInfo().pMappedData)
		+ id * sizeof(OvVK::Storage::RTObjDescData));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the number of instances in the storage's host OvVK buffer.
 * @param type A storage's type for which retriever the number of instance.
 * @return The number of instances in the storage's host OvVK buffer.
 */
uint32_t OVVK_API OvVK::Storage::getNrOfInstances(OvVK::Storage::StoredType type) const
{
	// Safety net:
	if (reserved->nrOfInstances[Engine::getInstance().getFrameInFlight()].count(type))
		return reserved->nrOfInstances[Engine::getInstance().getFrameInFlight()][type];

	OV_LOG_ERROR("Can't get the number of instance for the passed type (invalid type) in storage with id= %d.", getId());
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the storage's descriptor sets manager.
 * @return The storage's descriptor sets manager.
 */
OvVK::StorageDescriptorSetsManager OVVK_API& OvVK::Storage::getStorageDescriptorSetsManager() const
{
	return reserved->storageDescritorSetsManager;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the current frame in flight transfer command buffer.
 * @return The current frame in flight transfer command buffer.
 */
OvVK::CommandBuffer OVVK_API& OvVK::Storage::getSecondaryTransferCB() const 
{
	return reserved->secondaryTransferCB[Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the number of instances for a storage's type.
 * @param nrOfInstances Number of instance.
 * @param type A storage's type.
 * @return TF
 */
bool OVVK_API OvVK::Storage::setNrOfInstances(uint32_t nrOfInstances, OvVK::Storage::StoredType type)
{
	// Safety net:
	if (reserved->nrOfInstances[Engine::getInstance().getFrameInFlight()].count(type) > 0) 
	{
		reserved->nrOfInstances[Engine::getInstance().getFrameInFlight()][type] = nrOfInstances;
		return true;
	}


	OV_LOG_ERROR("Can't set the number of instance for the passed type (invalid type) in storage with id= %d.", getId());
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the queue family index of the queue owning the current frame in flight storage's device buffers.
 * @param queueFamilyIndex Index of the queue owning the current frame in flight storage's device buffers.
 * @return TF
 */
bool OVVK_API OvVK::Storage::setDeviceBufferQueueFamilyIndex(uint32_t queueFamilyIndex)
{
	uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

	return reserved->globalUniformDeviceBuffers[frameInFlight].setQueueFamilyIndex(queueFamilyIndex) &&
		reserved->lightsDeviceBuffers[frameInFlight].setQueueFamilyIndex(queueFamilyIndex) &&
		reserved->materialsDeviceBuffers[frameInFlight].setQueueFamilyIndex(queueFamilyIndex) &&
		reserved->rtObjDescsDeviceBuffers[frameInFlight].setQueueFamilyIndex(queueFamilyIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the storage.
 * @param value The uint value rapresenting the status the current frame in flight storage's device buffers is in.
 * @return TF
 */
bool OVVK_API OvVK::Storage::setDeviceBufferStatusSyncObjValue(uint64_t queueFamilyIndex)
{
	uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

	return reserved->globalUniformDeviceBuffers[frameInFlight].setStatusSyncObjValue(queueFamilyIndex) &&
		reserved->lightsDeviceBuffers[frameInFlight].setStatusSyncObjValue(queueFamilyIndex) &&
		reserved->materialsDeviceBuffers[frameInFlight].setStatusSyncObjValue(queueFamilyIndex) &&
		reserved->rtObjDescsDeviceBuffers[frameInFlight].setStatusSyncObjValue(queueFamilyIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sync obj for the lock.
 * @param fence Vulkan object to check to know whether the current frame in flight storage's device buffers are locked or not.
 * @return TF
 */
bool OVVK_API OvVK::Storage::setDeviceBufferLockSyncObj(VkFence fence)
{
	uint32_t frameInFlight = Engine::getInstance().getFrameInFlight();

	return reserved->globalUniformDeviceBuffers[frameInFlight].setLockSyncObj(fence) &&
		reserved->lightsDeviceBuffers[frameInFlight].setLockSyncObj(fence) &&
		reserved->materialsDeviceBuffers[frameInFlight].setLockSyncObj(fence) &&
		reserved->rtObjDescsDeviceBuffers[frameInFlight].setLockSyncObj(fence);
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::Storage::render(uint32_t value, void* data) const
{
	// Updated  
	auto c = std::begin(reserved->renderableToUpdate);
	while (c != std::end(reserved->renderableToUpdate))
	{
		// Render and decrease count:        
		if (c->get().render(static_cast<uint32_t>(Ov::Object::Flag::upload)) == false) 
		{
			OV_LOG_ERROR("Fail to call render method on the object with name %s and id %d in storage with id= %d",
				c->get().getName(), c->get().getId(), getId());
		}
		c = reserved->renderableToUpdate.erase(c);
	}

	if (reserved->storageDescritorSetsManager.render() == false) 
	{
		OV_LOG_ERROR("Fail to update storage descriptorSet in storage with id= %d.", getId());
		return false;
	}

	// Done:
	return true;
}

#pragma endregion

#pragma region Shader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds to the OvVK shader preprocessing the string to include the info to access the storage data in a shader.
 * @return TF
 */
bool OVVK_API OvVK::Storage::addShaderPreproc()
{
	std::string storagePreproc = R"(
#ifndef OVVULKAN_STORAGE_INCLUDED
#define OVVULKAN_STORAGE_INCLUDED
)";


	storagePreproc += GlobalUniformData::getShaderStruct() + "\n" +
		RTObjDescData::getShaderStruct() + "\n" +
		MaterialData::getShaderStruct() + "\n" +
		LightData::getShaderStruct();

	storagePreproc += R"(
#endif
 )";


	return OvVK::Shader::preprocessor.set("include Storage", storagePreproc);
}

#pragma endregion