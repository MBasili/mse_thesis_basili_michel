#pragma once
/**
 * @file	overv_vk_container.h
 * @brief	Vulkan centralized data container
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 * base from overv_gl_container.cpp
 */

//////////////
// #INCLUDE //
//////////////

// Library Headers
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::Container OvVK::Container::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Container reserved structure.
 */
struct OvVK::Container::Reserved
{
    std::list<Ov::Node> allNodes;                                   ///< List of all nodes.
    std::list<OvVK::Light> allLights;                           ///< List of all lights.
    std::list<OvVK::Geometry> allGeoms;                         ///< List of all geometries.
    std::list<OvVK::Material> allMats;                          ///< List of all materials.
    std::list<OvVK::Texture> allTexs;                           ///< List of all textures.
    std::list<OvVK::Texture2D> allTexs2D;                       ///< List of all textures.
    std::list<OvVK::Sampler> allSamplers;                       ///< List of all samplers.
    std::list<OvVK::BottomLevelAccelerationStructure> allBLASs; ///< List of all BLASs.
    std::list<OvVK::RTObjDesc> allRTObjDescs;                   ///< List of all rtObjDesc.

    /**
     * Constructor.
     */
    Reserved()
    {}
};


/////////////////////////////
// BODY OF CLASS Container //
/////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Container::Container() : reserved(std::make_unique<OvVK::Container::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Container::Container(const std::string& name) : Ov::Container(name),
reserved(std::make_unique<OvVK::Container::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Container::Container(Container&& other) noexcept: Ov::Container(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Container::~Container()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Resets the content of the container.
 * @return TF
 */
bool OVVK_API OvVK::Container::reset()
{
    reserved->allNodes.clear();
    reserved->allGeoms.clear();
    reserved->allMats.clear();
    reserved->allTexs.clear();
    reserved->allTexs2D.clear();
    reserved->allSamplers.clear();
    reserved->allLights.clear();
    reserved->allBLASs.clear();
    reserved->allRTObjDescs.clear();

    // Done:
    setDirty(true);
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add the passed object to the proper container.
 * @param obj The object to add to the container.
 * @return TF
 */
bool OVVK_API OvVK::Container::add(Ov::Object& obj)
{
    // Safety net:
    if (obj == Ov::Object::empty)
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Sort by type:
    if (dynamic_cast<Ov::Node*>(&obj))
    {
        reserved->allNodes.push_back(std::move(dynamic_cast<Ov::Node&>(obj)));
        this->Ov::Container::add(reserved->allNodes.back());
        return true;
    }
    else if (dynamic_cast<OvVK::Geometry*>(&obj))
    {
        reserved->allGeoms.push_back(std::move(dynamic_cast<OvVK::Geometry&>(obj)));
        this->Ov::Container::add(reserved->allGeoms.back());
        return true;
    }
	else if (dynamic_cast<OvVK::Material*>(&obj))
	{
		reserved->allMats.push_back(std::move(dynamic_cast<OvVK::Material&>(obj)));
		this->Ov::Container::add(reserved->allMats.back());
		return true;
	}
    else if (dynamic_cast<OvVK::Texture2D*>(&obj))
    {
        reserved->allTexs2D.push_back(std::move(dynamic_cast<OvVK::Texture2D&>(obj)));
        this->Ov::Container::add(reserved->allTexs2D.back());
        return true;
    }
	else if (dynamic_cast<OvVK::Texture*>(&obj))
	{
		reserved->allTexs.push_back(std::move(dynamic_cast<OvVK::Texture&>(obj)));
		this->Ov::Container::add(reserved->allTexs.back());
		return true;
	}
    else if (dynamic_cast<OvVK::Sampler*>(&obj))
    {
        reserved->allSamplers.push_back(std::move(dynamic_cast<OvVK::Sampler&>(obj)));
        this->Ov::Container::add(reserved->allSamplers.back());
        return true;
    }
    else if (dynamic_cast<OvVK::Light*>(&obj))
    {
        reserved->allLights.push_back(std::move(dynamic_cast<OvVK::Light&>(obj)));
        this->Ov::Container::add(reserved->allLights.back());
        return true;
    }
    else if (dynamic_cast<OvVK::BottomLevelAccelerationStructure*>(&obj))
    {
        reserved->allBLASs.push_back(std::move(dynamic_cast<OvVK::BottomLevelAccelerationStructure&>(obj)));
        this->Ov::Container::add(reserved->allBLASs.back());
        return true;
    }
    else if (dynamic_cast<OvVK::RTObjDesc*>(&obj))
    {
        reserved->allRTObjDescs.push_back(std::move(dynamic_cast<OvVK::RTObjDesc&>(obj)));
        this->Ov::Container::add(reserved->allRTObjDescs.back());
        return true;
    }

    // Done:
    OV_LOG_ERROR("Unsupported type");
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Remove the object with the passed id from the container. 
 * @param id Id of the object to remove.
 * @return TF
 */
bool OVVK_API OvVK::Container::remove(uint32_t id)
{
    // Safety net:
    if (id == Ov::Object::empty.getId())
    {
        OV_LOG_ERROR("Invalid params");
        return false;
    }

    // Search in nodes:
    std::list<Ov::Node>::iterator itNodes;
    for (itNodes = reserved->allNodes.begin(); itNodes != reserved->allNodes.end(); itNodes++)
        if (itNodes->getId() == id)
        {
            reserved->allNodes.erase(itNodes);
            this->Ov::Container::remove(id);
            return true;
        }
            
    // Search in geometries:
    std::list<OvVK::Geometry>::iterator itGeometries;
    for (itGeometries = reserved->allGeoms.begin(); itGeometries != reserved->allGeoms.end(); itGeometries++)
        if (itGeometries->getId() == id)
        {
            reserved->allGeoms.erase(itGeometries);
            this->Ov::Container::remove(id);
            return true;
        }

    // Search in materials:
    std::list<OvVK::Material>::iterator itMaterials;
    for (itMaterials = reserved->allMats.begin(); itMaterials != reserved->allMats.end(); itMaterials++)
        if (itMaterials->getId() == id)
        {
            reserved->allMats.erase(itMaterials);
            this->Ov::Container::remove(id);
            return true;
        }

    std::list<OvVK::Texture2D>::iterator itTextures2D;
    for (itTextures2D = reserved->allTexs2D.begin(); itTextures2D != reserved->allTexs2D.end(); itTextures2D++)
        if (itTextures2D->getId() == id)
        {
            reserved->allTexs2D.erase(itTextures2D);
            this->Ov::Container::remove(id);
            return true;
        }

    // Search in textures:
    std::list<OvVK::Texture>::iterator itTextures;
    for (itTextures = reserved->allTexs.begin(); itTextures != reserved->allTexs.end(); itTextures++)
        if (itTextures->getId() == id)
        {
            reserved->allTexs.erase(itTextures);
            this->Ov::Container::remove(id);
            return true;
        }

    // Search in samplers:
    std::list<OvVK::Sampler>::iterator itSamplers;
    for (itSamplers = reserved->allSamplers.begin(); itSamplers != reserved->allSamplers.end(); itSamplers++)
        if (itSamplers->getId() == id)
        {
            reserved->allSamplers.erase(itSamplers);
            this->Ov::Container::remove(id);
            return true;
        }

    // Search in lights:
    std::list<OvVK::Light>::iterator itLights;
    for (itLights = reserved->allLights.begin(); itLights != reserved->allLights.end(); itLights++)
        if (itLights->getId() == id)
        {
            reserved->allLights.erase(itLights);
            this->Ov::Container::remove(id);
            return true;
        }

    // Search in BLASs:
    std::list<OvVK::BottomLevelAccelerationStructure>::iterator itBLASs;
    for (itBLASs = reserved->allBLASs.begin(); itBLASs != reserved->allBLASs.end(); itBLASs++)
        if (itBLASs->getId() == id)
        {
            reserved->allBLASs.erase(itBLASs);
            this->Ov::Container::remove(id);
            return true;
        }

    // Search in RTObjDesc:
    std::list<OvVK::RTObjDesc>::iterator itRTObjDescs;
    for (itRTObjDescs = reserved->allRTObjDescs.begin(); itRTObjDescs != reserved->allRTObjDescs.end(); itRTObjDescs++)
        if (itRTObjDescs->getId() == id)
        {
            reserved->allRTObjDescs.erase(itRTObjDescs);
            this->Ov::Container::remove(id);
            return true;
        }

    // Not found:
    return false;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added node.
 * @return Ov::Node
 */
Ov::Node OVVK_API& OvVK::Container::getLastNode()
{
    // Safety net:
    if (reserved->allNodes.empty())
        return Ov::Node::empty;
    return reserved->allNodes.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added geometry.
 * @return OvVK::Geometry
 */
OvVK::Geometry OVVK_API& OvVK::Container::getLastGeometry()
{
    // Safety net:
    if (reserved->allGeoms.empty())
        return OvVK::Geometry::empty;
    return reserved->allGeoms.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added material.
 * @return OvVK::Material
 */
OvVK::Material OVVK_API& OvVK::Container::getLastMaterial()
{
    // Safety net:
    if (reserved->allMats.empty())
        return OvVK::Material::empty;
    return reserved->allMats.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added texture.
 * @return OvVK::Texture
 */
OvVK::Texture OVVK_API& OvVK::Container::getLastTexture()
{
    // Safety net:
    if (reserved->allTexs.empty())
        return OvVK::Texture::empty;
    return reserved->allTexs.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added texture.
 * @return OvVK::Texture2D
 */
OvVK::Texture2D OVVK_API& OvVK::Container::getLastTexture2D()
{
    // Safety net:
    if (reserved->allTexs2D.empty())
        return OvVK::Texture2D::empty;
    return reserved->allTexs2D.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added Sampler.
 * @return OvVK::Sampler
 */
OvVK::Sampler OVVK_API& OvVK::Container::getLastSampler()
{
    // Safety net:
    if (reserved->allSamplers.empty())
        return OvVK::Sampler::empty;
    return reserved->allSamplers.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added light.
 * @return OvVK::Light
 */
OvVK::Light OVVK_API& OvVK::Container::getLastLight()
{
    // Safety net:
    if (reserved->allLights.empty())
        return OvVK::Light::empty;
    return reserved->allLights.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added BLAS.
 * @return OvVK::BottomLevelAccelerationStructure
 */
OvVK::BottomLevelAccelerationStructure OVVK_API& OvVK::Container::getLastBLAS()
{
    // Safety net:
    if (reserved->allBLASs.empty())
        return OvVK::BottomLevelAccelerationStructure::empty;
    return reserved->allBLASs.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return last added RTObjDesc.
 * @return OvVK::RTObjDesc
 */
OvVK::RTObjDesc OVVK_API& OvVK::Container::getLastRTObjDesc()
{
    // Safety net:
    if (reserved->allRTObjDescs.empty())
        return OvVK::RTObjDesc::empty;
    return reserved->allRTObjDescs.back();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the nodes in the container.
 * @return std::list<Ov::Node>
 */
std::list<Ov::Node> OVVK_API& OvVK::Container::getNodeList()
{
    return reserved->allNodes;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the geometries in the container.
 * @return std::list<OvVK::Geometry>
 */
std::list<OvVK::Geometry> OVVK_API& OvVK::Container::getGeometryList()
{
    return reserved->allGeoms;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the materials in the container.
 * @return std::list<OvVK::Material>
 */
std::list<OvVK::Material> OVVK_API& OvVK::Container::getMaterialList()
{
    return reserved->allMats;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the textures in the container.
 * @return std::list<OvVK::Texture>
 */
std::list<OvVK::Texture> OVVK_API& OvVK::Container::getTextureList()
{
    return reserved->allTexs;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the 2D textures in the container.
 * @return std::list<OvVK::Texture2D>
 */
std::list<OvVK::Texture2D> OVVK_API& OvVK::Container::getTexture2DList()
{
    return reserved->allTexs2D;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the samplers in the container.
 * @return std::list<OvVK::Sampler>
 */
std::list<OvVK::Sampler> OVVK_API& OvVK::Container::getSamplerList()
{
    return reserved->allSamplers;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the lights in the container.
 * @return std::list<OvVK::Light>
 */
std::list<OvVK::Light> OVVK_API& OvVK::Container::getLightList()
{
    return reserved->allLights;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the BLASs in the container.
 * @return std::list<OvVK::BottomLevelAccelerationStructure>
 */
std::list<OvVK::BottomLevelAccelerationStructure> OVVK_API& OvVK::Container::getBLASList()
{
    return reserved->allBLASs;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return reference to the list containig all the RTObjDescs in the container.
 * @return std::list<OvVK::RTObjDesc>
 */
std::list<OvVK::RTObjDesc> OVVK_API& OvVK::Container::getRTObjDescList()
{
    return reserved->allRTObjDescs;
}

#pragma endregion

#pragma region Find

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns, if existing, the first object with the given name among its various lists.
 * @param name Object name
 * @return Ov::Object Object found or empty object.
 */
Ov::Object OVVK_API& OvVK::Container::find(const std::string& name) const
{
    // Safety net:
    if (name.empty())
    {
        OV_LOG_ERROR("Invalid params");
        return Ov::Object::empty;
    }


    // Search in nodes:
    for (auto& c : reserved->allNodes)
        if (c.getName() == name)
            return c;

    // Search in geometries:
    for (auto& c : reserved->allGeoms)
        if (c.getName() == name)
            return c;

    // Search in materials:
    for (auto& c : reserved->allMats)
        if (c.getName() == name)
            return c;

    // Search in textures:
    for (auto& c : reserved->allTexs)
        if (c.getName() == name)
            return c;

    // Search in textures2D:
    for (auto& c : reserved->allTexs2D)
        if (c.getName() == name)
            return c;

    // Search in sampler:
    for (auto& c : reserved->allSamplers)
        if (c.getName() == name)
            return c;

    // Search in lights:
    for (auto& c : reserved->allLights)
        if (c.getName() == name)
            return c;

    // Search in BLASs:
    for (auto& c : reserved->allBLASs)
        if (c.getName() == name)
            return c;

    // Search in RTObjDescs:
    for (auto& c : reserved->allRTObjDescs)
        if (c.getName() == name)
            return c;


    // Not found:
    return Ov::Object::empty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns, if existing, the first object with the given ID among its various lists.
 * @param id Object id
 * @return Ov::Object Found object or empty
 */
Ov::Object OVVK_API& OvVK::Container::find(const uint32_t id) const
{
    // Fast lane:
    if (id == 0)
        return Ov::Object::empty;


    // Search in nodes:
    for (auto& c : reserved->allNodes)
        if (c.getId() == id)
            return c;

    // Search in geometries:
    for (auto& c : reserved->allGeoms)
        if (c.getId() == id)
            return c;

    // Search in materials:
    for (auto& c : reserved->allMats)
        if (c.getId() == id)
            return c;

    // Search in textures:
    for (auto& c : reserved->allTexs)
        if (c.getId() == id)
            return c;

    // Search in textures2D:
    for (auto& c : reserved->allTexs2D)
        if (c.getId() == id)
            return c;

    // Search in samplers:
    for (auto& c : reserved->allSamplers)
        if (c.getId() == id)
            return c;

    // Search in lights:
    for (auto& c : reserved->allLights)
        if (c.getId() == id)
            return c;

    // Search in BLASs:
    for (auto& c : reserved->allBLASs)
        if (c.getId() == id)
            return c;

    // Search in RTObjDescs:
    for (auto& c : reserved->allRTObjDescs)
        if (c.getId() == id)
            return c;


    // Not found:
    return Ov::Object::empty;
}

#pragma endregion
