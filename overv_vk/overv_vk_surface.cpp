/**
 * @file	overv_vk_surface.cpp
 * @brief	Vulkan surface properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"

// GLFW
#include <GLFW/glfw3.h>


////////////
// STATIC //
////////////

// Special values
OvVK::Surface OvVK::Surface::empty("empty");


////////////////
// STRUCTURES //
////////////////

#pragma region Struct

/**
 * @brief Surface class reserved structure.
 */
struct OvVK::Surface::Reserved
{
    VkSurfaceKHR surface;                       ///< Vulkan opaque handle to a surface object.
    GLFWwindow* window;                         ///< GLFW window handler
    uint32_t width, height;                     ///< Window dimension.
    bool isReady;                               ///< Boolean to check the status of the surface after a callback functions call.

    // Callbacks:
    KeyboardCallback keyboardCallback;
    MouseCursorCallback mouseCursorCallback;
    MouseButtonCallback mouseButtonCallback;
    MouseScrollCallback mouseScrollCallback;
    WindowSizeCallback windowSizeCallback;

    std::vector<std::pair<uint32_t, std::reference_wrapper<SurfaceDependentObject>>> surfaceDependentObjects;
    /**
     * Constructor.
     */
    Reserved() : surface{ VK_NULL_HANDLE },
        window{ nullptr },
        width{ 0 }, height{ 0 },
        isReady{ false },
        keyboardCallback{ nullptr },
        mouseCursorCallback{ nullptr },
        mouseButtonCallback{ nullptr },
        mouseScrollCallback{ nullptr },
        windowSizeCallback{ nullptr }
    {}
};

#pragma endregion


///////////////////////////
// BODY OF CLASS Surface //
///////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Surface::Surface() : reserved(std::make_unique<OvVK::Surface::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The surface name.
 */
OVVK_API OvVK::Surface::Surface(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::Surface::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Surface::Surface(Surface&& other) : Ov::Object(std::move(other)),
    Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Surface::~Surface()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes surface
 * @return TF
 */
bool OVVK_API OvVK::Surface::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Surface should be destroyed
    if (reserved->surface != VK_NULL_HANDLE)
    {
        // Destroy Vulkan surface
        vkDestroySurfaceKHR(Engine::getInstance().getVulkanInstance().getVkInstance(), reserved->surface, nullptr);
        reserved->surface = VK_NULL_HANDLE;
    }

    // Window should be destroyed
    if (reserved->window != nullptr) 
    {
        // GLFW need to be initialize
        glfwDestroyWindow(reserved->window);
        reserved->window = nullptr;
        reserved->width = 0;
        reserved->height = 0;
    }


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases surface
 * @return TF
 */
bool OVVK_API OvVK::Surface::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Surface should be destroyed
    if (reserved->surface != VK_NULL_HANDLE)
    {
        // Destroy Vulkan surface
        vkDestroySurfaceKHR(Engine::getInstance().getVulkanInstance().getVkInstance(), reserved->surface, nullptr);
        reserved->surface = VK_NULL_HANDLE;
    }

    // Window should be destroyed
    if (reserved->window != nullptr)
    {
        // GLFW need to be initialize
        glfwDestroyWindow(reserved->window);
        reserved->window = nullptr;
        reserved->width = 0;
        reserved->height = 0;
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the surface.
 * @param title The title of the window/surface.
 * @param width The desired width of the window. Can change if glfw need to change it.
 * @param height The desired height of the window. Can change if glfw need to change it.
 * @return TF
 */
bool OVVK_API OvVK::Surface::create(std::string title, uint32_t width, uint32_t height)
{
    // Safety net:
    if (width == 0 || height == 0)
    {
        OV_LOG_ERROR("Width or height is 0.");
        return false;
    }

    // Init
    if (init() == false)
        return false;

    ///////////////////
    // Create Window //
    ///////////////////

    // Creating the actual window
    reserved->window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

    // Check window creation
    if (reserved->window == nullptr)
    {
        OV_LOG_ERROR("Unable to create window");
        this->free();
        return false;
    }

    // Different than specified size?
    int32_t check_width, check_height;
    glfwGetWindowSize(reserved->window, &check_width, &check_height);
    if (check_width != width || check_width != height)
        OV_LOG_WARN("Window resized to %dx%d pixels", check_width, check_height);

    reserved->width = check_width;
    reserved->height = check_height;

    // Store the pointer to this object in GLFWwindow
    glfwSetWindowUserPointer(reserved->window, this);

    // Sets limit minimum
    glfwSetWindowSizeLimits(reserved->window, 2, 2, GLFW_DONT_CARE, GLFW_DONT_CARE);

    ////////////////////
    // Create Surface //
    ////////////////////

    // Engine
    std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Vulkan instance
    std::reference_wrapper<OvVK::VulkanInstance> vulkanInstance = engine.get().getVulkanInstance();

    // The parameters are the VkInstance, GLFW window pointer, custom allocators and pointer to
    // VkSurfaceKHR variable.
    if (glfwCreateWindowSurface(vulkanInstance.get().getVkInstance(), reserved->window, nullptr, &reserved->surface)
        != VK_SUCCESS) {
        OV_LOG_ERROR("Fail to create the window surface!");
        this->free();
        return false;
    }

    
    ///////////////////////
    // Register callback //
    ///////////////////////

    // Keys
    glfwSetKeyCallback(reserved->window, static_cast<GLFWkeyfun>
        (
            // SurfaceCallbackTypes:
            [](GLFWwindow* window, int key, int scancode, int action, int mods)
            {
                Surface* _this = static_cast<Surface*>(glfwGetWindowUserPointer(window));
                _this->keyboardCallback(key, scancode, action, mods);
            }
    ));

    // Cursor position
    glfwSetCursorPosCallback(reserved->window, static_cast<GLFWcursorposfun>
        (
            // SurfaceCallbackTypes:
            [](GLFWwindow* window, double mouseX, double mouseY)
            {
                Surface* _this = static_cast<Surface*>(glfwGetWindowUserPointer(window));
                _this->mouseCursorCallback(mouseX, mouseY);
            }
    ));

    // Mouse button
    glfwSetMouseButtonCallback(reserved->window, static_cast<GLFWmousebuttonfun>
        (
            // SurfaceCallbackTypes:
            [](GLFWwindow* window, int button, int action, int mods)
            {
                Surface* _this = static_cast<Surface*>(glfwGetWindowUserPointer(window));
                _this->mouseButtonCallback(button, action, mods);
            }
    ));

    // Scroll 
    glfwSetScrollCallback(reserved->window, static_cast<GLFWscrollfun>
        (
            // SurfaceCallbackTypes:
            [](GLFWwindow* window, double scrollX, double scrollY)
            {
                Surface* _this = static_cast<Surface*>(glfwGetWindowUserPointer(window));
                _this->mouseScrollCallback(scrollX, scrollY);
            }
    ));


    // size
    glfwSetFramebufferSizeCallback(reserved->window, static_cast<GLFWframebuffersizefun>(
        // SurfaceCallbackTypes:
        [](GLFWwindow* window, int _width, int _height) {
            // The instance value can now be retrieved from within the callback with 
            // glfwGetWindowUserPointer to properly set the flag
            Surface* _this = static_cast<Surface*>(glfwGetWindowUserPointer(window));
            _this->windowSizeCallback(_width, _height);
        }
    ));

    reserved->isReady = true;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the window has been closed by the user.
 * @return TF
 */
bool OVVK_API OvVK::Surface::isClosed() const
{
    // the user closes the window event
    if (glfwWindowShouldClose(reserved->window)) 
        return true;

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the surface is ready after a callback function call. 
 * @return TF
 */
bool OVVK_API OvVK::Surface::isReady() const
{
    return reserved->isReady;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the surface is minimize.
 * @return TF
 */
bool OVVK_API OvVK::Surface::isMinimize() const
{
    int iconified = glfwGetWindowAttrib(reserved->window, GLFW_ICONIFIED);

    return (iconified == GLFW_TRUE) ? true : false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the key corresponding to the passed key code is pressed on the surface.
 * @return TF
 */
bool OVVK_API OvVK::Surface::queryKey(int keyCode) const
{
    if (glfwGetKey(reserved->window, keyCode) == GLFW_PRESS)
        return true;

    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method adds an object that depends on the surface.
 * @param id Id of the dependent obj. The id is given by the OvCore::Object class.
 * Needed for the diamond problem with Object class.
 * @param object Depedent obj.
 * @return TF
 */
bool OVVK_API OvVK::Surface::addSurfaceDependentObject(uint32_t id, SurfaceDependentObject& dependentObject)
{
    std::reference_wrapper<OvVK::SurfaceDependentObject> surfaceDependentObject = dependentObject;

    // Check if already present
    for (uint32_t c = 0; c < reserved->surfaceDependentObjects.size(); c++) {
        if (reserved->surfaceDependentObjects[c].first == id) {
            OV_LOG_WARN("The object (id=%d) dependent to this surface (id=%d) is already present.", id, getId());
            return true;
        }
    }

    reserved->surfaceDependentObjects.push_back(std::make_pair(id, surfaceDependentObject));

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method removes the object with the passed id that depends on the surface.
 * @param id Id of the dependent obj. The id is given by the OvCore::Object class.
 * Needed for the diamond problem with Object class.
 * @return TF
 */
bool OVVK_API OvVK::Surface::removeSurfaceDependentObject(uint32_t id)
{
    if (!reserved->surfaceDependentObjects.empty())
        for (auto it = reserved->surfaceDependentObjects.begin(); it != reserved->surfaceDependentObjects.end(); ++it)
            if (it->first == id)
            {
                reserved->surfaceDependentObjects.erase(it);
                break;
            }

    // Done:
    return true;
}

#pragma endregion

#pragma region CallBack

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets keyboard callback.
 * @param cb Keyboard callback function pointer.
 * @return TF
 */
bool OVVK_API OvVK::Surface::setKeyboardCallback(KeyboardCallback cb)
{
    reserved->keyboardCallback = cb;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets mouse cursor callback.
 * @param cb Mouse callback function pointer.
 * @return TF
 */
bool OVVK_API OvVK::Surface::setMouseCursorCallback(MouseCursorCallback cb)
{
    reserved->mouseCursorCallback = cb;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets mouse button callback.
 * @param cb Mouse button callback function pointer.
 * @return TF
 */
bool OVVK_API OvVK::Surface::setMouseButtonCallback(MouseButtonCallback cb)
{
    reserved->mouseButtonCallback = cb;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets mouse scroll callback.
 * @param cb Mouse scroll callback function pointer.
 * @return TF
 */
bool OVVK_API OvVK::Surface::setMouseScrollCallback(MouseScrollCallback cb)
{
    reserved->mouseScrollCallback = cb;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets Framebuffer resize callback
 * @param cb Window resize callback function pointer.
 * @return TF
 */
bool OVVK_API OvVK::Surface::setWindowSizeCallback(WindowSizeCallback cb) {

    reserved->windowSizeCallback = cb;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to call the keyboard callback set and forward the call to the keyboard callback
 * to the surface-dependent objects. Method called by GLFW.
 * @param key The keyboard key that was pressed or released. 
 * @param scancode The system-specific scancode of the key.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions. 
 * @param mods Bit field describing which modifier keys were held down.
 */
void OvVK::Surface::keyboardCallback(int key, int scancode, int action, int mods)
{
    // Dependent objs
    for (std::pair<uint32_t, std::reference_wrapper<SurfaceDependentObject>> &surface : reserved->surfaceDependentObjects)
        if((surface.second.get().getActiveCallback() & OvVK::Surface::SurfaceCallbackTypes::keyboard)
            == OvVK::Surface::SurfaceCallbackTypes::keyboard)
            surface.second.get().keyboardCallback(key, scancode, action, mods);

    // external staitic callback
    if (reserved->keyboardCallback != nullptr)
        reserved->keyboardCallback(key, scancode, action, mods);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to call the mouse cursor callback set and forward the call to the mouse cursor callback
 * to the surface-dependent objects. Method called by GLFW.
 * @param mouseX The new cursor x-coordinate, relative to the left edge of the content area.
 * @param mouseY The new cursor y-coordinate, relative to the top edge of the content area.
 */
void OvVK::Surface::mouseCursorCallback(double mouseX, double mouseY)
{
    // Dependent objs
    for (std::pair<uint32_t, std::reference_wrapper<SurfaceDependentObject>> &surface : reserved->surfaceDependentObjects)
        if ((surface.second.get().getActiveCallback() & OvVK::Surface::SurfaceCallbackTypes::mouseCursor)
            == OvVK::Surface::SurfaceCallbackTypes::mouseCursor)
            surface.second.get().mouseCursorCallback(mouseX, mouseY);

    // external staitic callback
    if (reserved->mouseCursorCallback != nullptr)
        reserved->mouseCursorCallback(mouseX, mouseY);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to call the mouse button callback set and forward the call to the mouse button callback
 * to the surface-dependent objects. Method called by GLFW.
 * @param button The mouse button that was pressed or released. 
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions. 
 * @param mods Bit field describing which modifier keys were held down.
 */
void OvVK::Surface::mouseButtonCallback(int button, int action, int mods)
{
    // Dependent objs
    for (std::pair<uint32_t, std::reference_wrapper<SurfaceDependentObject>> &surface : reserved->surfaceDependentObjects)
        if ((surface.second.get().getActiveCallback() & OvVK::Surface::SurfaceCallbackTypes::mouseButton)
            == OvVK::Surface::SurfaceCallbackTypes::mouseButton)
            surface.second.get().mouseButtonCallback(button, action, mods);

    // external staitic callback
    if (reserved->mouseButtonCallback != nullptr)
        reserved->mouseButtonCallback(button, action, mods);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to call the mouse scroll callback set and forward the call to the mouse scroll callback
 * to the surface-dependent objects. Method called by GLFW.
 * @param scrollX The scroll offset along the x-axis. 
 * @param scrollY The scroll offset along the y-axis.
 */
void OvVK::Surface::mouseScrollCallback(double scrollX, double scrollY)
{
    // Dependent objs
    for (std::pair<uint32_t, std::reference_wrapper<SurfaceDependentObject>> &surface : reserved->surfaceDependentObjects)
        if ((surface.second.get().getActiveCallback() & OvVK::Surface::SurfaceCallbackTypes::mouseScroll)
            == OvVK::Surface::SurfaceCallbackTypes::mouseScroll)
            surface.second.get().mouseScrollCallback(scrollX, scrollY);

    // external staitic callback
    if (reserved->mouseScrollCallback != nullptr)
        reserved->mouseScrollCallback(scrollX, scrollY);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method used to call the window size callback set and forward the call to the window size callback
 * to the surface-dependent objects. Method called by GLFW.
 * @param width New width.
 * @param height New heigth.
 */
void OvVK::Surface::windowSizeCallback(int width, int height) {

    reserved->isReady = false;

    reserved->width = width;
    reserved->height = height;

    // We wait here all the command in process because the resize of the windows affect all the frames-in-flight.
    // We shouldn't touch resources that may still be in use
    if (vkDeviceWaitIdle(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice()) != VK_SUCCESS)
        OV_LOG_ERROR("Fail to wait all queue to be in idle.");

    if (isMinimize() == false) {
        for (std::pair<uint32_t, std::reference_wrapper<SurfaceDependentObject>>& surface : reserved->surfaceDependentObjects)
            if ((surface.second.get().getActiveCallback() & OvVK::Surface::SurfaceCallbackTypes::windowSize)
                == OvVK::Surface::SurfaceCallbackTypes::windowSize)
                surface.second.get().windowSizeCallback(width, height);

        if (reserved->windowSizeCallback != nullptr)
            reserved->windowSizeCallback(width, height);
    }
    
    reserved->isReady = true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the Vulkan surface.
 * @return Vulkan handle to the surface.
 */
VkSurfaceKHR OVVK_API OvVK::Surface::getVkSurfaceKHR() const 
{
    return reserved->surface;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets width of the window.
 * @return Width of the surface.
 */
uint32_t OVVK_API OvVK::Surface::getWidth() const { return reserved->width; }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets height of the window.
 * @return Height of the surface.
 */
uint32_t OVVK_API OvVK::Surface::getHeight() const { return reserved->height; }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets GLFW window obj.
 * @return GLFW handle to the window used by the surface.
 */
void OVVK_API* OvVK::Surface::getGLFWWindow() const { return reserved->window; }

#pragma endregion