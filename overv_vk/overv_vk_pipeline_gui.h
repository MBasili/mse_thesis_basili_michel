/**
 * @file	overv_vk_pipeline_gui.h
 * @brief	GUI pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for manage the ImGUI libraries throught a pipeline
  */
class OVVK_API GUIPipeline final : public OvVK::Pipeline
{
//////////
public: //
//////////

	// Const/dest:
	GUIPipeline();
	GUIPipeline(GUIPipeline&& other);
	GUIPipeline(GUIPipeline const&) = delete;
	virtual ~GUIPipeline();

	// Operators
	GUIPipeline& operator=(const GUIPipeline&) = delete;
	GUIPipeline& operator=(GUIPipeline&&) = delete;

	// Managed
	virtual bool init() override;
	virtual bool free() override;

	// SurfaceResize
	virtual bool keyboardCallback(int key, int scancode, int action, int mods) override;
	virtual bool mouseCursorCallback(double mouseX, double mouseY) override;
	virtual bool mouseButtonCallback(int button, int action, int mods) override;
	virtual bool mouseScrollCallback(double scrollX, double scrollY) override;
	virtual bool windowSizeCallback(int width, int height) override;

	// General
	bool create() override;

	// DrawGUI
	bool addDrawGUIElement(uint32_t id, Ov::DrawGUI& object);
	bool removeDrawGUIElement(uint32_t id);

	// Get/Set
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkDescriptorPool getDescriptorPool() const;
	VkRenderPass getRenderPass() const;
	const std::vector<VkFramebuffer>& getFramebuffers() const;
#endif

	// Rendering
	virtual bool render(uint32_t value = 0, void* data = nullptr);

	// Static
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	static void checkVkResult(VkResult result);
#endif
	 
///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	GUIPipeline(const std::string& name);
};