/**
 * @file	overv_vk_command_pool.h
 * @brief	Vulkan command pool properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// C/C++
#include <iterator>


////////////
// STATIC //
////////////

// Special values:
OvVK::CommandPool OvVK::CommandPool::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief OvVK CommandPool reserved structure.
 */
struct OvVK::CommandPool::Reserved
{
    VkCommandPool commandPool;                          ///< Vulkan command pool handle.
    CommandTypesFlagBits types;                         ///< Types of commands supported by the command pool.
    VkCommandPoolCreateFlags createFlag;                ///< Flag used to create the commandPool.
    std::optional<uint32_t> queueFamilyIndex;           ///< Queue family index used to create the command pool.

    std::list<OvVK::CommandBuffer> commandBuffers;  ///< List of all allocated command buffers from this command pool.

    /**
     * Constructor.
     */
    Reserved() : commandPool{ VK_NULL_HANDLE },
        types{ CommandTypesFlagBits::none },
        createFlag{ 0x00000000 },
        queueFamilyIndex{ std::nullopt }
    {}
};


///////////////////////////////
// BODY OF CLASS CommandPool //
///////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::CommandPool::CommandPool() : reserved(std::make_unique<OvVK::CommandPool::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The command pool name.
 */
OVVK_API OvVK::CommandPool::CommandPool(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::CommandPool::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::CommandPool::CommandPool(CommandPool&& other) noexcept : Ov::Object(std::move(other)),
    Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::CommandPool::~CommandPool()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes the OvVK CommandPool.
 * @return TF
 */
bool OVVK_API OvVK::CommandPool::init()
{
    // Call base method
    if (this->Ov::Managed::init() == false)
        return false;


    // Destroy command pool
    if (reserved->commandPool != VK_NULL_HANDLE)
    {
        // Free commandBuffers
        for (auto& commandBuffer : reserved->commandBuffers)
            commandBuffer.free();

        reserved->commandBuffers.clear();

        // Destroy commandPool
        vkDestroyCommandPool(Engine::getInstance().getLogicalDevice().getVkDevice(),reserved->commandPool, nullptr);

        reserved->commandPool = VK_NULL_HANDLE;
        reserved->types = CommandTypesFlagBits::none;
        reserved->createFlag = 0x00000000;
        reserved->queueFamilyIndex = std::nullopt;
    }


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the OvVK CommandPool.
 * @return TF
 */
bool OVVK_API OvVK::CommandPool::free()
{
    // Call base method
    if (this->Ov::Managed::free() == false)
        return false;


    // Destroy command pool
    if (reserved->commandPool != VK_NULL_HANDLE)
    {
        // Free commandBuffers
        for (auto& commandBuffer : reserved->commandBuffers)
            commandBuffer.free();

        reserved->commandBuffers.clear();

        // Destroy commandPool
        vkDestroyCommandPool(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->commandPool, nullptr);

        reserved->commandPool = VK_NULL_HANDLE;
        reserved->types = CommandTypesFlagBits::none;
        reserved->createFlag = 0x00000000;
        reserved->queueFamilyIndex = std::nullopt;
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncObj

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the command pool is locked.
 * @return TF
 */
bool OVVK_API OvVK::CommandPool::isLock() const
{
    // Lock if one of the command buffer is lock.
    std::list<OvVK::CommandBuffer>::iterator it;
    for (it = reserved->commandBuffers.begin(); it != reserved->commandBuffers.end(); ++it)
        if (it->isLock()) return true;

    // Done:
    return false;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the command pool.
 * @param typesCP The types of commands supported by the command pool.
 * @param flag Flag specify usage behavior for the command pool.
 * @return TF
 */
bool OVVK_API OvVK::CommandPool::create(CommandTypesFlagBits typesCP, VkCommandPoolCreateFlags flag)
{
    // Safety net:
    if (isLock()) {
        OV_LOG_ERROR("Can't recreate command pool with id= %d if some of the command buffers allocated is lock.", getId());
        return false;
    }

    if (init() == false)
        return false;

    // Logicel device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = Engine::getInstance().getLogicalDevice();

    // Create command pool
    VkCommandPoolCreateInfo poolInfo;
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.pNext = NULL;
    poolInfo.flags = flag;

    // Get queue index family
    std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamiliesIndices =
        logicalDevice.get().getQueueFamiliesIndices();

    if (queueFamiliesIndices.has_value() == false) 
    {
        OV_LOG_ERROR("Fail to retriever the queue families indices from the Overv::Vulkan logical device with id= %d",
            logicalDevice.get().getId());
        return false;
    }

    // All bits combinations
    CommandTypesFlagBits allTypes =
        CommandTypesFlagBits::Graphic | CommandTypesFlagBits::Compute |
        CommandTypesFlagBits::Present | CommandTypesFlagBits::Transfer;


    if ((typesCP & allTypes) == CommandTypesFlagBits::Graphic ||
        (typesCP & allTypes) == (CommandTypesFlagBits::Graphic | CommandTypesFlagBits::Transfer)) 
    {
        if (queueFamiliesIndices.value().graphicsFamily.has_value() == false)
        {
            OV_LOG_ERROR("The Overv::Vulkan logical device with id= %d has no graphic queue available.", getId());
            return false;
        }
        poolInfo.queueFamilyIndex = queueFamiliesIndices.value().graphicsFamily.value();
    }
    else if ((typesCP & allTypes) == CommandTypesFlagBits::Compute ||
        (typesCP & allTypes) == (CommandTypesFlagBits::Compute | CommandTypesFlagBits::Transfer))
    {
        if (queueFamiliesIndices.value().computeFamily.has_value() == false)
        {
            OV_LOG_ERROR("The Overv::Vulkan logical device with id= %d has no compute queue available.", getId());
            return false;
        }
        poolInfo.queueFamilyIndex = queueFamiliesIndices.value().computeFamily.value();
    }
    else if ((typesCP & CommandTypesFlagBits::Present) == CommandTypesFlagBits::Present) 
    {
        if (queueFamiliesIndices.value().presentFamily.has_value() == false)
        {
            OV_LOG_ERROR("The Overv::Vulkan logical device with id= %d has no present queue available.", getId());
            return false;
        }
        poolInfo.queueFamilyIndex = queueFamiliesIndices.value().presentFamily.value();
    }
    else if ((typesCP & CommandTypesFlagBits::Transfer) == CommandTypesFlagBits::Transfer)
    {
        if (queueFamiliesIndices.value().transferFamily.has_value() == false)
        {
            OV_LOG_ERROR("The Overv::Vulkan logical device with id= %d has no transfer queue available.", getId());
            return false;
        }
        poolInfo.queueFamilyIndex = queueFamiliesIndices.value().transferFamily.value();
    }
    else{
        OV_LOG_ERROR("The combination of command types passed is invalid in command pool with id= %d.", getId());
        return false;
    }

    if (vkCreateCommandPool(logicalDevice.get().getVkDevice(), &poolInfo,
        nullptr, &reserved->commandPool) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create command pool with id= %d", getId());
        this->free();
        return false;
    }

    reserved->queueFamilyIndex = poolInfo.queueFamilyIndex;
    reserved->types = typesCP;
    reserved->createFlag = flag;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Resets the command pool.
 * @param flag Flag to control the behavior of the command pool reset.
 * @return TF
 */
bool OVVK_API OvVK::CommandPool::reset(VkCommandPoolResetFlags flag)
{
    // Safety net:
    if (reserved->commandPool == VK_NULL_HANDLE) {
        OV_LOG_ERROR("Command pool not created.");
        return false;
    }

    if (vkResetCommandPool(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->commandPool, flag) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to reset command pool with id= %d", getId());
        return false;
    }

    if ((flag & VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT) == VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT) {
        
        for (auto& commandBuffer : reserved->commandBuffers)
            commandBuffer.free();

        reserved->commandBuffers.clear();
    }

    // Done: 
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Allocates a command buffer on the command pool.
 * @param level Level of the command buffer. If primary o secondary command buffer. 
 * @return The command buffer OvVK obj allocated.
 */
OvVK::CommandBuffer OVVK_API& OvVK::CommandPool::allocateCommandBuffer(VkCommandBufferLevel level)
{
    OvVK::CommandBuffer commandBuffer;

    // Allocate command Buffer
    if (commandBuffer.allocate(*this, level) == false)
    {
        OV_LOG_ERROR("Fail to allocate command buffer from the command pool with id= %d.", getId());
        return OvVK::CommandBuffer::empty;
    }

    reserved->commandBuffers.push_back(std::move(commandBuffer));

    return reserved->commandBuffers.back();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees from the command pool the command buffer with the same id of the passed command buffer. The passed 
 * command buffer is destroy because the command pool own it.
 * @param commandBuffer The reference to the command buffer to free.
 * @return TF
 */
bool OVVK_API OvVK::CommandPool::freeCommandBuffer(const OvVK::CommandBuffer& commandBuffer)
{
    // Safety:
    if (commandBuffer == OvVK::CommandBuffer::empty)
    {
        OV_LOG_ERROR("Can't free empty command buffer in command pool with id= %d.", getId());
        return false;
    }
    if(reserved->commandBuffers.size() == 0)
    {
        OV_LOG_ERROR("In command pool with id= %d no command buffer allocated.", getId());
        return false;
    }
    if (commandBuffer.isLock()) {
        OV_LOG_ERROR("The passed command buffer is lock and can't be remove in command pool with id= %d.", getId());
        return false;
    }

    for (std::list<OvVK::CommandBuffer>::iterator it = reserved->commandBuffers.begin();
        it != reserved->commandBuffers.end(); it++)
    {
        if (it->getId() == commandBuffer.getId())
        {
            it->free();
            reserved->commandBuffers.erase(it);
            return true;
        }
    }

    // Done:
    OV_LOG_ERROR("Command buffer not found in command pool with id= %d.", getId());
    return false;
}

#pragma endregion

#pragma region  Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the Vulkan handle to the command pool.
 * @return Vulkan handle to the command pool.
 */
VkCommandPool OVVK_API OvVK::CommandPool::getVkCommandPool() const 
{
    return reserved->commandPool;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the types of commands supported by the command pool.
 * @return The types of commands supported by the command pool.
 */
OvVK::CommandPool::CommandTypesFlagBits OVVK_API
    OvVK::CommandPool::getCommandTypesSupported() const
{
    return reserved->types;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the flag specifying usage behavior for a command pool.
 * @return The flag specifying usage behavior for a command pool.
 */
VkCommandPoolCreateFlags OVVK_API
OvVK::CommandPool::getCommandPoolCreateFlags() const
{
    return reserved->createFlag;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a vector of command buffers references allocated on the command pool.
 * @return All the command buffers references allocated on the command pool.
 */
std::vector<std::reference_wrapper<OvVK::CommandBuffer>> OVVK_API OvVK::CommandPool::getCommandBuffers() const
{
    std::vector<std::reference_wrapper<OvVK::CommandBuffer>> commandBuffers(reserved->commandBuffers.size(),
        OvVK::CommandBuffer::empty);

    for (OvVK::CommandBuffer& commandBuffer : reserved->commandBuffers)
        commandBuffers.push_back(commandBuffer);

    return commandBuffers;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the reference to the command buffer saved at the passed index.
 * @param index Index of the command buffer to return.
 * @return Reference to the command buffer in the passed position.
 */
OvVK::CommandBuffer OVVK_API& OvVK::CommandPool::getCommandBuffer(uint32_t index) const 
{
    //Safety net:
    if (reserved->commandBuffers.size() <= index) 
    {
        OV_LOG_ERROR("Command buffer index out of range in command pool with id= %d", getId());
        return OvVK::CommandBuffer::empty;
    }

    auto commandBuffer = reserved->commandBuffers.begin();

    std::advance(commandBuffer, index);

    return *commandBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the number of command buffers allocated on the command pool.
 * @return uint32_t
 */
uint32_t OVVK_API OvVK::CommandPool::getNrCommandBuffers() const 
{
    return static_cast<uint32_t>(reserved->commandBuffers.size());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the queue family index of the queue used to allocated the command pool.
 * @return Optional std obj containing the index of the queue used to allocated the command pool.
 */
std::optional<uint32_t> OVVK_API OvVK::CommandPool::getQueueFamilyIndex() const
{
    return reserved->queueFamilyIndex;
}

#pragma endregion
