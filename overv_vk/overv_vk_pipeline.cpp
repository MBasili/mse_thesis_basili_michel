/**
 * @file	overv_vk_pipeline.cpp
 * @brief	Vulkan pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::Pipeline OvVK::Pipeline::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Pipeline class reserved structure.
 */
struct OvVK::Pipeline::Reserved
{
    VkPipeline pipeline;                                                        ///< Pipeline Vulkan handle obj.
    VkPipelineLayout pipelineLayout;                                            ///< PipelineLayout Vulkan handle obj. 

    VkFence statusSyncObjsCPU[OvVK::Engine::nrFramesInFlight];              ///< Status sync obj side CPU.
    VkSemaphore statusSyncObjsGPU[OvVK::Engine::nrFramesInFlight];          ///< Status sync obj side GPU.
    VkSemaphore statusSyncObjsPresentation[OvVK::Engine::nrFramesInFlight]; ///< Status sync obj side Presentation.

    std::list<std::reference_wrapper<OvVK::Pipeline>> pipelinesToWait;      ///< Pipeline on with this depend on.

    // This variable is used to wait in place at each frame for the completion of commands submitted by
    // the pipeline.
    // This is only necessary if the time taken to process the commands submitted to the device needs to be measured.
    // If this variable is set to FALSE all commands submitted by the pipeline during the
    // processing of a frame are waited for, depending on the value of the static variable 
    // OvVK::Engine::waitCommandsAtTheEndOfAFrame, either at the end of the processing or at the 
    // beginning of the next processing of the frame in which they are submitted.
    bool analysingTimeRequired;                                                 ///< Waits in place for submitted commands.

    /**
     * Constructor
     */
    Reserved() : pipeline{ VK_NULL_HANDLE },
        pipelineLayout{ VK_NULL_HANDLE },
        statusSyncObjsCPU{ VK_NULL_HANDLE },
        statusSyncObjsGPU{ VK_NULL_HANDLE },
        statusSyncObjsPresentation{ VK_NULL_HANDLE },
        analysingTimeRequired{ false }
    {}
};


////////////////////////////
// BODY OF CLASS Pipeline //
////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Pipeline::Pipeline() :
    reserved(std::make_unique<OvVK::Pipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::Pipeline::Pipeline(const std::string& name) : Ov::Pipeline(name),
    reserved(std::make_unique<OvVK::Pipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Pipeline::Pipeline(Pipeline&& other) :
    Ov::Pipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Pipeline::~Pipeline()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes pipeline.
 * @return TF
 */
bool OVVK_API OvVK::Pipeline::init()
{
    if (this->Ov::Pipeline::init() == false)
        return false;


    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // PhysicalDevice
    VkDevice logicalDevice = engine.get().getLogicalDevice().getVkDevice();
    // Frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();


    // Remove the pipeline from the active pipeline in the engine
    Engine::getInstance().removePipeline(*this);

    // Remove call back function for surface resize
    if (getIsActive())
    {
        Engine::getInstance().getPresentSurface().removeSurfaceDependentObject(getId());
        setIsActive(false);
    }

    if (reserved->pipeline != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(logicalDevice, reserved->pipeline, NULL);
        reserved->pipeline = VK_NULL_HANDLE;
    }

    if (reserved->pipelineLayout != VK_NULL_HANDLE)
    {
        vkDestroyPipelineLayout(logicalDevice, reserved->pipelineLayout, NULL);
        reserved->pipelineLayout = VK_NULL_HANDLE;
    }


    // Create sync objs.
    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.pNext = VK_NULL_HANDLE;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    // Create new timeline semaphore and delete previous one.
    VkSemaphoreTypeCreateInfo timelineCreateInfo = {};
    timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    timelineCreateInfo.pNext = VK_NULL_HANDLE;
    timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    timelineCreateInfo.initialValue = OvVK::RTScene::startValuePassStatusSyncObj;

    VkSemaphoreCreateInfo semaphoreInfoTimeline = {};
    semaphoreInfoTimeline.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfoTimeline.pNext = &timelineCreateInfo;
    semaphoreInfoTimeline.flags = 0;

    VkSemaphoreCreateInfo semaphoreInfoBinary = {};
    semaphoreInfoBinary.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfoBinary.pNext = VK_NULL_HANDLE;
    semaphoreInfoBinary.flags = 0;

    for (uint32_t i = 0; i < OvVK::Engine::nrFramesInFlight; i++) {

        // Destroy CPU sync obj.
        vkDestroyFence(logicalDevice, reserved->statusSyncObjsCPU[i], nullptr);
        if (vkCreateFence(logicalDevice, &fenceCreateInfo, NULL,
            &reserved->statusSyncObjsCPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to create the fence of the frame in flight number %d of the pipeline with id= %d.",
                frameInFlight, getId());
            return false;
        }

        // Destroy GPU sync objs.
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObjsGPU[i], nullptr);
        if (vkCreateSemaphore(logicalDevice, &semaphoreInfoTimeline,
            nullptr, &reserved->statusSyncObjsGPU[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the rendering GPU sync obj of the frame in flight number %d
 of the pipeline with id= %d.)",
                frameInFlight, getId());
            return false;
        }

        vkDestroySemaphore(logicalDevice, reserved->statusSyncObjsPresentation[i], nullptr);
        if (vkCreateSemaphore(logicalDevice, &semaphoreInfoBinary,
            nullptr, &reserved->statusSyncObjsPresentation[i]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the presentation GPU sync obj of the frame in flight number %d
 of the pipeline with id= %d.)",
                frameInFlight, getId());
            return false;
        }
    }

    reserved->pipelinesToWait.clear();


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees pipeline.
 * @return TF
 */
bool OVVK_API OvVK::Pipeline::free()
{
    if (this->Ov::Pipeline::free() == false)
        return false;


    // Logical Device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Remove the pipeline from the active pipeline in the engine
    Engine::getInstance().removePipeline(*this);

    // Remove call back function for surface resize
    if (getIsActive())
    {
        Engine::getInstance().getPresentSurface().removeSurfaceDependentObject(getId());
        setIsActive(false);
    }

    if (reserved->pipeline != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(logicalDevice, reserved->pipeline, NULL);
        reserved->pipeline = VK_NULL_HANDLE;
    }

    if (reserved->pipelineLayout != VK_NULL_HANDLE)
    {
        vkDestroyPipelineLayout(logicalDevice, reserved->pipelineLayout, NULL);
        reserved->pipelineLayout = VK_NULL_HANDLE;
    }

    for (uint32_t i = 0; i < OvVK::Engine::nrFramesInFlight; i++) {

        // Destroy CPU sync obj.
        vkDestroyFence(logicalDevice, reserved->statusSyncObjsCPU[i], nullptr);
        reserved->statusSyncObjsCPU[i] = VK_NULL_HANDLE;

        // Destroy GPU sync objs.
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObjsGPU[i], nullptr);
        reserved->statusSyncObjsGPU[i] = VK_NULL_HANDLE;

        vkDestroySemaphore(logicalDevice, reserved->statusSyncObjsPresentation[i], nullptr);
        reserved->statusSyncObjsPresentation[i] = VK_NULL_HANDLE;
    }

    reserved->pipelinesToWait.clear();


    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create pipeline.
 * @return TF
 */
bool OVVK_API OvVK::Pipeline::create()
{
    OV_LOG_ERROR("The create method of the Pipeline class.");

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up some dedicate commandBuffers to reuse each frame.
 * @return TF
 */
bool OVVK_API OvVK::Pipeline::setUpCommandBuffers()
{
    OV_LOG_ERROR("The setUpCommandBuffers method of the Pipeline class.");

    // Done:
    return false;
}

#pragma endregion

#pragma region Management

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a pipeline to wait before rendering.
 */
void OVVK_API OvVK::Pipeline::addPipelineToWait(OvVK::Pipeline& pipeline) 
{
    if (!reserved->pipelinesToWait.empty())
        for (auto it = reserved->pipelinesToWait.begin(); it != reserved->pipelinesToWait.end(); ++it)
            if (it->get().getId() == pipeline.getId())
            {
                OV_LOG_ERROR("The passed pipeline with id =%d has already been added", pipeline.getId());
                break;
            }

    reserved->pipelinesToWait.push_back(pipeline);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Remove pipeline to wait.
 */
void OVVK_API OvVK::Pipeline::removePipelineToWait(OvVK::Pipeline& pipeline) 
{
    if (!reserved->pipelinesToWait.empty())
        for (auto it = reserved->pipelinesToWait.begin(); it != reserved->pipelinesToWait.end(); ++it)
            if (it->get().getId() == pipeline.getId())
            {
                reserved->pipelinesToWait.erase(it);
                break;
            }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Reset pipeline to wait.
 */
void OVVK_API OvVK::Pipeline::resetPipelineToWait() 
{
    reserved->pipelinesToWait.clear();
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the pipeline Vulkan handle.
 * @return Pipeline Vulkan handle.
 */
VkPipeline OVVK_API OvVK::Pipeline::getVkPipeline() const 
{
    return reserved->pipeline;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the pipelineLayout Vulkan handle.
 * @return PipelineLayout Vulkan handle.
 */
VkPipelineLayout OVVK_API OvVK::Pipeline::getVkPipelineLayout() const 
{
    return reserved->pipelineLayout;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the pipeline Vulkan handle pointer.
 * @return Pipeline Vulkan handle pointer.
 */
VkPipeline OVVK_API* OvVK::Pipeline::getVkPipelinePointer() const
{
    return &reserved->pipeline;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the pipelineLayout Vulkan handle pointer.
 * @return PipelineLayout Vulkan handle pointer.
 */
VkPipelineLayout OVVK_API* OvVK::Pipeline::getVkPipelineLayoutPointer() const
{
    return &reserved->pipelineLayout;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the GPU side status sync objs to wait for a specific frame in flight.
 * @return Vector containing GPU sync status objs.
 */
std::vector<VkSemaphore> OVVK_API OvVK::Pipeline::getStatusSyncObjsGPUToWait() const 
{
    std::vector<VkSemaphore> toWait;

    for (auto pipeline : reserved->pipelinesToWait)
        toWait.push_back(pipeline.get().getStatusSyncObjGPU());

    return toWait;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the CPU side status sync obj of a specific frame in flight.
 * @return CPU side status sync obj
 */
VkFence OVVK_API OvVK::Pipeline::getStatusSyncObjCPU() const
{
    return reserved->statusSyncObjsCPU[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the CPU side status sync objs to wait.
 * @return Vector containing CPU sync status objs.
 */
std::vector<VkFence> OVVK_API OvVK::Pipeline::getStatusSyncObjsCPU() const
{
    return std::vector<VkFence>(std::begin(reserved->statusSyncObjsCPU), std::end(reserved->statusSyncObjsCPU));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the GPU side status sync obj of a specific frame in flight.
 * @return GPU side status sync obj
 */
VkSemaphore OVVK_API OvVK::Pipeline::getStatusSyncObjGPU() const
{
    return reserved->statusSyncObjsGPU[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the GPU side status sync objs to wait.
 * @return Vector containing GPU sync status objs.
 */
std::vector<VkSemaphore> OVVK_API OvVK::Pipeline::getStatusSyncObjsGPU() const
{
    return std::vector<VkSemaphore>(std::begin(reserved->statusSyncObjsGPU), std::end(reserved->statusSyncObjsGPU));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the GPU side presentation status sync obj of a specific frame in flight.
 * @return GPU side presentation status sync obj
 */
VkSemaphore OVVK_API OvVK::Pipeline::getStatusSyncObjPresentation() const
{
    return reserved->statusSyncObjsPresentation[OvVK::Engine::getInstance().getFrameInFlight()];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the GPU side presentation status sync objs.
 * @return Vector containing GPU side presentation status sync objs.
 */
std::vector<VkSemaphore> OVVK_API OvVK::Pipeline::getStatusSyncObjsPresentation() const
{
    return std::vector<VkSemaphore>(std::begin(reserved->statusSyncObjsPresentation),
        std::end(reserved->statusSyncObjsPresentation));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Retrive the variable used to wait in place at each frame for the completion of commands submitted by the pipeline.
 * @return TF
 */
bool OVVK_API OvVK::Pipeline::getAnalysingTimeRequired() const {
    return reserved->analysingTimeRequired;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the variable used to wait in place at each frame for the completion of commands submitted by the pipeline.
 * @return TF
 */
void OVVK_API OvVK::Pipeline::setAnalysingTimeRequired(bool value) {
    reserved->analysingTimeRequired = value;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the pipeline Vulkan handle.
 * @param pipeline The pipeline Vulkan handle.
 */
void OVVK_API OvVK::Pipeline::setVkPipeline(VkPipeline pipeline) 
{
    reserved->pipeline = pipeline;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the pipeline layout Vulkan handle.
 * @param pipelineLayout The pipeline layout Vulkan handle.
 */
void OVVK_API OvVK::Pipeline::setVkPipelineLayout(VkPipelineLayout pipelineLayout)
{
    reserved->pipelineLayout = pipelineLayout;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the rendering status of the GPU status sync obj.
 * @param value The uint value rapresenting the status the rendering is in.
 * @return TF
 */
bool OVVK_API OvVK::Pipeline::setStatusSyncObjGPU(uint64_t value)
{
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // LogicalDevice
    VkDevice logicalDevice = engine.get().getLogicalDevice().getVkDevice();
    // Frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

    // Safety net:
    if (reserved->statusSyncObjsGPU[frameInFlight] == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Rendering sync obj not created in the pipeline with id= %d in the frame in flight %d",
            getId(), frameInFlight);
        return false;
    }

    // Retrievers current sempahore value
    uint64_t currentValue;
    vkGetSemaphoreCounterValue(logicalDevice, reserved->statusSyncObjsGPU[frameInFlight], &currentValue);

    if (currentValue > value) {
        // Destroy semaphore because can't decrease the value
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObjsGPU[frameInFlight], nullptr);
        reserved->statusSyncObjsGPU[frameInFlight] = VK_NULL_HANDLE;

        // Create new timeline semaphore and delete previous one.
        VkSemaphoreTypeCreateInfo timelineCreateInfo;
        timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        timelineCreateInfo.pNext = NULL;
        timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        timelineCreateInfo.initialValue = value;

        VkSemaphoreCreateInfo semaphoreInfo;
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreInfo.pNext = &timelineCreateInfo;
        semaphoreInfo.flags = 0;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &reserved->statusSyncObjsGPU[frameInFlight])
            != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Failed to signal rendering GPU status synchronization object for the pipeline with id= %d
 in the frame in flight %d)", getId(), frameInFlight);
            return false;
        }
    }
    else if (currentValue < value) {
        VkSemaphoreSignalInfo signalInfo;
        signalInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
        signalInfo.pNext = NULL;
        signalInfo.semaphore = reserved->statusSyncObjsGPU[frameInFlight];
        signalInfo.value = value;

        if (vkSignalSemaphore(logicalDevice, &signalInfo) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Failed to signal rendering GPU status synchronization object for the pipeline with id= %d
 in the frame in flight %d)", getId(), frameInFlight);
            return false;
        }
    }

    // Done:
    return true;
}

#pragma endregion