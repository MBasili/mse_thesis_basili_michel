/**
 * @file	overv_vk_pipeline_picking.cpp
 * @brief	Picking pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum:
#include "magic_enum.hpp"


/////////////
// SHADERS //
/////////////

#pragma region Shaders

#pragma region RGen

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline ray generation.
 */
static const std::string default_rgen = R"(

$include RTObjDesc$
$include Storage$
$include PickingPipeline$

// Payloads
layout(location = 0) rayPayloadEXT PickingData prd;

void main()
{   
    if( pickingBuffer.data[0].pickingCoords.xy == gl_LaunchIDEXT.xy) {
        
        const vec2 inUV        = vec2(gl_LaunchIDEXT.xy) / vec2(gl_LaunchSizeEXT.xy);
        vec2       d           = inUV * 2.0 - 1.0;

        vec4 origin    = globalUniform.viewInverse * vec4(0, 0, 0, 1);
        vec4 target    = globalUniform.projInverse * vec4(d.x, d.y, 1, 1);
        vec4 direction = globalUniform.viewInverse * vec4(normalize(target.xyz), 0);

        uint  rayFlags = gl_RayFlagsNoneEXT;
        float tMin     = 0.001;
        float tMax     = 10000.0;

        traceRayEXT(topLevelAS,     // acceleration structure
                    rayFlags,       // rayFlags
                    0xFF,           // cullMask
                    0,              // sbtRecordOffset
                    0,              // sbtRecordStride
                    0,              // missIndex
                    origin.xyz,     // ray origin
                    tMin,           // ray min range
                    direction.xyz,  // ray direction
                    tMax,           // ray max range
                    0               // payload (location = 0)
        );

        if(prd.pickedSomething > 0) {
            pickingBuffer.data[0].geometryId = prd.geometryId;
            pickingBuffer.data[0].rtObjDescId = prd.rtObjDescId;
            pickingBuffer.data[0].pickedSomething = 1;
        }
    }
})";

#pragma endregion

#pragma region RAHits

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline any hit shaders. Empty
 */
static const std::string empty_rahit = R"( 

void main() 
{ 

})";

#pragma endregion

#pragma region RCHits

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline closest hit shaders. Empty
 */
static const std::string empty_rchit = R"( 

void main() 
{ 

})";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline closest hit shader for primary and secondary rays.
 */
static const std::string default_rchit = R"(

$include RTObjDesc$
$include Storage$
$include PickingPipeline$

// Payloads
rayPayloadInEXT PickingData prd;

void main()
{   

    /////////////////
    // Object data //
    /////////////////

    RTObjDescData rtObjDesc = rtObjDescs.data[gl_InstanceCustomIndexEXT];
    GeometriesData geometriesData = GeometriesData(rtObjDesc.geometryDataAddress);
    GeometryData geometryData = geometriesData.geometriesData[gl_GeometryIndexEXT];

    prd.geometryId = geometryData.geometryId;
    prd.rtObjDescId = geometryData.rtObjDescId;
    prd.pickedSomething = 1;

})";

#pragma endregion

#pragma region RMiss

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline miss shaders. Empty
 */
static const std::string empty_rmiss = R"( 

void main() 
{ 

})";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RayTracing pipeline miss shader. Default
 */
static const std::string default_rmiss = R"(

$include RTObjDesc$
$include Storage$
$include PickingPipeline$

// Payloads
layout(location = 0) rayPayloadInEXT PickingData prd;

void main()
{   
    prd.geometryId = 0;
    prd.rtObjDescId = 0;
    prd.pickedSomething = 0;
})";


#pragma endregion

#pragma endregion


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Pipeline class reserved structure.
 */
struct OvVK::PickingPipeline::Reserved
{
    // Descriptor sets
    OvVK::PickingDescriptorSetsManager pickingDescriptorSetsManager;

    // Buffer containing data for the picking
    OvVK::Buffer pickingBuffers[OvVK::Engine::nrFramesInFlight];    

    // Sync Obj. to sync GPU operation
    VkSemaphore transferPreviousRTStatusSyncObjs[OvVK::Engine::nrFramesInFlight];
    VkSemaphore graphicPreviousRTStatusSyncObjs[OvVK::Engine::nrFramesInFlight];

    // Coordinates used to pick
    glm::vec2 pickingCoords;

    /**
     * Constructor
     */
    Reserved() : transferPreviousRTStatusSyncObjs { VK_NULL_HANDLE },
        graphicPreviousRTStatusSyncObjs{ VK_NULL_HANDLE },
        pickingCoords{ 0.0f }
    {}
};


///////////////////////////////////
// BODY OF CLASS PickingPipeline //
///////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::PickingPipeline::PickingPipeline() :
    reserved(std::make_unique<OvVK::PickingPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name RTPipeline name
 */
OVVK_API OvVK::PickingPipeline::PickingPipeline(const std::string& name) : OvVK::RayTracingPipeline(name),
reserved(std::make_unique<OvVK::PickingPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::PickingPipeline::PickingPipeline(PickingPipeline&& other) :
    OvVK::RayTracingPipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::PickingPipeline::~PickingPipeline()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes pipeline.
 * @return TF
 */
bool OVVK_API OvVK::PickingPipeline::init()
{
    if (this->OvVK::RayTracingPipeline::init() == false)
        return false;


    // Free DSM
    if (reserved->pickingDescriptorSetsManager.free() == false)
        return false;

    // Free buffers memory
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
        if (reserved->pickingBuffers[c].free() == false)
            return false;


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees pipeline.
 * @return TF
 */
bool OVVK_API OvVK::PickingPipeline::free()
{
    if (this->OvVK::RayTracingPipeline::free() == false)
        return false;


    // Free DSM
    if (reserved->pickingDescriptorSetsManager.free() == false)
        return false;

    // Free buffers memory
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
        if (reserved->pickingBuffers[c].free() == false)
            return false;


    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates picking pipeline.
 * @return TF
 */
bool OVVK_API OvVK::PickingPipeline::create()
{
    // Init
    if (this->init() == false)
        return false;

    // Engine
    std::reference_wrapper<OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Presentation surface
    std::reference_wrapper<OvVK::Surface> presentSurface = engine.get().getPresentSurface();


    // Init picking DMS
    if (reserved->pickingDescriptorSetsManager.create() == false)
    {
        OV_LOG_ERROR("Fail to initialize picking DSM in the pipeline with id= %d.", getId());
        this->free();
        return false;
    }


    ///////////////////
    // PickingBuffer //
    ///////////////////

    // Create buffer
    VkBufferCreateInfo pickingVkBufferCreateInfo{};
    pickingVkBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device
    //      address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    // VK_BUFFER_USAGE_STORAGE_BUFFER_BIT specifies that the buffer can be used in a VkDescriptorBufferInfo suitable
    //      for occupying a VkDescriptorSet slot either of type VK_DESCRIPTOR_TYPE_STORAGE_BUFFER 
    //      or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC.
    pickingVkBufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    // Size of the buffer
    pickingVkBufferCreateInfo.size = sizeof(OvVK::PickingPipeline::PickingData);
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
    // from multiple queue families is supported.
    pickingVkBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    pickingVkBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    pickingVkBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo pickingVmaAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    pickingVmaAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    pickingVmaAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;


    for (uint32_t i = 0; i < Engine::nrFramesInFlight; i++)
    {
        // Create buffer
        if (reserved->pickingBuffers[i].create(pickingVkBufferCreateInfo, pickingVmaAllocationCreateInfo) == false ||
            reserved->pickingBuffers[i].setStatusSyncObjValue(
                OvVK::PickingPipeline::defaultPickingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to create picking buffer for frame in flight %d in the pipeline with id= %d.", i,
                getId());
            this->free();
            return false;
        }

        // Update DescriptorSet
        OvVK::PickingDescriptorSetsManager::UpdateData updateData = {};
        updateData.targetFrame = i;
        updateData.pickingBuffer = reserved->pickingBuffers[i];

        reserved->pickingDescriptorSetsManager.addToUpdateList(updateData);
    }


    //////////////////
    // Shaders init //
    //////////////////

    // Raygen shader
    std::reference_wrapper<OvVK::Shader> raygenShader = getRayGenShader();
    raygenShader.get().setName("raygenShader");
    if (raygenShader.get().load(Ov::Shader::Type::ray_generation, default_rgen) == false)
    {
        this->free();
        return false;
    }

    // AnyHit Shaders
    std::reference_wrapper<std::map<std::string, OvVK::Shader>> anyhitShaders = getAnyHitShaders();
    std::reference_wrapper<OvVK::Shader> emptyRAHit = OvVK::Shader::empty;
	{
		OvVK::Shader emptyAnyHitShader;
		emptyAnyHitShader.setName("emptyAnyHitShader");
		if (emptyAnyHitShader.load(Ov::Shader::Type::any_hit, empty_rahit) == false)
		{
			this->free();
			return false;
		}

		anyhitShaders.get().insert(std::make_pair("emptyAnyHitShader", std::move(emptyAnyHitShader)));
		emptyRAHit = anyhitShaders.get()["emptyAnyHitShader"];
	}

    // ClosestHit shaders
    std::reference_wrapper<std::map<std::string, OvVK::Shader>> closesthitShaders = getClosestHitShaders();
    std::reference_wrapper<OvVK::Shader> defaultRCHit = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> emptyRCHit = OvVK::Shader::empty;
    {
        // Default close hit shader for primary and secondary rays
        OvVK::Shader defaultCloseHitShader;
        defaultCloseHitShader.setName("defaultCloseHitShader");
        if (defaultCloseHitShader.load(Ov::Shader::Type::closes_hit, default_rchit) == false)
        {
            this->free();
            return false;
        }

        closesthitShaders.get().insert(std::make_pair("defaultCloseHitShader", std::move(defaultCloseHitShader)));
        defaultRCHit = closesthitShaders.get()["defaultCloseHitShader"];

        // Empty close hit shader
        OvVK::Shader emptyCloseHitShader;
        emptyCloseHitShader.setName("emptyCloseHitShader");
        if (emptyCloseHitShader.load(Ov::Shader::Type::closes_hit, empty_rchit) == false)
        {
            this->free();
            return false;
        }

        closesthitShaders.get().insert(std::make_pair("emptyCloseHitShader", std::move(emptyCloseHitShader)));
        emptyRCHit = closesthitShaders.get()["emptyCloseHitShader"];
    }

    // Miss shaders
    std::reference_wrapper<std::map<std::string, OvVK::Shader>> misshitShaders = getMissHitShaders();
    std::reference_wrapper<OvVK::Shader> defaultRMiss = OvVK::Shader::empty;
    std::reference_wrapper<OvVK::Shader> emptyRMiss = OvVK::Shader::empty;
    {
        // Default miss shader for all rays
        OvVK::Shader defaultMissShader;
        defaultMissShader.setName("defaultMissShader");
        if (defaultMissShader.load(Ov::Shader::Type::miss, default_rmiss) == false)
        {
            this->free();
            return false;
        }

        misshitShaders.get().insert(std::make_pair("defaultMissShader", std::move(defaultMissShader)));
        defaultRMiss = misshitShaders.get()["defaultMissShader"];

        // Empty miss shader
        OvVK::Shader emptyMissShader;
        emptyMissShader.setName("emptyMissShader");
        if (emptyMissShader.load(Ov::Shader::Type::miss, empty_rmiss) == false)
        {
            this->free();
            return false;
        }

        misshitShaders.get().insert(std::make_pair("emptyMissShader", std::move(emptyMissShader)));
        emptyRMiss = misshitShaders.get()["emptyMissShader"];
    }

    // Raygen shader group
    std::reference_wrapper<OvVK::RTShaderGroup> rayGenShaderGroup = getRayGenShaderGroup();
    rayGenShaderGroup.get().setName("raygenShaderGroup");
    if (rayGenShaderGroup.get().build({ raygenShader }) == false)
    {
        this->free();
        return false;
    }

    // Hit shader groups
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> hitShaderGroups = getHitShaderGroups();
    {
        // Default hit shader group for primary and secondary rays
        OvVK::RTShaderGroup defaultHitGroup;
        defaultHitGroup.setName("defaultHitShaderGroup");
        defaultHitGroup.build({ defaultRCHit, emptyRAHit });

        hitShaderGroups.get().insert(std::make_pair("defaultHitShaderGroup", std::move(defaultHitGroup)));

        // Empty hit shader
        OvVK::RTShaderGroup emptyHitShaderGroup;
        emptyHitShaderGroup.setName("emptyHitShaderGroup");
        emptyHitShaderGroup.build({ emptyRCHit, emptyRAHit });

        hitShaderGroups.get().insert(std::make_pair("emptyHitShaderGroup", std::move(emptyHitShaderGroup)));
    }

    // Miss shader
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> missShaderGroups = getMissShaderGroups();
    {
        // Add default miss shader group
        OvVK::RTShaderGroup missShaderGroup;
        missShaderGroup.setName("defaultMissShaderGroup");
        missShaderGroup.build({ defaultRMiss });

        missShaderGroups.get().insert(std::make_pair("defaultMissShaderGroup", std::move(missShaderGroup)));

        // Empty miss shader group
        OvVK::RTShaderGroup emptyMissShaderGroup;
        emptyMissShaderGroup.setName("emptyMissShaderGroup");
        emptyMissShaderGroup.build({ emptyRMiss });

        missShaderGroups.get().insert(std::make_pair("emptyMissShaderGroup", std::move(emptyMissShaderGroup)));
    }


    ///////////////////
    // PipelineLayot //
    ///////////////////

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
    pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreateInfo.pNext = NULL;
    pipelineLayoutCreateInfo.flags = 0x00000000;
    
    VkDescriptorSetLayout descriptorSetLayouts[2];
    descriptorSetLayouts[0] = engine.get().getStorage().getStorageDescriptorSetsManager().getVkDescriptorSetLayout();
    descriptorSetLayouts[1] = reserved->pickingDescriptorSetsManager.getVkDescriptorSetLayout();

    pipelineLayoutCreateInfo.setLayoutCount = 2;
    pipelineLayoutCreateInfo.pSetLayouts = descriptorSetLayouts;
    pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
    pipelineLayoutCreateInfo.pPushConstantRanges = VK_NULL_HANDLE;

    // Create rtpipeline pipeline
    // VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR specifies that a closest hit shader
    // will always be present when a closest hit shader would be executed.A NULL closest hit shader is a closest
    // hit shader which is effectively VK_SHADER_UNUSED_KHR, such as from a shader group consisting entirely of zeros.
    // VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR specifies that a miss shader will always 
    // be present when a miss shader would be executed.A NULL miss shader is a miss shader which is effectively 
    // VK_SHADER_UNUSED_KHR, such as from a shader group consisting entirely of zeros.

    if (createRTPipeline(pipelineLayoutCreateInfo, 1,
        VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR |
        VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR) == false) {
        this->free();
        return false;
    }


    // add Pipeline to engine
    engine.get().addPipeline(*this);

    // Record call back function for mouse position
    OvVK::Surface::SurfaceCallbackTypes activeCallback = OvVK::Surface::SurfaceCallbackTypes::mouseCursor;
    setActiveCallback(activeCallback);
    presentSurface.get().addSurfaceDependentObject(getId(), *this);
    setIsActive(true);

    // Done:
    return true;
}

#pragma endregion

#pragma region SurfaceCallBack

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse cursor callback is forwarded.
 * @param mouseX The new cursor x-coordinate, relative to the left edge of the content area.
 * @param mouseY The new cursor y-coordinate, relative to the top edge of the content area.
 */
bool OVVK_API OvVK::PickingPipeline::mouseCursorCallback(double mouseX, double mouseY)
{
    setPickingCoordinates(glm::vec2(mouseX, mouseY));

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param rtScene The rtScene containing the scene to render.
 * @return TF
 */
bool OVVK_API OvVK::PickingPipeline::render(const OvVK::RTScene& rtScene) {
    
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Logical Device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Storage
    std::reference_wrapper<OvVK::Storage> storage = engine.get().getStorage();
    // Get queue families index
    std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamilyIndices =
        logicalDevice.get().getQueueFamiliesIndices();
    // frame in flight
    uint64_t frameInFlight = engine.get().getFrameInFlight();

    // Wait CPU render sync object
    // Wait fences to know the work is finished
    VkFence fenceToWait = getStatusSyncObjCPU();
    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &fenceToWait,
        VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait render Fence in picking pipeline with id= %d.", getId());
        return false;
    }


    ////////////////////
    // Reset syncObjs //
    ////////////////////

    // Reset sync obj CPU.
    if (vkResetFences(logicalDevice.get().getVkDevice(), 1,
        &fenceToWait) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to reset the render fence of the frame in flight
 number %d of the picking pipeline with id= %d.)", frameInFlight, getId());
        return false;
    }

    if (getSBTHostBuffer().getVkBuffer() != VK_NULL_HANDLE)
        getSBTHostBuffer().setLockSyncObj(VK_NULL_HANDLE);
    if (getSBTDeviceBuffer().getVkBuffer() != VK_NULL_HANDLE)
        getSBTDeviceBuffer().setLockSyncObj(VK_NULL_HANDLE);

    // Binary semaphore
    VkSemaphoreCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    createInfo.pNext = VK_NULL_HANDLE;
    createInfo.flags = 0;

    if (reserved->transferPreviousRTStatusSyncObjs[frameInFlight] == VK_NULL_HANDLE &&
        vkCreateSemaphore(logicalDevice.get().getVkDevice(), &createInfo, NULL,
            &reserved->transferPreviousRTStatusSyncObjs[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to create semaphore to sync transfer ownership for transfer
 queue in picking pipeline with id= %d)", getId());
        return false;
    }
    if (reserved->graphicPreviousRTStatusSyncObjs[frameInFlight] == VK_NULL_HANDLE &&
        vkCreateSemaphore(logicalDevice.get().getVkDevice(), &createInfo, NULL,
            &reserved->graphicPreviousRTStatusSyncObjs[frameInFlight]) != VK_SUCCESS)
    {
        OV_LOG_ERROR(R"(Fail to create semaphore to sync transfer ownership for graphic
 queue in picking pipeline with id= %d)", getId());
        return false;
    }


    //////////////////////////////////////////
    // Calculate SBT Buffer size (in bytes) //
    //////////////////////////////////////////

    uint32_t shaderGroupHandleStride = getShaderGroupHandleStride();
    uint32_t shaderGroupBaseAlignment = getShaderGroupBaseAlignment();
    uint32_t shaderGroupHandleSize = getShaderGroupHandleSize();
    uint32_t numberOfClosestHitShaderHandle = rtScene.getNrOfHitShaderGroupsNeeded();
    
    VkDeviceSize sbtSize = 0;
    uint32_t baseAlignamentNeeded = 0;
    uint32_t baseAlignamentOverflow = 0;


    // RayGen shaderGroup
    VkDeviceSize sbtRayGenShaderGroupSize = 0;
    baseAlignamentNeeded = shaderGroupHandleStride / shaderGroupBaseAlignment;
    baseAlignamentOverflow = shaderGroupHandleStride % shaderGroupBaseAlignment;

    if (baseAlignamentNeeded == 0)
        sbtRayGenShaderGroupSize = shaderGroupBaseAlignment;
    else
        sbtRayGenShaderGroupSize = baseAlignamentNeeded * shaderGroupBaseAlignment +
        ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);

    sbtSize += sbtRayGenShaderGroupSize;

    // Hit shaderGroup
    VkDeviceSize sbtHitShaderGroupSize = 0;
    baseAlignamentNeeded = (numberOfClosestHitShaderHandle * shaderGroupHandleStride)
        / shaderGroupBaseAlignment;
    baseAlignamentOverflow = (numberOfClosestHitShaderHandle * shaderGroupHandleStride)
        % shaderGroupBaseAlignment;

    if (baseAlignamentNeeded == 0)
        sbtHitShaderGroupSize = shaderGroupBaseAlignment;
    else
        sbtHitShaderGroupSize = (baseAlignamentNeeded * shaderGroupBaseAlignment) +
        ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);


    sbtSize += sbtHitShaderGroupSize;

    // Miss shaderGroup
    VkDeviceSize sbtMissShaderGroupSize = 0;
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> missShaderGroups =
        getMissShaderGroups();
    baseAlignamentNeeded = (missShaderGroups.get().size() * shaderGroupHandleStride) / 
        shaderGroupBaseAlignment;
    baseAlignamentOverflow = (missShaderGroups.get().size() * shaderGroupHandleStride) % 
        shaderGroupBaseAlignment;

    if (baseAlignamentNeeded == 0)
        sbtMissShaderGroupSize = shaderGroupBaseAlignment;
    else
        sbtMissShaderGroupSize = (baseAlignamentNeeded * shaderGroupBaseAlignment) +
        ((baseAlignamentOverflow > 0) ? shaderGroupBaseAlignment : 0);

    sbtSize += sbtMissShaderGroupSize;


    // Staging Buffer
    // Create info buffer
    VkBufferCreateInfo sbtStagingBufferCreateInfo = {};
    sbtStagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    sbtStagingBufferCreateInfo.size = sbtSize;
    // VK_BUFFER_USAGE_TRANSFER_SRC_BIT => specifies that the buffer can be used as the source of a transfer command 
    sbtStagingBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    sbtStagingBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    sbtStagingBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    sbtStagingBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaSBTStagingBufferAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
    //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
    //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
    //      of allocation time.
    vmaSBTStagingBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
        VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
    //      mapped for host access using vkMapMemory.
    vmaSBTStagingBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

    if (getSBTHostBuffer().getVkBuffer() != VK_NULL_HANDLE && getSBTHostBuffer().getSize() == sbtSize) {
        if (getSBTHostBuffer().setStatusSyncObjValue(PickingPipeline::pickingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to change status of the staging buffer for picking pipeline with id= %d.", getId());
            return false;
        }
    }
    else {
        getSBTHostBuffer().free();
        if (getSBTHostBuffer().create(sbtStagingBufferCreateInfo, vmaSBTStagingBufferAllocationCreateInfo) == false ||
            getSBTHostBuffer().setStatusSyncObjValue(PickingPipeline::pickingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to create sbt staging buffer for picking pipeline with id= %d.", getId());
            return false;
        }
    }

    // Device Buffer
    // Create info buffer
    VkBufferCreateInfo sbtDeviceBufferCreateInfo{};
    sbtDeviceBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    sbtDeviceBufferCreateInfo.size = sbtSize;
    // VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR => specifies that the buffer is suitable for use as a Shader Binding Table.
    // VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
    //      of a transfer command.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device
    //      address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    sbtDeviceBufferCreateInfo.usage = VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR
        | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    sbtDeviceBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    sbtDeviceBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    sbtDeviceBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaSBTDeviceBufferAllocationCreateInfo{};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    vmaSBTDeviceBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaSBTDeviceBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Create buffer to contains vertex data
    if (getSBTDeviceBuffer().getVkBuffer() != VK_NULL_HANDLE && getSBTDeviceBuffer().getSize() == sbtSize) {
        if (getSBTDeviceBuffer().setStatusSyncObjValue(PickingPipeline::pickingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to change status of the device buffer for picking pipeline with id= %d.", getId());
            return false;
        }
    }
    else {
        getSBTDeviceBuffer().free();
        // Create buffer to contains vertex data
        if (getSBTDeviceBuffer().create(sbtDeviceBufferCreateInfo, vmaSBTDeviceBufferAllocationCreateInfo) == false ||
            getSBTDeviceBuffer().setStatusSyncObjValue(PickingPipeline::pickingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to create SBT device buffer for picking pipeline with id= %d.", getId());
            return false;
        }
    }


    /////////////////////////////
    // Fill SBT Staging Buffer //
    /////////////////////////////

    std::reference_wrapper<std::vector<uint8_t>> shaderGroupsHandleStorage = getShaderGroupsHandleStorage();

    // Get pointer to the memory where copy the data
    uint8_t* sbtStagingCurrentPosition = ((uint8_t*)getSBTHostBuffer().getVmaAllocationInfo().pMappedData);
    VkDeviceAddress sbtDeviceCurrentPosition = getSBTDeviceBuffer().getBufferDeviceAddress().value();


    // Copy RayGen shaderGroups on staging buffer
    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[getRayGenShaderGroup().getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize);

    VkStridedDeviceAddressRegionKHR& raygenShaderBindingTable = getRaygenShaderBindingTable();
    raygenShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    // The correct stride is reserved->strideShaderGroupHandle but there will be only one ray Gen.
    // The vulkan spec say that stride and size need to be equal in the pRaygenShaderBindingTable.
    //reserved->rgenShaderBindingTable.stride = reserved->strideShaderGroupHandle;
    raygenShaderBindingTable.stride = sbtRayGenShaderGroupSize;
    //Is reserved->shaderGroupBaseAlignment
    raygenShaderBindingTable.size = sbtRayGenShaderGroupSize;

    sbtStagingCurrentPosition += sbtRayGenShaderGroupSize;
    sbtDeviceCurrentPosition += sbtRayGenShaderGroupSize;


    // Get default hit shader group
    std::reference_wrapper<std::map<std::string, OvVK::RTShaderGroup>> hitShaderGroups = getHitShaderGroups();
    std::reference_wrapper< OvVK::RTShaderGroup> hitShaderGroup = OvVK::RTShaderGroup::empty;
    if (hitShaderGroups.get().size() == 0) 
    {
        OV_LOG_ERROR("In the picking pipeline with id= %d there are no hit shader groups.", getId());
        return false;
    }

    hitShaderGroup = hitShaderGroups.get()["defaultHitShaderGroup"];

    if (rtScene.getAreGeometriesSharingSameShadersGroups()) {
        for (uint32_t j = 0; j < numberOfClosestHitShaderHandle; j++)
        {
            memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[hitShaderGroup.get().getPositionInPipeline().value()
                * shaderGroupHandleSize],
                (size_t)shaderGroupHandleSize);

            sbtStagingCurrentPosition += shaderGroupHandleStride;
        }
    }
    else {
        // Copy hit shaderGroups on staging buffer
        const std::vector<OvVK::RTScene::RenderableElem>& renderableElems = rtScene.getRenderableElems();
        for (uint32_t i = rtScene.getNrOfLights(); i < rtScene.getNrOfRenderableElems(); i++)
        {
            // All RTObjDesc
            std::reference_wrapper<OvVK::RTObjDesc> renderableRTObjDesc =
                dynamic_cast<OvVK::RTObjDesc&>(renderableElems[i].renderable.get());

            uint32_t nrOfShadersGroupsForCurrentRTObjDesc = renderableRTObjDesc.get().getNrGeoemtriesInBLAS() * 
                magic_enum::enum_integer(OvVK::RayTracingPipeline::RayType::last);
            for (uint32_t j = 0; j < nrOfShadersGroupsForCurrentRTObjDesc; j++)
            {
                memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[hitShaderGroup.get().getPositionInPipeline().value()
                    * shaderGroupHandleSize],
                    (size_t)shaderGroupHandleSize);

                sbtStagingCurrentPosition += shaderGroupHandleStride;
            }
        }
    }

    VkStridedDeviceAddressRegionKHR& hitShaderBindingTable = getHitShaderBindingTable();
    hitShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    hitShaderBindingTable.stride = shaderGroupHandleStride;
    hitShaderBindingTable.size = sbtHitShaderGroupSize;

    sbtStagingCurrentPosition += (sbtHitShaderGroupSize - (numberOfClosestHitShaderHandle * shaderGroupHandleStride));
    sbtDeviceCurrentPosition += sbtHitShaderGroupSize;

    // Copy callable shaderGroups on staging buffer
    VkStridedDeviceAddressRegionKHR& callableShaderBindingTable = getCallableShaderBindingTable();
    callableShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    callableShaderBindingTable.stride = 0;
    callableShaderBindingTable.size = 0;


    // Copy miss shaderGroups on staging buffer
    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[missShaderGroups.get()["defaultMissShaderGroup"].getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize);

    sbtStagingCurrentPosition += shaderGroupHandleStride;

    memcpy(sbtStagingCurrentPosition, &shaderGroupsHandleStorage.get()[missShaderGroups.get()["emptyMissShaderGroup"].getPositionInPipeline().value()
        * shaderGroupHandleSize],
        (size_t)shaderGroupHandleSize);

    sbtStagingCurrentPosition += shaderGroupHandleStride;

    VkStridedDeviceAddressRegionKHR& missShaderBindingTable = getMissShaderBindingTable();
    missShaderBindingTable.deviceAddress = sbtDeviceCurrentPosition;
    missShaderBindingTable.stride = shaderGroupHandleStride;
    missShaderBindingTable.size = sbtMissShaderGroupSize;

    sbtStagingCurrentPosition += (sbtMissShaderGroupSize - (getMissShaderGroups().size() * shaderGroupHandleStride));
    sbtDeviceCurrentPosition += sbtMissShaderGroupSize;


    ////////////////
    // Update DSM //
    ////////////////

    reserved->pickingDescriptorSetsManager.render();


    ///////////////////////
    // Performs commands //
    ///////////////////////

    bool exit = false;
    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    std::reference_wrapper<OvVK::CommandPool> computeCommandPool = engine.get().getPrimaryComputeCmdPool();
    std::reference_wrapper<OvVK::CommandPool> transferCommandPool = engine.get().getPrimaryTransferCmdPool();
    std::reference_wrapper<OvVK::CommandPool> graphicCommandPool = engine.get().getPrimaryGraphicCmdPool();

    std::reference_wrapper<OvVK::CommandBuffer> computeCommandBuffer = computeCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> transferCommandBuffer = transferCommandPool.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> graphicCommandBuffer = graphicCommandPool.get().allocateCommandBuffer();

    if (computeCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        computeCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the compute command buffer into render
 picking pipeline with id= %d.)", getId());
        exit = true;
    }
    if (transferCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the transfer command buffer into render
 picking pipeline with id= %d.)", getId());
        exit = true;
    }
    if (graphicCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status to the graphic command buffer into render
 picking pipeline with id= %d.)", getId());
        exit = true;
    }

    if (exit) {
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        return false;
    }


    /////////
    // SBT //
    /////////

    // SetUp copy sbt buffer
    VkBufferCopy sbtBufferCopy = {};
    // The regions are defined in VkBufferCopy structs and consist of a source buffer offset, destination
    // buffer offset and size. It is not possible to specify VK_WHOLE_SIZE here, unlike the vkMapMemory command.
    sbtBufferCopy.srcOffset = 0; // Optional
    sbtBufferCopy.dstOffset = 0; // Optional
    sbtBufferCopy.size = sbtSize;

    // Copy AS Instances
    vkCmdCopyBuffer(computeCommandBuffer.get().getVkCommandBuffer(), getSBTHostBuffer().getVkBuffer(),
        getSBTDeviceBuffer().getVkBuffer(), 1, &sbtBufferCopy);

    // AS Instances buffer
    VkBufferMemoryBarrier2 sbtBufferMemoryBarrier2 = {};
    sbtBufferMemoryBarrier2.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    sbtBufferMemoryBarrier2.pNext = VK_NULL_HANDLE;
    // Need to wait previous stage 
    sbtBufferMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
    // Need to wait on specific memory access.
    sbtBufferMemoryBarrier2.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
        VK_ACCESS_2_TRANSFER_READ_BIT;
    // All ray tracing command need to wait the SBT buffer transfer.
    sbtBufferMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // All ray tracing access AS command need to wait the SBT buffer transfer.
    sbtBufferMemoryBarrier2.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
        VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    // Need to transfer ownership
    sbtBufferMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    sbtBufferMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // buffer
    sbtBufferMemoryBarrier2.buffer = getSBTDeviceBuffer().getVkBuffer();
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    sbtBufferMemoryBarrier2.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    sbtBufferMemoryBarrier2.size = VK_WHOLE_SIZE;

    // Barrier specification
    VkDependencyInfo sbtDependencyInfo = {};
    sbtDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    sbtDependencyInfo.dependencyFlags = 0x00000000;
    sbtDependencyInfo.memoryBarrierCount = 0;
    sbtDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    sbtDependencyInfo.bufferMemoryBarrierCount = 1;
    sbtDependencyInfo.pBufferMemoryBarriers = &sbtBufferMemoryBarrier2;
    sbtDependencyInfo.imageMemoryBarrierCount = 0;
    sbtDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register command for transition.
    vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &sbtDependencyInfo);


    /////////////
    // Storage //
    /////////////

    std::vector<VkBufferMemoryBarrier2> graphicVkBufferMemoryBarrier2;
    std::vector<VkBufferMemoryBarrier2> transferVkBufferMemoryBarrier2;

    // Memory barrier for storage buffers
    VkBufferMemoryBarrier2 memoryBarrier;
    memoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    memoryBarrier.pNext = VK_NULL_HANDLE;
    // No need to wait previous stage 
    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
    memoryBarrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
    // No need to wait on specific memory access.
    memoryBarrier.srcAccessMask = VK_ACCESS_2_NONE;
    // All ray tracing command need to wait the SBT buffer transfer.
    memoryBarrier.dstStageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // All ray tracing access AS command need to wait the SBT buffer transfer.
    memoryBarrier.dstAccessMask = VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR |
        VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    // Need to transfer ownership
    memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // offset is an offset in bytes into the backing memory for buffer;
    // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
    memoryBarrier.offset = 0;
    // size is a size in bytes of the affected area of backing memory for buffer,
    // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
    memoryBarrier.size = VK_WHOLE_SIZE;


    // Global uniform buffer change ownership
    if (storage.get().getGlobalUniformDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getGlobalUniformDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getGlobalUniformDeviceBuffer().getVkBuffer();

        if (storage.get().getGlobalUniformDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }

    // Materials buffer change ownership
    if (storage.get().getMaterialsDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getMaterialsDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getMaterialsDeviceBuffer().getVkBuffer();

        if (storage.get().getMaterialsDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }

    // Lights buffer change ownership
    if (storage.get().getLightsDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getLightsDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getLightsDeviceBuffer().getVkBuffer();

        if (storage.get().getLightsDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }

    // RTObjDesc uniform buffer change ownership
    if (storage.get().getRTObjDescsDeviceBuffer().getQueueFamilyIndex().has_value() == true &&
        storage.get().getRTObjDescsDeviceBuffer().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        memoryBarrier.buffer = storage.get().getRTObjDescsDeviceBuffer().getVkBuffer();

        if (storage.get().getRTObjDescsDeviceBuffer().getQueueFamilyIndex().value() == queueFamilyIndices.value().transferFamily.value()) {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().transferFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
        else {
            memoryBarrier.srcQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();
            memoryBarrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
        }
    }


    // Barrier specification
    VkDependencyInfo storageDependencyInfo = {};
    storageDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    storageDependencyInfo.dependencyFlags = 0x00000000;
    storageDependencyInfo.memoryBarrierCount = 0;
    storageDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
    storageDependencyInfo.bufferMemoryBarrierCount = graphicVkBufferMemoryBarrier2.size();
    storageDependencyInfo.pBufferMemoryBarriers = graphicVkBufferMemoryBarrier2.data();
    storageDependencyInfo.imageMemoryBarrierCount = 0;
    storageDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

    // Register command for transition.
    if (graphicVkBufferMemoryBarrier2.size() > 0) {
        vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
        vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
    }

    storageDependencyInfo.bufferMemoryBarrierCount = transferVkBufferMemoryBarrier2.size();
    storageDependencyInfo.pBufferMemoryBarriers = transferVkBufferMemoryBarrier2.data();

    // Register command for transition.
    if (transferVkBufferMemoryBarrier2.size() > 0) {
        vkCmdPipelineBarrier2(transferCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
        vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &storageDependencyInfo);
    }


    ///////////////////////////
    // Picking Data Transfer //
    ///////////////////////////

    std::reference_wrapper<OvVK::Buffer> pickingDataRTScene = rtScene.getPickingDataBuffer();
    // Set picking coordinates
    OvVK::PickingPipeline::PickingData* pickData = reinterpret_cast<OvVK::PickingPipeline::PickingData*>(
        ((uint8_t*)pickingDataRTScene.get().getVmaAllocationInfo().pMappedData));

    pickData->pickingCoords = reserved->pickingCoords;
    pickData->geometryId = 0;
    pickData->rtObjDescId = 0;
    pickData->pickedSomething = 0;

    if (pickingDataRTScene.get().getQueueFamilyIndex().has_value() == true &&
        pickingDataRTScene.get().getQueueFamilyIndex().value() != queueFamilyIndices.value().computeFamily.value()) {

        VkBufferMemoryBarrier2 barrier = {};
        // GlobaUniform buffer
        barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        barrier.pNext = VK_NULL_HANDLE;
        // No need to wait previous stage 
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT  
        barrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
        // No need to wait on specific memory access.
        barrier.srcAccessMask = VK_ACCESS_2_NONE;
        // All transfer commands need to wait.
        barrier.dstStageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
        // All write commands need to wait.
        barrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT |
            VK_ACCESS_2_TRANSFER_READ_BIT;
        // Need to transfer ownership
        barrier.srcQueueFamilyIndex = pickingDataRTScene.get().getQueueFamilyIndex().value();
        barrier.dstQueueFamilyIndex = queueFamilyIndices.value().computeFamily.value();
        // buffer
        barrier.buffer = pickingDataRTScene.get().getVkBuffer();
        // offset is an offset in bytes into the backing memory for buffer;
        // this is relative to the base offset as bound to the buffer (see vkBindBufferMemory). 
        barrier.offset = 0;
        // size is a size in bytes of the affected area of backing memory for buffer,
        // or VK_WHOLE_SIZE to use the range from offset to the end of the buffer.
        barrier.size = VK_WHOLE_SIZE;

        // Barrier specification
        VkDependencyInfo vkDependencyInfo = {};
        vkDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
        vkDependencyInfo.dependencyFlags = 0x00000000;
        vkDependencyInfo.memoryBarrierCount = 0;
        vkDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
        vkDependencyInfo.bufferMemoryBarrierCount = 1;
        vkDependencyInfo.pBufferMemoryBarriers = &barrier;
        vkDependencyInfo.imageMemoryBarrierCount = 0;
        vkDependencyInfo.pImageMemoryBarriers = VK_NULL_HANDLE;

        if (queueFamilyIndices.value().graphicsFamily.has_value() == true &&
            queueFamilyIndices.value().graphicsFamily.value() ==
            pickingDataRTScene.get().getQueueFamilyIndex().value()) {
            graphicVkBufferMemoryBarrier2.push_back(memoryBarrier);
            vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &vkDependencyInfo);
        }
        else if (queueFamilyIndices.value().transferFamily.has_value() == true &&
            queueFamilyIndices.value().transferFamily.value() ==
            pickingDataRTScene.get().getQueueFamilyIndex().value()) {
            transferVkBufferMemoryBarrier2.push_back(memoryBarrier);
            vkCmdPipelineBarrier2(transferCommandBuffer.get().getVkCommandBuffer(), &vkDependencyInfo);
        }

        vkCmdPipelineBarrier2(computeCommandBuffer.get().getVkCommandBuffer(), &vkDependencyInfo);
        pickingDataRTScene.get().setQueueFamilyIndex(queueFamilyIndices.value().computeFamily.value());
    }

    std::reference_wrapper<OvVK::Buffer> pickingDataDevice = reserved->pickingBuffers[frameInFlight];

    VkBufferCopy copyBuffer = {};
    copyBuffer.srcOffset = 0;
    copyBuffer.dstOffset = 0;

    // Update picking data to RTScene
    copyBuffer.size = pickingDataRTScene.get().getSize();
    vkCmdCopyBuffer(computeCommandBuffer.get().getVkCommandBuffer(),
        pickingDataRTScene.get().getVkBuffer(),
        pickingDataDevice.get().getVkBuffer(), 1, &copyBuffer);


    //////////////
    // Pipeline //
    //////////////

    // Bind Pipeline
    vkCmdBindPipeline(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, getVkPipeline());

    // Descriptor sets
    VkDescriptorSet descriptorSet;

    // Bind Storage DSM
    descriptorSet = engine.get().getStorage().getStorageDescriptorSetsManager().getVkDescriptorSet(frameInFlight);

    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, getVkPipelineLayout(),
        magic_enum::enum_integer(OvVK::PickingPipeline::BindSet::storage), 1,
        &descriptorSet, 0, 0);


    // Bind Picking pipeline DSM
    descriptorSet = reserved->pickingDescriptorSetsManager.getVkDescriptorSet(frameInFlight);

    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, getVkPipelineLayout(),
        magic_enum::enum_integer(OvVK::PickingPipeline::BindSet::picking), 1,
        &descriptorSet, 0, 0);

    // Bind the RT pipeline DSM 
    std::reference_wrapper<OvVK::RayTracingDescriptorSetsManager> rtDSM =
        getRayTracingDescriptorSetsManager();

    // Update DSM with the new TLAS
    OvVK::RayTracingDescriptorSetsManager::UpdateData updateRTDSM;
    updateRTDSM.targetFrame = frameInFlight;
    updateRTDSM.tlas = rtScene.getTLAS().getVkAS();

    rtDSM.get().addToUpdateList(updateRTDSM);
    rtDSM.get().render();

    descriptorSet = rtDSM.get().getVkDescriptorSet(frameInFlight);

    vkCmdBindDescriptorSets(computeCommandBuffer.get().getVkCommandBuffer(),
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, getVkPipelineLayout(),
        magic_enum::enum_integer(OvVK::PickingPipeline::BindSet::rt), 1,
        &descriptorSet, 0, 0);

    // Record Ray tracing command
    PFN_vkCmdTraceRaysKHR pvkCmdTraceRaysKHR = (PFN_vkCmdTraceRaysKHR)vkGetDeviceProcAddr(
        engine.get().getLogicalDevice().getVkDevice(), "vkCmdTraceRaysKHR");

    std::reference_wrapper<OvVK::Surface> surface = 
        OvVK::Engine::getInstance().getPresentSurface();

    pvkCmdTraceRaysKHR(computeCommandBuffer.get().getVkCommandBuffer(),
        &getRaygenShaderBindingTable(),
        &getMissShaderBindingTable(),
        &getHitShaderBindingTable(),
        &getCallableShaderBindingTable(),
        surface.get().getWidth(),
        surface.get().getHeight(),
        1);


    ///////////////////////////
    // Picking Data Transfer //
    ///////////////////////////


    // Update picking data to RTScene
    copyBuffer.size = pickingDataRTScene.get().getSize();
    vkCmdCopyBuffer(computeCommandBuffer.get().getVkCommandBuffer(), 
        pickingDataDevice.get().getVkBuffer(),
        pickingDataRTScene.get().getVkBuffer(), 1, &copyBuffer);


    ///////////////////////////////
    // End registration commands //
    ///////////////////////////////

    exit = false;
    if (computeCommandBuffer.get().endVkCommandBuffer() == false ||
        computeCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the compute command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (transferCommandBuffer.get().endVkCommandBuffer() == false ||
        transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the transfer command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }
    if (graphicCommandBuffer.get().endVkCommandBuffer() == false ||
        graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or change status to the graphic command buffer into render
 lighting pipeline with id= %d.)", getId());
        exit = true;
    }

    if (exit) {
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        return false;
    }


    /////////////////////
    // SetUp sync Objs //
    /////////////////////

    // Set before the value because if the value is smaller the sync obj are recreated.
    setStatusSyncObjGPU(OvVK::Pipeline::renderingValueStatusSyncObj);


    ////////////
    // Submit //
    ////////////

    // Retrieve queue handle
    std::optional<OvVK::LogicalDevice::QueueHandles> queueHandles =
        engine.get().getLogicalDevice().getQueueHandles();

    // Wait all pipeline the pipeline depend on.
    std::vector<VkSemaphore> toWaitSemaphore = getStatusSyncObjsGPUToWait();
    std::vector<uint64_t> waitValue(toWaitSemaphore.size(), OvVK::Pipeline::renderingDoneValueStatusSyncObj);
    // This pipeline sync obj
    toWaitSemaphore.push_back(getStatusSyncObjGPU());
    waitValue.push_back(OvVK::Pipeline::renderingValueStatusSyncObj);
    toWaitSemaphore.push_back(rtScene.getRederingStatusSyncObjGPU());
    waitValue.push_back(OvVK::RTScene::finishValueRederingStatusSyncObj);

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(toWaitSemaphore.size());
    for (uint32_t c = 0; c < toWaitSemaphore.size(); c++) {
        pWaitSemaphoreInfos[c].sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pWaitSemaphoreInfos[c].pNext = VK_NULL_HANDLE;
        pWaitSemaphoreInfos[c].semaphore = toWaitSemaphore[c];
        pWaitSemaphoreInfos[c].value = waitValue[c];
        // All command s need to wait
        // VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT 
        pWaitSemaphoreInfos[c].stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pWaitSemaphoreInfos[c].deviceIndex = 0;
    }

    // Graphic command buffer submit
    if (graphicVkBufferMemoryBarrier2.size() > 0) {

        // Submit
        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = graphicCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos;
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.semaphore = reserved->graphicPreviousRTStatusSyncObjs[frameInFlight];
        // Ignore because presentPreviousRTStatusSyncObjs binary
        pSignalSemaphoreInfos.value = 0;
        // All command s need to wait
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        submitInfo2.signalSemaphoreInfoCount = 1;
        submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        if (vkQueueSubmit2(queueHandles.value().graphicsQueue, 1, &submitInfo2, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit present commandBuffer in picking Pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            return false;
        }
    }
    // Transfer command buffer submit
    if (transferVkBufferMemoryBarrier2.size() > 0) {

        // Submit
        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = transferCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        VkSemaphoreSubmitInfo pSignalSemaphoreInfos;
        pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
        pSignalSemaphoreInfos.semaphore = reserved->transferPreviousRTStatusSyncObjs[frameInFlight];
        // Ignore because presentPreviousRTStatusSyncObjs binary
        pSignalSemaphoreInfos.value = 0;
        // All command s need to wait
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
        pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        pSignalSemaphoreInfos.deviceIndex = 0;

        submitInfo2.signalSemaphoreInfoCount = 1;
        submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

        if (vkQueueSubmit2(queueHandles.value().transferQueue, 1, &submitInfo2, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit transfer commandBuffer in picking Pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            return false;
        }
    }

    // RT
    VkSubmitInfo2 submitInfo2RT = {};
    submitInfo2RT.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    submitInfo2RT.pNext = VK_NULL_HANDLE;
    submitInfo2RT.flags = 0x00000000;

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfosRT;
    // Copying vector by copy function�
    pWaitSemaphoreInfosRT.insert(pWaitSemaphoreInfosRT.begin(), pWaitSemaphoreInfos.begin(), pWaitSemaphoreInfos.end());

    // Set semaphore to wait on ray tracing operation
    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoRT = {};
    vkSemaphoreSubmitInfoRT.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfoRT.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfoRT.value = 0;
    // Ray tracing need to wait the change of ownership of the resources.
    vkSemaphoreSubmitInfoRT.stageMask = VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfoRT.deviceIndex = 0;

    if (transferVkBufferMemoryBarrier2.size() > 0) {
        vkSemaphoreSubmitInfoRT.semaphore = reserved->transferPreviousRTStatusSyncObjs[frameInFlight];
        pWaitSemaphoreInfosRT.push_back(vkSemaphoreSubmitInfoRT);
    }
    if (graphicVkBufferMemoryBarrier2.size() > 0) {
        vkSemaphoreSubmitInfoRT.semaphore = reserved->graphicPreviousRTStatusSyncObjs[frameInFlight];
        pWaitSemaphoreInfosRT.push_back(vkSemaphoreSubmitInfoRT);
    }

    submitInfo2RT.waitSemaphoreInfoCount = pWaitSemaphoreInfosRT.size();
    submitInfo2RT.pWaitSemaphoreInfos = pWaitSemaphoreInfosRT.data();

    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(2);
    VkSemaphoreSubmitInfo pSignalSemaphoreInfosRT = {};
    pSignalSemaphoreInfosRT.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    pSignalSemaphoreInfosRT.pNext = VK_NULL_HANDLE;
    // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
    pSignalSemaphoreInfosRT.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    pSignalSemaphoreInfosRT.deviceIndex = 0;
    pSignalSemaphoreInfosRT.value = OvVK::Pipeline::renderingDoneValueStatusSyncObj;
    pSignalSemaphoreInfosRT.semaphore = getStatusSyncObjGPU();
    pSignalSemaphoreInfos[0] = pSignalSemaphoreInfosRT;
    // binary semaphores
    pSignalSemaphoreInfosRT.value = 0;
    pSignalSemaphoreInfosRT.semaphore = getStatusSyncObjPresentation();
    pSignalSemaphoreInfos[1] = pSignalSemaphoreInfosRT;

    submitInfo2RT.signalSemaphoreInfoCount = pSignalSemaphoreInfos.size();
    submitInfo2RT.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();

    // Command buffer set
    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = computeCommandBuffer.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;
    submitInfo2RT.commandBufferInfoCount = 1;
    submitInfo2RT.pCommandBufferInfos = &pCommandBufferInfos;

    // Submit
    if (vkQueueSubmit2(queueHandles.value().computeQueue, 1, &submitInfo2RT, fenceToWait) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit compute commandBuffer in picking Pipeline with id= %d.", getId());
        computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        return false;
    }

    // If enabled, commands that perform picking are waited for.
    // This is only necessary if the time taken to process the commands submitted to the device needs to be measured.
    if (getAnalysingTimeRequired()) {
        // Wait to finish the ray tracing command only if needed to measure the processing time. 
        if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &fenceToWait,
            VK_TRUE, UINT64_MAX) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to wait the command buffer used to process picking in the picking pipeline with id= %d.", getId());
            computeCommandPool.get().freeCommandBuffer(computeCommandBuffer.get());
            transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            return false;
        }
    }

    getSBTHostBuffer().setLockSyncObj(getStatusSyncObjCPU());
    getSBTDeviceBuffer().setLockSyncObj(getStatusSyncObjCPU());

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the screen picking coordinates. Is a vec3 because maybe the z value will be useful.
 * @param pickingCoords The screen picking coordinates.
 */
bool OVVK_API OvVK::PickingPipeline::setPickingCoordinates(glm::vec2 pickingCoords) 
{
    // Get frame in flight
    std::reference_wrapper<OvVK::Surface> surface = Engine::getInstance().getPresentSurface();

    // Safety net:
    if (pickingCoords.x < 0 || pickingCoords.x > surface.get().getWidth()) {
        OV_LOG_ERROR(R"(Fail to sets the coordinates because the passed width is out of range in
 picking pipeline with id = %d)", getId());
        return false;
    }
    if (pickingCoords.y < 0 || pickingCoords.y > surface.get().getHeight()) {
        OV_LOG_ERROR(R"(Fail to sets the coordinates because the passed height is out of range in
 picking pipeline with id = %d)", getId());
        return false;
    }

    reserved->pickingCoords = pickingCoords;
    // Done:
    return true;
}

#pragma endregion

#pragma region Shader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds to the preprocessor of the Shader class the picking pipeline include to access the info.
 * @return TF
 */
bool OVVK_API OvVK::PickingPipeline::addShaderPreproc()
{
    /////////////
    // Picking //
    /////////////

    std::string pickingPreproc = R"(
#ifndef OVVULKAN_PICKING_PIPELINE_INCLUDED
#define OVVULKAN_PICKING_PIPELINE_INCLUDED
)";

    pickingPreproc += PickingPipeline::PickingData::getShaderStruct() + "\n";
    pickingPreproc += R"(
#endif
)";


    ////////////////
    // StorageDSM //
    ////////////////

    std::string storageBindPreproc = R"(
#ifndef OVVULKAN_PICKING_PIPELINE_STORAGE_DSM_INCLUDED
#define OVVULKAN_PICKING_PIPELINE_STORAGE_DSM_INCLUDED
)";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::PickingPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::globalUniform)) +
        ") uniform GlobalUniform { GlobalUniformData globalUniform; };\n";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::materials)) +
        ") buffer Materials { MaterialData data[]; } materials;\n";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::lights)) +
        ") buffer Lights { LightData data[]; } lights;\n";

    storageBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::WhittedRTwithPBRPipeline::BindSet::storage)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::StorageDescriptorSetsManager::Binding::rtObjDescs)) +
        ") buffer RTObjDescs { RTObjDescData data[]; } rtObjDescs;\n";

    storageBindPreproc += R"(
#endif
)";


    ///////////
    // RTDSM //
    ///////////

    std::string rtBindPreproc = R"(
#ifndef OVVULKAN_PICKING_PIPELINE_RT_DSM_INCLUDED
#define OVVULKAN_PICKING_PIPELINE_RT_DSM_INCLUDED
)";

    rtBindPreproc += "layout(set = " + std::to_string(static_cast<uint32_t>(OvVK::PickingPipeline::BindSet::rt)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(OvVK::RayTracingDescriptorSetsManager::Binding::tlas)) +
        ") uniform accelerationStructureEXT topLevelAS;\n";

    rtBindPreproc += R"(
#endif
)";


    ////////////////
    // PickingDSM //
    ////////////////

    std::string pickingBindPreproc = R"(
#ifndef OVVULKAN_PICKING_PIPELINE_LIGHTING_DSM_INCLUDED
#define OVVULKAN_PICKING_PIPELINE_LIGHTING_DSM_INCLUDED
)";

    pickingBindPreproc += "layout(scalar, set = " + std::to_string(static_cast<uint32_t>(OvVK::PickingPipeline::BindSet::picking)) +
        ", binding = " + std::to_string(static_cast<uint32_t>(PickingDescriptorSetsManager::Binding::picking)) +
        ") buffer PickingBuffer { PickingData data[]; } pickingBuffer;\n";

    pickingBindPreproc += R"(
#endif
 )";

    std::string pickingPipelineDescSetsPreproc = pickingPreproc + storageBindPreproc + rtBindPreproc + pickingBindPreproc;
    return OvVK::Shader::preprocessor.set("include PickingPipeline", pickingPipelineDescSetsPreproc);
}

#pragma endregion