/**
 * @file	overv_vk.cpp
 * @brief	RayTracing Vulkan Library
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// Include VMA header
#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

// GLFW
#include <GLFW/glfw3.h>

// Magic enum:
#include "magic_enum.hpp"


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Engine class reserved structure.
 */
struct OvVK::Engine::Reserved
{
    uint64_t frameCounter;                                  ///< Total number of rendered frames. 
    uint32_t frameInFlight;                                 ///< Current frame in flight processing.

    VkSampleCountFlagBits nrOfAASamples;                    ///< Number of aaSamples allowed to use (supported or chosed).

    OvVK::VulkanInstance vulkanInstance;                    ///< OvVK Vulkan instance handle.
    OvVK::Surface presentSurface;                           ///< OvVK present Vulkan surface handle.
    OvVK::LogicalDevice logicalDevice;                      ///< OvVK Vulkan logical device handle.
    OvVK::Swapchain swapchain;                              ///< OvVK Swapchain handle.
    OvVK::Storage storage;

    OvVK::CommandPool primariesGraphicCommandPools[Engine::nrFramesInFlight];
    OvVK::CommandPool primariesTransferCommandPools[Engine::nrFramesInFlight];
    OvVK::CommandPool primariesComputeCommandPools[Engine::nrFramesInFlight];
    OvVK::CommandPool primariesPresentCommandPools[Engine::nrFramesInFlight];

    std::list<std::reference_wrapper<OvVK::Pipeline>> pipelines;

    uint32_t presentanbleImageIndex[Engine::nrFramesInFlight];

    /**
     * Constructor
     */
    Reserved() : frameCounter{ 0 },
        frameInFlight{ 0 },
        nrOfAASamples{ VK_SAMPLE_COUNT_1_BIT }
    {}
};


///////////////////
// BODY OF CLASS //
///////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Engine::Engine() : reserved(std::make_unique<OvVK::Engine::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Engine::~Engine()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Singleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 * @return OvVK::Engine
 */
OvVK::Engine OVVK_API& OvVK::Engine::getInstance()
{
    static Engine instance;
    return instance;
}

#pragma endregion

#pragma region Init/Free

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization method.
 * @param config struct
 * @return TF
 */
bool OVVK_API OvVK::Engine::init(const Ov::Config& config)
{
    if (this->Ov::Core::init(config) == false)
        return false;

    // Anti_aliasing parsing.
    if (config.getNrOfAASamples() <= 1) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_1_BIT; }
    else if (config.getNrOfAASamples() <= 2) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_2_BIT; }
    else if (config.getNrOfAASamples() <= 4) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_4_BIT; }
    else if (config.getNrOfAASamples() <= 8) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_8_BIT; }
    else if (config.getNrOfAASamples() <= 16) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_16_BIT; }
    else if (config.getNrOfAASamples() <= 32) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_32_BIT; }
    else if (config.getNrOfAASamples() <= 64) { reserved->nrOfAASamples = VK_SAMPLE_COUNT_64_BIT; }
    else
    {
        OV_LOG_WARN("The anti-aliasing samples number %d desired exceeds the maximum capacity.\
                     The maximum capacity will be used. ", config.getNrOfAASamples());
        reserved->nrOfAASamples = VK_SAMPLE_COUNT_64_BIT;
    }

    // Init GLFW
    if (!initGLFW())
        return false;


    // Init Vulkan instence
    if (!initInstance(config.getWindowTitle())) 
        return false;


    // Init present surface (output window).
    if (!initSurface(config.getWindowTitle(), static_cast<uint32_t>(config.getWindowSizeX()),
        static_cast<uint32_t>(config.getWindowSizeY())))
        return false;


    // Initialization of a logical device
    if (!initLogicalDevice())
        return false;


    // Initialization of the swapchain
    if (!initSwapChain())
        return false;


    // Initialize commandPool and commandBuffer
    if (!initCommandPools())
        return false;


    // Initialize descriptorSets
    if (!initDescriptorSets())
        return false;


    // Initialize storage
    if (!initStorage())
        return false;


    // Initialize shader preprocessor
    if (!initShaderPreprocessor())
        return false;


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free method.
 * @return TF
 */
bool OVVK_API OvVK::Engine::free() {
    
    // Pipeline
    std::list<std::reference_wrapper<OvVK::Pipeline>> pipelines = reserved->pipelines;
    for(auto pipeline : pipelines)
    {
        pipeline.get().free();
    }

    // Destroy storage
    if (reserved->storage.free() == false)
        return false;


    // Destroy textures DSM
    OvVK::TexturesDescriptorSetsManager::getInstance().free();


    // Destroy static objc
    const_cast<OvVK::Texture2D&>(OvVK::Texture2D::getDefault()).free();
    const_cast<OvVK::Sampler&>(OvVK::Sampler::getDefault()).free();


    // Destroy commandPools
    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++) 
    {
        reserved->primariesGraphicCommandPools[c].free();
        reserved->primariesTransferCommandPools[c].free();
        reserved->primariesComputeCommandPools[c].free();
        reserved->primariesPresentCommandPools[c].free();
    }


    // Destroy swapchain
    if (reserved->swapchain.free() == false)
        return false;


    // Seatroy surface
    if (reserved->presentSurface.free() == false)
        return false;


    // Destroy logical device
    if (reserved->logicalDevice.free() == false)
        return false;


    // Destroy instance vulkan. 
    if (reserved->vulkanInstance.free() == false)
        return false;


    // Terminating GLFW
    glfwTerminate();


    return true; //Done free
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the current frame number.
 * @return current frame number
 */
uint64_t OVVK_API OvVK::Engine::getFrameNr() const
{
    return reserved->frameCounter;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the number of the current processing frame in flight.
 * @return current frame number
 */
uint32_t OVVK_API OvVK::Engine::getFrameInFlight() const 
{
    return reserved->frameInFlight;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the sample counts supported for anti_aliasing.
 * @return VkSampleCountFlagBits Bitmask specifying sample counts supported for an image used for storage operations
 */
VkSampleCountFlagBits OVVK_API OvVK::Engine::getNrOfAASamples() const
{
    return reserved->nrOfAASamples;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the sync obj of the frame in flight
 * @return VkFence Sync obj handle.
 */
std::list<std::reference_wrapper<OvVK::Pipeline>> OVVK_API& OvVK::Engine::getPipelines() const
{
    return reserved->pipelines;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the OvVK Vulkan instance handle.
 * @return OvVK::Instance reference
 */
OvVK::VulkanInstance OVVK_API& OvVK::Engine::getVulkanInstance() const
{
    return reserved->vulkanInstance;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the OvVK Vulkan surface handle.
 * @return OvVK::Surface reference
 */
OvVK::Surface OVVK_API& OvVK::Engine::getPresentSurface() const
{
    return reserved->presentSurface;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the OvVK Vulkan logical device handle.
 * @return OvVK::LogicalDevice reference
 */
OvVK::LogicalDevice OVVK_API& OvVK::Engine::getLogicalDevice() const
{
    return reserved->logicalDevice;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the OvVK swapchain device handle.
 * @return OvVK::Swapchain reference
 */
OvVK::Swapchain OVVK_API& OvVK::Engine::getSwapchain() const
{
    return reserved->swapchain;
}


OvVK::CommandPool OVVK_API& OvVK::Engine::getPrimaryGraphicCmdPool(uint32_t frameInFlight) const
{
    // Safety net:
    if (frameInFlight >= Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed frame id is out of range of the frame in flight.");
        return OvVK::CommandPool::empty;
    }

    return reserved->primariesGraphicCommandPools[frameInFlight];
}

OvVK::CommandPool OVVK_API& OvVK::Engine::getPrimaryTransferCmdPool(uint32_t frameInFlight) const
{
    // Safety net:
    if (frameInFlight >= Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed frame id is out of range of the frame in flight.");
        return OvVK::CommandPool::empty;
    }

    return reserved->primariesTransferCommandPools[frameInFlight];
}

OvVK::CommandPool OVVK_API& OvVK::Engine::getPrimaryComputeCmdPool(uint32_t frameInFlight) const
{
    // Safety net:
    if (frameInFlight >= Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed frame id is out of range of the frame in flight.");
        return OvVK::CommandPool::empty;
    }

    return reserved->primariesComputeCommandPools[frameInFlight];
}

OvVK::CommandPool OVVK_API& OvVK::Engine::getPrimaryPresentCmdPool(uint32_t frameInFlight) const
{
    // Safety net:
    if (frameInFlight >= Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed frame id is out of range of the frame in flight.");
        return OvVK::CommandPool::empty;
    }

    return reserved->primariesPresentCommandPools[frameInFlight];
}

uint32_t OVVK_API OvVK::Engine::getPresentableImageIndex(uint32_t frameInFlight) const
{         
    return reserved->presentanbleImageIndex[frameInFlight];
}

OvVK::Storage OVVK_API& OvVK::Engine::getStorage() const
{
    return reserved->storage;
}

#pragma endregion

#pragma region Management

bool OVVK_API OvVK::Engine::processEvents()
{
    // Processes only those events that have already been received and then returns immediately.
    glfwPollEvents();

    if (reserved->presentSurface.isClosed())
    {
        vkDeviceWaitIdle(reserved->logicalDevice.getVkDevice());
        return false;
    }

    while (!reserved->presentSurface.isReady());

    return true;
}

void OVVK_API OvVK::Engine::addPipeline(OvVK::Pipeline& pipeline)
{
    // Safety net:
    // Check if already present
    if (!reserved->pipelines.empty())
        for (auto it = reserved->pipelines.begin(); it != reserved->pipelines.end(); ++it)
            if (it->get().getId() == pipeline.getId()) 
            {
                OV_LOG_ERROR("The passed pipeline with id =%d has already been added", pipeline.getId());
                break;
            }

    reserved->pipelines.push_back(pipeline);
}

void OVVK_API OvVK::Engine::removePipeline(OvVK::Pipeline& pipeline)
{
    if (!reserved->pipelines.empty())
        for (auto it = reserved->pipelines.begin(); it != reserved->pipelines.end(); ++it)
            if (it->get().getId() == pipeline.getId()) 
            {
                reserved->pipelines.erase(it);
                break;
            }
}

bool OVVK_API OvVK::Engine::acquireFrameImage() 
{
    // Increase framerate
    reserved->frameCounter++;

    // Next frame:
    reserved->frameInFlight += 1;
    reserved->frameInFlight %= OvVK::Engine::nrFramesInFlight;

    // Wait all pipeline
    std::vector<VkFence> syncObjsPipeline(reserved->pipelines.size(), VK_NULL_HANDLE);
    uint32_t pipelineCounter = 0;
    for (std::reference_wrapper<OvVK::Pipeline> pipeline : reserved->pipelines)
    {
        syncObjsPipeline[pipelineCounter] = pipeline.get().getStatusSyncObjCPU();
        pipelineCounter++;
    }
    if (pipelineCounter > 0) {
        if (vkWaitForFences(reserved->logicalDevice.getVkDevice(), pipelineCounter,
            syncObjsPipeline.data(),
            VK_TRUE, UINT64_MAX) != VK_SUCCESS)
        {
            // Hard wait 
            if (vkDeviceWaitIdle(reserved->logicalDevice.getVkDevice()) != VK_SUCCESS)
            {
                OV_LOG_ERROR("Fail to wait pipeline.");
                return false;
            }
        }
    }

    // Need to acquire the first image for the first frame in flight
    if (reserved->swapchain.acquireNextImage(reserved->presentanbleImageIndex[reserved->frameInFlight]) == false)
        return false;

    // Wait finish to present
    std::vector<VkFence> syncObjs(2, VK_NULL_HANDLE);
    syncObjs[0] = reserved->swapchain.getImageAvailableSyncObjCPU(reserved->presentanbleImageIndex[reserved->frameInFlight]);
    syncObjs[1] = reserved->swapchain.getPresentationSyncObjCPU(reserved->presentanbleImageIndex[reserved->frameInFlight]);
    if (vkWaitForFences(reserved->logicalDevice.getVkDevice(), syncObjs.size(),
        syncObjs.data(),
        VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        VkResult gg = vkDeviceWaitIdle(reserved->logicalDevice.getVkDevice());
        OV_LOG_ERROR("Fail to wait presentations fences.");
        return false;
    }

    // Clear commandPool
    reserved->primariesComputeCommandPools[reserved->frameInFlight].reset(VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
    reserved->primariesGraphicCommandPools[reserved->frameInFlight].reset(VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
    reserved->primariesPresentCommandPools[reserved->frameInFlight].reset(VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
    reserved->primariesTransferCommandPools[reserved->frameInFlight].reset(VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);

    // Done:
    return true;
}

bool OVVK_API OvVK::Engine::drawFrame()
{
    // Check if the windows is minimized and if it is skip presentation
    if (reserved->presentSurface.isMinimize())
        return true;

    // get syncObjs all pipelines
    std::vector<VkSemaphore> toWaitSyncObj(reserved->pipelines.size(), VK_NULL_HANDLE);
    uint32_t pipelineCounter = 0;
    for (std::reference_wrapper<OvVK::Pipeline> pipeline : reserved->pipelines)
    {
        toWaitSyncObj[pipelineCounter] = pipeline.get().getStatusSyncObjPresentation();
        pipelineCounter++;
    }

    if (waitCommandsAtTheEndOfAFrame) {
        
        std::vector<VkFence> toWaitSyncObjPerformance(reserved->pipelines.size(), VK_NULL_HANDLE);
        uint32_t pipelineCounterPer = 0;
        for (std::reference_wrapper<OvVK::Pipeline> pipeline : reserved->pipelines)
        {
            toWaitSyncObjPerformance[pipelineCounterPer] = pipeline.get().getStatusSyncObjCPU();
            pipelineCounterPer++;
        }
        if (pipelineCounterPer > 0) {
            if (vkWaitForFences(reserved->logicalDevice.getVkDevice(), pipelineCounterPer,
                toWaitSyncObjPerformance.data(),
                VK_TRUE, UINT64_MAX) != VK_SUCCESS)
            {
                OV_LOG_ERROR("Fail to wait pipeline.");
                return false;
            }
        }
    }

    //Present image
    if (reserved->swapchain.presentImage(reserved->presentanbleImageIndex[reserved->frameInFlight], toWaitSyncObj) == false)
        return false;
    //std::vector<VkSemaphore> gg;
    //if (reserved->swapchain.presentImage(reserved->presentanbleImageIndex[reserved->frameInFlight], gg) == false)
    //    return false;

    // Done:
    return true;
}

#pragma endregion

#pragma region Debug

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Are validation layers enable
 * @return TF
 */
bool OVVK_API OvVK::Engine::areValidationLayersEnable() { return enableValidationLayers; }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Is Debug Messanger enable
 * @return TF
 */
bool OVVK_API OvVK::Engine::isDebugMessangerEnable() { return enableDebugMessanger; }

#pragma endregion

#pragma region Extensions

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the instance extensions required by the engine
 * @return std::vector<const char*> Vector containing all the instance extensions required
 */
std::vector<const char*> OVVK_API OvVK::Engine::getRequiredInstanceExtensions() {

    // GLFW has a handy built-in function that returns the extension(s) it needs
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    // GLFW need to be initialized.
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    //Create vector to contain all extensions needed. Start populatin with glfw requested extension.
    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    //Add validation layer extension.
    if (OvVK::Engine::enableValidationLayers) {
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    return extensions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the device extensions required by the engine
 * @return std::vector<const char*> Vector containing all the device extensions required
 */
std::vector<const char*> OVVK_API OvVK::Engine::getRequiredDeviceExtensions() {

    // VK_KHR_SWAPCHAIN_EXTENSION_NAME => It introduces VkSwapchainKHR objects, which provide the ability to present
    //      rendering results to a surface.
    // VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME => adds:
    //      - A new ray tracing pipeline, A shader binding indirection table, Ray tracing commands.
    // VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME => adds:
    //      - Acceleration structure objects and build commands.
    //      - Structures to describe geometry inputs to acceleration structure builds.
    //      - Acceleration structure copy commands.
    // VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME => Extension defines the infrastructure and usage patterns for
    //      deferrable commands, but does not specify any commands as deferrable.
    // VK_KHR_PIPELINE_LIBRARY_EXTENSION_NAME => A pipeline library is a special pipeline that cannot be bound, 
    //      instead it defines a set of shaders and shader groups which can be linked into other pipelines. 
    //      This extension defines the infrastructure for pipeline libraries, but does not specify the creation
    //      or usage of pipeline libraries. This is left to additional dependent extensions.
    std::vector<const char*> requiredDeviceExtensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME,
        VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME,
        VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME,
        VK_KHR_PIPELINE_LIBRARY_EXTENSION_NAME
    };

    return requiredDeviceExtensions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get the required validation layers
 * @return std::vector<const char*> Vector containig the name of the required validation layers
 */
std::vector<const char*> OVVK_API OvVK::Engine::getRequiredValidationLayers() {

    // VK_LAYER_KHRONOS_validation => Layers activation
    std::vector<const char*> validationLayers = {
        "VK_LAYER_KHRONOS_validation"
    };

    return validationLayers;
}

#pragma endregion

#pragma region InitComponenet

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization of the window (GLFW)
 * @return TF
 */
bool OVVK_API OvVK::Engine::initGLFW() {

    // Init glfw:
    OV_LOG_PLAIN("Initialization of glfw: ...");

    typedef void(*GLWF_ERROR_CALLBACK_PTR)(int32_t error, const char* description);
    glfwSetErrorCallback(static_cast<GLWF_ERROR_CALLBACK_PTR>
        (
            // SurfaceCallbackTypes:
            [](int32_t error, const char* description)
            {
                OV_LOG_ERROR("[GLFW] code: %s, %s", error, description);
            }
    ));


    // Init framework:
    if (!glfwInit())
    {
        OV_LOG_ERROR("Unable to init GLFW");
        return false;
    }

    int32_t glfwMajor, glfwMinor, glfwRev;
    glfwGetVersion(&glfwMajor, &glfwMinor, &glfwRev);
    OV_LOG_ERROR("Using GLFW %d.%d.%d", glfwMajor, glfwMinor, glfwRev);

    //tell it to not create an OpenGL context
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    // resized windows disable
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization of the vulkan instance
 * @param appName The name of the application.
 * @return TF
 */
bool OVVK_API OvVK::Engine::initInstance(std::string appName) {

    // Initialization vulkan instance
    OV_LOG_PLAIN("Initialization of the vulkan instance: ...");

    // This data is technically optional. Provide some useful information to the driver in order to optimize 
    // our specific application
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pNext = VK_NULL_HANDLE;

    appInfo.pApplicationName = appName.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);

    appInfo.pEngineName = "OverVision Vulkan Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);

    appInfo.apiVersion = VK_API_VERSION_1_3;

    if (reserved->vulkanInstance.create(appInfo, getRequiredInstanceExtensions(),
        enableDebugMessanger, enableValidationLayers, getRequiredValidationLayers()) == false)
        return false;

    return true; // Done init vulkan instance
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization of a surface to present rendered images to
 * @return TF
 */
bool OVVK_API OvVK::Engine::initSurface(std::string title, uint32_t width, uint32_t height) {

    // Initialization presentation surface:
    OV_LOG_PLAIN("Initialization of a presentation surface: ...");

    if (reserved->presentSurface.create(title, width, height) == false)
        return false;

    return true; // Done create presentation surface
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialization of a logical device
 * @return TF
 */
bool OVVK_API OvVK::Engine::initLogicalDevice() {

    // Initialization presentation surface:
    OV_LOG_PLAIN("Initialization of a logical device: ...");

    // The next information to specify is the set of device features that we'll be using
    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE; ///< specifies whether anisotropic filtering is supported.
    // MSAA only smoothens out the edges of geometry but not the interior filling. 
    // This may lead to a situation when you get a smooth polygon rendered on screen
    // but the applied texture will still look aliased if it contains high contrasting colors. 
    // One way to approach this problem is to enable Sample Shading which will improve the image quality even further
    deviceFeatures.sampleRateShading = VK_TRUE; ///< enable sample shading feature for the device
    deviceFeatures.shaderInt64 = VK_TRUE; ///< shaderInt64 specifies whether 64-bit integers (signed and unsigned)
    // are supported in shader code. 


    VkPhysicalDeviceVulkan13Features physicalDeviceVulkan13Features = {};
    physicalDeviceVulkan13Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
    physicalDeviceVulkan13Features.synchronization2 = VK_TRUE;
    physicalDeviceVulkan13Features.pNext = NULL;

    VkPhysicalDeviceVulkan12Features physicalDeviceVulkan12Features = {};
    physicalDeviceVulkan12Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    physicalDeviceVulkan12Features.timelineSemaphore = VK_TRUE;
    // Indicates that the implementation supports accessing buffer memory in shaders as storage buffers
    // via an address queried from vkGetBufferDeviceAddressEXT.
    physicalDeviceVulkan12Features.bufferDeviceAddress = VK_TRUE;
    // Indicates that the implementation supports saving and reusing buffer addresses, e.g. for trace
    // capture and replay.
    physicalDeviceVulkan12Features.bufferDeviceAddressCaptureReplay = VK_FALSE;
    // Indicates that the implementation supports the bufferDeviceAddress feature for logical devices
    // created with multiple physical devices.
    physicalDeviceVulkan12Features.bufferDeviceAddressMultiDevice = VK_FALSE;
    physicalDeviceVulkan12Features.descriptorBindingSampledImageUpdateAfterBind = VK_TRUE;
    physicalDeviceVulkan12Features.descriptorBindingStorageImageUpdateAfterBind = VK_TRUE;
    physicalDeviceVulkan12Features.descriptorBindingStorageBufferUpdateAfterBind = VK_TRUE;
    physicalDeviceVulkan12Features.descriptorBindingUniformBufferUpdateAfterBind = VK_TRUE;
    physicalDeviceVulkan12Features.runtimeDescriptorArray = VK_TRUE;
    physicalDeviceVulkan12Features.shaderSampledImageArrayNonUniformIndexing = VK_TRUE;
    physicalDeviceVulkan12Features.pNext = &physicalDeviceVulkan13Features;
    

    VkPhysicalDeviceVulkan11Features physicalDeviceVulkan11Features = {};
    physicalDeviceVulkan11Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
    physicalDeviceVulkan11Features.pNext = &physicalDeviceVulkan12Features;
    /*physicalDeviceVulkan11Features.storageBuffer16BitAccess = VK_TRUE;
    physicalDeviceVulkan11Features.storageInputOutput16 = VK_FALSE;
    physicalDeviceVulkan11Features.storagePushConstant16 = VK_TRUE;
    physicalDeviceVulkan11Features.uniformAndStorageBuffer16BitAccess = VK_TRUE;
    physicalDeviceVulkan11Features.samplerYcbcrConversion = VK_TRUE;
    physicalDeviceVulkan11Features.protectedMemory = VK_FALSE;;
    physicalDeviceVulkan11Features.shaderDrawParameters = VK_TRUE;
    physicalDeviceVulkan11Features.variablePointers = VK_TRUE;
    physicalDeviceVulkan11Features.variablePointersStorageBuffer = VK_TRUE;*/
    
    // Ray tracing structs

    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayTracingPipelineFeaturesKHR.html
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR rtPipelineFeature{ 
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR };
    // pNext is NULL or a pointer to a structure extending this structure.
    rtPipelineFeature.pNext = &physicalDeviceVulkan11Features,
    // indicates whether the implementation supports the ray tracing pipeline functionality
    rtPipelineFeature.rayTracingPipeline = VK_TRUE;

    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceAccelerationStructureFeaturesKHR.html
    VkPhysicalDeviceAccelerationStructureFeaturesKHR accelFeature{ 
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR };
    // pNext is NULL or a pointer to a structure extending this structure.
    accelFeature.pNext = &rtPipelineFeature;
    // indicates whether the implementation supports the acceleration structure functionality.
    accelFeature.accelerationStructure = VK_TRUE;
    // indicates whether the implementation supports saving and reusing acceleration structure 
    // device addresses
    accelFeature.accelerationStructureCaptureReplay = VK_FALSE;
    // indicates whether the implementation supports indirect acceleration structure build commands
    accelFeature.accelerationStructureIndirectBuild = VK_FALSE;
    // indicates whether the implementation supports host side acceleration structure commands
    accelFeature.accelerationStructureHostCommands = VK_FALSE;
    // indicates whether the implementation supports updating acceleration structure descriptors 
    // after a set is bound
    accelFeature.descriptorBindingAccelerationStructureUpdateAfterBind = VK_TRUE;


    // Create logical device
    if (reserved->logicalDevice.create(deviceFeatures,
        VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT | VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT,
        &accelFeature) == false)
    {
        OV_LOG_ERROR("Fail to create logical device.");
        return false;
    }

    return true; //Done initialize the logical device
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialise the swapchain
 * @return TF
 */
bool OVVK_API OvVK::Engine::initSwapChain() {

    // Initialization of VMA:
    OV_LOG_PLAIN("Initialization of swapchain: ...");

    // The resolution {WIDTH, HEIGHT} that we specified earlier when creating the window is measured 
    // in screen coordinates.
    if (reserved->swapchain.create(reserved->presentSurface.getWidth(),
        reserved->presentSurface.getHeight()) == false)
    {
        OV_LOG_ERROR("Fail to create swapchain.");
        return false;
    }

    return true; // Done initialize swapChain
}

bool OVVK_API OvVK::Engine::initCommandPools() 
{
    // Initialization presentation surface:
    OV_LOG_PLAIN("Initialization the primary commandPools and commandBuffers: ...");

    // SetUp CommandPools Handles
    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++) 
    {
        if (reserved->primariesGraphicCommandPools[c].create(OvVK::CommandPool::CommandTypesFlagBits::Graphic,
                VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT) == false ||
            reserved->primariesTransferCommandPools[c].create(OvVK::CommandPool::CommandTypesFlagBits::Transfer,
                VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT) == false ||
            reserved->primariesComputeCommandPools[c].create(OvVK::CommandPool::CommandTypesFlagBits::Compute,
                VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT) == false ||
            reserved->primariesPresentCommandPools[c].create(OvVK::CommandPool::CommandTypesFlagBits::Present,
                VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT) == false
            )
        {
            OV_LOG_ERROR("Fail to initialize commandPools for frame nr. %d", c);
            return false;
        }
    }

    // Done:
    return true;
}

bool OVVK_API OvVK::Engine::initDescriptorSets() 
{
    // Initialization presentation surface:
    OV_LOG_PLAIN("Initialization of the descriptorSetsManagers used in the engine.");

    OvVK::TexturesDescriptorSetsManager::getInstance();

    // Done:
    return true;
}

bool OVVK_API OvVK::Engine::initStorage() 
{
    OV_LOG_PLAIN("Initialization of the storage used in the engine.");

    if (reserved->storage.create() == false) 
    {
        OV_LOG_ERROR("Fail to create the storage.");
        return false;
    }

    // Done:
    return true;
}

bool OVVK_API OvVK::Engine::initShaderPreprocessor() 
{
    // Init preprocessor
    OvVK::Geometry::addShaderPreproc();                         // include Geometry
    OvVK::Storage::addShaderPreproc();                          // include Storage
    OvVK::RTObjDesc::addShaderPreproc();                        // include RTObjDesc
    OvVK::WhittedRTwithPBRPipeline::addShaderPreproc();                 // include WhittedRTwithPBRPipeline
    OvVK::PickingPipeline::addShaderPreproc();                  // include PickingPipeline


    ///////////////
    // Constants //
    ///////////////

    std::string constantsPreproc = R"(
#ifndef OvVK_CONSTANTS_INCLUDED
#define OvVK_CONSTANTS_INCLUDED

    const float PI = 3.14159265359;
    const float generalRefractionIndex = 1.0f;
    const float thresholdRefractionDistance = 0.01f;

    #define UINT_MAX        4294967295          // Max uint (32 bit) value

#endif
)";

    OvVK::Shader::preprocessor.set("include Constants", constantsPreproc);

    OvVK::Shader::preprocessor.set("sbtRecordStride", std::to_string(magic_enum::enum_integer(OvVK::RayTracingPipeline::RayType::last)));

    // Done:
    return true;
}

#pragma endregion