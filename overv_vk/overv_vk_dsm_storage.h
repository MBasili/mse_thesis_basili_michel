/**
 * @file	overv_vk_dsm_storage.h
 * @brief	Descriptor set manager for storage
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

/**
 * @brief Class for modeling an descriptor set manager for storage.
 */
class OVVK_API StorageDescriptorSetsManager final : public OvVK::DescriptorSetsManager
{
//////////
public: //
//////////

	// Static
	static StorageDescriptorSetsManager empty;

	/**
	 * @brief Definition of binding points.
	 */
	enum class Binding : uint32_t
	{
		globalUniform = 0,
		materials = 1,
		lights = 2,
		rtObjDescs = 3
	};

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Struct used to update address of the storage buffer
	 */
	struct UpdateData
	{
		uint32_t targetFrame;
		std::optional<std::reference_wrapper<OvVK::Buffer>> globalUniformDeviceBuffer;
		std::optional<std::reference_wrapper<OvVK::Buffer>> rtObjDescsDeviceBuffer;
		std::optional<std::reference_wrapper<OvVK::Buffer>> materialsDeviceBuffer;
		std::optional<std::reference_wrapper<OvVK::Buffer>> lightsDeviceBuffer;

		UpdateData() : targetFrame{ 0 }, globalUniformDeviceBuffer{ std::nullopt },
			rtObjDescsDeviceBuffer{ std::nullopt }, materialsDeviceBuffer{ std::nullopt },
			lightsDeviceBuffer{ std::nullopt }

		{}
	};
#endif

	// Const/dest:
	StorageDescriptorSetsManager();
	StorageDescriptorSetsManager(StorageDescriptorSetsManager&& other);
	StorageDescriptorSetsManager(StorageDescriptorSetsManager const&) = delete;
	virtual ~StorageDescriptorSetsManager();

	// Operators
	StorageDescriptorSetsManager& operator=(const StorageDescriptorSetsManager&) = delete;
	StorageDescriptorSetsManager& operator=(StorageDescriptorSetsManager&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	virtual bool create() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool addToUpdateList(const UpdateData& data);
#endif

	// Rendering:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	StorageDescriptorSetsManager(const std::string& name);
};
