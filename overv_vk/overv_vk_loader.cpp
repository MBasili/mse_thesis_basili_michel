/**
 * @file	overv_vk_loader.cpp
 * @brief	Base 3D model loading functionality
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// Thread:
#include "overv_vk_thread_pool.h"

// Magic enum:
#include "magic_enum.hpp"

// Assimp:    
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/matrix4x4.h>
#include <assimp/cimport.h>
#include <assimp/material.h>
#include <assimp/pbrmaterial.h>

// C/C++
#include <filesystem> // C++17
#include <cmath>
#include <iostream>


////////////
// #ENUMS //
////////////

// Enum used to idenfy a thread 
enum class ThreadIdentifier : uint32_t
{
    geometries = 0,
    textures = 1,
    all
};


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Struct to use multiple thread to record commands.
 */
struct OvVK::Loader::ThreadData {
    // Thread commandPool
    OvVK::CommandPool commandPool;
    // CommandBuffer per obj
    std::vector<std::reference_wrapper<OvVK::CommandBuffer>> commandBuffers;
};

/**
 * @brief OvVK loader reserved structure.
 */
struct OvVK::Loader::Reserved
{
    // Assimp importer
    Assimp::Importer importer;
    const aiScene* modelScene;

    // Reference to data to Upload
    UploadData uploadData;

    // Status flags:
    bool tooManyMaterialsFlag;

    /**
     * Constructor.
     */
    Reserved() : modelScene { nullptr },
        tooManyMaterialsFlag{ false }
    {
        // Specifies the maximum angle that may be between two face normals at the same vertex position that their are smoothed together.
        importer.SetPropertyFloat("PP_GSN_MAX_SMOOTHING_ANGLE", 45);
    }
};


//////////////////////////
// BODY OF CLASS Loader //
//////////////////////////

#pragma region Const/dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Loader::Loader() : reserved(std::make_unique<OvVK::Loader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 * @param name loader name
 */
OVVK_API OvVK::Loader::Loader(const std::string& name) : Ov::Object(name),
reserved(std::make_unique<OvVK::Loader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Loader::Loader(Loader&& other) noexcept : Ov::Object(std::move(other)),
reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Loader::~Loader()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Get/set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * True when the tooManyMaterials error has been triggered.
 * @return TF
 */
bool OVVK_API OvVK::Loader::tooManyMaterials()
{
    return reserved->tooManyMaterialsFlag;
}

#pragma endregion

#pragma region Loading

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load 3D scene.
 * @param filename 3D file.
 * @param pFlags Assimp flags for post processing.
 * @return TF
 */
bool OVVK_API OvVK::Loader::load(const std::string& filename, unsigned int pFlags)
{

    showParamsUsed(pFlags);

    ////////////////////
    // Prepare import //
    ////////////////////

    reserved->modelScene = reserved->importer.ReadFile(OvVK::Engine::modelsSourceFolder + "\\" + filename, pFlags);

    if (reserved->modelScene == nullptr)
    {
        OV_LOG_ERROR("Error loading file '%s': %s", filename.c_str(), reserved->importer.GetErrorString());
        return false;
    }

    // Scene stats:
    OV_LOG_PLAIN("   Nr. of meshes :  %u", reserved->modelScene->mNumMeshes);
    OV_LOG_PLAIN("   Nr. of lights :  %u", reserved->modelScene->mNumLights);

    if (reserved->modelScene->mMetaData)
    {
        int upAxis = 0;
        reserved->modelScene->mMetaData->Get<int>("UpAxis", upAxis);
        int upAxisSign = 1;
        reserved->modelScene->mMetaData->Get<int>("UpAxisSign", upAxisSign);
        int frontAxis = 0;
        reserved->modelScene->mMetaData->Get<int>("FrontAxis", frontAxis);
        int frontAxisSign = 1;
        reserved->modelScene->mMetaData->Get<int>("FrontAxisSign", frontAxisSign);
        int coordAxis = 0;
        reserved->modelScene->mMetaData->Get<int>("CoordAxis", coordAxis);
        int coordAxisSign = 1;
        reserved->modelScene->mMetaData->Get<int>("CoordAxisSign", coordAxisSign);

        aiVector3D upVec = upAxis == 0 ? aiVector3D(-(ai_real)upAxisSign, 0, 0) : upAxis == 1 ? aiVector3D(0, -(ai_real)upAxisSign, 0) : aiVector3D(0, 0, -(ai_real)upAxisSign);
        aiVector3D forwardVec = frontAxis == 0 ? aiVector3D((ai_real)frontAxisSign, 0, 0) : frontAxis == 1 ? aiVector3D(0, (ai_real)frontAxisSign, 0) : aiVector3D(0, 0, (ai_real)frontAxisSign);
        aiVector3D rightVec = coordAxis == 0 ? aiVector3D((ai_real)coordAxisSign, 0, 0) : coordAxis == 1 ? aiVector3D(0, (ai_real)coordAxisSign, 0) : aiVector3D(0, 0, (ai_real)coordAxisSign);
        aiMatrix4x4 mat(rightVec.x, rightVec.y, rightVec.z, 0.0f,
            upVec.x, upVec.y, upVec.z, 0.0f,
            forwardVec.x, forwardVec.y, forwardVec.z, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f);

        reserved->modelScene->mRootNode->mTransformation *= mat;

        float factor;
        if (reserved->modelScene->mMetaData->Get("UnitScaleFactor", factor))
            OV_LOG_PLAIN("   Scaling factor:  %f", factor);
        else
            OV_LOG_PLAIN("   Scaling factor:  N/A");
    }

    /////////
    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp light as OvVK::Light.
 * @param light pointer to aiLight
 * @param light in/out light
 * @param node in/out node (Assimp stores the light position in the light props and not in its node)
 * @return TF
 */
bool OVVK_API OvVK::Loader::loadLight(void* light, OvVK::Light& l, Ov::Node& node)
{
    // Safety net:
    if (light == nullptr || l == OvVK::Light::empty)
    {
        OV_LOG_ERROR("Invalid params passed to the loadLight method of the loader with id= %d", getId());
        return false;
    }

    aiLight* _light = reinterpret_cast<aiLight*>(light);

    // Set node position
    node.setMatrix(node.getMatrix() * glm::translate(glm::mat4(1.0f), glm::vec3(_light->mPosition.x, _light->mPosition.y, _light->mPosition.z)));

    // Set props:
    // Name
    l.setName(_light->mName.C_Str());

    // Type
    switch (_light->mType)
    {
        //////////////////////////////
    case aiLightSource_DIRECTIONAL: //
        l.setLightType(OvVK::Light::Type::directional);
        break;
        ////////////////////////
    case aiLightSource_POINT: //
        l.setLightType(OvVK::Light::Type::omni);
        break;
        ///////////////////////
    case aiLightSource_SPOT: //
        l.setLightType(OvVK::Light::Type::spot);
        break;
    default:
        OV_LOG_ERROR("Light type not supported in the loader with id= %d", getId());
        return false;
    }

    // Set color of the light
    l.setColor(glm::vec3(_light->mColorDiffuse.r, _light->mColorDiffuse.g, _light->mColorDiffuse.b));
    l.setPower(0);

    // Relative to the transformation of the node corresponding to the light.
    // The direction is undefined for point lights. The vector may be normalized, but it needn't.
    glm::vec3 dirVec = glm::vec3(_light->mDirection.x, _light->mDirection.y, _light->mDirection.z);
    l.setDirection(glm::normalize(dirVec));

    // CutOff value in radians
    l.setSpotCutoff(_light->mAngleInnerCone);

    // SpotExponent value in radians
    // sources formula: https://learnopengl.com/Lighting/Light-casters
    l.setSpotExponent(cos(_light->mAngleInnerCone) - cos(_light->mAngleOuterCone));

    OV_LOG_DEBUG("Light  . . . :  %s", l.getName().c_str());
    OV_LOG_DEBUG("Color  . . . :  %s", glm::to_string(l.getColor()).c_str());

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp mesh as OvVK::Geometry.
 * @param mesh pointer to aiMesh
 * @param geometry in/out geometry
 * @return TF
 */
bool OVVK_API OvVK::Loader::loadGeometry(void* mesh, OvVK::Geometry& g)
{
    // Safety net:
    if (mesh == nullptr || g == OvVK::Geometry::empty)
    {
        OV_LOG_ERROR("Invalid params passed to the loadGeometry method of the loader with id= %d", getId());
        return false;
    }

    aiMesh* _mesh = reinterpret_cast<aiMesh*>(mesh);
    g.setName(_mesh->mName.C_Str());
    g.reset();

    // Process vertices:
    for (unsigned int v = 0; v < _mesh->mNumVertices; v++)
    {
        Ov::Geometry::VertexData vd;
        vd.vertex.x = _mesh->mVertices[v].x;
        vd.vertex.y = _mesh->mVertices[v].y;
        vd.vertex.z = _mesh->mVertices[v].z;

        // glm::packSnorm3x10_1x2 better but not GLSL function to unpack
        if (_mesh->HasNormals())
            vd.normal = glm::packSnorm4x8(glm::vec4(_mesh->mNormals[v].x,
                _mesh->mNormals[v].y,
                _mesh->mNormals[v].z, 0.0f));

        if (_mesh->HasTextureCoords(0))
            vd.uv = glm::packHalf2x16(glm::vec2(_mesh->mTextureCoords[0][v].x, _mesh->mTextureCoords[0][v].y));

        if (_mesh->HasTangentsAndBitangents())
            vd.tangent = glm::packSnorm4x8(glm::vec4(_mesh->mTangents[v].x,
                _mesh->mTangents[v].y,
                _mesh->mTangents[v].z, 0.0f));

        // Store it:
        g.addVertex(vd);
    }

    // Process faces:
    for (unsigned int f = 0; f < _mesh->mNumFaces; f++)
    {
        Ov::Geometry::FaceData fd;

        aiFace* face = &_mesh->mFaces[f];
        fd.a = face->mIndices[0];
        fd.b = face->mIndices[1];
        fd.c = face->mIndices[2];

        // Store it:
        g.addFace(fd);
    }

    OV_LOG_DEBUG("Geometry . . :  %s", g.getName().c_str());

    // Done
    return g.setUp();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load Assimp material as OvVK::Material.
 * @param mesh pointer to the aiMesh holding the material
 * @param m in/out material
 * @return TF
 */
bool OVVK_API OvVK::Loader::loadMaterial(void* material, OvVK::Material& m)
{
    // Safety net:
    if (material == nullptr || m == OvVK::Material::empty)
    {
        OV_LOG_ERROR("Invalid params passed to the loadMaterial method of the loader with id= %d", getId());
        return false;
    }

    aiMaterial* _material = reinterpret_cast<aiMaterial*>(material);

    // Pass parameters:
    m.setName(_material->GetName().C_Str());

    int shadingModel;
    aiGetMaterialInteger(_material, AI_MATKEY_SHADING_MODEL, &shadingModel);

    aiColor3D color;
    float value;


    // Emission:
    if (_material->Get(AI_MATKEY_COLOR_EMISSIVE, color) == AI_SUCCESS)
    {
        m.setEmission(glm::vec3(color.r, color.g, color.b));
    }
    else
    {
        m.setEmission(glm::vec3(0.0f));
        OV_LOG_WARN("Material '%s' has no emission: using default color", m.getName().c_str());
    }

    // Albedo:
    if (_material->Get(AI_MATKEY_COLOR_AMBIENT, color) == AI_SUCCESS)
    {
        m.setAlbedo(glm::vec3(color.r, color.g, color.b));
    }
    else
    {
        OV_LOG_WARN("Material '%s' has no ambient: using default color", m.getName().c_str());
        m.setAlbedo(glm::vec3(0.0f));
    }

    // Opacity
    if (_material->Get(AI_MATKEY_OPACITY, value) == AI_SUCCESS)
    {
        m.setOpacity(value);
    }
    else
    {
        m.setOpacity(1.0f);
        OV_LOG_WARN("Material '%s' has no opacity: using default value", m.getName().c_str());
    }

    // Roughness
    if (_material->Get(AI_MATKEY_ROUGHNESS_FACTOR, value) == AI_SUCCESS)
    {
        m.setRoughness(value < 0 ? 0 : value);
    }
    else
    {
        m.setRoughness(0.0f);
        OV_LOG_WARN("Material '%s' has no roughness: using default value", m.getName().c_str());
    }

    // Metalness
    if (_material->Get(AI_MATKEY_METALLIC_FACTOR, value) == AI_SUCCESS)
    {
        m.setMetalness(value);
    }
    else
    {
        m.setMetalness(0.0f);
        OV_LOG_WARN("Material '%s' has no roughness: using default value", m.getName().c_str());
    }

    // Refraction index
    if (_material->Get(AI_MATKEY_REFRACTI, value) == AI_SUCCESS)
    {
        m.setRefractionIndex((value == 0 ? 1.0f : value));
    }
    else
    {
        m.setRefractionIndex(1.0f);
        OV_LOG_WARN("Material '%s' has no refraction index: using default value", m.getName().c_str());
    }

#ifdef OV_DEBUG
    OV_LOG_PLAIN("Material . . :  %s", m.getName().c_str());
    OV_LOG_PLAIN("Emission . . :  %s", glm::to_string(m.getEmission()).c_str());
    OV_LOG_PLAIN("Albedo . . . :  %s", glm::to_string(m.getAlbedo()).c_str());
    OV_LOG_PLAIN("Opacity  . . :  %f", m.getOpacity());
    OV_LOG_PLAIN("Roughness  . :  %f", m.getRoughness());
    OV_LOG_PLAIN("Metalness  . :  %f", m.getMetalness());
    OV_LOG_PLAIN("Refraction index  . :  %f", m.getRefractionIndex());
#endif

    //Done: 
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load texure2D as OvVK::Texture2D.
 * @param imageName The name of the texture to load.
 * @param t The OvVK texture2D in which load the image.
 * @param type The OvVK texture type of the texture2D.
 * @return TF
 */
bool OVVK_API OvVK::Loader::loadTexture2D(std::string imageName, OvVK::Texture2D& t, Ov::Texture::Type type)
{
    // Texture create
    t.setName(imageName);

    // Load bitmap
    OvVK::Bitmap bitmap;
    // Check extension 
    bool loadingResult = false;
    if (imageName.substr(imageName.find_last_of(".") + 1) == "dds") {
        loadingResult = bitmap.loadDDS(OvVK::Engine::texturesSourceFolder + "\\" + imageName);
    }
    if (imageName.substr(imageName.find_last_of(".") + 1) == "ktx") {

        Ov::Bitmap::Format formatToLoad = Ov::Bitmap::Format::r8g8b8a8_compressed;

        switch (type) {
        case Ov::Texture::Type::metalness:
        case Ov::Texture::Type::roughness:
        case Ov::Texture::Type::height:
            formatToLoad = Ov::Bitmap::Format::r8_compressed;
            break;
        case Ov::Texture::Type::normal:
            formatToLoad = Ov::Bitmap::Format::r8g8_compressed;
            break;
        default:
            break;
        }

        loadingResult = bitmap.loadKTX(OvVK::Engine::texturesSourceFolder + "\\" + imageName, formatToLoad);
    }
    else {
        loadingResult = bitmap.loadWithFreeImage(OvVK::Engine::texturesSourceFolder + "\\" + imageName, false, false, true);
    }

    // Load bitmap into texture
    if (loadingResult == false)
    {
        OV_LOG_ERROR("Fail to create the bitmap for '%s' image in loader with id= %d.", imageName.c_str(), getId());
        return false;
    }

    bool isLinearEncoded = true;
    if (type == Ov::Texture::Type::albedo) {
        isLinearEncoded = false;
    }

    if (t.setUp(bitmap, isLinearEncoded) == false) {
        OV_LOG_ERROR("Fail to setUp bitmap with name %s in the texture2D with id= %d in loader with id= %d.",
            imageName, t.getId(), getId());
        return false;
    }

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load sampler as OvVK::Sampler.
 * @param t A OvVK texture reference.
 * @param s A OvVK sampler reference.
 * @param textureMapMode The Assimp aiTextureMapMode enum to know the wrap mode of the texture.
 * @return TF
 */
bool OVVK_API OvVK::Loader::loadSampler(OvVK::Texture& t, OvVK::Sampler& s, void* textureMapMode)
{
    VkSamplerCreateInfo samplerCreateInfo =
        OvVK::Sampler::getDefaultSamplerCreateInfo();

    // Check if mapmode is null
    aiTextureMapMode* _mapMode = reinterpret_cast<aiTextureMapMode*>(textureMapMode);
    if (_mapMode != nullptr)
    {
        switch (*_mapMode)
        {
        case aiTextureMapMode_Wrap:
            samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            break;
        case aiTextureMapMode_Clamp:
            samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            break;
        case aiTextureMapMode_Decal:
            samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
            samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
            samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
            break;
        case aiTextureMapMode_Mirror:
            samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
            samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
            samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
            break;
        default:
            break;
        }
    }

    samplerCreateInfo.maxLod = t.getImage().getNrOfLevels();

    return s.create(samplerCreateInfo);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load whole content starting from root node. A list is used instead of a vector because elements are not randomly reallocated.
 * @param container in/out data container
 * @param current current assimp node (starts with nullptr for root)
 * @return TF
 */
Ov::Node OVVK_API& OvVK::Loader::loadScene(OvVK::Container& container, void* current)
{
    // Root node?
    aiNode* _node;
    static int meshCount = 0;
    if (current == nullptr)
    {
        // Clear uplaod data for multi thread
        reserved->uploadData.geometriesStagingBufferSize = 0;
        reserved->uploadData.geometries.clear();
        reserved->uploadData.lights.clear();
        reserved->uploadData.materials.clear();
        reserved->uploadData.nodes.clear();

        _node = reserved->modelScene->mRootNode;
        // The following does not change anything. So even if names are used to identify nodes, this is not it...
        _node->mName.Append(std::to_string(meshCount++).c_str());
    }
    else
        _node = reinterpret_cast<aiNode*>(current);

    // Setup node:
    Ov::Node n;

    std::string nodeName = std::string(_node->mName.C_Str()) + "_Node";
    OV_LOG_DEBUG(nodeName.c_str());
    n.setName(nodeName.c_str());

    // Set matrix:
    glm::mat4 m;
    memcpy(&m, &_node->mTransformation, sizeof(glm::mat4));
    m = glm::transpose(m); // Assimp matrices are transposed wrt OpenGL
    n.setMatrix(m);

    // Check if this node contains a light:
    bool isLight = false;
    for (uint32_t c = 0; c < reserved->modelScene->mNumLights; c++)
    {
        aiLight* _light = reserved->modelScene->mLights[c];
        if (_light->mName == _node->mName)
        {
            isLight = true;

            OvVK::Light l;
            this->loadLight(_light, l, n);
            container.add(l);
            n.addRenderable(container.getLastLight(), Ov::Renderable::empty);
            // Add to process multi thread
            reserved->uploadData.lights.push_back(container.getLastLight());
        }
    }

    // Add mesh data?
    if (!isLight)
        switch (_node->mNumMeshes)
        {
            //////////
        case 0: //
           // Logical node, nothing to do
            break;

            ///////////
        default: // Can also be multi-mat   
        {
            for (uint32_t c = 0; c < _node->mNumMeshes; c++)
            {
                // Get mesh
                aiMesh* aimesh = reserved->modelScene->mMeshes[_node->mMeshes[c]];


                //////////////
                // Geometry //
                //////////////

                Ov::Object& geomFound = container.find(aimesh->mName.C_Str());
                std::reference_wrapper<OvVK::Geometry> geom = OvVK::Geometry::empty;
                if (geomFound != Ov::Object::empty)
                {
                    OV_LOG_WARN("Geometry already loaded in loader with id= %d.", getId());
                    geom = dynamic_cast<OvVK::Geometry&>(geomFound);
                }
                else
                {
                    OvVK::Geometry g;
                    if (this->loadGeometry(aimesh, g) == false) {
                        OV_LOG_ERROR("Fail to load geometry with name %s in the loader with id= %d.", aimesh->mName.C_Str(), getId());
                        continue;
                    }
                    container.add(g);
                    geom = container.getLastGeometry();

                    // Add to multi thread
                    reserved->uploadData.geometriesStagingBufferSize += (geom.get().getNrOfFaces() * sizeof(Ov::Geometry::FaceData));
                    reserved->uploadData.geometriesStagingBufferSize += (geom.get().getNrOfVertices() * sizeof(Ov::Geometry::VertexData));
                    reserved->uploadData.geometries.push_back(geom.get());
                }


                //////////////
                // Material //
                //////////////

                aiMaterial* aimaterial = reserved->modelScene->mMaterials[aimesh->mMaterialIndex];
                Ov::Object& materialFound = container.find(aimaterial->GetName().C_Str());
                std::reference_wrapper<OvVK::Material> material = OvVK::Material::empty;
                if (materialFound != Ov::Object::empty)
                {
                    OV_LOG_WARN("Material already loaded in loader with id= %d.", getId());
                    material = dynamic_cast<OvVK::Material&>(materialFound);
                }
                else
                {
                    OvVK::Material m;

                    if (m.getLutPos() == OvVK::Material::maxNrOfMaterials)
                    {
                        OV_LOG_ERROR("Too many materials: default one used instead in the loader with id= %d.", getId());
                        reserved->tooManyMaterialsFlag = true;
                        material = const_cast<OvVK::Material&>(OvVK::Material::getDefault());
                    }
                    else if (this->loadMaterial(aimaterial, m) == false)
                    {
                        OV_LOG_ERROR("Fail to load material name= %s: default one used instead in the loader with id= %d.",
                            aimaterial->GetName().C_Str(), getId());
                        material = const_cast<OvVK::Material&>(OvVK::Material::getDefault());
                    }
                    else
                    {
                        container.add(m);
                        material = container.getLastMaterial();
                        // To load in the multi thread
                        reserved->uploadData.materials.push_back(material.get());


                        /////////////
                        // Texture //
                        /////////////

                        // Emission Texture
                        //aimaterial->GetTexture(aiTextureType_EMISSION_COLOR, 0, &nnn);
                        // Texture to load if present
                        std::vector<aiTextureType> textureTypeToLoad = {
                            aiTextureType_BASE_COLOR,
                            aiTextureType_NORMAL_CAMERA,
                            aiTextureType_HEIGHT,
                            aiTextureType_DIFFUSE_ROUGHNESS,
                            aiTextureType_METALNESS,
                            aiTextureType_SHININESS,
                            aiTextureType_SPECULAR,
                            aiTextureType_AMBIENT_OCCLUSION,
                            aiTextureType_UNKNOWN,
                            aiTextureType_EMISSIVE
                        };

                        // Check all type of texture to load
                        for (aiTextureType textureType : textureTypeToLoad)
                        {
                            // Check if the material has one texture for te he current type.
                            if (aimaterial->GetTextureCount(textureType) >= 1)
                            {
                                // Get texture type in overv
                                Ov::Texture::Type overvTextureType;
                                switch (textureType)
                                {
                                case aiTextureType_BASE_COLOR:
                                    overvTextureType = Ov::Texture::Type::albedo;
                                    break;
                                case aiTextureType_NORMAL_CAMERA:
                                    overvTextureType = Ov::Texture::Type::normal;
                                    break;
                                case aiTextureType_HEIGHT:
                                    overvTextureType = Ov::Texture::Type::height;
                                    break;
                                case aiTextureType_DIFFUSE_ROUGHNESS:
                                    overvTextureType = Ov::Texture::Type::roughness;
                                    break;
                                case aiTextureType_METALNESS:
                                    overvTextureType = Ov::Texture::Type::metalness;
                                    break;
                                case aiTextureType_AMBIENT_OCCLUSION:
                                    overvTextureType = Ov::Texture::Type::roughness;
                                    break;
                                default:
                                    OV_LOG_ERROR("Texture type not found in the loader with id= %d.", getId());
                                    continue;
                                }

                                // Needed parameters
                                aiString path = {};
                                aiTextureMapping* mapping = nullptr;
                                unsigned int* uvindex = nullptr;
                                float* blend = nullptr;
                                aiTextureOp* op = nullptr;
                                aiTextureMapMode* mapMode = nullptr;

                                // Get info on the first texture other one are ignored.
                                aimaterial->GetTexture(textureType, 0, &path,
                                    mapping, uvindex, blend, op, mapMode);

                                // Check if texture is already present
                                std::string imageName(path.C_Str());
                                std::size_t found = imageName.find_last_of("/\\");
                                imageName = imageName.substr(found + 1);
                                Ov::Object& textureFound = container.find(imageName);
                                if (textureFound == Ov::Object::empty)
                                {
                                    OvVK::Texture2D texture;

                                    //Set default texture if maximum number is reached.
                                    if (texture.getLutPos() == OvVK::Texture::maxNrOfTextures)
                                    {
                                        OV_LOG_ERROR("Too many textures: default one used instead in the loader with id= %d", getId());
                                        material.get().setTexture(
                                            OvVK::Texture2D::getDefault(), overvTextureType);
                                    }
                                    else if (this->loadTexture2D(imageName, texture, overvTextureType))
                                    {
                                        /////////////
                                        // Sampler //
                                        /////////////

                                        // Get default sampler
                                        std::reference_wrapper<const OvVK::Sampler> textureSampler =
                                            OvVK::Sampler::getDefault();

                                        // Get all the already created sampler
                                        std::list<OvVK::Sampler>& samplers = container.getSamplerList();
                                        bool foundSuitableSampler = false;

                                        // Check if there exist already a valid sampler
                                        for (std::reference_wrapper<const OvVK::Sampler> sampler : samplers) {
                                            if (sampler.get().getMaxLevelOfDetail() == texture.getImage().getNrOfLevels())
                                            {
                                                if (mapMode != nullptr)
                                                {
                                                    // Translate map mode in vulkan
                                                    VkSamplerAddressMode samplerAddressMode;
                                                    switch (*mapMode)
                                                    {
                                                    case aiTextureMapMode_Wrap:
                                                        samplerAddressMode = VK_SAMPLER_ADDRESS_MODE_REPEAT;
                                                        break;
                                                    case aiTextureMapMode_Clamp:
                                                        samplerAddressMode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
                                                        break;
                                                    case aiTextureMapMode_Decal:
                                                        samplerAddressMode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
                                                        break;
                                                    case aiTextureMapMode_Mirror:
                                                        samplerAddressMode = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
                                                        break;
                                                    default:
                                                        break;
                                                    }

                                                    if (sampler.get().getAddressModeU() == samplerAddressMode &&
                                                        sampler.get().getAddressModeV() == samplerAddressMode &&
                                                        sampler.get().getAddressModeW() == samplerAddressMode)
                                                    {
                                                        foundSuitableSampler = true;
                                                        textureSampler = sampler;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    foundSuitableSampler = true;
                                                    textureSampler = sampler;
                                                    break;
                                                }
                                            }
                                        }

                                        if (foundSuitableSampler == false)
                                        {
                                            // Create sampler
                                            OvVK::Sampler sampler;
                                            this->loadSampler(texture, sampler, mapMode);
                                            container.add(sampler);
                                            textureSampler = dynamic_cast<OvVK::Sampler&>(container.getLastSampler());
                                        }

                                        // Assign sampler to the image
                                        texture.setSampler(textureSampler.get());

                                        // Add texture :
                                        container.add(texture);
                                        material.get().setTexture(
                                            container.getLastTexture2D(), overvTextureType);

                                        // To load with multi thread.
                                        reserved->uploadData.textures.push_back(container.getLastTexture2D());
                                    }
                                    else
                                    {
                                        OV_LOG_ERROR("Fail to load texture with name %s in the loader with id= %d.", imageName, getId());
                                        material.get().setTexture(OvVK::Texture2D::getDefault(), overvTextureType);
                                    }
                                }
                                else
                                {
                                    material.get().setTexture(
                                        dynamic_cast<OvVK::Texture2D&>(textureFound), overvTextureType);
                                }
                            }
                        }
                    }
                }
                // Add to the node
                n.addRenderable(geom, material);
            }
        }
        }

    // Store reference and get new value:
    container.add(n);
    Ov::Node& _n = container.getLastNode();
    reserved->uploadData.nodes.push_back(_n);

    // Iterate through the scenegraph:
    for (uint32_t c = 0; c < _node->mNumChildren; c++)
        _n.addChild(loadScene(container, _node->mChildren[c]));

    if (current == nullptr)
    {
        ///////////////////
        // Upload to GPU //
        ///////////////////

        if (uploadToGpu(container) == false)
            OV_LOG_ERROR("Fail to upload nodes content on the GPU in the loader with id= %d", getId());
    }

    // Done:   
    return _n;
}

#pragma endregion

#pragma region Upload

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Upload all the data that require in the device memory of the GPU.
 * @param container in/out data container
 * @return TF
 */
bool OVVK_API OvVK::Loader::uploadToGpu(OvVK::Container& container)
{
    //////////////////////
    // Geometries setUp //
    //////////////////////

    // Staging Buffer
    OvVK::Buffer geometriesStagingBuffer;


    ///////////////////////////
    // Create staging Buffer //
    ///////////////////////////

    {
        // Create info buffer
        VkBufferCreateInfo vkStagingBufferCreateInfo = {};
        vkStagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        vkStagingBufferCreateInfo.size = reserved->uploadData.geometriesStagingBufferSize;
        // VK_BUFFER_USAGE_TRANSFER_SRC_BIT => specifies that the buffer can be used as the source of a transfer command 
        vkStagingBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        // Set the sharing modo to exclusive allow only one queue family to own the buffer.
        // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
        // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
        vkStagingBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        vkStagingBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
        vkStagingBufferCreateInfo.queueFamilyIndexCount = 0;

        // Create allocation info
        VmaAllocationCreateInfo vmaStagingBufferAllocationCreateInfo = {};
        // VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
        //      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
        // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
        //      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
        //      of allocation time.
        vmaStagingBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
            VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
        // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
        //      mapped for host access using vkMapMemory.
        vmaStagingBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

        if (geometriesStagingBuffer.create(vkStagingBufferCreateInfo, vmaStagingBufferAllocationCreateInfo) == false)
        {
            OV_LOG_ERROR("Fail to create staging buffer for all geometries in loader with id= %d.", getId());
            return false;
        }
    }


    ////////////////////////
    // Multi Thread SetUp //
    ////////////////////////

    std::vector<OvVK::Loader::ThreadData> threadsData(magic_enum::enum_integer(ThreadIdentifier::all));

    {
        ThreadPool threadPool;
        threadPool.setThreadCount(magic_enum::enum_integer(ThreadIdentifier::all));


        threadPool.getThreads()[magic_enum::enum_integer(ThreadIdentifier::geometries)].addJob(
            [&] { uploadGeometries(threadsData[magic_enum::enum_integer(ThreadIdentifier::geometries)],
                reserved->uploadData.geometries, geometriesStagingBuffer); });
        threadPool.getThreads()[magic_enum::enum_integer(ThreadIdentifier::textures)].addJob(
            [&] { uploadTexture(threadsData[magic_enum::enum_integer(ThreadIdentifier::textures)],
                reserved->uploadData.textures); });


        // Process all the object to upload
        threadPool.wait();
    }


    /////////////////////////
    // Commands management //
    /////////////////////////

    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = OvVK::Engine::getInstance();
    // LogicalDevice
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();

    // CommandPools primary
    std::reference_wrapper<OvVK::CommandPool> transferPrimaryCP = engine.get().getPrimaryTransferCmdPool();
    std::reference_wrapper<OvVK::CommandPool> graphicPrimaryCP = engine.get().getPrimaryGraphicCmdPool();

    // Create commandBuffers to send data.
    std::reference_wrapper<OvVK::CommandBuffer> transferPrimaryCB = transferPrimaryCP.get().allocateCommandBuffer();
    std::reference_wrapper<OvVK::CommandBuffer> graphicPrimaryCB = graphicPrimaryCP.get().allocateCommandBuffer();

    VkCommandBufferBeginInfo commandBufferBeginInfo{};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    // Begin CommandBuffers
    bool exit = false;
    if (transferPrimaryCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        transferPrimaryCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or set status of the transfer command buffer to upload geometries data 
from staging buffer to device buffer in loader with id= %d.)", getId());
        exit = true;
    }
    if (graphicPrimaryCB.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
        graphicPrimaryCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin  or set status of the graphic command buffer perform graphic
 operation in loader with id= %d.)", getId());
        exit = true;
    }

    if (exit)
    {
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }


    /////////////
    // VkFence //
    /////////////

    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.pNext = VK_NULL_HANDLE;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    VkFence geometryPrimaryCBFence;
    if(vkCreateFence(logicalDevice.get().getVkDevice(), &fenceCreateInfo, NULL, &geometryPrimaryCBFence) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create fence to synchronise geometries upload in loader with id= %d.", getId());
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }

    VkFence texturePrimaryCBFence;
    if (vkCreateFence(logicalDevice.get().getVkDevice(), &fenceCreateInfo, NULL, &texturePrimaryCBFence) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create fence to synchronise geometries upload in loader with id= %d.", getId());
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }


    ////////////////
    // Geometries //
    ////////////////


#pragma region Geometries


    // Transfer secondary CBs
    std::vector<VkCommandBuffer> geometriesSecondaryCBs;

    // Geometries
    for (std::reference_wrapper<OvVK::CommandBuffer> commandBuffer :
        threadsData[magic_enum::enum_integer(ThreadIdentifier::geometries)].commandBuffers)
        geometriesSecondaryCBs.push_back(commandBuffer.get().getVkCommandBuffer());

    // No need to transfer ownership because first time used.
    // Execute render commands from the secondary command buffer.
    vkCmdExecuteCommands(transferPrimaryCB.get().getVkCommandBuffer(),
        static_cast<uint32_t>(geometriesSecondaryCBs.size()), geometriesSecondaryCBs.data());


    /////////////////////
    // Synchronization //
    /////////////////////

    // Submit
    VkSubmitInfo2 geometriesSubmitInfo = {};
    geometriesSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    geometriesSubmitInfo.pNext = VK_NULL_HANDLE;
    geometriesSubmitInfo.flags = 0x00000000;

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfosGeometries(reserved->uploadData.geometries.size() * 2);
    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfosGeometries(reserved->uploadData.geometries.size() * 2);

    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoGeometries;
    vkSemaphoreSubmitInfoGeometries.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfoGeometries.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfoGeometries.stageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfoGeometries.deviceIndex = 0;

    uint32_t geometriesCounter = 0;
    for (std::reference_wrapper<OvVK::Geometry> geometry : reserved->uploadData.geometries)
    {
        // Set sync lock obj
        geometry.get().setLockSyncObj(geometryPrimaryCBFence);

        // Vertex buffer
        vkSemaphoreSubmitInfoGeometries.semaphore = geometry.get().getVertexBuffer().getStatusSyncObj();
        vkSemaphoreSubmitInfoGeometries.value = OvVK::Geometry::uploadingValueStatusSyncObj;
        pWaitSemaphoreInfosGeometries[geometriesCounter] = vkSemaphoreSubmitInfoGeometries;
        vkSemaphoreSubmitInfoGeometries.value = OvVK::Geometry::readyValueStatusSyncObj;
        pSignalSemaphoreInfosGeometries[geometriesCounter] = vkSemaphoreSubmitInfoGeometries;
        geometriesCounter++;

        // Face buffer
        vkSemaphoreSubmitInfoGeometries.semaphore = geometry.get().getFaceBuffer().getStatusSyncObj();
        vkSemaphoreSubmitInfoGeometries.value = OvVK::Geometry::uploadingValueStatusSyncObj;
        pWaitSemaphoreInfosGeometries[geometriesCounter] = vkSemaphoreSubmitInfoGeometries;
        vkSemaphoreSubmitInfoGeometries.value = OvVK::Geometry::readyValueStatusSyncObj;
        pSignalSemaphoreInfosGeometries[geometriesCounter] = vkSemaphoreSubmitInfoGeometries;
        geometriesCounter++;
    }

    geometriesSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfosGeometries.size();
    geometriesSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfosGeometries.data();
    geometriesSubmitInfo.signalSemaphoreInfoCount = pSignalSemaphoreInfosGeometries.size();
    geometriesSubmitInfo.pSignalSemaphoreInfos = pSignalSemaphoreInfosGeometries.data();

    VkCommandBufferSubmitInfo pCommandBufferInfosGeometries;
    pCommandBufferInfosGeometries.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfosGeometries.pNext = VK_NULL_HANDLE;
    pCommandBufferInfosGeometries.commandBuffer = transferPrimaryCB.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfosGeometries.deviceMask = 0;

    geometriesSubmitInfo.commandBufferInfoCount = 1;
    geometriesSubmitInfo.pCommandBufferInfos = &pCommandBufferInfosGeometries;

#pragma endregion


    //////////////
    // Textures //
    //////////////


#pragma region Textures


    // Transfer secondary CBs
    std::vector<VkCommandBuffer> texturesSecondaryCBs;

    // Textures
    for (std::reference_wrapper<OvVK::CommandBuffer> commandBuffer :
        threadsData[magic_enum::enum_integer(ThreadIdentifier::textures)].commandBuffers)
        texturesSecondaryCBs.push_back(commandBuffer.get().getVkCommandBuffer());

    // No need to transfer ownership because first time used.
    // Execute render commands from the secondary command buffer.
    vkCmdExecuteCommands(graphicPrimaryCB.get().getVkCommandBuffer(),
        static_cast<uint32_t>(texturesSecondaryCBs.size()), texturesSecondaryCBs.data());


    /////////////////////
    // Synchronization //
    /////////////////////

    // Submit
    VkSubmitInfo2 texturesSubmitInfo = {};
    texturesSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    texturesSubmitInfo.pNext = VK_NULL_HANDLE;
    texturesSubmitInfo.flags = 0x00000000;

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfosTextures(reserved->uploadData.textures.size() * 2);
    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfosTextures(reserved->uploadData.textures.size() * 2);

    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfoTextures;
    vkSemaphoreSubmitInfoTextures.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfoTextures.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfoTextures.stageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfoTextures.deviceIndex = 0;

    uint32_t texturesCounter = 0;
    for (std::reference_wrapper<OvVK::Texture> texture : reserved->uploadData.textures)
    {
        // Set sync lock obj
        texture.get().setLockSyncObj(geometryPrimaryCBFence);

        // Texture
        vkSemaphoreSubmitInfoTextures.semaphore = texture.get().getImage().getStatusSyncObj();
        vkSemaphoreSubmitInfoTextures.value = OvVK::Texture::uploadingValueStatusSyncObj;
        pWaitSemaphoreInfosTextures[texturesCounter] = vkSemaphoreSubmitInfoTextures;
        vkSemaphoreSubmitInfoTextures.value = OvVK::Texture::readyValueStatusSyncObj;
        pSignalSemaphoreInfosTextures[texturesCounter] = vkSemaphoreSubmitInfoTextures;
        texturesCounter++;

        // Bitmap buffer
        vkSemaphoreSubmitInfoTextures.semaphore = texture.get().getBitmapBuffer().getStatusSyncObj();
        vkSemaphoreSubmitInfoTextures.value = OvVK::Texture::uploadingValueStatusSyncObj;
        pWaitSemaphoreInfosTextures[texturesCounter] = vkSemaphoreSubmitInfoTextures;
        vkSemaphoreSubmitInfoTextures.value = OvVK::Texture::readyValueStatusSyncObj;
        pSignalSemaphoreInfosTextures[texturesCounter] = vkSemaphoreSubmitInfoTextures;
        texturesCounter++;
    }

    texturesSubmitInfo.waitSemaphoreInfoCount = pWaitSemaphoreInfosTextures.size();
    texturesSubmitInfo.pWaitSemaphoreInfos = pWaitSemaphoreInfosTextures.data();
    texturesSubmitInfo.signalSemaphoreInfoCount = pSignalSemaphoreInfosTextures.size();
    texturesSubmitInfo.pSignalSemaphoreInfos = pSignalSemaphoreInfosTextures.data();

    VkCommandBufferSubmitInfo pCommandBufferInfosTextures;
    pCommandBufferInfosTextures.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfosTextures.pNext = VK_NULL_HANDLE;
    pCommandBufferInfosTextures.commandBuffer = graphicPrimaryCB.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfosTextures.deviceMask = 0;

    texturesSubmitInfo.commandBufferInfoCount = 1;
    texturesSubmitInfo.pCommandBufferInfos = &pCommandBufferInfosTextures;

#pragma endregion


    // End primary commandBuffers
    if (transferPrimaryCB.get().endVkCommandBuffer() == false ||
        transferPrimaryCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or set status of the transfer command buffer to upload geometries data from
 staging buffer to device buffer in loader with id= %d.)", getId());
        exit = true;
    }
    if (graphicPrimaryCB.get().endVkCommandBuffer() == false ||
        graphicPrimaryCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to end or set the status of the graphic command buffer to
 perform graphic operation in loader with id= %d.)", getId());
        exit = true;
    }

    if (exit)
    {
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }


    /////////////////////////////////
    // Set SyncObj to check status //
    /////////////////////////////////

    // Geometries
    for (std::reference_wrapper<OvVK::Geometry> geometry : reserved->uploadData.geometries)
        geometry.get().setStatusSyncObjValue(OvVK::Geometry::uploadingValueStatusSyncObj);

    // Textures
    for (std::reference_wrapper<OvVK::Texture> texture : reserved->uploadData.textures)
        texture.get().setStatusSyncObjValue(OvVK::Texture::uploadingValueStatusSyncObj);

    vkResetFences(logicalDevice.get().getVkDevice(), 1, &geometryPrimaryCBFence);
    vkResetFences(logicalDevice.get().getVkDevice(), 1, &texturePrimaryCBFence);


    ////////////
    // Submit //
    ////////////

    // Transfer commandBuffers
    if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(transferPrimaryCP.get().getQueueFamilyIndex().value()),
        1, &geometriesSubmitInfo, geometryPrimaryCBFence) != VK_SUCCESS)
    {
        OV_LOG_INFO("Fail to submit transfer primary command buffer in loader with id= %d.", getId());
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }

    // Graphic commandBuffers
    if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(graphicPrimaryCP.get().getQueueFamilyIndex().value()),
        1, &texturesSubmitInfo, texturePrimaryCBFence) != VK_SUCCESS)
    {
        OV_LOG_INFO("Fail to submit graphic primary command buffer in loader with id= %d.", getId());
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }

    // Wait fences to know the work is finished
    VkFence fenceToWait[2] = { geometryPrimaryCBFence, texturePrimaryCBFence };
    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 2, fenceToWait, VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait on the upload command buffers in loader with id= %d.", getId());
        transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
        graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());
        return false;
    }

    // Post process configuration
    for (std::reference_wrapper<OvVK::Geometry> geometry : reserved->uploadData.geometries)
    {
        geometry.get().setQueueFamilyIndex(transferPrimaryCP.get().getQueueFamilyIndex().value());
        geometry.get().setLockSyncObj(VK_NULL_HANDLE);
    }
    for (std::reference_wrapper<OvVK::Texture> texture : reserved->uploadData.textures)
    {
        texture.get().setQueueFamilyIndex(graphicPrimaryCP.get().getQueueFamilyIndex().value());
        texture.get().freeBitmapBuffer();
        texture.get().setLockSyncObj(VK_NULL_HANDLE);
        OvVK::TexturesDescriptorSetsManager::getInstance().addToUpdateList(texture);
    }

    // Remove commandBuffer
    transferPrimaryCP.get().freeCommandBuffer(transferPrimaryCB.get());
    graphicPrimaryCP.get().freeCommandBuffer(graphicPrimaryCB.get());

    // Clear commandPool
    for (uint32_t c = 0; c < threadsData.size(); c++)
        threadsData[c].commandPool.reset(VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);

    // Destroy Fence
    vkDestroyFence(logicalDevice.get().getVkDevice(), geometryPrimaryCBFence, NULL);
    vkDestroyFence(logicalDevice.get().getVkDevice(), texturePrimaryCBFence, NULL);

    // Done:
    return true;
}

#pragma endregion

#pragma region SyncData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create command buffers to copy geometries data from the GPU host memory to the GPU device memory.
 * @param threadData Tread data needed to crete command buffers.
 * @param geometries List of geometries reference to load data into GPU device memory.
 * @param geometriesStagingBuffer The staging buffer containing the geometries data.
 * @return TF
 */
bool OVVK_API OvVK::Loader::uploadGeometries(OvVK::Loader::ThreadData& threadData,
    std::list<std::reference_wrapper<OvVK::Geometry>>& geometries,
    OvVK::Buffer& geometriesStagingBuffer)
{
    // Create command pool
    if (threadData.commandPool.create(OvVK::CommandPool::CommandTypesFlagBits::Transfer) == false) 
    {
        OV_LOG_ERROR("Fail to create geometry transfer command pool for thread.");
        return false;
    }

    // Structure defines any state that will be inherited from the primary command buffer
    VkCommandBufferInheritanceInfo commandBufferInheritanceInfo = {};
    commandBufferInheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    commandBufferInheritanceInfo.pNext = NULL;
    commandBufferInheritanceInfo.renderPass = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.subpass = 0;
    commandBufferInheritanceInfo.framebuffer = VK_NULL_HANDLE;
    // occlusionQueryEnable specifies whether the command buffer can be executed while an occlusion query
    // is active in the primary command buffer. If this is VK_TRUE, then this command buffer can be executed
    // whether the primary command buffer has an occlusion query active or not. If this is VK_FALSE, 
    // then the primary command buffer must not have an occlusion query active.
    // Occlusion queries are only available on queue families supporting graphics operations.
    commandBufferInheritanceInfo.occlusionQueryEnable = VK_FALSE;
    commandBufferInheritanceInfo.queryFlags = 0x00000000;
    commandBufferInheritanceInfo.pipelineStatistics = 0x00000000;

    // Begin CommandBuffer info
    VkCommandBufferBeginInfo commandBufferBeginInfo = {};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    commandBufferBeginInfo.pInheritanceInfo = &commandBufferInheritanceInfo;

    // pointer to staging buffer memeory
    uint8_t* currentPosition = ((uint8_t*)geometriesStagingBuffer.getVmaAllocationInfo().pMappedData);
    // Offset to ready from staging buffer
    uint64_t offset = 0;
    // Copy info
    VkBufferCopy bufferCopy = {};
    bufferCopy.dstOffset = 0;

    // Record commandBuffer for all the geometries
    for (std::reference_wrapper<OvVK::Geometry> geometry : geometries)
    {
        // Allocate commandBuffer
		std::reference_wrapper<OvVK::CommandBuffer> transferCommandBuffer =
            threadData.commandPool.allocateCommandBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY);

        if (transferCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
            transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to begin or change status of the secondary command buffer to transfer geometry with name = %s and id = %d.",
                geometry.get().getName(), geometry.get().getId());
            geometry.get().free();
            threadData.commandPool.freeCommandBuffer(transferCommandBuffer.get());
            return false;
        }

        // Get commandBuffer
        VkCommandBuffer vkCommandBuffer = transferCommandBuffer.get().getVkCommandBuffer();

        // Calculate size of the vertex buffer in bytes
        uint64_t vertexSize = geometry.get().getNrOfVertices() * sizeof(Ov::Geometry::VertexData);
        // Calculate size of the face buffer in bytes
        uint64_t faceSize = geometry.get().getNrOfFaces() * sizeof(Ov::Geometry::FaceData);

        // Copy data on staging buffer
        memcpy(currentPosition, geometry.get().getVertexDataPtr(), (size_t)vertexSize);
        // Get pointer to the memory where copy the data
        currentPosition += vertexSize;
        // Copy data on staging buffer
        memcpy(currentPosition, geometry.get().getFaceDataPtr(), (size_t)faceSize);
        // Get pointer to the memory where copy the data
        currentPosition += faceSize;

        // Register copy Command
        bufferCopy.srcOffset = offset;
        bufferCopy.size = vertexSize;
        vkCmdCopyBuffer(vkCommandBuffer, geometriesStagingBuffer.getVkBuffer(),
            geometry.get().getVertexBuffer().getVkBuffer(), 1, &bufferCopy);
        offset += vertexSize;

        bufferCopy.srcOffset = offset;
        bufferCopy.size = faceSize;
        vkCmdCopyBuffer(vkCommandBuffer, geometriesStagingBuffer.getVkBuffer(),
            geometry.get().getFaceBuffer().getVkBuffer(), 1, &bufferCopy);
        offset += faceSize;


        //End command buffer
        if (transferCommandBuffer.get().endVkCommandBuffer() == false ||
            transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to end or change status of the secondary command buffer to upload geometry with name = %s and id = %d.",
                geometry.get().getName(), geometry.get().getId());
            geometry.get().free();
            threadData.commandPool.freeCommandBuffer(transferCommandBuffer.get());
            continue;
        }

        threadData.commandBuffers.push_back(transferCommandBuffer.get());
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create command buffers to copy textures data from the bitmap buffer to the image.
 * @param threadData Tread data needed to crete command buffers.
 * @param textures List of textures reference to load data into images.
 * @return TF
 */
bool OVVK_API OvVK::Loader::uploadTexture(OvVK::Loader::ThreadData& threadData,
    std::list<std::reference_wrapper<OvVK::Texture>>& textures)
{
    // Create command pool. Use graphic because if the texture need
    // mipmap generation the code will use blit operation.
    if (threadData.commandPool.create(OvVK::CommandPool::CommandTypesFlagBits::Graphic) == false)
    {
        OV_LOG_ERROR("Fail to create texture copy command pool for thread.");
        return false;
    }

    // Structure defines any state that will be inherited from the primary command buffer
    VkCommandBufferInheritanceInfo commandBufferInheritanceInfo = {};
    commandBufferInheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    commandBufferInheritanceInfo.pNext = NULL;
    commandBufferInheritanceInfo.renderPass = VK_NULL_HANDLE;
    commandBufferInheritanceInfo.subpass = 0;
    commandBufferInheritanceInfo.framebuffer = VK_NULL_HANDLE;
    // occlusionQueryEnable specifies whether the command buffer can be executed while an occlusion query
    // is active in the primary command buffer. If this is VK_TRUE, then this command buffer can be executed
    // whether the primary command buffer has an occlusion query active or not. If this is VK_FALSE, 
    // then the primary command buffer must not have an occlusion query active.
    // Occlusion queries are only available on queue families supporting graphics operations.
    commandBufferInheritanceInfo.occlusionQueryEnable = VK_FALSE;
    commandBufferInheritanceInfo.queryFlags = 0x00000000;
    commandBufferInheritanceInfo.pipelineStatistics = 0x00000000;

    // Begin CommandBuffer
    VkCommandBufferBeginInfo commandBufferBeginInfo = {};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    commandBufferBeginInfo.pInheritanceInfo = &commandBufferInheritanceInfo;

    // Record commandBuffer for all the textures
    for (std::reference_wrapper<OvVK::Texture> texture : textures)
    {
        // Allocate commandBuffer
        std::reference_wrapper<OvVK::CommandBuffer> copyCommandBuffer =
            threadData.commandPool.allocateCommandBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY);

        if (copyCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
            copyCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to begin or change status of the secondary command buffer to copy texture with name = %s and id = %d.",
                texture.get().getName(), texture.get().getId());
            texture.get().free();
            threadData.commandPool.freeCommandBuffer(copyCommandBuffer.get());
            return false;
        }

        // Get commandBuffer
        VkCommandBuffer vkCommandBuffer = copyCommandBuffer.get().getVkCommandBuffer();

        // Get texture image
        std::reference_wrapper<const OvVK::Image> textureImage = texture.get().getImage();

#pragma region TextureUpload

        // Change layout of the image to transfer to
        // Barrier set up
        VkImageMemoryBarrier2 changeLayoutBarrier = {};
        changeLayoutBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        changeLayoutBarrier.pNext = VK_NULL_HANDLE;
        // Wait previous stage for changing the texture layout
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
        changeLayoutBarrier.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
        // Wait on specific memory access.
        changeLayoutBarrier.srcAccessMask = VK_ACCESS_2_NONE;
        // All transfer commands need to wait that the texture change layout.
        changeLayoutBarrier.dstStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
        // All transfer commands need to wait that the texture change layout.
        changeLayoutBarrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
        // Current texture layout
        changeLayoutBarrier.oldLayout = textureImage.get().getImageLayout();
        // Set the layout that allow a fresh write to the texture.
        changeLayoutBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        // No need to transfer ownership because first time used.
        changeLayoutBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        changeLayoutBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        // The image and subresourceRange specify the image that is affected and the specific part of the image.
        changeLayoutBarrier.image = textureImage.get().getVkImage();
        changeLayoutBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        // baseMipLevel is the first mipmap level accessible to the view.
        changeLayoutBarrier.subresourceRange.baseMipLevel = 0;
        changeLayoutBarrier.subresourceRange.levelCount = textureImage.get().getNrOfLevels();
        changeLayoutBarrier.subresourceRange.baseArrayLayer = 0;
        changeLayoutBarrier.subresourceRange.layerCount = textureImage.get().getNrOfLayers();

        // Barrier specification
        VkDependencyInfo changeLayoutDependencyInfo = {};
        changeLayoutDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
        changeLayoutDependencyInfo.dependencyFlags = 0x00000000;
        changeLayoutDependencyInfo.memoryBarrierCount = 0;
        changeLayoutDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
        changeLayoutDependencyInfo.bufferMemoryBarrierCount = 0;
        changeLayoutDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
        changeLayoutDependencyInfo.imageMemoryBarrierCount = 1;
        changeLayoutDependencyInfo.pImageMemoryBarriers = &changeLayoutBarrier;

        // Register command for transition.
        vkCmdPipelineBarrier2(vkCommandBuffer, &changeLayoutDependencyInfo);


        // Register command
        vkCmdCopyBufferToImage(
            vkCommandBuffer,
            texture.get().getBitmapBuffer().getVkBuffer(),
            textureImage.get().getVkImage(),
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            texture.get().getBufferImageCopyRegions().size(),
            texture.get().getBufferImageCopyRegions().data()
        );

#pragma endregion

#pragma region GenerateMipmap

        // Loaded
        if (texture.get().getNeedGenerateMipmap())
        {
            /////////////////////
            // Generate Mipmap //
            /////////////////////

            // if greater than 1 need to generate mipmap
            if (textureImage.get().getNrOfLevels() > 1)
            {
                //Make several transitions, so we'll reuse this VkImageMemoryBarrier.
                // Barrier set up
                VkImageMemoryBarrier2 mipmapBarrier = {};
                mipmapBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
                // pNext is NULL or a pointer to a structure extending this structure.
                mipmapBarrier.pNext = VK_NULL_HANDLE;
                // No need to transfer ownership because first time used.
                mipmapBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                mipmapBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                // The image and subresourceRange specify the image that is affected and the specific part of the image.
                mipmapBarrier.image = textureImage.get().getVkImage();
                mipmapBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                // baseMipLevel is the first mipmap level accessible to the view.
                mipmapBarrier.subresourceRange.baseMipLevel = 0;
                mipmapBarrier.subresourceRange.levelCount = 1;
                mipmapBarrier.subresourceRange.baseArrayLayer = 0;
                mipmapBarrier.subresourceRange.layerCount = 1;

                // Barrier specification
                VkDependencyInfo mipmapDependencyInfo = {};
                mipmapDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
                mipmapDependencyInfo.dependencyFlags = 0x00000000;
                mipmapDependencyInfo.memoryBarrierCount = 0;
                mipmapDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
                mipmapDependencyInfo.bufferMemoryBarrierCount = 0;
                mipmapDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
                mipmapDependencyInfo.imageMemoryBarrierCount = 1;
                mipmapDependencyInfo.pImageMemoryBarriers = &mipmapBarrier;

                int32_t mipWidth = texture.get().getSizeX();
                int32_t mipHeight = texture.get().getSizeY();


                for (uint32_t c = 0; c < textureImage.get().getNrOfLayers(); c++) {
                    // The fields set above will remain the same for all barriers. subresourceRange.miplevel,
                    // oldLayout, newLayout, srcAccessMask, and dstAccessMask will be changed for each transition.
                    for (uint32_t i = 1; i < textureImage.get().getNrOfLevels(); i++) {

                        // Vulkan allows us to transition each mip level of an image independently. Each blit will only deal
                        // with two mip levels at a time, so we can transition each level into the optimal layout between blits commands.
                        // Wait previous stage for changing the texture layout
                        mipmapBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                        // Wait on specific memory access.
                        mipmapBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
                        // All blit commands need to wait that the texture change layout.
                        mipmapBarrier.dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                        // All blit commands need to wait that the texture change layout.
                        mipmapBarrier.dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT;
                        // Current texture layout
                        mipmapBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                        // Set the layout that allow a fresh write to the texture.
                        mipmapBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                        // baseMipLevel is the first mipmap level accessible to the view.
                        mipmapBarrier.subresourceRange.baseMipLevel = i - 1;
                        // baseMipLevel is the first layer accessible to the view.
                        mipmapBarrier.subresourceRange.baseArrayLayer = c;

                        // First, we transition level i - 1 to VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL. This transition will
                        // wait for level i - 1 to be filled, either from the previous blit command, or from vkCmdCopyBufferToImage.
                        // Register command for transition.
                        vkCmdPipelineBarrier2(vkCommandBuffer, &mipmapDependencyInfo);


                        // Next, we specify the regions that will be used in the blit operation.
                        // The source mip level is i - 1 and the destination mip level is i. 
                        // The Z dimension of srcOffsets[1] and dstOffsets[1] must be 1, since a 2D image has a depth of 1.
                        VkImageBlit2 blit = {};
                        blit.sType = VK_STRUCTURE_TYPE_IMAGE_BLIT_2;
                        blit.pNext = NULL;
                        // The two elements of the srcOffsets array determine the 3D region that data will be blitted from.
                        blit.srcOffsets[0] = { 0, 0, 0 };
                        blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
                        blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                        // The source mip level is i - 1
                        blit.srcSubresource.mipLevel = i - 1;
                        blit.srcSubresource.baseArrayLayer = c;
                        blit.srcSubresource.layerCount = 1;

                        // dstOffsets determines the region that data will be blitted to.
                        blit.dstOffsets[0] = { 0, 0, 0 };
                        // The X and Y dimensions of the dstOffsets[1] are divided by two since each mip level is half the size of the previous level.
                        blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
                        blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                        // the destination mip level is i. 
                        blit.dstSubresource.mipLevel = i;
                        blit.dstSubresource.baseArrayLayer = c;
                        blit.dstSubresource.layerCount = 1;

                        // Note that textureImage is used for both the srcImageand dstImage parameter.This is because we're blitting
                        // between different levels of the same image. The source mip level was just transitioned to
                        // VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL and the destination level is still in VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL from createTextureImage.
                        // vkCmdBlitImage must be submitted to a queue with graphics capability.
                        // The last parameter allows us to specify a VkFilter to use in the blit. We have the same 
                        // filtering options here that we had when making the VkSampler. We use the VK_FILTER_LINEAR to enable interpolation.
                        VkBlitImageInfo2 blitImageInfo2 = {};
                        blitImageInfo2.sType = VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2;
                        blitImageInfo2.pNext = NULL;
                        blitImageInfo2.srcImage = textureImage.get().getVkImage();
                        blitImageInfo2.srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                        blitImageInfo2.dstImage = textureImage.get().getVkImage();
                        blitImageInfo2.dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                        blitImageInfo2.regionCount = 1;
                        blitImageInfo2.pRegions = &blit;
                        blitImageInfo2.filter = VK_FILTER_LINEAR;

                        vkCmdBlitImage2(vkCommandBuffer, &blitImageInfo2);

                        // Wait previous stage for changing the texture layout
                        mipmapBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                        // Wait on specific memory access.
                        mipmapBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT;
                        // All commands need to wait that the texture change layout.
                        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
                        mipmapBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                        // All commands need to wait that the texture change layout.
                        mipmapBarrier.dstAccessMask = VK_ACCESS_2_NONE;
                        // Current texture layout
                        mipmapBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                        // Set the layout that allow a fresh write to the texture.
                        mipmapBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

                        // This transition waits on the current blit command to finish.
                        vkCmdPipelineBarrier2(vkCommandBuffer, &mipmapDependencyInfo);

                        // At the end of the loop, we divide the current mip dimensions by two. We check each dimension before
                        // the division to ensure that dimension never becomes 0. This handles cases where the image is not square,
                        // since one of the mip dimensions would reach 1 before the other dimension. When this happens, 
                        // that dimension should remain 1 for all remaining levels.
                        if (mipWidth > 1) mipWidth /= 2;
                        if (mipHeight > 1) mipHeight /= 2;
                    }

                    // This barrier transitions the last mip level from VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL 
                    // to VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL. This wasn't handled by the loop, 
                    // since the last mip level is never blitted from.
                    // Wait previous stage for changing the texture layout
                    mipmapBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                    // Wait on specific memory access.
                    mipmapBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
                    // All commands need to wait that the texture change layout.
                    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
                    mipmapBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                    // All commands need to wait that the texture change layout.
                    mipmapBarrier.dstAccessMask = VK_ACCESS_2_NONE;
                    // Current texture layout
                    mipmapBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                    // Set the layout that allow a fresh write to the texture.
                    mipmapBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                    // baseMipLevel is the first mipmap level accessible to the view.
                    mipmapBarrier.subresourceRange.baseMipLevel = textureImage.get().getNrOfLevels() - 1;
                    // baseMipLevel is the first layer accessible to the view.
                    mipmapBarrier.subresourceRange.baseArrayLayer = c;

                    // This transition waits on the current blit command to finish.
                    vkCmdPipelineBarrier2(vkCommandBuffer, &mipmapDependencyInfo);
                }
            }
        }
        else
        {
            // Change layout of the image to read from
            // Wait previous stage for changing the texture layout
            changeLayoutBarrier.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
            // Wait on specific memory access.
            changeLayoutBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
            // All commands need to wait that the texture change layout.
            // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
            changeLayoutBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
            // All commands need to wait that the texture change layout.
            changeLayoutBarrier.dstAccessMask = VK_ACCESS_2_NONE;
            // Current texture layout
            changeLayoutBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            // Set the layout that allow a fresh write to the texture.
            changeLayoutBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

            // Register command for transition.
            vkCmdPipelineBarrier2(vkCommandBuffer, &changeLayoutDependencyInfo);
        }

#pragma endregion

        // End command buffer
        if (copyCommandBuffer.get().endVkCommandBuffer() == false ||
            copyCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to end or change status of the secondary command buffer to copy texture with name = %s and id = %d.",
                texture.get().getName(), texture.get().getId());
            texture.get().free();
            threadData.commandPool.freeCommandBuffer(copyCommandBuffer.get());
            continue;
        }

        threadData.commandBuffers.push_back(copyCommandBuffer.get());
        texture.get().setImageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Debug

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Print on the console the Assimp post processing flags contained in the passed value.
 * @param pFlags flags for post processing.
 */
void OVVK_API OvVK::Loader::showParamsUsed(unsigned int pFlags) {
    if (pFlags & aiProcess_CalcTangentSpace)
        std::cout << "aiProcess_CalcTangentSpace" << std::endl;
    if (pFlags & aiProcess_FindInvalidData)
        std::cout << "aiProcess_FindInvalidData" << std::endl;
    if (pFlags & aiProcess_FixInfacingNormals)
        std::cout << "aiProcess_FixInfacingNormals" << std::endl;
    if (pFlags & aiProcess_GenSmoothNormals)
        std::cout << "aiProcess_GenSmoothNormals" << std::endl;
    if (pFlags & aiProcess_JoinIdenticalVertices)
        std::cout << "aiProcess_JoinIdenticalVertices" << std::endl;
    if (pFlags & aiProcess_SortByPType)
        std::cout << "aiProcess_SortByPType" << std::endl;
    if (pFlags & aiProcess_Triangulate)
        std::cout << "aiProcess_Triangulate" << std::endl;
    if (pFlags & aiProcess_ValidateDataStructure)
        std::cout << "aiProcess_ValidateDataStructure" << std::endl;
    if (pFlags & aiProcess_MakeLeftHanded)
        std::cout << "aiProcess_MakeLeftHanded" << std::endl;
    if (pFlags & aiProcess_FlipWindingOrder)
        std::cout << "aiProcess_FlipWindingOrder" << std::endl;
    if (pFlags & aiProcess_FlipUVs)
        std::cout << "aiProcess_FlipUVs" << std::endl;
    if (pFlags & aiProcess_PreTransformVertices)
        std::cout << "aiProcess_PreTransformVertices" << std::endl;
    if (pFlags & aiProcess_OptimizeMeshes)
        std::cout << "aiProcess_OptimizeMeshes" << std::endl;
}

#pragma endregion