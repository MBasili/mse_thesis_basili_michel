/**
 * @file	overv_vk_dsm.cpp
 * @brief	Vulkan descriptor set
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::DescriptorSetsManager OvVK::DescriptorSetsManager::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief DescriptorSet reserved structure.
 */
struct OvVK::DescriptorSetsManager::Reserved
{
    VkDescriptorPool descriptorPool;                ///< Vulkan descriptor pool handle
    VkDescriptorSetLayout descriptorSetLayout;      ///< Vulkan descriptor set layout handle

    std::vector<VkDescriptorSet> descriptorSets;    ///< Vulkan descriptor set handle

    /**
     * Constructor.
     */
    Reserved() : descriptorPool{ VK_NULL_HANDLE },
        descriptorSetLayout{ VK_NULL_HANDLE }
    {
        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
            descriptorSets.push_back(VK_NULL_HANDLE);
    }
};


/////////////////////////////////
// BODY OF CLASS DescriptorSet //
/////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::DescriptorSetsManager::DescriptorSetsManager() :
    reserved(std::make_unique<OvVK::DescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::DescriptorSetsManager::DescriptorSetsManager(const std::string& name) : Ov::Renderable(name),
    reserved(std::make_unique<OvVK::DescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::DescriptorSetsManager::DescriptorSetsManager(DescriptorSetsManager&& other) : Ov::Renderable(std::move(other)),
    Ov::Managed(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::DescriptorSetsManager::~DescriptorSetsManager()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan descriptor set.
 * @return TF
 */
bool OVVK_API OvVK::DescriptorSetsManager::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Retrive logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Free descriptor pool and destroy descriptor set layout and set
    if (reserved->descriptorPool != VK_NULL_HANDLE)
    {
        vkDestroyDescriptorPool(logicalDevice, reserved->descriptorPool, nullptr);
        reserved->descriptorPool = VK_NULL_HANDLE;

        for(uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
            reserved->descriptorSets[c] = VK_NULL_HANDLE;
    }

    // Free descriptor set layout
    if (reserved->descriptorSetLayout != VK_NULL_HANDLE)
    {
        vkDestroyDescriptorSetLayout(logicalDevice, reserved->descriptorSetLayout, nullptr);
        reserved->descriptorSetLayout = VK_NULL_HANDLE;
    }


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan descriptor set.
 * @return TF
 */
bool OVVK_API OvVK::DescriptorSetsManager::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Retrive logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Free descriptor pool and destroy descriptor set layout and set
    if (reserved->descriptorPool != VK_NULL_HANDLE)
    {
        vkDestroyDescriptorPool(logicalDevice, reserved->descriptorPool, nullptr);
        reserved->descriptorPool = VK_NULL_HANDLE;

        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
            reserved->descriptorSets[c] = VK_NULL_HANDLE;
    }

    // Free descriptor set layout
    if (reserved->descriptorSetLayout != VK_NULL_HANDLE)
    {
        vkDestroyDescriptorSetLayout(logicalDevice, reserved->descriptorSetLayout, nullptr);
        reserved->descriptorSetLayout = VK_NULL_HANDLE;
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets and descriptor sets layouts.
 * @return TF
 */
bool OVVK_API OvVK::DescriptorSetsManager::create()
{
    OV_LOG_ERROR("Called the setUp method of base class DescriptorSetManager in the DSM with id= %d.", getId());
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets layouts and descriptor sets.
 * @param descriptorPoolSizes A vector containig the structure specifying descriptor pool size.
 * @param descriptorSetLayoutBindings A vector containig the structures specifying a descriptor set layout binding
 * @param nrDescriptorSets The number of descriptor sets to create.
 * @return TF
 */
bool OVVK_API OvVK::DescriptorSetsManager::create(std::vector<VkDescriptorPoolSize>& descriptorPoolSizes,
    std::vector<VkDescriptorSetLayoutBinding>& descriptorSetLayoutBindings, uint32_t nrDescriptorSets)
{
    // Safety net:
    if (descriptorPoolSizes.size() == 0) 
    {
        OV_LOG_ERROR("The passed vector descriptorsPoolSize is empty in DSM with id= %d.", getId());
        return false;
    }
    if (descriptorSetLayoutBindings.size() == 0)
    {
        OV_LOG_ERROR("The passed vector descriptorSetLayoutBindings is empty in DSM with id= %d.", getId());
        return false;
    }
    if (nrDescriptorSets == 0)
    {
        OV_LOG_ERROR("The nrMaxSets passed is 0 in DSM with id= %d.", getId());
        return false;
    }

    // Engine
    std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();


   /////////////////////
   // Descriptor Pool //
   /////////////////////

   // Create descriptorPool
    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
    descriptorPoolCreateInfo.poolSizeCount = static_cast<uint32_t>(descriptorPoolSizes.size());
    descriptorPoolCreateInfo.pPoolSizes = descriptorPoolSizes.data();
    descriptorPoolCreateInfo.maxSets = nrDescriptorSets;

    if (vkCreateDescriptorPool(logicalDevice.get().getVkDevice(), &descriptorPoolCreateInfo, NULL,
        &reserved->descriptorPool) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create descriptorPool in DSM with id= %d.", getId());
        this->free();
        return false;
    }


    ///////////////////////////
    // Descriptor set layout //
    ///////////////////////////

    std::vector<VkDescriptorBindingFlags> descriptorBindingFlags(descriptorSetLayoutBindings.size());
    for (std::reference_wrapper<VkDescriptorBindingFlags> flag : descriptorBindingFlags)
        flag.get() = VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT;

    VkDescriptorSetLayoutBindingFlagsCreateInfo descriptorSetLayoutBindingFlagsCreateInfo = {};
    descriptorSetLayoutBindingFlagsCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
    descriptorSetLayoutBindingFlagsCreateInfo.pNext = NULL;
    descriptorSetLayoutBindingFlagsCreateInfo.bindingCount = static_cast<uint32_t>(descriptorBindingFlags.size());
    descriptorSetLayoutBindingFlagsCreateInfo.pBindingFlags = descriptorBindingFlags.data();


    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
    descriptorSetLayoutCreateInfo.pNext = &descriptorSetLayoutBindingFlagsCreateInfo;
    descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(descriptorSetLayoutBindings.size());
    descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

    if (vkCreateDescriptorSetLayout(logicalDevice.get().getVkDevice(), &descriptorSetLayoutCreateInfo, NULL,
        &reserved->descriptorSetLayout) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create descriptorSetLayout in DSM with id= %d.", getId());
        this->free();
        return false;
    }


    //////////////////
    // Allocate set //
    //////////////////

    // Allocate descriptor set
    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.pNext = NULL;
    descriptorSetAllocateInfo.descriptorPool = reserved->descriptorPool;
    descriptorSetAllocateInfo.descriptorSetCount = nrDescriptorSets;

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts(nrDescriptorSets, reserved->descriptorSetLayout);
    descriptorSetAllocateInfo.pSetLayouts = descriptorSetLayouts.data();

    // reset sets
    reserved->descriptorSets.clear();
    reserved->descriptorSets.resize(nrDescriptorSets, VK_NULL_HANDLE);

    if (vkAllocateDescriptorSets(logicalDevice.get().getVkDevice(), &descriptorSetAllocateInfo,
        reserved->descriptorSets.data()) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to allocate descriptorSets in DSM with id= %d.", getId());
        this->free();
        return false;
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the descriptor pool Vulkan handle.
 * @return The descriptor pool Vulkan handle.
 */
VkDescriptorPool OVVK_API OvVK::DescriptorSetsManager::getVkDescriptorPool() const
{
    return reserved->descriptorPool;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the descriptor set layout Vulkan handle.
 * @return The descriptor set layout Vulkan handle.
 */
VkDescriptorSetLayout OVVK_API OvVK::DescriptorSetsManager::getVkDescriptorSetLayout() const
{
    return reserved->descriptorSetLayout;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the descriptor set Vulkan handle.
 * @return The descriptor set Vulkan handle.
 */
VkDescriptorSet OVVK_API OvVK::DescriptorSetsManager::getVkDescriptorSet(uint32_t idDescriptorSet) const
{
    // Safety net:
    if (idDescriptorSet >= reserved->descriptorSets.size()) 
    {
        OV_LOG_ERROR("The passed descriptorSet index is out of range in DSM with id= %d.", getId());
        return VK_NULL_HANDLE;
    }

    return reserved->descriptorSets[idDescriptorSet];
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::DescriptorSetsManager::render(uint32_t value, void* data) const
{
    OV_LOG_ERROR("DescriptorSetsManager empty rendering method called in DSM with id= %d", getId());
    return false;
}

#pragma endregion