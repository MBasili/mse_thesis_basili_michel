/**
 * @file	overv_vk_surface_dependent_obj.h
 * @brief	Vulkan surface dependent object properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

#pragma once
/**
  * @brief Class used to forward the call to the callback functions to object that depends on a surface.
  */
class OVVK_API SurfaceDependentObject
{
//////////
public: //
//////////

	// Special values;
	static SurfaceDependentObject empty;

	// Const/dest:
	SurfaceDependentObject();
	SurfaceDependentObject(SurfaceDependentObject&& other);
	SurfaceDependentObject(SurfaceDependentObject const&) = delete;
	virtual ~SurfaceDependentObject();

	// Operators
	SurfaceDependentObject& operator=(const SurfaceDependentObject&) = delete;
	SurfaceDependentObject& operator=(SurfaceDependentObject&&) = delete;

	// SurfaceCallbackTypes:
	virtual bool keyboardCallback(int key, int scancode, int action, int mods);
	virtual bool mouseCursorCallback(double mouseX, double mouseY);
	virtual bool mouseButtonCallback(int button, int action, int mods);
	virtual bool mouseScrollCallback(double scrollX, double scrollY);
	virtual bool windowSizeCallback(int _width, int _height);

	// Get/set
	OvVK::Surface::SurfaceCallbackTypes getActiveCallback() const;
	bool getIsActive() const;
	void setActiveCallback(OvVK::Surface::SurfaceCallbackTypes activeCallback);
	void setIsActive(bool value);

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	SurfaceDependentObject(const std::string& name);
};