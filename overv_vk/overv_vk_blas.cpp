/**
 * @file    overv_vk_blas.cpp
 * @brief	Bottom Level Acceleration Structure
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main include:
#include "overv_vk.h"

// Boost
#include "boost/container_hash/hash.hpp"


////////////
// STATIC //
////////////

// Special values:   
OvVK::BottomLevelAccelerationStructure OvVK::BottomLevelAccelerationStructure::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief BLAS Class reserved structure.
 */
struct OvVK::BottomLevelAccelerationStructure::Reserved
{
    std::map<uint32_t, GeometryData> geometriesData;                                                ///< Map containing the geometries properties of the geometries added to the BLAS.
    std::vector<VkAccelerationStructureGeometryKHR> accelerationStructureGeometriesKHR;             ///< Vector containing the structures specifying geometries in the BLAS.
    std::vector<VkAccelerationStructureBuildRangeInfoKHR> accelerationStructureBuildRangeInfosKHR;  ///< The vector containing the structures specifying build offsets and counts for the BLAS.

    OvVK::Buffer transformsDataBuffer;                                                          ///< The buffer containing all the transform matrix of each geometries relative to the BLAS.

    std::optional<std::size_t> hash;

    /**
     * Constructor.
     */
    Reserved() : hash{ std::nullopt }
    {}
};


////////////////////////////////////////////////////
// BODY OF CLASS BottomLevelAccelerationStructure //
////////////////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::BottomLevelAccelerationStructure::BottomLevelAccelerationStructure() : 
    reserved(std::make_unique<OvVK::BottomLevelAccelerationStructure::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::BottomLevelAccelerationStructure::BottomLevelAccelerationStructure(const std::string& name) :
    OvVK::AccelerationStructure(name), reserved(std::make_unique<OvVK::BottomLevelAccelerationStructure::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::BottomLevelAccelerationStructure::BottomLevelAccelerationStructure(
    BottomLevelAccelerationStructure&& other) noexcept: OvVK::AccelerationStructure(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::BottomLevelAccelerationStructure::~BottomLevelAccelerationStructure()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize acceleration struture
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::init() 
{
    if (this->OvVK::AccelerationStructure::init() == false)
        return false;


    // Free transforms buffer
    if (reserved->transformsDataBuffer.free() == false)
    {
        OV_LOG_ERROR("Fail to free transformsData buffer of the AS with id= %d", getId());
        return false;
    }

    // Clear
    reserved->accelerationStructureGeometriesKHR.clear();
    reserved->accelerationStructureBuildRangeInfosKHR.clear();


    // Done
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases acceleration struture
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::free()
{
    if (this->OvVK::AccelerationStructure::free() == false)
        return false;


    // Free transforms buffer
    if (reserved->transformsDataBuffer.free() == false)
    {
        OV_LOG_ERROR("Fail to free transformsData buffer of the AS with id= %d", getId());
        return false;
    }

    // Clear
    reserved->accelerationStructureGeometriesKHR.clear();
    reserved->accelerationStructureBuildRangeInfosKHR.clear();


    //Done
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Add a geometry to the BLAS.
 * @param geometry A OvVK geometry reference to add to the BLAS.
 * @param geometryFlags Flags specifying additional parameters for geometries in acceleration structure builds.
 * @param transformsMatrix A matrix to transform the position of the geometry inside the BLAS.
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::addGeometry(
    OvVK::Geometry &geometry,
    VkGeometryFlagsKHR geometryFlags,
    VkTransformMatrixKHR transformsMatrix)
{

    // Create geometry record
    GeometryData geometryData = {};
    geometryData.geometry = geometry;
    geometryData.geometryFlags = geometryFlags;
    geometryData.transformsMatrix = transformsMatrix;
    
    // If the BLAS is already build check if support update.
    if (OvVK::AccelerationStructure::getVkAS() != VK_NULL_HANDLE)
    {
        if (getVkBuildAccelerationStructureFlagsKHR() & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR 
            != VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR)
        {
            OV_LOG_ERROR("The AS with id= %d is builded. But can't be update because flag not set.", getId());
            return false;
        }
        else if (reserved->geometriesData.count(geometry.getId()) > 0) 
            reserved->geometriesData[geometry.getId()] = geometryData;
        else 
        {
            OV_LOG_ERROR("The AS with id= %d is builded. So only update are allowed.", getId());
            return false;
        }
    }
    else 
        reserved->geometriesData.insert(std::make_pair(geometry.getId(), geometryData));

    reserved->hash = std::nullopt;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up the data necessary to build the BLAS.
 * @param flags Flags to specify the AS "properties".
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::setUpBuild(VkBuildAccelerationStructureFlagsKHR flags)
{
    // Safety net:
    if (reserved->geometriesData.size() == 0)
    {
        OV_LOG_ERROR("No geometry added to the BLAS with id= %d.", getId());
        return false;
    }
    if (isLock()) 
    {
        OV_LOG_ERROR("Can't rebuild a BLAS if it is locked.");
        return false;
    }

    if (!this->init())
        return false;

    uint32_t nrOfGeometries = reserved->geometriesData.size();


    ///////////////////////////
    // transformsData Buffer //
    ///////////////////////////

    // Create BLASBuffer
    VkBufferCreateInfo vkTransforsDataBufferCreateInfo{};
    vkTransforsDataBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    vkTransforsDataBufferCreateInfo.size = sizeof(VkTransformMatrixKHR) * nrOfGeometries;
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    vkTransforsDataBufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
        | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    vkTransforsDataBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    vkTransforsDataBufferCreateInfo.pQueueFamilyIndices = NULL;
    vkTransforsDataBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaTransforsDataAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible free range for the allocation
    // to minimize memory usage and fragmentation, possibly at the expense of allocation time.
    vmaTransforsDataAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // 	Flags that must be set in a Memory Type chosen for an allocation. 
    vmaTransforsDataAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    if (!reserved->transformsDataBuffer.create(vkTransforsDataBufferCreateInfo, vmaTransforsDataAllocationCreateInfo))
    {
        OV_LOG_ERROR("Fail to create transfors data buffer for BLAS with id= %d.", getId());
        this->free();
        return false;
    }


    // Use general setUp methods.
    if (setUp() == false || this->OvVK::AccelerationStructure::setUp(VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
        reserved->accelerationStructureGeometriesKHR,
        reserved->accelerationStructureBuildRangeInfosKHR,
        VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR, flags) == false)
    {
        reserved->accelerationStructureBuildRangeInfosKHR.clear();
        reserved->accelerationStructureGeometriesKHR.clear();
        this->free();
        return false;
    }


    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up the data necessary to update the BLAS.
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::setUpUpdate() 
{
    //Safety net:
    if (getVkAS() == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The BLAS with id= %d is not build, so can't be update.", getId());
        return false;
    }
    if (isLock() == true)
    {
        OV_LOG_ERROR("Can't update the BLAS with id= %d if it is locked.", getId());
        return false;
    }
    if (getVkBuildAccelerationStructureFlagsKHR() & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR 
        != VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR)
    {
        OV_LOG_ERROR("Can't update BLAS with id= %d because the flag for update is not set.", getId());
        return false;
    }

    // Clear setUp data.
    clearSetUpData();

    if (setUp() == false || this->OvVK::AccelerationStructure::setUp(VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
        reserved->accelerationStructureGeometriesKHR,
        reserved->accelerationStructureBuildRangeInfosKHR,
        VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR, getVkBuildAccelerationStructureFlagsKHR()) == false)
    {
        reserved->accelerationStructureBuildRangeInfosKHR.clear();
        reserved->accelerationStructureGeometriesKHR.clear();
        this->free();
        return false;
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear all geometries added to the BLAS.
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::clearGeometries()
{
    // Safety Net:
    if (OvVK::AccelerationStructure::getVkAS() != VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The AS with id= %d is build so can't clear geometries.", getId());
        return false;
    }

    reserved->geometriesData.clear();

    reserved->hash = std::nullopt;

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear the setUp data used to build or update the AS.
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::clearSetUpData()
{
    if (!this->OvVK::AccelerationStructure::freeScratchBuffer())
    {
        OV_LOG_ERROR("Can't delete build data of the BLAS with id= %d if they are use in not terminate process.",
            getId());
        return false;
    }

    reserved->accelerationStructureBuildRangeInfosKHR.clear();
    reserved->accelerationStructureGeometriesKHR.clear();
    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up data needed to build or update the AS.
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::setUp()
{
    // Get pointer to the transformsData buffer memeory on the device.
    std::optional<VkDeviceAddress> deviceAddress = reserved->transformsDataBuffer.getBufferDeviceAddress();
    if (deviceAddress.has_value() == false)
    {
        OV_LOG_ERROR("Fail to retriever device address of the transform data buffer in the BLAS with id= %d.", getId());
        return false;
    }
    VkDeviceAddress currentPositionDeviceBuffer = deviceAddress.value();

    uint32_t geometriesCounter = 0;
    reserved->accelerationStructureGeometriesKHR.resize(reserved->geometriesData.size());
    reserved->accelerationStructureBuildRangeInfosKHR.resize(reserved->geometriesData.size());
    for (auto [key, value] : reserved->geometriesData)
    {
        // Query the 64-bit vertex/index buffer device address value through which buffer memory 
        // can be accessed in a shader
        std::optional<VkDeviceAddress> vertexBufferAddress = value.geometry.get()
            .getVertexBuffer().getBufferDeviceAddress();
        if (vertexBufferAddress.has_value() == false)
        {
            OV_LOG_ERROR(R"(Fail to retrive the device address of the vertex buffer of
 the geometry with id= %d in BLAS with id= %d)",
                value.geometry.get().getId(), getId());
            return false;
        }

        std::optional<VkDeviceAddress> faceBufferAddress = value.geometry.get()
            .getFaceBuffer().getBufferDeviceAddress();
        if (faceBufferAddress.has_value() == false)
        {
            OV_LOG_ERROR(R"(Fail to retrive the device address of the face buffer of
 the geometry with id= %d in BLAS with id= %d)",
                value.geometry.get().getId(), getId());
            return false;
        }

        VkDeviceOrHostAddressConstKHR vertexDeviceOrHostAddressConst = {};
        vertexDeviceOrHostAddressConst.deviceAddress = vertexBufferAddress.value();

        VkDeviceOrHostAddressConstKHR faceDeviceOrHostAddressConst = {};
        faceDeviceOrHostAddressConst.deviceAddress = faceBufferAddress.value();

        // Structure specifying a triangle geometry in a bottom-level acceleration structure
        VkAccelerationStructureGeometryTrianglesDataKHR accelerationStructureGeometryTrianglesData = {};
        accelerationStructureGeometryTrianglesData.sType =
            VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
        accelerationStructureGeometryTrianglesData.pNext = NULL;
        // Vertex glm::vec3
        accelerationStructureGeometryTrianglesData.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
        accelerationStructureGeometryTrianglesData.vertexData = vertexDeviceOrHostAddressConst;
        accelerationStructureGeometryTrianglesData.vertexStride = sizeof(Ov::Geometry::VertexData);
        // # vertices = vertex buffer size bytes / vertex stride
        accelerationStructureGeometryTrianglesData.maxVertex =
            value.geometry.get().getNrOfVertices();
        accelerationStructureGeometryTrianglesData.indexType = VK_INDEX_TYPE_UINT32;
        accelerationStructureGeometryTrianglesData.indexData = faceDeviceOrHostAddressConst;
        // a VkTransformMatrixKHR structure
        VkDeviceOrHostAddressConstKHR transformDataDeviceOrHostAddressConst = {};
        transformDataDeviceOrHostAddressConst.deviceAddress = currentPositionDeviceBuffer;
        accelerationStructureGeometryTrianglesData.transformData = transformDataDeviceOrHostAddressConst;

        // Union specifying acceleration structure geometry data
        VkAccelerationStructureGeometryDataKHR accelerationStructureGeometryData = {};
        accelerationStructureGeometryData.triangles = accelerationStructureGeometryTrianglesData;

        // Structure specifying geometries to be built into an acceleration structure
        VkAccelerationStructureGeometryKHR& accelerationStructureGeometry =
            reserved->accelerationStructureGeometriesKHR[geometriesCounter];
        accelerationStructureGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
        accelerationStructureGeometry.pNext = NULL;
        accelerationStructureGeometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
        accelerationStructureGeometry.geometry = accelerationStructureGeometryData;
        accelerationStructureGeometry.flags = value.geometryFlags;

        // Structure specifying build offsets and counts for acceleration structure
        VkAccelerationStructureBuildRangeInfoKHR& accelerationStructureBuildRangeInfoKHR =
            reserved->accelerationStructureBuildRangeInfosKHR[geometriesCounter];
        // primitiveCount defines the number of primitives for a corresponding acceleration structure geometry.
        accelerationStructureBuildRangeInfoKHR.primitiveCount =
            value.geometry.get().getNrOfFaces();
        accelerationStructureBuildRangeInfoKHR.primitiveOffset = 0;
        accelerationStructureBuildRangeInfoKHR.firstVertex = 0;
        accelerationStructureBuildRangeInfoKHR.transformOffset = 0;

        // Increment pointer in memory.
        currentPositionDeviceBuffer += sizeof(VkTransformMatrixKHR);
        // Increment geometries counter
        geometriesCounter++;
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the OvVK Buffer reference of the transforms data buffer.
 * @return The OvVK Buffer reference of the transforms data buffer.
 */
const OvVK::Buffer OVVK_API& OvVK::BottomLevelAccelerationStructure::getTransformsDataBuffer() const 
{
    return reserved->transformsDataBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the vector containing the references to the geomtries data type GeometryData.
 * @return The vector containing the references to the geomtries data type GeometryData.
 */
std::vector<std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData>> 
    OVVK_API OvVK::BottomLevelAccelerationStructure::getGeometriesData() const
{
    std::vector<std::reference_wrapper<const GeometryData>> geometriesData;
    for (auto& [key, value] : reserved->geometriesData)
        geometriesData.push_back(value);

    return geometriesData;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the size of the vector containing the references to the geomtries data type GeometryData.
 * @return The size of the vector containing the references to the geomtries data type GeometryData.
 */
uint32_t OVVK_API OvVK::BottomLevelAccelerationStructure::getNrOfGeometries() const
{
    return reserved->geometriesData.size();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the vector containing the structures specifying geometries in the BLAS.
 * @return The vector containing the structures specifying geometries in the BLAS.
 */
const std::vector<VkAccelerationStructureGeometryKHR> OVVK_API& 
    OvVK::BottomLevelAccelerationStructure::getAccelerationStructureGeometriesKHR() const
{
    return reserved->accelerationStructureGeometriesKHR;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the vector containing the structures specifying build offsets and counts for the BLAS.
 * @return The vector containing the structures specifying build offsets and counts for the BLAS.
 */
const std::vector<VkAccelerationStructureBuildRangeInfoKHR> OVVK_API& 
    OvVK::BottomLevelAccelerationStructure::getAccelerationStructureBuildRangeInfosKHR() const
{
    return reserved->accelerationStructureBuildRangeInfosKHR;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the hash of the BLAS
 * @return The hash of the BLAS
 */
std::size_t OVVK_API OvVK::BottomLevelAccelerationStructure::getHash() const 
{
    if (reserved->hash.has_value())
        return reserved->hash.value();

    std::size_t hash = 0;

    std::vector<uint32_t> ids;
    for (auto& [key, value] : reserved->geometriesData)
        ids.push_back(key);

    std::sort(ids.begin(), ids.end(), std::greater<uint32_t>());

    for(uint32_t c = 0; c < ids.size(); c++)
        boost::hash_combine(hash, ids[c]);

    // Add the hash for each transformsMatrix of each geometry.
    // Not implemented because for now not needed.

    // Done:
    return hash;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::BottomLevelAccelerationStructure::render(uint32_t value, void* data) const
{
    // Geomtries renders
    for (auto [key, value] : reserved->geometriesData)
        if (value.geometry.get().render() == false)
            return false;

    if (isDirty())
        setDirty(false);

    // Done:
    return true;
}

#pragma endregion