/**
 * @file	overv_vk_light.h
 * @brief	Vulkan light properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


/**
 * @brief Class for modeling an Vulkan light.
 */
class OVVK_API Light : public Ov::Light, public Ov::DrawGUI
{
//////////
public: //
//////////

	// static
	constexpr static uint32_t maxNrOfLights = 16;	///< Max number of lights that can be rendered per frame

	// Types of lights
	enum class Type : uint32_t {
		none = 0,
		directional = 1,
		spot = 2,
		omni = 3,
		all
	};

	// Special values:
	static Light empty;

	// Const/dest:
	Light();
	Light(Light&& other) noexcept;
	Light(Light const&) = delete;
	virtual ~Light();

	// Get/set:
	Type getLightType() const;
	float getPower() const;
	float getSpotCutoff() const;
	float getSpotExponent() const;
	glm::vec3 getDirection() const;

	void setLightType(Type lightType);
	void setPower(float power);
	void setSpotCutoff(float cutOff);
	void setSpotExponent(float exponent);
	void setDirection(glm::vec3 direction);

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const;

	// DrawGUI
	virtual void drawGUI() override;

	// Ovo
	bool saveChunk(Ov::Serializer& serial) const;
	uint32_t loadChunk(Ov::Serializer& serial, void* data = nullptr);

///////////
private: //
///////////   

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Light(const std::string& name);
};