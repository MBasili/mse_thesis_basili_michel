/**
 * @file	overv_vk_rt_scene.h
 * @brief	Vulkan ray tracing scene
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

class Container;
class RTObjDesc;
class TopLevelAccelerationStructure;
class BottomLevelAccelerationStructure;

 /**
  * @brief Class for handling ray tracing scene.
  */
class OVVK_API RTScene final : public Ov::Object, public Ov::Managed, public Ov::DrawGUI
{
//////////
public: //
//////////

	// Special values:
	static RTScene empty;

	constexpr static uint64_t defaultValueStatusSyncObj = 0;
	constexpr static uint64_t startValuePassStatusSyncObj = 1;
	constexpr static uint64_t finishValuePassStatusSyncObj = 2;
	constexpr static uint64_t startValueRederingStatusSyncObj = 1;
	constexpr static uint64_t finishValueRederingStatusSyncObj = 2;

	/**
	 * @brief Struct to use multiple thread to record commands.
	 */
	struct ThreadData;

	/**
	 * @brief Renderable element
	 */
	struct RenderableElem
	{
		std::reference_wrapper<Ov::Renderable> renderable;
		bool visibleFlag;										///< Visibility
		glm::mat4 matrix;										///< Final position matrix
		std::reference_wrapper<const Ov::Node> reference;		///< Reference to the original node

		/**
		 * Constructor.
		 */
		RenderableElem() : renderable{ Ov::Renderable::empty },
			reference{ Ov::Node::empty }
		{}
	};

	// Const/dest:
	RTScene();
	RTScene(RTScene&& other) noexcept;
	RTScene(RTScene const&) = delete;
	virtual ~RTScene();

	// Operators
	RTScene& operator=(const RTScene&) = delete;
	RTScene& operator=(RTScene&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// Scene graph traversal:
	bool pass(const Ov::Node& node, OvVK::Container& container,
		bool geometriesSharingSameShadersGroups = true, glm::mat4 prevMatrix = glm::mat4(1.0f));
	bool update(const Ov::Node& node, glm::mat4 prevMatrix = glm::mat4(1.0f));
	bool reset();
	bool clearPassData();

	// DrawGUI
	virtual void drawGUI() override;

	// Get/set:
	const OvVK::TopLevelAccelerationStructure& getTLAS() const;

	const std::vector<OvVK::RTScene::RenderableElem>& getRenderableElems() const;
	const OvVK::RTScene::RenderableElem& getRenderableElem(uint32_t elemNr) const;

	uint32_t getNrOfRenderableElems() const;
	uint32_t getNrOfLights() const;
	uint32_t getNrOfHitShaderGroupsNeeded() const;
	bool getAreGeometriesSharingSameShadersGroups() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkSemaphore getPassStatusSyncObjGPU() const;
	VkFence getPassStatusSyncObjCPU() const;
	VkSemaphore getRederingStatusSyncObjGPU() const;
	VkFence getRederingStatusSyncObjCPU() const;
	VkEvent getTlasBuildedEvent() const;
#endif

	OvVK::Buffer& getPickingDataBuffer() const;

	// Rendering:
	bool render(const glm::mat4& cameraMatrix, const glm::mat4& projectionMatrix);

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// SetUp for Upload/Build
	bool uploadRTObjDesc(OvVK::RTScene::ThreadData& threadData,
		std::list<std::reference_wrapper<OvVK::RTObjDesc>>& rtObjDescs,
		OvVK::Buffer& stagingGeometriesDataBuffer,
		VkEvent evetToSignal, std::vector<VkBufferMemoryBarrier2>& rtObjDescBufferMemoryBarrier2s);

	bool buildBLASs(OvVK::RTScene::ThreadData& threadData,
		std::list<std::reference_wrapper<OvVK::BottomLevelAccelerationStructure>>& blassToBuild,
		OvVK::Buffer& stagingTransformsDataBuffer,
		PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR,
		VkEvent eventToWait, std::vector<VkBufferMemoryBarrier2>& rtObjDescBufferMemoryBarrier2s,
		VkEvent eventToSignal, std::vector<VkBufferMemoryBarrier2>& blassBufferMemoryBarrier2s);

	bool buildTLASs(OvVK::RTScene::ThreadData& threadData,
		std::list<std::reference_wrapper<OvVK::TopLevelAccelerationStructure>>& tlassToBuild,
		OvVK::Buffer& stagingASInstanceBuffer,
		PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR,
		VkEvent eventToWait, std::vector<VkBufferMemoryBarrier2>& blassBufferMemoryBarrier2s,
		VkEvent eventToSignal, VkBufferMemoryBarrier2& tlasBufferMemoryBarrier2);
#endif

	// Get/Set
	bool setRederingStatusSyncObjGPUValue(uint64_t value);
	bool setPassStatusSyncObjGPUValue(uint64_t value);

	// Const/dest:
	RTScene(const std::string& name);
};