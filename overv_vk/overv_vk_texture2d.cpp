/**
 * @file    overv_vk_texture2d.cpp
 * @brief	Texture 2d
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// magic enum
#include "magic_enum.hpp"


////////////
// STATIC //
////////////

// Special values:
OvVK::Texture2D OvVK::Texture2D::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Texture2s reserved structure.
 */
struct OvVK::Texture2D::Reserved
{
    /**
     * Constructor.
     */
    Reserved()

    {}
};


///////////////////////////
// BODY OF CLASS Texture //
///////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Texture2D::Texture2D() : reserved(std::make_unique<OvVK::Texture2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Texture2D::Texture2D(const std::string& name) : OvVK::Texture(name),
    reserved(std::make_unique<OvVK::Texture2D::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Texture2D::Texture2D(Texture2D&& other) : OvVK::Texture(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Texture2D::~Texture2D()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Default

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the default texture2D.
 * @return The default texture2D.
 */
const OvVK::Texture2D OVVK_API& OvVK::Texture2D::getDefault()
{
    uint8_t data[] = { 255, 255, 255, 255 };
    static Ov::Bitmap dfltBitmap(Ov::Bitmap::Format::r8g8b8a8, 1, 1, data);
    static Texture2D dfltTexture2D;

    // SetUp image
    if (!dfltTexture2D.isInitialized())
    {
        dfltTexture2D.setSampler(OvVK::Sampler::getDefault());
        dfltTexture2D.load(dfltBitmap);
    }

    return dfltTexture2D;
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan texture2D.
 * @return TF
 */
bool OVVK_API OvVK::Texture2D::init()
{
    if (this->OvVK::Texture::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan texture2D.
 * @return TF
 */
bool OVVK_API OvVK::Texture2D::free()
{
    if (this->OvVK::Texture::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Loading

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads texture from a bitmap.
 * @param bitmap The bipmap that will be used to take the data of the texture.
 * @param isLinearEncoded If the data in the bitmap are linear encoded or not. (UNORM/SRGB)
 * @param holdReferenceBitmap If the texture need to hold the reference to the source bitmap.
 * @return TF
 */
bool OVVK_API OvVK::Texture2D::load(const Ov::Bitmap& bitmap, bool isLinearEncoded, bool holdReferenceBitmap)
{
    // Set texture
    if (setUp(bitmap, isLinearEncoded, holdReferenceBitmap) == false)
        return false;

    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Texture image
    std::reference_wrapper<const OvVK::Image> image = getImage();
    // Texture bitmap buffer
    std::reference_wrapper<const OvVK::Buffer> bitmapBuffer = getBitmapBuffer();

    // Get graphic pool to allocate the command buffer to transfer the bitmap buffer to texture's image and generate bitmap.
    std::reference_wrapper<OvVK::CommandPool> graphicCommandPool = engine.get().getPrimaryGraphicCmdPool();
    std::reference_wrapper<OvVK::CommandBuffer> graphicCommandBuffer = graphicCommandPool.get().allocateCommandBuffer();


    //////////////////////////
    // SetUp command buffer //
    //////////////////////////

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (vkBeginCommandBuffer(graphicCommandBuffer.get().getVkCommandBuffer(), &beginInfo) != VK_SUCCESS ||
        graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to begin or change status of transfer command buffer with id= %d for the texture2D with id= %d.",
            graphicCommandBuffer.get().getId(), getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }


    //////////////////////
    // Set write layout //
    //////////////////////

    // Barrier set up
    VkImageMemoryBarrier2 transferStateImageMemoryBarrier2 = {};
    transferStateImageMemoryBarrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
    // pNext is NULL or a pointer to a structure extending this structure.
    transferStateImageMemoryBarrier2.pNext = VK_NULL_HANDLE;
    // No need to wait previous stage for changing the texture layout
    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
    transferStateImageMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
    // No need to wait on specific memory access.
    transferStateImageMemoryBarrier2.srcAccessMask = VK_ACCESS_2_NONE;
    // All transfer commands need to wait that the texture change layout.
    transferStateImageMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
    // All write commands need to wait that the texture change layout.
    transferStateImageMemoryBarrier2.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
    // Current texture layout
    transferStateImageMemoryBarrier2.oldLayout = image.get().getImageLayout();
    // Set the layout that allow a fresh write to the texture.
    transferStateImageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    // Set chnage of ownership of the resource if necessary
    transferStateImageMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    transferStateImageMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    // The image and subresourceRange specify the image that is affected and the specific part of the image.
    transferStateImageMemoryBarrier2.image = image.get().getVkImage();
    transferStateImageMemoryBarrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    // describes the image subresource range within image that is affected by this barrier.
    // baseMipLevel is the first mipmap level accessible to the view.
    transferStateImageMemoryBarrier2.subresourceRange.baseMipLevel = 0;
    transferStateImageMemoryBarrier2.subresourceRange.levelCount = image.get().getNrOfLevels();
    transferStateImageMemoryBarrier2.subresourceRange.baseArrayLayer = 0;
    transferStateImageMemoryBarrier2.subresourceRange.layerCount = image.get().getNrOfLayers();

    // Barrier specification
    VkDependencyInfo transferStateDependencyInfo = {};
    transferStateDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    transferStateDependencyInfo.dependencyFlags = 0x00000000;
    transferStateDependencyInfo.memoryBarrierCount = 0;
    transferStateDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
    transferStateDependencyInfo.bufferMemoryBarrierCount = 0;
    transferStateDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
    transferStateDependencyInfo.imageMemoryBarrierCount = 1;
    transferStateDependencyInfo.pImageMemoryBarriers = &transferStateImageMemoryBarrier2;

    // Register command for transition.
    vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &transferStateDependencyInfo);


    /////////////////////////////
    // Record transfer command //
    /////////////////////////////

    vkCmdCopyBufferToImage(graphicCommandBuffer.get().getVkCommandBuffer(), bitmapBuffer.get().getVkBuffer(),
        image.get().getVkImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        static_cast<uint32_t>(getBufferImageCopyRegions().size()),
        getBufferImageCopyRegions().data());

#pragma region GenerateMipmap

    // Loaded
    if (getNeedGenerateMipmap())
    {
        /////////////////////
        // Generate Mipmap //
        /////////////////////

        // if greater than 1 need to generate mipmap
        if (image.get().getNrOfLevels() > 1)
        {
            //Make several transitions, so we'll reuse this VkImageMemoryBarrier.
            // Barrier set up
            VkImageMemoryBarrier2 mipmapBarrier = {};
            mipmapBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
            // pNext is NULL or a pointer to a structure extending this structure.
            mipmapBarrier.pNext = VK_NULL_HANDLE;
            // No need to transfer ownership because first time used.
            mipmapBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            mipmapBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            // The image and subresourceRange specify the image that is affected and the specific part of the image.
            mipmapBarrier.image = image.get().getVkImage();
            mipmapBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            // baseMipLevel is the first mipmap level accessible to the view.
            mipmapBarrier.subresourceRange.baseMipLevel = 0;
            mipmapBarrier.subresourceRange.levelCount = 1;
            mipmapBarrier.subresourceRange.baseArrayLayer = 0;
            mipmapBarrier.subresourceRange.layerCount = 1;

            // Barrier specification
            VkDependencyInfo mipmapDependencyInfo = {};
            mipmapDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
            mipmapDependencyInfo.dependencyFlags = 0x00000000;
            mipmapDependencyInfo.memoryBarrierCount = 0;
            mipmapDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
            mipmapDependencyInfo.bufferMemoryBarrierCount = 0;
            mipmapDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
            mipmapDependencyInfo.imageMemoryBarrierCount = 1;
            mipmapDependencyInfo.pImageMemoryBarriers = &mipmapBarrier;

            int32_t mipWidth = getSizeX();
            int32_t mipHeight = getSizeY();


            for (uint32_t c = 0; c < image.get().getNrOfLayers(); c++) {
                // The fields set above will remain the same for all barriers. subresourceRange.miplevel,
                // oldLayout, newLayout, srcAccessMask, and dstAccessMask will be changed for each transition.
                for (uint32_t i = 1; i < image.get().getNrOfLevels(); i++) {

                    // Vulkan allows us to transition each mip level of an image independently. Each blit will only deal
                    // with two mip levels at a time, so we can transition each level into the optimal layout between blits commands.
                    // Wwait previous stage for changing the texture layout
                    mipmapBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                    // Wait on specific memory access.
                    mipmapBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
                    // All blit commands need to wait that the texture change layout.
                    mipmapBarrier.dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                    // All write commands need to wait that the texture change layout.
                    mipmapBarrier.dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT;
                    // Current texture layout
                    mipmapBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                    // Set the layout that allow a fresh write to the texture.
                    mipmapBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                    // baseMipLevel is the first mipmap level accessible to the view.
                    mipmapBarrier.subresourceRange.baseMipLevel = i - 1;
                    // baseMipLevel is the first layer accessible to the view.
                    mipmapBarrier.subresourceRange.baseArrayLayer = c;

                    // First, we transition level i - 1 to VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL. This transition will
                    // wait for level i - 1 to be filled, either from the previous blit command, or from vkCmdCopyBufferToImage.
                    // Register command for transition.
                    vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &mipmapDependencyInfo);


                    // Next, we specify the regions that will be used in the blit operation.
                    // The source mip level is i - 1 and the destination mip level is i. 
                    // The Z dimension of srcOffsets[1] and dstOffsets[1] must be 1, since a 2D image has a depth of 1.
                    VkImageBlit2 blit = {};
                    blit.sType = VK_STRUCTURE_TYPE_IMAGE_BLIT_2;
                    blit.pNext = NULL;
                    // The two elements of the srcOffsets array determine the 3D region that data will be blitted from.
                    blit.srcOffsets[0] = { 0, 0, 0 };
                    blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
                    blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                    // The source mip level is i - 1
                    blit.srcSubresource.mipLevel = i - 1;
                    blit.srcSubresource.baseArrayLayer = c;
                    blit.srcSubresource.layerCount = 1;

                    // dstOffsets determines the region that data will be blitted to.
                    blit.dstOffsets[0] = { 0, 0, 0 };
                    // The X and Y dimensions of the dstOffsets[1] are divided by two since each mip level is half the size of the previous level.
                    blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
                    blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                    // the destination mip level is i. 
                    blit.dstSubresource.mipLevel = i;
                    blit.dstSubresource.baseArrayLayer = c;
                    blit.dstSubresource.layerCount = 1;

                    // Note that textureImage is used for both the srcImageand dstImage parameter.This is because we're blitting
                    // between different levels of the same image. The source mip level was just transitioned to
                    // VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL and the destination level is still in VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL from createTextureImage.
                    // vkCmdBlitImage must be submitted to a queue with graphics capability.
                    // The last parameter allows us to specify a VkFilter to use in the blit. We have the same 
                    // filtering options here that we had when making the VkSampler. We use the VK_FILTER_LINEAR to enable interpolation.
                    VkBlitImageInfo2 blitImageInfo2 = {};
                    blitImageInfo2.sType = VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2;
                    blitImageInfo2.pNext = NULL;
                    blitImageInfo2.srcImage = image.get().getVkImage();
                    blitImageInfo2.srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                    blitImageInfo2.dstImage = image.get().getVkImage();
                    blitImageInfo2.dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                    blitImageInfo2.regionCount = 1;
                    blitImageInfo2.pRegions = &blit;
                    blitImageInfo2.filter = VK_FILTER_LINEAR;

                    vkCmdBlitImage2(graphicCommandBuffer.get().getVkCommandBuffer(), &blitImageInfo2);

                    // Wait previous stage for changing the texture layout
                    mipmapBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                    // Wait on specific memory access.
                    mipmapBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT;
                    // All commands need to wait that the texture change layout.
                    // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
                    mipmapBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                    // All commands need to wait that the texture change layout.
                    mipmapBarrier.dstAccessMask = VK_ACCESS_2_NONE;
                    // Current texture layout
                    mipmapBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                    // Set the layout that allow a fresh write to the texture.
                    mipmapBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

                    // This transition waits on the current blit command to finish.
                    vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &mipmapDependencyInfo);

                    // At the end of the loop, we divide the current mip dimensions by two. We check each dimension before
                    // the division to ensure that dimension never becomes 0. This handles cases where the image is not square,
                    // since one of the mip dimensions would reach 1 before the other dimension. When this happens, 
                    // that dimension should remain 1 for all remaining levels.
                    if (mipWidth > 1) mipWidth /= 2;
                    if (mipHeight > 1) mipHeight /= 2;
                }

                // This barrier transitions the last mip level from VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL 
                // to VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL. This wasn't handled by the loop, 
                // since the last mip level is never blitted from.
                // Wait previous stage for changing the texture layout
                mipmapBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
                // Wait on specific memory access.
                mipmapBarrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
                // All commands need to wait that the texture change layout.
                // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
                mipmapBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                // All commands need to wait that the texture change layout.
                mipmapBarrier.dstAccessMask = VK_ACCESS_2_NONE;
                // Current texture layout
                mipmapBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                // Set the layout that allow a fresh write to the texture.
                mipmapBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                // baseMipLevel is the first mipmap level accessible to the view.
                mipmapBarrier.subresourceRange.baseMipLevel = image.get().getNrOfLevels() - 1;
                // baseMipLevel is the first layer accessible to the view.
                mipmapBarrier.subresourceRange.baseArrayLayer = c;

                // This transition waits on the current blit command to finish.
                vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &mipmapDependencyInfo);
            }
        }
    }
    else
    {
	    /////////////////////
        // Set read layout //
        /////////////////////
        
        // Barrier set up
        VkImageMemoryBarrier2 readStateImageMemoryBarrier2 = {};
        readStateImageMemoryBarrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
        // pNext is NULL or a pointer to a structure extending this structure.
        readStateImageMemoryBarrier2.pNext = VK_NULL_HANDLE;
        // Wait previous transfer stage for changing the texture layout
        readStateImageMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
        // Wait on specific write memory access.
        readStateImageMemoryBarrier2.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
        // All commands need to wait that the texture change layout.
        // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
        readStateImageMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // All commands need to wait that the texture change layout.
        readStateImageMemoryBarrier2.dstAccessMask = VK_ACCESS_2_NONE;
        // Current texture layout
        readStateImageMemoryBarrier2.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        // Set the layout that allow a fresh write to the texture.
        readStateImageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        // Set chnage of ownership of the resource if necessary
        readStateImageMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        readStateImageMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        // The image and subresourceRange specify the image that is affected and the specific part of the image.
        readStateImageMemoryBarrier2.image = image.get().getVkImage();
        readStateImageMemoryBarrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        // describes the image subresource range within image that is affected by this barrier.
        // baseMipLevel is the first mipmap level accessible to the view.
        readStateImageMemoryBarrier2.subresourceRange.baseMipLevel = 0;
        readStateImageMemoryBarrier2.subresourceRange.levelCount = image.get().getNrOfLevels();
        readStateImageMemoryBarrier2.subresourceRange.baseArrayLayer = 0;
        readStateImageMemoryBarrier2.subresourceRange.layerCount = image.get().getNrOfLayers();
        
        
        // Barrier specification
        VkDependencyInfo shaderStateDependencyInfo = {};
        shaderStateDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
        shaderStateDependencyInfo.dependencyFlags = 0x00000000;
        shaderStateDependencyInfo.memoryBarrierCount = 0;
        shaderStateDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
        shaderStateDependencyInfo.bufferMemoryBarrierCount = 0;
        shaderStateDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
        shaderStateDependencyInfo.imageMemoryBarrierCount = 1;
        shaderStateDependencyInfo.pImageMemoryBarriers = &readStateImageMemoryBarrier2;
        
        // Register command for transition. If two command buffer this is the release of the resource.
        vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &shaderStateDependencyInfo);
    }

#pragma endregion

    // Close command buffer registering
    if (vkEndCommandBuffer(graphicCommandBuffer.get().getVkCommandBuffer()) != VK_SUCCESS ||
        graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to end or change status of graphic command buffer with id=% for the texture2D with id= %d.",
            graphicCommandBuffer.get().getId(), getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }


    ////////////
    // Submit //
    ////////////

    // Submit
    VkSubmitInfo2 submitInfo2 = {};
    submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    submitInfo2.pNext = VK_NULL_HANDLE;
    submitInfo2.flags = 0x00000000;

    VkSemaphoreSubmitInfo pWaitSemaphoreInfos;
    pWaitSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    pWaitSemaphoreInfos.pNext = VK_NULL_HANDLE;
    pWaitSemaphoreInfos.value = OvVK::Texture::uploadingValueStatusSyncObj;
    pWaitSemaphoreInfos.semaphore = image.get().getStatusSyncObj();
    pWaitSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    pWaitSemaphoreInfos.deviceIndex = 0;

    submitInfo2.waitSemaphoreInfoCount = 1;
    submitInfo2.pWaitSemaphoreInfos = &pWaitSemaphoreInfos;

    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = graphicCommandBuffer.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;

    submitInfo2.commandBufferInfoCount = 1;
    submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(2);
    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfo.value = OvVK::Texture::readyValueStatusSyncObj;
    vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfo.deviceIndex = 0;

    vkSemaphoreSubmitInfo.semaphore = image.get().getStatusSyncObj();
    pSignalSemaphoreInfos[0] = vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.semaphore = bitmapBuffer.get().getStatusSyncObj();
    pSignalSemaphoreInfos[1] = vkSemaphoreSubmitInfo;

    submitInfo2.signalSemaphoreInfoCount = 2;
    submitInfo2.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();


    // Create sync CPU obj to check the status of the command buffer execution.
    VkFenceCreateInfo vkFenceCreateInfo = {};
    vkFenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    vkFenceCreateInfo.pNext = VK_NULL_HANDLE;
    vkFenceCreateInfo.flags = 0x00000000;

    VkFence syncObjDefault;
    if (vkCreateFence(logicalDevice.get().getVkDevice(), &vkFenceCreateInfo, nullptr, &syncObjDefault) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create sync obj to graphic the texture2D with id= %d.", getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }

    // Set up the default texture status and lock obj for the transfer.
    if (setStatusSyncObjValue(OvVK::Texture::uploadingValueStatusSyncObj) == false ||
        setLockSyncObj(syncObjDefault) == false)
    {
        OV_LOG_ERROR("Fail to change status or set the lock to the texture2D with id= %d for trasfering data from bitmap buffer.",
            getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }

    // Submit
    if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(graphicCommandBuffer.get().getQueueFamilyIndex().value()),
        1, &submitInfo2, syncObjDefault) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit graphic command buffer to trasfering data from bitmap buffer to the texture2D with id= %d.",
            getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }

    // Wait that the transfer command buffer finish.
    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &syncObjDefault, VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait until the texture2D with id= %d is ready.", getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }

    if (setImageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) == false ||
        setQueueFamilyIndex(graphicCommandPool.get().getQueueFamilyIndex().value()) == false ||
        freeBitmapBuffer() == false ||
        setLockSyncObj(VK_NULL_HANDLE) == false)
    {
        OV_LOG_ERROR("Fail to configure the texture2D with id= %d after bitmap buffer copy.", getId());
        graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
        this->free();
        return false;
    }

    if (graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get()) == false)
    {
        OV_LOG_ERROR("Fail to free command buffer used to trasfer bitmap buffer to the texture2D with id= %d", getId());
        return false;
    }

    // Destroy the sync CPU obj.
    vkDestroyFence(logicalDevice.get().getVkDevice(), syncObjDefault, nullptr);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * SetUp the image and the bitmap buffer for loading the texture.
 * @param bitmap The bipmap that will be used to take the data of the texture.
 * @param isLinearEncoded If the data in the bitmap are linear encoded or not. (UNORM/SRGB)
 * @param holdReferenceBitmap If the texture need to hold the reference to the source bitmap.
 * @return TF
 */
bool OVVK_API OvVK::Texture2D::setUp(const Ov::Bitmap& bitmap, bool isLinearEncoded, bool holdReferenceBitmap)
{
    // Safety net:
    if (bitmap == Ov::Bitmap::empty)
    {
        OV_LOG_ERROR("Invalid bitmap passed to the setUp method of the texture 2D with id= %d", getId());
        return false;
    }

    // Get info of mipmap
    uint32_t nrOfLevels = bitmap.getNrOfLevels();
    uint32_t nrOfSides = bitmap.getNrOfSides();

    // Texture format
    Ov::Texture::Format ovTextureFomart = Ov::Texture::Format::none;
    VkFormat vkTextureFormat;
    bool generateMipmap = false;

	// Load the texture with the gamma correction format.
	switch (bitmap.getFormat())
	{
		////////////////////////////
	case Ov::Bitmap::Format::r8: //         
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8_UNORM : VK_FORMAT_R8_SRGB;
		ovTextureFomart = Ov::Texture::Format::r8;
        if (nrOfLevels == 1)
            generateMipmap = true;
		break;

		//////////////////////////////
	case Ov::Bitmap::Format::r8g8: //         
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8_UNORM : VK_FORMAT_R8_SRGB;
		ovTextureFomart = Ov::Texture::Format::r8g8;
        if (nrOfLevels == 1)
            generateMipmap = true;
		break;

		////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8: //         
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8G8B8_UNORM : VK_FORMAT_R8G8B8_SRGB;
		ovTextureFomart = Ov::Texture::Format::r8g8b8;
        if (nrOfLevels == 1)
            generateMipmap = true;
		break;

		//////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8a8: //		   
		ovTextureFomart = Ov::Texture::Format::r8g8b8a8;
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8G8B8A8_UNORM : VK_FORMAT_R8G8B8A8_SRGB;
        if (nrOfLevels == 1)
            generateMipmap = true;
		break;

		///////////////////////////////////////
	case Ov::Bitmap::Format::r8_compressed: //
		ovTextureFomart = Ov::Texture::Format::r8_compressed;
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC4_UNORM_BLOCK : VK_FORMAT_BC4_SNORM_BLOCK;
		break;

		/////////////////////////////////////////
	case Ov::Bitmap::Format::r8g8_compressed: //
		ovTextureFomart = Ov::Texture::Format::r8g8_compressed;
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC5_UNORM_BLOCK : VK_FORMAT_BC5_SNORM_BLOCK;
		break;

		///////////////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8_compressed: //
		ovTextureFomart = Ov::Texture::Format::r8g8b8_compressed;
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC1_RGB_UNORM_BLOCK : VK_FORMAT_BC1_RGB_SRGB_BLOCK;
		break;

		/////////////////////////////////////////////
	case Ov::Bitmap::Format::r8g8b8a8_compressed: //
		ovTextureFomart = Ov::Texture::Format::r8g8b8a8_compressed;
		vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC3_UNORM_BLOCK : VK_FORMAT_BC3_SRGB_BLOCK;
		break;

		///////
	default: //
		OV_LOG_ERROR("Unexpected bitmap type (%s)", OV_LOG_ENUM(bitmap.getFormat()));
		return false;
	}

    ////////////////////
    // Create Texture //
    ////////////////////

    // Get info of the textures
    uint32_t textureNrOfLevels = bitmap.getNrOfLevels();
    if (generateMipmap)
    {
        // calculate level of mipmap
        // The max function selects the largest dimension. The log2 function calculates how many times that dimension
        // can be divided by 2. The floor function handles cases where the largest dimension is not a power of 2.
        // 1 is added so that the original image has a mip level.
        textureNrOfLevels = static_cast<uint32_t>(std::floor(
            std::log2(std::max(bitmap.getSizeX(), bitmap.getSizeY())))) + 1;
        if (nrOfLevels != textureNrOfLevels) {
            setNeedGenerateMipmap(true);
        }
    }

    VkImageViewType imageViewType = VK_IMAGE_VIEW_TYPE_2D;
    VkImageCreateFlags imageFlags = 0x00000000;

    if (nrOfSides == 6) {
        imageViewType = VK_IMAGE_VIEW_TYPE_CUBE;
        imageFlags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    }

    if (this->create(bitmap.getSizeX(), bitmap.getSizeY(), textureNrOfLevels,
        nrOfSides, ovTextureFomart, isLinearEncoded, imageViewType, imageFlags) == false)
        return false;


    ///////////////////
    // Bitmap buffer //
    ///////////////////

    // Calculate Buffer size
    VkDeviceSize size = 0;
    for (uint32_t s = 0; s < nrOfSides; s++)
    {
        for (uint32_t c = 0; c < nrOfLevels; c++)
        {
            // Debug message
            OV_LOG_DEBUG("Type: 2D, side: %d/%d, Level: %d/%d, IntFormat: 0x%x, x: %u, y: %u", s + 1, bitmap.getNrOfSides(),
                c + 1, bitmap.getNrOfLevels(),
                ovTextureFomart, bitmap.getSizeX(c, s), bitmap.getSizeY(c, s));

            // Compute image size
            size += bitmap.getNrOfBytes(c, s);
        }
    }

    // Create bitmap buffer for current mipmap level.
    // Create info buffer
    VkBufferCreateInfo bitmapBufferCreateInfo{};
    bitmapBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    bitmapBufferCreateInfo.size = size;
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    // VK_BUFFER_USAGE_TRANSFER_SRC_BIT specifies that the buffer can be used as the source of a transfer command.
    bitmapBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    bitmapBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    bitmapBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    bitmapBufferCreateInfo.queueFamilyIndexCount = 0;


    // Create allocation info
    VmaAllocationCreateInfo bitmapBufferAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_MAPPED_BIT => Set this flag to use a memory that will be persistently 
    // mappedand retrieve pointer to it. It is valid to use this flag for allocation made from memory
    // type that is not HOST_VISIBLE. This flag is then ignored and memory is not mapped. This is useful
    // if you need an allocation that is efficient to use on GPU (DEVICE_LOCAL) and still want to map it
    // directly if possible on platforms that support it (e.g. Intel GPU).
    bitmapBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
        VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // 	Flags that must be set in a Memory Type chosen for an allocation. 
    bitmapBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    if (createBitmapBuffer(bitmapBufferCreateInfo, bitmapBufferAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create bitmap buffer fot texture2D with id= %d", getId());
        this->free();
        return false;
    }


    // Transfer data from bitmap to buffer
    // Get pointer to the start of the memeory
    uint8_t* bitmapBufferPos = (uint8_t*)getBitmapBuffer().getVmaAllocationInfo().pMappedData;
    // Offset of the bitmap buffer.
    uint64_t offset = 0;
    // Copy regions to copy bitmap buffer to image
    std::vector<VkBufferImageCopy> regions;

    // Copy data into the buffer
    for (uint32_t s = 0; s < nrOfSides; s++)
    {
        for (uint32_t c = 0; c < nrOfLevels; c++)
        {
            // Copy data in bitmap buffer.
            uint32_t currentLevelSize = bitmap.getNrOfBytes(c, s);
            memcpy(bitmapBufferPos + offset, bitmap.getData(c, s), (size_t)currentLevelSize);

            // Record copy from bitmap buffer to image
            VkBufferImageCopy& region = regions.emplace_back();

            // The bufferOffset specifies the byte offset in the buffer at which the pixel values start.
            // The bufferRowLength and bufferImageHeight fields specify how the pixels are laid out in memory.
            // For example, you could have some padding bytes between rows of the image. Specifying 0 for both
            // indicates that the pixels are simply tightly packed like they are in our case.
            region.bufferOffset = offset;
            region.bufferRowLength = 0;
            region.bufferImageHeight = 0;

            // The imageSubresource, imageOffset and imageExtent fields indicate to which part of the image we want to copy the pixels.
            region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.baseArrayLayer = s;
            region.imageSubresource.layerCount = 1;

            region.imageSubresource.mipLevel = c;


            region.imageOffset = { 0, 0, 0 };
            region.imageExtent = {
                bitmap.getSizeX(c, s),
                bitmap.getSizeY(c, s),
                1
            };

            // Increment the pointer by the number of bytes used to store the current data.
            offset += currentLevelSize;
        }
    }

    this->OvVK::Texture::setBufferImageCopyRegions(regions);

    // Hold reference to the bitmap
    if (holdReferenceBitmap)
        this->Ov::Texture::setBitmap(bitmap);


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates texture 2D.
 * @param bitmap The bipmap that will be used to take the data of the texture.
 * @param width The width of the texture.
 * @param height The height of the texture.
 * @param nrOfLevels The number of levels(levels mipmap) in the texture.
 * @param nrOfLayers The number of layers(sides) in the texture.
 * @param format The format of the texture.
 * @param isLinearEncoded If the data in the bitmap are linear encoded or not. (need to choose the texture format)
 * @return TF
 */
bool OVVK_API OvVK::Texture2D::create(uint32_t width, uint32_t height, uint32_t nrOfLevels,
    uint32_t nrOfLayers, Format format, bool isLinearEncoded, VkImageViewType imageViewType,
    VkImageCreateFlags flagsImage, VkImageViewCreateFlags flagsImageView)
{
    // Safety net:
    if (nrOfLevels == 0 || nrOfLayers == 0)
    {
        OV_LOG_ERROR("The nrOfLevels or the nrOfLayers is 0 in the texture2D with id= %d.", getId());
        return false;
    }
    if (width == 0 || height == 0)
    {
        OV_LOG_ERROR("The width or the height is 0 in the texture2D with id= %d.", getId());
        return false;
    }

    // Texture format
    Ov::Texture::Format ovTextureFomart = Ov::Texture::Format::none;
    VkFormat vkTextureFormat = VK_FORMAT_UNDEFINED;

    // Load the texture with the gamma correction format.
    switch (format)
    {
        ////////////////////////////
    case Ov::Texture::Format::r8: //         
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8_UNORM : VK_FORMAT_R8_SRGB;
        ovTextureFomart = Ov::Texture::Format::r8;
        break;

        //////////////////////////////
    case Ov::Texture::Format::r8g8: //         
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8_UNORM : VK_FORMAT_R8_SRGB;
        ovTextureFomart = Ov::Texture::Format::r8g8;
        break;

        ////////////////////////////////
    case Ov::Texture::Format::r8g8b8: //         
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8G8B8_UNORM : VK_FORMAT_R8G8B8_SRGB;
        ovTextureFomart = Ov::Texture::Format::r8g8b8;
        break;

        //////////////////////////////////
    case Ov::Texture::Format::r8g8b8a8: //		   
        ovTextureFomart = Ov::Texture::Format::r8g8b8a8;
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_R8G8B8A8_UNORM : VK_FORMAT_R8G8B8A8_SRGB;
        break;

        ///////////////////////////////////////
    case Ov::Texture::Format::r8_compressed: //
        ovTextureFomart = Ov::Texture::Format::r8_compressed;
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC4_UNORM_BLOCK : VK_FORMAT_BC4_SNORM_BLOCK;
        break;

        /////////////////////////////////////////
    case Ov::Texture::Format::r8g8_compressed: //
        ovTextureFomart = Ov::Texture::Format::r8g8_compressed;
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC5_UNORM_BLOCK : VK_FORMAT_BC5_SNORM_BLOCK;
        break;

        ///////////////////////////////////////////
    case Ov::Texture::Format::r8g8b8_compressed: //
        ovTextureFomart = Ov::Texture::Format::r8g8b8_compressed;
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC1_RGB_UNORM_BLOCK : VK_FORMAT_BC1_RGB_SRGB_BLOCK;
        break;

        /////////////////////////////////////////////
    case Ov::Texture::Format::r8g8b8a8_compressed: //
        ovTextureFomart = Ov::Texture::Format::r8g8b8a8_compressed;
        vkTextureFormat = isLinearEncoded ? VK_FORMAT_BC3_UNORM_BLOCK : VK_FORMAT_BC3_SRGB_BLOCK;
        break;

        ///////
    default: //
        OV_LOG_ERROR("Unexpected bitmap type (%s)", OV_LOG_ENUM(format));
        return false;
    }

    // Init
    if (!this->OvVK::Texture2D::init())
        return false;


    //////////////////
    // Create Image //
    //////////////////

#pragma region CreateImage

    // Image info
    VkImageCreateInfo imageInfo{};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    // The image type, specified in the imageType field, tells Vulkan with what kind of coordinate
    // system the texels in the image are going to be addressed. 
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    // Vulkan supports many possible image formats, but we should use the same format 
    // for the texels as the pixels in the buffer, otherwise the copy operation will fail.
    imageInfo.format = vkTextureFormat;

    imageInfo.flags = flagsImage;

    // The extent field specifies the dimensions of the image, basically how many texels there are on each axis.
    VkExtent3D extent3D = {};
    extent3D.width = width;
    extent3D.height = height;
    extent3D.depth = 1;

    imageInfo.extent = extent3D;
    imageInfo.mipLevels = nrOfLevels;
    imageInfo.arrayLayers = nrOfLayers;

    // The samples flag is related to multisampling. This is only relevant
    // for images that will be used as attachments, so stick to one sample.
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

    // The tiling field can have one of two values:
    // - VK_IMAGE_TILING_LINEAR: Texels are laid out in row - major order like our pixels array
    // - VK_IMAGE_TILING_OPTIMAL : Texels are laid out in an implementation defined order for optimal access

    // Unlike the layout of an image, the tiling mode cannot be changed at a later time. 
    // If you want to be able to directly access texels in the memory of the image, then you
    // must use VK_IMAGE_TILING_LINEAR. We will be using VK_IMAGE_TILING_OPTIMAL
    // for efficient access from the shader.
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;

    // The usage field has the same semantics as the one during buffer creation. 
    // The image is going to be used as destination for the buffer copy, so it should
    // be set up as a transfer destination. We also want to be able to access the image 
    // from the shader to color our mesh, so the usage should include VK_IMAGE_USAGE_SAMPLED_BIT.
    imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT
        | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    imageInfo.queueFamilyIndexCount = 0;

    // There are only two possible values for the initialLayout of an image:
    // - VK_IMAGE_LAYOUT_UNDEFINED: Not usable by the GPUand the very first transition will discard the texels.
    // - VK_IMAGE_LAYOUT_PREINITIALIZED : Not usable by the GPU, but the first transition will preserve the texels.
    // There are few situations where it is necessary for the texels to be preserved during the first transition. 
    // One example, however, would be if you wanted to use an image as a staging image in combination with the
    // VK_IMAGE_TILING_LINEAR layout. In that case, you'd want to upload the texel data to it and then
    // transition the image to be a transfer source without losing the data. In our case, however, 
    // we're first going to transition the image to be a transfer destination and then copy texel
    // data to it from a buffer object, so we don't need this property and can safely 
    // use VK_IMAGE_LAYOUT_UNDEFINED.
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;


    // Image allocation info
    VmaAllocationCreateInfo allocInfo = {};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
    // free range for the allocation
    // to minimize memory usage and fragmentation, possibly at the expense of allocation time.
    allocInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Imageview info
    VkImageViewCreateInfo imageViewCreateInfo = {};
    imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCreateInfo.pNext = VK_NULL_HANDLE;
    imageViewCreateInfo.flags = flagsImageView;
    imageViewCreateInfo.viewType = imageViewType;
    imageViewCreateInfo.format = vkTextureFormat;
    imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    imageViewCreateInfo.subresourceRange.levelCount = nrOfLevels;
    imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    imageViewCreateInfo.subresourceRange.layerCount = nrOfLayers;

    // Create image
    if (createImage(imageInfo, allocInfo, imageViewCreateInfo) == false)
    {
        OV_LOG_INFO("Fail to create image for the Texture2D with id= %d", getId());
        this->free();
        return false;
    }

#pragma endregion


    // Set up image properties.
    if (setStatusSyncObjValue(OvVK::Texture::readyToUploadValueStatusSyncObj) == false ||
        setImageLayout(VK_IMAGE_LAYOUT_UNDEFINED) == false)
    {
        OV_LOG_ERROR("Fail set the status or image layout value in the texture2D with id= %d.", getId());
        this->free();
        return false;
    }

    // Set info
    this->Ov::Texture::setFormat(format);
    this->Ov::Texture::setSizeX(width);
    this->Ov::Texture::setSizeY(height);

    this->Ov::Object::setDirty(true);

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::Texture2D::render(uint32_t value, void* data) const
{
    // Sanity check:
    if (getLutPos() == OvVK::Texture::maxNrOfTextures)
    {
        OV_LOG_ERROR("Invalid LUT entry for texture '%s', id %u", this->getName().c_str(), this->getId());
        return false;
    }

    if (isDirty()) 
    {
        // upload into textures descriptorSet
        if (TexturesDescriptorSetsManager::getInstance().addToUpdateList(*this) == false)
        {
            OV_LOG_INFO("Fail to upload texture2D with id= %d into the textures descriptorSetsManager.", getId());
            return false;
        }

        setDirty(false);
    }

    // Done:
    return true;
}

#pragma endregion