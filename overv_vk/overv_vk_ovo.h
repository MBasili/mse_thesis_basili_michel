/**
 * @file	overv_vk_ovo.h
 * @brief	OVO import/export functions
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


 /**
  * @brief 3D OVO manager.
  */
class OVVK_API Ovo : public Ov::Ovo
{
/////////////
protected: //
/////////////	

	// Derived-type specific factories:
	void* createByType(ChunkId type) const;
};