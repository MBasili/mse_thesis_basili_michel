/**
 * @file	overv_vk_dsm.h
 * @brief	Descriptor set manager
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling a descriptor set manager.
  */
class OVVK_API DescriptorSetsManager : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

	// Static
	static DescriptorSetsManager empty;

	// Const/dest:
	DescriptorSetsManager();
	DescriptorSetsManager(DescriptorSetsManager&& other);
	DescriptorSetsManager(DescriptorSetsManager const&) = delete;
	virtual ~DescriptorSetsManager();

	// Operators
	DescriptorSetsManager& operator=(const DescriptorSetsManager&) = delete;
	DescriptorSetsManager& operator=(DescriptorSetsManager&&) = delete;

	// Managed:
	bool virtual init() override;
	bool virtual free() override;

	// General:
	virtual bool create();

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Get/set:   
	VkDescriptorPool getVkDescriptorPool() const;
	VkDescriptorSetLayout getVkDescriptorSetLayout() const;
	VkDescriptorSet getVkDescriptorSet(uint32_t idDescriptorSet) const;
#endif

	// Rendering
	virtual bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool create(std::vector<VkDescriptorPoolSize>& descriptorPoolSizes,
		std::vector<VkDescriptorSetLayoutBinding>& descriptorSetLayoutBindings,
		uint32_t nrDescriptorSets);
#endif

	// Const/dest:
	DescriptorSetsManager(const std::string& name);
};