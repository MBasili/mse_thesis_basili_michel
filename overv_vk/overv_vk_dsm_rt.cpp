/**
 * @file	overv_vk_dsm_rt.cpp
 * @brief	Descriptor set manager for ray tracing
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Ray tracing descriptor set manager reserved structure.
 */
struct OvVK::RayTracingDescriptorSetsManager::Reserved
{
    std::pair<VkAccelerationStructureKHR, bool> tlas[OvVK::Engine::nrFramesInFlight];

    /**
     * Constructor.
     */
    Reserved()
    {}
};


///////////////////////////////////////////////////
// BODY OF CLASS RayTracingDescriptorSetsManager //
///////////////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::RayTracingDescriptorSetsManager::RayTracingDescriptorSetsManager() :
    reserved(std::make_unique<OvVK::RayTracingDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::RayTracingDescriptorSetsManager::RayTracingDescriptorSetsManager(const std::string& name) :
    OvVK::DescriptorSetsManager(name), 
    reserved(std::make_unique<OvVK::RayTracingDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::RayTracingDescriptorSetsManager::RayTracingDescriptorSetsManager(
    RayTracingDescriptorSetsManager&& other) :
    OvVK::DescriptorSetsManager(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::RayTracingDescriptorSetsManager::~RayTracingDescriptorSetsManager()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::RayTracingDescriptorSetsManager::init()
{
    if (this->OvVK::DescriptorSetsManager::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::RayTracingDescriptorSetsManager::free()
{
    if (this->OvVK::DescriptorSetsManager::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets and descriptor sets layouts.
 * @return TF
 */
bool OVVK_API OvVK::RayTracingDescriptorSetsManager::create()
{
    // Init
    if (this->init() == false)
        return false;


    /////////////////////
    // Descriptor Pool //
    /////////////////////

    // Create descriptorPool
    std::vector<VkDescriptorPoolSize> descriptorPoolSizes(1);

    // TLAS
    descriptorPoolSizes[0].type = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    descriptorPoolSizes[0].descriptorCount = OvVK::Engine::nrFramesInFlight;

    ///////////////////////////
    // Descriptor set layout //
    ///////////////////////////

    // Create descriptorSetLayout
    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings(1);

    // Tlas 
    descriptorSetLayoutBindings[0].binding = static_cast<uint32_t>(OvVK::RayTracingDescriptorSetsManager::Binding::tlas);
    descriptorSetLayoutBindings[0].descriptorCount = 1;
    descriptorSetLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    descriptorSetLayoutBindings[0].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[0].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;


    // Create
    return this->OvVK::DescriptorSetsManager::create(descriptorPoolSizes, descriptorSetLayoutBindings,
        OvVK::Engine::nrFramesInFlight);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds an update to update the raytracing DSM.
 * @param data The reference to a structure containing the new tlas and image.
 * @return TF
 */
bool OVVK_API OvVK::RayTracingDescriptorSetsManager::addToUpdateList(const UpdateData& data)
{
    // Safety net
    if (data.targetFrame >= OvVK::Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed target frame is out of range in the ray tracing DSM with id= %d.", getId());
        return false;
    }

    if (data.tlas.has_value())
        reserved->tlas[data.targetFrame] = std::make_pair(data.tlas.value(), true);


    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::RayTracingDescriptorSetsManager::render(uint32_t value, void* data) const
{
    // Engine
    std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Frame in flight
    uint32_t frameInFlight = engine.get().getFrameInFlight();


    /////////////////////////////
    // Write to descriptor set //
    /////////////////////////////

    std::vector<VkWriteDescriptorSet> writeDescriptorSets;
    VkWriteDescriptorSetAccelerationStructureKHR rayDescriptorSetAccelerationStructure = {};


    //////////
    // TLAS //
    //////////

    if (reserved->tlas[engine.get().getFrameInFlight()].second)
    {
        rayDescriptorSetAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        rayDescriptorSetAccelerationStructure.pNext = NULL;
        rayDescriptorSetAccelerationStructure.accelerationStructureCount = 1;
        rayDescriptorSetAccelerationStructure.pAccelerationStructures =
            &reserved->tlas[engine.get().getFrameInFlight()].first;

        VkWriteDescriptorSet& tlasWriteDescriptorSet = writeDescriptorSets.emplace_back();
        tlasWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        tlasWriteDescriptorSet.pNext = &rayDescriptorSetAccelerationStructure;
        tlasWriteDescriptorSet.dstSet = getVkDescriptorSet(frameInFlight);
        tlasWriteDescriptorSet.dstBinding = static_cast<uint32_t>(RayTracingDescriptorSetsManager::Binding::tlas);
        tlasWriteDescriptorSet.dstArrayElement = 0;
        tlasWriteDescriptorSet.descriptorCount = 1;
        tlasWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
        tlasWriteDescriptorSet.pImageInfo = NULL;
        tlasWriteDescriptorSet.pBufferInfo = NULL;
        tlasWriteDescriptorSet.pTexelBufferView = NULL;

        reserved->tlas[engine.get().getFrameInFlight()].second = false;
    }


    // Update
    if (writeDescriptorSets.size() > 0)
    {
        // This can generate a race condition and undefined behavior if the descriptor set to 
        // update is in use by a command buffer that�s being executed in a queue.
        // cannot modify a descriptor set unless you know that the GPU is finished with it.
        // Update ad the beginning of the frame.
        vkUpdateDescriptorSets(engine.get().getLogicalDevice().getVkDevice(), static_cast<uint32_t>(writeDescriptorSets.size()),
            writeDescriptorSets.data(), 0, NULL);
    }

    // Done:
    return true;
}

#pragma endregion