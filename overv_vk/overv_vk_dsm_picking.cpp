/**
 * @file	overv_vk_dsm_picking.cpp
 * @brief	Descriptor set manager for picking
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Picking descriptor set manager reserved structure.
 */
struct OvVK::PickingDescriptorSetsManager::Reserved
{
    std::vector<std::pair<std::reference_wrapper<const OvVK::Buffer>, bool>> pickingBuffers;

    /**
     * Constructor.
     */
    Reserved()
    {
        std::reference_wrapper<const OvVK::Buffer> buffer = OvVK::Buffer::empty;

        // Init vectors
        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
            pickingBuffers.push_back(std::make_pair(buffer, false));
    }
};


////////////////////////////////////////////////
// BODY OF CLASS PickingDescriptorSetsManager //
////////////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::PickingDescriptorSetsManager::PickingDescriptorSetsManager() :
    reserved(std::make_unique<OvVK::PickingDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::PickingDescriptorSetsManager::PickingDescriptorSetsManager(const std::string& name) :
    OvVK::DescriptorSetsManager(name),
    reserved(std::make_unique<OvVK::PickingDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::PickingDescriptorSetsManager::PickingDescriptorSetsManager(
    PickingDescriptorSetsManager&& other) :
    OvVK::DescriptorSetsManager(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::PickingDescriptorSetsManager::~PickingDescriptorSetsManager()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::PickingDescriptorSetsManager::init()
{
    if (this->OvVK::DescriptorSetsManager::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::PickingDescriptorSetsManager::free()
{
    if (this->OvVK::DescriptorSetsManager::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets and descriptor sets layouts.
 * @return TF
 */
bool OVVK_API OvVK::PickingDescriptorSetsManager::create()
{
    // Init
    if (this->init() == false)
        return false;

    /////////////////////
    // Descriptor Pool //
    /////////////////////

    // Create descriptorPool
    std::vector<VkDescriptorPoolSize> descriptorPoolSizes(1);

    // Picking buffer
    descriptorPoolSizes[0].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorPoolSizes[0].descriptorCount = OvVK::Engine::nrFramesInFlight;


    ///////////////////////////
    // Descriptor set layout //
    ///////////////////////////

    // Create descriptorSetLayout
    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings(1);

    // Picking
    descriptorSetLayoutBindings[0].binding = static_cast<uint32_t>(OvVK::PickingDescriptorSetsManager::Binding::picking);
    descriptorSetLayoutBindings[0].descriptorCount = 1;
    descriptorSetLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorSetLayoutBindings[0].pImmutableSamplers = NULL;
    descriptorSetLayoutBindings[0].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        | VK_SHADER_STAGE_CALLABLE_BIT_KHR;


    // Create
    return this->OvVK::DescriptorSetsManager::create(descriptorPoolSizes, descriptorSetLayoutBindings,
        OvVK::Engine::nrFramesInFlight);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds an update to update the picking DSM.
 * @param data The reference to a structure containing the new buffer address.
 * @return TF
 */
bool OVVK_API OvVK::PickingDescriptorSetsManager::addToUpdateList(const UpdateData& data)
{
    // Safety net
    if (data.targetFrame >= OvVK::Engine::nrFramesInFlight)
    {
        OV_LOG_ERROR("The passed target frame is out of range in the ray tracing DSM with id= %d.", getId());
        return false;
    }

    if (data.pickingBuffer.has_value())
        reserved->pickingBuffers[data.targetFrame] = std::make_pair(data.pickingBuffer.value(), true);

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::PickingDescriptorSetsManager::render(uint32_t value, void* data) const
{
    // Engine
    std::reference_wrapper<Engine> engine = Engine::getInstance();
    // Frame in flight
    uint32_t frameInFlight = engine.get().getFrameInFlight();


    /////////////////////////////
    // Write to descriptor set //
    /////////////////////////////

    std::vector<VkWriteDescriptorSet> writeDescriptorSets;
    VkDescriptorBufferInfo pickingDescriptorBufferInfo = {};

    /////////////
    // Picking //
    /////////////

    if (reserved->pickingBuffers[frameInFlight].second)
    {
        pickingDescriptorBufferInfo.buffer =
            reserved->pickingBuffers[frameInFlight].first.get().getVkBuffer();
        pickingDescriptorBufferInfo.offset = 0;
        pickingDescriptorBufferInfo.range = VK_WHOLE_SIZE;

        VkWriteDescriptorSet& globalUniformWriteDescriptorSet = writeDescriptorSets.emplace_back();
        globalUniformWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        globalUniformWriteDescriptorSet.pNext = NULL;
        globalUniformWriteDescriptorSet.dstSet = getVkDescriptorSet(frameInFlight);
        globalUniformWriteDescriptorSet.dstBinding = static_cast<uint32_t>(PickingDescriptorSetsManager::Binding::picking);
        globalUniformWriteDescriptorSet.dstArrayElement = 0;
        globalUniformWriteDescriptorSet.descriptorCount = 1;
        globalUniformWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        globalUniformWriteDescriptorSet.pImageInfo = NULL;
        globalUniformWriteDescriptorSet.pBufferInfo = &pickingDescriptorBufferInfo;
        globalUniformWriteDescriptorSet.pTexelBufferView = NULL;

        reserved->pickingBuffers[frameInFlight].second = false;
    }


    // Update
    if (writeDescriptorSets.size() > 0)
    {
        // This can generate a race condition and undefined behavior if the descriptor set to 
        // update is in use by a command buffer that�s being executed in a queue.
        // cannot modify a descriptor set unless you know that the GPU is finished with it.
        // Update ad the beginning of the frame.
        vkUpdateDescriptorSets(engine.get().getLogicalDevice().getVkDevice(), static_cast<uint32_t>(writeDescriptorSets.size()),
            writeDescriptorSets.data(), 0, NULL);
    }

    // Done:
    return true;
}

#pragma endregion