/**
 * @file	overv_vk_image.cpp
 * @brief	Vulkan images
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// Enum
#include "magic_enum.hpp"


/////////////
// #STATIC //
/////////////

// Special values:
OvVK::Image OvVK::Image::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Image reserved structure.
 */
struct OvVK::Image::Reserved
{
    VkImage image;                              ///< Vulkan image handle
    VmaAllocation allocation;                   ///< Vma allocation handle
    VmaAllocationInfo allocationInfo;           ///< Vma allocation info

    VkFormat format;                            ///< The format of the image
    VkExtent3D extend3D;                        ///< The width, height and depth of the image
    uint32_t nrOfLevels;                        ///< The number of levels of an image
    uint32_t nrOfLayers;                        ///< The number of layers(sides for cubemap) of an image
    VkImageLayout imageLayout;                  ///< The layout of the image

    std::vector<OvVK::ImageView> imageViews;///< Vulkan ImageViews handle

    VkFence lockSyncObj;                        ///< Vulkan sync object for lock
    VkSemaphore statusSyncObj;                  ///< Vulkan sync object for status
    std::optional<uint32_t> queueFamilyIndex;   ///< Queue family index owner of the image

    /**
     * Constructor.
     */
    Reserved() : image{ VK_NULL_HANDLE },
        allocation{ nullptr },
        allocationInfo{ {} },
        format{ VK_FORMAT_UNDEFINED },
        extend3D{ {} },
        nrOfLevels{ 0 },
        nrOfLayers{ 0 },
        imageLayout{ VK_IMAGE_LAYOUT_UNDEFINED },
        lockSyncObj{ VK_NULL_HANDLE },
        statusSyncObj{ VK_NULL_HANDLE },
        queueFamilyIndex{ std::nullopt }
    {}
};


/////////////////////////
// BODY OF CLASS Image //
/////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Image::Image() : reserved(std::make_unique<OvVK::Image::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Image::Image(const std::string& name) : Ov::Object(name),
    reserved(std::make_unique<OvVK::Image::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Image::Image(Image&& other) noexcept : Ov::Object(std::move(other)), Ov::Managed(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Image::~Image()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize image
 * @return TF
 */
bool OVVK_API OvVK::Image::init()
{
    // Default initialization method
    if (this->Ov::Managed::init() == false)
        return false;


    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Free imageViews
    reserved->imageViews.clear();

    // Free image
    if (reserved->image != VK_NULL_HANDLE)
    {
        VmaAllocator allocator = Engine::getInstance().getLogicalDevice().getVmaAllocator();

        vmaDestroyImage(allocator, reserved->image, reserved->allocation);
        reserved->image = VK_NULL_HANDLE;
        reserved->allocation = nullptr;
        reserved->allocationInfo = {};

        reserved->format = VK_FORMAT_UNDEFINED;
        reserved->extend3D = {};
        reserved->nrOfLevels = 0;
        reserved->nrOfLayers = 0;
        reserved->imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        reserved->queueFamilyIndex = std::nullopt;
    }

    // Destroy sync obj for status
    if (reserved->statusSyncObj != VK_NULL_HANDLE)
    {
        // Destroy the timeline semaphore object
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, NULL);
        reserved->statusSyncObj = VK_NULL_HANDLE;
    }

    // Create Status sync obj.
    VkSemaphoreTypeCreateInfo syncStatusObjTypeCreateInfo;
    syncStatusObjTypeCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    syncStatusObjTypeCreateInfo.pNext = NULL;
    syncStatusObjTypeCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
    syncStatusObjTypeCreateInfo.initialValue = OvVK::Image::defaultValueStatusSyncObj;

    VkSemaphoreCreateInfo syncStatusObjCreateInfo;
    syncStatusObjCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    syncStatusObjCreateInfo.pNext = &syncStatusObjTypeCreateInfo;
    syncStatusObjCreateInfo.flags = 0;

    if (vkCreateSemaphore(logicalDevice, &syncStatusObjCreateInfo,
        NULL, &reserved->statusSyncObj) != VK_SUCCESS) {
        OV_LOG_ERROR("Failed to create synchronization status object for image with id= %d", getId());
        this->free();
        return false;
    }

    // Reset sync obj for lock (not the owner of the object)
    reserved->lockSyncObj = VK_NULL_HANDLE;


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free image.
 * @return TF
 */
bool OVVK_API OvVK::Image::free()
{
    if (this->Ov::Managed::free() == false)
        return false;


    // Logical device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Free imageViews
    reserved->imageViews.clear();

    // Free image
    if (reserved->image != VK_NULL_HANDLE)
    {
        VmaAllocator allocator = Engine::getInstance().getLogicalDevice().getVmaAllocator();

        vmaDestroyImage(allocator, reserved->image, reserved->allocation);
        reserved->image = VK_NULL_HANDLE;
        reserved->allocation = nullptr;
        reserved->allocationInfo = {};

        reserved->format = VK_FORMAT_UNDEFINED;
        reserved->extend3D = {};
        reserved->nrOfLevels = 0;
        reserved->nrOfLayers = 0;
        reserved->imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        reserved->queueFamilyIndex = std::nullopt;
    }

    // Destroy sync obj for status
    if (reserved->statusSyncObj != VK_NULL_HANDLE)
    {
        // Destroy the timeline semaphore object
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, NULL);
        reserved->statusSyncObj = VK_NULL_HANDLE;
    }

    // Reset sync obj for lock (not the owner of the object)
    reserved->lockSyncObj = VK_NULL_HANDLE;


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncObj

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the image is locked.
 * @return TF
 */
bool OVVK_API OvVK::Image::isLock() const
{
    // Safety net:
    if (reserved->lockSyncObj == VK_NULL_HANDLE)
        return false;

    // vkGetFenceStatus == VK_SUCCESS => The fence specified by fence is signaled.
    if (vkGetFenceStatus(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->lockSyncObj) != VK_SUCCESS)
        return true;

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the image is onwed by a queue or not.
 * @return TF
 */
bool OVVK_API OvVK::Image::isOwned() const
{
    return reserved->queueFamilyIndex.has_value();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an image by allocating the required size throught VMA.
 * @param vkImageCreateInfo structure specifying the parameters to create a new image in Vulkan.
 * @param vmaAllocationCreateInfo structure specifying the parameters to create a new allocation in VMA.
 * @return TF
 */
bool OVVK_API OvVK::Image::create(VkImageCreateInfo vkImageCreateInfo,
    VmaAllocationCreateInfo vmaAllocationCreateInfo)
{
    // Engine
    std::reference_wrapper<const Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Physical device
    std::reference_wrapper<const OvVK::PhysicalDevice> physicalDevice = logicalDevice.get().getPhysicalDevice();

    ////////////////////////////
    // Check image properties //
    ////////////////////////////

    VkPhysicalDeviceImageFormatInfo2 imageFormatInfo = {};
    imageFormatInfo.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2;
    imageFormatInfo.pNext = VK_NULL_HANDLE;
    imageFormatInfo.format = vkImageCreateInfo.format;
    imageFormatInfo.type = vkImageCreateInfo.imageType;
    imageFormatInfo.tiling = vkImageCreateInfo.tiling;
    imageFormatInfo.usage = vkImageCreateInfo.usage;
    imageFormatInfo.flags = vkImageCreateInfo.flags;

    std::optional<VkImageFormatProperties2> imageFormatProperties = 
        physicalDevice.get().getImageFormatProperties(&imageFormatInfo);

    // If no value the format not supported.
    if (imageFormatProperties.has_value() == false)
        return false;

    VkImageFormatProperties formatProperties = imageFormatProperties.value().imageFormatProperties;

    // Safety net:
    if (vkImageCreateInfo.extent.height == 0 ||
        vkImageCreateInfo.extent.height > formatProperties.maxExtent.height)
    {
        OV_LOG_ERROR("Height is 0 or exceeds the max bound in image with id= %d.", getId());
        return false;
    }

    if (vkImageCreateInfo.extent.width == 0 ||
        vkImageCreateInfo.extent.width > formatProperties.maxExtent.width)
    {
        OV_LOG_ERROR("Width is 0 or exceeds the max bound in image with id= %d.", getId());
        return false;
    }

    if (vkImageCreateInfo.extent.depth == 0 ||
        vkImageCreateInfo.extent.depth > formatProperties.maxExtent.depth)
    {
        OV_LOG_ERROR("Depth is 0 or exceeds the max bound in image with id= %d.", getId());
        return false;
    }

    if (vkImageCreateInfo.mipLevels == 0 ||
        vkImageCreateInfo.mipLevels > formatProperties.maxMipLevels)
    {
        OV_LOG_ERROR("Levels are 0 or exceed the max bound in image with id= %d.", getId());
        return false;
    }

    if (vkImageCreateInfo.arrayLayers == 0 ||
        vkImageCreateInfo.arrayLayers > formatProperties.maxArrayLayers)
    {
        OV_LOG_INFO("ArrayLayers are 0 or exceed the max bound in image with id= %d.", getId());
        return false;
    }

    if ((formatProperties.sampleCounts & vkImageCreateInfo.samples) != vkImageCreateInfo.samples)
    {
        OV_LOG_ERROR("Number of samples set for the image exceeds the max capacity in image with id= %d.", getId());
        return false;
    }

    if (reserved->image != VK_NULL_HANDLE && isLock()) {
        OV_LOG_ERROR("Can't recreate the image with id= %d because it is locked.", getId());
        return false;
    }


    //////////////////
    // Create Image //
    //////////////////

    if (!this->init())
        return false;

    // Create Image
    if(vkCreateImage(logicalDevice.get().getVkDevice(), &vkImageCreateInfo, nullptr, &reserved->image) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create image with id= %d.", getId());
        this->free();
        return false;
    }

    // Query image memeory requirements to check if the memory needed doesn't exceed the maximum.
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(logicalDevice.get().getVkDevice(), reserved->image, &memRequirements);

    if (memRequirements.size >= formatProperties.maxResourceSize)
    {
        OV_LOG_INFO("The size (in bytes) of the image with id= %d, exceeds the max bound.", getId());
        this->free();
        return false;
    }

    // Allocate memory
    if(vmaAllocateMemoryForImage(logicalDevice.get().getVmaAllocator(), reserved->image, &vmaAllocationCreateInfo,
        &reserved->allocation, &reserved->allocationInfo) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to allocate memory for image with id= %d.", getId());
        this->free();
        return false;
    }

    if (vmaBindImageMemory2(logicalDevice.get().getVmaAllocator(), reserved->allocation, 0, reserved->image, NULL)
        != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to bind allocation and image together for image with id= %d.", getId());
        this->free();
        return false;
    }

    // Set info
    reserved->format = vkImageCreateInfo.format;
    reserved->extend3D = vkImageCreateInfo.extent;
    reserved->nrOfLevels = vkImageCreateInfo.mipLevels;
    reserved->nrOfLayers = vkImageCreateInfo.arrayLayers;
    reserved->imageLayout = vkImageCreateInfo.initialLayout;

    // Done
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an imageView to access the image in the shaders.
 * @param vkImageViewCreateInfo The structure specifying the parameters to create a new imageView in Vulkan.
 * @return Pointer to an image view
 */
const OvVK::ImageView OVVK_API& OvVK::Image::createImageView(VkImageViewCreateInfo vkImageViewCreateInfo)
{
    // Safety net:
    if (reserved->image == VK_NULL_HANDLE) 
    {
        OV_LOG_ERROR("Image with id= %d not created!", getId());
        return OvVK::ImageView::empty;
    }

    // Fill image relative data to build the imageView
    vkImageViewCreateInfo.image = reserved->image;
    vkImageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    vkImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    vkImageViewCreateInfo.subresourceRange.levelCount = reserved->nrOfLevels;
    vkImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    vkImageViewCreateInfo.subresourceRange.layerCount = reserved->nrOfLayers;

    OvVK::ImageView imageView;
    if (imageView.create(*this, vkImageViewCreateInfo) == false) {
        OV_LOG_ERROR("Failed to create image view for image with id= %d", getId());
        return OvVK::ImageView::empty;
    }

    reserved->imageViews.push_back(std::move(imageView));

    // Done:
    return reserved->imageViews.back();
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the Vulkan image handle.
 * @return Pointer to VKImage.
 */
VkImage OVVK_API OvVK::Image::getVkImage() const
{
    return reserved->image;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the VMA allocation handle.
 * @return Pointer to VmaAllocation.
 */
VmaAllocation OVVK_API OvVK::Image::getVmaAllocation() const
{
    return reserved->allocation;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the VMA allocation info.
 * @return Struct containing info about the allocation.
 */
VmaAllocationInfo OVVK_API OvVK::Image::getVmaAllocationInfo() const
{
    return reserved->allocationInfo;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the Vulkan format of the image.
 * @return The format of the image.
 */
VkFormat OVVK_API OvVK::Image::getFormat() const 
{
    return reserved->format;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the width, height and depth of the image in a single struct.
 * @return Struct with width, height and depth of the image.
 */
VkExtent3D OVVK_API OvVK::Image::getExtend3D() const 
{
    return reserved->extend3D;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the number of levels that the image contains.
 * @return The number of levels of the image.
 */
uint32_t OVVK_API OvVK::Image::getNrOfLevels() const
{
    return reserved->nrOfLevels;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the number of layers(sides for cube maps) that the image contains.
 * @return The number of layer of the image.
 */
uint32_t OVVK_API OvVK::Image::getNrOfLayers() const
{
    return reserved->nrOfLayers;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the layout of the image.
 * @return The layout of the image.
 */
VkImageLayout OVVK_API OvVK::Image::getImageLayout() const 
{
    return reserved->imageLayout;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the lock sync obj.
 * @return The linked fence to check the lock status.
 */
VkFence OVVK_API OvVK::Image::getLockSyncObj() const
{
    return reserved->lockSyncObj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the status sync obj.
 * @return The semaphore used to check in which status the image is.
 */
VkSemaphore OVVK_API OvVK::Image::getStatusSyncObj() const
{
    return reserved->statusSyncObj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the queue family index of the queue owninig the image if one.
 * @return Std optional object containing the index of the queue family owning the image.
 * If no queue own the image the value will be std::nullopt.
 */
std::optional<uint32_t> OVVK_API OvVK::Image::getQueueFamilyIndex() const
{
    return reserved->queueFamilyIndex;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the number of imageView created for this image.
 * @return The number of image view create to access the image.
 */
uint32_t OVVK_API OvVK::Image::getNrOfVkImageView() const 
{
    return reserved->imageViews.size();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return a reference to the vector containing all the created imageViews for this image.
 * @return Vector containing the image view created.
 */
const std::vector<OvVK::ImageView> OVVK_API& OvVK::Image::getVkImageViews() const
{
    return reserved->imageViews;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the Vulkan imageView handle.
 * @param pos The position in the vector of the imageview to retrive.
 * @return Image view to access the image.
 */
const OvVK::ImageView OVVK_API& OvVK::Image::getVkImageView(uint32_t pos) const
{
    // Safety net:
    if (pos >= reserved->imageViews.size()) 
    {
        OV_LOG_ERROR("The position to access an imageView is out the range in image with id= %d.", getId());
        return OvVK::ImageView::empty;
    }

    return reserved->imageViews[pos];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the image layout.
 * @param imageLayout The current Vulkan image layout of the image.
 * @return TF
 */
bool OVVK_API OvVK::Image::setImageLayout(VkImageLayout imageLayout) 
{
    // Safety net:
    if (reserved->image == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Image not created!");
        return false;
    }

    reserved->imageLayout = imageLayout;

    //Done
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the queue family index of the queue owning the image.
 * @param queueFamilyIndex The index of the queue owning the image.
 * @return TF
 */
bool OVVK_API OvVK::Image::setQueueFamilyIndex(uint32_t queueFamilyIndex)
{
    // Safety net:
    if (reserved->image == VK_NULL_HANDLE) {
        OV_LOG_ERROR("Image not created!");
        return false;
    }
    if (isLock()) {
        OV_LOG_ERROR("Can't set the queue family for the image with id= %d because is locked", getId());
        return false;
    }

    reserved->queueFamilyIndex = queueFamilyIndex;

    //Done
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the status of the image.
 * @param value The uint value rapresenting the status the image is in.
 * @return TF
 */
bool OVVK_API OvVK::Image::setStatusSyncObjValue(uint64_t value)
{
    if (reserved->statusSyncObj == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Sync obj not created!");
        return false;
    }

    // Logicel device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();

    // Retrievers current sempahore value
    uint64_t currentValue;
    vkGetSemaphoreCounterValue(logicalDevice, reserved->statusSyncObj, &currentValue);

    if (currentValue > value) {
        // Destroy semaphore because can't decrease the value
        vkDestroySemaphore(logicalDevice, reserved->statusSyncObj, nullptr);
        reserved->statusSyncObj = VK_NULL_HANDLE;

        // Create new timeline semaphore and delete previous one.
        VkSemaphoreTypeCreateInfo timelineCreateInfo;
        timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        timelineCreateInfo.pNext = NULL;
        timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        timelineCreateInfo.initialValue = value;

        VkSemaphoreCreateInfo semaphoreInfo;
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreInfo.pNext = &timelineCreateInfo;
        semaphoreInfo.flags = 0;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &reserved->statusSyncObj) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status synchronization object for Image with id= %d", getId());
            return false;
        }
    }
    else if (currentValue < value) {
        VkSemaphoreSignalInfo signalInfo;
        signalInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
        signalInfo.pNext = NULL;
        signalInfo.semaphore = reserved->statusSyncObj;
        signalInfo.value = value;

        if (vkSignalSemaphore(logicalDevice, &signalInfo) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Failed to signal status synchronization object for Image with id= %d", getId());
            return false;
        }
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the sync obj for the lock.
 * @param fence The obj to check to know the lock status.
 * @return TF
 */
bool OVVK_API OvVK::Image::setLockSyncObj(VkFence fence)
{
    // Safety net:
    if (reserved->image == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Image not created!");
        return false;
    }

    reserved->lockSyncObj = fence;

    //Done:
    return true;
}

#pragma endregion
