/**
 * @file	overv_vk_geometry.cpp
 * @brief	Geometry
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::Geometry OvVK::Geometry::empty("[empty]");


/////////////////////////
// RESERVED STRUCTURES //
/////////////////////////

/**
 * @brief Geometry class reserved structure.
 */
struct OvVK::Geometry::Reserved
{
    // Buffers:
    OvVK::Buffer vertexBuffer;  ///< Device Buffer obj containing the vertex data.
    OvVK::Buffer faceBuffer;    ///< Device Buffer obj containing the face (index) data.

    /**
     * Constructor
     */
    Reserved()
    {}
};


////////////////////////////
// BODY OF CLASS Geometry //
////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Geometry::Geometry() : reserved(std::make_unique<OvVK::Geometry::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::Geometry::Geometry(const std::string& name) : Ov::Geometry(name),
    reserved(std::make_unique<OvVK::Geometry::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Geometry::Geometry(Geometry&& other) noexcept : Ov::Geometry(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Geometry::~Geometry()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes an Vulkan geometry.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::init()
{
    
    if (this->Ov::Managed::init() == false)
        return false;


    // Free buffer
    if (reserved->faceBuffer.free() == false)
        return false;
    if (reserved->vertexBuffer.free() == false)
        return false;


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases an Vulkan geometry.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::free()
{

    if (this->Ov::Managed::free() == false)
        return false;


    // Free buffer
    if (reserved->faceBuffer.free() == false)
        return false;
    if (reserved->vertexBuffer.free() == false)
        return false;


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the geometry is locked.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::isLock() const
{
    return reserved->vertexBuffer.isLock() || reserved->faceBuffer.isLock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the geometry is owned from a queue family.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::isOwned() const
{
    return reserved->vertexBuffer.getQueueFamilyIndex().has_value() ||
        reserved->faceBuffer.getQueueFamilyIndex().has_value();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up the buffers to update the data in the GPU memory.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::setUp()
{
    // Safety net:
    if (isLock()) 
    {
        OV_LOG_ERROR("Geometry with id= %d is lock.", getId());
        return false;
    }

    if (!this->init())
        return false;


    // Logical Device
    VkDevice logicalDevice = Engine::getInstance().getLogicalDevice().getVkDevice();


    // Calculate the size of the buffers
    // Calculate size of the vertex buffer in bytes
    uint64_t vertexSize = this->getNrOfVertices() * sizeof(Ov::Geometry::VertexData);
    // Calculate size of the face buffer in bytes
    uint64_t faceSize = this->getNrOfFaces() * sizeof(Ov::Geometry::FaceData);


    ///////////////////
    // Vetrex Buffer //
    ///////////////////

#pragma region VertexBuffer

    // Create info buffer
    VkBufferCreateInfo vkVertexBufferCreateInfo{};
    vkVertexBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vkVertexBufferCreateInfo.size = vertexSize;
    // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable
    //      for use as a read-only input to an acceleration structure build.
    // VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
    //      of a transfer command.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device
    //      address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    // VK_BUFFER_USAGE_VERTEX_BUFFER_BIT specifies that the buffer is suitable for passing as an element of
    //      the pBuffers array to vkCmdBindVertexBuffers.
    vkVertexBufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
        | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT
        | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    vkVertexBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vkVertexBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    vkVertexBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaVertexBufferAllocationCreateInfo{};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    vmaVertexBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaVertexBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Create buffer to contains vertex data
    if (reserved->vertexBuffer.create(vkVertexBufferCreateInfo, vmaVertexBufferAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create vertex buffer for Geometry with id= %d.", getId());
        this->free();
        return false;
    }

#pragma endregion


    /////////////////
    // Face Buffer //
    /////////////////

#pragma region FaceBuffer

    // Create info buffer
    VkBufferCreateInfo vkIndexBufferCreateInfo{};
    vkIndexBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vkIndexBufferCreateInfo.size = faceSize;
    // VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR => specifies that the buffer is suitable
    //      for use as a read-only input to an acceleration structure build.
    // VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
    //      of a transfer command.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer
    //      device address via vkGetBufferDeviceAddress and use that address to access the buffer�s
    //      memory from a shader.
    // VK_BUFFER_USAGE_VERTEX_BUFFER_BIT specifies that the buffer is suitable for passing as an element of
    //      the pBuffers array to vkCmdBindVertexBuffers.
    vkIndexBufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
        | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT
        | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    vkIndexBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vkIndexBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    vkIndexBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaIndexBufferAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible 
    // free range for the allocation
    // to minimize memory usage and fragmentation, possibly at the expense of allocation time.
    vmaIndexBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaIndexBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Create buffer to contains face data
    if (reserved->faceBuffer.create(vkVertexBufferCreateInfo, vmaVertexBufferAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create face buffer for Geometry with id= %d.", getId());
        this->free();
        return false;
    }

#pragma endregion

    // Set the status of the geometry to ready to upload.
    if (setStatusSyncObjValue(OvVK::Geometry::readyToUploadValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail set the status value in the geometry with id= %d.", getId());
        this->free();
        return false;
    }

    setDirty(true);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Upload the geometry in the loader to load it in the engine. This method is used when loading ovo files.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::upload() 
{
    // Set Up
    if (setUp() == false)
        return false;

    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();

    // Calculate size of the vertex buffer in bytes
    uint64_t vertexSize = getNrOfVertices() * sizeof(Ov::Geometry::VertexData);
    // Calculate size of the face buffer in bytes
    uint64_t faceSize = getNrOfFaces() * sizeof(Ov::Geometry::FaceData);


    ////////////////////
    // Staging Buffer //
    ////////////////////

    OvVK::Buffer geometryStagingBuffer;

	// Create info buffer
	VkBufferCreateInfo vkStagingBufferCreateInfo = {};
	vkStagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	vkStagingBufferCreateInfo.size = vertexSize + faceSize;
	// VK_BUFFER_USAGE_TRANSFER_SRC_BIT => specifies that the buffer can be used as the source of a transfer command 
	vkStagingBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	// Set the sharing modo to exclusive allow only one queue family to own the buffer.
	// Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
	// We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
	vkStagingBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	vkStagingBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
	vkStagingBufferCreateInfo.queueFamilyIndexCount = 0;

	// Create allocation info
	VmaAllocationCreateInfo vmaStagingBufferAllocationCreateInfo = {};
	// VMA_ALLOCATION_CREATE_MAPPED_BIT => This is useful if you need an allocation that is efficient to
	//      use on GPU (DEVICE_LOCAL) and still want to map it directly if possible on platforms that support it.
	// VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
	//      free range for the allocation to minimize memory usage and fragmentation, possibly at the expense
	//      of allocation time.
	vmaStagingBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT |
		VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
	// VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT => bit specifies that memory allocated with this type can be
	//      mapped for host access using vkMapMemory.
	vmaStagingBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

	if (geometryStagingBuffer.create(vkStagingBufferCreateInfo, vmaStagingBufferAllocationCreateInfo) == false)
	{
		OV_LOG_ERROR("Fail to create staging buffer for the geometry with id= %d.", getId());
		this->free();
		return false;
	}

    // pointer to staging buffer memeory
    uint8_t* currentPosition = ((uint8_t*)geometryStagingBuffer.getVmaAllocationInfo().pMappedData);
    // Copy vertices
    memcpy(currentPosition, getVertexDataPtr(), vertexSize);
    // Get pointer to the memory where copy the data
    currentPosition += vertexSize;
    // Copy faces
    memcpy(currentPosition, getFaceDataPtr(), faceSize);


    //////////////////////////
    // SetUp command buffer //
    //////////////////////////

    // Get command pool to allocate the command buffer to transfer the geometry data to the GPU.
    std::reference_wrapper<OvVK::CommandPool> transferCommandPool = engine.get().getPrimaryTransferCmdPool();
    std::reference_wrapper<OvVK::CommandBuffer> transferCommandBuffer = transferCommandPool.get().allocateCommandBuffer();

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
    //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (vkBeginCommandBuffer(transferCommandBuffer.get().getVkCommandBuffer(), &beginInfo) != VK_SUCCESS ||
        transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to begin or change status of transfer command buffer with id= %d for the geometry with id= %d.",
            transferCommandBuffer.get().getId(), getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }

    // Get commandBuffer
    VkCommandBuffer vkCommandBuffer = transferCommandBuffer.get().getVkCommandBuffer();

    // Copy info
    VkBufferCopy bufferCopy = {};
    bufferCopy.srcOffset = 0;
    bufferCopy.size = vertexSize;

    // Register copy command for vertex
    vkCmdCopyBuffer(vkCommandBuffer, geometryStagingBuffer.getVkBuffer(),
        reserved->vertexBuffer.getVkBuffer(), 1, &bufferCopy);

    bufferCopy.srcOffset = vertexSize;
    bufferCopy.size = faceSize;

    // Register copy command for faces
    vkCmdCopyBuffer(vkCommandBuffer, geometryStagingBuffer.getVkBuffer(),
        reserved->faceBuffer.getVkBuffer(), 1, &bufferCopy);

    // Close command buffer registering
    if (vkEndCommandBuffer(transferCommandBuffer.get().getVkCommandBuffer()) != VK_SUCCESS ||
        transferCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to end or change status of transfer command buffer with id=% for the geometry with id= %d.",
            transferCommandBuffer.get().getId(), getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }


    ////////////
    // Submit //
    ////////////

    // Submit
    VkSubmitInfo2 submitInfo2 = {};
    submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    submitInfo2.pNext = VK_NULL_HANDLE;
    submitInfo2.flags = 0x00000000;

    std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(2);
    VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfo.value = OvVK::Geometry::uploadingValueStatusSyncObj;
    vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfo.deviceIndex = 0;

    vkSemaphoreSubmitInfo.semaphore = reserved->vertexBuffer.getStatusSyncObj();
    pWaitSemaphoreInfos[0] = vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.semaphore = reserved->faceBuffer.getStatusSyncObj();
    pWaitSemaphoreInfos[1] = vkSemaphoreSubmitInfo;

    submitInfo2.waitSemaphoreInfoCount = 2;
    submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = transferCommandBuffer.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;

    submitInfo2.commandBufferInfoCount = 1;
    submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

    std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(2);
    vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
    vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
    vkSemaphoreSubmitInfo.value = OvVK::Geometry::readyValueStatusSyncObj;
    vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_COPY_BIT;
    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
    vkSemaphoreSubmitInfo.deviceIndex = 0;

    vkSemaphoreSubmitInfo.semaphore = reserved->vertexBuffer.getStatusSyncObj();
    pSignalSemaphoreInfos[0] = vkSemaphoreSubmitInfo;
    vkSemaphoreSubmitInfo.semaphore = reserved->faceBuffer.getStatusSyncObj();
    pSignalSemaphoreInfos[1] = vkSemaphoreSubmitInfo;

    submitInfo2.signalSemaphoreInfoCount = 2;
    submitInfo2.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();


    // Create sync CPU obj to check the status of the command buffer execution.
    VkFenceCreateInfo vkFenceCreateInfo = {};
    vkFenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    vkFenceCreateInfo.pNext = VK_NULL_HANDLE;
    vkFenceCreateInfo.flags = 0x00000000;

    VkFence syncObjDefault;
    if (vkCreateFence(logicalDevice.get().getVkDevice(), &vkFenceCreateInfo, nullptr, &syncObjDefault) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create sync obj to trasnfer the data of the geometry with id= %d.", getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }

    // Set up the default texture status and lock obj for the transfer.
    if (setStatusSyncObjValue(OvVK::Geometry::uploadingValueStatusSyncObj) == false ||
        setLockSyncObj(syncObjDefault) == false)
    {
        OV_LOG_ERROR("Fail to change status or set the lock to the geometry with id= %d for trasfering data from staging buffer.",
            getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }

    // Submit
    if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(transferCommandBuffer.get().getQueueFamilyIndex().value()),
        1, &submitInfo2, syncObjDefault) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit transfer command buffer to trasfering data from staging buffer to the geometry's buffers with id= %d.",
            getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }


    // Wait that the transfer command buffer finish.
    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &syncObjDefault, VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait until the geometry with id= %d is ready.", getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }

    // Destroy the sync CPU obj.
    vkDestroyFence(logicalDevice.get().getVkDevice(), syncObjDefault, nullptr);

    if (setLockSyncObj(VK_NULL_HANDLE) == false || 
        setQueueFamilyIndex(transferCommandPool.get().getQueueFamilyIndex().value()) == false)
    {
        OV_LOG_INFO("Fail to configure the geometry with id= %d after staging buffer copy.", getId());
        transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get());
        this->free();
        return false;
    }

    if (transferCommandPool.get().freeCommandBuffer(transferCommandBuffer.get()) == false)
    {
        OV_LOG_INFO("Fail to free command buffer used to trasfer staging buffer to the geometry's buffers with id= %d", getId());
        return false;
    }


    //Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the reference to the OvVK buffer containing the vertex data of the geometry.
 * @return The reference to the OvVK buffer containing the vertex data of the geometry.
 */
const OvVK::Buffer OVVK_API& OvVK::Geometry::getVertexBuffer() const
{
    return reserved->vertexBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the reference to the OvVK buffer containing the face data of the geometry.
 * @return The reference to the OvVK buffer containing the face data of the geometry.
 */
const OvVK::Buffer OVVK_API& OvVK::Geometry::getFaceBuffer() const
{
    return reserved->faceBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the queue family index of the queue owning the geometry's buffers.
 * @param queueFamilyIndex Index of the queue owning the geometry's buffers.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::setQueueFamilyIndex(uint32_t queueFamilyIndex)
{
    return reserved->vertexBuffer.setQueueFamilyIndex(queueFamilyIndex) &&
        reserved->faceBuffer.setQueueFamilyIndex(queueFamilyIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the geometry.
 * @param value The uint value rapresenting the status the geometry is in.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::setStatusSyncObjValue(uint64_t value)
{
    return reserved->faceBuffer.setStatusSyncObjValue(value) &&
        reserved->vertexBuffer.setStatusSyncObjValue(value);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sync obj for the lock.
 * @param fence Vulkan object to check to know whether the geometry is locked or not.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::setLockSyncObj(VkFence fence)
{
    return reserved->vertexBuffer.setLockSyncObj(fence) &&
        reserved->faceBuffer.setLockSyncObj(fence);
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::Geometry::render(uint32_t value, void* data) const
{
    // Delete staging buffer if the data are updated
    if (this->Ov::Object::isDirty())
        setDirty(false);

    // Done:
    return true;
}

#pragma endregion

#pragma region Shader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds to the OvVK shader preprocessing the string to include the info to access the geometry data in a shader.
 * @return TF
 */
bool OVVK_API OvVK::Geometry::addShaderPreproc()
{

    std::string geometryPreproc = R"(
#ifndef OVVULKAN_GEOMETRY_INCLUDED
#define OVVULKAN_GEOMETRY_INCLUDED

struct VertexData
{
    vec3 vertex;
    uint32_t normal;
    uint32_t uv;
    uint32_t tangent; 
};

layout(buffer_reference, scalar) buffer Vertices {VertexData vertices[]; }; // Vertex of an object

struct FaceData
{
    uint32_t a;
    uint32_t b;
    uint32_t c;
};

layout(buffer_reference, scalar) buffer Faces {FaceData faces[]; }; // Triangle faces

#endif
)";

    return OvVK::Shader::preprocessor.set("include Geometry", geometryPreproc);
}

#pragma endregion