#pragma once
/**
 * @file    overv_vk_texture2d.h
 * @brief	Texture 2d
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Texture 2d
  */
class OVVK_API Texture2D final : public OvVK::Texture
{
//////////
public: //
//////////

	// Special values:
	static Texture2D empty;

	// Const/dest:
	Texture2D();		
	Texture2D(Texture2D&& other);
	Texture2D(Texture2D const&) = delete;
	~Texture2D();

	// Operators
	Texture2D& operator=(const Texture2D&) = delete;
	Texture2D& operator=(Texture2D&&) = delete;

	// Default texture:
	static const Texture2D& getDefault();

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	bool load(const Ov::Bitmap& bitmap, bool isLinearEncoded = true, bool holdReferenceBitmap = false) override;
	bool setUp(const Ov::Bitmap& bitmap, bool isLinearEncoded = true, bool holdReferenceBitmap = false) override;
	
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool create(uint32_t width, uint32_t height, uint32_t nrOfLevels, uint32_t nrOfLayers,
		Format format, bool isLinearEncoded = true, VkImageViewType imageViewType = VK_IMAGE_VIEW_TYPE_2D,
		VkImageCreateFlags flagsImage = 0x00000000, VkImageViewCreateFlags flagsImageView = 0x00000000) override;
#endif

	// Get/set:   
	uint32_t getSizeZ() const = delete;

	// Rendering methods:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Texture2D(const std::string& name);

	// Get/set:
	void setSizeZ(uint32_t sizeZ) = delete;
};