/**
 * @file	overv_vk_dsm_whitted_rt_with_pbr_lighting.h
 * @brief	Descriptor set manager for Whitted ray tracing with PBR
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling a descriptor set manager for lighting.
  */
class OVVK_API WhittedRTwithPBRDescriptorSetsManager final : public OvVK::DescriptorSetsManager
{
//////////
public: //
//////////

	/**
	 * @brief Definition of binding points.
	 */
	enum class Binding : uint32_t
	{
		outputImage = 0,
		aoImage = 1
	};

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Struct used to update
	 */
	struct UpdateData
	{
		uint32_t targetFrame;
		std::optional<VkDescriptorImageInfo> descriptorOutputImageInfo;
		std::optional<VkDescriptorImageInfo> descriptorAOImageInfo;

		UpdateData() : targetFrame{ 0 },
			descriptorOutputImageInfo{ std::nullopt },
			descriptorAOImageInfo{ std::nullopt }
		{}
	};
#endif

	// Const/dest:
	WhittedRTwithPBRDescriptorSetsManager();
	WhittedRTwithPBRDescriptorSetsManager(WhittedRTwithPBRDescriptorSetsManager&& other);
	WhittedRTwithPBRDescriptorSetsManager(WhittedRTwithPBRDescriptorSetsManager const&) = delete;
	virtual ~WhittedRTwithPBRDescriptorSetsManager();

	// Operators
	WhittedRTwithPBRDescriptorSetsManager& operator=(const WhittedRTwithPBRDescriptorSetsManager&) = delete;
	WhittedRTwithPBRDescriptorSetsManager& operator=(WhittedRTwithPBRDescriptorSetsManager&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	virtual bool create() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool addToUpdateList(const UpdateData& data);
#endif

	// Rendering:
	bool render(uint32_t value = 0, void* data = nullptr) const override;


///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	WhittedRTwithPBRDescriptorSetsManager(const std::string& name);
};
