/**
 * @file	overv_vk_bitmap.cpp
 * @brief	Basic generic bitmap properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// Ktx include
#include "ktx.h"

// Magic enum
#include "magic_enum.hpp"

// C/C++
#include <functional>


////////////
// STATIC //
////////////

// Special values:
OvVK::Bitmap OvVK::Bitmap::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Bitmap reserved structure.
 */
struct OvVK::Bitmap::Reserved
{
	VkFormat vulkanFormat;

	/**
	 * Constructor.
	 */
	Reserved()
	{}
};


//////////////////////////
// BODY OF CLASS Bitmap //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Bitmap::Bitmap() : reserved(std::make_unique<OvVK::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Bitmap::Bitmap(const std::string& name) : Ov::Image(name),
	reserved(std::make_unique<OvVK::Bitmap::Reserved>())
{
	OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Bitmap::Bitmap(Bitmap&& other) noexcept : Ov::Image(std::move(other)),
reserved(std::move(other.reserved))
{
	OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Bitmap::~Bitmap()
{
	OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Loader

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Load image from a .ktx file.
 * @param filename ktx file name
 * @param format to load
 * @return TF
 */
bool OVVK_API OvVK::Bitmap::loadKTX(const std::string& filename, Format loadFormat)
{
	// Safety net:
	if (filename.empty())
	{
		OV_LOG_ERROR("Invalid params");
		return false;
	}

	// Clear
	if (!Ov::Bitmap::clear())
	{
		OV_LOG_ERROR("Fail to clear bitmap with id= %d.", getId());
		return false;
	}

	ktxTexture2* texture;
	ktx_error_code_e result;

	result = ktxTexture2_CreateFromNamedFile(filename.c_str(), KTX_TEXTURE_CREATE_NO_FLAGS,
		&texture);

	if (result != KTX_SUCCESS)
	{
		OV_LOG_ERROR("Fail to load KTX file %s. The error is %s", filename.c_str(),
			magic_enum::enum_name(result));
		return false;
	}

	// Transcode if it is needed
	if (ktxTexture2_NeedsTranscoding(texture))
	{
		ktx_transcode_fmt_e transcodeFormat;
		switch (loadFormat)
		{
		case Ov::Bitmap::Format::r8g8b8:
			transcodeFormat = KTX_TTF_ETC1_RGB;
			break;
		case Ov::Bitmap::Format::r8g8b8a8:
			transcodeFormat = KTX_TTF_ETC2_RGBA;
			break;
		case Ov::Bitmap::Format::r8g8b8_compressed:
			transcodeFormat = KTX_TTF_BC1_RGB;
			break;
		case Ov::Bitmap::Format::r8g8b8a8_compressed:
			transcodeFormat = KTX_TTF_BC3_RGBA;
			break;
		case Ov::Bitmap::Format::r8g8_compressed:
			transcodeFormat = KTX_TTF_BC5_RG;
			break;
		case Ov::Bitmap::Format::r8_compressed:
			transcodeFormat = KTX_TTF_BC4_R;
			break;
		case Ov::Bitmap::Format::last:
		case Ov::Bitmap::Format::none:
		default:
			OV_LOG_ERROR("The passed format %s to load is not valid in the bitmap with id= %d",
				magic_enum::enum_name(loadFormat), getId());
			ktxTexture_Destroy(ktxTexture(texture));
			return false;
			break;
		}

		result = ktxTexture2_TranscodeBasis(texture, transcodeFormat, 0);
		if (result != KTX_SUCCESS)
		{
			OV_LOG_ERROR("Fail to trasncode KTX file %s in the bitmap with id= %d. The error is %s",
				filename.c_str(), getId() , magic_enum::enum_name(result));
			ktxTexture_Destroy(ktxTexture(texture));
			return false;
		}
	}

	// Load image into memory
	// Create layers to contains side and levels
	std::vector<Ov::Bitmap::Layer> layers(texture->numFaces * texture->numLevels);
	// Get pointer to the memory where the image is stored.
	ktx_uint8_t* imageData = ktxTexture_GetData(ktxTexture(texture));
	ktx_size_t pOffset;

	// Sides of the texture
	for (uint32_t s = 0; s < texture->numFaces; s++) 
	{
		uint32_t sizeX = texture->baseWidth;
		uint32_t sizeY = texture->baseHeight;
		// Levels of the texture
		for (uint32_t l = 0; l < texture->numLevels; l++) 
		{
			std::reference_wrapper<Ov::Bitmap::Layer> currentLayer = layers[s * texture->numLevels + l];

			ktx_size_t levelSize = ktxTexture_GetImageSize(ktxTexture(texture), l);

			currentLayer.get().data.resize((size_t) levelSize);
			currentLayer.get().size.x = sizeX;
			currentLayer.get().size.y = sizeY;

			result = ktxTexture_GetImageOffset(ktxTexture(texture), l, 0, s, &pOffset);
			if (result != KTX_SUCCESS)
			{
				OV_LOG_ERROR("Fail to load KTX file %s into the bitmap with id= %d. The error is %s",
					filename.c_str(), getId(), magic_enum::enum_name(result));
				ktxTexture_Destroy(ktxTexture(texture));
				return false;
			}

			memcpy(currentLayer.get().data.data(), imageData + pOffset, levelSize);

			OV_LOG_DEBUG("Mipmap: %u, %ux%u, %u bytes", l, sizeX, sizeY, levelSize);

			// Update values:
			if (sizeX > 1)
				sizeX /= 2;
			if (sizeY > 1)
				sizeY /= 2;
		}
	}

	if (!Ov::Bitmap::setLayers(layers)) {
		OV_LOG_ERROR("Fail to set layers in the base classe Bitmap in the bitmap with id= %d", getId());
		ktxTexture_Destroy(ktxTexture(texture));
		return false;
	}

	ktxTexture_Destroy(ktxTexture(texture));

	// Done:
	return true;
}

#pragma endregion

#pragma region Save

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Save image to a .ktx file.
 * @param preserveHighQuality determine which type of compression to use.
 * @return TF
 */
bool OVVK_API OvVK::Bitmap::saveAsKTX(bool supercompress, bool preserveHighQuality) const
{
	// Define struct to create ktxtecture
	ktxTextureCreateInfo createInfo;

	// Get Vulkan format
	Format format = Ov::Bitmap::getFormat();
	switch (format)
	{
	case Ov::Bitmap::Format::r8g8b8:
		createInfo.vkFormat = VK_FORMAT_R8G8B8_SRGB;
		break;
	case Ov::Bitmap::Format::r8g8b8a8:	      
		createInfo.vkFormat = VK_FORMAT_R8G8B8A8_SRGB;
		break;
	case Ov::Bitmap::Format::r8g8b8_compressed:
		createInfo.vkFormat = VK_FORMAT_BC1_RGB_SRGB_BLOCK;
		break;
	case Ov::Bitmap::Format::r8g8b8a8_compressed:
		createInfo.vkFormat = VK_FORMAT_BC3_SRGB_BLOCK;
		break;
	case Ov::Bitmap::Format::r8g8_compressed:
		createInfo.vkFormat = VK_FORMAT_BC5_UNORM_BLOCK;
		break;
	case Ov::Bitmap::Format::r8_compressed:
		createInfo.vkFormat = VK_FORMAT_BC4_UNORM_BLOCK;
		break;
	default:
		break;
	}

	createInfo.baseWidth = Ov::Bitmap::getSizeX();
	createInfo.baseHeight = Ov::Bitmap::getSizeY();
	createInfo.baseDepth = 1;
	createInfo.numDimensions = 2;
	createInfo.numLevels = (Ov::Bitmap::getNrOfLevels() == 1) ? log2(createInfo.baseWidth) + 1 : Ov::Bitmap::getNrOfLevels();
	createInfo.numLayers = 1;
	createInfo.numFaces = Ov::Bitmap::getNrOfSides();
	createInfo.numFaces = 1;
	createInfo.isArray = KTX_FALSE;
	createInfo.generateMipmaps = (Ov::Bitmap::getNrOfLevels() == 1) ? KTX_TRUE : KTX_FALSE;


	ktx_error_code_e result;
	ktxTexture2* texture;

	// Create ktxTexture2
	result = ktxTexture2_Create(&createInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE,
	&texture);

	if (result != KTX_SUCCESS)
	{
		OV_LOG_ERROR("Fail to create ktxTexture2.");
		return false;
	}

	// Fill the ktxTexture2 with the data
	for (uint32_t s = 0; s < Ov::Bitmap::getNrOfSides(); s++)
	{
		for (uint32_t l = 0; l < Ov::Bitmap::getNrOfLevels(); l++)
		{
			result = ktxTexture_SetImageFromMemory(ktxTexture(texture), l, 0, s,
				Ov::Bitmap::getData(l, s), Ov::Bitmap::getNrOfBytes(l, s));

			if (result != KTX_SUCCESS)
			{
				OV_LOG_ERROR("Fail to load data into the ktx file in Bitmap with id= %d. Fail at level: %d and side: %d",
					getId(), l, s);
				ktxTexture_Destroy(ktxTexture(texture));
				return false;
			}
		}
	}

	// Superconpress if required
	if (supercompress) 
	{
		ktxBasisParams params = {0};
		params.structSize = sizeof(params);
		// For BasisLZ/ETC1S
		params.compressionLevel = KTX_ETC1S_DEFAULT_COMPRESSION_LEVEL;
		// For UASTC
		params.uastc = (preserveHighQuality) ? KTX_TRUE : KTX_FALSE;
		// Set other BasisLZ/ETC1S or UASTC params to change default quality settings.
		result = ktxTexture2_CompressBasisEx(texture, &params);

		if (result != KTX_SUCCESS)
		{
			OV_LOG_ERROR("Fail to compress file KTX in bitmao with id= %d.", getId());
			ktxTexture_Destroy(ktxTexture(texture));
			return false;
		}
	}

	// Save and destrox the ktxTexture2
	ktxTexture_WriteToNamedFile(ktxTexture(texture), (Ov::Object::getName() + ".ktx").c_str());
	ktxTexture_Destroy(ktxTexture(texture));

	// Done:
	return true;
}

#pragma endregion
