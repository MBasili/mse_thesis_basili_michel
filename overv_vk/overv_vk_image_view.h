/**
 * @file	overv_vk_image_view.h
 * @brief	Vulkan image view
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 // Forward Declaration
class Image;

/**
 * @brief Class for modeling a Vulkan image view
 */
class OVVK_API ImageView : public Ov::Object, public Ov::Managed
{
//////////
public: //
//////////

	// Static:
	static ImageView empty;

	// Const/dest:
	ImageView();
	ImageView(ImageView&& other) noexcept;
	ImageView(ImageView const&) = delete;
	virtual ~ImageView();

	// Operators
	ImageView& operator=(const ImageView&) = delete;
	ImageView& operator=(ImageView&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Get/Set
	VkImageView getVkImageView() const;
	VkImageViewCreateFlags getCreateFlags() const;
	VkImageViewType getType() const;
	VkFormat getFormat() const;
	VkComponentMapping getComponentMapping() const;
	VkImageSubresourceRange getSubresourceRange() const;
#endif

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General
	bool create(OvVK::Image& image, VkImageViewCreateInfo vkImageViewCreateInfo);
#endif

	// Const/dest:
	ImageView(const std::string& name);

	friend class OvVK::Image;
};