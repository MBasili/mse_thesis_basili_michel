/**
 * @file	overv_vk_dsm_texture.cpp
 * @brief	Descriptor set manager for textures
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Textures descriptor set manager reserved structure.
 */
struct OvVK::TexturesDescriptorSetsManager::Reserved
{
    VkDescriptorImageInfo descriptorImagesInfos[OvVK::Texture::maxNrOfTextures];    ///< Array of VkDescriptorImageInfo
    bool needDescriptorSetsUpdate[OvVK::Engine::nrFramesInFlight];                  ///< Boolean array to check if a descriptor set need update the data.

    std::list<std::reference_wrapper<const OvVK::Texture>> texturesToUpdate;        ///< List of textures used to update the descriptor sets

    /**
     * Constructor.
     */
    Reserved() 
    {
        // Initialize the map to check if the descriptor set of a particular frame need update.
        for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
            needDescriptorSetsUpdate[c] = false;
    }
};


/////////////////////////////////////////
// BODY OF CLASS TexturesDescriptorSet //
/////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::TexturesDescriptorSetsManager::TexturesDescriptorSetsManager() :
    reserved(std::make_unique<OvVK::TexturesDescriptorSetsManager::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::TexturesDescriptorSetsManager::~TexturesDescriptorSetsManager()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::TexturesDescriptorSetsManager::init()
{
    if (this->OvVK::DescriptorSetsManager::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan descriptor set textures.
 * @return TF
 */
bool OVVK_API OvVK::TexturesDescriptorSetsManager::free()
{
    if (this->OvVK::DescriptorSetsManager::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Singleton

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get singleton instance.
 */
OvVK::TexturesDescriptorSetsManager OVVK_API& OvVK::TexturesDescriptorSetsManager::getInstance()
{
    static TexturesDescriptorSetsManager instance;

    if (!instance.isInitialized())
        instance.create();

    return instance;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the descriptor pool, descriptor sets and descriptor sets layouts.
 * @return TF
 */
bool OVVK_API OvVK::TexturesDescriptorSetsManager::create()
{
    // Init
    if (this->init() == false)
        return false;

    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();

    // Get default textures
    std::reference_wrapper<const OvVK::Texture2D> defaultTexture = OvVK::Texture2D::getDefault();
    // Get default sampler
    VkSampler samplerDefault = OvVK::Sampler::getDefault().getVkSampler();


    /////////////////////////////////
    // SetUp descriptor image info //
    /////////////////////////////////

    for (uint32_t c = 0; c < OvVK::Texture::maxNrOfTextures; c++)
    {
        VkDescriptorImageInfo& descriptorImageInfo = reserved->descriptorImagesInfos[c];

        VkSampler sampler = defaultTexture.get().getSampler().getVkSampler();
        descriptorImageInfo.sampler = (sampler != VK_NULL_HANDLE) ? sampler : samplerDefault;
        descriptorImageInfo.imageView = defaultTexture.get().getImageView().getVkImageView();
        descriptorImageInfo.imageLayout = defaultTexture.get().getImage().getImageLayout();
    }

    // VkDescriptorPoolSize create
    std::vector<VkDescriptorPoolSize> descriptorPoolSize(1);
    descriptorPoolSize[0].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorPoolSize[0].descriptorCount = OvVK::Texture::maxNrOfTextures * OvVK::Engine::nrFramesInFlight;

    // VkDescriptorSetLayoutBinding create
    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBinding(1);
    descriptorSetLayoutBinding[0].binding = static_cast<uint32_t>(TexturesDescriptorSetsManager::Binding::textures);
    descriptorSetLayoutBinding[0].descriptorCount = OvVK::Texture::maxNrOfTextures;
    descriptorSetLayoutBinding[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorSetLayoutBinding[0].pImmutableSamplers = NULL;
    descriptorSetLayoutBinding[0].stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR;


    // Create descriptor pool, descriptor sets and descriptor sets layouts
    if (this->OvVK::DescriptorSetsManager::create(descriptorPoolSize, descriptorSetLayoutBinding,
        OvVK::Engine::nrFramesInFlight) == false) 
        return false;


    /////////////////////////////
    // Write to descriptor set //
    /////////////////////////////

    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++) 
    {
        VkWriteDescriptorSet texturesWriteDescriptorSets;
        texturesWriteDescriptorSets.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        texturesWriteDescriptorSets.pNext = NULL;
        texturesWriteDescriptorSets.dstSet = this->OvVK::DescriptorSetsManager::getVkDescriptorSet(c);
        texturesWriteDescriptorSets.dstBinding = static_cast<uint32_t>(TexturesDescriptorSetsManager::Binding::textures);
        texturesWriteDescriptorSets.dstArrayElement = 0;
        texturesWriteDescriptorSets.descriptorCount = OvVK::Texture::maxNrOfTextures;
        texturesWriteDescriptorSets.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        texturesWriteDescriptorSets.pImageInfo = reserved->descriptorImagesInfos;
        texturesWriteDescriptorSets.pBufferInfo = NULL;
        texturesWriteDescriptorSets.pTexelBufferView = NULL;

        // This can generate a race condition and undefined behavior if the descriptor set to 
        // update is in use by a command buffer that�s being executed in a queue.
        // cannot modify a descriptor set unless you know that the GPU is finished with it.
        // Update ad the beginning of the frame.
        vkUpdateDescriptorSets(engine.get().getLogicalDevice().getVkDevice(), 1,
            &texturesWriteDescriptorSets, 0, NULL);
    }

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Updates the DSMTextures. Adds to the DSMTextures a new texture or updates a already present one.
 * @param texture The texture to add or to update into the DSMTextures.
 * @return TF
 */
bool OVVK_API OvVK::TexturesDescriptorSetsManager::addToUpdateList(const OvVK::Texture& texture)
{
    // Safety net:
    if (texture.getId() == Ov::Renderable::empty.getId())
    {
        OV_LOG_ERROR("texture passed is the empty texture in DSMTextures.");
        return false;
    }

    // Alread in the list?
    bool isPresent = false;
    for (auto& c : reserved->texturesToUpdate)
        if (c.get() == texture) {
            isPresent == true;
            break;
        }

    if (!isPresent) {
        // Store it:
        reserved->texturesToUpdate.push_back(texture);
    }

    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
        reserved->needDescriptorSetsUpdate[c] = true;

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::TexturesDescriptorSetsManager::render(uint32_t value, void* data) const
{
    // Engine
    std::reference_wrapper<const OvVK::Engine> engine = Engine::getInstance();

    // Get current frame in flight
    uint32_t frameInFlight = engine.get().getFrameInFlight();
    // Get default sampler
    VkSampler samplerDefault = OvVK::Sampler::getDefault().getVkSampler();

    // Check if there is the need to update:
    if (reserved->needDescriptorSetsUpdate[frameInFlight])
    {
        for (std::reference_wrapper<const OvVK::Texture> texture : reserved->texturesToUpdate) 
        {
            VkDescriptorImageInfo& descriptorImageInfo = reserved->descriptorImagesInfos[texture.get().getLutPos()];

            VkSampler sampler = texture.get().getSampler().getVkSampler();
            descriptorImageInfo.sampler = (sampler != VK_NULL_HANDLE) ? sampler : samplerDefault;
            descriptorImageInfo.imageView = texture.get().getImageView().getVkImageView();
            descriptorImageInfo.imageLayout = texture.get().getImage().getImageLayout();
        }

        // Clear updates
        reserved->texturesToUpdate.clear();


        /////////////////////////////
        // Write to descriptor set //
        /////////////////////////////

        VkWriteDescriptorSet texturesWriteDescriptorSets;
        texturesWriteDescriptorSets.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        texturesWriteDescriptorSets.pNext = NULL;
        texturesWriteDescriptorSets.dstSet = this->OvVK::DescriptorSetsManager::getVkDescriptorSet(frameInFlight);
        texturesWriteDescriptorSets.dstBinding = static_cast<uint32_t>(TexturesDescriptorSetsManager::Binding::textures);
        texturesWriteDescriptorSets.dstArrayElement = 0;
        texturesWriteDescriptorSets.descriptorCount = OvVK::Texture::maxNrOfTextures;
        texturesWriteDescriptorSets.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        texturesWriteDescriptorSets.pImageInfo = reserved->descriptorImagesInfos;
        texturesWriteDescriptorSets.pBufferInfo = NULL;
        texturesWriteDescriptorSets.pTexelBufferView = NULL;

        // This can generate a race condition and undefined behavior if the descriptor set to 
        // update is in use by a command buffer that�s being executed in a queue.
        // cannot modify a descriptor set unless you know that the GPU is finished with it.
        // Update ad the beginning of the frame.
        vkUpdateDescriptorSets(engine.get().getLogicalDevice().getVkDevice(), 1,
            &texturesWriteDescriptorSets, 0, NULL);

        reserved->needDescriptorSetsUpdate[frameInFlight] = false;
    }

    // Done:
    return true;
}

#pragma endregion