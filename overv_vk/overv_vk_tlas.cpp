/**
 * @file    overv_vk_tlas.cpp
 * @brief	Top Level Acceleration Structure
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:   
OvVK::TopLevelAccelerationStructure OvVK::TopLevelAccelerationStructure::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Buffer reserved structure.
 */
struct OvVK::TopLevelAccelerationStructure::Reserved
{
    OvVK::Buffer deviceASInstancesBuffer;                                       ///< The buffer containing the VkAccelerationStructureInstanceKHR of the TLAS
    uint32_t nrOfASInstance;                                                        ///< The number of VkAccelerationStructureInstanceKHR
    
    VkGeometryFlagsKHR geometriesFlag;                                              ///< Specifying the type of allowed interaction for all instances.

    // The use of vectors are necessary for compatibility to the base class AS methods.
    // Basically for the TLAS we need only one object for each of the two following strutures.
    std::vector<VkAccelerationStructureGeometryKHR> geometryAS;                     ///< Vector containing the structure specifying geometries instaces in the TLAS.
    std::vector<VkAccelerationStructureBuildRangeInfoKHR> geometriesBuildRangeAS;   ///< Vector containing the structuresspecifying build offsets and counts for the TLAS.

    /**
     * Constructor.
     */
    Reserved()
    {}
};


/////////////////////////////////////////////////
// BODY OF CLASS TopLevelAccelerationStructure //
/////////////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::TopLevelAccelerationStructure::TopLevelAccelerationStructure() :
    reserved(std::make_unique<OvVK::TopLevelAccelerationStructure::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::TopLevelAccelerationStructure::TopLevelAccelerationStructure(const std::string& name) :
    OvVK::AccelerationStructure(name),
    reserved(std::make_unique<OvVK::TopLevelAccelerationStructure::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::TopLevelAccelerationStructure::TopLevelAccelerationStructure(
    TopLevelAccelerationStructure&& other) noexcept : OvVK::AccelerationStructure(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::TopLevelAccelerationStructure::~TopLevelAccelerationStructure()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize acceleration struture
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::init()
{
    if (this->OvVK::AccelerationStructure::init() == false)
        return false;


    if (reserved->deviceASInstancesBuffer.free() == false)
        return false;

    reserved->nrOfASInstance = 0;
    reserved->geometriesFlag = 0x00000000; 
    reserved->geometryAS.clear();
    reserved->geometriesBuildRangeAS.clear();


    // Done
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases acceleration struture
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::free()
{
    if (this->OvVK::AccelerationStructure::free() == false)
        return false;

 
    if (reserved->deviceASInstancesBuffer.free() == false)
        return false;

    reserved->nrOfASInstance = 0;
    reserved->geometriesFlag = 0x00000000;
    reserved->geometryAS.clear();
    reserved->geometriesBuildRangeAS.clear();


    //Done::
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up the data necessary to build the TLAS.
 * @param nrOfASInstance number of geometries instances that the TLAS will contains.
 * @param geometriesFlag the flag describing the allowed type of interaction for the geometries instances.
 * @param asFlag the flag to specify the TLAS "properties".
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::setUpBuild(
    uint32_t nrOfASInstance,
    VkGeometryFlagsKHR geometriesFlag,
    VkBuildAccelerationStructureFlagsKHR asFlag)
{
    //Safety net:
    if (nrOfASInstance == 0)
    {
        OV_LOG_ERROR("The TLAS with id= %d need to contains at most one ASInstance.", getId());
        return false;
    }
    if (isLock())
    {
        OV_LOG_ERROR("Can't rebuild the TLAS with id= %d if it is in use.", getId());
        return false;
    }

    if (!this->init())
        return false;


    ///////////////////////////////
    // Device ASInstances Buffer //
    ///////////////////////////////

#pragma region ASInstancesBuffer

    // Create info buffer
    VkBufferCreateInfo asInstancesBufferCreateInfo{};
    asInstancesBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // The second field of the struct is size, which specifies the size of the buffer in bytes
    asInstancesBufferCreateInfo.size = sizeof(VkAccelerationStructureInstanceKHR) * nrOfASInstance;
    // The third field is usage, which indicates for which purposes the data in the buffer 
    // is going to be used. It is possible to specify multiple purposes using a bitwise or.
    asInstancesBufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
        | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    // buffers can also be owned by a specific queue family or be shared between multiple 
    // at the same time. 
    // VK_SHARING_MODE_CONCURRENT specifies that concurrent access to any range or image subresource of the object
    // from multiple queue families is supported.
    asInstancesBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // From which queue family the buffer will be accessed.
    asInstancesBufferCreateInfo.pQueueFamilyIndices = NULL;
    asInstancesBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaASInstancesAllocationCreateInfo = {};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest possible
    // free range for the allocation to minimize memory usage and fragmentation, possibly at the expense of allocation time.
    vmaASInstancesAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // 	Flags that must be set in a Memory Type chosen for an allocation. 
    vmaASInstancesAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    if (!reserved->deviceASInstancesBuffer.create(asInstancesBufferCreateInfo, vmaASInstancesAllocationCreateInfo))
    {
        OV_LOG_ERROR("Fail to create device geometries instances buffer for TLAS with id= %d.", getId());
        this->free();
        return false;
    }

#pragma endregion


    if (setUp(nrOfASInstance, geometriesFlag) == false || this->OvVK::AccelerationStructure::setUp(
        VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
        reserved->geometryAS, reserved->geometriesBuildRangeAS,
        VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
        asFlag) == false)
    {
        reserved->geometryAS.clear();
        reserved->geometriesBuildRangeAS.clear();
        this->free();
        return false;
    }

    reserved->nrOfASInstance = nrOfASInstance;
    reserved->geometriesFlag = geometriesFlag;

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up the data necessary to update the TLAS.
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::setUpUpdate()
{
    // Safety net:
    if (getVkAS() == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("The TLAS with id= %d is not build, so can't update.", getId());
        return false;
    }
    if (isLock())
    {
        OV_LOG_ERROR("Can't update the TLAS with id= %d if it is in use.", getId());
        return false;
    }
    if ((getVkBuildAccelerationStructureFlagsKHR() & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR)
        != VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR)
    {
        OV_LOG_ERROR("Can't update the TLAS with id= %d, because the flag for update is not set.", getId());
        return false;
    }

    // Clear the setUp.
    clearSetUpData();

    if (setUp(reserved->nrOfASInstance, reserved->geometriesFlag) == false ||
        this->OvVK::AccelerationStructure::setUp(VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
        reserved->geometryAS, reserved->geometriesBuildRangeAS,
        VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR,
        getVkBuildAccelerationStructureFlagsKHR()) == false)
    {
        reserved->geometryAS.clear();
        reserved->geometriesBuildRangeAS.clear();
        this->free();
        return false;
    }

    //Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Clear the setUp data used to build or update the AS.
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::clearSetUpData()
{
    if (!this->OvVK::AccelerationStructure::freeScratchBuffer())
    {
        OV_LOG_ERROR("Can't delete build data of the TLAS with id= %d if they are use in not terminate process.", getId());
        return false;
    }

    reserved->geometryAS.clear();
    reserved->geometriesBuildRangeAS.clear();

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up data needed to build or update the AS.
 * @param nrOfASInstance the number of geometries instances in the TLAS.
 * @param geometriesFlag the flag describing the allowed type of interaction for the geometries instances.
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::setUp( uint32_t nrOfASInstance,
    VkGeometryFlagsKHR geometriesFlag) {

    // Get device 64-bit address of the buffer containing the instances.
    std::optional<VkDeviceAddress> deviceBufferAddress = reserved->deviceASInstancesBuffer.getBufferDeviceAddress();
    if (deviceBufferAddress.has_value() == false)
    {
        OV_LOG_ERROR("Fail to retrive the device address of the geometries instances device buffer of the TLAS with id= %d.", getId());
        return false;
    }

    VkDeviceOrHostAddressConstKHR geometryInstanceDeviceOrHostAddressConst = {};
    geometryInstanceDeviceOrHostAddressConst.deviceAddress = deviceBufferAddress.value();

    // Structure specifying the geometries instances in a bottom-level acceleration structure
    VkAccelerationStructureGeometryInstancesDataKHR accelerationStructureGeometryInstancesData = {};
    accelerationStructureGeometryInstancesData.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    accelerationStructureGeometryInstancesData.pNext = NULL;
    accelerationStructureGeometryInstancesData.arrayOfPointers = VK_FALSE;
    accelerationStructureGeometryInstancesData.data = geometryInstanceDeviceOrHostAddressConst;

    VkAccelerationStructureGeometryDataKHR accelerationStructureGeometryData = {};
    accelerationStructureGeometryData.instances = accelerationStructureGeometryInstancesData;

    // Structure specifying geometries instances to be built into an acceleration structure
    VkAccelerationStructureGeometryKHR& accelerationStructureGeometryKHR = reserved->geometryAS.emplace_back();
    accelerationStructureGeometryKHR.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    accelerationStructureGeometryKHR.pNext = NULL;
    accelerationStructureGeometryKHR.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    accelerationStructureGeometryKHR.geometry = accelerationStructureGeometryData;
    accelerationStructureGeometryKHR.flags = geometriesFlag;

    // Structure specifying build offset and count for acceleration structure
    VkAccelerationStructureBuildRangeInfoKHR& accelerationStructureBuildRangeInfoKHR = reserved->geometriesBuildRangeAS.emplace_back();
    accelerationStructureBuildRangeInfoKHR.primitiveCount = nrOfASInstance;
    accelerationStructureBuildRangeInfoKHR.primitiveOffset = 0;
    accelerationStructureBuildRangeInfoKHR.firstVertex = 0;
    accelerationStructureBuildRangeInfoKHR.transformOffset = 0;

    //Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the OvVK Buffer reference of the geometries instances buffer.
 * @return The OvVK Buffer reference of the geometries instances buffer.
 */
const OvVK::Buffer OVVK_API& OvVK::TopLevelAccelerationStructure::getDeviceASInstancesBuffer() const
{
    return reserved->deviceASInstancesBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the number of geometries instances.
 * @return The number of geometries instances.
 */
uint32_t OVVK_API OvVK::TopLevelAccelerationStructure::getNrOfASInstances() const 
{
    return reserved->nrOfASInstance;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the flag that describe the allowed interaction to the geometries instances.
 * @return The number of geometries instances.
 */
VkGeometryFlagsKHR OVVK_API OvVK::TopLevelAccelerationStructure::getASGeometriesFlag() const 
{
    return reserved->geometriesFlag;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the referece to the structures specifying geometries instances in the TLAS.
 * @return Std optional obj containing the referece to the structures specifying geometries instances in the TLAS.
 * This is missing if the setUp is not done.
 */
std::optional<std::reference_wrapper<const VkAccelerationStructureGeometryKHR>> 
OvVK::TopLevelAccelerationStructure::getAccelerationStructureGeometriesKHR() const
{
    //Safety net:
    if (reserved->geometryAS.size() == 0)
        return std::nullopt;

    return reserved->geometryAS[0];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the referece to the structures specifying build offset and count for the TLAS.
 * @return Std optional obj containing the referece to the structures specifying build offset and count for the TLAS.
 * This is missing if the setUp is not done.
 */
std::optional<std::reference_wrapper<const VkAccelerationStructureBuildRangeInfoKHR>> 
OvVK::TopLevelAccelerationStructure::getAccelerationStructureBuildRangeInfosKHR() const 
{
    //Safety net:
    if (reserved->geometriesBuildRangeAS.size() == 0)
        return std::nullopt;

    return reserved->geometriesBuildRangeAS[0];
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::TopLevelAccelerationStructure::render(uint32_t value, void* data) const
{
    if (isDirty())
        setDirty(false);

    // Done:
    return true;
}

#pragma endregion