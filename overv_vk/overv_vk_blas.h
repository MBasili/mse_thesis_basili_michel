#pragma once
/**
 * @file    overv_vk_blas.h
 * @brief	Bottom Level Acceleration Structure
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/**
 * @brief BottomLevelAccelerationStructure BLAS in vukan
 */
class OVVK_API BottomLevelAccelerationStructure final : public OvVK::AccelerationStructure
{
//////////
public: //
//////////

	// Special values:
	static BottomLevelAccelerationStructure empty;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Geometry data for each geometry in a BLAS
	 */
	struct GeometryData
	{
		std::reference_wrapper<OvVK::Geometry> geometry;	///< Reference to a geometry
		VkGeometryFlagsKHR geometryFlags;						///< Specify the allowed interaction with the geometry.
		VkTransformMatrixKHR transformsMatrix;					///< Position of the geometry relative to the BLAS.

		GeometryData() : geometry{ OvVK::Geometry::empty },
			transformsMatrix {
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f }
		{}
	};
#endif

	// Const/dest:
	BottomLevelAccelerationStructure();
	BottomLevelAccelerationStructure(BottomLevelAccelerationStructure&& other) noexcept;
	BottomLevelAccelerationStructure(BottomLevelAccelerationStructure const&) = delete;
	~BottomLevelAccelerationStructure();

	// Operators
	BottomLevelAccelerationStructure& operator=(const BottomLevelAccelerationStructure&) = delete;
	BottomLevelAccelerationStructure& operator=(BottomLevelAccelerationStructure&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// General:
	bool addGeometry(OvVK::Geometry &geometry,
		VkGeometryFlagsKHR geometryFlags = 0x00000000,
		VkTransformMatrixKHR transformsMatrix = {
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f });
	bool setUpBuild(VkBuildAccelerationStructureFlagsKHR flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR);
#endif

	bool setUpUpdate();
	bool clearGeometries();
	bool clearSetUpData();

	// Get/Set
	const OvVK::Buffer& getTransformsDataBuffer() const;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	std::vector<std::reference_wrapper<const GeometryData>> getGeometriesData() const;
	const std::vector<VkAccelerationStructureGeometryKHR>& getAccelerationStructureGeometriesKHR() const;
	const std::vector<VkAccelerationStructureBuildRangeInfoKHR>& getAccelerationStructureBuildRangeInfosKHR() const;
#endif

	uint32_t getNrOfGeometries() const;
	std::size_t getHash() const;
	
	// Render:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// General
	bool setUp();

	// Const/dest:
	BottomLevelAccelerationStructure(const std::string& name);
};
