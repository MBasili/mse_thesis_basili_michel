/**
 * @file	overv_vk_dsm_picking.h
 * @brief	Descriptor set manager for picking
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modeling a descriptor set manager for picking.
  */
class OVVK_API PickingDescriptorSetsManager final : public OvVK::DescriptorSetsManager
{
//////////
public: //
//////////

	/**
	 * @brief Definition of binding points.
	 */
	enum class Binding : uint32_t
	{
		picking = 0
	};

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	/**
	 * @brief Struct used to update
	 */
	struct UpdateData
	{
		uint32_t targetFrame;
		std::optional<std::reference_wrapper<OvVK::Buffer>> pickingBuffer;

		UpdateData() : targetFrame{ 0 },
			pickingBuffer{ std::nullopt }
		{}
	};
#endif

	// Const/dest:
	PickingDescriptorSetsManager();
	PickingDescriptorSetsManager(PickingDescriptorSetsManager&& other);
	PickingDescriptorSetsManager(PickingDescriptorSetsManager const&) = delete;
	virtual ~PickingDescriptorSetsManager();

	// Operators
	PickingDescriptorSetsManager& operator=(const PickingDescriptorSetsManager&) = delete;
	PickingDescriptorSetsManager& operator=(PickingDescriptorSetsManager&&) = delete;

	// Managed:
	bool init() override;
	bool free() override;

	// General:
	virtual bool create() override;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool addToUpdateList(const UpdateData& data);
#endif

	// Rendering:
	bool render(uint32_t value = 0, void* data = nullptr) const override;

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	PickingDescriptorSetsManager(const std::string& name);
};
