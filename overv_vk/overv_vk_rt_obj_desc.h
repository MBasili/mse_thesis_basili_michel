#pragma once
/**
 * @file    overv_vk_rt_obj_desc.h
 * @brief	Ray Tracing Obj Desc
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Ray Tracing Obj Description
  */
class OVVK_API RTObjDesc : public Ov::Renderable, public Ov::Managed
{
//////////
public: //
//////////

	// Static special value
	static RTObjDesc empty;

	constexpr static uint32_t maxNrOfRTObjDescs = 1000;

	constexpr static uint64_t readyToUploadValueStatusSyncObj = 1;
	constexpr static uint64_t uploadingValueStatusSyncObj = 2;
	constexpr static uint64_t readyValueStatusSyncObj = 3;

	// Information of a geometry when referenced in a shader
	struct GeometryData
	{
		uint64_t vertexAddress;			// Address of the Vertex buffer
		uint64_t faceAddress;			// Address of the index buffer
		uint32_t materialIndex;			// Index of the material in the material buffer
		uint32_t geometryId;			// Index of the geometry
		uint32_t rtObjDescId;			// Index of the RTObjDesc containg the BLAS containing the geometry.
		// 32 bytes

		GeometryData() : vertexAddress{ 0 },
			faceAddress{ 0 },
			materialIndex{ 0 },
			geometryId{ 0 },
			rtObjDescId{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct GeometryData
{
	uint64_t vertexAddress; 
	uint64_t indexAddress; 
	uint32_t materialIndex;
	uint32_t geometryId;
	uint32_t rtObjDescId;
};
)";
		}
	};

	// Const/dest:
	RTObjDesc();
	RTObjDesc(RTObjDesc&& other);
	RTObjDesc(RTObjDesc const&) = delete;
	~RTObjDesc();

	// Operators
	RTObjDesc& operator=(const RTObjDesc&) = delete;
	RTObjDesc& operator=(RTObjDesc&&) = delete;

	// Managed
	virtual bool init() override;
	virtual bool free() override;

	//Sync Obj
	bool isLock() const;
	bool isOwned() const;

	// General
	bool setUp();

	// Get/Set
	const OvVK::Buffer& getGeometryDataDeviceBuffer() const;
	const OvVK::BottomLevelAccelerationStructure& getBLAS() const;
	const std::vector<std::reference_wrapper<OvVK::Material>>& getMaterials() const;
	uint32_t getNrGeoemtriesInBLAS() const;
	OvVK::Material& getGeometryMaterial(const OvVK::Geometry& geom) const;
	OvVK::Material& getGeometryMaterial(uint32_t geometryId) const;
	const std::vector<OvVK::RTObjDesc::GeometryData> getGeometriesData() const;
	std::size_t getHash() const;

	bool setBLAS(const OvVK::BottomLevelAccelerationStructure& blas);
	bool setGeometryMaterial(const OvVK::Geometry& geom, OvVK::Material& material);
	bool setGeometryMaterial(uint32_t geometryId, OvVK::Material& material);

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	bool setQueueFamilyIndex(uint32_t queueFamilyIndex);
	bool setStatusSyncObjValue(uint64_t value);
	bool setLockSyncObj(VkFence fence);
#endif

	// Rendering
	virtual bool render(uint32_t value = 0, void* data = nullptr) const override;

	// Shader
	static bool addShaderPreproc();

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	RTObjDesc(const std::string& name);
};