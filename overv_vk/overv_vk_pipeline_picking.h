/**
 * @file	overv_vk_pipeline_picking.h
 * @brief	Picking pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modelling Picking Pipeline
  */
class OVVK_API PickingPipeline final : public OvVK::RayTracingPipeline
{
//////////
public: //
//////////

	constexpr static uint64_t defaultPickingValueStatusSyncObj = 0;
	constexpr static uint64_t pickingValueStatusSyncObj = 1;
	constexpr static uint64_t donePickingValueStatusSyncObj = 2;

	/**
     * @brief Definition of bind sets.
     */
	enum class BindSet : uint32_t
	{
		storage = 0,
		picking = 1,
		rt = 2
	};

	/**
	 * @brief Renderable element
	 */
	struct PickingData
	{
		glm::vec2 pickingCoords;
		uint32_t geometryId;
		uint32_t rtObjDescId;
		uint32_t pickedSomething;

		PickingData() :	geometryId{ 0 },
			rtObjDescId{ 0 }
		{}

		static std::string getShaderStruct()
		{
			return R"(
struct PickingData
{
	vec2 pickingCoords;
	uint32_t geometryId;
	uint32_t rtObjDescId;
	uint32_t pickedSomething;
};
)";
		}
	};

	// Const/dest:
	PickingPipeline();
	PickingPipeline(PickingPipeline&& other);
	PickingPipeline(PickingPipeline const&) = delete;
	virtual ~PickingPipeline();

	// Operators
	PickingPipeline& operator=(const PickingPipeline&) = delete;
	PickingPipeline& operator=(PickingPipeline&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// General
	virtual bool create() override;

	// Call backs
	virtual bool mouseCursorCallback(double mouseX, double mouseY) override;

	// Get/Set:
	bool setPickingCoordinates(glm::vec2 pickingCoords);

	// Rendering methods:
	bool render(const OvVK::RTScene& rtScene);

	// Preprocessor
	static bool addShaderPreproc();

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	PickingPipeline(const std::string& name);
};