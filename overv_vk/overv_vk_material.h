#pragma once
/**
 * @file	overv_vk_material.h
 * @brief	Vulkan material properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Class for modeling a generic material.
  */
class OVVK_API Material final : public Ov::MaterialPbr, public Ov::DrawGUI
{
//////////
public: //
//////////   

	// Special values:
	static Material empty;
	constexpr static uint32_t maxNrOfMaterials = 256;					///< Max number of concurrent materials
	constexpr static uint32_t maxNrOfTextures = 5;			///< Max number of textures per material

	// Const/dest:
	Material();
	Material(Material&& other);
	Material(Material const&) = delete;
	virtual ~Material();

	// Operators
	Material& operator=(const Material&) = delete;
	Material& operator=(Material&&) = delete;

	// Default materials:
	static const Material& getDefault();

	// Managed:
	bool init() override;
	bool free() override;

	// Get/set:   
	uint32_t getLutPos() const;
	const OvVK::Texture2D& getTexture(Ov::Texture::Type type = Ov::Texture::Type::diffuse) const;

	bool setTexture(const OvVK::Texture2D& tex, Ov::Texture::Type type = Ov::Texture::Type::diffuse);

	// Texture
	virtual bool loadTexture(const std::string& textureName, Ov::Texture::Type type, Ov::Container& container) override;

	// Sync
	bool uploadToStorage() const;

	// Rendering methods:   
	bool render(uint32_t value = 0, void* data = nullptr) const;

	// DrawGUI
	virtual void drawGUI() override;

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Material(const std::string& name);

	// LUT:
	static bool lut[OvVK::Material::maxNrOfMaterials];
};