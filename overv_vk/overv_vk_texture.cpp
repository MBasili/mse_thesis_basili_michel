/**
 * @file	overv_vk_texture.cpp
 * @brief	Vulkan texture properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values:
OvVK::Texture OvVK::Texture::empty("[empty]");

// Look-up table:
bool OvVK::Texture::lut[OvVK::Texture::maxNrOfTextures];


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Texture class reserved structure.
 */
struct OvVK::Texture::Reserved
{
    OvVK::Buffer bitmapBuffer;                              ///< bitmap buffer obj.
    OvVK::Image image;                                      ///< Image obj.
    std::reference_wrapper<const OvVK::ImageView> imageView;///< Image view Vulkan handle

    std::vector<VkBufferImageCopy> bufferImageCopyRegions;      ///< Copy regions from bitmap to image

    std::reference_wrapper<const OvVK::Sampler> sampler;    ///< Handle of the sampler used in the shader to read the texture                         

    uint32_t lutPos;                                            ///< Position in the lut 
    bool needGenerateMipmap;                                    ///< Variable to know if need to generate mipmap

    /**
     * Constructor.
     */
    Reserved() : imageView{ OvVK::ImageView::empty },
        sampler { OvVK::Sampler::empty },
        lutPos{ OvVK::Texture::maxNrOfTextures },
        needGenerateMipmap{ false }
    {
        // Find a free slot in the LUT:
        for (uint32_t c = 0; c < OvVK::Texture::maxNrOfTextures; c++)
        {
            if (lut[c] == false)
            {
                lut[c] = true;
                lutPos = c;
                break;
            }
        }
        if (lutPos == OvVK::Texture::maxNrOfTextures)
            OV_LOG_ERROR("Too many textures created");
    }

    /**
     * Destructor.
     */
    ~Reserved()
    {
        if (lutPos != OvVK::Texture::maxNrOfTextures)
            lut[lutPos] = false;
    }
};


///////////////////////////
// BODY OF CLASS Texture //
///////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Texture::Texture() : reserved(std::make_unique<OvVK::Texture::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Texture::Texture(const std::string& name) : Ov::Texture(name),
    reserved(std::make_unique<OvVK::Texture::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Texture::Texture(Texture&& other) : Ov::Texture(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Texture::~Texture()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initialize vulkan texture.
 * @return TF
 */
bool OVVK_API OvVK::Texture::init()
{
    if (this->Ov::Texture::init() == false)
        return false;


    // Free bitmap buffer and image
    if (reserved->bitmapBuffer.free() == false)
        return false;
    if (reserved->image.free() == false)
        return false;


    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free vulkan texture.
 * @return TF
 */
bool OVVK_API OvVK::Texture::free()
{
    if (this->Ov::Texture::free() == false)
        return false;


    // Free bitmap buffer and image
    if (reserved->bitmapBuffer.free() == false)
        return false;
    if (reserved->image.free() == false)
        return false;


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncObj

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the texture is locked.
 * @return TF
 */
bool OVVK_API OvVK::Texture::isLock() const
{
    return reserved->image.isLock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns if the texture is owned from a queue family.
 * @return TF
 */
bool OVVK_API OvVK::Texture::isOwned() const 
{
    return reserved->image.isOwned();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Loads texture from a bitmap.
 * @param bitmap The bipmap that will be used to take the data of the texture.
 * @param isLinearEncoded If the data in the bitmap are linear encoded or not. (UNORM/SRGB)
 * @param holdReferenceBitmap If the texture need to hold the reference to the source bitmap.
 * @return TF
 */
bool OVVK_API OvVK::Texture::load(const Ov::Bitmap& bitmap, bool isLinearEncoded, bool holdReferenceBitmap)
{
    OV_LOG_ERROR("Texture empty load method called in texture with id= %d.", getId());
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * SetUp the image and the bitmap buffer for loading the texture.
 * @param bitmap The bipmap that will be used to take the data of the texture.
 * @param isLinearEncoded If the data in the bitmap are linear encoded or not. (UNORM/SRGB)
 * @param holdReferenceBitmap If the texture need to hold the reference to the source bitmap.
 * @return TF
 */
bool OVVK_API OvVK::Texture::setUp(const Ov::Bitmap& bitmap, bool isLinearEncoded, bool holdReferenceBitmap)
{
    OV_LOG_ERROR("Texture empty setUp method called in texture with id= %d.", getId());
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates texture.
 * @param bitmap The bipmap that will be used to take the data of the texture.
 * @param width The width of the texture.
 * @param height The height of the texture.
 * @param nrOfLevels The number of levels(levels mipmap) in the texture.
 * @param nrOfLayers The number of layers(sides) in the texture.
 * @param format The format of the texture.
 * @param isLinearEncoded If the data in the bitmap are linear encoded or not. (need to choose the texture format)
 * @return TF
 */
bool OVVK_API OvVK::Texture::create(uint32_t width, uint32_t height, uint32_t nrOfLevels,
    uint32_t nrOfLayers, Format format, bool isLinearEncoded, VkImageViewType imageViewType,
    VkImageCreateFlags flagsImage, VkImageViewCreateFlags flagsImageView)
{
    OV_LOG_ERROR("Texture empty create method called.");
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Free the buffer used to load in the GPU the bitmap.
 * @return TF
 */
bool OVVK_API OvVK::Texture::freeBitmapBuffer() 
{

    if (reserved->bitmapBuffer.free() == false)
    {
        OV_LOG_ERROR("Fail to free bitmap buffer in the texture wih id= %d.", getId());
        return false;
    }

    reserved->bufferImageCopyRegions.clear();

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the buffer to load the bitmap in the GPU.
 * @param bufferCreateInfo The Vulkan struct with the info to create the buffer.
 * @param bufferAllocationCreateInfo The VMA struct with the info to allocate the buffer.
 * @return TF
 */
bool OVVK_API OvVK::Texture::createBitmapBuffer(VkBufferCreateInfo bufferCreateInfo,
    VmaAllocationCreateInfo bufferAllocationCreateInfo)
{
    if (reserved->bitmapBuffer.getVkBuffer() != VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Buffer of the texture with id= %d already created.", getId());
        return false;
    }

    return reserved->bitmapBuffer.create(bufferCreateInfo, bufferAllocationCreateInfo);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Creates the image holding the texture and the image view to access it.
 * @param imageCreateInfo The Vulkan struct with the info to create the image.
 * @param imageAllocationCreateInfo The VMA struct with the info to allocate the image.
 * @param imageViewCreateInfo The Vulkan struct with info to create the image view.
 * @return TF
 */
bool OVVK_API OvVK::Texture::createImage(VkImageCreateInfo imageCreateInfo,
    VmaAllocationCreateInfo imageAllocationCreateInfo, VkImageViewCreateInfo imageViewCreateInfo)
{
    if (reserved->image.getVkImage() != VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Image of the texture with id= %d already created.", getId());
        return false;
    }

    // Create image
    if (reserved->image.create(imageCreateInfo, imageAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create the image of the texture with id= %d", getId());
        return false;
    }

    // Create image view
    reserved->imageView = reserved->image.createImageView(imageViewCreateInfo);
    if (reserved->imageView.get().getId() == OvVK::ImageView::empty.getId()) {
        OV_LOG_ERROR("Fail to create the image view of the texture with id= %d", getId());
        this->free();
        return false;
    }

    // Done
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the image obj containing the texture.
 * @return The reference to the image obj containing the texture.
 */
const OvVK::Image OVVK_API& OvVK::Texture::getImage() const 
{
    return reserved->image;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the image view obj used to access the texture.
 * @return The reference to the image view obj used to access the texture.
 */
const OvVK::ImageView OVVK_API& OvVK::Texture::getImageView() const
{
    return reserved->imageView;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the reference to the bitmap buffer.
 * @return The reference to the bitmap buffer.
 */
const OvVK::Buffer OVVK_API& OvVK::Texture::getBitmapBuffer() const
{
    return reserved->bitmapBuffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the lutpos.
 * @return The lutpos
 */
uint32_t OVVK_API OvVK::Texture::getLutPos() const
{
    return reserved->lutPos;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the sampler used to access the texture.
 * @return The sampler used to access the texture.
 */
const OvVK::Sampler OVVK_API& OvVK::Texture::getSampler() const
{
    return reserved->sampler.get();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns Vulkan region objs to copy from bitmap buffer to image. 
 * @return A vector containing the Vulkan region objs to copy from bitmap buffer to image. 
 */
const std::vector<VkBufferImageCopy> OVVK_API& OvVK::Texture::getBufferImageCopyRegions() const
{
    return reserved->bufferImageCopyRegions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets if the texture needs mipmap to be generates.
 * @return TF
 */
bool OVVK_API OvVK::Texture::getNeedGenerateMipmap() const
{
    return reserved->needGenerateMipmap;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sampler used to access the texture.
 * @param sampler The reference to the sampler that will be use to access the texture.
 * @return TF
 */
bool OVVK_API OvVK::Texture::setSampler(const OvVK::Sampler& sampler)
{
    // Safety net
    if (!sampler.isInitialized()) {
        OV_LOG_ERROR("Sampler with id= %d not initialized in texture with id= %d.", sampler.getId(), getId());
        return false;
    }

    if (sampler.getVkSampler() == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Sampler with id= %d has null Vulkan sampler in texture with id= %d.", sampler.getId(), getId());
        return false;
    }

    reserved->sampler = sampler;

    this->Ov::Object::setDirty(true);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the texture's image layout.
 * @param imageLayout The image layout to set to the texture's image.
 * @return TF
 */
bool OVVK_API OvVK::Texture::setImageLayout(VkImageLayout imageLayout) 
{
    return reserved->image.setImageLayout(imageLayout);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the queue family index of the queue owning the texture's image.
 * @param queueFamilyIndex The index of the queue owning the texture's image.
 * @return TF
 */
bool OVVK_API OvVK::Texture::setQueueFamilyIndex(uint32_t queueFamilyIndex) 
{
    return reserved->image.setQueueFamilyIndex(queueFamilyIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the regions to copy from the bitmap buffer to the texture's image.
 * @param regions A vector containing Vulkan region objs to copy from the bitmap buffer to the texture's image.
 * @return TF
 */
bool OvVK::Texture::setBufferImageCopyRegions(std::vector<VkBufferImageCopy>& regions) 
{
    if (reserved->bitmapBuffer.getVkBuffer() == VK_NULL_HANDLE)
    {
        OV_LOG_ERROR("Buffer of the texture with id= %d not created.", getId());
        return false;
    }

    reserved->bufferImageCopyRegions = regions;

    // done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the status of the texture's image.
 * @param value The uint value rapresenting the status the texture's image is in.
 * @return TF
 */
bool OVVK_API OvVK::Texture::setStatusSyncObjValue(uint64_t value)
{
    if(reserved->bitmapBuffer.getVkBuffer() != VK_NULL_HANDLE)
        return reserved->bitmapBuffer.setStatusSyncObjValue(value) && reserved->image.setStatusSyncObjValue(value);

    return reserved->image.setStatusSyncObjValue(value);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set the sync obj for the lock.
 * @param fence The obj to check to know the lock status.
 * @return TF
 */
bool OVVK_API OvVK::Texture::setLockSyncObj(VkFence fence)
{
    return reserved->image.setLockSyncObj(fence);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set if the texture needs mipmap to be generated.
 * @param value TF
 */
void OVVK_API OvVK::Texture::setNeedGenerateMipmap(bool value) 
{
    reserved->needGenerateMipmap = value;
}

#pragma endregion

#pragma region Render

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::Texture::render(uint32_t value, void* data) const
{
    OV_LOG_ERROR("Texture empty rendering method called");
    return false;
}

#pragma endregion