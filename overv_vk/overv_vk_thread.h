#pragma once
/**
 * @file	overv_vk_thread.h
 * @brief	Definition of thread in overv_vk
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

/**
 * @brief Class for represent a thread inside the overv_vulkan engine.
 */
class OVVK_API Thread : public Ov::Object
{
//////////
public: //
//////////

	// Static
	static Thread empty;

	// Const/dest:
	Thread();
	Thread(Thread&& other) noexcept;
	Thread(Thread const&) = delete;
	virtual ~Thread();

	// Operators
	Thread& operator=(const Thread&) = delete;
	Thread& operator=(Thread&&) = delete;

	// General:
	void addJob(std::function<void()> function);
	void wait();

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// General:
	void queueLoop();

	// Const/dest:
	Thread(const std::string& name);
};