/**
 * @file	overv_vk_pipeline_gui.h
 * @brief	GUI pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


 //////////////
 // #INCLUDE //
 //////////////

// Main inlcude:
#include "overv_vk.h"

// ImGui
#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_vulkan.h"
#include "imgui_internal.h"
#include "imconfig.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief GUI pipeline class reserved structure.
 */
struct OvVK::GUIPipeline::Reserved
{
    VkDescriptorPool descriptorPool;                                        ///< Descriptor pool used by ImGUI.
    VkRenderPass renderPass;                                                ///< RenderPass used by ImGUI (needed because of renderization).
    std::vector<VkFramebuffer> framebuffers;                                ///< Frame buffers used by ImGUI needed for renderization.
    std::vector<VkSemaphore> ownershipRelease;                              ///< GPU sync obj to release the swapchain image.

    std::map<uint32_t, std::reference_wrapper<Ov::DrawGUI>> drawGUIObjects; ///< All the elemente that need to be draw.

    bool contextCreated = false;                                            ///< Context is created.
    bool rederingStarted = false;                                           ///< Rendernis started. To know witch action to do.

    /**
     * Constructor
     */
    Reserved() : descriptorPool{ VK_NULL_HANDLE },
        renderPass{ VK_NULL_HANDLE },
        contextCreated{ false },
        rederingStarted{ false }
    {
        // framebuffers default values
        framebuffers.resize(OvVK::Engine::nrFramesInFlight, VK_NULL_HANDLE);
        ownershipRelease.resize(OvVK::Engine::nrFramesInFlight, VK_NULL_HANDLE);
    }
};


///////////////////////////////
// BODY OF CLASS GUIPipeline //
///////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::GUIPipeline::GUIPipeline() :
    reserved(std::make_unique<OvVK::GUIPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::GUIPipeline::GUIPipeline(const std::string& name) : OvVK::Pipeline(name),
reserved(std::make_unique<OvVK::GUIPipeline::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::GUIPipeline::GUIPipeline(GUIPipeline&& other) :
    OvVK::Pipeline(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::GUIPipeline::~GUIPipeline()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes buffer.
 * @return TF
 */
bool OVVK_API OvVK::GUIPipeline::init()
{
    // init method base class call
    if (this->OvVK::Pipeline::init() == false)
        return false;


    // Destroy descriptorPool
    if (reserved->descriptorPool != VK_NULL_HANDLE)
    {
        vkDestroyDescriptorPool(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
            reserved->descriptorPool, nullptr);
        reserved->descriptorPool = VK_NULL_HANDLE;
    }

    // Destroy renderPass
    if (reserved->renderPass != VK_NULL_HANDLE) 
    {
        vkDestroyRenderPass(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
            reserved->renderPass, nullptr);
        reserved->renderPass = VK_NULL_HANDLE;
    }

    // Destroy Framebuffers
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
        if (reserved->framebuffers[c] != VK_NULL_HANDLE) 
        {
            vkDestroyFramebuffer(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
                reserved->framebuffers[c], nullptr);
            reserved->framebuffers[c] = VK_NULL_HANDLE;
        }

    // Destroy semaphore
    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++)
    {
        vkDestroySemaphore(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->ownershipRelease[c], nullptr);
        reserved->ownershipRelease[c] = VK_NULL_HANDLE;
    }

    // Free ImGUI
    if (reserved->rederingStarted)
    {
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
    }

    if (reserved->contextCreated)
        ImGui::DestroyContext();


    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Frees the OvVK buffer.
 * @return TF
 */
bool OVVK_API OvVK::GUIPipeline::free()
{
    // free method base class call
    if (this->OvVK::Pipeline::free() == false)
        return false;

    
    // Destroy descriptorPool
    if (reserved->descriptorPool != VK_NULL_HANDLE)
    {
        vkDestroyDescriptorPool(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
            reserved->descriptorPool, nullptr);
        reserved->descriptorPool = VK_NULL_HANDLE;
    }

    // Destroy renderPass
    if (reserved->renderPass != VK_NULL_HANDLE)
    {
        vkDestroyRenderPass(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
            reserved->renderPass, nullptr);
        reserved->renderPass = VK_NULL_HANDLE;
    }

    // Destroy Framebuffers
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
        if (reserved->framebuffers[c] != VK_NULL_HANDLE)
        {
            vkDestroyFramebuffer(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
                reserved->framebuffers[c], nullptr);
            reserved->framebuffers[c] = VK_NULL_HANDLE;
        }

    // Destroy semaphore
    for (uint32_t c = 0; c < Engine::nrFramesInFlight; c++)
    {
        vkDestroySemaphore(Engine::getInstance().getLogicalDevice().getVkDevice(), reserved->ownershipRelease[c], nullptr);
        reserved->ownershipRelease[c] = VK_NULL_HANDLE;
    }

    // Free ImGUI
    if (reserved->rederingStarted)
    {
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
    }

    if (reserved->contextCreated)
        ImGui::DestroyContext();


    // Done:
    return true;
}

#pragma endregion

#pragma region SurfaceDependencies

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the keyboard callback is forwarded.
 * @param key The keyboard key that was pressed or released.
 * @param scancode The system-specific scancode of the key.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
bool OVVK_API OvVK::GUIPipeline::keyboardCallback(int key, int scancode, int action, int mods)
{
    ImGui_ImplGlfw_KeyCallback((GLFWwindow*)Engine::getInstance().getPresentSurface().getGLFWWindow(), key, scancode, action, mods);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse cursor callback is forwarded.
 * @param mouseX The new cursor x-coordinate, relative to the left edge of the content area.
 * @param mouseY The new cursor y-coordinate, relative to the top edge of the content area.
 */
bool OVVK_API OvVK::GUIPipeline::mouseCursorCallback(double mouseX, double mouseY)
{
    ImGui_ImplGlfw_CursorPosCallback((GLFWwindow*)Engine::getInstance().getPresentSurface().getGLFWWindow(), mouseX, mouseY);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse button callback is forwarded.
 * @param button The mouse button that was pressed or released.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
bool OVVK_API OvVK::GUIPipeline::mouseButtonCallback(int button, int action, int mods)
{
    ImGui_ImplGlfw_MouseButtonCallback((GLFWwindow*)Engine::getInstance().getPresentSurface().getGLFWWindow(), button, action, mods);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse scroll callback is forwarded.
 * @param scrollX The scroll offset along the x-axis.
 * @param scrollY The scroll offset along the y-axis.
 */
bool OVVK_API OvVK::GUIPipeline::mouseScrollCallback(double scrollX, double scrollY)
{
    ImGui_ImplGlfw_ScrollCallback((GLFWwindow*)Engine::getInstance().getPresentSurface().getGLFWWindow(), scrollX, scrollY);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the window size callback is forwarded.
 * @param width New width.
 * @param height New heigth.
 */
bool OVVK_API OvVK::GUIPipeline::windowSizeCallback(int width, int height)
{
    // Destroy Framebuffers
    for (uint32_t c = 0; c < OvVK::Engine::nrFramesInFlight; c++)
        if (reserved->framebuffers[c] != VK_NULL_HANDLE)
        {
            vkDestroyFramebuffer(OvVK::Engine::getInstance().getLogicalDevice().getVkDevice(),
                reserved->framebuffers[c], nullptr);
            reserved->framebuffers[c] = VK_NULL_HANDLE;
        }

    // Gather resources
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Swapchain
    std::reference_wrapper<const OvVK::Swapchain> swapchain = engine.get().getSwapchain();

    ////////////////////////
    // Create FrameBuffer //
    ////////////////////////

    VkFramebufferCreateInfo infoFrameBuffer = {};
    infoFrameBuffer.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    infoFrameBuffer.pNext = NULL;
    infoFrameBuffer.renderPass = reserved->renderPass;
    infoFrameBuffer.attachmentCount = 1;
    // New dimension. Two possibility
    //infoFrameBuffer.width = width;
    //infoFrameBuffer.height = height;
    infoFrameBuffer.width = swapchain.get().getSwapChainExtent().width;
    infoFrameBuffer.height = swapchain.get().getSwapChainExtent().height;
    infoFrameBuffer.layers = 1;

    for (uint32_t c = 0; c < swapchain.get().getNrOfSwapchainImages(); c++)
    {
        VkImageView attachment[] =
        {
            swapchain.get().getSwapchainImageView(c)
        };

        infoFrameBuffer.pAttachments = attachment;

        if (vkCreateFramebuffer(logicalDevice.get().getVkDevice(),
            &infoFrameBuffer, nullptr, &reserved->framebuffers[c]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the framebuffer of the ImGUI for the swapchain image
 nr. = %d in gui pipeline with id= %d.)", c, getId());
            this->free();
            return false;
        }
    }

    // To override MinImageCount after initialization (e.g. if swap chain is recreated)
    ImGui_ImplVulkan_SetMinImageCount(swapchain.get().getNrOfSwapchainImages());

    // Done:
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create gui pipeline.
 * @return TF
 */
bool OvVK::GUIPipeline::create() 
{

    // Init
    if (this->init() == false)
        return false;

    // Gather resources
    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Vulkan instance
    std::reference_wrapper<const OvVK::VulkanInstance> instance = engine.get().getVulkanInstance();
    // Presentation surface
    std::reference_wrapper<OvVK::Surface> presentSurface = engine.get().getPresentSurface();
    // Logical device
    std::reference_wrapper<const OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();
    // Swapchain
    std::reference_wrapper<const OvVK::Swapchain> swapchain = engine.get().getSwapchain();

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForVulkan((GLFWwindow*)presentSurface.get().getGLFWWindow(), false);


    ///////////////////////////
    // Create DescriptorPool //
    ///////////////////////////

    // Create descriptor pool for IMGUI
    // the size of the pool is very oversize, but it's copied from imgui demo itself.
    VkDescriptorPoolSize pool_sizes[] =
    {
        { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
    };

    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptorPoolCreateInfo.maxSets = 1000;
    descriptorPoolCreateInfo.poolSizeCount = std::size(pool_sizes);
    descriptorPoolCreateInfo.pPoolSizes = pool_sizes;
    if (vkCreateDescriptorPool(logicalDevice.get().getVkDevice(),
        &descriptorPoolCreateInfo, nullptr, &reserved->descriptorPool) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create descriptor pool for ImGUI in gui pipeline with id= %d.", getId());
        this->free();
        return false;
    }


    ///////////////////////
    // Create RenderPass //
    ///////////////////////

    VkAttachmentDescription attachmentDescription = {};
    attachmentDescription.format = swapchain.get().getSwapchainImageFormat();
    attachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
    attachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    attachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDescription.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    // Color attachment
    VkAttachmentReference attachmentReference = {};
    // index in VkRenderPassCreateInfo::pAttachments
    attachmentReference.attachment = 0;
    attachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpassDescription = {};
    subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDescription.colorAttachmentCount = 1;
    subpassDescription.pColorAttachments = &attachmentReference;

    VkSubpassDependency subpassDependency = {};
    subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    subpassDependency.dstSubpass = 0;
    subpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    subpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    subpassDependency.srcAccessMask = VK_ACCESS_NONE;  // or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    subpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkRenderPassCreateInfo renderPassCreateInfo = {};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCreateInfo.attachmentCount = 1;
    renderPassCreateInfo.pAttachments = &attachmentDescription;
    renderPassCreateInfo.subpassCount = 1;
    renderPassCreateInfo.pSubpasses = &subpassDescription;
    renderPassCreateInfo.dependencyCount = 1;
    renderPassCreateInfo.pDependencies = &subpassDependency;

    if (vkCreateRenderPass(logicalDevice.get().getVkDevice(),
        &renderPassCreateInfo, nullptr, &reserved->renderPass) != VK_SUCCESS) {
        OV_LOG_ERROR("Could not create ImGui's render pass in gui pipeline with id= %d.", getId());
        this->free();
        return false;
    }


    /////////////////////////////////////
    // ImGUI initialization for Vulkan //
    /////////////////////////////////////

    ImGui_ImplVulkan_InitInfo imGui_ImplVulkan_InitInfo = {};
    imGui_ImplVulkan_InitInfo.Instance = instance.get().getVkInstance();
    imGui_ImplVulkan_InitInfo.PhysicalDevice = logicalDevice.get().getPhysicalDevice().getPhysicalDevice();
    imGui_ImplVulkan_InitInfo.Device = logicalDevice.get().getVkDevice();
    imGui_ImplVulkan_InitInfo.QueueFamily = logicalDevice.get().getQueueFamiliesIndices().value().graphicsFamily.value();
    imGui_ImplVulkan_InitInfo.Queue = logicalDevice.get().getQueueHandles().value().graphicsQueue;
    imGui_ImplVulkan_InitInfo.PipelineCache = VK_NULL_HANDLE;
    imGui_ImplVulkan_InitInfo.DescriptorPool = reserved->descriptorPool;
    imGui_ImplVulkan_InitInfo.Allocator = nullptr;
    imGui_ImplVulkan_InitInfo.MinImageCount = swapchain.get().getNrOfSwapchainImages();
    imGui_ImplVulkan_InitInfo.ImageCount = swapchain.get().getNrOfSwapchainImages();
    imGui_ImplVulkan_InitInfo.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
    imGui_ImplVulkan_InitInfo.CheckVkResultFn = checkVkResult;

    if (ImGui_ImplVulkan_Init(&imGui_ImplVulkan_InitInfo, reserved->renderPass) == false)
    {
        OV_LOG_ERROR("Fail to init ImGUI for Vulkan in gui pipeline with id= %d.", getId());
        this->free();
        return false;
    }


    ////////////////////////
    // Create FrameBuffer //
    ////////////////////////

    VkFramebufferCreateInfo infoFrameBuffer = {};
    infoFrameBuffer.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    infoFrameBuffer.pNext = NULL;
    infoFrameBuffer.renderPass = reserved->renderPass;
    infoFrameBuffer.attachmentCount = 1;
    infoFrameBuffer.width = swapchain.get().getSwapChainExtent().width;
    infoFrameBuffer.height = swapchain.get().getSwapChainExtent().height;
    infoFrameBuffer.layers = 1;

    for (uint32_t c = 0; c < swapchain.get().getNrOfSwapchainImages(); c++)
    {
        VkImageView attachment[] =
        {
            swapchain.get().getSwapchainImageView(c)
        };

        infoFrameBuffer.pAttachments = attachment;

        if (vkCreateFramebuffer(logicalDevice.get().getVkDevice(),
            &infoFrameBuffer, nullptr, &reserved->framebuffers[c]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create the Framebuffer of the ImGUI for the swapchain image nr. = %d
 in gui pipeline with id= %d.)", c, getId());
            this->free();
            return false;
        }
    }


    /////////////////////////
    // Create font Texture //
    /////////////////////////

    // Get primary command pool
    std::reference_wrapper<OvVK::CommandPool> graphicCP =
        engine.get().getPrimaryGraphicCmdPool();
    // Create commandBuffer to perform the operation
    std::reference_wrapper<OvVK::CommandBuffer> graphicCB = 
        graphicCP.get().allocateCommandBuffer();

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (graphicCB.get().beginVkCommandBuffer(beginInfo) == false ||
        graphicCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR(R"(Fail to begin or change status of the command buffer use to create font texture
 for ImGUI in gui pipeline with id= %d.)", getId());
        this->free();
        return false;
    }

    // All command are recorded by ImGUI
    ImGui_ImplVulkan_CreateFontsTexture(graphicCB.get().getVkCommandBuffer());

    if (graphicCB.get().endVkCommandBuffer() == false ||
        graphicCB.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false) 
    {
        OV_LOG_ERROR(R"(Fail to end or change status of the command buffer use to create font texture
            for ImGUI in gui pipeline with id= %d.)", getId());
        this->free();
        return false;
    }


    /////////////////////
    // Synchronization //
    /////////////////////

    VkSubmitInfo2 submitInfo2 = {};
    submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
    submitInfo2.pNext = VK_NULL_HANDLE;
    submitInfo2.flags = 0x00000000;
    submitInfo2.waitSemaphoreInfoCount = 0;
    submitInfo2.pWaitSemaphoreInfos = VK_NULL_HANDLE;
    submitInfo2.signalSemaphoreInfoCount = 0;
    submitInfo2.pSignalSemaphoreInfos = VK_NULL_HANDLE;

    VkCommandBufferSubmitInfo pCommandBufferInfos;
    pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
    pCommandBufferInfos.pNext = VK_NULL_HANDLE;
    pCommandBufferInfos.commandBuffer = graphicCB.get().getVkCommandBuffer();
    // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
    pCommandBufferInfos.deviceMask = 0;

    submitInfo2.commandBufferInfoCount = 1;
    submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

    // Create sync CPU Obj.
    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.pNext = VK_NULL_HANDLE;
    fenceCreateInfo.flags = 0X00000000;

    VkFence syncObj = VK_NULL_HANDLE;
    if (vkCreateFence(logicalDevice.get().getVkDevice(), &fenceCreateInfo, nullptr, &syncObj) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to create Fence to check ImGUI font texture creation in gui pipeline with id= %d.", getId());
        this->free();
        return false;
    }

    if (vkQueueSubmit2(logicalDevice.get().getQueueHandles().value().graphicsQueue,
        1, &submitInfo2, syncObj) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to submit the command buffer to create ImGUI font textures in gui pipeline with id= %d.", getId());
        this->free();
        return false;
    }

    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &syncObj, VK_TRUE, UINT64_MAX) != VK_SUCCESS) 
    {
        OV_LOG_ERROR("Fail to wait the command buffer used to create ImGUI font Textures in gui pipeline with id= %d.", getId());
        this->free();
        return false;
    }

    // Clean resources
    vkDestroyFence(logicalDevice.get().getVkDevice(), syncObj, nullptr);
    if (graphicCP.get().freeCommandBuffer(graphicCB.get()) == false) 
        OV_LOG_ERROR("Fail to free command buffer used to create ImGUI font textures in gui pipeline with id= %d.", getId());

    // add Pipeline to engine
    engine.get().addPipeline(*this);

    // Surface dependent
    OvVK::Surface::SurfaceCallbackTypes activeCallback = OvVK::Surface::SurfaceCallbackTypes::keyboard |
        OvVK::Surface::SurfaceCallbackTypes::mouseCursor | OvVK::Surface::SurfaceCallbackTypes::mouseButton |
        OvVK::Surface::SurfaceCallbackTypes::mouseScroll | OvVK::Surface::SurfaceCallbackTypes:: windowSize;
    setActiveCallback(activeCallback);
    presentSurface.get().addSurfaceDependentObject(getId(), *this);
    setIsActive(true);

    reserved->contextCreated = true;

    //Done:
    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds a Ov::Draw obj to the gui pipeline.
 * @param id The Ov::Draw obj id to add.
 * @return TF
 */
bool OVVK_API OvVK::GUIPipeline::addDrawGUIElement(uint32_t id, Ov::DrawGUI& object)
{
    std::reference_wrapper<Ov::DrawGUI> surfaceResizableObject = object;

    // Check if already present
    if (reserved->drawGUIObjects.count(id) == 0)
        reserved->drawGUIObjects.insert(std::make_pair(id, surfaceResizableObject));
    else
        OV_LOG_WARN("The ov draw obj is already present in gui pipeline with id= %d.", getId());

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Removes a Ov::Draw obj from the gui pipeline.
 * @param id The Ov::Draw obj id to remove.
 * @return TF
 */
bool OVVK_API OvVK::GUIPipeline::removeDrawGUIElement(uint32_t id)
{
    if (!reserved->drawGUIObjects.empty())
        for (auto it = reserved->drawGUIObjects.begin(); it != reserved->drawGUIObjects.end(); ++it)
            if (it->first == id)
            {
                reserved->drawGUIObjects.erase(it);
                break;
            }

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the descriptor pool.
 * @return The Vulkan descriptor pool of the gui pipeline.
 */
VkDescriptorPool OVVK_API OvVK::GUIPipeline::getDescriptorPool() const
{
    return reserved->descriptorPool;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the render pass.
 * @return The Vulkan render pass of the gui pipeline.
 */
VkRenderPass OVVK_API OvVK::GUIPipeline::getRenderPass() const
{
    return reserved->renderPass;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the frame buffers.
 * @return A vector containing the Vulkan frame buffers of the gui pipeline.
 */
const std::vector<VkFramebuffer> OVVK_API& OvVK::GUIPipeline::getFramebuffers() const
{
    return reserved->framebuffers;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::GUIPipeline::render(uint32_t value, void* data) 
{

    if (isDirty())
        this->setDirty(false);

    // Engine
    std::reference_wrapper<OvVK::Engine> engine = OvVK::Engine::getInstance();
    // Logical Device
    std::reference_wrapper<OvVK::LogicalDevice> logicalDevice = engine.get().getLogicalDevice();

    uint32_t frameInFlight = engine.get().getFrameInFlight();


    // Wait CPU render sync object
    // Wait fences to know the work is finished
    VkFence fenceToWait = getStatusSyncObjCPU();
    if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &fenceToWait,
        VK_TRUE, UINT64_MAX) != VK_SUCCESS)
    {
        OV_LOG_ERROR("Fail to wait render Fence in lighting pipeline with id= %d.", getId());
        return false;
    }


    if (value == 0) 
    {
        // ImGUI init frame
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // Set value status sync obj GPU.
        setStatusSyncObjGPU(OvVK::Pipeline::renderingValueStatusSyncObj);
        reserved->rederingStarted = true;
    }
    else if (value == 1)
    {
        ////////////////////
        // Reset syncObjs //
        ////////////////////
        
        // Reset sync obj CPU.
        if (vkResetFences(logicalDevice.get().getVkDevice(), 1,
            &fenceToWait) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to reset the CPU sync obj of the frame in flight number %d of the GUIPipeline with id= %d.",
                frameInFlight, getId());
            return false;
        }

        // Sync Obj ownership change
        // Binary semaphore
        VkSemaphoreCreateInfo createInfo;
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        createInfo.pNext = VK_NULL_HANDLE;
        createInfo.flags = 0;

        if (reserved->ownershipRelease[frameInFlight] == VK_NULL_HANDLE &&
            vkCreateSemaphore(logicalDevice.get().getVkDevice(), &createInfo, NULL,
                &reserved->ownershipRelease[frameInFlight]) != VK_SUCCESS)
        {
            OV_LOG_ERROR(R"(Fail to create semaphore to sync transfer ownership in lighting pipeline with id= %d)", getId());
            return false;
        }

        // Swapchain
        std::reference_wrapper<OvVK::Swapchain> swapchain = engine.get().getSwapchain();

        // Get the target image of the swapchain
        uint32_t presentableImageIndex = engine.get().getPresentableImageIndex(frameInFlight);
        VkImage presentableImage = swapchain.get().getSwapchainImage(presentableImageIndex);

        // Check if is VK_NULL_HANDLE. If not no need to check futher data gather
        // from swapchain beacause the index is valid.
        if (presentableImage == VK_NULL_HANDLE) {
            OV_LOG_ERROR("Failed to retriever swapchain image with index= %d in the gui pipeline with id= %d.",
                presentableImageIndex, getId());
            return false;
        }

        VkImageLayout presentableImageLayout =
            swapchain.get().getSwapchainImageLayout(presentableImageIndex).value();

        // Get queue families index
        std::optional<OvVK::LogicalDevice::QueueFamilyIndices> queueFamilyIndices =
            logicalDevice.get().getQueueFamiliesIndices();


        ////////////////////////////
        // Record/Submit commands //
        ////////////////////////////

        VkCommandBufferBeginInfo commandBufferBeginInfo{};
        commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT specifies that each recording of the command buffer will
        //      only be submitted once, and the command buffer will be reset and recorded again between each submission.
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        // CommandBuffer needed to add the ImGUI to the final rendered image.
        std::reference_wrapper<OvVK::CommandPool> graphicCommandPool = engine.get().getPrimaryGraphicCmdPool();
        std::reference_wrapper<OvVK::CommandBuffer> graphicCommandBuffer = graphicCommandPool.get().allocateCommandBuffer();

        if (graphicCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
            graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR(R"(Fail to begin or change status to the graphic command buffer to render
 GUIPipeline in gui pipeline with id= %d.)", getId());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            return false;
        }

        bool needPreparation = false;

        // CommandBuffer needed to change the ownership of the swapchain image.
        if (presentableImageLayout != VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL ||
            swapchain.get().getSwapchainImageOwnership(presentableImageIndex).has_value() == true &&
            swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value() != queueFamilyIndices.value().graphicsFamily.value())
        {
            // Swapchain image take ownership
            VkImageMemoryBarrier2 imageMemoryBarrier2 = {};
            imageMemoryBarrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
            // pNext is NULL or a pointer to a structure extending this structure.
            imageMemoryBarrier2.pNext = VK_NULL_HANDLE;
            // Wait previous stage for changing the texture layout
            // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
            imageMemoryBarrier2.srcStageMask = VK_PIPELINE_STAGE_2_NONE;
            // Wait on specific memory access.
            imageMemoryBarrier2.srcAccessMask = VK_ACCESS_2_NONE;
            // All commands need to wait that the texture change layout.
            // VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT 
            imageMemoryBarrier2.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
            // All commands need to wait that the texture change layout.
            imageMemoryBarrier2.dstAccessMask = VK_ACCESS_2_NONE;

            if (presentableImageLayout != VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
            {
                // Current texture layout
                imageMemoryBarrier2.oldLayout = presentableImageLayout;
                // Set the layout that allow a fresh write to the texture.
                imageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            }
            else
            {
                // Current texture layout
                imageMemoryBarrier2.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                // Set the layout that allow a fresh write to the texture.
                imageMemoryBarrier2.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            }

            // No need to transfer ownership because first time used.
            imageMemoryBarrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            imageMemoryBarrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            // The image and subresourceRange specify the image that is affected and the specific part of the image.
            imageMemoryBarrier2.image = presentableImage;
            imageMemoryBarrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            // baseMipLevel is the first mipmap level accessible to the view.
            imageMemoryBarrier2.subresourceRange.baseMipLevel = 0;
            imageMemoryBarrier2.subresourceRange.levelCount = 1;
            imageMemoryBarrier2.subresourceRange.baseArrayLayer = 0;
            imageMemoryBarrier2.subresourceRange.layerCount = 1;

            // Barrier specification
            VkDependencyInfo imageDependencyInfo = {};
            imageDependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
            imageDependencyInfo.dependencyFlags = 0x00000000;
            imageDependencyInfo.memoryBarrierCount = 0;
            imageDependencyInfo.pMemoryBarriers = VK_NULL_HANDLE;
            imageDependencyInfo.bufferMemoryBarrierCount = 0;
            imageDependencyInfo.pBufferMemoryBarriers = VK_NULL_HANDLE;
            imageDependencyInfo.imageMemoryBarrierCount = 1;
            imageDependencyInfo.pImageMemoryBarriers = &imageMemoryBarrier2;

            // CommandBuffer needed to change the ownership of the swapchain image.
            if (swapchain.get().getSwapchainImageOwnership(presentableImageIndex).has_value() == true &&
                swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value() != queueFamilyIndices.value().graphicsFamily.value())
            {
                needPreparation = true;

                std::reference_wrapper<OvVK::CommandPool> ownershipCommandPool = OvVK::CommandPool::empty;

                // Select command pool from which allocate the command buffer to change ownership.
                if (queueFamilyIndices.value().graphicsFamily.has_value() == true &&
                    queueFamilyIndices.value().graphicsFamily.value() == swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value())
                    ownershipCommandPool = engine.get().getPrimaryGraphicCmdPool();
                else if (queueFamilyIndices.value().transferFamily.has_value() == true &&
                    queueFamilyIndices.value().transferFamily.value() == swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value())
                    ownershipCommandPool = engine.get().getPrimaryTransferCmdPool();
                else if (queueFamilyIndices.value().computeFamily.has_value() == true &&
                    queueFamilyIndices.value().computeFamily.value() == swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value())
                    ownershipCommandPool = engine.get().getPrimaryComputeCmdPool();
                else {
                    OV_LOG_ERROR(R"(In the queue family indices retriever from the logical device there is not the queue owning the image
 with id= %d in the swapchain with id= %d in gui pipeline method.)", presentableImageIndex, getId());
                    graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
                    return false;
                }

                std::reference_wrapper<OvVK::CommandBuffer> releasePresImgOwnershipCommandBuffer =
                    ownershipCommandPool.get().allocateCommandBuffer();

                if (releasePresImgOwnershipCommandBuffer.get().beginVkCommandBuffer(commandBufferBeginInfo) == false ||
                    releasePresImgOwnershipCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::recordingValueStatusSyncObj) == false)
                {
                    OV_LOG_ERROR(R"(Fail to begin or change status to the release ownership command buffer in the swapchain with
 id= %d in gui pipeline method.)", getId());
                    ownershipCommandPool.get().freeCommandBuffer(releasePresImgOwnershipCommandBuffer.get());
                    graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
                    return false;
                }

                // Chnage of ownership
                imageMemoryBarrier2.srcQueueFamilyIndex = swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value();
                imageMemoryBarrier2.dstQueueFamilyIndex = queueFamilyIndices.value().graphicsFamily.value();

                vkCmdPipelineBarrier2(releasePresImgOwnershipCommandBuffer.get().getVkCommandBuffer(), &imageDependencyInfo);

                // End commandBuffer
                if (releasePresImgOwnershipCommandBuffer.get().endVkCommandBuffer() == false ||
                    releasePresImgOwnershipCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
                {
                    OV_LOG_ERROR(R"(Fail to end or change status to the release ownership command buffer in the swapchain with
 id= %d in gui pipeline method.)", getId());
                    ownershipCommandPool.get().freeCommandBuffer(releasePresImgOwnershipCommandBuffer.get());
                    graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
                    return false;
                }


                ////////////
                // Submit //
                ////////////

                VkSubmitInfo2 submitInfo2 = {};
                submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
                submitInfo2.pNext = VK_NULL_HANDLE;
                submitInfo2.flags = 0x00000000;

                // Wait all pipeline the pipeline depend on.
                std::vector<VkSemaphore> toWaitSemaphore = getStatusSyncObjsGPUToWait();
                std::vector<uint64_t> waitValue(toWaitSemaphore.size(), OvVK::Pipeline::renderingDoneValueStatusSyncObj);
                std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(toWaitSemaphore.size());
                for (uint32_t c = 0; c < toWaitSemaphore.size(); c++) {
                    pWaitSemaphoreInfos[c].sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
                    pWaitSemaphoreInfos[c].pNext = VK_NULL_HANDLE;
                    pWaitSemaphoreInfos[c].semaphore = toWaitSemaphore[c];
                    pWaitSemaphoreInfos[c].value = waitValue[c];
                    // All command s need to wait
                    // VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT 
                    pWaitSemaphoreInfos[c].stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                    // If the device that semaphore was created on is not a device group, deviceIndex must be 0
                    pWaitSemaphoreInfos[c].deviceIndex = 0;
                }
                submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
                submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

                VkSemaphoreSubmitInfo pSignalSemaphoreInfos = {};
                pSignalSemaphoreInfos.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
                pSignalSemaphoreInfos.pNext = VK_NULL_HANDLE;
                // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT 
                pSignalSemaphoreInfos.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
                pSignalSemaphoreInfos.semaphore = reserved->ownershipRelease[frameInFlight];
                // binary semaphores
                pSignalSemaphoreInfos.value = 0;
                // If the device that semaphore was created on is not a device group, deviceIndex must be 0
                pSignalSemaphoreInfos.deviceIndex = 0;

                submitInfo2.signalSemaphoreInfoCount = 1;
                submitInfo2.pSignalSemaphoreInfos = &pSignalSemaphoreInfos;

                VkCommandBufferSubmitInfo pCommandBufferInfos;
                pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
                pCommandBufferInfos.pNext = VK_NULL_HANDLE;
                pCommandBufferInfos.commandBuffer = releasePresImgOwnershipCommandBuffer.get().getVkCommandBuffer();
                // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
                pCommandBufferInfos.deviceMask = 0;

                submitInfo2.commandBufferInfoCount = 1;
                submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

                // Submit
                if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(swapchain.get().getSwapchainImageOwnership(presentableImageIndex).value()),
                    1, &submitInfo2, NULL) != VK_SUCCESS)
                {
                    OV_LOG_ERROR(R"(Fail to submit the release ownership command buffer in the swapchain with
 id= %d in gui pipeline method.)", getId());
                    ownershipCommandPool.get().freeCommandBuffer(releasePresImgOwnershipCommandBuffer.get());
                    graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
                    return false;
                }
            }
            
            vkCmdPipelineBarrier2(graphicCommandBuffer.get().getVkCommandBuffer(), &imageDependencyInfo);
        }

        // Register ImGUI draw commands
        VkRenderPassBeginInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        info.renderPass = reserved->renderPass;
        info.framebuffer = reserved->framebuffers[presentableImageIndex];
        info.renderArea.extent.width = swapchain.get().getSwapChainExtent().width;
        info.renderArea.extent.height = swapchain.get().getSwapChainExtent().height;
        info.clearValueCount = 0;
        info.pClearValues = nullptr;
        vkCmdBeginRenderPass(graphicCommandBuffer.get().getVkCommandBuffer(), &info, VK_SUBPASS_CONTENTS_INLINE);

		// render your GUI
		ImGui::Begin("OvVK");

		for (auto [key, value] : reserved->drawGUIObjects)
			value.get().drawGUI();

		ImGui::End();

        ImGui::Render();
        ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), graphicCommandBuffer.get().getVkCommandBuffer());

        vkCmdEndRenderPass(graphicCommandBuffer.get().getVkCommandBuffer());

        // End commandBuffer
        if (graphicCommandBuffer.get().endVkCommandBuffer() == false ||
            graphicCommandBuffer.get().setStatusSyncObjValue(OvVK::CommandBuffer::readyValueStatusSyncObj) == false)
        {
            OV_LOG_ERROR("Fail to end or change status to the graphic command buffer to render GUIPipeline in gui pipeline with id= %d.",
                getId());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            return false;
        }


        ////////////
        // Submit //
        ////////////

        VkSubmitInfo2 submitInfo2 = {};
        submitInfo2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo2.pNext = VK_NULL_HANDLE;
        submitInfo2.flags = 0x00000000;

        // Wait all pipeline the pipeline depend on.
        std::vector<VkSemaphore> toWaitSemaphore = getStatusSyncObjsGPUToWait();
        std::vector<uint64_t> waitValue(toWaitSemaphore.size(), OvVK::Pipeline::renderingDoneValueStatusSyncObj);
        if (needPreparation)
        {
            toWaitSemaphore.push_back(reserved->ownershipRelease[frameInFlight]);
            // Binary
            waitValue.push_back(0);
        }
        std::vector<VkSemaphoreSubmitInfo> pWaitSemaphoreInfos(toWaitSemaphore.size());
        for (uint32_t c = 0; c < toWaitSemaphore.size(); c++) {
            pWaitSemaphoreInfos[c].sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
            pWaitSemaphoreInfos[c].pNext = VK_NULL_HANDLE;
            pWaitSemaphoreInfos[c].semaphore = toWaitSemaphore[c];
            pWaitSemaphoreInfos[c].value = waitValue[c];
            // All command s need to wait
            // VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT 
            pWaitSemaphoreInfos[c].stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
            // If the device that semaphore was created on is not a device group, deviceIndex must be 0
            pWaitSemaphoreInfos[c].deviceIndex = 0;
        }
        submitInfo2.waitSemaphoreInfoCount = pWaitSemaphoreInfos.size();
        submitInfo2.pWaitSemaphoreInfos = pWaitSemaphoreInfos.data();

        std::vector<VkSemaphoreSubmitInfo> pSignalSemaphoreInfos(2);
        VkSemaphoreSubmitInfo vkSemaphoreSubmitInfo = {};
        vkSemaphoreSubmitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        vkSemaphoreSubmitInfo.pNext = VK_NULL_HANDLE;
        // VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT  
        vkSemaphoreSubmitInfo.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT;
        // If the device that semaphore was created on is not a device group, deviceIndex must be 0
        vkSemaphoreSubmitInfo.deviceIndex = 0;

        vkSemaphoreSubmitInfo.value = OvVK::Pipeline::renderingDoneValueStatusSyncObj;
        vkSemaphoreSubmitInfo.semaphore = getStatusSyncObjGPU();
        pSignalSemaphoreInfos[0] = vkSemaphoreSubmitInfo;
        vkSemaphoreSubmitInfo.value = 0;
        vkSemaphoreSubmitInfo.semaphore = getStatusSyncObjPresentation();
        pSignalSemaphoreInfos[1] = vkSemaphoreSubmitInfo;

        submitInfo2.signalSemaphoreInfoCount = pSignalSemaphoreInfos.size();
        submitInfo2.pSignalSemaphoreInfos = pSignalSemaphoreInfos.data();

        VkCommandBufferSubmitInfo pCommandBufferInfos;
        pCommandBufferInfos.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        pCommandBufferInfos.pNext = VK_NULL_HANDLE;
        pCommandBufferInfos.commandBuffer = graphicCommandBuffer.get().getVkCommandBuffer();
        // A deviceMask of 0 is equivalent to setting all bits corresponding to valid devices in the group to 1.
        pCommandBufferInfos.deviceMask = 0;

        submitInfo2.commandBufferInfoCount = 1;
        submitInfo2.pCommandBufferInfos = &pCommandBufferInfos;

        // Submit
        if (vkQueueSubmit2(logicalDevice.get().getQueueHandle(queueFamilyIndices.value().graphicsFamily.value()),
            1, &submitInfo2, fenceToWait) != VK_SUCCESS)
        {
            OV_LOG_ERROR("Fail to submit graphics command buffer in GUIPipeline in gui pipeline with id= %d.",
                getId());
            graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
            return false;
        }

        // If enabled, commands that perform picking are waited for.
        // This is only necessary if the time taken to process the commands submitted to the device needs to be measured.
        if (getAnalysingTimeRequired()) {
            // Wait to finish the ray tracing command only if needed to measure the processing time. 
            if (vkWaitForFences(logicalDevice.get().getVkDevice(), 1, &fenceToWait,
                VK_TRUE, UINT64_MAX) != VK_SUCCESS)
            {
                OV_LOG_ERROR("Fail to wait the command buffer used to draw GUIs in GUI pipeline with id= %d.", getId());
                graphicCommandPool.get().freeCommandBuffer(graphicCommandBuffer.get());
                return false;
            }
        }

        // Set some value
        swapchain.get().setSwapchainImageLayout(presentableImageIndex, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
        swapchain.get().setSwapchainImageOwnership(presentableImageIndex, queueFamilyIndices.value().graphicsFamily.value());
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region Static

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Prints the errors throw by the ImGUI creation process.
 */
void OvVK::GUIPipeline::checkVkResult(VkResult result) 
{
    if (result != VK_SUCCESS) 
        OV_LOG_ERROR("Fail to init ImGUI for Vulkan. Error VkResult = %d", result);
}

#pragma endregion