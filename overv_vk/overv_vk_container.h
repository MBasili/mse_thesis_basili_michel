#pragma once
/**
 * @file	overv_vk_container.h
 * @brief	Vulkan centralized data container
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

 /**
  * @brief Class for storing all the data used during the life-cycle of the engine.
  */
class OVVK_API Container final : public Ov::Container
{
//////////
public: //
//////////

	// Special values:
	static Container empty;

	// Const/dest:
	Container();
	Container(Container&& other) noexcept;
	Container(Container const&) = delete;
	virtual ~Container();

	// Operators
	Container& operator=(const Container&) = delete;
	Container& operator=(Container&&) = delete;

	// Manager:
	bool add(Ov::Object& obj) override;
	bool remove(uint32_t id) override;
	bool reset();

	// Get/set:
	Ov::Node& getLastNode();
	OvVK::Geometry& getLastGeometry();
	OvVK::Material& getLastMaterial();
	OvVK::Texture& getLastTexture();
	OvVK::Texture2D& getLastTexture2D();
	OvVK::Sampler& getLastSampler();
	OvVK::Light& getLastLight();
	OvVK::BottomLevelAccelerationStructure& getLastBLAS();
	OvVK::RTObjDesc& getLastRTObjDesc();

	std::list<Ov::Node>& getNodeList();
	std::list<OvVK::Geometry>& getGeometryList();
	std::list<OvVK::Material>& getMaterialList();
	std::list<OvVK::Texture>& getTextureList();
	std::list<OvVK::Texture2D>& getTexture2DList();
	std::list<OvVK::Sampler>& getSamplerList();
	std::list<OvVK::Light>& getLightList();
	std::list<OvVK::BottomLevelAccelerationStructure>& getBLASList();
	std::list<OvVK::RTObjDesc>& getRTObjDescList();

	// Finders:
	Ov::Object& find(const std::string& name) const;   ///< By name
	Ov::Object& find(uint32_t id) const;               ///< By ID

///////////
private: //
///////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Container(const std::string& name);
};