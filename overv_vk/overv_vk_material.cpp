/**
 * @file	overv_vk_material.cpp
 * @brief	Vulkan material properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include:
#include "overv_vk.h"

// C/C++
#include <math.h>

// ImGui
#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_vulkan.h"
#include "imgui_internal.h"
#include "imconfig.h"


////////////
// STATIC //
////////////

// Special values:   
OvVK::Material OvVK::Material::empty("[empty]");

// Look-up table:
bool OvVK::Material::lut[OvVK::Material::maxNrOfMaterials];


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Material reserved structure.
 */
struct OvVK::Material::Reserved
{
    uint32_t texHandles[OvVK::Material::maxNrOfTextures];    ///< Texture bindless handles  

    int lutPos;        ///< Position in the lut  
    std::reference_wrapper<const OvVK::Texture2D> textures[OvVK::Material::maxNrOfTextures];

    /**
     * Constructor.
     */
    Reserved() : texHandles{ 0, 0, 0, 0, 0 },
        lutPos{ maxNrOfMaterials },
        textures{ OvVK::Texture2D::empty, OvVK::Texture2D::empty, OvVK::Texture2D::empty, OvVK::Texture2D::empty, OvVK::Texture2D::empty }
    {
        // Find a free slot in the LUT:
        for (uint32_t c = 0; c < OvVK::Material::maxNrOfMaterials; c++)
            if (lut[c] == false)
            {
                lut[c] = true;
                lutPos = c;
                break;
            }

        if (lutPos == OvVK::Material::maxNrOfMaterials)
            OV_LOG_ERROR("Too many materials created");
    }

    /**
     * Destructor.
     */
    ~Reserved()
    {
        if (lutPos != OvVK::Material::maxNrOfMaterials)
            lut[lutPos] = false;
    }
};


////////////////////////////
// BODY OF CLASS Material //
////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Material::Material() : reserved(std::make_unique<OvVK::Material::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name material name
 */
OVVK_API OvVK::Material::Material(const std::string& name) : Ov::MaterialPbr(name),
    reserved(std::make_unique<OvVK::Material::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Material::Material(Material&& other) : Ov::MaterialPbr(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Material::~Material()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Default

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets default material, which is instatiated on first usage.
 * @return default material
 */
const OvVK::Material OVVK_API& OvVK::Material::getDefault()
{
    static Material dfltMaterial("[default]");
    dfltMaterial.setAlbedo(glm::vec3(1.0f, 0.0f, 0.0f));
    return dfltMaterial;
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes an Vulkan texture. Not really used, just a placeholder.
 * @return TF
 */
bool OVVK_API OvVK::Material::init()
{
    if (this->Ov::MaterialPbr::init() == false)
        return false;

    // Done:   
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases an Vulkan texture. Not really used, just a placeholder.
 * @return TF
 */
bool OVVK_API OvVK::Material::free()
{
    if (this->Ov::MaterialPbr::free() == false)
        return false;

    // Done:   
    return true;
}

#pragma endregion

#pragma region Set/Get

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get position of this material in the LUT.
 * @return LUT position
 */
uint32_t OVVK_API OvVK::Material::getLutPos() const
{
    return reserved->lutPos;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets texture.
 * @param type texture level
 * @return texture at level
 */
const OvVK::Texture2D OVVK_API& OvVK::Material::getTexture(Ov::Texture::Type type) const
{
    // Get texture:
    switch (type)
    {
    case Ov::Texture::Type::albedo:
        return reserved->textures[0];

    case Ov::Texture::Type::normal:
        return reserved->textures[1];

    case Ov::Texture::Type::height:
        return reserved->textures[2];

    case Ov::Texture::Type::roughness:
        return reserved->textures[3];

    case Ov::Texture::Type::metalness:
        return reserved->textures[4];

    default:
        OV_LOG_ERROR("Unsupported texture level");
        return OvVK::Texture2D::empty;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets texture.
 * @param tex texture
 * @param type texture level
 * @return TF
 */
bool OVVK_API OvVK::Material::setTexture(const OvVK::Texture2D& tex, Ov::Texture::Type type)
{
    // Safety net:
    if (tex == OvVK::Texture2D::empty) {
        OV_LOG_ERROR("The passed texture is the empty texture in material with id= %d", getId());
        return false;
    }

    switch (type)
    {
    case Ov::Texture::Type::albedo:
        reserved->textures[0] = tex;
        reserved->texHandles[0] = tex.getLutPos();
        break;

    case Ov::Texture::Type::normal:
        reserved->textures[1] = tex;
        reserved->texHandles[1] = tex.getLutPos();
        break;

    case Ov::Texture::Type::height:
        reserved->textures[2] = tex;
        reserved->texHandles[2] = tex.getLutPos();
        break;

    case Ov::Texture::Type::roughness:
        reserved->textures[3] = tex;
        reserved->texHandles[3] = tex.getLutPos();
        break;

    case Ov::Texture::Type::metalness:
        reserved->textures[4] = tex;
        reserved->texHandles[4] = tex.getLutPos();
        break;

    default:
        OV_LOG_ERROR("Unsupported texture level");
        return false;
    }

    Ov::MaterialPbr::setTexture(tex, type);

    // Done:
    return true;
}

#pragma endregion

#pragma region Texture

bool OvVK::Material::loadTexture(const std::string& textureName, Ov::Texture::Type type, Ov::Container& container) {

    // Cast container to current engine container definition
    std::reference_wrapper<OvVK::Container> loadingContainer = static_cast<OvVK::Container&>(container);

    // Texture.
    std::reference_wrapper<const OvVK::Texture2D> textureToSet = OvVK::Texture2D::empty;

    // Search for the texture
    Ov::Object& textureFound = loadingContainer.get().find(textureName);
    if (textureFound == Ov::Object::empty)
    {
        OvVK::Texture2D texture;

        // Set default texture if maximum number is reached.
        if (texture.getLutPos() == OvVK::Texture::maxNrOfTextures)
        {
            OV_LOG_ERROR("Too many texture, the default texture2D is returned.");
            textureToSet = OvVK::Texture2D::getDefault();
        }
        else
        {
            // Load bitmap
            OvVK::Bitmap bitmap;

            // Check extension 
            bool loadingResult = false;
            if (textureName.substr(textureName.find_last_of(".") + 1) == "dds") {
                loadingResult = bitmap.loadDDS(OvVK::Engine::texturesSourceFolder + "\\" + textureName);
            }
            if (textureName.substr(textureName.find_last_of(".") + 1) == "ktx") {

                Ov::Bitmap::Format formatToLoad = Ov::Bitmap::Format::r8g8b8a8_compressed;

                switch (type) {
                case Ov::Texture::Type::metalness:
                case Ov::Texture::Type::roughness:
                case Ov::Texture::Type::height:
                    formatToLoad = Ov::Bitmap::Format::r8_compressed;
                    break;
                case Ov::Texture::Type::normal:
                    formatToLoad = Ov::Bitmap::Format::r8g8_compressed;
                    break;
                default:
                    break;
                }

                loadingResult = bitmap.loadKTX(OvVK::Engine::texturesSourceFolder + "\\" + textureName, formatToLoad);
            }
            else {
                loadingResult = bitmap.loadWithFreeImage(OvVK::Engine::texturesSourceFolder + "\\" + textureName, false, false, true);
            }

            if (loadingResult)
            {
                bool isLinearEncoded = true;
                if (type == Ov::Texture::Type::albedo) {
                    isLinearEncoded = false;
                }

                if (texture.load(bitmap, isLinearEncoded))
                {
                    // Add texture :
                    texture.setName(textureName);

                    /////////////
                    // Sampler //
                    /////////////

                    // Get default sampler
                    std::reference_wrapper<const OvVK::Sampler> textureSampler =
                        OvVK::Sampler::getDefault();

                    // Get all the already created sampler
                    std::list<OvVK::Sampler>& samplers = loadingContainer.get().getSamplerList();
                    bool foundSuitableSampler = false;

                    // Check if there exist already a valid sampler
                    for (std::reference_wrapper<const OvVK::Sampler> sampler : samplers) {
                        if (sampler.get().getMaxLevelOfDetail() == texture.getImage().getNrOfLevels())
						{
							foundSuitableSampler = true;
							textureSampler = sampler;
							break;
                        }
                    }

                    if (foundSuitableSampler == false)
                    {
                        // Create sampler
                        OvVK::Sampler sampler;
                        
                        VkSamplerCreateInfo samplerCreateInfo =
                            OvVK::Sampler::getDefaultSamplerCreateInfo();

                        samplerCreateInfo.maxLod = texture.getImage().getNrOfLevels();

                        if (sampler.create(samplerCreateInfo) == false) {
                            OV_LOG_ERROR(R"(Fail to crete sampler for the texture with id= %d into a container
 in the material with id= %d.)", texture.getId(), getId());
                        }
                        else 
                        {
                            loadingContainer.get().add(sampler);
                            textureSampler = dynamic_cast<OvVK::Sampler&>(loadingContainer.get().getLastSampler());
                        }
                    }

                    // Assign sampler to the image
                    texture.setSampler(textureSampler.get());

                    if (loadingContainer.get().add(texture))
                        textureToSet = loadingContainer.get().getLastTexture2D();
                    else
                        OV_LOG_ERROR("Fail to add the texture with id= %d into a container in the material with id= %d.", texture.getId(), getId());
                }
                else
                {
                    OV_LOG_ERROR("Fail to load image with name %s into a texture in the material with id= %d", textureName, getId());
                }
            }
            else
            {
                OV_LOG_ERROR("Fail to load image with name %s into a bitmap in the material with id= %d", textureName, getId());
            }
        }
    }
    else
    {
        textureToSet = dynamic_cast<OvVK::Texture2D&>(textureFound);
    }

    OvVK::TexturesDescriptorSetsManager::getInstance().addToUpdateList(textureToSet);
    return setTexture(textureToSet, type);
}

#pragma endregion

#pragma region Sync

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Synch material data to GPU.
 * @return TF
 */
bool OVVK_API OvVK::Material::uploadToStorage() const
{
    // Apply mat props to OpenGL:
    OvVK::Storage::MaterialData* mat = OvVK::Engine::getInstance().getStorage().getMaterialData(reserved->lutPos);
    memcpy(mat, this->Ov::MaterialPbr::reserved.get(), 48); // Copy the first 64 bytes only with mat props

    uint8_t* matTexture = reinterpret_cast<uint8_t*>(mat);
    matTexture += 48;
    memcpy(matTexture, reserved->texHandles, OvVK::Material::maxNrOfTextures * sizeof(uint32_t)); // Copy additional 32 bytes with texture info

    // Done:
    return true;
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::Material::render(uint32_t value, void* data) const
{
    // Sanity check:
    if (reserved->lutPos == OvVK::Material::maxNrOfMaterials)
    {
        OV_LOG_ERROR("Invalid LUT entry for material '%s', id %u", this->getName().c_str(), this->getId());
        return false;
    }

    // If value is non-zero, perform a data synch instead:
    if (value > 0)
        return uploadToStorage();

    // Force refresh if dirty:
    if (isDirty())
    {
        if (OvVK::Engine::getInstance().getStorage().addToUpdateList(*this) == false)
        {
            OV_LOG_ERROR("Fail to upload material into the engine storage.");
            return false;
        }

        setDirty(false);
    }

    // Done:
    return true;
}

#pragma endregion

#pragma region DrawGUI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Draw the gui for a material.
 */
void OVVK_API OvVK::Material::drawGUI()
{
    //// Emission
    //glm::vec3 emission = getEmission();
    //ImGui::SliderFloat3("Emission color", glm::value_ptr(emission), 0.0f, 1.0f);
    //setEmission(emission);

	// Opacity
	float opacity = getOpacity();
	ImGui::SliderFloat("Opacity", &opacity, 0.0f, 1.0f);
	setOpacity(opacity);

	// Albedo
	//if (getTexture(Ov::Texture::Type::albedo) == OvVK::Texture2D::empty)
	//{
	glm::vec3 albedo = getAlbedo();
	ImGui::SliderFloat3("Albedo color", glm::value_ptr(albedo), 0.0f, 1.0f);
	setAlbedo(albedo);
	//}

    // RimIntensity
    //float rimIntensity = getRimIntensity();
    //ImGui::SliderFloat("Rim intensity", &rimIntensity, 0.01f, 1.0f);
    //setRimIntensity(rimIntensity);

	// RefractionIndex
	float refractionIndex = getRefractionIndex();
	ImGui::SliderFloat("Refraction index", &refractionIndex, 0.0f, 3.0f);
	setRefractionIndex(refractionIndex);

	// Roughness
	//if (getTexture(Ov::Texture::Type::roughness) == OvVK::Texture2D::empty)
	//{
	float roughness = getRoughness();
	ImGui::SliderFloat("Roughness", &roughness, 0.0f, 1.0f);
	setRoughness(roughness);
	//}

	// Metalness
	//if (getTexture(Ov::Texture::Type::metalness) == OvVK::Texture2D::empty)
	//{
	float metalness = getMetalness();
	ImGui::SliderFloat("Metalness", &metalness, 0.0f, 1.0f);
	setMetalness(metalness);
    //}

    // absorption threshold:
    //float absorptionThreshold = getAbsorptionThreshold();
    //ImGui::SliderFloat("Absorption Threashold", &absorptionThreshold, 0.0f, 1.0f);
    //setAbsorptionThreshold(absorptionThreshold);
}

#pragma endregion