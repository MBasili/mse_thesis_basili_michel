/**
 * @file	overv_vk_camera.h
 * @brief	Basic camera properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once


/**
 * @brief Class for modelling an Vulkan camera.
 */
class OVVK_API Camera : public Ov::Camera
{
//////////
public: //
//////////

   // Const/dest:
	Camera();
	Camera(Camera&& other) noexcept;
	Camera(Camera const&) = delete;
	virtual ~Camera();

///////////
private: //
///////////   

   // Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

	// Const/dest:
	Camera(const std::string& name);
};
