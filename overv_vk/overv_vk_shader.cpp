/**
 * @file	overv_vk_shader.cpp
 * @brief	Vulkan generic shader properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Magic enum:
#include "magic_enum.hpp"

// Vulkan
#include <shaderc/shaderc.hpp>


////////////
// STATIC //
////////////

// Global preprocessor:   
Ov::Preprocessor OvVK::Shader::preprocessor;

OvVK::Shader OvVK::Shader::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

#pragma region Structures

/**
 * @brief Shader class reserved structure.
 */
struct OvVK::Shader::Reserved
{
    std::vector<uint32_t> buffer;   ///< Buffer contain SPIR-V shader code
    Ov::Shader::Type shaderType;

    /**
     * Constructor
     */
    Reserved() : shaderType{ Ov::Shader::Type::none }
    {}
};

#pragma endregion

//////////////////////////
// BODY OF CLASS Shader //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Shader::Shader() : reserved(std::make_unique<OvVK::Shader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name node name
 */
OVVK_API OvVK::Shader::Shader(const std::string& name) : Ov::Shader(name), 
    reserved(std::make_unique<OvVK::Shader::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Shader::Shader(Shader&& other) : Ov::Shader(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Shader::~Shader()
{
    OV_LOG_DETAIL("[-]");
    
    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an Vulkan shader.
 * @return TF
 */
bool OVVK_API OvVK::Shader::init()
{
    if (this->Ov::Managed::init() == false)
        return false;

    // Done:      
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destroy an Vulkan instance.
 * @return TF
 */
bool OVVK_API OvVK::Shader::free()
{
    if (this->Ov::Managed::free() == false)
        return false;

    // Done:      
    return true;
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Create an Vulkan shader.
 * @return TF
 */
bool OVVK_API OvVK::Shader::load(Type kind, const std::string& code)
{
    // Preprocess:
    std::string _code = preprocessor.preprocess(code);
    OV_LOG_DEBUG("[preprocessed] %s", _code.c_str());

    // Preload:
    if (this->Ov::Shader::load(kind, _code) == false)
        return false;

    // Init shader:
    if (!this->init())
        return false;

    // Source code
    std::string source_code = std::string(OvVK::Shader::glslVersion) + this->getCode();
    OV_LOG_INFO("[preprocessed] %s", source_code.c_str());

    // Get kind of shader to compile
    shaderc_shader_kind vkKind;
    switch (kind)
    {
        /////////////////////////////////////
    case Ov::Shader::Type::ray_generation: //
        vkKind = shaderc_raygen_shader;
        break;
        //////////////////////////////
    case Ov::Shader::Type::any_hit: //
        vkKind = shaderc_anyhit_shader;
        break;
        /////////////////////////////////
    case Ov::Shader::Type::closes_hit: //
        vkKind = shaderc_closesthit_shader;
        break;
        ///////////////////////////
    case Ov::Shader::Type::miss: //
        vkKind = shaderc_miss_shader;
        break;
        ///////////////////////////////////
    case Ov::Shader::Type::intersection: //
        vkKind = shaderc_intersection_shader;
        break;
        ///////////////////////////////
    case Ov::Shader::Type::callable: //
        vkKind = shaderc_callable_shader;
        break;
        ///////
    default: //
        OV_LOG_ERROR("Fail to find the shader type.");
        this->free();
        return false;
    }

    // String to identify shader in case of compilation error.
    std::string name_id = std::string(OV_LOG_ENUM(kind)) + "_SHADER";
    // Convert to upper case
    std::transform(name_id.begin(), name_id.end(), name_id.begin(), ::toupper);
    name_id = "OV_ID_" + std::to_string(this->Ov::Object::getId()) + "_" + name_id + "\n";

    // Compile
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;

    // optimize towards reducing code size
    options.SetOptimizationLevel(shaderc_optimization_level_size);
    // set target enviroment (vulkan)
    options.SetTargetEnvironment(shaderc_target_env::shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_3);
    // set target spirv version
    options.SetTargetSpirv(shaderc_spirv_version::shaderc_spirv_version_1_6);

    // compile
    shaderc::SpvCompilationResult module =
        compiler.CompileGlslToSpv(source_code, vkKind, name_id.c_str(), options);

    if (module.GetCompilationStatus() != shaderc_compilation_status_success) {
        OV_LOG_ERROR(module.GetErrorMessage().c_str());
        this->free();
        return false;
    }

    // set the result
    reserved->buffer = { module.cbegin(), module.cend() };
    reserved->shaderType = kind;

    // Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the shader as binary.
 * @return The shader as binary contained in a vector.
 */
std::vector<uint32_t> OVVK_API &OvVK::Shader::getSPIRVBinary() const
{
    return reserved->buffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the size in bytes of the buffer containig the shader.
 * @return The size in bytes of the buffer containig the shader.
 */
uint32_t OVVK_API OvVK::Shader::getNrOfBytes() const
{
    return reserved->buffer.size() * sizeof(uint32_t);
}

#pragma endregion

#pragma region Rendering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::Shader::render(uint32_t value, void* data) const
{
    OV_LOG_INFO("Default render method of the vulkan shader.");

    // Done:
    return false;
}

#pragma endregion