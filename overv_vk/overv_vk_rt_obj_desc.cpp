/**
 * @file    overv_vk_rt_obj_desc.cpp
 * @brief	Ray Tracing Obj Desc
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */

//////////////
// #INCLUDE //
//////////////

// Main inlcude:
#include "overv_vk.h"

// Boost
#include "boost/container_hash/hash.hpp"


////////////
// STATIC //
////////////

// Special values:
OvVK::RTObjDesc OvVK::RTObjDesc::empty("[empty]");


////////////////
// STRUCTURES //
////////////////

/**
 * @brief AccelerationStructure class reserved structure.
 */
struct OvVK::RTObjDesc::Reserved
{
    std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure> blas;  ///< The BLAS contained by the rt obj desc
    std::vector<std::reference_wrapper<OvVK::Material>> materials;              ///< The materials of the geometries contained in the BLAS
    std::map<uint32_t, uint32_t> geomToMaterial;                                    ///< Link between BLAS's geometries and the materials.           

    OvVK::Buffer deviceGeometryData;                                            ///< Buffer containing structures info about the geometries contained in the RTObjDesc

    /**
     * Constructor
     */
    Reserved() : blas{ OvVK::BottomLevelAccelerationStructure::empty }
    {}
};


/////////////////////////////
// BODY OF CLASS RTObjDesc //
/////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::RTObjDesc::RTObjDesc() :
    reserved(std::make_unique<OvVK::RTObjDesc::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name mesh name
 */
OVVK_API OvVK::RTObjDesc::RTObjDesc(const std::string& name) : Ov::Renderable(name),
    reserved(std::make_unique<OvVK::RTObjDesc::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::RTObjDesc::RTObjDesc(RTObjDesc&& other) :
    Ov::Renderable(std::move(other)), reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::RTObjDesc::~RTObjDesc()
{
    OV_LOG_DETAIL("[-]");

    if (reserved && isInitialized())
        this->free();
}

#pragma endregion

#pragma region Managed

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Initializes
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::init()
{
    if (this->Ov::Managed::init() == false)
        return false;


    // Free buffer
    if (reserved->deviceGeometryData.free() == false)
    {
        OV_LOG_INFO("Fail to free device buffer in the RTObjDesc with id= %d.", getId());
        return false;
    }


    // Done: 
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Releases
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::free()
{

    if (this->Ov::Managed::free() == false)
        return false;


    // Free buffer
    if (reserved->deviceGeometryData.free() == false)
    {
        OV_LOG_INFO("Fail to free device buffer in the RTObjDesc with id= %d.", getId());
        return false;
    }


    // Done:   
    return true;
}

#pragma endregion

#pragma region SyncData

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the rtObjDesc is locked.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::isLock() const
{
    return reserved->deviceGeometryData.isLock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return if the rtObjDesc is owned from a queue family.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::isOwned() const
{
    return reserved->deviceGeometryData.getQueueFamilyIndex().has_value();
}

#pragma endregion

#pragma region General

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set up the rtObjDesc's geoemtries buffer to update the data in the GPU memory.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setUp()
{
    // Safety net:
    if (isLock())
    {
        OV_LOG_INFO("RTObjDesc with id= %d is lock.", getId());
        return false;
    }

    if (reserved->blas.get() == OvVK::BottomLevelAccelerationStructure::empty ||
        reserved->blas.get().getVkAS() == VK_NULL_HANDLE)
    {
        OV_LOG_INFO("The empty passed BLAS is not valid in the RTObjDesc with id= %d.", getId());
        return false;
    }

    // Init
    if (!this->init())
        return false;


    // Retrieve BLAS Geometries data
    std::vector<std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData>> geometriesDataBLAS =
        reserved->blas.get().getGeometriesData();

    VkDeviceSize bufferSize = geometriesDataBLAS.size() * sizeof(OvVK::RTObjDesc::GeometryData);
    

    ///////////////////
    // Device Buffer //
    ///////////////////

#pragma region DeviceBuffer

    // Create info buffer
    VkBufferCreateInfo vkVertexBufferCreateInfo{};
    vkVertexBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vkVertexBufferCreateInfo.size = bufferSize;
    // VK_BUFFER_USAGE_TRANSFER_DST_BIT => specifies that the buffer can be used as the destination 
    //      of a transfer command.
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device
    //      address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
    // VK_BUFFER_USAGE_STORAGE_BUFFER_BIT specifies that the buffer can be used in a VkDescriptorBufferInfo suitable
    //      for occupying a VkDescriptorSet slot either of type VK_DESCRIPTOR_TYPE_STORAGE_BUFFER 
    //      or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC.
    vkVertexBufferCreateInfo.usage =  VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT
        | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    // Set the sharing modo to exclusive allow only one queue family to own the buffer.
    // Need to change ownership between queue through memeoryBarrier (automatically done in concurret mode).
    // We use VK_SHARING_MODE_EXCLUSIVE for perfomance.
    vkVertexBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vkVertexBufferCreateInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    vkVertexBufferCreateInfo.queueFamilyIndexCount = 0;

    // Create allocation info
    VmaAllocationCreateInfo vmaVertexBufferAllocationCreateInfo{};
    // VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT  => Allocation strategy that chooses smallest
    // possible free range for the allocation to minimize memory usage and fragmentation, possibly 
    // at the expense of allocation time.
    vmaVertexBufferAllocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    // VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT => bit specifies that memory allocated with this type is the most
    // efficient for device access. This property will be set if and only if the memory type belongs to a heap
    // with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.
    vmaVertexBufferAllocationCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // Create buffer to contains vertex data
    if (reserved->deviceGeometryData.create(vkVertexBufferCreateInfo, vmaVertexBufferAllocationCreateInfo) == false)
    {
        OV_LOG_ERROR("Fail to create RTObjDesc's geometries buffer in the RTObjDesc with id= %d.", getId());
        this->free();
        return false;
    }

#pragma endregion


    if (reserved->deviceGeometryData.setStatusSyncObjValue(OvVK::RTObjDesc::readyToUploadValueStatusSyncObj) == false)
    {
        OV_LOG_ERROR("Fail to set the status value of RTObjDesc's geometries buffer in the RTObjDesc with id= %d.", getId());
        this->free();
        return false;
    }

    this->Ov::Object::setDirty(true);

    //Done:
    return true;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the rtObjDesc's geometries buffer.
 * @return The rtObjDesc's geometries buffer.
 */
const OvVK::Buffer OVVK_API& OvVK::RTObjDesc::getGeometryDataDeviceBuffer() const 
{
    return reserved->deviceGeometryData;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the BLAS contained by the rtObjDesc.
 * @return The BLAS contained by the rtObjDesc.
 */
const OvVK::BottomLevelAccelerationStructure OVVK_API& OvVK::RTObjDesc::getBLAS() const
{
    return reserved->blas;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the references to the materials contained by the rtObjDesc.
 * @return Vector containing the references to the materials contained by the rtObjDesc.
 */
const std::vector<std::reference_wrapper<OvVK::Material>> OVVK_API& OvVK::RTObjDesc::getMaterials() const
{
    return reserved->materials;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the material linked to the passed geometry.
 * @param geom A reference to a geometry contained in the BLAS contained by the rtObjDesc.
 * @return The material linked to the passed geometry.
 */
OvVK::Material OVVK_API& OvVK::RTObjDesc::getGeometryMaterial(const OvVK::Geometry& geom) const
{
    // Safety net:
    if (reserved->geomToMaterial.count(geom.getId()) == 0)
    {
        OV_LOG_ERROR(R"(The geometry passed with id= %d is not inside the BLAS with id= %d contained
 by the RTObjDesc with id= %d.)",
            geom.getId(), reserved->blas.get().getId(), getId());
        return OvVK::Material::empty;
    }

    return reserved->materials[reserved->geomToMaterial[geom.getId()]];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the material linked to the passed geometry id.
 * @param geometryId The geometry index of a geometry contained in the BLAS contained by the rtObjDesc.
 * @return The material linked to the passed geometry index.
 */
OvVK::Material OVVK_API& OvVK::RTObjDesc::getGeometryMaterial(uint32_t geometryId) const
{
    // Safety net:
    if (reserved->geomToMaterial.count(geometryId) == 0)
    {
        OV_LOG_ERROR(R"(The geometry passed with id= %d is not inside the BLAS with id= %d contained
 by the RTObjDesc with id= %d.)",
            geometryId, reserved->blas.get().getId(), getId());
        return OvVK::Material::empty;
    }

    return reserved->materials[reserved->geomToMaterial[geometryId]];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the number of geometries contained by the BLAS contained by the rtObjDesc.
 * @return The number of geometries contained by the BLAS contained by the rtObjDesc.
 */
uint32_t OVVK_API OvVK::RTObjDesc::getNrGeoemtriesInBLAS() const 
{
    return (reserved->blas.get() != OvVK::BottomLevelAccelerationStructure::empty) 
        ? reserved->blas.get().getNrOfGeometries() : 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the info structures of the RTobjDesc's geometries.
 * @return A vector containing the info structures of the RTobjDesc's geometries.
 */
const std::vector<OvVK::RTObjDesc::GeometryData> OVVK_API OvVK::RTObjDesc::getGeometriesData() const 
{
    std::vector<OvVK::RTObjDesc::GeometryData> geometriesData(reserved->blas.get().getNrOfGeometries());
    std::vector<std::reference_wrapper<const OvVK::BottomLevelAccelerationStructure::GeometryData>>
        blasGeoemtriesData = reserved->blas.get().getGeometriesData();

    for (uint32_t c = 0; c < geometriesData.size(); c++)
    {
        std::reference_wrapper<OvVK::Geometry> geometry = blasGeoemtriesData[c].get().geometry;
        // No need to check if the device address is present because when add geometry to a BLAS need to be uploaded.
        geometriesData[c].vertexAddress = geometry.get().getVertexBuffer().getBufferDeviceAddress().value();
        geometriesData[c].faceAddress = geometry.get().getFaceBuffer().getBufferDeviceAddress().value();

        if(reserved->materials[reserved->geomToMaterial[geometry.get().getId()]].get() !=
            OvVK::Material::empty)
            geometriesData[c].materialIndex = reserved->materials[reserved->geomToMaterial[geometry.get().getId()]].get().getLutPos();
        else
            geometriesData[c].materialIndex = OvVK::Material::getDefault().getLutPos();

        geometriesData[c].geometryId = geometry.get().getId();
        geometriesData[c].rtObjDescId = getId();
    }

	// Done:
	return geometriesData;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Return the hash of the rtObjDesc.
 * @return The hash of the rtObjDesc.
 */
std::size_t OVVK_API OvVK::RTObjDesc::getHash() const
{
    std::size_t hash = 0;

    boost::hash_combine(hash, reserved->blas.get().getId());

    std::vector<uint32_t> ids;
    for (std::reference_wrapper<const OvVK::Material> material : reserved->materials)
        ids.push_back(material.get().getId());

    std::sort(ids.begin(), ids.end(), std::greater<uint32_t>());

    for (uint32_t c = 0; c < ids.size(); c++)
        boost::hash_combine(hash, ids[c]);

    // Done:
    return hash;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the BLAS contained by the rtObjDesc.
 * blas A BLAS
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setBLAS(const OvVK::BottomLevelAccelerationStructure& blas)
{
    // Safety net:
    if (blas == OvVK::BottomLevelAccelerationStructure::empty)
    {
        OV_LOG_ERROR("The empty passed BLAS is not valid in RTObjDesc with id= %d.", getId());
        return false;
    }

    // Set blas
    reserved->blas = blas;
    // Reset material
    reserved->geomToMaterial.clear();
    reserved->materials.clear();
    for (auto geom : blas.getGeometriesData()) 
    {
        reserved->geomToMaterial[geom.get().geometry.get().getId()] = reserved->materials.size();
        reserved->materials.push_back(OvVK::Material::empty);
    }

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Links the passed material to the passed geometry.
 * @param geom A reference to a OvVK geometry contained by the BLAS contained by the rtObjDesc.
 * @param material A reference to a OvVulan material.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setGeometryMaterial(const OvVK::Geometry& geom, OvVK::Material& material)
{
    if (reserved->geomToMaterial.count(geom.getId()) == 0) 
    {
        OV_LOG_ERROR(R"(The geometry passed with the id= %d is not inside the assigned BLAS with id= %d
 in the RTObjDesc with id= %d.)", geom.getId(), reserved->blas.get().getId(), getId());
        return false;
    }

    reserved->geomToMaterial[geom.getId()] = reserved->materials.size();
    reserved->materials.push_back(material);

    // Done:
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Links the passed material to the passed geometry.
 * @param geometryId The geometry index of a OvVK geometry contained by the BLAS contained by the rtObjDesc.
 * @param material A reference to a OvVulan material.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setGeometryMaterial(uint32_t geometryId, OvVK::Material& material)
{
    if (reserved->geomToMaterial.count(geometryId) == 0)
    {
        OV_LOG_ERROR(R"(The geometry passed with the id= %d is not inside the assigned BLAS with id= %d
 in the RTObjDesc with id= %d.)", geometryId, reserved->blas.get().getId(), getId());
        return false;
    }

    reserved->geomToMaterial[geometryId] = reserved->materials.size();
    reserved->materials.push_back(material);

    // Done:
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the queue family index of the queue owning the rtObjDesc's buffers.
 * @param queueFamilyIndex Index of the queue owning the rtObjDesc's buffers.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setQueueFamilyIndex(uint32_t queueFamilyIndex)
{
    if (isLock() == true)
    {
        OV_LOG_INFO("The RTObjDesc is lock.");
        return false;
    }

    return reserved->deviceGeometryData.setQueueFamilyIndex(queueFamilyIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the status of the rtObjDesc.
 * @param value The uint value rapresenting the status the rtObjDesc is in.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setStatusSyncObjValue(uint64_t value)
{
    if (isLock() == true)
    {
        OV_LOG_INFO("The Geometry is lock.");
        return false;
    }

    //Done:
    return reserved->deviceGeometryData.setStatusSyncObjValue(value);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Sets the sync obj for the lock.
 * @param fence Vulkan object to check to know whether the rtObjDesc is locked or not.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::setLockSyncObj(VkFence fence)
{
    return reserved->deviceGeometryData.setLockSyncObj(fence);
}

#pragma endregion

#pragma region Redering

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Rendering method.
 * @param value generic value
 * @param data generic pointer to any kind of data
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::render(uint32_t value, void* data) const 
{
    if (this->Ov::Object::isDirty()) 
        this->Ov::Object::setDirty(false);

    // BLAS render (geomtry inside BLAS also)
    if (!reserved->blas.get().render())
        return false;

    // Render material
    for (std::reference_wrapper<const OvVK::Material> material : reserved->materials)
        if (material.get().render() == false)
            return false;

    // Done:
    return true;
}

#pragma endregion

#pragma region Static

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Adds to the OvVK shader preprocessing the string to include the info to access the rtObjDesc data in a shader.
 * @return TF
 */
bool OVVK_API OvVK::RTObjDesc::addShaderPreproc()
{
    std::string geometryDataPreproc = R"(
#ifndef OVVULKAN_RT_OBJ_DESC_GEOMETRY_DATA_INCLUDED
#define OVVULKAN_RT_OBJ_DESC_GEOMETRY_DATA_INCLUDED
)";

    geometryDataPreproc += OvVK::RTObjDesc::GeometryData::getShaderStruct();

    geometryDataPreproc += "layout(buffer_reference, scalar) buffer GeometriesData {GeometryData geometriesData[]; }; // Vertex of an object";

    geometryDataPreproc += R"(
#endif
)";

    return OvVK::Shader::preprocessor.set("include RTObjDesc", geometryDataPreproc);
}

#pragma endregion