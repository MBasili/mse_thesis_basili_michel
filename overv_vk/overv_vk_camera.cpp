/**
 * @file	overv_vk_camera.cpp
 * @brief	Basic camera properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


/////////////
// INCLUDE //
/////////////

// Main include:
#include "overv_vk.h"


////////////////
// STRUCTURES //
////////////////

/**
 * @brief Camera class reserved structure.
 */
struct OvVK::Camera::Reserved
{
    /**
     * Constructor
     */
    Reserved()
    {}
};


//////////////////////////
// BODY OF CLASS Camera //
//////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::Camera::Camera() : reserved(std::make_unique<OvVK::Camera::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The camera name.
 */
OVVK_API OvVK::Camera::Camera(const std::string& name) : Ov::Camera(name),
    reserved(std::make_unique<OvVK::Camera::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::Camera::Camera(Camera&& other) noexcept : Ov::Camera(std::move(other)),
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::Camera::~Camera()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion
