/**
 * @file	overv_vk_pipeline.h
 * @brief	Vulkan pipeline
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */
#pragma once

 /**
  * @brief Class for modelling a generic pipeline in Vulkan
  */
class OVVK_API Pipeline : public Ov::Pipeline, public OvVK::SurfaceDependentObject
{
//////////
public: //
//////////

	// Static
	static Pipeline empty;

	// Sync obj value:
	constexpr static uint64_t defaultValueStatusSyncObj = 0;
	constexpr static uint64_t renderingValueStatusSyncObj = 1;
	constexpr static uint64_t renderingDoneValueStatusSyncObj = 2;

	// Const/dest:
	Pipeline();
	Pipeline(Pipeline&& other);
	Pipeline(Pipeline const&) = delete;
	virtual ~Pipeline();

	// Operators
	Pipeline& operator=(const Pipeline&) = delete;
	Pipeline& operator=(Pipeline&&) = delete;

	// Managed:
	virtual bool init() override;
	virtual bool free() override;

	// General:
	virtual bool create();
	virtual bool setUpCommandBuffers();

	// Management Sync
	void addPipelineToWait(OvVK::Pipeline& pipeline);
	void removePipelineToWait(OvVK::Pipeline& pipeline);
	void resetPipelineToWait();


	// Get/Set
#ifdef OVERV_VULKAN_LIBRARY_BUILD
	VkPipeline getVkPipeline() const;
	VkPipelineLayout getVkPipelineLayout() const;

	VkFence getStatusSyncObjCPU() const;
	std::vector<VkFence> getStatusSyncObjsCPU() const;
	VkSemaphore getStatusSyncObjGPU() const;
	std::vector<VkSemaphore> getStatusSyncObjsGPU() const;
	VkSemaphore getStatusSyncObjPresentation() const;
	std::vector<VkSemaphore> getStatusSyncObjsPresentation() const;
#endif

	bool getAnalysingTimeRequired() const;
	void setAnalysingTimeRequired(bool value);

/////////////
protected: //
/////////////

	// Reserved:
	struct Reserved;
	std::unique_ptr<Reserved> reserved;

#ifdef OVERV_VULKAN_LIBRARY_BUILD
	// Get/Set
	VkPipeline* getVkPipelinePointer() const;
	VkPipelineLayout* getVkPipelineLayoutPointer() const;
	std::vector<VkSemaphore> getStatusSyncObjsGPUToWait() const;

	void setVkPipeline(VkPipeline pipeline);
	void setVkPipelineLayout(VkPipelineLayout pipelineLayout);
	bool setStatusSyncObjGPU(uint64_t value);
#endif

	// Const/dest:
	Pipeline(const std::string& name);
};
