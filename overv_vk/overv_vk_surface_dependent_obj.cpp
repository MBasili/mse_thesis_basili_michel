/**
 * @file	overv_vk_surface_dependent_obj.cpp
 * @brief	Vulkan surface dependent object properties
 *
 * @author	Michel Basili (michel.basili@student.supsi.ch), 2021-2022
 */


//////////////
// #INCLUDE //
//////////////

// Main include
#include "overv_vk.h"


////////////
// STATIC //
////////////

// Special values
OvVK::SurfaceDependentObject OvVK::SurfaceDependentObject::empty("empty");


////////////////
// STRUCTURES //
////////////////

#pragma region Struct

/**
 * @brief SurfaceDependentObject class reserved structure.
 */
struct OvVK::SurfaceDependentObject::Reserved
{
    OvVK::Surface::SurfaceCallbackTypes activeCallback;  ///< The enable callback types. On which callback function the forwarding is performed.
    bool isActive;                               ///< Dependency is active. SurfaceCallbackTypes forwarding is active.

    /**
     * Constructor.
     */
    Reserved() : isActive{ false },
        activeCallback{ OvVK::Surface::SurfaceCallbackTypes::none }
    {}
};

#pragma endregion


//////////////////////////////////////////
// BODY OF CLASS SurfaceDependentObject //
//////////////////////////////////////////

#pragma region Const/Dest

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 */
OVVK_API OvVK::SurfaceDependentObject::SurfaceDependentObject() :
    reserved(std::make_unique<OvVK::SurfaceDependentObject::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor with name.
 * @param name The dependent object name.
 */
OVVK_API OvVK::SurfaceDependentObject::SurfaceDependentObject(const std::string& name) :
    reserved(std::make_unique<OvVK::SurfaceDependentObject::Reserved>())
{
    OV_LOG_DETAIL("[+]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Move constructor.
 */
OVVK_API OvVK::SurfaceDependentObject::SurfaceDependentObject(SurfaceDependentObject&& other) :
    reserved(std::move(other.reserved))
{
    OV_LOG_DETAIL("[M]");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Destructor.
 */
OVVK_API OvVK::SurfaceDependentObject::~SurfaceDependentObject()
{
    OV_LOG_DETAIL("[-]");
}

#pragma endregion

#pragma region CallBack

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the keyboard callback is forwarded.
 * @param key The keyboard key that was pressed or released.
 * @param scancode The system-specific scancode of the key.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
bool OVVK_API OvVK::SurfaceDependentObject::keyboardCallback(int key, int scancode, int action, int mods)
{
    OV_LOG_ERROR("The base keyboardCallback method of the class SurfaceDependentObject was called.");

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse cursor callback is forwarded.
 * @param mouseX The new cursor x-coordinate, relative to the left edge of the content area.
 * @param mouseY The new cursor y-coordinate, relative to the top edge of the content area.
 */
bool OVVK_API OvVK::SurfaceDependentObject::mouseCursorCallback(double mouseX, double mouseY)
{
    OV_LOG_ERROR("The base mouseCursorCallback method of the class SurfaceDependentObject was called.");

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse button callback is forwarded.
 * @param button The mouse button that was pressed or released.
 * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT. Future releases may add more actions.
 * @param mods Bit field describing which modifier keys were held down.
 */
bool OVVK_API OvVK::SurfaceDependentObject::mouseButtonCallback(int button, int action, int mods)
{
    OV_LOG_ERROR("The base mouseButtonCallback method of the class SurfaceDependentObject was called.");

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the mouse scroll callback is forwarded.
 * @param scrollX The scroll offset along the x-axis.
 * @param scrollY The scroll offset along the y-axis.
 */
bool OVVK_API OvVK::SurfaceDependentObject::mouseScrollCallback(double scrollX, double scrollY)
{
    OV_LOG_ERROR("The base mouseScrollCallback method of the class SurfaceDependentObject was called.");

    // Done:
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method on which the window size callback is forwarded.
 * @param width New width.
 * @param height New heigth.
 */
bool OVVK_API OvVK::SurfaceDependentObject::windowSizeCallback(int _width, int _height) {

    OV_LOG_ERROR("The base windowSizeCallback method of the class SurfaceDependentObject was called.");

    // Done:
    return false;
}

#pragma endregion

#pragma region Get/Set

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method returns the callback types on which forwarding is enabled.
 * @return SurfaceCallbackTypes types on which forwarding is enabled.
 */
OvVK::Surface::SurfaceCallbackTypes OVVK_API OvVK::SurfaceDependentObject::getActiveCallback() const
{
    return reserved->activeCallback;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method returns whether forwarding is enabled on the dependent object.
 * @return TF
 */
bool OVVK_API OvVK::SurfaceDependentObject::getIsActive() const
{
    return reserved->isActive;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Method sets on which callback types the forwarding is enabled.
 * @param activeCallback SurfaceCallbackTypes types on which forwarding is enabled.
 */
void OVVK_API OvVK::SurfaceDependentObject::setActiveCallback(Surface::SurfaceCallbackTypes activeCallback)
{
    reserved->activeCallback = activeCallback;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * The method sets whether forwarding is enabled on the dependent object.
 * @param value Enable or disable forwarding
 */
void OVVK_API OvVK::SurfaceDependentObject::setIsActive(bool value)
{
    reserved->isActive = value;
}

#pragma endregion