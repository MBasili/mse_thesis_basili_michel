var searchData=
[
  ['getframenr_0',['getFrameNr',['../class_ov_g_l_1_1_engine.html#aedd1eb1e3d4052f856ae001835bd98d1',1,'OvGL::Engine']]],
  ['getglfwwindow_1',['getGLFWWindow',['../class_ov_g_l_1_1_engine.html#a9e456891469a2c8b78b88c7a29d9b538',1,'OvGL::Engine']]],
  ['getinstance_2',['getInstance',['../class_ov_g_l_1_1_engine.html#a9a3524ddc1c32a8c49928eaaad16e084',1,'OvGL::Engine']]],
  ['getlutpos_3',['getLutPos',['../class_texture.html#a3e21be686440a3294304907f819aa43f',1,'Texture']]],
  ['getoglbindlesshandle_4',['getOglBindlessHandle',['../class_texture.html#a3beac8dc7663a2dfaecac037e3aa2515',1,'Texture']]],
  ['getoglhandle_5',['getOglHandle',['../class_buffer.html#ac59baa446d446ad5111292cf836d26c8',1,'Buffer::getOglHandle()'],['../class_texture.html#a7ab2a1546a354acaba20940d26776525',1,'Texture::getOglHandle()']]]
];
