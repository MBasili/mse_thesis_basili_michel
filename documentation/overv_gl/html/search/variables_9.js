var searchData=
[
  ['oglbindlesshandle_0',['oglBindlessHandle',['../struct_ov_g_l_1_1_texture_1_1_reserved.html#a79112efffa96d65bf6ed6f378e9ee214',1,'OvGL::Texture::Reserved']]],
  ['oglid_1',['oglId',['../struct_ov_g_l_1_1_atomic_counter_1_1_reserved.html#afb7d168e66af88ccadd217743b196ed7',1,'OvGL::AtomicCounter::Reserved::oglId()'],['../struct_ov_g_l_1_1_buffer_1_1_reserved.html#ac5cdf3a7784a3abd6be65599972e6e6f',1,'OvGL::Buffer::Reserved::oglId()'],['../struct_ov_g_l_1_1_framebuffer_1_1_reserved.html#a39c1d0a7e64032edf2282594cc915118',1,'OvGL::Framebuffer::Reserved::oglId()'],['../struct_ov_g_l_1_1_program_1_1_reserved.html#a7d27686d4588ee8743937b5e171273fe',1,'OvGL::Program::Reserved::oglId()'],['../struct_ov_g_l_1_1_shader_1_1_reserved.html#a79e5c851a821f95c8da88e7c4adfd5ff',1,'OvGL::Shader::Reserved::oglId()'],['../struct_ov_g_l_1_1_texture_1_1_reserved.html#a2d956acedd039cbcba81429f27d08fbb',1,'OvGL::Texture::Reserved::oglId()'],['../struct_ov_g_l_1_1_vertex_array_obj_1_1_reserved.html#abfd1f7ffc12ff789dbec75152a546c29',1,'OvGL::VertexArrayObj::Reserved::oglId()']]],
  ['oglinternalformat_2',['oglInternalFormat',['../struct_ov_g_l_1_1_texture_1_1_reserved.html#a93c334bc3f32bdb4c42c05a5ac39f621',1,'OvGL::Texture::Reserved']]],
  ['opacity_3',['opacity',['../struct_storage_1_1_material_data.html#a13f5a6ac929510f4f50dd9c877f69bc9',1,'Storage::MaterialData']]]
];
