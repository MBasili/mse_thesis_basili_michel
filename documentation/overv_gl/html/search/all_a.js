var searchData=
[
  ['makeresident_0',['makeResident',['../class_texture.html#a36972070a8002668d531e681588a5d74',1,'Texture']]],
  ['mapping_1',['Mapping',['../class_atomic_counter.html#aa5088cf8d70631f041b110138f6411c2',1,'AtomicCounter']]],
  ['material_2',['Material',['../class_material.html',1,'']]],
  ['material_3',['material',['../struct_ov_g_l_1_1_storage_1_1_reserved.html#a423485bcd4d1b1701299a0f29b82a07d',1,'OvGL::Storage::Reserved']]],
  ['materialdata_4',['MaterialData',['../struct_storage_1_1_material_data.html#a5ef7100dced96bb153b19db627fbf24f',1,'Storage::MaterialData::MaterialData()'],['../struct_storage_1_1_material_data.html',1,'Storage::MaterialData']]],
  ['maxlogsize_5',['maxLogSize',['../class_shader.html#a1648c47f431bf103273a2c183a689a59',1,'Shader']]],
  ['maxnrofgeometries_6',['maxNrOfGeometries',['../class_geometry.html#af95be2b76560d572947a86024a5ef53c',1,'Geometry']]],
  ['maxnroflights_7',['maxNrOfLights',['../class_light.html#a170ef1606562cd4fceb327bf06dc6eec',1,'Light']]],
  ['maxnrofmaterials_8',['maxNrOfMaterials',['../class_material.html#a8c135e4bedf580b6bb0e473abc26cdbc',1,'Material']]],
  ['maxnroftextures_9',['maxNrOfTextures',['../class_material.html#acda966dd96b61a2399ec13aa6cac9f57',1,'Material::maxNrOfTextures()'],['../class_texture.html#abd38963359a4b0a2dd1ab46ad5c37048',1,'Texture::maxNrOfTextures()']]],
  ['metalness_10',['metalness',['../struct_storage_1_1_material_data.html#a84d32d57b76c1db4d63a3c39db0cb905',1,'Storage::MaterialData']]],
  ['modelssourcefolder_11',['modelsSourceFolder',['../class_ov_g_l_1_1_engine.html#a859f649603e511a73b7102c48c654180',1,'OvGL::Engine']]],
  ['mrt_12',['mrt',['../struct_ov_g_l_1_1_framebuffer_1_1_reserved.html#a222a037e7eb3d595e03e5eb04a2ac1c1',1,'OvGL::Framebuffer::Reserved']]]
];
