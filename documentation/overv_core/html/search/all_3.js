var searchData=
[
  ['camera_0',['Camera',['../class_camera.html',1,'']]],
  ['children_1',['children',['../struct_ov_1_1_node_1_1_reserved.html#ad8d08cb4371308917a3ba07f52c03199',1,'Ov::Node::Reserved']]],
  ['chunkid_2',['ChunkId',['../class_ovo.html#a0b6a539b6dfa3354fd72b8af86c428ca',1,'Ovo']]],
  ['clear_3',['clear',['../class_bitmap.html#a4897562950948bf47c645d9f251194c1',1,'Bitmap']]],
  ['code_4',['code',['../struct_ov_1_1_shader_1_1_reserved.html#aacd38935ed31c3c9ac88af51f39cc665',1,'Ov::Shader::Reserved']]],
  ['color_5',['color',['../struct_ov_1_1_light_1_1_reserved.html#a40416baa6350f1ad18614dd3c7da548a',1,'Ov::Light::Reserved']]],
  ['config_6',['Config',['../class_config.html',1,'']]],
  ['config_7',['config',['../struct_ov_1_1_core_1_1_reserved.html#a1ec2e6045530d725a2a3b011ae40f478',1,'Ov::Core::Reserved']]],
  ['container_8',['Container',['../class_container.html',1,'']]],
  ['core_9',['Core',['../class_ov_1_1_core.html#a68bfd30769fe75c0f682c69fff67a668',1,'Ov::Core::Core()'],['../class_ov_1_1_core.html',1,'Ov::Core']]],
  ['cpufreq_10',['cpuFreq',['../struct_ov_1_1_timer_1_1_reserved.html#aef89b2f5dc21423615b7ceae41d0310d',1,'Ov::Timer::Reserved']]],
  ['createbytype_11',['createByType',['../class_ovo.html#a63c170cf29405d404f6622bb36be036d',1,'Ovo']]],
  ['customcallback_12',['customCallback',['../struct_ov_1_1_log_1_1_static_reserved.html#a796de3dc52ede9f30260c386b602823c',1,'Ov::Log::StaticReserved']]],
  ['customdata_13',['customData',['../struct_ov_1_1_object_1_1_reserved.html#a322ad203ef37692a7c343b8f2c6c3182',1,'Ov::Object::Reserved']]]
];
