var searchData=
[
  ['name_0',['name',['../struct_ov_1_1_object_1_1_reserved.html#a93a07bb4bef6f82694ff9b3085963cce',1,'Ov::Object::Reserved']]],
  ['normal_1',['normal',['../struct_geometry_1_1_vertex_data.html#af360438653e0170a022b528a8750c380',1,'Geometry::VertexData']]],
  ['nrofaasamples_2',['nrOfAASamples',['../struct_ov_1_1_config_1_1_reserved.html#a7dde34fea0c0b411f8a242fb25e1f07f',1,'Ov::Config::Reserved']]],
  ['nroflevels_3',['nrOfLevels',['../struct_ov_1_1_bitmap_1_1_reserved.html#af6ca27fd6b8bc78f98493e0b90dbd3b8',1,'Ov::Bitmap::Reserved']]],
  ['nroflights_4',['nrOfLights',['../struct_ov_1_1_list_1_1_reserved.html#adfe67639eb75f8b260c095ab7b9f2f37',1,'Ov::List::Reserved']]],
  ['nrofsides_5',['nrOfSides',['../struct_ov_1_1_bitmap_1_1_reserved.html#a5f9edb1395f46645e4f46d29d966a504',1,'Ov::Bitmap::Reserved']]]
];
