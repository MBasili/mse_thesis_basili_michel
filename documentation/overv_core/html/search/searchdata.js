var indexSectionsWithContent =
{
  0: "_abcdefgilmnoprstuvw~",
  1: "abcdfgilmnoprstv",
  2: "o",
  3: "abcdfgilmnorstv~",
  4: "_acdefilmnoprstuvw",
  5: "cflt",
  6: "deilnpuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

