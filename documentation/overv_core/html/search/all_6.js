var searchData=
[
  ['facedata_0',['FaceData',['../struct_geometry_1_1_face_data.html#aa8033a642b98034bf5b8587d1ba43ea1',1,'Geometry::FaceData::FaceData()'],['../struct_geometry_1_1_face_data.html',1,'Geometry::FaceData']]],
  ['filename_1',['filename',['../class_log.html#a22b265f0551555f851015fcfe1f730a2',1,'Log']]],
  ['find_2',['find',['../class_container.html#a5107bfb2b1951aa2908924551c2129d1',1,'Container::find(const std::string &amp;name) const'],['../class_container.html#ad0d03e818be6b54d59f12de83b030b36',1,'Container::find(uint32_t id) const']]],
  ['flag_3',['Flag',['../class_object.html#af82a63a617e08c8b0d73c0567dd98dff',1,'Object']]],
  ['forcerelease_4',['forceRelease',['../class_managed.html#a860f23a96d9b790a10ead4cb5680a113',1,'Managed']]],
  ['format_5',['format',['../struct_ov_1_1_bitmap_1_1_reserved.html#ad2651826a50fcc0b2d7032efe6e55831',1,'Ov::Bitmap::Reserved::format()'],['../struct_ov_1_1_buffer_1_1_reserved.html#a0fbb67482dae9d93fb46964d6799fe73',1,'Ov::Buffer::Reserved::format()']]],
  ['format_6',['Format',['../class_bitmap.html#a4340d7354401f4f2f13f045f097bca84',1,'Bitmap::Format()'],['../class_buffer.html#a9436fbcf473ba861f0b31fe9cb702e93',1,'Buffer::Format()'],['../class_texture.html#a06ca07b71e0f48066b1f2b639db6fd31',1,'Texture::Format()']]],
  ['framebuffer_7',['Framebuffer',['../class_framebuffer.html',1,'']]],
  ['free_8',['free',['../class_ov_1_1_core.html#aa9fffe55537fdf989f8a980d36496a17',1,'Ov::Core::free()'],['../class_managed.html#a4d960585e6d843c15ab3411d1f204cf5',1,'Managed::free()']]]
];
