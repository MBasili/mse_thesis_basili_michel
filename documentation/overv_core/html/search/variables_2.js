var searchData=
[
  ['children_0',['children',['../struct_ov_1_1_node_1_1_reserved.html#ad8d08cb4371308917a3ba07f52c03199',1,'Ov::Node::Reserved']]],
  ['code_1',['code',['../struct_ov_1_1_shader_1_1_reserved.html#aacd38935ed31c3c9ac88af51f39cc665',1,'Ov::Shader::Reserved']]],
  ['color_2',['color',['../struct_ov_1_1_light_1_1_reserved.html#a40416baa6350f1ad18614dd3c7da548a',1,'Ov::Light::Reserved']]],
  ['config_3',['config',['../struct_ov_1_1_core_1_1_reserved.html#a1ec2e6045530d725a2a3b011ae40f478',1,'Ov::Core::Reserved']]],
  ['cpufreq_4',['cpuFreq',['../struct_ov_1_1_timer_1_1_reserved.html#aef89b2f5dc21423615b7ceae41d0310d',1,'Ov::Timer::Reserved']]],
  ['customcallback_5',['customCallback',['../struct_ov_1_1_log_1_1_static_reserved.html#a796de3dc52ede9f30260c386b602823c',1,'Ov::Log::StaticReserved']]],
  ['customdata_6',['customData',['../struct_ov_1_1_object_1_1_reserved.html#a322ad203ef37692a7c343b8f2c6c3182',1,'Ov::Object::Reserved']]]
];
