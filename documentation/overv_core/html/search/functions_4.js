var searchData=
[
  ['facedata_0',['FaceData',['../struct_geometry_1_1_face_data.html#aa8033a642b98034bf5b8587d1ba43ea1',1,'Geometry::FaceData']]],
  ['find_1',['find',['../class_container.html#a5107bfb2b1951aa2908924551c2129d1',1,'Container::find(const std::string &amp;name) const'],['../class_container.html#ad0d03e818be6b54d59f12de83b030b36',1,'Container::find(uint32_t id) const']]],
  ['forcerelease_2',['forceRelease',['../class_managed.html#a860f23a96d9b790a10ead4cb5680a113',1,'Managed']]],
  ['free_3',['free',['../class_ov_1_1_core.html#aa9fffe55537fdf989f8a980d36496a17',1,'Ov::Core::free()'],['../class_managed.html#a4d960585e6d843c15ab3411d1f204cf5',1,'Managed::free()']]]
];
