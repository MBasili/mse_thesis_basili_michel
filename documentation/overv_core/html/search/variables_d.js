var searchData=
[
  ['shader_0',['shader',['../struct_ov_1_1_program_1_1_reserved.html#ac364a2353f2e829014b1dc9117028368',1,'Ov::Program::Reserved']]],
  ['shininess_1',['shininess',['../struct_ov_1_1_material_phong_1_1_reserved.html#a82d44604bd1fc9a3a234d700e4d3214d',1,'Ov::MaterialPhong::Reserved']]],
  ['size_2',['size',['../struct_bitmap_1_1_layer.html#aed8c938fea91284a83f56d41d7eab527',1,'Bitmap::Layer::size()'],['../struct_ov_1_1_buffer_1_1_reserved.html#a248baf27cddbdd7b5314693620800c4e',1,'Ov::Buffer::Reserved::size()'],['../struct_ov_1_1_framebuffer_1_1_static_reserved.html#ad312b1995b3a8ba2009110ba4d09c9c5',1,'Ov::Framebuffer::StaticReserved::size()'],['../struct_framebuffer_1_1_attachment.html#af628d8fa7d67bb91eb8f8dda4ff1a6b4',1,'Framebuffer::Attachment::size()']]],
  ['specular_3',['specular',['../struct_ov_1_1_material_phong_1_1_reserved.html#a055f116127de1bbe2c4f71ed5fde7e05',1,'Ov::MaterialPhong::Reserved']]]
];
