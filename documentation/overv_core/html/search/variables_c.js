var searchData=
[
  ['reference_0',['reference',['../struct_list_1_1_renderable_elem.html#a78db8e0640b2a22b6c3c614c74177ad8',1,'List::RenderableElem']]],
  ['refractionindex_1',['refractionIndex',['../struct_ov_1_1_material_pbr_1_1_reserved.html#a33e8b36539531aa0cdf25b3b0838ab91',1,'Ov::MaterialPbr::Reserved']]],
  ['renderabledata_2',['renderableData',['../struct_list_1_1_renderable_elem.html#a51fba81caf3fef1390c1e24c04d6499f',1,'List::RenderableElem']]],
  ['renderableelem_3',['renderableElem',['../struct_ov_1_1_list_1_1_reserved.html#acc2365700bc07e78c3d6c49179fe0f17',1,'Ov::List::Reserved']]],
  ['rimintensity_4',['rimIntensity',['../struct_ov_1_1_material_pbr_1_1_reserved.html#adbbfd716c4ebec28e8598be750a37520',1,'Ov::MaterialPbr::Reserved']]],
  ['roughness_5',['roughness',['../struct_ov_1_1_material_pbr_1_1_reserved.html#a4db38da6f2147ab291612cd5ad037004',1,'Ov::MaterialPbr::Reserved']]]
];
