var searchData=
[
  ['bitmap_0',['Bitmap',['../class_bitmap.html',1,'Bitmap'],['../class_bitmap.html#abce44c051759086619c51970a2d4b33f',1,'Bitmap::Bitmap()'],['../class_bitmap.html#a65d68d37b550bc4a86acef675fe597c0',1,'Bitmap::Bitmap(Bitmap &amp;&amp;other)'],['../class_bitmap.html#a1748001d68d6f713610feda6806881da',1,'Bitmap::Bitmap(Format format, uint32_t sizeX, uint32_t sizeY, uint8_t *data)'],['../class_bitmap.html#a0ba7e245af306b17d47095ce4037cd01',1,'Bitmap::Bitmap(const std::string &amp;name)']]],
  ['buffer_1',['Buffer',['../class_buffer.html',1,'']]]
];
