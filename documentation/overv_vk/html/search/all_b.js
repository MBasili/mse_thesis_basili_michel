var searchData=
[
  ['needdescriptorsetsupdate_0',['needDescriptorSetsUpdate',['../struct_ov_v_k_1_1_textures_descriptor_sets_manager_1_1_reserved.html#a31c243a8da55cb2779d60b32246b9f13',1,'OvVK::TexturesDescriptorSetsManager::Reserved']]],
  ['needgeneratemipmap_1',['needGenerateMipmap',['../struct_ov_v_k_1_1_texture_1_1_reserved.html#a7c5dbbce24dbd01578fb20a2821b0a98',1,'OvVK::Texture::Reserved']]],
  ['nrofaasamples_2',['nrOfAASamples',['../struct_ov_v_k_1_1_engine_1_1_reserved.html#a58f5d82c3056adaf2a450faa273db726',1,'OvVK::Engine::Reserved']]],
  ['nrofasinstance_3',['nrOfASInstance',['../struct_ov_v_k_1_1_top_level_acceleration_structure_1_1_reserved.html#a6e6ec6a8ddcb89629a8c77df10becf1e',1,'OvVK::TopLevelAccelerationStructure::Reserved']]],
  ['nrofhitshadergroupsneeded_4',['nrOfHitShaderGroupsNeeded',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a974566b27eb782ca40ccd15eb069719f',1,'OvVK::RTScene::Reserved']]],
  ['nrofinstances_5',['nrOfInstances',['../struct_ov_v_k.html#aface7da619f34e5f472524f5a19c21d9',1,'OvVK']]],
  ['nroflayers_6',['nrOfLayers',['../struct_ov_v_k_1_1_image_1_1_reserved.html#abf7397707bb1aafe83f2dbedb62077ad',1,'OvVK::Image::Reserved']]],
  ['nroflevels_7',['nrOfLevels',['../struct_ov_v_k_1_1_image_1_1_reserved.html#ac159fae992f175214a88ce00698ef480',1,'OvVK::Image::Reserved']]],
  ['nroflights_8',['nrOfLights',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a8177cc2f00a030042c5e8df91a0f1708',1,'OvVK::RTScene::Reserved']]]
];
