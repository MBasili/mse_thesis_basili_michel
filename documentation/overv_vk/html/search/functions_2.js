var searchData=
[
  ['getframeinflight_0',['getFrameInFlight',['../class_ov_v_k_1_1_engine.html#a2ed92f76304be13f68441ee21f44747f',1,'OvVK::Engine']]],
  ['getframenr_1',['getFrameNr',['../class_ov_v_k_1_1_engine.html#a1ea794566f8cb8d88008a7bfc96a471d',1,'OvVK::Engine']]],
  ['getinstance_2',['getInstance',['../class_ov_v_k_1_1_engine.html#a32402be8e8ae2c5656b1264e3f2b3a4d',1,'OvVK::Engine']]],
  ['getlogicaldevice_3',['getLogicalDevice',['../class_ov_v_k_1_1_engine.html#af5add715f46a44812edb5c1584133eb3',1,'OvVK::Engine']]],
  ['getpipelines_4',['getPipelines',['../class_ov_v_k_1_1_engine.html#a0a44df8fc48f4e685cf5d697fd634788',1,'OvVK::Engine']]],
  ['getpresentsurface_5',['getPresentSurface',['../class_ov_v_k_1_1_engine.html#a189ab5f69b511fbc2518ba186e72fb4b',1,'OvVK::Engine']]],
  ['getrequireddeviceextensions_6',['getRequiredDeviceExtensions',['../class_ov_v_k_1_1_engine.html#ad892521f12936f254c9e14f85853dc4b',1,'OvVK::Engine']]],
  ['getrequiredinstanceextensions_7',['getRequiredInstanceExtensions',['../class_ov_v_k_1_1_engine.html#a2000e576dee0618bf6e3e6878be91d0e',1,'OvVK::Engine']]],
  ['getrequiredvalidationlayers_8',['getRequiredValidationLayers',['../class_ov_v_k_1_1_engine.html#a93ad35fac08b794d58a4a5b493ab5cee',1,'OvVK::Engine']]],
  ['getswapchain_9',['getSwapchain',['../class_ov_v_k_1_1_engine.html#aac1ac6f1cf5278c47d65ce045954aaf5',1,'OvVK::Engine']]],
  ['getthreadcount_10',['getThreadCount',['../class_thread_pool.html#a8a0f4c7d133dabaf9760677b7ffe497e',1,'ThreadPool']]],
  ['getthreads_11',['getThreads',['../class_thread_pool.html#a597c9d7577334d534ebc01203882e3ff',1,'ThreadPool']]],
  ['getvulkaninstance_12',['getVulkanInstance',['../class_ov_v_k_1_1_engine.html#a55214fa76eefb7769a6304df7ecf27d6',1,'OvVK::Engine']]]
];
