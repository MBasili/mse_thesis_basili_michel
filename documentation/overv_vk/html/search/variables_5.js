var searchData=
[
  ['facebuffer_0',['faceBuffer',['../struct_ov_v_k_1_1_geometry_1_1_reserved.html#adb50df3cee9dcbc9d20c3b405fa3a0e9',1,'OvVK::Geometry::Reserved']]],
  ['format_1',['format',['../struct_ov_v_k_1_1_image_1_1_reserved.html#a7f64a3fdf62cc316054ac7484c1ebf77',1,'OvVK::Image::Reserved::format()'],['../struct_ov_v_k_1_1_image_view_1_1_reserved.html#a67ff3a5d2c8cdefc9d0c01aef950864e',1,'OvVK::ImageView::Reserved::format()']]],
  ['framebuffers_2',['framebuffers',['../struct_ov_v_k_1_1_g_u_i_pipeline_1_1_reserved.html#a8e0f0a0ea1f408d359fc370746ecb1a6',1,'OvVK::GUIPipeline::Reserved']]],
  ['framecounter_3',['frameCounter',['../struct_ov_v_k_1_1_engine_1_1_reserved.html#a0385102155f6ff4533865d5a28ad4c44',1,'OvVK::Engine::Reserved']]],
  ['frameinflight_4',['frameInFlight',['../struct_ov_v_k_1_1_engine_1_1_reserved.html#ac4be309f2cd27dff08fc5c69f356cd3c',1,'OvVK::Engine::Reserved']]]
];
