var searchData=
[
  ['texhandles_0',['texHandles',['../struct_ov_v_k_1_1_material_1_1_reserved.html#a0991a8552b5169f753278340bc3266eb',1,'OvVK::Material::Reserved']]],
  ['texturessourcefolder_1',['texturesSourceFolder',['../class_ov_v_k_1_1_engine.html#ad6b19060c5e4b5cc99f94ac195ab31b3',1,'OvVK::Engine']]],
  ['texturestoupdate_2',['texturesToUpdate',['../struct_ov_v_k_1_1_textures_descriptor_sets_manager_1_1_reserved.html#a750caa4057e836320afb8b594863f248',1,'OvVK::TexturesDescriptorSetsManager::Reserved']]],
  ['transfercp_3',['transferCP',['../struct_ov_v_k.html#a87807accdf68ad350d50e3901cac5f14',1,'OvVK']]],
  ['transformsdatabuffer_4',['transformsDataBuffer',['../struct_ov_v_k_1_1_bottom_level_acceleration_structure_1_1_reserved.html#a21f06209b72ae40491dfdf4540003ca1',1,'OvVK::BottomLevelAccelerationStructure::Reserved']]],
  ['types_5',['types',['../struct_ov_v_k_1_1_command_pool_1_1_reserved.html#a9f29083ead7939634f613f0f4b755ea6',1,'OvVK::CommandPool::Reserved']]]
];
