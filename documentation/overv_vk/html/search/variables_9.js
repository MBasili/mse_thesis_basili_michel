var searchData=
[
  ['level_0',['level',['../struct_ov_v_k_1_1_command_buffer_1_1_reserved.html#a4c60106de7136975cf56f998312bc8fe',1,'OvVK::CommandBuffer::Reserved']]],
  ['lightsdevicebuffers_1',['lightsDeviceBuffers',['../struct_ov_v_k.html#aa68f4ceed2698db140030a33a1ac95c0',1,'OvVK']]],
  ['lightshostbuffer_2',['lightsHostBuffer',['../struct_ov_v_k.html#aef419cfa4cf8ba3cfbcffbb71f482f72',1,'OvVK']]],
  ['locksyncobj_3',['lockSyncObj',['../struct_ov_v_k_1_1_buffer_1_1_reserved.html#a29fe32d343e14ef2cfae1bb0158da379',1,'OvVK::Buffer::Reserved::lockSyncObj()'],['../struct_ov_v_k_1_1_command_buffer_1_1_reserved.html#a772dfe56334c65ba98f680b76fb0e464',1,'OvVK::CommandBuffer::Reserved::lockSyncObj()'],['../struct_ov_v_k_1_1_image_1_1_reserved.html#a899be8cdbe8828993979812a03edfd61',1,'OvVK::Image::Reserved::lockSyncObj()']]],
  ['logicaldevice_4',['logicalDevice',['../struct_ov_v_k_1_1_engine_1_1_reserved.html#a79c08960233caeead534a88f83bf3c0a',1,'OvVK::Engine::Reserved::logicalDevice()'],['../struct_ov_v_k_1_1_logical_device_1_1_reserved.html#a5c705a23ac8443c014f286985b1f87f7',1,'OvVK::LogicalDevice::Reserved::logicalDevice()']]],
  ['lutpos_5',['lutPos',['../struct_ov_v_k_1_1_material_1_1_reserved.html#ac08cf8b9b63e7474659b49003c49d9d3',1,'OvVK::Material::Reserved::lutPos()'],['../struct_ov_v_k_1_1_texture_1_1_reserved.html#a00c025b4c61e6c6732c0465f093fc207',1,'OvVK::Texture::Reserved::lutPos()']]]
];
