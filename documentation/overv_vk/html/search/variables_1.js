var searchData=
[
  ['bitmapbuffer_0',['bitmapBuffer',['../struct_ov_v_k_1_1_texture_1_1_reserved.html#a670d44abdf7a9980941c6e3fb56532f2',1,'OvVK::Texture::Reserved']]],
  ['blas_1',['blas',['../struct_ov_v_k_1_1_r_t_obj_desc_1_1_reserved.html#aefa67affc59b89d28949a9417375c980',1,'OvVK::RTObjDesc::Reserved']]],
  ['blasstobuild_2',['blassToBuild',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a573e827d3a624b904c9777bdaf573b78',1,'OvVK::RTScene::Reserved']]],
  ['buffer_3',['buffer',['../struct_ov_v_k_1_1_buffer_1_1_reserved.html#a23f554265f30e834daa726af7467f2c4',1,'OvVK::Buffer::Reserved::buffer()'],['../struct_ov_v_k_1_1_shader_1_1_reserved.html#a450d71ad4643b63200062792c4c5ae06',1,'OvVK::Shader::Reserved::buffer()']]],
  ['bufferimagecopyregions_4',['bufferImageCopyRegions',['../struct_ov_v_k_1_1_texture_1_1_reserved.html#a7e1cb0be0ddfca7873f6c6afb5f34815',1,'OvVK::Texture::Reserved']]],
  ['buildaccelerationstructureflagskhr_5',['buildAccelerationStructureFlagsKHR',['../struct_ov_v_k_1_1_acceleration_structure_1_1_reserved.html#a5a355db6fba0498e1f5d422eafdfed54',1,'OvVK::AccelerationStructure::Reserved']]]
];
