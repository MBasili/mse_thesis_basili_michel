var searchData=
[
  ['material_0',['Material',['../class_material.html',1,'']]],
  ['materialdata_1',['MaterialData',['../struct_storage_1_1_material_data.html#a5ef7100dced96bb153b19db627fbf24f',1,'Storage::MaterialData::MaterialData()'],['../struct_storage_1_1_material_data.html',1,'Storage::MaterialData']]],
  ['materials_2',['materials',['../struct_ov_v_k_1_1_r_t_obj_desc_1_1_reserved.html#acc2abb891eb70c3d60ea1a698d690264',1,'OvVK::RTObjDesc::Reserved']]],
  ['materialsdevicebuffers_3',['materialsDeviceBuffers',['../struct_ov_v_k.html#a5558522717d7f9f7f3268a476f04ee81',1,'OvVK']]],
  ['materialshostbuffer_4',['materialsHostBuffer',['../struct_ov_v_k.html#a3d0fd6056dc3c6a298b04239cf2aefed',1,'OvVK']]],
  ['matrix_5',['matrix',['../struct_r_t_scene_1_1_renderable_elem.html#a2859c1e3872919fa5054355be790ccb9',1,'RTScene::RenderableElem']]],
  ['maxlogsize_6',['maxLogSize',['../class_shader.html#a1648c47f431bf103273a2c183a689a59',1,'Shader']]],
  ['maxnroflights_7',['maxNrOfLights',['../class_light.html#a170ef1606562cd4fceb327bf06dc6eec',1,'Light']]],
  ['maxnrofmaterials_8',['maxNrOfMaterials',['../class_material.html#a8c135e4bedf580b6bb0e473abc26cdbc',1,'Material']]],
  ['maxnroftextures_9',['maxNrOfTextures',['../class_material.html#acda966dd96b61a2399ec13aa6cac9f57',1,'Material::maxNrOfTextures()'],['../class_texture.html#abd38963359a4b0a2dd1ab46ad5c37048',1,'Texture::maxNrOfTextures()']]],
  ['metalness_10',['metalness',['../struct_storage_1_1_material_data.html#a84d32d57b76c1db4d63a3c39db0cb905',1,'Storage::MaterialData']]],
  ['modelssourcefolder_11',['modelsSourceFolder',['../class_ov_v_k_1_1_engine.html#a9a8502bf2db97ba66cc3fc567bdc03db',1,'OvVK::Engine']]]
];
