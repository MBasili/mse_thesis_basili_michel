var searchData=
[
  ['rederingstarted_0',['rederingStarted',['../struct_ov_v_k_1_1_g_u_i_pipeline_1_1_reserved.html#ac61de9648f504370f371cece9a95ed23',1,'OvVK::GUIPipeline::Reserved']]],
  ['rederingstatussyncobjcpu_1',['rederingStatusSyncObjCPU',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a4faf4ad1babfdee969b45b0adcd77a6e',1,'OvVK::RTScene::Reserved']]],
  ['rederingstatussyncobjgpu_2',['rederingStatusSyncObjGPU',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a5c93eaeec605862a02ab313b3cc17afe',1,'OvVK::RTScene::Reserved']]],
  ['reference_3',['reference',['../struct_r_t_scene_1_1_renderable_elem.html#a64a4c4c56adab3be6bcd1380d394a532',1,'RTScene::RenderableElem']]],
  ['refractionindex_4',['refractionIndex',['../struct_storage_1_1_material_data.html#a525446ae3540518b0cb27c21184afdda',1,'Storage::MaterialData']]],
  ['renderableelements_5',['renderableElements',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#aab40ad13c820946e1d90fe1191e58dbf',1,'OvVK::RTScene::Reserved']]],
  ['renderabletoupdate_6',['renderableToUpdate',['../struct_ov_v_k.html#ad39367b04aeccb534755c1e26fb0692b',1,'OvVK']]],
  ['renderpass_7',['renderPass',['../struct_ov_v_k_1_1_g_u_i_pipeline_1_1_reserved.html#a4db535f1a2e4e8bb7f57f3a71ed2d7c6',1,'OvVK::GUIPipeline::Reserved']]],
  ['rimintensity_8',['rimIntensity',['../struct_storage_1_1_material_data.html#a2372be0aa3f86ae93b6fc20e1736e7ea',1,'Storage::MaterialData']]],
  ['roughness_9',['roughness',['../struct_storage_1_1_material_data.html#ad16bd0673067763ff5de7702091bc960',1,'Storage::MaterialData']]],
  ['rtobjdescsdevicebuffers_10',['rtObjDescsDeviceBuffers',['../struct_ov_v_k.html#ae64560c9abfa91248f59b9cd6d097a33',1,'OvVK']]],
  ['rtobjdescshostbuffer_11',['rtObjDescsHostBuffer',['../struct_ov_v_k.html#aad0a9168b811d537a6f5bd682ac73cb6',1,'OvVK']]],
  ['rtshadergrouptype_12',['rtShaderGroupType',['../struct_ov_v_k_1_1_r_t_shader_group_1_1_reserved.html#a3276152bbb2a9afde333536e612e6498',1,'OvVK::RTShaderGroup::Reserved']]]
];
