var searchData=
[
  ['opacity_0',['opacity',['../struct_storage_1_1_material_data.html#a13f5a6ac929510f4f50dd9c877f69bc9',1,'Storage::MaterialData']]],
  ['ownershipcomputestatussyncobjgpu_1',['ownershipComputeStatusSyncObjGPU',['../struct_ov_v_k.html#a9270ffb545521826d1ec7ea9aa45d084',1,'OvVK']]],
  ['ownershipgraphicstatussyncobjgpu_2',['ownershipGraphicStatusSyncObjGPU',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a6ded75ff25d2940417321a40220b48a1',1,'OvVK::RTScene::Reserved::ownershipGraphicStatusSyncObjGPU()'],['../struct_ov_v_k.html#afdc88b3a2b35ac914753272a9b2b2a53',1,'OvVK::ownershipGraphicStatusSyncObjGPU()']]],
  ['ownershiprelease_3',['ownershipRelease',['../struct_ov_v_k_1_1_g_u_i_pipeline_1_1_reserved.html#a4d3eb80c7f63754601b73f3e910296ec',1,'OvVK::GUIPipeline::Reserved']]],
  ['ownershiptransferstatussyncobjgpu_4',['ownershipTransferStatusSyncObjGPU',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#aa325ee0eb1f31efa5d0b2fc9a3623bdc',1,'OvVK::RTScene::Reserved']]]
];
