var searchData=
[
  ['geometriesbuildrangeas_0',['geometriesBuildRangeAS',['../struct_ov_v_k_1_1_top_level_acceleration_structure_1_1_reserved.html#aaf66f78e4fbed0c1020dfff1cb95c2c9',1,'OvVK::TopLevelAccelerationStructure::Reserved']]],
  ['geometriesdata_1',['geometriesData',['../struct_ov_v_k_1_1_bottom_level_acceleration_structure_1_1_reserved.html#a894cd11acf0b501f57b1ce67444a4268',1,'OvVK::BottomLevelAccelerationStructure::Reserved']]],
  ['geometriesflag_2',['geometriesFlag',['../struct_ov_v_k_1_1_top_level_acceleration_structure_1_1_reserved.html#a003b468c2c2457f897c324656d49f8b9',1,'OvVK::TopLevelAccelerationStructure::Reserved']]],
  ['geometryas_3',['geometryAS',['../struct_ov_v_k_1_1_top_level_acceleration_structure_1_1_reserved.html#a43f280114dcbc887d486a44b774916e5',1,'OvVK::TopLevelAccelerationStructure::Reserved']]],
  ['geomtomaterial_4',['geomToMaterial',['../struct_ov_v_k_1_1_r_t_obj_desc_1_1_reserved.html#a9144e1d9e73bee1c549f2e39cb9d0150',1,'OvVK::RTObjDesc::Reserved']]],
  ['globaluniformdevicebuffers_5',['globalUniformDeviceBuffers',['../struct_ov_v_k.html#a0d3c6569ef62d998a3a2d6a57c2dce23',1,'OvVK']]],
  ['globaluniformhostbuffer_6',['globalUniformHostBuffer',['../struct_ov_v_k.html#a2ed2a42daf22b4eaffbd1bff29bef8f0',1,'OvVK']]],
  ['glslversion_7',['glslVersion',['../class_shader.html#a913ab5f4fbf9fd87c576433bd9d5682b',1,'Shader']]]
];
