var searchData=
[
  ['sampler_0',['Sampler',['../class_sampler.html',1,'']]],
  ['sampler_1',['sampler',['../struct_ov_v_k_1_1_texture_1_1_reserved.html#a7d59c309f115ebe5e8199f71ca853b55',1,'OvVK::Texture::Reserved']]],
  ['scratchbuffer_2',['scratchBuffer',['../struct_ov_v_k_1_1_acceleration_structure_1_1_reserved.html#aa8220a8d866ede75163bac47a25d58e5',1,'OvVK::AccelerationStructure::Reserved']]],
  ['secondarytransfercb_3',['secondaryTransferCB',['../struct_ov_v_k.html#afe2cea7120ec81d521d7a45c78576577',1,'OvVK']]],
  ['setthreadcount_4',['setThreadCount',['../class_thread_pool.html#abb91ed946b72f9fbdf9532ee83a45926',1,'ThreadPool']]],
  ['shader_5',['Shader',['../class_shader.html',1,'']]],
  ['stagingasinstancetlassdatabuffers_6',['stagingASInstanceTLASsDataBuffers',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a27ca3af5e01edb07e61b9e2912d6f2a8',1,'OvVK::RTScene::Reserved']]],
  ['staginggeometriesrtobjdescsdatabuffers_7',['stagingGeometriesRTObjDescsDataBuffers',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#a189535fedd20570d7d9f28ddd8310801',1,'OvVK::RTScene::Reserved']]],
  ['stagingtransformsblassdatabuffers_8',['stagingTransformsBLASsDataBuffers',['../struct_ov_v_k_1_1_r_t_scene_1_1_reserved.html#ad26b37b26d8244dda1eb47cb1f780541',1,'OvVK::RTScene::Reserved']]],
  ['statussyncobj_9',['statusSyncObj',['../struct_ov_v_k_1_1_buffer_1_1_reserved.html#ab28dc1672d0414f6e04649bca5ad17ea',1,'OvVK::Buffer::Reserved::statusSyncObj()'],['../struct_ov_v_k_1_1_command_buffer_1_1_reserved.html#afe74c1b4cad25517ab508a3afa1e3a3b',1,'OvVK::CommandBuffer::Reserved::statusSyncObj()'],['../struct_ov_v_k_1_1_image_1_1_reserved.html#abd9021190dca8a75f8df36f11602fd99',1,'OvVK::Image::Reserved::statusSyncObj()']]],
  ['statussyncobjscpu_10',['statusSyncObjsCPU',['../struct_ov_v_k_1_1_pipeline_1_1_reserved.html#a90e811229d55d443678d51b6e3727bb5',1,'OvVK::Pipeline::Reserved']]],
  ['statussyncobjsgpu_11',['statusSyncObjsGPU',['../struct_ov_v_k_1_1_pipeline_1_1_reserved.html#a3cd2f11200c7c2f3c2bd81620bba76c2',1,'OvVK::Pipeline::Reserved']]],
  ['statussyncobjspresentation_12',['statusSyncObjsPresentation',['../struct_ov_v_k_1_1_pipeline_1_1_reserved.html#a95814a7986d7b0cf1532cdddb8cbfbfe',1,'OvVK::Pipeline::Reserved']]],
  ['storage_13',['Storage',['../class_storage.html',1,'']]],
  ['storagedescriptorsetsmanager_14',['StorageDescriptorSetsManager',['../class_storage_descriptor_sets_manager.html',1,'']]],
  ['storagedescritorsetsmanager_15',['storageDescritorSetsManager',['../struct_ov_v_k.html#ad6631809284fa1afcef56954b94f9891',1,'OvVK']]],
  ['storedtype_16',['StoredType',['../class_storage.html#adf4da77c56186872823722d6f7ed0825',1,'Storage']]],
  ['surface_17',['Surface',['../class_surface.html',1,'']]],
  ['surface_18',['surface',['../struct_ov_v_k_1_1_surface_1_1_reserved.html#a886e28c2d745ac4badbbe903ebdc5836',1,'OvVK::Surface::Reserved']]],
  ['surfacecallbacktypes_19',['SurfaceCallbackTypes',['../class_surface.html#acffeffcbb05e5dae65f966ff8e6261dd',1,'Surface']]],
  ['surfacedependentobject_20',['SurfaceDependentObject',['../class_surface_dependent_object.html',1,'']]],
  ['swapchain_21',['Swapchain',['../class_swapchain.html',1,'']]],
  ['swapchain_22',['swapchain',['../struct_ov_v_k_1_1_engine_1_1_reserved.html#a3fbbd3d66b6f9f0de8103227c5886276',1,'OvVK::Engine::Reserved::swapchain()'],['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#a83c5950a80e83a5001b49c8ca6416771',1,'OvVK::Swapchain::Reserved::swapchain()']]],
  ['swapchainextent_23',['swapchainExtent',['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#a762a87f9e3a73dc791113697eb7c84b2',1,'OvVK::Swapchain::Reserved']]],
  ['swapchainimageformat_24',['swapchainImageFormat',['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#a7be4b9007196eaf9ef55cc78dfd83784',1,'OvVK::Swapchain::Reserved']]],
  ['swapchainimages_25',['swapchainImages',['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#a48d316f0a40c7263509ad8d8664505b0',1,'OvVK::Swapchain::Reserved']]],
  ['swapchainimageslayouts_26',['swapchainImagesLayouts',['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#a1f151466a45b3c948d96daa8147d3e4b',1,'OvVK::Swapchain::Reserved']]],
  ['swapchainimagesqueuefamilyindex_27',['swapchainImagesQueueFamilyIndex',['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#a576434e5964b4323c8c07a9590db6715',1,'OvVK::Swapchain::Reserved']]],
  ['swapchainimageviews_28',['swapchainImageViews',['../struct_ov_v_k_1_1_swapchain_1_1_reserved.html#ade23452cc148898650dd3694b0debb74',1,'OvVK::Swapchain::Reserved']]]
];
