var searchData=
[
  ['debugmessenger_0',['debugMessenger',['../struct_ov_v_k_1_1_vulkan_instance_1_1_reserved.html#ad6b1592b9f3c5accbe3b86f37d9a4610',1,'OvVK::VulkanInstance::Reserved']]],
  ['descriptorimagesinfos_1',['descriptorImagesInfos',['../struct_ov_v_k_1_1_textures_descriptor_sets_manager_1_1_reserved.html#a01182021522e18a2e7118072509d7a1b',1,'OvVK::TexturesDescriptorSetsManager::Reserved']]],
  ['descriptorpool_2',['descriptorPool',['../struct_ov_v_k_1_1_descriptor_sets_manager_1_1_reserved.html#a0105ad7ca5f3a3a52515e033821d8f50',1,'OvVK::DescriptorSetsManager::Reserved::descriptorPool()'],['../struct_ov_v_k_1_1_g_u_i_pipeline_1_1_reserved.html#a5399f271649e7021a7c368ca741dc98a',1,'OvVK::GUIPipeline::Reserved::descriptorPool()']]],
  ['descriptorsetlayout_3',['descriptorSetLayout',['../struct_ov_v_k_1_1_descriptor_sets_manager_1_1_reserved.html#a8102780a2dde48aec549bb3e66ee17d6',1,'OvVK::DescriptorSetsManager::Reserved']]],
  ['descriptorsets_4',['descriptorSets',['../struct_ov_v_k_1_1_descriptor_sets_manager_1_1_reserved.html#ac9b518aaa804dc1e036f58ff2f603b4b',1,'OvVK::DescriptorSetsManager::Reserved']]],
  ['descriptorsetsmanager_5',['DescriptorSetsManager',['../class_descriptor_sets_manager.html',1,'']]],
  ['deviceasinstancesbuffer_6',['deviceASInstancesBuffer',['../struct_ov_v_k_1_1_top_level_acceleration_structure_1_1_reserved.html#af6f7c75e10cfab07ab8f98def414f17b',1,'OvVK::TopLevelAccelerationStructure::Reserved']]],
  ['devicegeometrydata_7',['deviceGeometryData',['../struct_ov_v_k_1_1_r_t_obj_desc_1_1_reserved.html#aa1df80b0d3b8788f5b25c26f2360a3cf',1,'OvVK::RTObjDesc::Reserved']]],
  ['drawguiobjects_8',['drawGUIObjects',['../struct_ov_v_k_1_1_g_u_i_pipeline_1_1_reserved.html#ad902ba602c887ada1eed50cb6fe0853e',1,'OvVK::GUIPipeline::Reserved']]]
];
