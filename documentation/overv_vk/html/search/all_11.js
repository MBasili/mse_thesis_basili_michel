var searchData=
[
  ['texhandles_0',['texHandles',['../struct_ov_v_k_1_1_material_1_1_reserved.html#a0991a8552b5169f753278340bc3266eb',1,'OvVK::Material::Reserved']]],
  ['texture_1',['Texture',['../class_texture.html',1,'']]],
  ['texture2d_2',['Texture2D',['../class_texture2_d.html',1,'']]],
  ['texturesdescriptorsetsmanager_3',['TexturesDescriptorSetsManager',['../class_textures_descriptor_sets_manager.html',1,'']]],
  ['texturessourcefolder_4',['texturesSourceFolder',['../class_ov_v_k_1_1_engine.html#ad6b19060c5e4b5cc99f94ac195ab31b3',1,'OvVK::Engine']]],
  ['texturestoupdate_5',['texturesToUpdate',['../struct_ov_v_k_1_1_textures_descriptor_sets_manager_1_1_reserved.html#a750caa4057e836320afb8b594863f248',1,'OvVK::TexturesDescriptorSetsManager::Reserved']]],
  ['thread_6',['Thread',['../class_thread.html',1,'']]],
  ['threaddata_7',['ThreadData',['../struct_ov_v_k_1_1_loader_1_1_thread_data.html',1,'OvVK::Loader::ThreadData'],['../struct_ov_v_k_1_1_r_t_scene_1_1_thread_data.html',1,'OvVK::RTScene::ThreadData']]],
  ['threadpool_8',['ThreadPool',['../class_thread_pool.html',1,'']]],
  ['toplevelaccelerationstructure_9',['TopLevelAccelerationStructure',['../class_top_level_acceleration_structure.html',1,'']]],
  ['transfercp_10',['transferCP',['../struct_ov_v_k.html#a87807accdf68ad350d50e3901cac5f14',1,'OvVK']]],
  ['transformsdatabuffer_11',['transformsDataBuffer',['../struct_ov_v_k_1_1_bottom_level_acceleration_structure_1_1_reserved.html#a21f06209b72ae40491dfdf4540003ca1',1,'OvVK::BottomLevelAccelerationStructure::Reserved']]],
  ['type_12',['Type',['../class_r_t_shader_group.html#abe5932d6bdfc16e1ed10fb0f6644b31b',1,'RTShaderGroup']]],
  ['types_13',['types',['../struct_ov_v_k_1_1_command_pool_1_1_reserved.html#a9f29083ead7939634f613f0f4b755ea6',1,'OvVK::CommandPool::Reserved']]]
];
